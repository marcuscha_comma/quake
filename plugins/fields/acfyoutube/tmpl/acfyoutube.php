<?php

/**
 * @package         Advanced Custom Fields
 * @version         0.4.0 Free
 *
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 */

defined('_JEXEC') or die;

if (!$videoID = $field->value)
{
	return;
}

if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $field->value, $match))
{
	$videoID = $match[1];
}

// Setup Variables
$id             = 'acf_yt_' . $field->id;
$width          = $fieldParams->get('width', '480');
$height         = $fieldParams->get('height', '270');
$query          = $videoID;



// Output
$buffer = '
	<iframe
		id="' . $id . '"
		class="acf_yt"
		width="' . $width . '"
		height="' . $height . '"
		src="https://www.youtube.com/embed/' . $query . '"
		frameborder="0"
		allowfullscreen>
	</iframe>
';

echo $buffer;
