<?php
/**
 * @package         Slider field plugin
 * @link            http://www.stereonomy.com
 * @copyright       Copyright © 2017 Anthony Ceccarelli - Stereonomy - All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldSyslider extends JFormField
{
	

	public function getInput() {
		//dump($this->value, "$this->value");
		$doc = JFactory::getDocument();
		
		$doc->addScript(JUri::root() . '/plugins/fields/syslider/assets/js/jquery-ui.min.js');
		$doc->addScript(JUri::root() . '/plugins/fields/syslider/assets/js/tooltipster.bundle.min.js');
		$doc->addScript(JUri::root() . '/plugins/fields/syslider/assets/js/tooltipster-follower.min.js');
		$doc->addScript(JUri::root() . '/plugins/fields/syslider/assets/js/syslider.js');
		$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/jquery-ui.min.css');
		$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/jquery-ui.structure.min.css');
		$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/tooltipster.bundle.min.css');
		$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/tooltipster-follower.min.css');
		$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/jquery-ui.theme.min.css');
		$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/syslider.css');

		if($this->getAttribute('sytooltipstyle') == 1)
		{
			$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css');
		}

		$mini = $this->getAttribute('syminimum');
		$maxi = $this->getAttribute('symaximum');
		$step = $this->getAttribute('systep');
		$base = $this->getAttribute('sybase');
		$width = explode(',', $this->getAttribute('sywidth'));

		if ($this->value == "")
		{
			$theValue = $base;
		}
		else 
		{
			$theValue = $this->value;
		}

		$doc->addStyleDeclaration('
			.syslider {
				width:'. $width[0] .';
			}
		');
		
		$doc->addScriptDeclaration('
			jQuery(document).ready(function(){	
				var tooltipstyle = '.$this->getAttribute('sytooltipstyle').';
				switch(tooltipstyle) {
				    case 0:
				        var ttTheme = "default";
				        break;
				    case 1:
				        var ttTheme = "tooltipster-light";
				        break;
				    case 2:
				        var ttTheme = "tooltipster-borderless";
				        break;
				    case 3:
				        var ttTheme = "tooltipster-punk";
				        break;
				    case 4:
				        var ttTheme = "tooltipster-noir";
				        break;
				    case 5:
				        var ttTheme = "tooltipster-shadow";
				        break;
				    default:
				        var ttTheme = "default";
				} 

				var handle = jQuery( "#sy_value_handle_'.$this->id.'" );
				jQuery("#sy_value_handle_'.$this->id.'").tooltipster({
					content:'.$theValue.',
					theme: ttTheme,
					plugins: ["follower"],
					contentCloning: false,
					trigger:"custom",
					triggerOpen: {
				        mouseenter: true,
				        tap: true,
				    },
				    triggerClose: {
				    	mouseleave: true,
				        scroll: true,
				        tap: true,
				    }
					});
				jQuery("#syslider_'.$this->id.'").slider({
					value:'.$theValue.',min:'.$mini.',max:'.$maxi.',step:'.$step.',animate: 300,
					create: function() {
				        jQuery(".sy-tooltip-result-'.$this->id.'").text(jQuery( this ).slider( "value" ) );
				        var widthBg = ('.$theValue.' / '.$maxi.') * 100;
						jQuery(".sy-bg-slider_'.$this->id.'").css("width", widthBg + "%");
						
			      	},
					slide: function( event, ui ) {
						jQuery( "#'.$this->id.'" ).val(ui.value );
						
						jQuery(".sy-tooltip-result-'.$this->id.'").text( jQuery( this ).slider( "value" ) );
						var widthBg = (ui.value / '.$maxi.') * 100;
						jQuery(".sy-bg-slider_'.$this->id.'").css("width", widthBg + "%");
						jQuery("#sy_value_handle_'.$this->id.'").tooltipster("content", ui.value);
					}
			      
			      
				})
				
			})
    	');

    	$doc->addStyleDeclaration('
    		.sy-bg-slider_'.$this->id.' {
    			background-color:'.$this->getAttribute('color1').';
    		}
    		.syslider_'.$this->id.' .ui-widget-content{
    			background-color:'.$this->getAttribute('color2').' !important;
    		}
    		.syslider_'.$this->id.' .ui-state-active{
    			background-color:'.$this->getAttribute('color3').' !important;
    		}
    		', 'text/css');
		
		// Test slider 1
		$html = '<input name="'.$this->name.'" id="'.$this->id.'" class="sysl_config" value="'.$theValue.'" type="hidden"><div class="syslider_'.$this->id.' syslider"><div id="syslider_'.$this->id.'" class="syslider-content"><div class="sy-bg-slider_'.$this->id.'"></div><div id="sy_value_handle_'.$this->id.'" class="ui-slider-handle" data-tooltip-content="sy-tooltip-result-'.$this->id.'"></div></div><div class="sy-min">'.$mini.'</div><div class="sy-max">'.$maxi.'</div></div><div class="tooltip-container tooltip-container-'.$this->id.'"><div class="sy-tooltip-result-'.$this->id.'" ></div></div>';
		return $html;
	}

}

