<?php
/**
 * @package         Slider field plugin
 * @link            http://www.stereonomy.com
 * @copyright       Copyright © 2017 Anthony Ceccarelli - Stereonomy - All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldParamfield extends JFormField
{
	protected $type = 'paramfield';
	
	public function getInput() {

		$doc = JFactory::getDocument();

		$doc->addScriptDeclaration('
			jQuery(document).ready(function(){
				jQuery(".syhidden").parents(".control-group").css("display", "none");
				jQuery("#general").find(".sy-mini").parents(".control-group").css("display", "none");
				jQuery("#general").find(".sy-base").parents(".control-group").css("display", "none");
				jQuery("#general").find(".sy-maxi").parents(".control-group").css("display", "none");
				jQuery("#general").find(".sy-step").parents(".control-group").css("display", "none");
				jQuery("#general").find(".sy-width").parents(".control-group").css("display", "none");
				jQuery("#general").find(".sy-height").parents(".control-group").css("display", "none");
				jQuery("#general").find(".sy-sytooltipstyle").parents(".control-group").css("display", "none");
				jQuery("#general").find(".sy-color").each(function(){
					jQuery(this).parents(".control-group").css("display", "none");
				})
				
			});
			');

		$doc->addStyleDeclaration('
				
			');

		$html = '<input name="'.$this->name.'" id="'.$this->id.'" class="syhidden" value="" type="hidden">';
		return $html;
	}
}