<?php
/**
 * @package         Slider field plugin
 * @link            http://www.stereonomy.com
 * @copyright       Copyright © 2017 Anthony Ceccarelli - Stereonomy - All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 */

defined('_JEXEC') or die;

JLoader::import('components.com_fields.libraries.fieldsplugin', JPATH_ADMINISTRATOR);

/**
 * Fields Period Plugin
 *
 * @since  3.7.0
 */
class PlgFieldsSyslider extends FieldsPlugin
{
	public function onCustomFieldsPrepareDom($field, DOMElement $parent, JForm $form)
	{
		$fieldNode = parent::onCustomFieldsPrepareDom($field, $parent, $form);

		if (!$fieldNode)
		{
			return $fieldNode;
		}

		$form->addFieldPath(JPATH_PLUGINS . '/fields/syslider/includes');

		$fieldNode->setAttribute('type', 'syslider');

		return $fieldNode;
	}


}
