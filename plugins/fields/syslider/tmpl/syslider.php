<?php
/**
 * @package         Slider field plugin
 * @link            http://www.stereonomy.com
 * @copyright       Copyright © 2017 Anthony Ceccarelli - Stereonomy - All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 */

defined('_JEXEC') or die;

$color1 = $field->fieldparams->toObject()->color1;
$color2 = $field->fieldparams->toObject()->color2;
$color3 = $field->fieldparams->toObject()->color3;
$maxi = $field->fieldparams->toObject()->symaximum;
$height = $field->fieldparams->toObject()->syheight;
$front_layout = $field->fieldparams->toObject()->front_result_layout;
$widthslide = explode(',', $field->fieldparams->toObject()->sywidth);

$value = $field->value;
$label = $field->label;
//$group = $field->group;
$name = $field->name;
//dump($field, "field");
$width = $value/$maxi*100;

$doc = JFactory::getDocument();
$doc->addScript(JUri::root() . 'plugins/fields/syslider/assets/js/tooltipster.bundle.min.js');
$doc->addScript(JUri::root() . 'plugins/fields/syslider/assets/js/tooltipster-follower.min.js');
$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/syslider.css');
$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/tooltipster.bundle.min.css');
$doc->addStyleSheet(JUri::root() . 'plugins/fields/syslider/assets/css/tooltipster-follower.min.css');

if ($front_layout < 2){
$doc->addScriptDeclaration('
	jQuery(document).ready(function(){
	    jQuery(".sysf_value_' . $value . '").prop("Counter",0).animate({
	        Counter: ' . $value . '
	    }, {
	        duration: 1000,
	        easing: "swing",
	        step: function (now) {
	            jQuery(".sysf_value_' . $value . '").text(Math.ceil(now));
	            //console.log(Math.ceil(now));
	            if (Math.ceil(now) == ' . $value . '){
	            	return false;
	            }
	        }
	    });	
	})
	');

 
	$sysf_value_style = '.sysf_value_' . $value . '{';
	$sysf_value_style .=	'display:inline-block;';
	$sysf_value_style .=	'font-size: '. $height*0.75 . 'px;';
	$sysf_value_style .=	'line-height: '. $height . 'px;';
	$sysf_value_style .=	'margin-left: 3px;';	
	$sysf_value_style .= '}';
}
else {
	$sysf_value_style = '';
}


$doc->addStyleDeclaration('
	.sysf_slide_container{
		background-color:'.$color2.';
		height:' . $height . 'px;
		margin-top:10px;
		display:flex;
		flex-wrap: wrap;
	}
	.blog .items-leading .sysf_slide_container{
		width:' . $widthslide[1] . ';
	}
	.blog .items-row .sysf_slide_container{
		width:' . $widthslide[2] . ';
	}
	.item-page .sysf_slide_container{
		width:' . $widthslide[1] . ';
	}

	@keyframes slider_result_' . $value . ' {
	    from {width: 0%;}
	    to {width: ' . $width . '%;}
	}
	.sysf_slide_result_' . $value . '{
		background-color:' . $color1 . ';
		animation-name: slider_result_' . $value . ';
    	animation-duration: 1s;
    	height:' . $height . 'px;
    	width:'. $width .'%;
    	display: inline-block;
	}
	'. $sysf_value_style .'
	.sysf_before {
		color:white;
		float:right;
		margin-right:5px;
	}
	', "text/css");

if ($value == '')
{
	return;
}

if (is_array($value))
{
	
	$value = implode(', ', $value);

}

?>
<div class="sysf_slide_container">
	<?php if($front_layout == 0): ?>
		<div class="sysf_slide_result_<?php echo $value ?>"></div><div class="sysf_value_<?php echo $value ?>"></div>
	<?php endif ?>
	<?php if($front_layout == 1): ?>
		<div class="sysf_slide_result_<?php echo $value ?>"><div class="sysf_value_<?php echo $value ?> sysf_before"></div></div>
	<?php endif ?>
	<?php if($front_layout == 2): ?>
		<div class="sysf_slide_result_<?php echo $value ?>" data-tooltip-content=".sysf_value_<?php echo $value ?>"></div>
		<div class="syhidden"><div class="sysf_value_<?php echo $value ?>"><?php echo $label . ' ' . $value ?></div></div>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			var tooltipstyle = <?php echo $field->fieldparams->toObject()->sytooltipstyle ?>;
			switch(tooltipstyle) {
			    case 0:
			        var ttTheme = "default";
			        break;
			    case 1:
			        var ttTheme = "tooltipster-light";
			        break;
			    case 2:
			        var ttTheme = "tooltipster-borderless";
			        break;
			    case 3:
			        var ttTheme = "tooltipster-punk";
			        break;
			    case 4:
			        var ttTheme = "tooltipster-noir";
			        break;
			    case 5:
			        var ttTheme = "tooltipster-shadow";
			        break;
			    default:
			        var ttTheme = "default";
			} 

			jQuery('.sysf_slide_result_<?php echo $value ?>').tooltipster({
				theme: ttTheme,
				plugins: ["follower"],
				contentCloning: false,
				trigger:"custom",
				triggerOpen: {
			        mouseenter: true,
			        tap: true,
			    },
			    triggerClose: {
			    	mouseleave: true,
			        scroll: true,
			        tap: true,
			    }
			})
		})
		</script>
	<?php endif ?>
	<?php if($front_layout == 3): ?>
		<div class="sysf_slide_result_<?php echo $value ?>"></div>
	<?php endif ?>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('.sysf_slide_container').parents('dd').css('margin-left', '0');
})
</script>
<?php
return $value;
