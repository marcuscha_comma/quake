/**
 * @package         Advanced Custom Fields
 * @version         0.4.0 Free
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

function acf_callback_bingmaps() {
    // get all bingmaps
    var acf_bingmaps = document.querySelectorAll(".acf_bingmap");

    // iterate through them
    for (var i = 0; i < acf_bingmaps.length; i++) {

        // get the map
        map = acf_bingmaps[i];

        // grab latitude, longitude, zoom and ID
        var map_latitude  = parseFloat(map.getAttribute("data-latitude"));
        var map_longitude = parseFloat(map.getAttribute("data-longitude"));
        var map_zoom      = parseInt(map.getAttribute("data-zoom"));
        var map_id        = map.getAttribute("id");

        // create the map center, also needed for the pin below
        var map_center = new Microsoft.Maps.Location(map_latitude, map_longitude);

        // create the bing map
        map = new Microsoft.Maps.Map("#" + map_id, {
            center: map_center,
            mapTypeId: Microsoft.Maps.MapTypeId.aerial,
            zoom: map_zoom
        });

        //Create custom Pushpin
        var pin = new Microsoft.Maps.Pushpin(map_center, {
            color: 'red'
        });

        //Add the pushpin to the map
        map.entities.push(pin);
    }
}