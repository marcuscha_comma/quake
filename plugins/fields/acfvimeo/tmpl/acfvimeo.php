<?php

/**
 * @package         Advanced Custom Fields
 * @version         0.4.0 Free
 *
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 */

defined('_JEXEC') or die;

if (!$videoID = $field->value)
{
	return;
}

if (preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $field->value, $match)) 
{
    $videoID = $match[5];
}

// Setup Variables
$width    = $fieldParams->get('width', '640');
$height   = $fieldParams->get('height', '360');
$query    = $videoID;




// Output
$buffer = '
	<iframe
		src="//player.vimeo.com/video/' . $query . '"
		frameborder="0"
		width="' . $width . '"
		height="' . $height . '"
		webkitallowfullscreen
		mozallowfullscreen
		allowfullscreen>
	</iframe>
';

echo $buffer;