<?php

/**
 * @package         Advanced Custom Fields
 * @version         0.4.0 Free
 *
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 */

defined('_JEXEC') or die;

if (!$videoID = $field->value)
{
	return;
}

if (preg_match('!^.+dailymotion\.com/(video|hub)/([^_]+)[^#]*(#video=([^_&]+))?|(dai\.ly/([^_]+))!', $field->value, $match)) 
{
        if (isset($match[6])) 
        {
            $videoID = $match[6];
            unset($match);
        }

        if (isset($match[4])) 
        {
            $videoID = $match[4];
            unset($match);
        }

        $videoID = $match[2];
}

// Setup Variables
$width             = $fieldParams->get('width', '480');
$height            = $fieldParams->get('height', '270');
$query             = $videoID;



// Output
$buffer = '
	<iframe
		src="//www.dailymotion.com/embed/video/' . $query . '"
		width="' . $width . '"
		height="' . $height . '"
		frameborder="0"
		allowfullscreen>
	</iframe>
';

echo $buffer;