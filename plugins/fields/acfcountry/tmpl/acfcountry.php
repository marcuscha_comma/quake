<?php

/**
 * @package         Advanced Custom Fields
 * @version         0.4.0 Free
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

defined('_JEXEC') or die;

if (!$value = $field->value)
{
	return;
}

$display = $fieldParams->get('countrydisplay', 'name');

if ($display == 'name')
{
	require_once JPATH_PLUGINS . '/system/nrframework/fields/geo.php';

	$geo = new JFormFieldNR_Geo();
	$countries = $geo->countries;
	$value = $countries[$value];
}

echo $value;





