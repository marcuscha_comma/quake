<?php

/**
 * @package         Advanced Custom Fields
 * @version         0.4.0 Free
 *
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 */

defined('_JEXEC') or die;

if (!$trackID = $field->value)
{
	return;
}

// Setup Variables
$width        = $fieldParams->get('width', '100%');
$height       = $fieldParams->get('height', '166');
$query        = $trackID;



// Output
$buffer = '
	<iframe
		src="https://w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/' . $query . '"
		width="' . $width . '"
		height="' . $height . '"
		scrolling="no"
		frameborder="0">
	</iframe>
';

echo $buffer;