<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2018 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2018/9/19 15:58
 */

define('_JEXEC', 1);
define('JPATH_BASE', dirname(dirname(dirname(__DIR__))));

//导入
require_once JPATH_BASE . '/includes/defines.php';
require_once JPATH_BASE . '/includes/framework.php';

//初始化Joomla的相关信息
$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

//引入session
$session = JFactory::getSession();
$state = $session->getState();
if ($state !== 'active') {
	return;
}

error_reporting(0);
require_once dirname(__FILE__) . '/libs/TnCode.class.php';
$tn = new TnCode();
$tn->make();
