<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2018 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2018/9/19 15:58
 */

defined('_JEXEC') or die;

/**
 * Recaptcha Plugin.
 *
 * @since  2.5
 */
class PlgCaptchaSixecaptcha extends JPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  3.1
	 */
	protected $autoloadLanguage = true;

	/**
	 * Gets the challenge HTML
	 *
	 * @param   string $name The name of the field. Not Used.
	 * @param   string $id The id of the field.
	 * @param   string $class The class of the field. This should be passed as
	 *                          e.g. 'class="required"'.
	 *
	 * @return  string  The HTML to be embedded in the form.
	 *
	 * @since  2.5
	 */
	public function onDisplay($name = null, $id = 'dynamic_sixecaptcha_1', $class = '')
	{
		// doc
		$doc = JFactory::getDocument();
		$plg_root = JURI::root(true) . '/plugins/captcha/sixecaptcha';

		// script to show img
		JHtml::_('jquery.framework');

		// main css
		$mainclass = $this->params->get('mainclass');

		// ext style
		$extstyle = $this->params->get('extstyle');
		if ($extstyle) {
			$doc->addStyleDeclaration($extstyle);
		}

		// ext script
		$extscript = $this->params->get('extscript');
		if ($extscript) {
			$doc->addScriptDeclaration($extscript);
		}

		// html
		$html = '';

		switch ($this->params->get('type', 'image')) {
			case 'puzzle':
				JText::script('PLG_CAPTCHA_SIXECAPTCHA_PUZZLE_DRAGDROP');
				JText::script('PLG_CAPTCHA_SIXECAPTCHA_MSG_SUCCESS');
				JText::script('PLG_CAPTCHA_SIXECAPTCHA_MSG_FAILED');
				JText::script('PLG_CAPTCHA_SIXECAPTCHA_COPYLINK');
				JText::script('PLG_CAPTCHA_SIXECAPTCHA_LOADING');

				$js = $plg_root . '/assets/js/tncode.js';
				$doc->addScript($js);

				$css = $plg_root . '/assets/css/tncode.css';
				$doc->addStyleSheet($css);

				$script = "jQuery(function(){tncode.init();tncode.onsuccess(function(r){jQuery('.sixecaptcha-wraper input[name=\"" . $name . "\"]').val(r)});})";
				$doc->addScriptDeclaration($script);

				$html .= '<div class="sixecaptcha-wraper ' . $mainclass . '"><input type="text" name="' . $name . '" id="' . $id . '" autocomplete="off" class="' . $class . ' tncode" placeholder="' . JText::_('PLG_CAPTCHA_SIXECAPTCHA_PLACEHOLDER') . '" /></div>';
				break;

			case 'image':
				$url = $plg_root . '/picture.php'; //img url
				$img = '<img src="' . $url . '?t=' . time() . '"  title="Click to refresh" style="cursor:pointer" onclick="this.src=\\\'' . $url . '?r=\\\'+Math.random()" />';
				$script = "jQuery(function(){jQuery('.sixecaptcha-wraper input[name=\"" . $name . "\"]').focus(function(){jQuery(this).next('.sixecaptcha-image').html('" . $img . "');});});";
				$doc->addScriptDeclaration($script);

				$html .= '<div class="sixecaptcha-wraper ' . $mainclass . ' ' . $class . '"><input type="text" name="' . $name . '" id="' . $id . '" autocomplete="off" class="' . $class . '" placeholder="' . JText::_('PLG_CAPTCHA_SIXECAPTCHA_PLACEHOLDER') . '" /><span class="sixecaptcha-image"></span></div>';
				break;

			default:
				break;
		}

		// return html
		return $html;
	}

	/**
	 * Calls an HTTP POST function to verify if the user's guess was correct
	 *
	 * @param   string $code Answer provided by user. Not needed for the Recaptcha implementation
	 *
	 * @return  True if the answer is correct, false otherwise
	 *
	 * @since  2.5
	 */
	public function onCheckAnswer($code = null)
	{
		$session = JFactory::getSession();

		switch ($this->params->get('type', 'image')) {
			case 'puzzle':
				$sess_puzzle_code = isset($_SESSION['sixecaptcha_puzzle_code']) ? $_SESSION['sixecaptcha_puzzle_code'] : '';

				if (!$code || strtolower($code) != strtolower($sess_puzzle_code)) {
					$this->_subject->setError(JText::_('PLG_CAPTCHA_SIXECAPTCHA_ERROR'));
					return false;
				}

				// destory session
				unset($_SESSION['sixecaptcha_puzzle_code']);
				break;

			case 'image':
				//get code from session
				$sess_image_code = $session->get('sixecaptcha_image_code');

				if (!$code || strtolower($code) != strtolower($sess_image_code)) {
					$this->_subject->setError(JText::_('PLG_CAPTCHA_SIXECAPTCHA_ERROR'));
					return false;
				}

				// destory session
				$session->set('sixecaptcha_image_code', '');
				break;

			default:
				break;
		}

		return true;
	}

}
