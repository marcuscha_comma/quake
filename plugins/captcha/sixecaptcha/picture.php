<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2018 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2018/9/19 15:58
 */

define('_JEXEC', 1);
define('JPATH_BASE', dirname(dirname(dirname(__DIR__))));

//导入
require_once JPATH_BASE . '/includes/defines.php';
require_once JPATH_BASE . '/includes/framework.php';

//引入外部验证码类
JLoader::register('SixeValidateCode', __DIR__ . '/libs/SixeValidateCode.php');

//初始化Joomla的相关信息
$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

//引入session
$session = JFactory::getSession();
$state = $session->getState();
if ($state !== 'active') {
	return;
}

//验证码配置
$config = array();

//获取插件参数设置
$plugin = JPluginHelper::getPlugin('captcha', 'sixecaptcha');
if ($plugin->params) {
	$config = json_decode($plugin->params, true);

	//风格
	if (isset($config['theme'])) {
		switch ($config['theme']) {
			case 'clean':
				$config["bgcolor"] = "#ffffff";
				$config["fontcolor"] = "#000000";
				$config["linenum"] = "6";
				$config["linecolor"] = "#000000";
				$config["snownum"] = "100";
				$config["snowcolor"] = "#ffffff";
				break;
			case 'random':
				$config["bgcolor"] = "";
				$config["fontcolor"] = "";
				$config["linenum"] = "";
				$config["linecolor"] = "";
				$config["snownum"] = "";
				$config["snowcolor"] = "";
				break;
			case 'custom':
				break;
			default:
				break;
		}
	}
}

//new一个实例
$_vc = new SixeValidateCode($config);

//创建验证码
$_vc->createImage();

//记忆验证码值
$session->set('sixecaptcha_image_code', $_vc->getCode());

//输出图片流
$_vc->outPutImage();
