<?php
/**
 * 图片验证码类
 * Created by PhpStorm.
 * User: adan
 * Date: 2018/9/17
 * Time: 15:52
 */

defined('_JEXEC') or die;

class SixeValidateCode
{

	private $img; //图形资源句柄
	private $charset = 'abcdefghkmnprstuvwxyzABCDEFGHKMNPRSTUVWXYZ23456789'; //字符集
	private $code; //验证码值
	private $codelen = 4; //字符长度
	private $width = 130; //图片宽度
	private $height = 50; //图片高度
	private $font; //指定的字体
	private $fontsize = 20; //字体大小
	private $fontcolor = '#000000'; //字体颜色
	private $bgcolor; //背景颜色
	private $linenum = 6; //干扰线数量
	private $linecolor; //干扰线颜色
	private $snownum = 50; //雪花数量
	private $snowcolor; //雪花颜色

	//构造方法初始化
	public function __construct($config = array())
	{
		$this->font = dirname(__FILE__) . '/font/elephant.ttf';

		if (isset($config["charset"]) && $config["charset"]) {
			$this->charset = $config["charset"];
		}
		if (isset($config["codelen"]) && $config["codelen"]) {
			$this->codelen = $config["codelen"];
		}
		if (isset($config["width"]) && $config["width"]) {
			$this->width = $config["width"];
		}
		if (isset($config["height"]) && $config["height"]) {
			$this->height = $config["height"];
		}
		if (isset($config["fontsize"]) && $config["fontsize"]) {
			$this->fontsize = $config["fontsize"];
		}
		if (isset($config["fontcolor"]) && $config["fontcolor"]) {
			$this->fontcolor = $config["fontcolor"];
		}
		if (isset($config["bgcolor"]) && $config["bgcolor"]) {
			$this->bgcolor = $config["bgcolor"];
		}
		if (isset($config["linenum"]) && $config["linenum"]) {
			$this->linecolor = $config["linenum"];
		}
		if (isset($config["linecolor"]) && $config["linecolor"]) {
			$this->linecolor = $config["linecolor"];
		}
		if (isset($config["snownum"]) && $config["snownum"]) {
			$this->snowcolor = $config["snownum"];
		}
		if (isset($config["snowcolor"]) && $config["snowcolor"]) {
			$this->snowcolor = $config["snowcolor"];
		}
	}

	//生成随机码
	private function createCode()
	{
		$_len = strlen($this->charset) - 1;
		for ($i = 0; $i < $this->codelen; $i++) {
			$this->code .= $this->charset[mt_rand(0, $_len)];
		}
	}

	//生成背景
	private function createBg()
	{
		if ($this->bgcolor) {
			$rgb = $this->hex2RGB($this->bgcolor);
		}
		$this->img = imagecreatetruecolor($this->width, $this->height);
		if ($this->bgcolor) {
			$color = imagecolorallocate($this->img, $rgb["r"], $rgb["g"], $rgb["b"]);
		} else {
			$color = imagecolorallocate($this->img, mt_rand(157, 255), mt_rand(157, 255), mt_rand(157, 255));
		}
		imagefilledrectangle($this->img, 0, $this->height, $this->width, 0, $color);
	}

	//生成文字
	private function createFont()
	{
		if ($this->fontcolor) {
			$rgb = $this->hex2RGB($this->fontcolor);
		}
		$_x = $this->width / $this->codelen;
		for ($i = 0; $i < $this->codelen; $i++) {
			if ($this->fontcolor) {
				$color = imagecolorallocate($this->img, $rgb["r"], $rgb["g"], $rgb["b"]);
			} else {
				$color = imagecolorallocate($this->img, mt_rand(0, 156), mt_rand(0, 156), mt_rand(0, 156));
			}
			imagettftext($this->img, $this->fontsize, mt_rand(-30, 30), $_x * $i + mt_rand(1, 5), $this->height / 1.4, $color, $this->font, $this->code[$i]);
		}
	}

	//生成线条
	private function createLine()
	{
		if ($this->linecolor) {
			$rgb = $this->hex2RGB($this->linecolor);
		}
		for ($i = 0; $i < $this->linenum; $i++) {
			if ($this->linecolor) {
				$color = imagecolorallocate($this->img, $rgb["r"], $rgb["g"], $rgb["b"]);
			} else {
				$color = imagecolorallocate($this->img, mt_rand(0, 156), mt_rand(0, 156), mt_rand(0, 156));
			}
			imageline($this->img, mt_rand(0, $this->width), mt_rand(0, $this->height), mt_rand(0, $this->width), mt_rand(0, $this->height), $color);
		}
	}

	//生成雪花
	private function createSnow()
	{
		if ($this->snowcolor) {
			$rgb = $this->hex2RGB($this->snowcolor);
		}
		for ($i = 0; $i < $this->snownum; $i++) {
			if ($this->snowcolor) {
				$color = imagecolorallocate($this->img, $rgb["r"], $rgb["g"], $rgb["b"]);
			} else {
				$color = imagecolorallocate($this->img, mt_rand(200, 255), mt_rand(200, 255), mt_rand(200, 255));
			}
			imagestring($this->img, mt_rand(1, 5), mt_rand(0, $this->width), mt_rand(0, $this->height), '*', $color);
		}
	}

	//16进制颜色转RGB
	private function hex2RGB($hexColor)
	{
		$color = str_replace('#', '', trim($hexColor));
		if (strlen($color) > 3) {
			$rgb = array('r' => hexdec(substr($color, 0, 2)), 'g' => hexdec(substr($color, 2, 2)), 'b' => hexdec(substr($color, 4, 2)));
		} else {
			$color = $hexColor;
			$r = substr($color, 0, 1) . substr($color, 0, 1);
			$g = substr($color, 1, 1) . substr($color, 1, 1);
			$b = substr($color, 2, 1) . substr($color, 2, 1);
			$rgb = array('r' => hexdec($r), 'g' => hexdec($g), 'b' => hexdec($b));
		}
		return $rgb;
	}

	//生成
	public function createImage()
	{
		$this->createBg();
		$this->createCode();
		$this->createLine();
		$this->createSnow();
		$this->createFont();
	}

	//输出图片
	public function outPutImage()
	{
		header('Content-type:image/png');
		imagepng($this->img);
		imagedestroy($this->img);
		die;
	}

	//获取验证码
	public function getCode()
	{
		return strtolower($this->code);
	}
}