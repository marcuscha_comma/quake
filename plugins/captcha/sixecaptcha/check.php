<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2018 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2018/9/19 15:58
 */

define('_JEXEC', 1);
define('JPATH_BASE', dirname(dirname(dirname(__DIR__))));

//导入
require_once JPATH_BASE . '/includes/defines.php';
require_once JPATH_BASE . '/includes/framework.php';

//初始化Joomla的相关信息
$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

//引入session
$session = JFactory::getSession();
$state = $session->getState();
if ($state !== 'active') {
	return;
}

require_once dirname(__FILE__) . '/libs/TnCode.class.php';
$tn = new TnCode();

// response
$response = array(
	'status' => 'error',
	'code' => ''
);

if ($tn->check()) {
	//$_SESSION['tncode_check'] = 'ok';
	//echo "ok";

	if (isset($_SESSION['sixecaptcha_puzzle_code']) && $_SESSION['sixecaptcha_puzzle_code']) {
		$response['status'] = 'ok';
		$response['code'] = $_SESSION['sixecaptcha_puzzle_code'];
		echo json_encode($response);
		exit(0);
	}
} else {
	//$_SESSION['tncode_check'] = 'error';
	//echo 'error';
}

echo json_encode($response);
exit(0);