<?php

/**
 * @package         Advanced Custom Fields
 * @version         0.4.0 Free
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

defined('JPATH_PLATFORM') or die;
JHtml::_('behavior.modal');

$modalRel = 'rel="{handler: \'iframe\', size: {x: 1000, y: 630}}"';

?>

<div class="acft">
	<table class="table table-striped">
		<thead>
			<tr>
				<th><?php echo JText::_('ACF_FIELDS_COLLECTION') ?></th>
				<th width="140px"></th>
			</tr>
		</thead>
	    <?php foreach ($displayData as $key => $field) { ?>
			<tr>
	            <td class="acft-text">
	                <h4><?php echo $field["label"] ?></h4>
	                <?php echo $field["description"] ?>
	            </td>
	            <td class="acft-btn">
                    <?php 
                    	if ($field["comingsoon"])
                    	{
                    		echo "Coming Soon";
                    		continue;
                    	}
                    ?>
		               
                	<?php if ($field["extensionid"]) { ?>
                        <a class="btn btn-success modal" <?php echo $modalRel; ?> href="<?php echo $field["backendurl"] ?>">
                            <span class="icon-checkmark"></span>
                            <?php echo JText::_('NR_INSTALLED'); ?>
                        </a>
                    <?php continue; } ?>

					<?php if ($field["proonly"]) { ?>
		                <a href="<?php echo $field["siteurl"] ?>" class="btn btn-danger" target="_blank">
		                    <span class="icon icon-lock"></span>
		                    <?php echo JText::_('NR_UPGRADE_TO_PRO'); ?>
		                </a>
					<?php continue; } ?>

                    <a class="btn" target="_blank" href="<?php echo $field["siteurl"] ?>">
                        <?php echo JText::_("NR_INSTALL_NOW") ?>
                    </a>
	            </td>
	        </tr>
	    <?php } ?>
		<tr>
        	<td>
                <div><strong><?php echo JText::_("ACF_MISSING_FIELD") ?></strong></div>
                <?php echo JText::_("ACF_MISSING_FIELD_DESC") ?>
        	</td>
			<td class="acft-btn">
				<a class="btn btn-primary" target="_blank" href="https://www.tassos.gr/contact?s=Custom Field Request">
					<?php echo JText::_("NR_DROP_EMAIL")?>
				</a>
			</td>
        </tr>
	</table>
</div>

<style>
	.acft-btn {
	    text-align: right !important;
	}
	.acft-btn .btn {
	    width:100%;
	    box-sizing: border-box;
	    white-space: nowrap;
	}
	.acft td, .acft th {
		padding:13px 0 13px 0;
		vertical-align: middle;
	}
	.acft h4 {
		margin:0 0 5px 0;
		padding:0;
		color:#3071a9;
	}	
</style>