<?php

/**
 * @package         Advanced Custom Fields
 * @version         0.4.0 Free
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

defined('_JEXEC') or die('Restricted access');

use NRFramework\Cache;

/**
 *  Advanced Custom Fields Helper
 */
class ACFHelper
{
    /**
     *  Check field publishing assignments.
     *  The field will not show up on front-end if it doesn't pass the checks.
     *  
     *  Note: Field is passed by reference.
     *
     *  @param   object  $field  The field object
     *
     *  @return  void
     */
	public static function checkAssignments($field)
	{
        // Skip fields with an empty value. 
        // The Fields component removes them from the front-end by default.
        if (empty($field->value))
        {
            return;
        }

        // For a reason the plugin events are fired multiple times.
        // Let's fix this by caching the result.
        $hash  = md5("acf_" . $field->id);
        $cache = Cache::read($hash);

        if ($cache)
        {
            // Cool, the result is already cached!
            return $cache;
        }

        // Load publishing assignments class
        $assignments = new NRFramework\Assignments();

        // Pass Check
        $pass = $assignments->passAll($field, $field->params->get("assignmentMatchingMethod", "and"));        

        if (!$pass)
        {
            // Unset field
            unset($field->value);
            $field->params->set('display', '666'); // We may not need this line.
        }

        Cache::set($hash, $pass);
	}

    public static function getFileSources($sources, $allowedExtensions = null)
    {
        if (!$sources)
        {
            return;
        }

        // Support comma separated values
        $sources = is_array($sources) ? $sources : explode(',', $sources);
        $result  = array();

        foreach ($sources as $source)
        {
            if (!$pathinfo = pathinfo($source))
            {
                continue;
            }

            if ($allowedExtensions && !in_array($pathinfo['extension'], $allowedExtensions))
            {
                continue;
            }

            // Add root path to local source
            if (strpos($source, 'http') === false)
            {
                $source = JURI::root() . ltrim($source, '/');
            }

            $result[] = array(
                'ext'  => $pathinfo['extension'],
                'file' => $source
            );
        }

        return $result;
    }
}