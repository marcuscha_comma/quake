// Add Back to Top button when scroll down
jQuery(window).scroll(function() {
  if (jQuery(document).scrollTop() >100) {
    jQuery('#backTop').addClass('show');
  } else {
    jQuery('#backTop').removeClass('show');
  };

  // Shrink menu when scroll
  if (jQuery(document).scrollTop() >100) {
    jQuery('header').addClass('shrink');

    if ( jQuery( ".scrollable-tab" ).length ){
      jQuery('.scrollable-tab').addClass('shrink');
    };
  } else {
    jQuery('header').removeClass('shrink');

    if ( jQuery( ".scrollable-tab" ).length ){
      jQuery('.scrollable-tab').removeClass('shrink');
    };
  }
});

jQuery(document).ready(function () {
  jQuery("body").removeClass("preload");

  jQuery(window).trigger('resize');

  jQuery(".birthday-reminder .toggle").on("click", function () {
    jQuery(".birthday-reminder .message").toggleClass('show');
  });

  jQuery(".birthday-reminder .message").on("click", function () {
    jQuery(this).toggleClass('show');
  });

  if (window.location.search == '?faq-8') {
    jQuery('#faq-8').collapse('show');
  }

  if (window.location.hash) {
    var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
    jQuery("#" + hash).collapse('show');
    setTimeout(function () {
      jQuery('html, body').animate({
        scrollTop: (jQuery("#" + hash).offset().top - jQuery('header').height() - 80)
      }, 100);
    }, 200);
  };

  jQuery(".selectpicker").removeAttr("data-mobile");

  // Channel Profile tooltip

  if (jQuery(window).width() < 992) {
    jQuery( ".channel-item img" ).click(function() {
      jQuery(this).next( ".channel-tooltip" ).toggleClass( "show" );
      jQuery( "body" ).append( "<div class='overlay'></div>" );
      jQuery( "body" ).css("overflow","hidden");
    });

    // Remove bootstrap select in Mobile
    if ( jQuery( ".selectpicker" ).length ){
      jQuery(".selectpicker").attr("data-mobile","true");
      jQuery(".selectpicker").addClass("mobile-device");
    };
  };

  jQuery(".close").click(function () {
    jQuery(this).parent().removeClass("show");
    jQuery('.overlay').fadeOut();
    setTimeout(function () {
      jQuery(".overlay").remove();
    }, 400);
    jQuery("body").removeAttr("style");
  });

  // Mobile Filter
  jQuery( "#filter-trigger" ).click(function() {
    jQuery('.mobile-filter-container').addClass('show');
    jQuery( "body" ).append( "<div class='overlay'></div>" );
    jQuery( "body" ).css("overflow","hidden");
  });
  jQuery( "#filter-close" ).click(function() {
    jQuery('.mobile-filter-container').removeClass('show');
    jQuery('.overlay').fadeOut();
    setTimeout(function(){
      jQuery( ".overlay" ).remove();
    }, 400);
    jQuery( "body" ).removeAttr( "style" );
  });

  // Animate Scroll to Top
  jQuery("#backTop").click(function() {
    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
  });

  /// Smooth Scroll
  jQuery('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .not('[role="tab"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = jQuery(this.hash);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        jQuery('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var jQuerytarget = jQuery(target);
          jQuerytarget.focus();
          if (jQuerytarget.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            jQuerytarget.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            jQuerytarget.focus(); // Set focus again
          };
        });
      }
    }
  });

  // Bootstrap Select
  jQuery('.selectpicker').selectpicker();

  /// js matchHeight
  setTimeout(function(){
      jQuery('.same-height').matchHeight();
      jQuery('.tab-pill').matchHeight();
      jQuery('.product-info').matchHeight({
        target: jQuery('.product-slider .item'),
        property: 'min-height'
      });

  }, 100);

});

jQuery(window).resize(function () {

  if (jQuery(window).width() < 992) {
    /// Scrollable tab for mobile
    if ( jQuery( ".scrollable-tab" ).length ){
      jQuery(".scrollable-tab").insertAfter(".page-banner-wrapper");
      jQuery(".page-banner").css("margin-bottom","0");
    };

    if ( jQuery( ".tab-pills.scrollable-tab .row > div" ).length < 3 ){
      jQuery( ".tab-pills.scrollable-tab .row > div" ).css('min-width', '50%');
    };

    /// Move back button to top for mobile
    if ( jQuery( "#back-article" ).length ){
      jQuery("#back-article").insertAfter(".page-banner-wrapper");
      jQuery("#back-article").addClass("stick");
      jQuery(".page-banner").css("margin-bottom","0");
    };

  }
  else {
    if ( jQuery( ".scrollable-tab" ).length ){
      jQuery(".scrollable-tab").prependTo("#app");
      jQuery(".scrollable-tab").prependTo(".sc");
      jQuery(".page-banner").removeAttr( 'style' );
      jQuery(".tab-pills.scrollable-tab .row > div").removeAttr( 'style' );
    };

    if ( jQuery( "#back-article" ).length ){
      jQuery("#back-article").insertBefore(".pagination-side.prev");
      jQuery(".page-banner").removeAttr( 'style' );
      jQuery("#back-article").removeClass( 'stick' );
    };
  };

  /// Scrollable nav - move active tab in middle
  if ( jQuery( ".scrollable-tab" ).length ){
    var myScrollPos = jQuery('.scrollable-tab a.active').offset().left + jQuery('.scrollable-tab a.active').outerWidth(true)/2 + jQuery('.scrollable-tab .row').scrollLeft() - jQuery('.scrollable-tab .row').width()/2;
    jQuery('.scrollable-tab .row').scrollLeft(myScrollPos);
  };

});
