jQuery(document).ready(function () {
    //Carousel js
    jQuery('#index-banner').owlCarousel({
        loop: true,
        items: 1,
        video: true,
        lazyLoad: true,
        autoplay: true,
        autoplayTimeout: 8000,
        responsive: {
            // breakpoint from 0 up
            0: {
                dots: false,
            },
            // breakpoint from 992 up
            768: {
                dots: true,
            }
        }
    })
    // Quakecast Main banner> 
    jQuery('#vg-banner').owlCarousel({
        loop: true,
        items: 1,
        video: true,
        lazyLoad: true,
        autoplay: true,
        autoplayTimeout: 8000,
        responsive: {
            // breakpoint from 0 up
            0: {
                dots: false,
            },
            // breakpoint from 992 up
            768: {
                dots: true,
            }
        }
    })
    jQuery('#club-banner').owlCarousel({
        loop: true,
        items: 1,
        video: true,
        lazyLoad: true,
        autoplay: true,
        autoplayTimeout: 8000,
        responsive: {
            // breakpoint from 0 up
            0: {
                dots: false,
            },
            // breakpoint from 992 up
            992: {
                dots: true,
            }
        }
    })

    jQuery('#highlight-banner').owlCarousel({
        loop: true,
        lazyLoad: true,
        margin: 30,
        autoplay: true,
        autoplayTimeout: 8000,
        autoHeight: true,

        responsive: {
            // breakpoint from 0 up
            0: {
                items: 1,
                dots: false,
            },
            // breakpoint from 992 up
            992: {
                items: 2,
                slideBy: 2,
                dots: true,
            }
        }
    })

    jQuery('#quakecast-video-gallery').owlCarousel({
        loop: false,
        lazyLoad: true,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 8000,
        autoHeight: true,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],

        responsive: {
            // breakpoint from 0 up
            0: {
                items: 2,
                dots: true,
            },
            // breakpoint from 992 up
            992: {
                items: 3,
                slideBy: 3,
                dots: true,
            }
        }
    })

    jQuery('#product-slider').owlCarousel({
        loop: true,
        lazyLoad: true,
        margin: 20,
        autoplay: false,
        navText: ['<i class="fas fa-arrow-left"></i>','<i class="fas fa-arrow-right"></i>'],
        autoHeight: true,

        responsive: {
            // breakpoint from 0 up
            0: {
                dots: true,
                items: 1,
            },
            // breakpoint from 0 up
            521: {
                items: 2,
                slideBy: 2,
                dots: true,
            },
            // breakpoint from 768 up
            768: {
                items: 3,
                slideBy: 3,
                dots: true,
            },
            991: {
                items: 3,
                slideBy: 3,
                nav: true,
                dots: false,
            }
        }
    })

    jQuery('.product-slider .owl-carousel').owlCarousel({
        loop: false,
        items: 1,
    })

    function postsCarousel() {
        var checkWidth = jQuery(window).width();
        var owlPost = jQuery("#q-perks");
        if (checkWidth > 991) {
            if (typeof owlPost.data('owl.carousel') != 'undefined') {
                owlPost.data('owl.carousel').destroy();
            }
            owlPost.removeClass('owl-carousel');
        } else if (checkWidth < 992) {
            owlPost.addClass('owl-carousel');
            owlPost.owlCarousel({
                loop: false,
                lazyLoad: true,
                margin: 20,
                autoplay: true,
                autoplayTimeout: 6000,
                dots: true,
                autoHeight: true,

                responsive: {
                    // breakpoint from 0 up
                    0: {
                    slideBy: 2,
                    items: 2,
                    },
                    480: {
                        items: 3,
                        slideBy: 3,
                    },
                    767: {
                        items: 4,
                        slideBy: 4,
                    }
                }
            });
        }
    }
    
    postsCarousel();
    jQuery(window).resize(postsCarousel);

    
    jQuery('.profile-overview-slider').owlCarousel({
        margin: 30,

        responsive: {
          0: {
            loop: false,
            autoplay: true,
            autoplayTimeout: 8000,
            dots: true,
            autoHeight: true,
            items: 1,
          },
          992: {
            items: 2,
            mouseDrag: false,
          },
        }
    })

    jQuery('#subscribe-form').submit(function (e) {
        e.preventDefault();
        var email_val = jQuery('#subscribe-email').val();
        var name_val = email_val.substring(0, email_val.indexOf("@"));
        jQuery("#loading-icon").prepend('<i id="load-icon" class="fas fa-spinner fa-spin text-muted"></i>');
        jQuery("#load-icon").remove();
        console.log(name_val);


        jQuery.ajax({
            url: checkEmailUrl,
            type: 'post',
            data: { email: email_val },
            success: function (result) {
                if (result == 1) {
                    console.log("aldy exist");
                    jQuery("#loading-icon").prepend('<i id="load-icon" class="fas fa-check text-success"></i>');
                    jQuery('#subscribe-email').val('');
                } else {
                    jQuery.ajax({
                        url: createClientUrl,
                        type: 'post',
                        data: { email: email_val, user_type: 1 },
                        success: function (result) {
                            jQuery("#loading-icon").prepend('<i id="load-icon" class="fas fa-check text-success"></i>');
                        },
                        error: function () {
                            console.log("fail");
                            jQuery("#loading-icon").prepend('<i id="load-icon" class="fas fa-close text-danger"></i>');
                        }
                    });
                    jQuery('#subscribe-email').val('');
                }
            }
        });
    })

});
