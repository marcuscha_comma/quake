<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.protostar
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$msgList = $displayData['msgList'];

$alert = array('error' => 'alert-danger', 'warning' => 'alert-warning', 'notice' => 'alert-info', 'message' => 'alert-success');
?>
<!-- <div id="system-message-container">
	<?php // if (is_array($msgList) && !empty($msgList)) : ?>
		<div id="system-message">
			<?php // foreach ($msgList as $type => $msgs) : ?>
				<div class="alert <?php // echo isset($alert[$type]) ? $alert[$type] : 'alert-' . $type; ?>" role="alert">
					<?php // // This requires JS so we should add it through JS. Progressive enhancement and stuff. ?>
					<a class="close" data-dismiss="alert">×</a>

					<?php // if (!empty($msgs)) : ?>
						<h4 class="alert-heading"><?php // echo JText::_($type); ?></h4>
						<div>
							<?php // foreach ($msgs as $msg) : ?>
								<div class="alert-message"><?php // echo $msg; ?></div>
							<?php // endforeach; ?>
						</div>
					<?php // endif; ?>
				</div>
			<?php // endforeach; ?>
		</div>
	<?php // endif; ?>
</div> -->

<script>
	var msgList = <?php echo json_encode($msgList); ?>;
	console.log(msgList['warning']);
	console.log(msgList['message']);
	console.log(msgList['error']);


	if ( msgList['message'] != undefined ) {
		// swal("Thank you for your message!", msgList['message'][0], "success", {
		swal(msgList['message'][0], "", "success", {
		button: {
			text: "Done",
				className: "btn btn-pink",
		},
		});
	}else if( msgList['warning'] != undefined ){
		swal("Oops!", msgList['warning'][0],"warning", {
		button: {
			text: "Try Again",
				className: "btn btn-pink",
		},
		});
	}else if( msgList['error'] != undefined ){
		swal("Oops!", msgList['error'][0],"error", {
		button: {
			text: "Try Again",
				className: "btn btn-pink",
		},
		});
	}
</script>
