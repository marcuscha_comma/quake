<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
$db    = JFactory::getDBO();
$query = $db->getQuery(true);
// Create shortcuts to some parameters.
$params  = $this->item->params;
// $images  = json_decode($this->item->images);
// $urls    = json_decode($this->item->urls);
$sharerImage = "";
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$emailStatus = JRequest::getVar('email')==""?"":JRequest::getVar('email');

//get user details
if ($userId != 0) {
	$db->setQuery('SELECT designation,name FROM #__cus_qperks_company_user where user_id ="'.$userId .'"');
	$userCompanyDetail             = $db->loadRow();
	$userEmail    = $user->get('email')?$user->get('email'):"";
	$userName     = $user->get('name')?$user->get('name'):"";
	$userContact  = $user->get('mobileNo')?$user->get('mobileNo'):"";
	$userDesignation  = $userCompanyDetail[0]?$userCompanyDetail[0]:"";
	$userCompanyName  = $userCompanyDetail[1]?$userCompanyDetail[1]:"";
}

//get Custome field form this article
$myCustomFields = array();
foreach($this->item->jcfields as $field) {
	switch ($field->name) {
		case 'slider-1-image':
			$myCustomFields[$field->name] = explode("\"",$field->value);
			$sharerImage = explode("\"",$field->value);
		break;
		case 'slider-2-image':
			$myCustomFields[$field->name] = explode("\"",$field->value);
		break;

		case 'slider-3-image':
			$myCustomFields[$field->name] = explode("\"",$field->value);
		break;
		case 'slider-4-image':
			$myCustomFields[$field->name] = explode("\"",$field->value);
		break;
		case 'slider-5-image':
			$myCustomFields[$field->name] = explode("\"",$field->value);
		break;
		default:
			$myCustomFields[$field->name] = $field->value;
			break;
	}
}

$doc = JFactory::getDocument();
$fbimage = JURI::base() . $sharerImage[1];
$doc->addCustomTag( '<meta property="og:image" content="' . $fbimage . '">' );

$awards['Gold'] = [];
$awards['Silver'] = [];
$awards['Bronze'] = [];
$awards['pGold'] = 1;
$awards['pSilver'] = 2;
$awards['pBronze'] = 3;
for ($i=1; $i <= 10; $i++) {
	switch ($myCustomFields['award-'.$i]) {
		case 'Gold':
			$awards['Gold'][] = $myCustomFields['award-'.$i.'-description'];
			break;
		case 'Silver':
			$awards['Silver'][] = $myCustomFields['award-'.$i.'-description'];
			break;
		case 'Bronze':
			$awards['Bronze'][] = $myCustomFields['award-'.$i.'-description'];
			break;
		
		default:
			# code...
			break;
	}
}
if (count($awards['Gold']) == 0 && count($awards['Silver']) == 0) {
				$awards['pBronze'] = 1;	
			}elseif (count($awards['Silver']) == 0){
				$awards['pGold'] = 1;
				$awards['pBronze'] = 2;
			}elseif (count($awards['Gold']) == 0) {
				$awards['pSilver'] = 1;
				$awards['pBronze'] = 2;
			}else{
				$awards['pGold'] = 1;
			}

// echo "<pre>";
// print_r( $awards);
// echo "</pre>";
$string = json_encode($myCustomFields);
//convert publish_up date into format date
$timestamp = strtotime($this->item->publish_up);
$new_date_format = date('M d,Y', $timestamp);
$previous_listing_href = "";
$show_date_status = True;
$show_client_status = True;
$show_awards_status = False;
$show_category_status = False;
$show_replicate_brand_status = False;
$shareOrDownloadStatus = "share";




switch ($this->item->category_title) {
	case 'Programme Highlights':
		$previous_listing_href = "./whats-on";
		$award = "";
		$show_date_status = True;
		$show_category_status = False;
		$show_client_status = False;
		$show_awards_status = False;
		$show_replicate_brand_status = False;
		$shareOrDownloadStatus = "share";
		break;
	case 'Latest Updates':
		$previous_listing_href = "./whats-on/latest-updates";
		$award = "";
		$show_date_status = True;
		$show_category_status = False;
		$show_client_status = False;
		$show_awards_status = False;
		$show_replicate_brand_status = False;
		$shareOrDownloadStatus = "share";
		break;
	case 'Our Work':
		$previous_listing_href = "./our-work";
		$award = $myCustomFields['award'];
		$our_work_category = $myCustomFields['our-work-category'];
		$our_work_category = explode(",",$our_work_category);
		$show_date_status = True;
		$show_client_status = True;
		$show_awards_status = True;
		$show_category_status = True;
		$show_replicate_brand_status = True;
		$shareOrDownloadStatus = "shareWining";
		break;
	default:
		$previous_listing_href = "./";
		break;
}

?>

<style>
	#overlay-div{
		width: 100%;
		position: absolute;
		top: 0;
		bottom: 0;
		z-index: 10;
	}
</style>
<div id="app">

	<div class="article-details">
		<div class="row">
			<div class="col-lg-2">
				<div class="sticky-box">
					<a id="back-article" class="btn btn-pink d-lg-block mb-3"
						href="<?php echo $previous_listing_href; ?>">
						Back to Listing
					</a>

					<?php
				if ($this->item->prev !== ""):
				?>
					<a class="text-right pagination-side prev" href="<?php echo $this->item->prev; ?>">
						<div class="nav-label">
							<i class="fas fa-chevron-left"></i> Previous
						</div>
						<div class="related-article-title">
							<?php echo $this->item->prev_title; ?>
						</div>
					</a>
					<?php endif; ?>
				</div>
			</div>

			<div class="col-lg-8">
				<div class="demo">

					<!-- Badge -->
					<?php if (count($awards['Gold'])): ?>

					<div class="ribbon-holder" :class="'ribbon-'+awards.pGold">
							<div class='ribbon gold'>
							<i class="fas fa-trophy"></i>
							<span class="amount">x<?php echo count($awards['Gold']); ?></span>
						</div>
					
					</div>

					<?php endif; ?>
					<?php if (count($awards['Silver'])): ?>
					<div class="ribbon-holder" :class="'ribbon-'+awards.pSilver">
						<div class='ribbon silver'>
							<i class="fas fa-trophy"></i>
							<span class="amount">x<?php echo count($awards['Silver']); ?></span>
						</div>
					</div>
					<?php endif; ?>
					<?php if (count($awards['Bronze'])): ?>
					<div class="ribbon-holder" :class="'ribbon-'+awards.pBronze">
						<div class='ribbon bronze'>
							<i class="fas fa-trophy"></i>
							<span class="amount">x<?php echo count($awards['Bronze']); ?></span>
						</div>
					</div>
					<?php endif; ?>

				<ul id="lightSlider">
					<?php for ($i=1; $i <= 5; $i++) {
						if ($myCustomFields['slider-'.$i.'-title'] !== "") { ?>

					<li style="text-align: center;"
						data-thumb="<?php echo JUri::root(true) .'/'. $myCustomFields['slider-'.$i.'-image'][1]; ?>">

						<?php if ($myCustomFields['slider-'.$i.'-type'] =='Youtube') { ?>

						<a href='<?php echo $myCustomFields["slider-".$i."-url"]; ?>' data-lity>
							<img src="<?php echo $myCustomFields['slider-'.$i.'-image'][1]; ?>" />
						</a>

						<?php }else{ ?>

						<a href="" data-lity
							data-lity-target="../<?php echo $myCustomFields['slider-'.$i.'-image'][1]; ?>">
							<img src="<?php echo $myCustomFields['slider-'.$i.'-image'][1]; ?>" />
						</a>

						<?php } ?>
					</li>

					<?php }
					} ?>
				</ul>
			</div>
			<div class="article-wrapper">

				<?php if ($show_date_status) { ?>
				<div class="date">
					<?php echo $new_date_format; ?>
				</div>
				<?php } ?>

				<h2 class="title">
					<?php echo $this->item->title; ?>

					<small>
						<?php echo $myCustomFields['description']; ?>
					</small>

					<div style="position:relative;">
						<div id="addthis_button" style="padding-top:10px;" class="addthis_inline_share_toolbox"></div>

						<div v-if="loginStatus == '0'" id="overlay-div" data-toggle="modal"
							data-target="#downloadCenterModal"></div>
					</div>

					<?php if($this->articleShared == 1){ ?>
						<div class="sharing-status">
							<i class="far fa-check-circle"></i> You have collected some Q-Perks by sharing!
						</div>
					<?php } ?>
				</h2>

				<?php if ($show_client_status) { ?>
				<div class="mb-4">
					<div class="date mb-1">Client</div>
					<h6><?php echo $myCustomFields['client']; ?></h6>
				</div>

				<?php } ?>

				<?php if ($show_awards_status) { ?>
				<div class="mb-4" v-if="awards.Gold.length != 0 || awards.Silver.length != 0 || awards.Bronze.length != 0">
					<div class="date mb-1">Award(s)</div>
					<ul class="award-listing">
						<li class="list-award" v-for="gold in awards.Gold" >
							<div class="ribbon-holder">
								<div class='ribbon gold'>
									<i class="fas fa-trophy"></i>
								</div>
							</div>
							<span v-html="gold"></span>
						</li>
						<li class="list-award" v-for="silver in awards.Silver">
							<div class="ribbon-holder">
								<div class='ribbon silver'>
									<i class="fas fa-trophy"></i>
								</div>
							</div>
							<span v-html="silver"></span>
						</li>
						<li class="list-award" v-for="bronze in awards.Bronze">
							<div class="ribbon-holder">
								<div class='ribbon bronze'>
									<i class="fas fa-trophy"></i>
								</div>
							</div>
							<span v-html="bronze"></span>
						</li>
					</ul>
				</div>
				<?php } ?>

				<?php if ($show_category_status) { ?>
				
				<div class="mb-4">
					<div class="date mb-1">Category</div>
					<?php foreach ($our_work_category as $key => $value) {
						echo "<span class='badge badge-pill badge-pink'>".  $value ."</span>";
					} ?>
				</div>

				<?php } ?>

				<hr />

				<div class="editor-content">
					<?php echo $this->item->text; ?>

					<?php if ($show_replicate_brand_status) { ?>
					<div class="card bg-grey p-4 border-0 mt-5 rounded-4">
						<div class="row align-items-center">
							<div class="col-md-8 text-center text-md-left f-24 font-brand bold mb-3 mb-md-0">
								Like the campaign?
							</div>
							<div class="col-md-4 text-center text-md-right">
								<button class="btn btn-link p-0 pt-2" data-toggle="modal" data-target="#replicateBrandModal">
									<img src="./images/2020/nextisyours.png" alt="Next is Yours" width="220">
								</button>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>

				<div class="clearfix"></div>

				<div class="mobile-pagination d-lg-none">
					<?php
					if ($this->item->next !== ""):
					?>
					<div class="row">
						<div class="col">
							<a href="<?php echo $this->item->prev; ?>">
								<div class="nav-label">
									<i class="fas fa-chevron-left"></i> Previous
								</div>
								<div class="related-article-title">
									<?php echo $this->item->prev_title; ?>
								</div>
							</a>
						</div>

						<div class="col">
							<a class="text-right" href="<?php echo $this->item->next; ?>">
								<div class="nav-label">
									Next <i class="fas fa-chevron-right"></i>
								</div>
								<div class="related-article-title">
									<?php echo $this->item->next_title; ?>
								</div>
							</a>
						</div>
					</div>


					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="col-lg-2 d-lg-inline-block d-none">
			<div class="sticky-box">
				<?php
				if ($this->item->next !== ""):
				?>
				<a class="pagination-side next" href="<?php echo $this->item->next; ?>">
					<div class="nav-label">
						Next <i class="fas fa-chevron-right"></i>
					</div>
					<div class="related-article-title">
						<?php echo $this->item->next_title; ?>
					</div>
				</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<div id="inline" style="background:#fff" class="lity-hide">
	Inline content
</div>

<div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" hidden="true"></div>

			<div class="modal-body">
				<div>
					<button @click="closeModal" type="button" class="close font-brand" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">Close</span>
					</button>

					<div style="clear: both;"></div>
				</div>
				<div class="text-center">
					<h3 class="text-white">Login in to earn points</h3>
					<p class="text-white mb-4">Hey, we know you're excited to redeem exclusive merchandise! Log in now or sign up to earn Q-Perks when you share this article.</p>
					<a href="#" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
					<a href="#" data-dismiss="modal" class="btn btn-white">Skip</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade custom-modal" ref="vuemodal" id="replicateBrandModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" hidden="true"></div>

			<div class="modal-body">
				<div>
					<button @click="closeModal" type="button" class="close font-brand" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">Close</span>
					</button>

					<div style="clear: both;"></div>
				</div>
				<div class="text-center">
					<!-- <h3 class="text-white">Login in to earn points</h3>
					<p class="text-white mb-4">Hey, we know you're excited to redeem exclusive merchandise! Log in now or sign up to earn Q-Perks when you share this article.</p>
					<a href="#" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
					<a href="#" data-dismiss="modal" class="btn btn-white">Skip</a> -->
					<form id="form-replicatebrandlog"
						action="<?php echo Route::_('index.php?option=com_replicate_brand&task=replicatebrandlogform.save'); ?>"
						method="post" class="text-center enquiry" enctype="multipart/form-data">

						<h3 class="text-white">Set New Benchmark Together</h3>
						<p class="text-white mb-4">Tell us what you aspire to achieve and we'll deliver the best solution for you.</p>


						<input type="hidden" name="jform[id]" value="" />

						<input type="hidden" name="jform[ordering]" value="" />

						<input type="hidden" name="jform[state]" value="" />

						<input type="hidden" name="jform[checked_out]" value="" />

						<input type="hidden" name="jform[checked_out_time]" value="" />

						<input type="hidden" name="jform[article_title]" value="<?php echo $this->item->title; ?>" />


						<div class="">
							<div class="">
								<div class="field-placey form-group">
									<input class="form-control" name="jform[brand_name]" type="text" placeholder="Brand name *" required>
									<label for="">Brand name *</label>
								</div>
								<div class="field-placey form-group">
									<input class="form-control" name="jform[product_category]" type="text" placeholder="Product category *" required>
									<label for="">Product category *</label>
								</div>
								<div class="field-placey form-group">
									<textarea class="form-control" name="jform[opinion]" type="text" placeholder="Objective" required></textarea>
									<label for="">Objective *</label>

								</div>
								<p class="form-text text-white font-italic" for="name">(Tell us what you aspire to achieve)</p>

								
								<h5 class="mb-3 mt-5 text-white">Your Personal Details</h5>

								<div class="field-placey form-group">
									<input class="form-control" name="jform[user_name]" type="text" placeholder="Your name" value="<?php echo $userName;?>" required>
									<label for="name">Full Name *</label>
								</div>
								<div class="field-placey form-group">
									<input class="form-control" name="jform[user_email]" type="text"
										pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="example@email.com" value="<?php echo $userEmail;?>" required>
									<label for="email">Email address *</label>
								</div>

								<div class="field-placey form-group">
									<input class="form-control" name="jform[user_contact]" type="text" placeholder="Contact number" value="<?php echo $userContact;?>"
										required>
									<label for="contact">Contact number *</label>
								</div>

								<div class="field-placey form-group">
									<input class="form-control" name="jform[user_designation]" type="text" placeholder="Designation" value="<?php echo $userDesignation;?>"
										required>
									<label for="designation">Designation *</label>
								</div>

								<div class="field-placey form-group">
									<input class="form-control" name="jform[user_company_name]" type="text" placeholder="Company name" value="<?php echo $userCompanyName;?>"
										required>
									<label for="company_name">Company name *</label>
								</div>

								<!-- <button style="width: 180px;" class="btn btn-pink mt-3" type="submit">Submit</button> -->
								<button style="width: 180px;" class="btn btn-pink mt-3 mr-2">Submit</button>
								<a href="#" data-dismiss="modal" class="btn btn-white mt-3">Skip</a>
							</div>
						</div>

						<input type="hidden" name="option" value="com_replicate_brand" />
						<input type="hidden" name="task" value="replicatebrandlogform.save" />
						<?php echo HTMLHelper::_('form.token'); ?>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

</div></div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#lightSlider').lightSlider({
			gallery: true,
			item: 1,
			loop: true,
			slideMargin: 0,
			thumbItem: 6,
			enableTouch:true,
			enableDrag:true,
			freeMove:true,
			swipeThreshold: 40,
			autoWidth: false,
			controls: false,
			adaptiveHeight:true,
			thumbMargin: 10,

		});
	});
	var source = <?php echo json_encode($this->item->alias); ?>;
	var shareOrDownloadStatus = <?php echo json_encode($shareOrDownloadStatus); ?>;
	var userIdFromObject = <?php echo json_encode($userId); ?>;
	var awardsFromObject = <?php echo json_encode($awards); ?>;
	var emailStatus = <?php echo json_encode($emailStatus); ?>;

	if (emailStatus != "" && emailStatus == "sent") {
	swal("Thank you.", "We will contact you soon!", "success", {
	  button: {
	    text: "Done",
			className: "btn btn-pink",
	  },
	});
}else if( emailStatus == "fail"){
	swal("Oops!", "There's something wrong, kindly resubmit your survey.","error", {
	  button: {
	    text: "Try Again",
			className: "btn btn-pink",
	  },
	});
}

    var app = new Vue({
        el: '#app',
        data: {
			item : userIdFromObject,
			awards: awardsFromObject,
			loginStatus : 0
        },
        mounted: function () {
			console.log(window.location.href);
			if (window.location.href == "https://quake.com.my/latest-update/465-a-raya-aidilfitri-guide-for-marketers") {
				window.location.replace("https://quake.com.my/raya-guide-for-marketers");
			}
			if (window.location.href == "https://quake.com.my/latest-update/551-an-oxpicious-guide-for-marketers") {
				window.location.replace("https://quake.com.my/moomooda2021");
			}
			if (window.location.href == "https://quake.com.my/latest-update/579-mmdcny2021-together-registration") {
				window.location.replace("https://quake.com.my/events/rsvp/mmd2021");
			}

			if (window.location.href == "https://43.228.245.193/~quakesta/latest-update/509-moomooda2021") {
				window.location.replace("https://43.228.245.193/~quakesta/moomooda2021");
			}

			if (window.location.href == "http://43.228.245.193/~quakesta/latest-update/509-moomooda2021") {
				window.location.replace("http://43.228.245.193/~quakesta/moomooda2021");
			}
			
			// window.location.href = "http://www.w3schools.com";
			if (parseInt(this.item) > 0) {
				this.loginStatus = 1;
			}
			jQuery(this.$refs.vuemodal).on("hidden.bs.modal", this.closeModal)
        },
        updated: function () {
			
        },
        methods: {
			goToLink :function(){
                window.location.href = '../login';
            },
			closeModal :function(){
				this.loginStatus = 1
			}
        }
    })
</script>
