<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

?>
<!-- <div class="reset<?php echo $this->pageclass_sfx; ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1>
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</h1>
		</div>
	<?php endif; ?>
	<form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>" method="post" class="form-validate form-horizontal well">
		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<fieldset>
				<p><?php echo JText::_($fieldset->label); ?></p>
				<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
					<?php if ($field->hidden === false) : ?>
						<div class="control-group">
							<div class="control-label">
								<?php echo $field->label; ?>
							</div>
							<div class="controls">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</fieldset>
		<?php endforeach; ?>
		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary validate">
					<?php echo JText::_('JSUBMIT'); ?>
				</button>
			</div>
		</div>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div> -->

<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

?>
<div class="reset<?php echo $this->pageclass_sfx; ?>">

  <div class="row justify-content-center">
    <div class="col-md-8 col-lg-6 text-center">
      <h2 class="f-30 mt-40">Forgot your password?</h2>
      <p class="f-20 font-brand">Enter the email address you registered with Quake Club and check your inbox for a password reset email. </p>

    	<form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>" method="post" class="form-validate club-form">
    		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
    			<fieldset>
    				<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
    					<?php if ($field->hidden === false) : ?>
                <div class="field-placey form-group">
      						<input type="text" name="jform[email]" id="jform_email" value="" class="validate-username required form-control" size="30" required="required" aria-required="true" autocomplete="off" placeholder=" ">
      						<label for="jform_email">Email address</label>
      					</div>
    					<?php endif; ?>
    				<?php endforeach; ?>
    			</fieldset>
    		<?php endforeach; ?>
    		<div class="control-group">
    			<div class="controls">
    				<div class="row">
              <div class="col-sm-6">
                <a href="<?php echo $this->baseurl ?>/login" class="btn btn-pink btn-block mb-3">
        					Back
        				</a>
              </div>
              <div class="col-sm-6 order-first order-sm-last">
                <button type="submit" class="btn btn-pink btn-block validate mb-3">
        					<?php echo JText::_('JSUBMIT'); ?>
        				</button>
              </div>
            </div>
    			</div>
    		</div>
    		<?php echo JHtml::_('form.token'); ?>
    	</form>
    </div>
  </div>

</div>

<!-- <div class="reset<?php echo $this->pageclass_sfx; ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1>
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</h1>
		</div>
	<?php endif; ?>
	<form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>" method="post" class="form-validate form-horizontal well">
		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<fieldset>
				<p><?php echo JText::_($fieldset->label); ?></p>
				<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
					<?php if ($field->hidden === false) : ?>
						<div class="control-group">
							<div class="control-label">
								<?php echo $field->label; ?>
							</div>
							<div class="controls">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</fieldset>
		<?php endforeach; ?>
		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary validate">
					<?php echo JText::_('JSUBMIT'); ?>
				</button>
			</div>
		</div>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div> -->


<!-- <div class="reset">
		<form id="user-registration" action="/~cheeliem/astro-joomla/user-profile?task=reset.request" method="post" class="form-validate form-horizontal well">
					<fieldset>
    				<p>Please enter the email address for your account. A verification code will be sent to you. Once you have received the verification code, you will be able to choose a new password for your account.</p>
						<div class="control-group form-group">
							<div class="control-label">
								<label id="jform_email-lbl" for="jform_email" class="hasPopover required" title="" data-content="Please enter the email address associated with your User account.<br />A verification code will be sent to you. Once you have received the verification code, you will be able to choose a new password for your account." data-original-title="Email Address">
	Email Address<span class="star">&nbsp;*</span></label>
							</div>
							<div class="controls">
								<input type="text" name="jform[email]" id="jform_email" value="" class="validate-username required form-control" size="30" required="required" aria-required="true" autocomplete="off">
							</div>
						</div>
					</fieldset>
				<div class="control-group form-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary validate">
					Submit				</button>
			</div>
		</div>
		<input type="hidden" name="c9bd055fa04511bba896aa303599c50a" value="1">	</form>
</div> -->
