<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

?>
<div class="reset-complete<?php echo $this->pageclass_sfx; ?>" id="app">
	<div class="row justify-content-center  mt-40 text-center">
		<div class="col-lg-6 col-md-8">
			<h2 class="f-30">Reset your password</h2>
      <p class="f-20 font-brand">You have requested to reset the password for: <span class="d-block text-highlight"><?php echo htmlspecialchars($_GET["email"]);?></span></p>

			<form @submit="checkFormValidate" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.complete'); ?>" method="post" class="form-validate club-form" novalidate>

				<div class="control-group">
					<div class="control-label">
						<div class="field-placey form-group">
						<div class="form-text form-error" v-if="!checkPass1">Please fill in your new password
						</div>
						<div class="form-text">Must contain at least one number and one alphabet, and at least 8 or more characters. Password is case sensitive.</div>
						<i class="far fa-eye-slash toggle-password" ></i>
							<input type="password" v-model="vpass1" name="jform[password1]" id="jform_password1" value="" class="validate-password required form-control" maxlength="99" size="30" required="required" aria-required="true" autocomplete="off" placeholder=" " pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$">

							<label for="jform_password1">New password</label>
						</div>
					</div>
					<div class="controls">
						<div class="field-placey form-group">
						<div class="form-text form-error" v-if="!checkPass2">Please reconfirm your password</div>
						<div class="form-text form-error" v-if="!checkPass2Confirm">New password and confirm password do not match, please try again.</div>
						<i class="far fa-eye-slash toggle-password" ></i>
							<input type="password" v-model="vpass2" name="jform[password2]" id="jform_password2" value="" class="validate-password required form-control" maxlength="99" size="30" required="required" aria-required="true" autocomplete="off" placeholder=" " aria-invalid="false" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$">
							<label for="jform_password2">Retype new password</label>
						</div>
						</div>
					</div>

				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-pink btn-block validate">
							Reset my password
						</button>
					</div>
				</div>
				<?php echo JHtml::_('form.token'); ?>
			</form>
		</div>
	</div>

</div>

<!-- <div class="reset-complete<?php echo $this->pageclass_sfx; ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1>
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</h1>
		</div>
	<?php endif; ?>
	<form action="<?php echo JRoute::_('index.php?option=com_users&task=reset.complete'); ?>" method="post" class="form-validate form-horizontal well">
		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<fieldset>
				<p><?php echo JText::_($fieldset->label); ?></p>
				<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php echo $field->input; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</fieldset>
		<?php endforeach; ?>
		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary validate">
					<?php echo JText::_('JSUBMIT'); ?>
				</button>
			</div>
		</div>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div> -->

<script type="text/javascript">

    var app = new Vue({
        el: '#app',
        data: {
			checkPass1 : true,
			checkPass2 : true,
			checkPass2Confirm : true,
			vpass1 : "",
			vpass2 : "",
		},
        mounted: function () {
        },
        updated: function () {
        },
        methods: {
			checkFormValidate :function(e){
				this.checkPass1 = jform_password1.checkValidity();
				this.checkPass2 = jform_password2.checkValidity();
				if (this.vpass1 != this.vpass2) {
					this.checkPass2Confirm = false;
				}else{
					this.checkPass2Confirm = true;
				}
				if (jform_password1.checkValidity() && jform_password2.checkValidity() && this.vpass1 == this.vpass2) {
					return true;
				}
				e.preventDefault();
			}
        }
    })
</script>

<script type="text/javascript">
	jQuery(".toggle-password").click(function() {

		jQuery(this).toggleClass("fa-eye fa-eye-slash");
		var input = jQuery(this).next();
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});
</script>
