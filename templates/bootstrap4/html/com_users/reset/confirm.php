<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

?>
<div class="reset-confirm<?php echo $this->pageclass_sfx; ?>">
	<div class="row justify-content-center  mt-40 text-center">
		<div class="col-lg-6 col-md-8">
			<div class="email-icon">
				<i class="fas fa-envelope"></i>
			</div>
			<h2 class="f-30">Password reset email sent</h2>
      <p class="f-20 font-brand">We have sent a reset password email to <span class="text-highlight"><?php echo htmlspecialchars($_GET["email"]);?></span>. Please click the reset password link to set your new password.</p>

			<!-- <form action="<?php echo JRoute::_('index.php?option=com_users&task=reset.confirm'); ?>" method="post" class="form-validate club-form">

				<div class="control-group">
					<div class="control-label">
						<div class="field-placey form-group">
							<input type="text" name="jform[username]" id="jform_username" value="<?php echo htmlspecialchars($_GET["email"]);?>" class="validate-username required form-control" size="30" required="required" aria-required="true" autocomplete="off" placeholder=" ">
							<label for="jform_username">Email address</label>
							<input type="hidden" name="jform[email]" value="<?php echo htmlspecialchars($_GET["email"]);?>">
						</div>
					</div>
					<div class="controls">
						<div class="field-placey form-group">
							<input type="text" name="jform[token]" id="jform_token" value="<?php echo htmlspecialchars($_GET["token"]);?>" class="validate-username required form-control" size="32" required="required" aria-required="true" autocomplete="off" placeholder=" " aria-invalid="false">
							<label for="jform_token">Verification code</label>
						</div>
						</div>
					</div>

				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-pink btn-block validate">
							<?php echo JText::_('JSUBMIT'); ?>
						</button>
					</div>
				</div>
				<?php echo JHtml::_('form.token'); ?>
			</form> -->
		</div>
	</div>

</div>

<!-- <div class="reset-confirm<?php echo $this->pageclass_sfx; ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1>
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</h1>
		</div>
	<?php endif; ?>
	<form action="<?php echo JRoute::_('index.php?option=com_users&task=reset.confirm'); ?>" method="post" class="form-validate form-horizontal well">
		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<fieldset>
				<p><?php echo JText::_($fieldset->label); ?></p>
				<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php echo $field->input; ?>
							</div>
						</div>
				<?php endforeach; ?>
			</fieldset>
		<?php endforeach; ?>
		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary validate">
					<?php echo JText::_('JSUBMIT'); ?>
				</button>
			</div>
		</div>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div> -->
