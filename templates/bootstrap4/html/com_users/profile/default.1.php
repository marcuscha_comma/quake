<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
if ($this->data->mobileNo != '') {
	$mobileNo = explode('-',$this->data->mobileNo);
}
if ($this->company->mobile_no != '') {
	$companyMobileNo = explode('-',$this->company->mobile_no);
}
$user = JFactory::getUser();
$db_cus_quake_club_qperks_monthly    = JFactory::getDBO();
$query_cus_quake_club_qperks_monthly = $db_cus_quake_club_qperks_monthly->getQuery( true );
$query_cus_quake_club_qperks_monthly
  ->select( '(month01+month02+month03+month04+month05+month06+month07+month08+month09+month10+month11+month12) as total' )
  ->from( $db_cus_quake_club_qperks_monthly->quoteName( '#__cus_quake_club_qperks_monthly' ) )
  ->where($db_cus_quake_club_qperks_monthly->quoteName('state')." > 0")
  ->where($db_cus_quake_club_qperks_monthly->quoteName('user_id')." = " .$user->id);
$db_cus_quake_club_qperks_monthly->setQuery( $query_cus_quake_club_qperks_monthly );
$cus_quake_club_qperks_monthly = $db_cus_quake_club_qperks_monthly->loadObjectList();



$user_total_qperks_point = $cus_quake_club_qperks_monthly[0]->total;
$userWhatsappSaveUrl = JRoute::_('index.php?option=com_users&task=profile.userWhatsappSave');
echo "<pre>";
print_r($this->data);
echo "</pre>";

?>

<div id="app">
	<div class="pink-page-title">
		<div class="container">
			<h2>My Account</h2>
		</div>
	</div>

	<div class="profile-pop bg-blue rounded-card">
		<div class="card-body">
			<div class="row ">
				<div class="d-none d-md-inline-block col-xl-8 col-lg-7 col-md-6 mb-4 mb-md-0">
					<div class="welcome-user">
						<div class="profile-pic">
							<i class="fas fa-user"></i>
						</div>
						<div class="text-area">
							<div class="profile-text">
								Hi there,
								<div class="profile-name">
									<?php echo $this->data->name; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-5 col-md-6">
					<div class="welcome-user">
						<div class="percentage-bubble">
							<span>{{calProfileComplete}}%</span>
						</div>
						<div class="text-area">
							<span v-if="calProfileComplete < 100">Profile completion</span>
							<span v-if="calProfileComplete >= 100">Profile completed</span>
							<small v-if="calProfileComplete < 100">Keep going, you’re almost there!</small>
							<small v-if="calProfileComplete >= 100">Congratulation! You have successfully earned 10 Q-Perks.</small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row justify-content-between">
		<div class="col-12 col-lg-6 mb-4 mb-lg-0">
			<div class="profile-points font-brand">
				<div class="row align-items-center position-relative">
					<div class="col-sm-4 col-md-4 col-lg-5 col-xl-4">
						<div class="perks-status mx-auto">
							<div class="">
								<h3><?php if($this->data->dob_redemption_num == 1){echo $user_total_qperks_point+200;}else{
									echo $user_total_qperks_point;
								} ?></h3>
								Q-Perks
							</div>
						</div>
					</div>
					<div class="col-sm-8 col-md-8 col-lg-7 col-xl-8">
						<div class="text-area">
							<h4 class="f-24">Points balance</h4>
							<?php if($this->previous_quarter_month_point){ ?>
							<p><?php echo $this->previous_quarter_month_point; ?> points will expire on <?php echo $this->previous_quarter_expire_text; ?></p>
							<?php } ?>
							<p><?php echo $this->quarter_month_point; ?> points will expire on <?php echo $this->quarter_expire_text; ?></p>
							<div class="d-block d-sm-inline-block">
								<a href="./quake-club/points-history" class="link-underline text-pink mr-0 mr-sm-5">Points <span class="d-inline-block">history <i class="fas fa-angle-right text-link-arrow"></i></span></a>
							</div>
							<div class="d-block d-sm-inline-block">
								<a href="./quake-club/redemption-list" class="link-underline text-pink">Redemption <span class="d-inline-block">history  <i class="fas fa-angle-right text-link-arrow"></i></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-xl-5 col-lg-6">
			<div class="row no-gutters redemption-reminder" :class="{'redeemed' : userDobRedemptionNum!='1' && userRedemptionNum!='1'}"> <!--** ADD class /.redeemed for REDEEMED status---->
				<div class="col-4 col-lg-5 col-xl-4">
					<div class="bg-pink icon-holder">
						<div class="gift-icon">

						</div>
					</div>
				</div>
				<div class="col-8 col-lg-7 col-xl-8">
					<div class="download-center-div">
						<?php echo $this->quarter_date_html; ?>
						<!-- <h5 class="f-14">01 Jan - 31 March</h5> -->
						<p v-if="userDobRedemptionNum=='1' || userRedemptionNum=='1'">You have not made any redemptions this quarter yet</p>
						<p v-if="userDobRedemptionNum!='1' && userRedemptionNum!='1'">You have already made a redemption this quarter</p> <!--Content for REDEEMED---->
						<div class="download-holder" v-if="userDobRedemptionNum=='1' || userRedemptionNum=='1'">
							<a href="./quake-club/rewards-catalogue" class="btn btn-text download-center-button link-underline text-pink">
								View catalogue & redeem <span class="d-inline-block">now <i class="fas fa-angle-right text-link-arrow"></i></span>
							</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>


	<h3 class="f-20 mt-5 border-bottom pb-3 mb-3">
		My profile
		<i class="title-action fas fa-pen" v-show="editProfileStatus==0" @click="editProfileStatus=1"></i>
		<i class="title-action fas fa-times" v-show="editProfileStatus==1" @click="editProfileStatus=0"></i>
	</h3>
	<form id="my-profile"	class="profile-form" :class="{ 'edit-form': editProfileStatus, 'readonly-form': !editProfileStatus }" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.save&user_id=' . (int) $this->data->id);?>" method="post">
		<input type="hidden" name="jform[email1]" value="<?php echo $this->data->email; ?>">
		<input type="hidden" name="jform[email2]" value="<?php echo $this->data->email; ?>">

		<div class="row">
			<div class="col-lg-6">
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="name">My name</label>
					<div class="col-sm-8">
						<input autocomplete="quake" id="name" class="required form-control" name="jform[name]"
						:class="{ 'form-control-plaintext' : !editProfileStatus }"
						value="<?php if($this->data->name)echo $this->data->name;else echo "-";?>" type="text" placeholder=" "
						:readonly="!editProfileStatus" required="required" aria-required="true">
					</div>
				</div>

				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="name">Mobile no.</label>
					<div class="col-sm-8">
						<input v-show="!editProfileStatus" class="form-control form-control-plaintext" readonly value="<?php echo $mobileNo[0].'-'. $mobileNo[1];?>">

						<div v-show="editProfileStatus" class="row">
							<div class="col-5">
								<select name="jform[country_code]" id="country_code" class="selectpicker white-dropdown"
									data-width="100%" v-model="mobileCountryCode" :disabled="!editProfileStatus">
									<option v-for="item in countryCode" :value="item">{{item}}</option>
								</select>
							</div>
							<div class="col-7">
								<input autocomplete="quake" class="form-control" name="jform[contact_number]" type="tel"
									placeholder=" " value="<?php echo $mobileNo[1];?>"
									:readonly="!editProfileStatus" pattern=".{9,16}">
							</div>
						</div>
					</div>
				</div>

				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="datepicker">Birthday</label>
					<div class="col-sm-8">
						<input autocomplete="quake" class="form-control" :class="{ 'form-control-plaintext' : !editProfileStatus }" name="jform[dob]" id="datepicker" type="text" value="<?php
						$tmpDate = strtotime($this->data->dob);
						$this->data->dob = date('d-m-Y', $tmpDate);
						if($this->data->dob)echo $this->data->dob;else echo "-";?>" :readonly="!editProfileStatus" :disabled="!editProfileStatus">
					</div>
				</div>

				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="birthday">Gender</label>
					<div class="col-sm-8">
						<input v-show="!editProfileStatus" class="form-control form-control-plaintext" readonly value="<?php
						if ($this->data->gender != null) {
							echo JText::_('COM_USERS_USER_GENDER_OPTION_'.$this->data->gender);
						}else{
							echo "-";
						}
						 ?>
						 ">

						<div v-show="editProfileStatus">
							<div class="custom-control custom-radio custom-control-inline">
								<input class="custom-control-input" name="jform[gender]" type="radio" id="Male" value="1" v-model="pickedGender"
									:disabled="!editProfileStatus">
							  <label class="custom-control-label" for="Male">Male</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input class="custom-control-input" name="jform[gender]" type="radio" id="Female" value="0" v-model="pickedGender"
									:disabled="!editProfileStatus">
							  <label class="custom-control-label" for="Female">Female</label>
							</div>
						</div>

					</div>
				</div>
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="">Race</label>
					<div class="col-sm-8">
						<input v-show="!editProfileStatus" class="form-control form-control-plaintext" readonly value="<?php
						if ($this->data->race != null) {
							echo JText::_('COM_USERS_USER_RACE_OPTION_'.$this->data->race);
						}else{
							echo "-";
						}
						?>">

						<div v-show="editProfileStatus">
							<select name="jform[race]" id="race" class="selectpicker white-dropdown"
							data-width="100%" v-model="pickedRace" :disabled="!editProfileStatus">
								<option v-for="item in race" :value="item.value">{{item.name}}</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="birthday">Delivery address</label>
					<div class="col-sm-8">
						<div class="form-control" v-show="!editProfileStatus"><?php if( $this->data->address)echo $this->data->address;else echo "-";?></div>
						<textarea autocomplete="quake" v-show="editProfileStatus" class="form-control" name="jform[address]" type="text" placeholder=" " rows="2"><?php echo $this->data->address;?></textarea>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row mt-3 justify-content-end">
					<div class="col-lg-8">
						<button v-show="editProfileStatus" class="btn btn-pink" type="submit">Save changes</button>
					</div>
				</div>
			</div>
		</div>

	</form>


	<h3 class="f-20 mt-5 border-bottom pb-3 mb-3">
		Company profile
		<i class="title-action fas fa-pen" v-show="editCompanyStatus==0" @click="editCompanyStatus=1"></i>
		<i class="title-action fas fa-times" v-show="editCompanyStatus==1" @click="editCompanyStatus=0"></i>
	</h3>
	<form id="my-company" class="profile-form" :class="{ 'edit-form': editCompanyStatus, 'readonly-form': !editCompanyStatus }"
		action="<?php echo JRoute::_('index.php?option=com_users&task=profile.companySave&user_id=' . (int) $this->data->id);?>"
		method="post" enctype="multipart/form-data">

		<div class="row">
			<div class="col-lg-6">
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="company-name">Company name</label>
					<div class="col-sm-8">
						<input id="name" class="required form-control" name="jform[name]"
						:class="{ 'form-control-plaintext' : !editCompanyStatus }"
						value="<?php if($this->company->name)echo $this->company->name; else echo "-";?>" type="text" placeholder=" "
						:readonly="!editCompanyStatus" required="required" aria-required="true" autocomplete="quake">
					</div>
				</div>

				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="company-email">Email address</label>
					<div class="col-sm-8">
						<input pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="jform[email]"
							:class="{ 'form-control-plaintext' : !editCompanyStatus }"
							class="required form-control" id="company-email" value="<?php if($this->data->email)echo $this->data->email; else echo "-";?>" type="email" placeholder=""
							:readonly="!editCompanyStatus" @change="emailOnChanged()" required="required" aria-required="true" autocomplete="quake">
							<input type="hidden" name="jform[emailChangedStatus]" :value="emailChangedStatus">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="company-designation">Company address</label>
					<div class="col-sm-8">
						<div class="form-control" v-show="!editCompanyStatus"><?php if($this->company->address)echo $this->company->address; else echo "-";?></div>
						<textarea autocomplete="quake" v-show="editCompanyStatus" class="form-control" name="jform[address]" type="text" placeholder=" " rows="2"><?php echo $this->company->address;?></textarea>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="">Company phone no.</label>
					<div class="col-sm-8">
						<input v-show="!editCompanyStatus" class="form-control form-control-plaintext" readonly value="<?php echo $companyMobileNo[0] .'-'. $companyMobileNo[1];?>">

						<div v-show="editCompanyStatus" class="row">
							<div class="col-5">
								<select name="jform[country_code]" id="country_code" class="selectpicker white-dropdown"
									data-width="100%" v-model="companyMobileCountryCode" :disabled="!editCompanyStatus">
									<option v-for="item in countryCode" :value="item">{{item}}</option>
								</select>
							</div>
							<div class="col-7">
								<input autocomplete="quake" class="form-control" name="jform[contact_number]" type="tel"
									placeholder=" " value="<?php echo $companyMobileNo[1];?>"
									:readonly="!editCompanyStatus" pattern=".{9,16}">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="">Your job level</label>
					<div class="col-sm-8">
						<input v-show="!editCompanyStatus" class="form-control form-control-plaintext" readonly value="<?php if($this->company->classification != null)echo $this->company->classification; else echo "-";?>">

						<div v-show="editCompanyStatus">
							<select name="jform[classification]" id="classification" class="selectpicker white-dropdown"
							data-width="100%" v-model="pickedClassification" :disabled="!editCompanyStatus">
								<option v-for="item in classification" :value="item.value">{{item.name}}</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="">Type of profession</label>
					<div class="col-sm-8">
						<input v-show="!editCompanyStatus" class="form-control form-control-plaintext" readonly value="<?php if($this->company->industry != null)echo $this->company->industry; else echo "-";?>">

						<div v-show="editCompanyStatus">
							<select name="jform[industry]" id="industry" class="selectpicker white-dropdown"
							data-width="100%" v-model="pickedIndustry" :disabled="!editCompanyStatus">
								<option v-for="item in industry" :value="item.value">{{item.name}}</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="company-designation">Designation</label>
					<div class="col-sm-8">
						<input name="jform[designation]"
							:class="{ 'form-control-plaintext' : !editCompanyStatus }"
							class="required form-control" id="company-designation" value="<?php echo $this->company->designation;?>" type="text" placeholder=""
							:readonly="!editCompanyStatus" required="required" aria-required="true" autocomplete="quake">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<div class="mb-3 row">
					<label class="col-sm-4 col-form-label" for="">Your business card</label>
					<div class="col-sm-6 col-lg-8 col-xl-6">
						<div class="" v-show="!editCompanyStatus" >
							<div v-show="!showImage">
								-
							</div>
						</div>
						<div class="hidden_input" :class="{ 'mt-3' : editCompanyStatus }">
							<div v-show="editCompanyStatus" class="">
								<div v-show="!showImage" class="bcard-placeholder">
									<label v-show="editCompanyStatus">Upload your business card here</label>
								</div>
							</div>
							<input v-show="editCompanyStatus" class="file_upload" name="business_card" type='file' accept="image/*" id="imgInp" @change="readUrl(this)" />

							<div v-show="showImage"  class="hover">
								<label>Change</label>
							</div>
							<div v-show="showImage" class="bcard-placeholder with-img" id="business-card">
							</div>
						</div>
						<div v-show="showProgress"  id="progress-wrp">
								<div class="progress-bar"></div>
								<div class="status">0%</div>
						</div>
						<div class="form-text form-error" v-if="!checkBusinessCard">Please upload your business card.</div>
						<div class="form-text form-error" v-if="!checkImgSize">Maximum upload file size: 2MB</div>
						<div class="form-text form-error" v-if="!checkImgPercent">Please wait while the upload process is completed.</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<div class="row mt-3 justify-content-end">
					<div class="col-lg-8">
						<button v-show="editCompanyStatus" class="btn btn-pink" type="submit">Save changes</button>
					</div>
				</div>
			</div>
		</div>



	</form>

	<div class="mt-5 mb-3">
		<div class="row">
			<div class="col-lg-6">
				<div class="d-block d-md-inline-block d-lg-block d-xl-inline-block mr-3">
					<h3 class="f-20">
						<span>Link with your social media account</span><br />
						<span class="form-text small" style="margin-top: 5px;"><em>Earn additional 10 Q-Perks</em></span>
					</h3>
				</div>
				<div class="d-inline-block align-top">
					<div :class="{'incomplete' : facebook == '' || facebook == null}" class="add-facebook" data-toggle="modal"
						data-target="#downloadCenterModal">
						<i class="fab fa-facebook"></i>
					</div>
				</div>
			</div>
			<div class="col-lg-6 mt-lg-0 mt-4">
				<div class="d-block d-md-inline-block d-lg-block d-xl-inline-block mr-3">
					<h3 class="f-20">
						<span>Click and send message to join Quake WhatsApp</span><br />
						<span class="form-text small" style="margin-top: 5px;"><em>Earn additional 20 Q-Perks</em></span>
					</h3>
				</div>
				<div class="d-inline-block align-top">
					<a @click="updateWhatsapp()" href="https://wa.me/60126040968?text=Hi%2C%20send%20me%20the%20latest%20issue%20of%20Astro%20Winning%20Partnership%20Series" target="_blank">
						<div :class="{'incomplete' : whatsapp == '' || whatsapp == 0}" class="add-facebook add-whatsapp">
							<i class="fab fa-whatsapp"></i>
						</div>
					</a>
				</div>
			</div>
		</div>

	</div>

	<div class="modal fade" id="downloadCenterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" hidden="true"></div>

				<div class="modal-body">
					<div>
						<button type="button" class="close font-brand" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">Close</span>
						</button>

						<div style="clear: both;"></div>
					</div>

					<form
						action="<?php echo JRoute::_('index.php?option=com_users&task=profile.userFacebookSave&user_id=' . (int) $this->data->id);?>"
						method="post" id="modal-form">
						<h4 class="form-title">Link with your Facebook account</h5>

							<div class="field-placey form-user-email text-center">
								<input type="text" name="jform[facebook]" v-model="facebook"
									placeholder="https://www.facebook.com/xxxxxx" required>
								<label for="facebook">Your Facebook profile URL</label>
							</div>

							<!-- <button class="btn-white btn" :class="{'form-download-now-disabled':firstRegisterUser, 'form-download-now':!firstRegisterUser}" type="submit" :disabled="firstRegisterUser" >Download Now</button> -->
							<button class="btn-white btn btn-block" type="submit">Save</button>

					</form>
				</div>
			</div>
		</div>
	</div>

</div>


<script>
	var userGender = <?php echo json_encode($this->data->gender); ?>;
	var userRedemptionNum = <?php echo json_encode($this->data->redemption_num); ?>;
	var userDobRedemptionNum = <?php echo json_encode($this->data->dob_redemption_num); ?>;
	var userRace = <?php echo json_encode($this->data->race); ?>;
	var companyIndustry = <?php echo json_encode($this->company->industry); ?>;
	var companyClassification = <?php echo json_encode($this->company->classification); ?>;
	var companyImage = <?php echo json_encode($this->company->business_card); ?>;
	var userFacebook = <?php echo json_encode($this->data->facebook); ?>;
	var userWhatsapp = <?php echo json_encode($this->data->whatsapp); ?>;
	var countryCode = <?php echo json_encode($mobileNo[0]); ?>;
	var companyMobileCountryCode = <?php echo json_encode($companyMobileNo[0]); ?>;
	var calProfileComplete = <?php echo json_encode($this->calProfileComplete); ?>;
	var originalEmail = <?php echo json_encode($this->data->email); ?>;
	var userWhatsappSaveUrl = <?php echo json_encode($userWhatsappSaveUrl); ?>;
	
	if(countryCode == ""){
		countryCode= "+60";
	}
	if(companyMobileCountryCode == ""){
		companyMobileCountryCode= "+60";
	}

	var app = new Vue({
		el: '#app',
		data: {
			userRedemptionNum : userRedemptionNum,
			userDobRedemptionNum : userDobRedemptionNum,
			editProfileStatus: false,
			editCompanyStatus: false,
			checkImgSize : true,
			checkBusinessCard: true,
			checkImgPercent: true,
			pickedGender: userGender,
			pickedIndustry: companyIndustry,
			pickedClassification: companyClassification,
			pikedImage : companyImage,
			pickedRace : userRace,
			countryCode: ["+60", "+65", "+86", "+852", "+91"],
			mobileCountryCode: countryCode,
			companyMobileCountryCode: companyMobileCountryCode,
			showImage: false,
			showProgress: false,
			facebook: userFacebook,
			whatsapp: userWhatsapp,
			calProfileComplete :calProfileComplete,
			emailChangedStatus : 0,
			percent: 0,
			industry: [{
					name: 'Advertiser/Marketer',
					value: 'Advertiser/Marketer'
				}, {
					name: 'Creative agency',
					value: 'Creative agency'
				}, {
					name: 'Digital agency',
					value: 'Digital agency'
				}, {
					name: 'Media agency',
					value: 'Media agency'
				}, {
					name: 'Media owner',
					value: 'Media owner'
				},{
				name: 'PR agency',
				value: 'PR agency'
			}, {
				name: 'Production house',
				value: 'Production house'
			}],
			classification: [{
				name: 'Executive',
				value: 'Executive'
			}, {
				name: 'Manager',
				value: 'Manager'
			}, {
				name: 'Director',
				value: 'Director'
			}, {
				name: 'C-Suite',
				value: 'C-Suite'
			}],
			race: [{
				name: 'Malay',
				value: 0
			}, {
				name: 'Chinese',
				value: 1
			}, {
				name: 'Indian',
				value: 2
			}, {
				name: 'Others',
				value: 3
			}]
		},
		mounted: function () {
			jQuery("#datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeYear: true,
				changeMonth: true,
				yearRange: "-100:-18",
			});

			if (companyImage != "") {
				this.showImage = true;
				this.percent = 100;
				this.checkImgPercent = true;
				var progress_bar_id = "#progress-wrp";

				jQuery(progress_bar_id + " .progress-bar").css("width", +this.percent + "%");
				jQuery(progress_bar_id + " .status").text(this.percent + "%");
				jQuery('#business-card').css('background-image','url(".'+this.pikedImage+'")');
			}



		},
		updated: function () {
			jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
		},
		methods: {
			readUrl: function(input) {
				input = jQuery("#imgInp")[0];
				_this = this;
				_this.percent = 0;
				_this.showProgress = true;
				var progress_bar_id = "#progress-wrp";
				jQuery(progress_bar_id + " .progress-bar").css("width", +_this.percent + "%");
				jQuery(progress_bar_id + " .status").text(_this.percent + "%");

				if (input.files && input.files[0]) {
					if (input.files[0].size >= 2376496) {

						_this.checkImgSize = false;

					}else{
						var file = input.files[0];
						var xhr = new XMLHttpRequest();
						xhr.upload.addEventListener('progress', onprogressHandler, false);
						xhr.open('POST', '/upload/uri', true);
						xhr.send(file); // Simple!

						function onprogressHandler(evt) {
							_this.percent = Math.ceil(evt.loaded/evt.total*100);

							console.log('Upload progress: ' + _this.percent + '%');
							jQuery(progress_bar_id + " .progress-bar").css("width", +_this.percent + "%");
							jQuery(progress_bar_id + " .status").text(_this.percent + "%");
						}
						var reader = new FileReader();

						reader.onload = function (e) {
							console.log(e.target.result);
							_this.pikedImage = e.target.result;
							jQuery('#business-card').css('background-image', 'url("'+_this.pikedImage+'")');
							// jQuery('#business-card').attr('src', e.target.result);
						}

						reader.readAsDataURL(input.files[0]);
						_this.showImage = true;
						_this.showImage = true;
						_this.checkImgSize = true;
					}
				}
			},
			emailOnChanged: function(){
				var email = jQuery('#company-email').val();
				if (email != originalEmail) {
					this.emailChangedStatus = 1;
				}

			},
			updateWhatsapp: function(){
				_this = this;
				jQuery.ajax({
					url: userWhatsappSaveUrl,
					type: 'post',
					data: {
					'whatsapp': 1,
					},
					success: function (result) {
					console.log("success",result);
					if (result) {
						swal("Profile saved.", "", "success", {
						button: {
							text: "Done",
								className: "btn btn-pink",
						},
						});
						_this.whatsapp = 1;
					}


					},
					error: function () {
					// console.log('fail');
					}
				});
				
			}
		}
	})

</script>
