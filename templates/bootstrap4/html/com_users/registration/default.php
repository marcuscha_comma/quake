<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

$getReferralCode = htmlspecialchars($_GET["ref"]) == ""?$_COOKIE["referralCode"]:htmlspecialchars($_GET["ref"]);
$getGuestUniqueKey = $_COOKIE["_guk"]?$_COOKIE["_guk"]:0;
$cookie_name = "_guk";
setcookie($cookie_name, 0, time() + (3600*2), "/");

if ($getReferralCode != "") {
	// echo "yes";
	$db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$db->setQuery('SELECT rs.hits,u.name FROM #__cus_qperks_referrals_session rs join #__users u on u.id = rs.user_id WHERE referral_code = "'.$getReferralCode  .'"');
	$hits = $db->loadRow();

	$fields = array(
		$db->quoteName('hits') . ' = ' . ($hits[0] + 1 )
	);
	$conditions = array(
		$db->quoteName('referral_code') . ' = "'.$getReferralCode .'"'
	);
	$query->update($db->quoteName('#__cus_qperks_referrals_session'))->set($fields)->where($conditions);
	$db->setQuery($query);

	$result = $db->execute();
	$referral_code =$getReferralCode;
	$referral_user =$hits[1];
}
$checkEmailExist = JRoute::_('index.php?option=com_users&task=registration.checkEmailExist');

?>
<div id="app">
	<div class="mt-40 row justify-content-center text-center">
		<div class="col-xl-6 col-lg-8 col-md-10 ">
			<h1 class="f-30">Registration</h1>
			<div class="f-20 font-brand">
				<?php if($referral_user){ ?>
					You're referred by <b class="text-highlight"><?php echo $referral_user;?></b>.<br />
				<?php } ?>
				Please fill in your profile details as per below:
			</div>


			<div class="step-progress">
				<div :class="{active:showStep==1}">
					Step <span>01</span>
				</div>
				<div :class="{active:showStep==2}">
					Step <span>02</span>
				</div>
				<div :class="{active:showStep==3}">
					Step <span>03</span>
				</div>
			</div>

			<form id="member-registration" @submit="checkFormValidate" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="form-validate club-form" enctype="multipart/form-data" novalidate>

				<div v-show="showStep == 1" class="">
					<div class="mb-4">
						<select v-model="pickedIndustry" name="jform[industry]" id="industry" class="selectpicker white-dropdown" :class="{'check-validate': !checkIndustry}" data-width="100%" required>
							<option value="">Choose your type of profession</option>
							<option v-for="item in industry" :value="item.value">{{item.name}}</option>
						</select>
						<div class="form-text form-error" v-if="!checkIndustry">Please choose your type of profession</div>
						<div class="form-text form-error" v-if="pickedIndustry == 'None of the above'">Hey there, please note that the Quake Club membership is only open to professionals in the advertising and marketing industry. Thank you.</div>
					</div>


					<div class="mb-4">
						<select name="jform[classification]" id="classification" class="selectpicker white-dropdown" :class="{'check-validate': !checkClassification}" data-width="100%" required>
							<option value="">Choose your job level</option>
							<option v-for="item in classification" :value="item.value">{{item.name}}</option>
						</select>
						<div class="form-text form-error" v-if="!checkClassification">Please choose your job level</div>
					</div>


					<div class="field-placey form-group">
						<div class="form-text form-error" v-if="!checkDesignation">Please fill in your job designation</div>
						<input autocomplete="quake" id="designation" class="form-control" :class="{'check-validate': !checkDesignation}" name="jform[designation]" type="text" placeholder="Key in your designation" required>
						<label for="designation">Designation</label>
					</div>

				</div>

				<div v-show="showStep == 2" class="">
					<div class="field-placey form-group">
						<div class="form-text form-error" v-if="!checkComEmail">Please fill in your company’s email address</div>
						<div class="form-text form-error" v-if="!checkEmailExist">This email address is already registered.</div>
						<input autocomplete="quake" v-model="companyEmail" id="com_email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" :class="{'check-validate': !checkComEmail}" name="jform[email1]" type="email" id="email" placeholder=" " required>
						<label for="com_email">Company’s email address</label>
					</div>
				</div>

				<div class="" v-show="showStep == 3">
					<div class="field-placey form-group">
						<div class="form-text form-error" v-if="!checkUserName">Please fill in your name</div>
						<input autocomplete="quake" id="user_name" v-model="userName" class="form-control" :class="{'check-validate': !checkUserName}" name="jform[name]" type="text" placeholder=" " required>
						<label for="name">Name</label>
					</div>

					<div class="mb-4">
						<div class="row">
							<div class="col-md-4 col-5">
								<select v-model="userCountryCode" name="jform[country_code]" id="country_code" class="selectpicker white-dropdown" :class="{'check-validate': !checkCountryCode}"
									data-width="100%" required>
									<option v-for="item in country_code" :value="item">{{item}}</option>
								</select>
							</div>
							<div class="col-md-8 col-7">
								<div class="field-placey">
									<div class="form-text form-error" v-if="!checkMobile">Please fill in your mobile number</div>
									<div class="form-text form-error" v-if="!checkMobileValid">Please fill in a valid mobile number</div>
									<input autocomplete="quake" v-model="userContactNumber" class="form-control" :class="{'check-validate': !checkMobile}" id="mobile" name="jform[contact_number]" type="tel" placeholder=" " required>
									<label for="mobile">Mobile number</label>
								</div>
							</div>
						</div>
					</div>

					<div class="field-placey form-group">
						<div class="form-text form-error" v-if="!checkCompany">Please fill in your company’s name</div>
						<input v-model="vcompname" autocomplete="quake" class="form-control" :class="{'check-validate': !checkCompany}" name="jform[company]" id="company" type="text" placeholder=" " required>
						<label for="company">Company’s name</label>
					</div>

					<div class="field-placey form-group">
						<div class="form-text form-error" v-if="!checkDob">Please choose your date of birth</div>
						<input autocomplete="quake" v-model="userDob" @click="dobChanged()" class="form-control" :class="{'check-validate': !checkDob}" name="jform[dob]" id="datepicker" type="text" placeholder=" " required>
						<label for="datepicker">Birthday</label>
						<i class="form-icon right fas fa-calendar"></i>
					</div>

					<input style="opacity: 0;position: absolute;"/><!--invisible to autofill username-->

					<input type="hidden" name="jform[username]" id="jform_username" value="" class="validate-username">
					<div class="field-placey form-group">
						<div class="form-text text-muted" >Must contain at least one uppercase letter, one lowercase letter and one number, and at least 8 or more characters. Password is case sensitive.</div>
						<div class="form-text form-error" v-if="!checkPass1">Please create a password</div>
						<i class="far fa-eye-slash toggle-password" ></i>
						<input v-model="vpass1" class="validate-password required form-control" :class="{'check-validate': !checkPass1}" name="jform[password1]" id="jform_password1" type="password" autocomplete="new-password" placeholder=" " size="30" maxlength="99" required="required" aria-required="true" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$">
						<label id="jform_password1-lbl" for="jform_password1" class="hasPopover required">Password</label>
					</div>

					<div class="field-placey form-group">
						<!-- <div class="form-text text-muted" >Must contain at least one number and one alphabet, and at least 8 or more characters.</div> -->
						<div class="form-text form-error" v-if="!checkPass2">Please reconfirm your password</div>
						<div class="form-text form-error" v-if="!checkPass2Confirm">New password and confirm password do not match, please try again.</div>
						<i class="far fa-eye-slash toggle-password" ></i>
						<input v-model="vpass2" class="validate-password required form-control" :class="{'check-validate': !checkPass2}" name="jform[password2]" id="jform_password2" type="password" autocomplete="new-password" placeholder=" " size="30" maxlength="99" required="required" aria-required="true" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$">
						<label id="jform_password2-lbl" for="jform_password2" class="hasPopover required">Confirm password</label>
					</div>

					<div class="custom-control text-left custom-checkbox mt-4 remember-me" style="margin-bottom:10px !important;">

						<input type="checkbox" class="custom-control-input" id="agree-terms" v-model="newsletter" name="jform[newsletter]">
						<label class="custom-control-label" for="agree-terms">Subscribe to Quake newsletter</label>
					</div>

				</div>

				<!-- <div class="control-group form-group">
					<div class="control-label">
						<label id="jform_email1-lbl" for="jform_email1" class="hasPopover required invalid" title=""
							data-content="Enter your email address." data-original-title="Email Address">
							Email Address<span class="star">&nbsp;*</span></label>
					</div>
					<div class="controls">
						<input type="email" name="jform[email1]" class="validate-email required form-control invalid"
							id="jform_email1" value="" size="30" autocomplete="off" required="required" aria-required="true"
							aria-invalid="true"> </div>
				</div>
				<div class="control-group form-group">
					<div class="control-label">
						<label id="jform_email2-lbl" for="jform_email2" class="hasPopover required" title=""
							data-content="Confirm your email address." data-original-title="Confirm Email Address">
							Confirm Email Address<span class="star">&nbsp;*</span></label>
					</div>
					<div class="controls">
						<input type="email" name="jform[email2]" class="validate-email required form-control" id="jform_email2"
							value="" size="30" required="required" aria-required="true" autocomplete="off"> </div>
				</div> -->
				<div class="row">
					<div class="col-6">
						<button type="button" class="btn btn-pink btn-block validate" @click="backToPrevious()">
							Back
						</button>
					</div>
					<div class="col-6">
						<button v-if="showStep != 3" type="button" class="btn btn-pink btn-block validate" @click="goToNext()">
							Next
						</button>
						<button v-if="showStep == 3" type="submit" class="btn btn-pink btn-block validate">
							<?php echo JText::_('JREGISTER'); ?>
						</button>
					</div>
					<input type="hidden" name="option" value="com_users" />
					<input type="hidden" name="jform[referral_code]" v-model="vReferralCode" />
					<input type="hidden" name="jform[guest_unique_key]" v-model="guestUniqueKey" />
					<input type="hidden" name="task" value="registration.register" />
				</div>
				<?php echo JHtml::_('form.token'); ?>
			</form>

		</div>
	</div>

</div>

<script type="text/javascript">
    var checkEmailExistUrl = <?php echo json_encode($checkEmailExist); ?>;
    var referralCode = <?php echo json_encode($referral_code); ?>;
    var guestUniqueKey = <?php echo json_encode($getGuestUniqueKey); ?>;
    var companyNameFromPhp = <?php echo json_encode($company_name); ?>;

    var app = new Vue({
        el: '#app',
        data: {
			pickedIndustry : '',
			showStep : 1,
			checkIndustry : true,
			checkClassification : true,
			checkDesignation : true,
			checkComEmail : true,
			checkUserName : true,
			checkCountryCode : true,
			checkMobile : true,
			checkMobileValid : true,
			checkCompany : true,
			checkDob : true,
			checkPass1 : true,
			checkPass2 : true,
			checkPass2Confirm : true,
			checkEmailExist : true,
			vReferralCode : "",
			guestUniqueKey : guestUniqueKey,
			industry: [{
					name: 'Advertiser/Marketer',
					value: 'Advertiser/Marketer'
				}, {
					name: 'Creative agency',
					value: 'Creative agency'
				}, {
					name: 'Digital agency',
					value: 'Digital agency'
				}, {
					name: 'Media agency',
					value: 'Media agency'
				}, {
					name: 'Media owner',
					value: 'Media owner'
				},{
				name: 'PR agency',
				value: 'PR agency'
			}, {
				name: 'Production house',
				value: 'Production house'
			}, {
					name: 'None of the above',
					value: 'None of the above'
				}
			],
		classification : [{
			name:'Executive',
			value:'Executive'},{
			name:'Manager',
			value:'Manager'},{
			name:'Director',
			value:'Director'},{
			name:'C-Suite',
			value:'C-Suite'}
			],
			companyEmail : "",
			userName : "",
			userDob : "",
			userCountryCode : "+60",
			userContactNumber : "",
			vcompname : "",
			vpass1 : "",
			vpass2 : "",
			newsletter : 1,
			country_code : ["+60","+62","+65","+86","+852","+91"]
        },
        mounted: function () {
            _this = this;
			jQuery('meta[property=og\\:image]').attr('content', 'http://quake.com.my//images/club/sharer.jpg');
			jQuery('<meta property="og:title" content="You have been referred to Quake Club!">').insertBefore('meta[property=og\\:image]');
			jQuery('<meta property="og:description" content="Quake Club is Marketing Professionals club where members can participate in exciting activities and earn Q-Perks. Awesome rewards await, the more Q-Perks you accumulate!">').insertBefore('meta[property=og\\:image]');
			jQuery( "#datepicker" ).datepicker({
				dateFormat: 'dd-mm-yy',
				changeYear: true,
				changeMonth: true,
				yearRange: "-100:-18",
				defaultDate: '01-01-2001',
				onSelect:function(selectedDate, datePicker) {
					_this.userDob = selectedDate;
				}
				});

            // this.selectOnChanged();
			var now = new Date();
			var time = now.getTime();
			var expireTime = time + 1000*3600*3;
			now.setTime(expireTime);
			// console.log(referralCode);
			if (referralCode!=null) {
				document.cookie = "referralCode="+referralCode+"; expires=" + now.toUTCString()+';path=/';
			}
			if (document.cookie.split(';').filter(function(item) {
				return item.trim().indexOf('referralCode=') == 0
			}).length) {
				document.cookie.split(";").forEach(function(item){
					if (item.trim().indexOf('referralCode=')==0) {
						tmp = item.split("=");
						_this.vReferralCode = tmp[1];
					}

				});
			}
        },
        updated: function () {
            jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
			checkEmail : function(email){
				console.log("check");
				
            _this = this;
			var com_email = document.getElementById('com_email');
			if (email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")) {
				jQuery.ajax({
                url: checkEmailExistUrl,
                type: 'post',
                data: { email: email },
                success: function (result) {
					if (result != 0) {
						if (result==1) {
							_this.checkEmailExist = true;
							_this.showStep++;
						}else{
							dataResult = JSON.parse(result);
							console.log(dataResult.data[0]);
							if (dataResult.data[0].mobile != null) {
								var tmp = dataResult.data[0].mobile.split("-");
								_this.userCountryCode = tmp[0];
								_this.userContactNumber = tmp[1];
							}
							_this.userName = dataResult.data[0].name;
							_this.vcompname = dataResult.data[0].company_name;
							_this.userDob = dataResult.data[0].dob;
							_this.checkEmailExist = true;
							_this.checkComEmail = true;
							_this.showStep++;
						}
					}else{
						_this.userName = "";
						_this.userDob = "";
						_this.userCountryCode = "+60";
						_this.userContactNumber = "";
						_this.checkEmailExist = false;
						_this.checkComEmail = true;

					}

                },
                error: function () {
                //   console.log('fail');
                }
              });
			}else if(email != ""){
				_this.checkComEmail = false;
			}else{
				_this.checkComEmail = false;
			}
            },
            loadMore :function(loadMoreNumber){
                this.loadMoreNumber =loadMoreNumber + 6;
            },
            goToLink :function(link){
                if (link == "/happening/230-cny-8-huat-facts") {
                    window.location.href = '/cny-8-huat-facts';
                }else{
                    window.location.href = link;
                }
            },
			backToPrevious :function(){
				if (this.showStep == 1) {
					window.location.href = "./notice";
				}else{
					this.showStep--;
				}
			},
			checkFormValidate :function(e){
				var user_name = document.getElementById('user_name');
				var country_code = document.getElementById('country_code');
				var mobile = document.getElementById('mobile');
				var company = document.getElementById('company');
				var datepicker = document.getElementById('datepicker');
				var jform_password1 = document.getElementById('jform_password1');
				var jform_password2 = document.getElementById('jform_password2');

				this.checkUserName = user_name.checkValidity();
				this.checkCountryCode = country_code.checkValidity();
				this.checkMobile = mobile.checkValidity();
				this.checkCompany = company.checkValidity();
				this.checkDob = datepicker.checkValidity();
				this.checkPass1 = jform_password1.checkValidity();
				this.checkPass2 = jform_password2.checkValidity();
				if (this.vpass1 != this.vpass2 && this.checkPass2 ) {
					this.checkPass2Confirm = false;
				}else{
					this.checkPass2Confirm = true;
				}
				if (!this.userContactNumber.match(/.{9,16}/) && this.userContactNumber!="") {
					this.checkMobileValid = false;
				}else{
					this.checkMobileValid = true;
				}
				// console.log("asd");

				if (
					this.userName != "" &&
					this.userDob != "" &&
					this.userCountryCode != "" &&
					this.userContactNumber != "" && this.userContactNumber.match(/.{9,16}/) &&
					this.vcompname != "" &&
					this.vpass1 != "" && this.vpass1.match(/^(?=.*\d)(?=.*[a-z]).{8,}/) &&
					this.vpass2 != "" && this.vpass2.match(/^(?=.*\d)(?=.*[a-z]).{8,}/) && this.vpass1 == this.vpass2
				) {
					// console.log("asdasads");

					return true;
				}else{
					// console.log("userName",this.userName != "");
					// console.log("userDob",this.userDob != "");
					// console.log("userCountryCode",this.userCountryCode != "");
					// console.log("userContactNumber",this.userContactNumber != "");
					// console.log("vcompname",this.vcompname != "");
					// console.log("vpass2",this.vpass2 != "");
					// console.log("vpass1",this.vpass1.match(/^(?=.*\d)(?=.*[a-z]).{8,}/));
					// console.log("vpass2",this.vpass2.match(/^(?=.*\d)(?=.*[a-z]).{8,}/));
					// console.log("this.vpass1 == this.vpass2",this.vpass1 == this.vpass2);

				}
				e.preventDefault();
			},
			goToNext :function(){
				var designation = document.getElementById('designation');
				var industry = document.getElementById('industry');
				var classification = document.getElementById('classification');

				switch (this.showStep) {
					case 1:
						this.checkIndustry = industry.checkValidity();
						this.checkClassification = classification.checkValidity();
						this.checkDesignation = designation.checkValidity();
						if (this.pickedIndustry != 'None of the above' && classification.checkValidity() && designation.checkValidity() && industry.checkValidity()) {
							this.showStep++;
						}
						break;
					case 2:
						this.checkComEmail = com_email.checkValidity();
						this.checkEmail(this.companyEmail);
						break;

					default:
						break;
				}
			},
			dobChanged :function(){
				// console.log(this.userDob);

			}
        }
    })
</script>
<script type="text/javascript">
	jQuery(".toggle-password").click(function() {

		jQuery(this).toggleClass("fa-eye fa-eye-slash");
		var input = jQuery(this).next();
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});
</script>
