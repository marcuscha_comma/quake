<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>

<div class="row justify-content-center">
	<div class="col-xl-6 col-lg-8 col-md-10">
		<div class="club-logo mx-auto mb-4">Quake Club</div>

		<div class="card rounded-card bg-grey mb-4 club-shadow">
			<div class="card-body">

				<h2 class="f-30 mb-3 mt-0 text-center">Thank you for signing up</h2>
				<p>You can now start collecting Q-Perks to redeem amazing rewards!</p>

				<a href="./login"><button class="btn btn-pink btn-wide">Let's start</button></a>

			</div>
		</div>


	</div>
</div>

<script>
	jQuery( document ).ready(function() {
		jQuery( "#user-top-banner" ).hide();
	});
</script>
