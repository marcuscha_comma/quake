<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$menu = $app->getMenu();
$menu_items = $menu->getItems('menutype', 'top');
$menu_items_2 = $menu->getItems('menutype', 'quake-club-top');
$this->language = $doc->language;
$this->direction = $doc->direction;


// Getting params from template
$params = $app->getTemplate(true)->params;
$userToken = JSession::getFormToken();

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
$isMobile = "";
$checkEmailUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.checkEmailExist');
$createClientUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.createClient');

if ($menu->getActive()->alias == "quake-club-dashboard") {
  header("Location: ".$this->baseurl."/quake-club/dashboard");
}

if ($task == "edit" || $layout == "form") {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}

// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap.min.css?v=0620');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/fontawesome-all.css?v=0620');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css?v=0620');
//$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap-select.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/owl.carousel.min.css?v=0620');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/owl.theme.default.min.css?v=0620');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/lity/dist/lity.css?v=0620');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/light-slider/dist/css/lightslider.css?v=0620');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap-select.min.css?v=0620');
$doc->addStyleSheet("//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css?v=0620");
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/style.css?v=0708.32');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/modal.css?v=0620');

// Add scripts
JHtml::_('jquery.framework');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/popper.min.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/bootstrap.min.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/vue.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/jquery.matchHeight.js?v=0620');
//$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/bootstrap-select.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/lity/dist/lity.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/chart-js/dist/Chart.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/light-slider/dist/js/lightslider.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/chartjs-plugin-labels.js?v=0620');
$doc->addScript('https://unpkg.com/sweetalert/dist/sweetalert.min.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/bootstrap-select.min.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/script.js?v=0620');
$doc->addScript("https://code.jquery.com/ui/1.12.1/jquery-ui.js?v=0620");


// Adjusting content width
if ($this->countModules('sidebar-left') && $this->countModules('sidebar-right')) {
    $span = "col-md-6";
} elseif ($this->countModules('sidebar-left') && !$this->countModules('sidebar-right')) {
    $span = "col-md-9";
} elseif (!$this->countModules('sidebar-left') && $this->countModules('sidebar-right')) {
    $span = "col-md-9";
} else {
    $span = "col-md-12";
}

//get all slider
$db_cus_sliders_banner    = JFactory::getDBO();
$query_cus_sliders_banner = $db_cus_sliders_banner->getQuery( true );
$query_cus_sliders_banner
  ->select( '*' )
  ->from( $db_cus_sliders_banner->quoteName( '#__cus_sliders_banner' ) )
  ->where($db_cus_sliders_banner->quoteName('state')." > 0")
  ->order( 'ordering asc' );
$db_cus_sliders_banner->setQuery( $query_cus_sliders_banner );
$cus_sliders_banner = $db_cus_sliders_banner->loadObjectList();

//get all slider highlighs
$db_cus_sliders_highlight    = JFactory::getDBO();
$query_cus_sliders_highlight = $db_cus_sliders_highlight->getQuery( true );
$query_cus_sliders_highlight
  ->select( '*' )
  ->from( $db_cus_sliders_highlight->quoteName( '#__cus_sliders_highlight' ) )
  ->where($db_cus_sliders_highlight->quoteName('state')." > 0")
  ->order( 'id asc' );
$db_cus_sliders_highlight->setQuery( $query_cus_sliders_highlight );
$cus_sliders_highlight = $db_cus_sliders_highlight->loadObjectList();

//get all slider highlighs
$db_cus_banner_v1ewership    = JFactory::getDBO();
$query_cus_banner_v1ewership = $db_cus_banner_v1ewership->getQuery( true );
$query_cus_banner_v1ewership
  ->select( '*' )
  ->from( $db_cus_banner_v1ewership->quoteName( '#__cus_banner_v1ewership' ) )
  ->order( 'id asc' );
$db_cus_banner_v1ewership->setQuery( $query_cus_banner_v1ewership );
$cus_banner_v1ewership = $db_cus_banner_v1ewership->loadObjectList();

//get all slider
$db_cus_qperks_sliders_banner    = JFactory::getDBO();
$query_cus_qperks_sliders_banner = $db_cus_qperks_sliders_banner->getQuery( true );
$query_cus_qperks_sliders_banner
  ->select( '*' )
  ->from( $db_cus_qperks_sliders_banner->quoteName( '#__cus_qperks_sliders_banner' ) )
  ->where($db_cus_qperks_sliders_banner->quoteName('state')." > 0")
  ->order( 'ordering asc' );
$db_cus_qperks_sliders_banner->setQuery( $query_cus_qperks_sliders_banner );
$cus_qperks_sliders_banner = $db_cus_qperks_sliders_banner->loadObjectList();

$db_cus_cart    = JFactory::getDBO();
$db_cus_cart->setQuery('SELECT count(id) FROM #__cus_qperks_cart where user_id = '. $user->id );
$cart_num            = $db_cus_cart->loadResult();

//detect mobile or desktop
if (stristr($_SERVER['HTTP_USER_AGENT'],'mobi')!==FALSE) {
  $isMobile = True;
}else{
  $isMobile = False;
}
$updateUserShareArticlePointUrl = JRoute::_('index.php?option=com_quake_club_qperks&task=qperksuserpoint.addPoint');

// echo $this->params->get('favicon');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <jdoc:include type="head" />
        <link rel="shortcut icon" href="<?php echo JUri::root(true) . htmlspecialchars($this->params->get('favicon'), ENT_COMPAT, 'UTF-8'); ?>" />
        <!--[if lt IE 9]>
                <script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
        <![endif]-->
    </head>
    <body class="preload">
      <header class="navbar navbar-expand-xl navbar-light">
        <?php if ($this->countModules('position-1')) : ?>
            <!-- Logo start -->
            <a class="brand pull-left" href="<?php echo $this->baseurl; ?>/">
              <?php echo '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename .'" />'; ?>
            </a>
            <!-- Logo End -->

            <!--before login-->
            <?php if (JFactory::getUser()->guest) { ?>
            <a class="mobile-quake d-xl-none" href="<?php echo $this->baseurl ?>/login">
              <span class="join-us">Join us now</span>
              <img src="<?php echo $this->baseurl ?>/images/2020/club/quake-club-white.png" alt="Quake Club"/>
            </a>
            <!--////////////-->
            <?php }else{ ?>
              <!--after login-->
              <div class="logged-in d-xl-none ml-auto mr-3">
                <a href="<?php echo $this->baseurl ?>/quake-club/my-carts" class="circle-icon cart mr-3">
                  <?php if ($cart_num > 0) {
                    echo "<span class='cart-quantity'>". $cart_num ."</span>";
                  }  ?>
                  <i class="fas fa-shopping-cart"></i>
                </a>

                <div class='dropdown d-inline-block user-dropdown'>
                  <a href="#" class="circle-icon" id='userDropdown2' data-toggle='dropdown' aria-haspopup='true'
                    aria-expanded='false'>
                    <i class="fas fa-user"></i>
                  </a>
                  <div class='dropdown-menu' aria-labelledby='userDropdown2'>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/dashboard'>Dashboard</a>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/rewards-catalogue'>Rewards Catalogue</a>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/user-profile/profile'>My Account</a>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/friend-referral'>Friend
                      Referral</a>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/points-history'>Points History</a>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/redemption-list'>Redemption History</a>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/change-password'>Change Password</a>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/frequently-asked-questions'>Frequently Asked Questions (FAQ)</a>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/terms-and-conditions'>Terms & Conditions</a>
                    <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/contact-us'>Contact Us</a>
                    <a class='dropdown-item'
                      href='index.php?option=com_users&task=user.logout&<?php echo $userToken ?>=1'>Logout</a>
                  </div>
                </div>
              </div>
            <!--////////////-->
            <?php } ?>





            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <div id="nav-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              </div>
            </button>
            <!-- Menu List Start -->
            <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="menu navbar-nav ml-auto">
            <?php $i=0; foreach ($menu_items as $key => $menu_item_value) {

              if ($menu_item_value->level == 1) {
                if ($menu_item_value === end($menu_items)) {
                  echo "<li>";
                  echo "
                    <div class='quake-nav'>
                      <div class='logo-side'>";
                      if (JFactory::getUser()->guest) {
                        echo "<span class='join-us'>Join us now</span>";
                      }
                        echo "<a href=". $this->baseurl.'/'.$menu_item_value->alias .">
                          <img src='".$this->baseurl."/images/2020/club/quake-club-white.png' alt='Quake Club'/>
                        </a>
                      </div>";
                      if (!JFactory::getUser()->guest) {
                        echo "<div class='logged-in'>
                        <a href='".$this->baseurl."/quake-club/my-carts' class='circle-icon cart'>";
                        if ($cart_num > 0) {
                          echo "<span class='cart-quantity'>". $cart_num ."</span>";
                        }

                          echo "<i class='fas fa-shopping-cart'></i>
                        </a>

                        <div class='dropdown d-inline-block user-dropdown'>
                          <button class='btn dropdown-toggle' type='button' id='userDropdown' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                            <span class='circle-icon'><i class='fas fa-user'></i></span>
                            <span class='username'>".$user->name."</span>
                          </button>
                          <div class='dropdown-menu' aria-labelledby='userDropdown'>
                            <a class='dropdown-item' href='".$this->baseurl."/user-profile/profile'>My Account</a>
                            <a class='dropdown-item' href='".$this->baseurl."/quake-club/friend-referral'>Friend Referral</a>
                            <a class='dropdown-item' href='".$this->baseurl."/quake-club/points-history'>Points History</a>
                            <a class='dropdown-item' href='".$this->baseurl."/quake-club/redemption-list'>Redemption History</a>
                            <a class='dropdown-item' href='".$this->baseurl."/quake-club/change-password'>Change Password</a>
                            <a class='dropdown-item' href='".$this->baseurl."/quake-club/contact-us'>Contact Us</a>
                            <a class='dropdown-item' href='index.php?option=com_users&task=user.logout&" . $userToken . "=1'>Logout</a>
                          </div>
                        </div>

                      </div>";
                      }
                      echo "
                    </div>
                  ";
                  echo "</li>";
                }else{
                  echo "<li>";
                  echo "<a class='nav-link' href='". $this->baseurl.'/'.$menu_item_value->alias ."'>" . $menu_item_value->title . "</a>";
                  echo "</li>";
                }
              }

              ?>
            <?php }; ?>
            </ul>
              <!-- <jdoc:include type="modules" name="position-1" style="none"/> -->
            </div>

            <!-- Menu List End -->
        <?php endif; ?>
      </header>

        <div class="body" id="<?php echo $menu->getActive()->alias;?>">
            <div class="content">
                <!-- Head Banner Start -->
                <div class="page-banner-wrapper">
                  <jdoc:include type="modules" name="top-banner" style="none" />
                </div>
                <!-- quake-club-slider start -->
                <?php
                if ($menu->getActive()->menutype == "quake-club-top" || $menu->getActive()->title == 'Quake Club') :?>
                <!-- Top Menu -->
                <div class="club-main-menu">
                  <ul class="nav">
                    <?php $i=0; foreach ($menu_items_2 as $key => $menu_item_value) {

                      if ($menu_item_value->level == 2 && $menu_item_value->getParams()['menu_show'] == 1) {
                          if ($menu_item_value->alias == $menu->getActive()->alias) {
                            echo "<li class='nav-item active'>";
                          }else{
                            echo "<li class='nav-item'>";
                          }

                          echo "<a class='nav-link' href='". $this->baseurl.'/quake-club/'.$menu_item_value->alias ."'>" . $menu_item_value->title . "</a>";
                          echo "</li>";
                      }

                      ?>
                    <?php }; ?>
                  </ul>
                </div>

                <!--Birthday Reminder Floating Button-->
                <!--<div class="birthday-reminder">
                  <div class="toggle">

                  </div>

                  <div class="message">
                    <span>Happy Birthday!</span> 200 Q-Perks from us once you redeem any items from the Rewards Catalogue.
                  </div>
                </div>-->

                  <?php if ($menu->getActive()->title == "Dashboard" || $menu->getActive()->alias == 'quake-club-dashboard') :?>
                  <div class="owl-carousel owl-theme" id="club-banner">
                    <?php foreach ($cus_qperks_sliders_banner as $key => $cus_qperks_sliders_banner_value) {
                          /**
                           * link_type value meaning
                           * 0 = Don't link
                           * 1 = Url Address
                           * 2 = Youtube
                           */
                          ?>
                    <div class="item-video">
                      <?php if ($cus_qperks_sliders_banner_value->link_type == 2) {?>

                      <a href="<?php echo $cus_qperks_sliders_banner_value->youtube_link; ?>" data-lity>
                        <div class="banner-image"
                          style="background-image: url('<? echo $cus_qperks_sliders_banner_value->image;?>')">
                          <img src="images/2020/club/club-banner-mobile-guide.png" class="guide-xs" />
                        </div>
                      </a>

                      <?php }elseif ($cus_qperks_sliders_banner_value->link_type == 1) { ?>

                      <a target="<?php echo $cus_qperks_sliders_banner_value->target_window; ?>"
                        href="<?php echo $cus_qperks_sliders_banner_value->url_address; ?>">
                        <div class="banner-image"
                          style="background-image: url('<? echo $cus_qperks_sliders_banner_value->image;?>')">
                          <img src="images/2020/club/club-banner-mobile-guide.png" class="guide-xs" />
                        </div>
                      </a>

                      <?php }elseif ($cus_qperks_sliders_banner_value->link_type == 0) { ?>
                      <div class="banner-image"
                        style="background-image: url('<? echo $cus_qperks_sliders_banner_value->image;?>')">
                        <img src="images/2020/club/club-banner-mobile-guide.png" class="guide-xs" />
                      </div>
                      <?php } ?>
                    </div>
                    <?php } ?>
                  </div>
                  <?php endif; ?>
                <?php endif; ?>
                <!-- quake-club-slider end -->
                <?php if ($menu->getActive() == $menu->getDefault()) :
                ?>
                <div class="modal fade" ref="vuemodal" id="noticeModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-body">
                          <div class="position-relative">
                            <button @click="closeModal" type="button" class="close" data-dismiss="modal"
                              aria-label="Close">
                              <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>

                            <a href="<?php echo $this->baseurl ?>/login" target="_blank">
                              <img src="./images/2020/Quake_popupBanner.png" alt="Sign up now at Quake Club">
                            </a>


                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="owl-carousel owl-theme" id="index-banner">
                      <?php foreach ($cus_sliders_banner as $key => $cus_sliders_banner_value) {
                         /**
                         * link_type value meaning
                         * 0 = Don't link
                         * 1 = Url Address
                         * 2 = Youtube
                         */
                        ?>
                        <div class="item-video">
                          <?php if ($cus_sliders_banner_value->link_type == 2) {?>

                            <a href="<?php echo $cus_sliders_banner_value->youtube_link; ?>" data-lity>
                            <div class="banner-image" style="background-image: url('<? echo $cus_sliders_banner_value->desktop_image;?>')">
                              <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                            </a>

                          <?php }elseif ($cus_sliders_banner_value->link_type == 1) { ?>

                            <a target="<?php echo $cus_sliders_banner_value->target_window; ?>" href="<?php echo $cus_sliders_banner_value->url_address; ?>">
                            <div class="banner-image" style="background-image: url('<? echo $cus_sliders_banner_value->desktop_image;?>')">
                              <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                            </a>

                          <?php }elseif ($cus_sliders_banner_value->link_type == 0) { ?>
                            <div class="banner-image" style="background-image: url('<? echo $cus_sliders_banner_value->desktop_image;?>')">
                              <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                          <?php } ?>
                        </div>
                        <?php } ?>
                  </div>
                <?php endif; ?>

                <div id="inline" style="background:#fff" class="lity-hide">
                Inline content
                </div>
                <!-- Head Banner End -->
                <!-- Slider Highlights Start -->
                <?php if ($menu->getActive() == $menu->getDefault()) : ?>
                <div class="highlight-banner-class">
                  <div class="container">
                    <h2>Top User's Picks</h2>
                    <p>Earn Q-Perks for every share</p>
                    <div class="owl-carousel owl-theme mt-4" id="highlight-banner">
                      <?php foreach ($cus_sliders_highlight as $key => $cus_sliders_highlight_value) { ?>

                          <div class="item highlight-banner-wrapper">
                          <a href="<?php echo $cus_sliders_highlight_value->url_address; ?>">
                            <div class="img-holder" style="background-image: url('<? echo $cus_sliders_highlight_value->image;?>')">
                              <img src="images/2020/card-guide.png" class="placeholder" />
                            </div>
                            <div class="highlight-banner-div">
                              <div class="highlight-banner-tag"><?php echo $cus_sliders_highlight_value->tag; ?></div>
                              <div class="d-none d-lg-block">
                                <h3><?php echo mb_strimwidth("$cus_sliders_highlight_value->title", 0, 30, "…"); ?></h3>
                                <p><?php echo mb_strimwidth("$cus_sliders_highlight_value->description", 0,80, "…"); ?></p>
                              </div>
                              <div class="d-lg-none">
                                <h3><?php echo $cus_sliders_highlight_value->title; ?></h3>
                                <p><?php echo $cus_sliders_highlight_value->description; ?></p>
                              </div>
                            </div>
                          </a>
                          </div>

                      <?php } ?>
                    </div>
                  </div>
                </div>

                <?php endif; ?>
                <!-- Slider Highlights End -->
                <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                    <jdoc:include type="modules" name="banner" style="xhtml" />
                    <?php if ($this->countModules('breadcrumbs')) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <jdoc:include type="modules" name="breadcrumbs" style="xhtml" />
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <?php if ($this->countModules('sidebar-left')) : ?>
                            <div id="sidebar" class="col-md-3">
                                <div class="sidebar-nav">
                                    <jdoc:include type="modules" name="sidebar-left" style="xhtml" />
                                </div>
                            </div>
                        <?php endif; ?>
                        <main id="content" role="main" class="<?php echo $span; ?>">
                            <jdoc:include type="modules" name="position-3" style="xhtml" />
                            <jdoc:include type="message" />
                            <jdoc:include type="component" />
                             <!-- <jdoc:include type="modules" name="position-2" style="none" />  -->
                        </main>
                        <?php if ($this->countModules('sidebar-right')) : ?>
                            <div id="aside" class="col-md-3">
                                <jdoc:include type="modules" name="sidebar-right" style="xhtml" />
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <footer class="btm-footer" role="contentinfo">
            <jdoc:include type="modules" name="viewership" style="none" />
            <jdoc:include type="modules" name="footer" style="none" />

            <div class="footer">
              <div class="container">
                <div class="row mb-4 mb-lg-5 align-items-center">
                  <div class="col-6">
                    <img src="images/2020/astro-media.png" alt="Astro Media Sales" width="95" class="astro-media">
                  </div>
                  <div class="col-6 text-right">
                    <h4 class="upper d-sm-inline-block d-block">Follow us on</h4>
                    <a href="https://www.facebook.com/QuakeMY" target="_blank"><i class="ml-2 fab fa-facebook-square text-highlight"></i></a>
                    <a href="https://www.youtube.com/channel/UCR6P2vsz2ncymL8OCL7CCMw/featured" target="_blank"><i class="ml-2 fab fa-youtube-square text-highlight"></i></a>
                    <a href="https://api.whatsapp.com/send?phone=60126040968&text=Hi,%20send%20me%20the%20monthly%20issue%20of%20Winning%20Partnership%20Series%20by%20MARKETING" target="_blank"><i class="ml-2 fab fa-whatsapp-square text-highlight"></i></a>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4 mb-3 mb-md-0">
                    <ul class="upper list-unstyled mb-0">
                      <li>
                        <a href="./privacy-policy"><h4>Privacy Policy</h4></a>
                      </li>
                      <li>
                        <a href="./privacy-notice"><h4>Privacy Notice</h4></a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-4 mb-4 mb-md-0">
                    <h4 class="upper">Astro Media Sales</h4>
                    <div>
                      <address class="mb-2">
                        Lower Penthouse B-21-1, Northpoint Offices,<br />
                        No. 1 Medan Syed Putra Utara,<br />
                        59200 Kuala Lumpur
                      </address>
                      <a href="./about-quake/get-in-touch" class="bold text-highlight">Be in Touch</a>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <h4 class="upper">Subscribe to Our Newsletter</h4>
                    <p>Be in the know of all things Quake</p>

                    <form class="input-group" action="" id="subscribe-form">
                      <input id="subscribe-email" type="email" class="form-control" placeholder="What's Your Email Address?" aria-label="Email Address" aria-describedby="button-addon2" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                      <div class="input-group-append">
                        <div class="input-group-text" id="loading-icon"></div>
                        <button class="btn btn-link" type="submit" id="button-addon2">Subscribe</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <!-- <div class="container<?php// echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>"> -->
            <div class="container">
              <div class="copyright">
                &copy; <?php echo date('Y'); ?> Astro Quake Rights Reserved.
              </div>
            </div>

            <a id="backTop">
                <i class="fas fa-chevron-up"></i>
            </a>
        </footer>
        <jdoc:include type="modules" name="debug" style="none" />
    </body>


</html>
<script>
  var checkEmailUrl = <?php echo json_encode($checkEmailUrl); ?>;
  var createClientUrl = <?php echo json_encode($createClientUrl); ?>;
  var banner_viewership = <?php echo json_encode($cus_banner_v1ewership); ?>;
  var current_alias = <?php echo json_encode($menu->getActive()->alias); ?>;

  //bind viewership data from db
  jQuery('#viewership-figure-1').html(banner_viewership[0]['block_1_title']);
  jQuery('#viewership-content-1').html(banner_viewership[0]['block_1_description']);

  jQuery('#viewership-figure-2').html(banner_viewership[0]['block_2_title']);
  jQuery('#viewership-content-2').html(banner_viewership[0]['block_2_description']);

  jQuery('#viewership-figure-3').html(banner_viewership[0]['block_3_title']);
  jQuery('#viewership-content-3').html(banner_viewership[0]['block_3_description']);

</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bcd3eb6f160f08f"></script>
<?php
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/owl.carousel.min.js?v=0620');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/custom.js?v=0620.1');
?>

<script>
    var updateUserShareArticlePointUrl = <?php echo json_encode($updateUserShareArticlePointUrl); ?>;

    jQuery( document ).ready(function() {
      jQuery('#noticeModal').modal('show')
    });

    var addthis_config = {
      data_track_clickback: true
    }
    // Alert a message when the user shares somewhere
    function eventHandler(evt) {
      switch (evt.type) {
        case "addthis.menu.open":
          // console.log('menu opened; surface=' + evt.data.pane);
          break;
        case "addthis.menu.close":
          // console.log('menu closed; surface=' + evt.data.pane);
          break;
        case "addthis.menu.share":
          // console.log('user shared to ' + evt.data.service);
          console.log(shareOrDownloadStatus);
          
          jQuery.ajax({
            url: updateUserShareArticlePointUrl,
            type: 'post',
            data: {
              'source': source,
              'shareOrDownloadStatus': shareOrDownloadStatus
            },
            success: function (result) {
              // console.log("success");

            },
            error: function () {
              // console.log('fail');
            }
          });
          break;
        case "addthis.user.clickback":
          // console.log('user clickback to ' + evt.data.service);
        default:
          // console.log('received an unexpected event', evt);
      }

      if (evt.type == 'addthis.menu.share') {
        // console.log(typeof(evt.data)); // evt.data is an object hash containing all event data
        // console.log(evt.data.service); // evt.data.service is specific to the "addthis.menu.share" event
      }
    }

    // Listen to various events
    addthis.addEventListener('addthis.menu.open', eventHandler);
    addthis.addEventListener('addthis.menu.close', eventHandler);
    addthis.addEventListener('addthis.menu.share', eventHandler);
</script>
