<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$menu = $app->getMenu();
$menu_items = $menu->getItems('menutype', 'top');
$menu_items_2 = $menu->getItems('menutype', 'quake-club-top');
$this->language = $doc->language;
$this->direction = $doc->direction;

if ($user->id != 0) {
  $db = JFactory::getDBO();
  $db->setQuery('SELECT play_times_earn,qcoins_after_redemption FROM #__cus_quake_club_game_records WHERE state > 0 and user_id='.$user->id);
  $records = $db->loadRow();
}else{
  $records =array(
    '0' => 0,
    '1' => 0
  );
}
$this->records = $records;


// Getting params from template
$params = $app->getTemplate(true)->params;
$userToken = JSession::getFormToken();

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
$isMobile = "";
$checkEmailUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.checkEmailExist');
$createClientUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.createClient');

if ($menu->getActive()->alias == "quake-club-dashboard") {
  header("Location: ".$this->baseurl."/quake-club/dashboard");
}

if ($task == "edit" || $layout == "form") {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}

// Add Stylesheets
// $doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap.min.css?v=0620');
$doc->addStyleSheet('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css');
$doc->addStyleSheet('https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', array('integrity'=>'sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh','crossorigin'=>'anonymous','version'=>'auto'));
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css', array('version'=>'auto'));
//$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap-select.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/owl.carousel.min.css', array('version'=>'auto'));
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/owl.theme.default.min.css', array('version'=>'auto'));
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/lity/dist/lity.css', array('version'=>'auto'));
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/light-slider/dist/css/lightslider.css', array('version'=>'auto'));
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap-select.min.css', array('version'=>'auto'));
$doc->addStyleSheet("//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css", array('version'=>'auto'));
$doc->addStyleSheet("//cdnjs.cloudflare.com/ajax/libs/bricklayer/0.4.2/bricklayer.min.css");
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/style.css', array('version'=>'auto'));
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/modal.css', array('version'=>'auto'));

// Add scripts
JHtml::_('jquery.framework');
// $doc->addScript('https://code.jquery.com/jquery-3.4.1.slim.min.js', array('integrity'=>'sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n', 'crossorigin'=>'anonymous', 'version'=>'auto'));
$doc->addScript('https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', array('integrity'=>'sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo', 'crossorigin'=>'anonymous', 'version'=>'auto'));
$doc->addScript('https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',array('integrity'=>'sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6','crossorigin'=>'anonymous','version'=>'auto'));
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js', array('version'=>'auto'));
$doc->addScript('https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js', array('version'=>'auto'));
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/jquery.matchHeight.js', array('version'=>'auto'));
//$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/bootstrap-select.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/lity/dist/lity.js', array('version'=>'auto'));
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/chart-js/dist/Chart.js', array('version'=>'auto'));
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/light-slider/dist/js/lightslider.js', array('version'=>'auto'));
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/chartjs-plugin-labels.js', array('version'=>'auto'));
$doc->addScript('https://unpkg.com/sweetalert/dist/sweetalert.min.js', array('version'=>'auto'));
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/bootstrap-select.min.js', array('version'=>'auto'));
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/script.js', array('version'=>'auto'));
$doc->addScript("https://code.jquery.com/ui/1.12.1/jquery-ui.js", array('version'=>'auto'));
$doc->addScript("https://cdnjs.cloudflare.com/ajax/libs/bricklayer/0.4.2/bricklayer.min.js");
$doc->addScript("https://www.google.com/recaptcha/api.js");

// Adjusting content width
if ($this->countModules('sidebar-left') && $this->countModules('sidebar-right')) {
    $span = "col-md-6";
} elseif ($this->countModules('sidebar-left') && !$this->countModules('sidebar-right')) {
    $span = "col-md-9";
} elseif (!$this->countModules('sidebar-left') && $this->countModules('sidebar-right')) {
    $span = "col-md-9";
} else {
    $span = "col-md-12";
}

//get all slider
$db_cus_sliders_banner    = JFactory::getDBO();
$query_cus_sliders_banner = $db_cus_sliders_banner->getQuery( true );
$query_cus_sliders_banner
  ->select( '*' )
  ->from( $db_cus_sliders_banner->quoteName( '#__cus_sliders_banner' ) )
  ->where($db_cus_sliders_banner->quoteName('state')." > 0")
  ->order( 'ordering asc' );
$db_cus_sliders_banner->setQuery( $query_cus_sliders_banner );
$cus_sliders_banner = $db_cus_sliders_banner->loadObjectList();

//get all slider highlighs
$db_cus_sliders_highlight    = JFactory::getDBO();
$query_cus_sliders_highlight = $db_cus_sliders_highlight->getQuery( true );
$query_cus_sliders_highlight
  ->select( '*' )
  ->from( $db_cus_sliders_highlight->quoteName( '#__cus_sliders_highlight' ) )
  ->where($db_cus_sliders_highlight->quoteName('state')." > 0")
  ->order( 'ordering asc' );
$db_cus_sliders_highlight->setQuery( $query_cus_sliders_highlight );
$cus_sliders_highlight = $db_cus_sliders_highlight->loadObjectList();

//get all slider highlighs
$db_cus_banner_v1ewership    = JFactory::getDBO();
$query_cus_banner_v1ewership = $db_cus_banner_v1ewership->getQuery( true );
$query_cus_banner_v1ewership
  ->select( '*' )
  ->from( $db_cus_banner_v1ewership->quoteName( '#__cus_banner_v1ewership' ) )
  ->order( 'id asc' );
$db_cus_banner_v1ewership->setQuery( $query_cus_banner_v1ewership );
$cus_banner_v1ewership = $db_cus_banner_v1ewership->loadObjectList();

//get all slider
$db_cus_qperks_sliders_banner    = JFactory::getDBO();
$query_cus_qperks_sliders_banner = $db_cus_qperks_sliders_banner->getQuery( true );
$query_cus_qperks_sliders_banner
  ->select( '*' )
  ->from( $db_cus_qperks_sliders_banner->quoteName( '#__cus_qperks_sliders_banner' ) )
  ->where($db_cus_qperks_sliders_banner->quoteName('state')." > 0")
  ->order( 'ordering asc' );
$db_cus_qperks_sliders_banner->setQuery( $query_cus_qperks_sliders_banner );
$cus_qperks_sliders_banner = $db_cus_qperks_sliders_banner->loadObjectList();

$db_cus_cart    = JFactory::getDBO();
$db_cus_cart->setQuery('SELECT count(id) FROM #__cus_qperks_cart where user_id = '. $user->id );
$cart_num            = $db_cus_cart->loadResult();

//detect mobile or desktop
if (stristr($_SERVER['HTTP_USER_AGENT'],'mobi')!==FALSE) {
  $isMobile = True;
}else{
  $isMobile = False;
}
$updateUserShareArticlePointUrl = JRoute::_('index.php?option=com_quake_club_qperks&task=qperksuserpoint.addPoint');

// echo $this->params->get('favicon');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>"
    lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <jdoc:include type="head" />
        <link rel="shortcut icon"
            href="<?php echo JUri::root(true) . htmlspecialchars($this->params->get('favicon'), ENT_COMPAT, 'UTF-8'); ?>" />
        <!--[if lt IE 9]>
                <script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
        <![endif]-->
    </head>

    <body class="preload">
        <header class="navbar navbar-expand-xl navbar-light">
            <?php if ($this->countModules('position-1')) : ?>
            <!-- Logo start -->
            <a class="brand pull-left" href="<?php echo $this->baseurl; ?>/">
                <?php echo '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename .'" />'; ?>
            </a>
            <!-- Logo End -->

            <!--before login-->
            <?php if (JFactory::getUser()->guest) { ?>
            <a class="mobile-quake d-xl-none" href="<?php echo $this->baseurl ?>/login">
                <span class="join-us">Join us now</span>
                <img src="<?php echo $this->baseurl ?>/images/2020/club/quake-club-white.png" alt="Quake Club" />
            </a>
            <!--////////////-->
            <?php }else{ ?>
            <!--after login-->
            <div class="logged-in d-xl-none ml-auto mr-3">
                <!-- <a href="<?php echo $this->baseurl ?>/quake-club/my-carts" class="circle-icon cart mr-3">
                <?php if ($cart_num > 0) {
                    echo "<span class='cart-quantity'>". $cart_num ."</span>";
                  }  ?>
                  <i class="fas fa-shopping-cart"></i>
                </a> -->

                <div class='dropdown d-inline-block user-dropdown'>
                    <a href="#" class="circle-icon" id='userDropdown2' data-toggle='dropdown' aria-haspopup='true'
                        aria-expanded='false'>
                        <i class="fas fa-user"></i>
                    </a>
                    <div class='dropdown-menu' aria-labelledby='userDropdown2'>
                        <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/dashboard'>Dashboard</a>
                        <a class='dropdown-item'
                            href='<?php echo $this->baseurl ?>/quake-club/rewards-catalogue'>Rewards Catalogue</a>
                        <a class='dropdown-item' href='<?php echo $this->baseurl ?>/user-profile/profile'>My Account</a>
                        <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/friend-referral'>Friend
                            Referral</a>
                        <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/points-history'>Points
                            History</a>
                        <a class='dropdown-item'
                            href='<?php echo $this->baseurl ?>/quake-club/redemption-list'>Redemption History</a>
                        <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/change-password'>Change
                            Password</a>
                        <a class='dropdown-item'
                            href='<?php echo $this->baseurl ?>/quake-club/frequently-asked-questions'>Frequently Asked
                            Questions (FAQ)</a>
                        <a class='dropdown-item'
                            href='<?php echo $this->baseurl ?>/quake-club/terms-and-conditions'>Terms & Conditions</a>
                        <a class='dropdown-item' href='<?php echo $this->baseurl ?>/quake-club/contact-us'>Contact
                            Us</a>
                        <a class='dropdown-item'
                            href='index.php?option=com_users&task=user.logout&<?php echo $userToken ?>=1'>Logout</a>
                    </div>
                </div>
            </div>
            <!--////////////-->
            <?php } ?>





            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <div id="nav-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <!-- Menu List Start -->
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="menu navbar-nav mx-auto">
                    <?php $i=0; foreach ($menu_items as $key => $menu_item_value) {

                    if ($menu_item_value->level == 1) {
                        if ($menu_item_value === end($menu_items)) {
                        echo "<li>";
                        echo "
                            <div class='quake-nav'>
                            <div class='logo-side'>";
                            if (JFactory::getUser()->guest) {
                                echo "<span class='join-us'>Join us now</span>";
                            }
                                echo "<a href=". $this->baseurl.'/'.$menu_item_value->alias .">
                                <img src='".$this->baseurl."/images/2020/club/quake-club-white.png' alt='Quake Club'/>
                                </a>
                            </div>";
                            if (!JFactory::getUser()->guest) {
                                echo "<div class='logged-in'>
                                <a href='".$this->baseurl."/quake-club/my-carts' class='circle-icon cart'>";
                        if ($cart_num > 0) {
                          echo "<span class='cart-quantity'>". $cart_num ."</span>";
                        }

                          echo "<i class='fas fa-shopping-cart'></i>
                        </a>
                                

                                <div class='dropdown d-inline-block user-dropdown'>
                                <button class='btn dropdown-toggle' type='button' id='userDropdown' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                    <span class='circle-icon'><i class='fas fa-user'></i></span>
                                    <span class='username'>".$user->name."</span>
                                </button>
                                <div class='dropdown-menu' aria-labelledby='userDropdown'>
                                    <a class='dropdown-item' href='".$this->baseurl."/user-profile/profile'>My Account</a>
                                    <a class='dropdown-item' href='".$this->baseurl."/quake-club/friend-referral'>Friend Referral</a>
                                    <a class='dropdown-item' href='".$this->baseurl."/quake-club/points-history'>Points History</a>
                                    <a class='dropdown-item' href='".$this->baseurl."/quake-club/redemption-list'>Redemption History</a>
                                    <a class='dropdown-item' href='".$this->baseurl."/quake-club/change-password'>Change Password</a>
                                    <a class='dropdown-item' href='".$this->baseurl."/quake-club/contact-us'>Contact Us</a>
                                    <a class='dropdown-item' href='index.php?option=com_users&task=user.logout&" . $userToken . "=1'>Logout</a>
                                </div>
                                </div>

                            </div>";
                            }
                            echo "
                            </div>
                        ";
                        echo "</li>";
                        }else{
                        echo "<li>";
                        echo "<a class='nav-link' href='". $this->baseurl.'/'.$menu_item_value->alias ."'>" . $menu_item_value->title . "</a>";
                        echo "</li>";
                        }
                    }

                    ?>
                    <?php }; ?>
                </ul>
                <div class="menu navbar-nav enquiry-menu">
                    <a href="sme" class='btn btn-outlined btn-pink btn-enquiry'>
                        <!-- <i class="far fa-envelope"></i>  -->
                        SME 360
                    </a>
                </div>
                <!-- <jdoc:include type="modules" name="position-1" style="none"/> -->
            </div>

            <!-- Menu List End -->
            <?php endif; ?>
        </header>

        <div class="body" id="<?php echo $menu->getActive()->alias;?>">
            <div class="content">
                <!-- Head Banner Start -->
                <div class="page-banner-wrapper">
                    <jdoc:include type="modules" name="top-banner" style="none" />
                    <?php if (strpos(JUri::current(), 'quakecast/gallery') !== false) { ?>
                    <div class="page-banner quakecast-page-banner">
                        <div class="container">
                        <h2 class="page-title">
                            <img width="270" src="images/2020/quakecast-logo.png" alt="QuakeCast" class="img-fluid">
                        </h2>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <!-- quake-club-slider start -->
                <?php
                if ($menu->getActive()->menutype == "quake-club-top" || $menu->getActive()->title == 'Quake Club') :?>
                <!-- Top Menu -->
                <div class="club-main-menu">
                    <ul class="nav">
                        <?php $i=0; foreach ($menu_items_2 as $key => $menu_item_value) {

                      if ($menu_item_value->level == 2 && $menu_item_value->getParams()['menu_show'] == 1) {
                          if ($menu_item_value->alias == $menu->getActive()->alias) {
                            echo "<li class='nav-item active'>";
                          }else{
                            echo "<li class='nav-item'>";
                          }

                          echo "<a class='nav-link' href='". $this->baseurl.'/quake-club/'.$menu_item_value->alias ."'>" . $menu_item_value->title . "</a>";
                          echo "</li>";
                      }

                      ?>
                        <?php }; ?>
                    </ul>
                </div>

                <!--Birthday Reminder Floating Button-->
                <!--<div class="birthday-reminder">
                  <div class="toggle">

                  </div>

                  <div class="message">
                    <span>Happy Birthday!</span> 200 Q-Perks from us once you redeem any items from the Rewards Catalogue.
                  </div>
                </div>-->

                <?php if ($menu->getActive()->title == "Dashboard" || $menu->getActive()->alias == 'quake-club-dashboard') :?>
                <div class="owl-carousel owl-theme" id="club-banner">
                    <?php foreach ($cus_qperks_sliders_banner as $key => $cus_qperks_sliders_banner_value) {
                          /**
                           * link_type value meaning
                           * 0 = Don't link
                           * 1 = Url Address
                           * 2 = Youtube
                           */
                          ?>
                    <div class="item-video">
                        <?php if ($cus_qperks_sliders_banner_value->link_type == 2) {?>

                        <a href="<?php echo $cus_qperks_sliders_banner_value->youtube_link; ?>" data-lity>
                            <div class="banner-image"
                                style="background-image: url('<? echo $cus_qperks_sliders_banner_value->image;?>')">
                                <img src="images/2020/club/club-banner-mobile-guide.png" class="guide-xs" />
                            </div>
                        </a>

                        <?php }elseif ($cus_qperks_sliders_banner_value->link_type == 1) { ?>

                        <a target="<?php echo $cus_qperks_sliders_banner_value->target_window; ?>"
                            href="<?php echo $cus_qperks_sliders_banner_value->url_address; ?>">
                            <div class="banner-image"
                                style="background-image: url('<? echo $cus_qperks_sliders_banner_value->image;?>')">
                                <img src="images/2020/club/club-banner-mobile-guide.png" class="guide-xs" />
                            </div>
                        </a>

                        <?php }elseif ($cus_qperks_sliders_banner_value->link_type == 0) { ?>
                        <div class="banner-image"
                            style="background-image: url('<? echo $cus_qperks_sliders_banner_value->image;?>')">
                            <img src="images/2020/club/club-banner-mobile-guide.png" class="guide-xs" />
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
                <?php endif; ?>
                <?php endif; ?>
                <!-- quake-club-slider end -->

                <!-- CNY Prosperatty Rush Dashboard Main Banner -->
                <?php if ($menu->getActive()->alias == 'prosperatty') :?>
                <div class="cny-main-banner">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-xl-5 col-md-9 col-sm-10 col-10">
                                <img src="images/2020/cny2020/campaign.svg" class="campaign-logo mb-3 img-fluid" />
                                <p>Welcome to ProspeRATty Rush! A game designed specially to exhilarate advertising and
                                    marketing professionals!<br />
                                    Collect Q-Coins for rewards redemption and be in the running to win weekly prizes.
                                    Play now!</p>

                                <div class="row justify-content-center">
                                    <div class="col-xl-7 col-lg-5 col-md-6 col-sm-8">
                                        <div class="coin-summary-wrapper">
                                            <?php if($user->id != 0){ ?>

                                            <div class="coin-summary">
                                                <img src="images/2020/cny2020/qcoin.svg" />
                                                Q-Coins

                                                <figure><?php echo number_format($this->records[1]);?></figure>
                                            </div>

                                            <?php } ?>

                                        </div>
                                        <div class="game-menu">
                                            <!-- <a href="./prosperatty-rush" class="btn btn-cny d-block">Play Now</a> -->
                                            <!-- <button type="button" class="btn btn-cny d-block" data-toggle="modal" data-target="#modalEarnLife">Earn More Life (<?php echo $this->records[0];?>)</button> -->
                                            <button type="button" class="btn btn-cny d-block" data-toggle="modal"
                                                data-target="#modalPrize">Prize</button>
                                            <button type="button" class="btn btn-cny d-block" data-toggle="modal"
                                                data-target="#modalRanking">Leaderboard</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <img class="main-deco deco-4" src="images/cny2020/main-4.png"/>
                    <img class="main-deco deco-1" src="images/2020/cny2020/main-1.png"/>
                    <img class="main-deco deco-2" src="images/2020/cny2020/main-2.png"/>
                    <img class="main-deco deco-3" src="images/2020/cny2020/main-3.png"/> -->
                </div>
                <?php endif; ?>
                <!-- END OF CNY Prosperatty Rush Dashboard Main Banner -->

                <!---JUri::current() === (JURI::base() .'quakecast') -->
                <?php
                if (JUri::current() === (JURI::base() .'quakecast')) :?>
                <div class="owl-carousel owl-theme mb-5" id="vg-banner">
                    <div class="quakecast-banner" v-for="slider in sliders_banner">

                        <div class="video-banner" v-if="slider.link_type == 3">
                            <div class="overlayer">
                                <div class="container">
                                    <div class="content-holder">
                                        <!-- <figure class="category">Category Here</figure> -->
                                        <h3 v-if="slider.title != ''">{{slider.title}}</h3>
                                        <!-- {{slider.description}} --> <!--no description needed-->
                                        <figure class="date" v-if="slider.publish_date != ''">{{slider.publish_date}}</figure>
                                        <a v-if="slider.button_text != ''" :href="slider.button_url" :target="slider.target_window" class="btn btn-white">{{slider.button_text}}</a>
                                        <!--missing: button url-->
                                    </div>
                                </div>
                            </div>
                            <video preload="auto" loop autoplay playsinline muted>
                                <source :src="'uploads/video-gallery/'+slider.video_file" type="video/mp4">
                                <source :src="'uploads/video-gallery/'+slider.video_file" type="video/ogg">
                                <!-- <img src="{{+slider.desktop_image+}}"> -->
                                Your browser does not support HTML video.
                            </video>

                            <div class="banner-image" :style="{'background-image': 'url('+slider.desktop_image+')'}">
                                <img src="images/2020/cast-bannerguide.png" class="guide-xl d-none d-xl-block" />
                                <img src="images/2020/banner-mobile-guide.png" class="guide-xs d-xl-none" />
                            </div>
                        </div>

                        <div class="video-banner full-video-banner" v-if="slider.link_type == 4">
                            <div class="overlayer">
                                <div class="container">
                                    <div class="content-holder">
                                        <!-- <figure class="category">Category Here</figure> -->
                                        <h3 v-if="slider.title != ''">{{slider.title}}</h3>
                                        <!-- {{slider.description}} --> <!--no description needed-->
                                        <figure class="date" v-if="slider.publish_date != ''">{{slider.publish_date}}</figure>
                                        
                                        <a v-if="slider.button_text != ''" :href="slider.button_url" :target="slider.target_window" class="btn btn-white">{{slider.button_text}}</a>
                                        <!--missing: button url-->
                                    </div>
                                </div>
                            </div>
                            <video preload="auto" loop autoplay playsinline muted>
                                <source :src="'uploads/video-gallery/'+slider.video_file" type="video/mp4">
                                <source :src="'uploads/video-gallery/'+slider.video_file" type="video/ogg">
                                <!-- <img src="{{+slider.desktop_image+}}"> -->
                                Your browser does not support HTML video.
                            </video>

                            <div class="banner-image">
                                <img src="images/2020/cast-bannerguide.png" class="guide-xl d-none d-xl-block" />
                                <img src="images/2020/banner-mobile-guide.png" class="guide-xs d-xl-none" />
                            </div>
                        </div>

                        <a v-if="slider.link_type == 2" :href="slider.youtube_link" data-lity>
                            <div class="banner-image" :style="{'background-image': 'url('+slider.desktop_image+')'}">
                                <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                        </a>


                        <a v-if="slider.link_type == 1" :target="slider.target_window" :href="slider.url_address">
                            <div class="banner-image" :style="{'background-image': 'url('+slider.desktop_image+')'}">
                                <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                        </a>

                        <div v-if="slider.link_type == 0" class="banner-image"
                            :style="{'background-image': 'url('+slider.desktop_image+')'}">
                            <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if ($menu->getActive() == $menu->getDefault()) :
                ?>
                <div class="modal fade" ref="vuemodal" id="noticeModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="position-relative">
                                    <button @click="closeModal" type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                                    </button>

                                    <!--- Quake CLub --->
                                    <!-- <a href="<?php echo $this->baseurl ?>/login" target="_blank">
                              <img src="./images/2020/Quake_popupBanner.png" alt="Sign up now at Quake Club">
                            </a> -->
                            <a href="<?php echo $this->baseurl ?>/enquiry">
                            <img src="./images/2020/STO-500x500.jpg"
                                        alt="Quake club is turning 1.">
                                        </a> 
                                        <!-- <H1>NOTICE : </H1>
                                        <br>
                                        <p>Due to the restricted and conditional movement orders, you may experience delay in delivery of your order. We apologize for any inconvenience caused and thank you for your patience.</p>
                                        <button type="button" class="btn btn-pink btn-block" data-dismiss="modal">OK</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="owl-carousel owl-theme" id="index-banner">
                    <?php foreach ($cus_sliders_banner as $key => $cus_sliders_banner_value) {
                         /**
                         * link_type value meaning
                         * 0 = Don't link
                         * 1 = Url Address
                         * 2 = Youtube
                         */
                        ?>
                    <div class="item-video">
                        <?php if ($cus_sliders_banner_value->link_type == 2) {?>

                        <a href="<?php echo $cus_sliders_banner_value->youtube_link; ?>" data-lity>
                            <div class="banner-image"
                                style="background-image: url('<? echo $cus_sliders_banner_value->desktop_image;?>')">
                                <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                        </a>

                        <?php }elseif ($cus_sliders_banner_value->link_type == 1) { ?>

                        <a target="<?php echo $cus_sliders_banner_value->target_window; ?>"
                            href="<?php echo $cus_sliders_banner_value->url_address; ?>">
                            <div class="banner-image"
                                style="background-image: url('<? echo $cus_sliders_banner_value->desktop_image;?>')">
                                <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                        </a>

                        <?php }elseif ($cus_sliders_banner_value->link_type == 0) { ?>
                        <div class="banner-image"
                            style="background-image: url('<? echo $cus_sliders_banner_value->desktop_image;?>')">
                            <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
                <?php endif; ?>

                <!-- CNY 2020 Floating Button -->
                <!-- <div class="cny-float">
                  <a href="< echo $this->baseurl ?>/prosperatty" target="_blank">
                    <img src="images/2020/cny2020/float-md.png" class="d-none d-lg-block" alt="">
                    <img src="images/2020/cny2020/float-xs.png" class="d-lg-none" alt="">
                  </a>
                </div> -->

                <div id="inline" style="background:#fff" class="lity-hide">
                    Inline content
                </div>
                <!-- Head Banner End -->
                <!-- Slider Highlights Start -->
                <?php if ($menu->getActive() == $menu->getDefault()) : ?>
                <div class="highlight-banner-class">
                    <div class="container">
                        <h2>Top user's picks</h2>
                        <p>Earn Q-Perks for every share</p>
                        <div class="owl-carousel owl-theme mt-4" id="highlight-banner">
                            <?php foreach ($cus_sliders_highlight as $key => $cus_sliders_highlight_value) { ?>

                            <div class="item highlight-banner-wrapper">
                                <a href="<?php echo $cus_sliders_highlight_value->url_address; ?>">
                                    <div class="img-holder"
                                        style="background-image: url('<? echo $cus_sliders_highlight_value->image;?>')">
                                        <img src="images/2020/card-guide.png" class="placeholder" />
                                    </div>
                                    <div class="highlight-banner-div">
                                        <div class="highlight-banner-tag">
                                            <?php echo $cus_sliders_highlight_value->tag; ?></div>
                                        <!-- <div class="d-none d-lg-block">
                                <h3><?php echo mb_strimwidth("$cus_sliders_highlight_value->title", 0, 30, "…"); ?></h3>
                                <p><?php echo mb_strimwidth("$cus_sliders_highlight_value->description", 0,80, "…"); ?></p>
                              </div>
                              <div class="d-lg-none">
                                <h3><?php echo $cus_sliders_highlight_value->title; ?></h3>
                                <p><?php echo $cus_sliders_highlight_value->description; ?></p>
                              </div> -->
                                    </div>
                                </a>
                            </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="ecosystem">
                    <div class="container">
                        <h2 class="mb-3">Content & consumer based ecosystem</h2>

                        <p>QUAKE, we fuse <b>Quality</b> data with <b>Uncommon</b> thinking to develop <b>Amazing</b> strategies. Blending this with <b>Keen</b> insights, we offer <b>Effective</b> solutions – forward-looking, disruptive ideas which embody our spirit of winning the future.</p>

                        <div class="mt-5 row justify-content-center">
                            <div class="col-lg-10">

                            <div class="nav nav mb-3" id="pills-tab" role="tablist">
                                    <a class="nav-link active" id="pill-tv" data-toggle="pill" href="#tv" role="tab"
                                        aria-controls="v-pills-tv" aria-selected="true">
                                        <div class="eco-btn">
                                            <div class="svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="60" height="55" viewBox="0 0 60 55"><rect width="60" height="55" fill="none"/><path d="M56.3,5.25H3.8A3.6,3.6,0,0,0,.25,8.8V43.9A3.6,3.6,0,0,0,3.8,47.45H25.35v3.138l-14.2,2.368a1.025,1.025,0,0,0-.792,1.188.985.985,0,0,0,.946.806l15-2.5,7.359,0L48.7,54.95h.1a.965.965,0,0,0,.952-.971.231.231,0,0,0-.005-.066.915.915,0,0,0-.809-.96L34.75,50.588V47.45H56.309A3.653,3.653,0,0,0,59.85,43.8V8.8A3.6,3.6,0,0,0,56.3,5.25ZM32.85,47.35v3h-5.5v-3ZM2.25,8.8A1.543,1.543,0,0,1,3.8,7.25H56.407A1.543,1.543,0,0,1,57.95,8.786V43.9A1.542,1.542,0,0,1,56.4,45.45l-52.6-.1A1.543,1.543,0,0,1,2.25,43.8Z" fill="#00aeef"/><path d="M53.9,10.25H6.288A1.046,1.046,0,0,0,5.25,11.3V41.4a1.036,1.036,0,0,0,.294.74,1.046,1.046,0,0,0,.756.314H54.15v-.128a1.034,1.034,0,0,0,.8-1.022v-30A1.046,1.046,0,0,0,53.9,10.25Zm-1.05,2v28.1H7.25V12.25Z" fill="#00aeef"/></svg>
                                            </div>
                                            <div class="title">
                                                <span>TV</span>
                                            </div>
                                        </div>

                                        <div class="d-md-none">
                                            <h3>TV</h3>
                                            <div>
                                                Largest Pay-TV operator in SEA. Serving 5.7 mil Households with 75% Penetration across 204 channels.
                                            </div>
                                        </div>
                                    </a>

                                    <a class="nav-link" id="pill-radio" data-toggle="pill" href="#radio" role="tab"
                                        aria-controls="v-pills-radio" aria-selected="true">
                                        <div class="eco-btn">
                                            <div class="svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="74.758" height="56.557" viewBox="0 0 74.758 56.557"><defs><clipPath id="a"><rect width="17" height="26" transform="translate(-26.187 30.518)" fill="#fff" stroke="#707070" stroke-width="1"/></clipPath></defs><g transform="translate(27.187)"><g transform="translate(-9)"><path d="M53.462,20.858H48.436L12.815,2.114a.943.943,0,0,0-.877,1.669L44.429,20.858H7.11A5.111,5.111,0,0,0,2,25.969H2V53.462a5.111,5.111,0,0,0,5.11,5.11H53.462a5.111,5.111,0,0,0,5.11-5.11V25.969A5.111,5.111,0,0,0,53.462,20.858Zm3.225,32.6a3.224,3.224,0,0,1-3.225,3.225H7.11a3.224,3.224,0,0,1-3.225-3.225V25.969A3.224,3.224,0,0,1,7.11,22.744H53.462a3.224,3.224,0,0,1,3.225,3.225Z" transform="translate(-2 -2.016)" fill="#f58226"/><path d="M54.086,26H6.943A.943.943,0,0,0,6,26.943v7.543a.943.943,0,0,0,.943.943H54.086a.943.943,0,0,0,.943-.943V26.943A.943.943,0,0,0,54.086,26Zm-.943,7.543h-33V29.771a.943.943,0,0,0-1.886,0v3.771H7.886V27.886H53.143Z" transform="translate(-2.229 -3.386)" fill="#f58226"/><path d="M15.429,38a9.429,9.429,0,1,0,9.429,9.429A9.428,9.428,0,0,0,15.429,38Zm0,16.972a7.543,7.543,0,1,1,7.543-7.543A7.543,7.543,0,0,1,15.429,54.972Z" transform="translate(-2.229 -4.072)" fill="#f58226"/><path d="M47.429,38a9.429,9.429,0,1,0,9.429,9.429A9.428,9.428,0,0,0,47.429,38Zm0,16.972a7.543,7.543,0,1,1,7.543-7.543A7.543,7.543,0,0,1,47.429,54.972Z" transform="translate(-4.057 -4.072)" fill="#f58226"/></g><g transform="translate(-1)" clip-path="url(#a)"><g transform="translate(0 11)"><path d="M330.183,5.611A19.743,19.743,0,0,0,316.306,0a.892.892,0,0,0,0,1.784,17.968,17.968,0,0,1,12.63,5.1,17.065,17.065,0,0,1,5.22,12.3.892.892,0,1,0,1.785,0A18.837,18.837,0,0,0,330.183,5.611Z" transform="translate(-340.162 23.443)" fill="#f58226"/><path d="M316.306,36.331a.892.892,0,0,0,0,1.784,13.6,13.6,0,0,1,13.6,13.6.892.892,0,1,0,1.785,0,15.385,15.385,0,0,0-15.386-15.385Z" transform="translate(-340.162 -9.089)" fill="#f58226"/><path d="M316.305,72.249A10.031,10.031,0,0,1,326.337,82.28a.892.892,0,1,0,1.784,0,11.816,11.816,0,0,0-11.816-11.816.892.892,0,0,0,0,1.785Z" transform="translate(-340.161 -39.653)" fill="#f58226"/><path d="M316.306,106.382a6.463,6.463,0,0,1,6.463,6.463.892.892,0,1,0,1.784,0,8.248,8.248,0,0,0-8.247-8.247.892.892,0,0,0,0,1.784Z" transform="translate(-340.162 -70.217)" fill="#f58226"/><path d="M2193.314,8193.518a2.5,2.5,0,1,1,2.5-2.5A2.5,2.5,0,0,1,2193.314,8193.518Zm0-3.845a1.346,1.346,0,1,0,1.345,1.345A1.346,1.346,0,0,0,2193.314,8189.672Z" transform="translate(-2216 -8150)" fill="#f58226"/></g></g></g></svg>
                                            </div>
                                            <div class="title">
                                                <span>Radio</span>
                                            </div>
                                        </div>
                                        <div class="d-md-none">
                                            <h3>Radio</h3>
                                            <div>
                                                <b>#1</b> in every language –  English, Malay, Chinese and Tamil. 16.9 mil weekly listeners across 11 radio brands. Digitalising terrestrial radio via SYOK.
                                            </div>
                                        </div>
                                    </a>
                                    <a class="nav-link" id="pill-web" data-toggle="pill" href="#web" role="tab"
                                        aria-controls="v-pills-web" aria-selected="true">
                                        <div class="eco-btn">
                                            <div class="svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="57.193" height="49.023"
                                                    viewBox="0 0 57.193 49.023">
                                                    <path
                                                        d="M24.469,46.895h4.085V40.767H24.469Zm0,2.043a4.084,4.084,0,0,1-4.085,4.085H6.085A4.084,4.084,0,0,1,2,48.937v-28.6a4.084,4.084,0,0,1,4.085-4.085H8.128V8.085A4.084,4.084,0,0,1,12.213,4H55.108a4.084,4.084,0,0,1,4.085,4.085v28.6a4.084,4.084,0,0,1-4.085,4.085H40.81v6.128h8.17v2.043Zm14.3-2.043V40.767H30.6v6.128Zm-16.341,0V20.341A2.043,2.043,0,0,0,20.383,18.3H6.085a2.043,2.043,0,0,0-2.043,2.043v28.6A2.043,2.043,0,0,0,6.085,50.98h14.3a2.043,2.043,0,0,0,2.043-2.043Zm8.17-8.17H55.108a2.043,2.043,0,0,0,2.043-2.043V8.085a2.043,2.043,0,0,0-2.043-2.043H12.213A2.043,2.043,0,0,0,10.17,8.085v8.17H20.383a4.084,4.084,0,0,1,4.085,4.085V38.724H30.6Zm-16.341,8.17v2.043H12.213V46.895Z"
                                                        transform="translate(-2 -4)" fill="#ec118c" /></svg>
                                            </div>
                                            <div class="title">
                                                <span>Web, Mobile & OTT</span>
                                            </div>
                                        </div>
                                        <div class="d-md-none">
                                            <h3>Web, Mobile & Ott</h3>
                                            <div>
                                                We offer a full suite of digital services across 47 digital brands with a reach of 13.9 mil monthly unique visitors. Expand individuals’ experience via Astro GO, HBO Go and iQIYI.
                                            </div>
                                        </div>
                                    </a>
                                    <a class="nav-link" id="pill-ip" data-toggle="pill" href="#ip" role="tab"
                                        aria-controls="v-pills-ip" aria-selected="true">
                                        <div class="eco-btn">
                                            <div class="svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="82.043" height="47.717" viewBox="0 0 82.043 47.717"><g transform="translate(-639.098 -295.137)"><g transform="translate(639.098 295.137)"><path d="M682.236,312.018H647.8l29.619-8.117a.932.932,0,1,0-.493-1.8l-1.573.431-5.957-4,5.087-1.394a3.847,3.847,0,0,1,4.345,1.791.932.932,0,1,0,1.615-.932h0a5.712,5.712,0,0,0-6.453-2.66l-30.69,8.413h-.027a5.713,5.713,0,0,0-3.969,7l.635,2.314v24.08a5.712,5.712,0,0,0,5.705,5.705h31.822a5.711,5.711,0,0,0,5.705-5.705v-24.2a.932.932,0,0,0-.931-.933Zm-.932,7.3h-9.119l4.688-5.433H681.3Zm-17.288,0,4.688-5.433h5.709l-4.688,5.433Zm-8.169,0,4.688-5.433h5.709l-4.688,5.433Zm-8.169,0,4.688-5.433h5.709l-4.688,5.433Zm-5.876,0v-5.433h8.1l-4.688,5.433Zm9.457-15.813,5.957,4-5.51,1.509-5.957-4Zm7.884-2.159,5.957,4-5.51,1.508-5.957-4Zm13.833,1.841-5.509,1.508-5.954-4,5.51-1.508Zm-29.575,2.5,5.933,3.99-7.814,2.141-.421-1.536A3.847,3.847,0,0,1,643.4,305.682Zm37.9,31.478A3.844,3.844,0,0,1,677.46,341H645.638a3.846,3.846,0,0,1-3.841-3.841V321.18H681.3Z" transform="translate(-639.098 -295.137)" fill="#ffc219" fill-rule="evenodd"/><path d="M676.74,333.025H647.068a.932.932,0,1,0,0,1.864H657.1v2.031a.932.932,0,0,0,1.864,0v-2.031h5.872v2.031a.932.932,0,0,0,.932.932h0a.932.932,0,0,0,.932-.932v-2.031h10.036a.932.932,0,0,0,0-1.864Z" transform="translate(-639.45 -297.034)" fill="#ffc219" fill-rule="evenodd"/><path d="M646.136,328.124a.932.932,0,0,0,.932.932h29.672a.932.932,0,0,0,0-1.864H647.068a.932.932,0,0,0-.932.932Z" transform="translate(-639.45 -296.742)" fill="#ffc219" fill-rule="evenodd"/><path d="M681.98,302.33a.932.932,0,1,0,.932-.932h0A.934.934,0,0,0,681.98,302.33Z" transform="translate(-641.246 -295.45)" fill="#ffc219" fill-rule="evenodd"/></g><path d="M723.277,312.77c-.015-.017-.03-.035-.047-.051a.709.709,0,0,0-1,0h0l-1.292,1.292-1.674-1.674,1.292-1.292a.71.71,0,0,0,0-1h0l-10.508-10.509a.709.709,0,0,0-1,0h0l-1.292,1.294-1.674-1.676,1.292-1.292a.71.71,0,1,0-1-1l-1.791,1.787-6.264,6.269-3.135,3.135-3.135,3.135-3.135,3.129-3.127,3.131h0v0l-.771.772v2.006l1.262-1.263,1.674,1.674-2.116,2.119-.819-.819v3.646l5.445-5.446,9.5,9.509-8.545,8.545-6.4-6.4V333.8l5.9,5.9a.711.711,0,0,0,1,0h0l1.292-1.292,1.674,1.674-1.292,1.292a.71.71,0,0,0,1,1l1.79-1.79,6.259-6.259,3.126-3.126,3.125-3.125,3.126-3.126,3.124-3.124h0v0l3.125-3.125,3.124-3.124,0,0,0,0,1.789-1.789A.71.71,0,0,0,723.277,312.77Zm-13.736-11.735,9.5,9.509-8.549,8.549-9.5-9.509Zm-4.471-.883,1.674,1.674-2.129,2.135-1.674-1.678Zm-3.135,3.135,1.674,1.674-2.129,2.129-1.672-1.673Zm-3.135,3.135,1.674,1.674-2.129,2.129-1.67-1.675Zm-3.135,3.135,1.674,1.674-2.129,2.129-1.674-1.674Zm-3.135,3.135,1.674,1.674-2.129,2.129-1.674-1.674Zm-2.6,5.948-.985.985-1.674-1.674,2.125-2.125,1.674,1.674Zm1.516.495L700,310.584l9.5,9.506-8.549,8.549Zm4.427,19.938L694.2,337.4l2.129-2.129L698,336.942Zm3.135-3.135-1.676-1.674,2.129-2.129,1.674,1.674Zm3.135-3.135-1.674-1.674L702.6,329l1.674,1.674Zm3.135-3.135-1.674-1.674,2.129-2.129,1.674,1.674Zm3.135-3.135-1.674-1.674,2.129-2.129,1.674,1.674Zm3.135-3.135-1.674-1.674,1.14-1.14L712,319.6l1.674,1.674Zm3.135-3.135-1.674-1.674,2.114-2.115,1.674,1.674Zm3.135-3.135-1.688-1.659,2.128-2.129,1.674,1.674Z" transform="translate(-2.299 -0.076)" fill="#ffc219"/></g></svg>
                                            </div>
                                            <div class="title">
                                                <span>Local & Intl IPs</span>
                                            </div>
                                        </div>
                                        <div class="d-md-none">
                                            <h3>Local & International IPs</h3>
                                            <div>
                                                #1 premium content creator & aggregator in the region. Collectively grossed over RM75 million GBO in 2019 and dominated 7 of top 10 highest grossing Malaysian films of all time.
                                            </div>
                                        </div>
                                    </a>
                                    <a class="nav-link" id="pill-commerce" data-toggle="pill" href="#commerce"
                                        role="tab" aria-controls="v-pills-commerce" aria-selected="true">
                                        <div class="eco-btn">
                                            <div class="svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="67" height="51.235"
                                                    viewBox="0 0 67 51.235">
                                                    <g transform="translate(-27.237 -22)">
                                                        <rect width="67" height="51" transform="translate(27.237 22)"
                                                            fill="none" />
                                                        <path
                                                            d="M72.576,56H66.621a2.981,2.981,0,0,0-2.975,2.865l-1.809,9.046H16.992a.993.993,0,0,0-.973,1.187l3.97,19.851a.993.993,0,0,0,.973.8H58.68a1.985,1.985,0,1,1,0,3.97H20.962a.993.993,0,0,0-.993.993v3.97a.993.993,0,0,0,.993.993h1.527a3.97,3.97,0,1,0,6.872,0h20.92a3.97,3.97,0,1,0,6.872,0H58.68a.993.993,0,0,0,.993-.993v-3.1a3.97,3.97,0,0,0,.183-7.637L63.624,69.1l1.508-7.543a2.958,2.958,0,0,0,1.489.4h5.955a2.978,2.978,0,1,0,0-5.955ZM27.91,101.658a1.985,1.985,0,1,1-1.985-1.985A1.985,1.985,0,0,1,27.91,101.658Zm27.792,0a1.985,1.985,0,1,1-1.985-1.985A1.985,1.985,0,0,1,55.7,101.658Zm1.985-3.97H21.955V95.7H57.688ZM34.311,77.837H27.276L26.085,69.9h7.829ZM35.9,69.9h7.839l-.4,7.941H36.3Zm-1.491,9.926.4,7.941H28.765l-1.191-7.941Zm1.988,0h6.846l-.4,7.941H36.8Zm8.834,0h6.836l-1.191,7.941H44.835Zm.1-1.985.4-7.941h7.829l-1.191,7.941ZM24.078,69.9l1.191,7.941H19.791L18.2,69.9Zm-3.89,9.926h5.379l1.191,7.941H21.776Zm37.678,7.941H52.885l1.191-7.941h5.379Zm1.985-9.926H54.374L55.565,69.9H61.44ZM72.576,59.97H66.621a.993.993,0,0,1,0-1.985h5.955a.993.993,0,1,1,0,1.985Z"
                                                            transform="translate(18.309 -32.398)" fill="#F58226" />
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="title">
                                                <span>Commerce</span>
                                            </div>
                                        </div>
                                        <div class="d-md-none">
                                            <h3>Commerce</h3>
                                            <div>
                                                24/7 multilanguage TV & ECMC shopping experience with up to 2.4 mil registered customers.
                                            </div>
                                        </div>
                                    </a>
                                    <a class="nav-link" id="pill-talents" data-toggle="pill" href="#talents" role="tab"
                                        aria-controls="v-pills-talents" aria-selected="true">
                                        <div class="eco-btn">
                                            <div class="svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="69.4" height="57.115" viewBox="0 0 69.4 57.115"><g transform="translate(-0.225 -44.696)"><path d="M.309,60.874a1.232,1.232,0,0,0,0,.892L.42,62.1C2.763,67.009,6,73.814,11.017,79.056c5.131,5.466,11.043,8.254,17.736,8.254a15.71,15.71,0,0,0,2.231-.112h.223l.112.223A23.159,23.159,0,0,0,42.7,101.7a.836.836,0,0,0,.558.112h.112c8.143-.558,14.612-4.573,19.186-12.047,3.793-6.135,5.577-13.5,6.916-18.74l.112-.446a1.259,1.259,0,0,0-.112-.892c-2.231-3.346-6.247-6.135-11.712-8.031a1.222,1.222,0,0,0-.892,0,1.344,1.344,0,0,0-.558.558.953.953,0,0,0,0,.781,1.344,1.344,0,0,0,.558.558c4.8,1.673,8.366,4.016,10.374,6.693l.112.112v.112c-2.566,10.262-6.916,27.44-23.759,28.89l-.112.335h-.112C28.753,91.215,32.1,73.814,34.107,63.44v-.112l.112-.112c3.012-1.562,7.25-2.231,12.158-1.673a1.105,1.105,0,0,0,1.227-.892.941.941,0,0,0-.223-.781,2.334,2.334,0,0,0-.669-.446,32.441,32.441,0,0,0-6.581,0h-.223L39.8,59.2a105.959,105.959,0,0,0-4.462-12.6v-.446c-.112-.335-.335-.446-.669-.558-6.247-2.231-14.055.112-19.52,2.566C9.79,50.5,2.874,54.628.309,60.874Zm15.839-10.82c6.8-3.012,12.939-4.016,17.29-2.677h.112v.112a87.719,87.719,0,0,1,4.239,11.824l.112.335-.335.112a16.568,16.568,0,0,0-4.908,1.9.917.917,0,0,0-.446.669l-.112.335c-.446,2.342-1,5.02-1.339,7.808v.223h-.223a9.77,9.77,0,0,0-4.685.892,11.217,11.217,0,0,0-5.8,6.358.953.953,0,0,0,0,.781,1.142,1.142,0,0,0,.669.558.953.953,0,0,0,.781,0,1.142,1.142,0,0,0,.558-.669,8.482,8.482,0,0,1,4.573-5.02,9.2,9.2,0,0,1,3.458-.781h.335v.335a44.9,44.9,0,0,0,0,9.481c.112.669.223,1.339.335,2.119v.335h-.335c-.558,0-1.115.112-1.562.112C13.917,85.191,6.89,70.356,2.54,61.432l-.112-.112.112-.112C4.547,57.082,9.344,53.066,16.148,50.055Z" transform="translate(0)" fill="#ec008c" fill-rule="evenodd"/><path d="M50.344,84.255a.916.916,0,0,0-.112-.781,1.086,1.086,0,0,0-1.45-.335A8.44,8.44,0,0,1,36.959,81.02c-.112-.223-.223-.335-.335-.558a1.176,1.176,0,0,0-1.562-.446,1.142,1.142,0,0,0-.558.669.916.916,0,0,0,.112.781h0a10.678,10.678,0,0,0,14.612,3.793c.223-.112.446-.335.669-.446A.741.741,0,0,0,50.344,84.255Z" transform="translate(3.952 4.06)" fill="#ec008c" fill-rule="evenodd"/><path d="M39.464,70.4a1.085,1.085,0,0,0,.446-2.119L36.341,67.5A1.147,1.147,0,0,0,35,68.283a1.212,1.212,0,0,0,.781,1.339h.112Z" transform="translate(4.013 2.631)" fill="#ec008c" fill-rule="evenodd"/><path d="M49.286,72.613l3.569.781a.976.976,0,0,0,1.227-.781,1.9,1.9,0,0,0-.112-.781.917.917,0,0,0-.669-.446L49.732,70.6a1.053,1.053,0,0,0-1.115,1.562,1.024,1.024,0,0,0,.669.446l-.112.223Z" transform="translate(5.569 2.986)" fill="#ec008c" fill-rule="evenodd"/><path d="M23.859,57.758l3.346-1.45a1.344,1.344,0,0,0,.558-.558.953.953,0,0,0,0-.781,1.344,1.344,0,0,0-.558-.558c-.112,0-.223-.112-.446-.112a.671.671,0,0,0-.446.112l-3.346,1.562a1.107,1.107,0,0,0-.558,1.45A1.405,1.405,0,0,0,23.859,57.758Z" transform="translate(2.551 1.109)" fill="#ec008c" fill-rule="evenodd"/><g transform="translate(10.933 61.209)"><path d="M10.466,61.4a1.344,1.344,0,0,0-.558.558.953.953,0,0,0,0,.781,1.107,1.107,0,0,0,1.45.558l3.346-1.562a1.1,1.1,0,1,0-.892-2.008L13.7,59.5l.112.223Z" transform="translate(-9.825 -59.5)" fill="#ec008c" fill-rule="evenodd"/><path d="M13.3,59.7Z" transform="translate(-9.424 -59.477)" fill="#ec008c" fill-rule="evenodd"/></g><g transform="translate(50.393 59.536)"><path d="M46.84,61.077A1.339,1.339,0,1,0,45.5,59.739h0A1.286,1.286,0,0,0,46.84,61.077Z" transform="translate(-45.167 -57.954)" fill="#ec008c" fill-rule="evenodd"/><path d="M46.873,61.346h0a1.673,1.673,0,1,1,1.673-1.673A1.716,1.716,0,0,1,46.873,61.346Zm0-2.677a1.115,1.115,0,1,0,1.115,1.115A1.118,1.118,0,0,0,46.873,58.669Z" transform="translate(-45.2 -58)" fill="#fff"/></g></g></svg>
                                            </div>
                                            <div class="title">
                                                <span>Talents</span>
                                            </div>
                                        </div>
                                        <div class="d-md-none">
                                            <h3>Talents</h3>
                                            <div>
                                                Exclusive access to top personalities and social influencers, offering talent management, influencer marketing and endorsement services.
                                            </div>
                                        </div>
                                    </a>
                                    <a class="nav-link" id="pill-activation" data-toggle="pill" href="#activation"
                                        role="tab" aria-controls="v-pills-activation" aria-selected="true">
                                        <div class="eco-btn">
                                            <div class="svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="54.527" height="56.305" viewBox="0 0 54.527 56.305"><g transform="translate(-685 -455)"><g transform="translate(695.132 459.273)"><path d="M740.98,475.508a.757.757,0,1,0,.316,1.48l3.186-.68a.757.757,0,0,0-.316-1.481Z" transform="translate(-709.146 -464.287)" fill="#ffc219"/><path d="M743.095,484.774a.773.773,0,0,0,1.047-.223.756.756,0,0,0-.223-1.046l-2.577-1.67a.757.757,0,1,0-.823,1.27Z" transform="translate(-709.093 -466.033)" fill="#ffc219"/><path d="M740.913,467.487a.756.756,0,0,0-1.374-.253l-1.671,2.577a.757.757,0,0,0,.223,1.046.774.774,0,0,0,1.047-.223l1.671-2.576A.755.755,0,0,0,740.913,467.487Z" transform="translate(-708.479 -462.282)" fill="#ffc219"/><path d="M710.992,490.954a2.93,2.93,0,0,0,2.253-3.475l-1.2-5.631,1.383-.3a10.111,10.111,0,0,1,7.464,1.321l1.267.821.038.1a2.928,2.928,0,0,0,5.6-1.643l-4.078-19.114a2.929,2.929,0,0,0-5.787.787l.007.108-.822,1.267a10.111,10.111,0,0,1-6.274,4.252l-6.371,1.359a2.935,2.935,0,0,0-2.292,2.486l-.031.23-.409.087a4.014,4.014,0,1,0,1.675,7.852l.409-.087.122.2a2.959,2.959,0,0,0,2.118,1.372l.231.031,1.22,5.719A2.931,2.931,0,0,0,710.992,490.954Zm8.482-27.009a1.414,1.414,0,1,1,2.767-.59l4.079,19.114a1.415,1.415,0,0,1-2.767.591Zm-7.584,6.811.287-.089a11.562,11.562,0,0,0,5.771-4.012l.434-.574,3.3,15.474-.63-.347a11.574,11.574,0,0,0-6.906-1.308l-.3.037Zm-8.467,9.164-.322.068a2.5,2.5,0,0,1-1.044-4.891l.322-.069Zm8.342,7.875a1.415,1.415,0,1,1-2.768.59l-1.2-5.63,2.767-.591Zm-6.7-7.452-1.359-6.371a1.417,1.417,0,0,1,1.089-1.679l5.631-1.2,1.949,9.139-5.63,1.2A1.417,1.417,0,0,1,705.06,480.343Z" transform="translate(-698.564 -460.721)" fill="#ffc219"/></g><path d="M721.93,492.257a2.8,2.8,0,0,0-2.8,2.8V504.8H717.8a2.8,2.8,0,0,0-2.8,2.8v5.713a8.205,8.205,0,0,0,4.538,7.342.732.732,0,0,0,.325.077h14.47a.727.727,0,0,0,.516-.214,7.734,7.734,0,0,0,2.28-5.505v-9.479a2.794,2.794,0,0,0-3.827-2.6l-.317.125-.089-.329a2.8,2.8,0,0,0-2.7-2.062,2.761,2.761,0,0,0-1.03.2l-.317.126-.09-.329a2.794,2.794,0,0,0-3.628-1.9l-.407.145v-3.856A2.8,2.8,0,0,0,721.93,492.257Zm4.134,7.8A1.34,1.34,0,0,1,727.4,501.4v6.2a.729.729,0,0,0,1.458,0v-4.134a1.338,1.338,0,1,1,2.676,0V507.6a.729.729,0,0,0,1.458,0v-2.067a1.338,1.338,0,1,1,2.676,0v9.479a6.317,6.317,0,0,1-1.556,4.157l-.092.1-14.052-.037a6.742,6.742,0,0,1-3.513-5.923V507.6a1.339,1.339,0,0,1,1.338-1.338h1.339v4.748a.729.729,0,0,0,1.458,0V495.054a1.338,1.338,0,1,1,2.676,0V507.6a.729.729,0,0,0,1.458,0v-6.2A1.34,1.34,0,0,1,726.064,500.062Z" transform="translate(-7.592 -9.428)" fill="#ffc219"/><path d="M694.494,487.119v-29.3a1.322,1.322,0,0,1,1.321-1.321h37.694a1.322,1.322,0,0,1,1.321,1.321v29.3h1.494v-29.3A2.817,2.817,0,0,0,733.508,455H695.815A2.817,2.817,0,0,0,693,457.815v29.3Z" transform="translate(-2.024)" fill="#ffc219"/><g transform="translate(686.494 492.347)"><rect width="7" height="2" transform="translate(44.506 -0.422)" fill="#ffc219"/><rect width="20" height="2" transform="translate(-0.494 -0.422)" fill="#ffc219"/></g><circle cx="1" cy="1" r="1" transform="translate(695 458.925)" fill="#ffc219"/><ellipse cx="1.5" cy="1" rx="1.5" ry="1" transform="translate(698 458.925)" fill="#ffc219"/><circle cx="1" cy="1" r="1" transform="translate(702 458.925)" fill="#ffc219"/><g transform="translate(685 461.723)"><path d="M686.494,500.027V466.815a1.322,1.322,0,0,1,1.321-1.321h3.908V464h-3.908A2.817,2.817,0,0,0,685,466.815v33.212a2.817,2.817,0,0,0,2.815,2.815h18.1v-1.494h-18.1A1.322,1.322,0,0,1,686.494,500.027Z" transform="translate(-685 -464)" fill="#ffc219"/><path d="M752.149,464h-2.414v1.494h2.414a1.322,1.322,0,0,1,1.321,1.321v33.212a1.322,1.322,0,0,1-1.321,1.321H746v1.494h6.149a2.817,2.817,0,0,0,2.814-2.815V466.815A2.817,2.817,0,0,0,752.149,464Z" transform="translate(-700.436 -464)" fill="#ffc219"/></g></g></svg>
                                            </div>
                                            <div class="title">
                                                <span>Activation</span>
                                            </div>
                                        </div>
                                        <div class="d-md-none">
                                            <h3>Activation</h3>
                                            <div>
                                                Targeted consumer engagement events integrated with strong content IPs, allowing brands to further elevate their experiential marketing campaigns. 
                                            </div>
                                        </div>
                                    </a>
                                    <a class="nav-link" id="pill-creative" data-toggle="pill" href="#creative"
                                        role="tab" aria-controls="v-pills-creative" aria-selected="true">
                                        <div class="eco-btn">
                                            <div class="svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="52.787" height="53.922" viewBox="0 0 52.787 53.922"><g transform="translate(-0.014 0)"><path d="M227.326,152.027a5.392,5.392,0,1,0,5.392,5.392A5.392,5.392,0,0,0,227.326,152.027Zm0,8.987a3.595,3.595,0,1,1,3.595-3.595A3.594,3.594,0,0,1,227.326,161.014Zm0,0" transform="translate(-196.991 -134.95)" fill="#00aeef"/><path d="M195.5,116.608v-3.595a.9.9,0,0,0-.719-.881l-1.594-.319a8.934,8.934,0,0,0-.359-.872l.9-1.348a.9.9,0,0,0-.112-1.134l-2.543-2.545a.9.9,0,0,0-1.134-.112l-1.348.9a8.935,8.935,0,0,0-.873-.359l-.322-1.6a.9.9,0,0,0-.881-.719H182.92a.9.9,0,0,0-.881.719l-.319,1.594a8.934,8.934,0,0,0-.873.359l-1.348-.9a.9.9,0,0,0-1.134.112l-2.545,2.543a.9.9,0,0,0-.112,1.134l.9,1.348a8.945,8.945,0,0,0-.36.873l-1.6.322a.9.9,0,0,0-.719.881v3.595a.9.9,0,0,0,.719.881l1.594.319a8.937,8.937,0,0,0,.36.873l-.9,1.348a.9.9,0,0,0,.112,1.134l2.543,2.545a.9.9,0,0,0,1.134.112l1.348-.9a8.941,8.941,0,0,0,.873.36l.322,1.6a.9.9,0,0,0,.881.719h3.595a.9.9,0,0,0,.881-.719l.319-1.594a9.049,9.049,0,0,0,.872-.359l1.348.9a.9.9,0,0,0,1.134-.112l2.545-2.543a.9.9,0,0,0,.112-1.134l-.9-1.348a8.935,8.935,0,0,0,.359-.873l1.6-.322A.9.9,0,0,0,195.5,116.608Zm-1.8-.737-1.391.279a.9.9,0,0,0-.687.634A7.183,7.183,0,0,1,191,118.3a.9.9,0,0,0,.037.936l.785,1.177-1.5,1.5-1.177-.785a.9.9,0,0,0-.935-.037,7.193,7.193,0,0,1-1.518.629.9.9,0,0,0-.634.688l-.275,1.387h-2.121l-.279-1.391a.9.9,0,0,0-.634-.688,7.2,7.2,0,0,1-1.518-.629.9.9,0,0,0-.935.037l-1.177.785-1.5-1.5.785-1.177a.9.9,0,0,0,.037-.936,7.207,7.207,0,0,1-.629-1.518.9.9,0,0,0-.688-.634l-1.387-.275v-2.121l1.391-.279a.9.9,0,0,0,.688-.635,7.194,7.194,0,0,1,.629-1.518.9.9,0,0,0-.037-.936l-.785-1.177,1.5-1.5,1.177.785a.9.9,0,0,0,.936.037,7.183,7.183,0,0,1,1.518-.629.9.9,0,0,0,.634-.688l.275-1.387h2.121l.279,1.391a.9.9,0,0,0,.634.687,7.171,7.171,0,0,1,1.518.629.9.9,0,0,0,.936-.037l1.177-.785,1.5,1.5-.785,1.177a.9.9,0,0,0-.037.936,7.173,7.173,0,0,1,.629,1.518.9.9,0,0,0,.688.634l1.387.275Zm0,0" transform="translate(-154.383 -92.342)" fill="#00aeef"/><path d="M261.934,56.027h1.8v3.595h-1.8Zm0,0" transform="translate(-232.498 -49.734)" fill="#00aeef"/><path d="M191.008,72.223l1.556-.9,1.8,3.113-1.556.9Zm0,0" transform="translate(-169.539 -63.313)" fill="#00aeef"/><path d="M141.223,122.662l.9-1.556,3.113,1.8-.9,1.556Zm0,0" transform="translate(-125.347 -107.502)" fill="#00aeef"/><path d="M125.934,192.027h3.595v1.8h-3.595Zm0,0" transform="translate(-111.775 -170.457)" fill="#00aeef"/><path d="M141.227,250.891l3.113-1.8.9,1.556-3.113,1.8Zm0,0" transform="translate(-125.35 -221.113)" fill="#00aeef"/><path d="M191,296.141l1.8-3.113,1.556.9-1.8,3.113Zm0,0" transform="translate(-169.536 -260.111)" fill="#00aeef"/><path d="M261.934,312.027h1.8v3.595h-1.8Zm0,0" transform="translate(-232.498 -276.977)" fill="#00aeef"/><path d="M319,293.919l1.556-.9,1.8,3.113-1.557.9Zm0,0" transform="translate(-283.157 -260.104)" fill="#00aeef"/><path d="M362.93,250.658l.9-1.556,3.113,1.8-.9,1.556Zm0,0" transform="translate(-322.149 -221.12)" fill="#00aeef"/><path d="M381.934,192.027h3.595v1.8h-3.595Zm0,0" transform="translate(-339.018 -170.457)" fill="#00aeef"/><path d="M362.934,122.895l3.113-1.8.9,1.556-3.113,1.8Zm0,0" transform="translate(-322.152 -107.495)" fill="#00aeef"/><path d="M319,74.434l1.8-3.113,1.556.9-1.8,3.113Zm0,0" transform="translate(-283.154 -63.309)" fill="#00aeef"/><path d="M52.727,20.631A22.526,22.526,0,0,0,31.8.052,22.279,22.279,0,0,0,14.967,6.085a22.556,22.556,0,0,0-7.1,16.034L.481,30.247a1.8,1.8,0,0,0,1.331,3.006H6.071v5.392a6.3,6.3,0,0,0,6.291,6.291h7.189v8.088a.9.9,0,0,0,.9.9H45.611a.9.9,0,0,0,.9-.9V38.059A22.5,22.5,0,0,0,52.727,20.631ZM44.975,37.057v.006a20.8,20.8,0,0,1-4.195,3.248l.908,1.55a22.561,22.561,0,0,0,3.024-2.143V52.125H21.348V44.935a1.8,1.8,0,0,0-1.8-1.8H12.361a4.5,4.5,0,0,1-4.493-4.493V32.354a.9.9,0,0,0-.9-.9H1.811l7.621-8.381a.9.9,0,0,0,.234-.605,20.669,20.669,0,1,1,35.31,14.588Zm0,0" transform="translate(0 0)" fill="#00aeef"/></g></svg>
                                            </div>
                                            <div class="title">
                                                <span>Creative Solutions</span>
                                            </div>
                                        </div>
                                        <div class="d-md-none">
                                            <h3>Creative Solutions</h3>
                                            <div>
                                                End-to-end integrated solutions customized for brands from ideation to content production and campaign fulfilment, using creativity at the core of its solution to grab the attention of today's audience and drive real engagement.
                                            </div>
                                        </div>
                                    </a>
                                    <a class="nav-link" id="pill-marketing" data-toggle="pill" href="#marketing"
                                        role="tab" aria-controls="v-pills-marketing" aria-selected="true">
                                        <div class="eco-btn">
                                            <div class="svg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="54.252" height="54.252" viewBox="0 0 54.252 54.252"><path d="M43.39,19.555a15.369,15.369,0,0,0-15.5,14.9A15.183,15.183,0,0,0,30.7,43.709a17.013,17.013,0,0,1,3.068,9.922v4.087a.741.741,0,0,0,.263.566l2.7,2.282v2.783A2.173,2.173,0,0,0,38.9,65.518h8.578a2.173,2.173,0,0,0,2.171-2.171V60.565l2.7-2.282a.741.741,0,0,0,.263-.566V53.629a17.572,17.572,0,0,1,3.163-10.054A15.307,15.307,0,0,0,43.39,19.555Zm5.249,39.926H37.746L35.6,57.67H50.781Zm2.49-3.295H35.255V54.371H51.129Zm-2.96,7.163a.689.689,0,0,1-.688.688H38.9a.689.689,0,0,1-.688-.688V60.965h9.954Zm6.389-20.623a19.057,19.057,0,0,0-3.415,10.162h-15.9a18.385,18.385,0,0,0-3.328-10.039,13.716,13.716,0,0,1-2.538-8.356A13.875,13.875,0,0,1,43.2,21.037h.176A13.824,13.824,0,0,1,54.561,42.725Z" transform="translate(-16.066 -11.266)" fill="#ec118c"/><path d="M58.185,44.415a8.434,8.434,0,0,0-.979-2.374l.972-.973a.741.741,0,0,0,0-1.049l-1.765-1.765a.741.741,0,0,0-1.049,0l-.967.967a8.431,8.431,0,0,0-2.377-.989V36.868a.742.742,0,0,0-.742-.742h-2.5a.742.742,0,0,0-.742.742v1.364a8.43,8.43,0,0,0-2.379.989l-.967-.967a.741.741,0,0,0-1.049,0l-1.765,1.765a.741.741,0,0,0,0,1.049l.972.973a8.434,8.434,0,0,0-.979,2.374h-1.38a.742.742,0,0,0-.742.742v2.5a.742.742,0,0,0,.742.742h1.391a8.419,8.419,0,0,0,.983,2.358l-.989.989a.741.741,0,0,0,0,1.049l1.765,1.765a.763.763,0,0,0,1.049,0l.994-.994a8.425,8.425,0,0,0,2.35.972v1.408a.742.742,0,0,0,.742.742h2.5a.742.742,0,0,0,.742-.742V54.533a8.425,8.425,0,0,0,2.352-.972l.994.994a.741.741,0,0,0,1.049,0l1.765-1.765a.741.741,0,0,0,0-1.049l-.986-.989a8.419,8.419,0,0,0,.983-2.357h1.39a.742.742,0,0,0,.742-.742v-2.5a.742.742,0,0,0-.742-.742Zm.638,2.5H57.57a.741.741,0,0,0-.732.619,6.912,6.912,0,0,1-1.2,2.882.741.741,0,0,0,.078.961l.893.89-.716.716-.9-.9A.742.742,0,0,0,54.037,52a6.915,6.915,0,0,1-2.882,1.191.741.741,0,0,0-.618.734V55.2H49.52V53.928A.741.741,0,0,0,48.9,53.2,6.915,6.915,0,0,1,46.018,52a.742.742,0,0,0-.956.079l-.9.9-.716-.716.895-.891a.741.741,0,0,0,.078-.958,6.912,6.912,0,0,1-1.2-2.882.741.741,0,0,0-.732-.619H41.233V45.9h1.245a.742.742,0,0,0,.732-.624,6.913,6.913,0,0,1,1.194-2.9.742.742,0,0,0-.079-.956l-.875-.875.716-.716.871.871a.742.742,0,0,0,.958.078,6.9,6.9,0,0,1,2.9-1.207.742.742,0,0,0,.62-.735V37.608h1.017v1.229a.742.742,0,0,0,.622.732,6.9,6.9,0,0,1,2.9,1.208.741.741,0,0,0,.958-.078l.871-.871.716.716-.875.875a.742.742,0,0,0-.079.956,6.913,6.913,0,0,1,1.194,2.9.742.742,0,0,0,.732.623h1.245Z" transform="translate(-22.902 -20.814)" fill="#ec118c"/><path d="M56.913,48.025a5.213,5.213,0,1,0,5.213,5.213,5.213,5.213,0,0,0-5.213-5.213Zm0,8.946a3.73,3.73,0,1,1,3.73-3.73,3.73,3.73,0,0,1-3.73,3.73Z" transform="translate(-29.787 -27.67)" fill="#ec118c"/><path d="M62.992,5.757a.742.742,0,0,0,.742-.742V.742a.742.742,0,0,0-1.483,0V5.015a.742.742,0,0,0,.742.742Z" transform="translate(-35.866 0)" fill="#ec118c"/><path d="M102.377,18.45l-3.022,3.022a.742.742,0,0,0,1.049,1.049l3.022-3.022a.742.742,0,0,0-1.049-1.049Z" transform="translate(-57.119 -10.505)" fill="#ec118c"/><path d="M119.432,62.25h-4.274a.742.742,0,0,0,0,1.483h4.274a.742.742,0,0,0,0-1.483Z" transform="translate(-65.922 -35.866)" fill="#ec118c"/><path d="M5.015,62.25H.742a.742.742,0,0,0,0,1.483H5.015a.742.742,0,0,0,0-1.483Z" transform="translate(0 -35.866)" fill="#ec118c"/><path d="M22.521,21.472,19.5,18.45A.742.742,0,0,0,18.45,19.5l3.022,3.022a.742.742,0,1,0,1.049-1.049Z" transform="translate(-10.505 -10.505)" fill="#ec118c"/></svg>
                                            </div>
                                            <div class="title">
                                                <span>Data-Driven Marketing</span>
                                            </div>
                                        </div>
                                        <div class="d-md-none">
                                            <h3>Data-Driven Marketing</h3>
                                            <div>
                                                Enhance targeted marketing by audience segment based on TV Return-Path-Data, regionalised our channels for geographical targeting and personalized ad-serving via Addressable TV to improve performance.
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div class="tab-content d-none d-md-block">
                                    <div class="tab-pane fade show active" id="tv" role="tabpanel"
                                        aria-labelledby="tv-tab">
                                        <h3>TV</h3>
                                        <div>
                                            Largest Pay-TV operator in SEA. Serving 5.7 mil Households with 75% Penetration across 204 channels.
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="radio" role="tabpanel" aria-labelledby="radio-tab">
                                        <h3>Radio</h3>
                                        <div>
                                            <b>#1</b> in every language –  English, Malay, Chinese and Tamil. 16.9 mil weekly listeners across 11 radio brands. Digitalising terrestrial radio via SYOK.
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="web" role="tabpanel" aria-labelledby="web-tab">
                                        <h3>Web, Mobile & OTT</h3>
                                        <div>
                                            We offer a full suite of digital services across 47 digital brands with a reach of 13.9 mil monthly unique visitors. Expand individuals’ experience via Astro GO, HBO Go and iQIYI.
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="ip" role="tabpanel" aria-labelledby="ip-tab">
                                        <h3>Local & International IPs</h3>
                                        <div>
                                            #1 premium content creator & aggregator in the region. Collectively grossed over RM75 million GBO in 2019 and dominated 7 of top 10 highest grossing Malaysian films of all time.
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="commerce" role="tabpanel"
                                        aria-labelledby="commerce-tab">
                                        <h3>Commerce</h3>
                                        <div>
                                            24/7 multilanguage TV & ECMC shopping experience with up to 2.4 mil registered customers.
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="talents" role="tabpanel"
                                        aria-labelledby="talents-tab">
                                        <h3>Talents</h3>
                                        <div>
                                            Exclusive access to top personalities and social influencers, offering talent management, influencer marketing and endorsement services.
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="activation" role="tabpanel"
                                        aria-labelledby="activation-tab">
                                        <h3>Activation</h3>
                                        <div>
                                            Targeted consumer engagement events integrated with strong content IPs, allowing brands to further elevate their experiential marketing campaigns. 
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="creative" role="tabpanel"
                                        aria-labelledby="creative-tab">
                                        <h3>Creative Solutions</h3>
                                        <div>
                                            End-to-end integrated solutions customized for brands from ideation to content production and campaign fulfilment, using creativity at the core of its solution to grab the attention of today's audience and drive real engagement.
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="marketing" role="tabpanel"
                                        aria-labelledby="marketing-tab">
                                        <h3>Data-Driven Marketing</h3>
                                        <div>
                                            Enhance targeted marketing by audience segment based on TV Return-Path-Data, regionalised our channels for geographical targeting and personalized ad-serving via Addressable TV to improve performance.
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-5">
                                    <a class="btn btn-pink btn-shadow mr-2" href="./credentials">Our Credentials</a>
                                    <a class="btn btn-pink btn-shadow" href="./our-brands">Our Brands</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <!-- Slider Highlights End -->
                <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                    <jdoc:include type="modules" name="banner" style="xhtml" />
                    <?php if ($this->countModules('breadcrumbs')) : ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <jdoc:include type="modules" name="breadcrumbs" style="xhtml" />
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <?php if ($this->countModules('sidebar-left')) : ?>
                        <div id="sidebar" class="col-md-3">
                            <div class="sidebar-nav">
                                <jdoc:include type="modules" name="sidebar-left" style="xhtml" />
                            </div>
                        </div>
                        <?php endif; ?>
                        <main id="content" role="main" class="<?php echo $span; ?>">
                            <jdoc:include type="modules" name="position-3" style="xhtml" />
                            <jdoc:include type="message" />
                            <jdoc:include type="component" />
                            <!-- <jdoc:include type="modules" name="position-2" style="none" />  -->
                        </main>
                        <?php if ($this->countModules('sidebar-right')) : ?>
                        <div id="aside" class="col-md-3">
                            <jdoc:include type="modules" name="sidebar-right" style="xhtml" />
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <footer class="btm-footer" role="contentinfo">
            <!-- <jdoc:include type="modules" name="viewership" style="none" /> -->
            <jdoc:include type="modules" name="footer" style="none" />

            <?php if(JModuleHelper::renderModule(array_shift(JModuleHelper::getModules( 'quake-footer' )))){?>
            
            <div class="qperks-howto text-center">
                <div class="container ">
                  <h2 class="f-30 text-white">How do I collect Q-Perks?</h2>

                  <div class="row justify-content-between qperks-howto-wrapper">
                    <div class="col-lg-7">
                        <h3>Via activities performed</h3>
                        <div class="owl-carousel owl-theme row mx-0 justify-content-center mt-4" id="q-perks">
                          <div class="col-lg-3">
                          
                            <div class="perks-holder">
                              <div class="bubble-holder">
                                <span>+100</span>
                                <img alt="First-time sign up" srcset="images/2020/club/reward-01@2x.png 2x, images/2020/club/reward-01.png 1x " src="images/2020/club/reward-01.png" >
                              </div>
                              <h4>First-time sign up</h4>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="perks-holder">
                              <div class="bubble-holder">
                                <span>+10</span>
                                <img alt="" srcset="images/2020/club/reward-03@2x.png 2x, images/2020/club/reward-03.png 1x " src="images/2020/club/reward-03.png" >
                              </div>
                              <h4>Profile update</h4>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="perks-holder">
                              <div class="bubble-holder">
                                <span>+10</span>
                                <img alt="Social media linking" srcset="images/2020/club/reward-05@2x.png 2x, images/2020/club/reward-05.png 1x " src="images/2020/club/reward-05.png" >
                              </div>
                              <h4>Social media linking</h4>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="perks-holder">
                              <div class="bubble-holder">
                                <span>+20</span>
                                <img alt="Join Quake WhatsApp" srcset="images/2020/club/reward-10@2x.png 2x, images/2020/club/reward-10.png 1x " src="images/2020/club/reward-10.png" >
                              </div>
                              <h4>Join Quake WhatsApp</h4>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="perks-holder">
                              <div class="bubble-holder">
                                <span>+50</span>
                                <img alt="Friend referral" srcset="images/2020/club/reward-06@2x.png 2x, images/2020/club/reward-06.png 1x " src="images/2020/club/reward-06.png" >
                              </div>
                              <h4>Friend referral</h4>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="perks-holder">
                              <div class="bubble-holder">
                                <span>+80</span>
                                <img alt="Event attendance" srcset="images/2020/club/reward-07@2x.png 2x, images/2020/club/reward-07.png 1x " src="images/2020/club/reward-07.png" >
                              </div>
                              <h4>Event attendance</h4>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="perks-holder">
                              <div class="bubble-holder">
                                <span>+200</span>
                                <img alt="Birthday gift redemption" srcset="images/2020/club/reward-08@2x.png 2x, images/2020/club/reward-08.png 1x " src="images/2020/club/reward-08.png" >
                              </div>
                              <h4>Birthday gift redemption</h4>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="perks-holder engagement">
                              <div class="bubble-holder">
                                <span>+20<br>
                                    <small>per video</small>
                                </span>
                                <img alt="Watch video on QuakeCast" srcset="images/2020/club/reward-watch@2x.png 2x, images/2020/club/reward-watch.png 1x " src="images/2020/club/reward-watch.png" >
                              </div>
                              <h4>Watch video on QuakeCast</h4>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-lg-5 points-container-border">
                        <h3>Via social media sharing</h3>
                        <div class="row mt-4 justify-content-center">
                            <div class="col-xl-5 col-sm-4">
                                <div class="engagement">
                                    <div class="perks-holder">
                                        <div class="bubble-holder">
                                            <span>
                                                +3 <small>per<br/>engagement</small>
                                            </span>
                                            <img alt="Content sharing on social media" srcset="images/2020/club/engagment@2x.png 2x, images/2020/club/engagment@2x.png 1x " src="images/2020/club/engagment@2x.png" >
                                        </div>
                                        <h4>Content sharing on social media</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-5 col-sm-4">
                                <div class="engagement">
                                    <div class="perks-holder">
                                        <div class="bubble-holder">
                                            <span>
                                                +50 <br>
                                                <small>per video</small>
                                            </span>
                                            <img alt="Share QuakeCast video on Social Media" srcset="images/2020/club/reward-share@2x.png 2x, images/2020/club/reward-share.png 1x " src="images/2020/club/reward-share.png" >
                                        </div>
                                        <h4>Share QuakeCast video on Social Media</h4>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                  </div>
                  
                  
                  <a href="./quake-club/frequently-asked-questions#faq-8" class="btn btn-pink btn-wide mt-3">Read more</a>
            
                </div>
            
              </div>

            <?php } ?>

            <div class="footer" id="home-footer">
                <div class="container">
                    <div class="row mb-4 mb-lg-5 align-items-center">
                        <div class="col-6">
                            <img src="images/2020/astro-media-solutions.png" alt="Astro Media Sales" width="95"
                                class="astro-media">
                        </div>
                        <div class="col-6 text-right">
                            <h4 class="upper d-sm-inline-block d-block">Follow us on</h4>
                            <a href="https://www.facebook.com/QuakeMY" target="_blank"><i
                                    class="ml-2 fab fa-facebook-square text-highlight"></i></a>
                            <a href="https://www.youtube.com/channel/UCR6P2vsz2ncymL8OCL7CCMw/featured"
                                target="_blank"><i class="ml-2 fab fa-youtube-square text-highlight"></i></a>
                            <a href="https://www.instagram.com/quakeseries/" target="_blank"><i class="ml-2 fab fa-instagram-square text-highlight"></i></a>
                            <!-- <a href="https://api.whatsapp.com/send?phone=60126040968&text=Hi,%20send%20me%20the%20monthly%20issue%20of%20Winning%20Partnership%20Series%20by%20MARKETING" target="_blank"><i class="ml-2 fab fa-whatsapp-square text-highlight"></i></a> -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 mb-3 mb-md-0">
                            <ul class="upper list-unstyled mb-0">
                                <li>
                                    <a href="./privacy-policy">
                                        <h4>Privacy Policy</h4>
                                    </a>
                                </li>
                                <li>
                                    <a href="./privacy-notice">
                                        <h4>Privacy Notice</h4>
                                    </a>
                                </li>
                                <li>
                                    <a href="./advertising-terms-and-conditions">
                                        <h4>Advertising Terms & Conditions</h4>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 mb-4 mb-md-0">
                            <h4 class="">
                                <span class="upper">Astro Media Solutions Sdn Bhd</span><br>
                                <em><small>Company No. 200501026797 (708931-H)</small></em>
                            </h4>
                            <div>
                                <address class="mb-2">
                                    All Asia Broadcast Centre, <br />
                                    Technology Park Malaysia,<br />
                                    Bukit Jalil, 57000 Kuala Lumpur.
                                </address>
                                <a href="./enquiry" class="bold text-highlight">Be in Touch</a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h4 class="upper">Subscribe to Our Newsletter</h4>
                            <p>Be in the know of all things Quake</p>

                            <form class="input-group" action="" id="subscribe-form">
                                <input id="subscribe-email" type="email" class="form-control"
                                    placeholder="What's Your Email Address?" aria-label="Email Address"
                                    aria-describedby="button-addon2" required
                                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                                <div class="input-group-append">
                                    <div class="input-group-text" id="loading-icon"></div>
                                    <button class="btn btn-link" type="submit" id="button-addon2">Subscribe</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="container<?// echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>"> -->
            <div class="container">
                <div class="copyright">
                &#8482; <?php echo date('Y'); ?> Astro Quake Rights Reserved.
                </div>
            </div>

            <a id="backTop">
                <i class="fas fa-chevron-up"></i>
            </a>
        </footer>
        <jdoc:include type="modules" name="debug" style="none" />
    </body>


</html>
<script>
var checkEmailUrl = <?php echo json_encode($checkEmailUrl); ?>;
var createClientUrl = <?php echo json_encode($createClientUrl); ?>;
var banner_viewership = <?php echo json_encode($cus_banner_v1ewership); ?>;
var current_alias = <?php echo json_encode($menu-> getActive()-> alias); ?>;

//bind viewership data from db
jQuery('#viewership-figure-1').html(banner_viewership[0]['block_1_title']);
jQuery('#viewership-content-1').html(banner_viewership[0]['block_1_description']);

jQuery('#viewership-figure-2').html(banner_viewership[0]['block_2_title']);
jQuery('#viewership-content-2').html(banner_viewership[0]['block_2_description']);

jQuery('#viewership-figure-3').html(banner_viewership[0]['block_3_title']);
jQuery('#viewership-content-3').html(banner_viewership[0]['block_3_description']);
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bcd3eb6f160f08f"></script>
<!-- <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer></script> -->
<?php
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/owl.carousel.min.js', array('version'=>'auto'));
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/custom.js', array('version'=>'auto'));
?>

<script>
var updateUserShareArticlePointUrl = <?php echo json_encode($updateUserShareArticlePointUrl); ?>;
var current_path = window.location.pathname.split("/").pop();
jQuery(document).ready(function() {
    console.log(current_path);
    jQuery('#noticeModal').modal('show');
    
    // if (current_path == "prosperatty-rush") {
    //     jQuery("#home-footer").hide();
    // }
});

var addthis_config = {
    data_track_clickback: true
}
// Alert a message when the user shares somewhere
function eventHandler(evt) {
    switch (evt.type) {
        case "addthis.menu.open":
            // console.log('menu opened; surface=' + evt.data.pane);
            break;
        case "addthis.menu.close":
            // console.log('menu closed; surface=' + evt.data.pane);
            break;
        case "addthis.menu.share":
            // console.log('user shared to ' + evt.data.service);
            jQuery.ajax({
                url: updateUserShareArticlePointUrl,
                type: 'post',
                data: {
                    'source': source,
                    'shareOrDownloadStatus': shareOrDownloadStatus
                },
                success: function(result) {
                    // console.log("success");

                },
                error: function() {
                    // console.log('fail');
                }
            });
            break;
        case "addthis.user.clickback":
            // console.log('user clickback to ' + evt.data.service);
        default:
            // console.log('received an unexpected event', evt);
    }

    if (evt.type == 'addthis.menu.share') {
        // console.log(typeof(evt.data)); // evt.data is an object hash containing all event data
        // console.log(evt.data.service); // evt.data.service is specific to the "addthis.menu.share" event
    }
}

// Listen to various events
addthis.addEventListener('addthis.menu.open', eventHandler);
addthis.addEventListener('addthis.menu.close', eventHandler);
addthis.addEventListener('addthis.menu.share', eventHandler);
</script>