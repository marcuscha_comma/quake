jQuery(document).ready(function () {
    //Carousel js
    jQuery('#index-banner').owlCarousel({
        loop: true,
        items: 1,
        video: true,
        lazyLoad: true,
        autoplay: true,
        autoplayTimeout: 8000,
    })

    jQuery('#highlight-banner').owlCarousel({
        loop: true,
        lazyLoad: true,
        margin: 30,
        autoplay: true,
        autoplayTimeout: 8000,
        autoHeight: true,

        responsive: {
            // breakpoint from 0 up
            0: {
                items: 1,
            },
            // breakpoint from 768 up
            992: {
                items: 2,
                slideBy: 2,
            }
        }
    })

    jQuery('#subscribe-form').submit(function (e) {
        e.preventDefault();
        var email_val = jQuery('#subscribe-email').val();
        var name_val = email_val.substring(0, email_val.indexOf("@"));
        jQuery("#loading-icon").prepend('<i id="load-icon" class="fas fa-spinner fa-spin text-muted"></i>');
        jQuery("#load-icon").remove();
        console.log(name_val);


        jQuery.ajax({
            url: checkEmailUrl,
            type: 'post',
            data: { email: email_val },
            success: function (result) {
                if (result == 1) {
                    console.log("aldy exist");
                    jQuery("#loading-icon").prepend('<i id="load-icon" class="fas fa-check text-success"></i>');
                    jQuery('#subscribe-email').val('');
                } else {
                    jQuery.ajax({
                        url: createClientUrl,
                        type: 'post',
                        data: { email: email_val, user_type: 1 },
                        success: function (result) {
                            jQuery("#loading-icon").prepend('<i id="load-icon" class="fas fa-check text-success"></i>');
                        },
                        error: function () {
                            console.log("fail");
                            jQuery("#loading-icon").prepend('<i id="load-icon" class="fas fa-close text-danger"></i>');
                        }
                    });
                    jQuery('#subscribe-email').val('');
                }
            }
        });
    })
});
