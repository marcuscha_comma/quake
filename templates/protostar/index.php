<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/** @var JDocumentHtml $this */

$app  = JFactory::getApplication();
$user = JFactory::getUser();
$updateUserShareArticlePointUrl = JRoute::_('index.php?option=com_quake_club_qperks&task=qperksuserpoint.addPoint');


// Output as HTML5
$this->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');

if ($task === 'edit' || $layout === 'form')
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
// JHtml::_('bootstrap.framework');


// Add html5 shiv
// JHtml::_('script', 'jui/html5.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));

// Add Stylesheets

// JHtml::_('stylesheet', 'bootstrap.min.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'style.css', array('version' => 'auto', 'relative' => true));

// Add template js
// JHtml::_('script', 'template.js', array('version' => 'auto', 'relative' => true));
// JHtml::_('script', 'popper.min.js', array('version' => 'auto', 'relative' => true));
// JHtml::_('script', 'bootstrap.min.js', array('version' => 'auto', 'relative' => true));

// Use of Google Font
// if ($this->params->get('googleFont'))
// {
// 	JHtml::_('stylesheet', 'https://fonts.googleapis.com/css?family=' . $this->params->get('googleFontName'));
// 	$this->addStyleDeclaration("
// 	h1, h2, h3, h4, h5, h6, .site-title {
// 		font-family: '" . str_replace('+', ' ', $this->params->get('googleFontName')) . "', sans-serif;
// 	}");
// }

// Template color
if ($this->params->get('templateColor'))
{
	$this->addStyleDeclaration('
	body.site {
		border-top: 3px solid ' . $this->params->get('templateColor') . ';
		background-color: ' . $this->params->get('templateBackgroundColor') . ';
	}
	a {
		color: ' . $this->params->get('templateColor') . ';
	}
	.nav-list > .active > a,
	.nav-list > .active > a:hover,
	.dropdown-menu li > a:hover,
	.dropdown-menu .active > a,
	.dropdown-menu .active > a:hover,
	.nav-pills > .active > a,
	.nav-pills > .active > a:hover,
	.btn-primary {
		background: ' . $this->params->get('templateColor') . ';
	}');
}

// Check for a custom CSS file
// JHtml::_('stylesheet', 'user.css', array('version' => 'auto', 'relative' => true));

// Check for a custom js file
// JHtml::_('script', 'user.js', array('version' => 'auto', 'relative' => true));

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
$position7ModuleCount = $this->countModules('position-7');
$position8ModuleCount = $this->countModules('position-8');

if ($position7ModuleCount && $position8ModuleCount)
{
	$span = 'span6';
}
elseif ($position7ModuleCount && !$position8ModuleCount)
{
	$span = 'span9';
}
elseif (!$position7ModuleCount && $position8ModuleCount)
{
	$span = 'span9';
}
else
{
	$span = 'span12';
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<link rel="icon" href="../../images/2020/rsvp/raya/raya-favicon.png" type="image/png">
	<link rel="apple-touch-icon" sizes="57x57" href="../../images/2020/rsvp/raya/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="../../images/2020/rsvp/raya/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../../images/2020/rsvp/raya/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="../../images/2020/rsvp/raya/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../../images/2020/rsvp/raya/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="../../images/2020/rsvp/raya/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="../../images/2020/rsvp/raya/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="../../images/2020/rsvp/raya/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="../../images/2020/rsvp/raya/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="../../images/2020/rsvp/raya/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../images/2020/rsvp/raya/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="../../images/2020/rsvp/raya/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../images/2020/rsvp/raya/favicon-16x16.png">
	<link rel="manifest" href="../../images/2020/rsvp/raya/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="../../images/2020/rsvp/raya/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

	<!-- jQuery library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>


<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="./templates/protostar/js/turnjs4/lib/turn.min.js"></script>
<script>
	PleaseRotateOptions = {
		forcePortrait: false, // if you would prefer to force portrait mode
		subMessage: "",
    	allowClickBypass: false,
	};
</script>
<script src="./templates/protostar/js/pleaserotate/pleaserotate.min.js"></script>

	<jdoc:include type="head" />
</head>
<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '')
	. ($this->direction === 'rtl' ? ' rtl' : '');
?>">
	<!-- Body -->
	<div class="body" id="top">
		<!-- <div class="flower">
			<img src="../../../../../images/rsvp/flower.png" alt="" class="img-fluid">
		</div> -->
		
		<div class="container-fluid">
			<!-- Header -->
			<!-- <header class="header" role="banner">
				<div class="header-inner clearfix">
					<a class="brand pull-left" href="<?php //echo $this->baseurl; ?>/">
						<?php //echo $logo; ?>
						<?php //if ($this->params->get('sitedescription')) : ?>
							<?php //echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription'), ENT_COMPAT, 'UTF-8') . '</div>'; ?>
						<?php //endif; ?>
					</a>
					<div class="header-search pull-right">
						<jdoc:include type="modules" name="position-0" style="none" />
					</div>
				</div>
			</header> -->
			<?php if ($this->countModules('position-1')) : ?>
				<nav class="navigation" role="navigation">
					<div class="navbar pull-left">
						<a class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
							<span class="element-invisible"><?php echo JTEXT::_('TPL_PROTOSTAR_TOGGLE_MENU'); ?></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
					</div>
					<div class="nav-collapse">
						<jdoc:include type="modules" name="position-1" style="none" />
					</div>
				</nav>
			<?php endif; ?>
			<jdoc:include type="modules" name="banner" style="xhtml" />
			<div class="row-fluid">
				<?php if ($position8ModuleCount) : ?>
					<!-- Begin Sidebar -->
					<div id="sidebar" class="span3">
						<div class="sidebar-nav">
							<jdoc:include type="modules" name="position-8" style="xhtml" />
						</div>
					</div>
					<!-- End Sidebar -->
				<?php endif; ?>
				<main id="content" role="main" class="<?php echo $span; ?>">
					<!-- Begin Content -->
					<jdoc:include type="modules" name="position-3" style="xhtml" />
					<jdoc:include type="message" />
					<jdoc:include type="component" />
					<div class="clearfix"></div>
					<jdoc:include type="modules" name="position-2" style="none" />
					<!-- End Content -->
				</main>
				<?php if ($position7ModuleCount) : ?>
					<div id="aside" class="span3">
						<!-- Begin Right Sidebar -->
						<jdoc:include type="modules" name="position-7" style="well" />
						<!-- End Right Sidebar -->
					</div>
				<?php endif; ?>
			</div>
		</div>

		
	</div>
	<!-- Footer -->
	<!-- <footer class="footer" role="contentinfo">
		<div class="container<?php //echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
			<hr />
			<jdoc:include type="modules" name="footer" style="none" />
			<p class="pull-right">
				<a href="#top" id="back-top">
					<?php //echo JText::_('TPL_PROTOSTAR_BACKTOTOP'); ?>
				</a>
			</p>
			<p>
				&copy; <?php //echo date('Y'); ?> <?php //echo $sitename; ?>
			</p>
		</div>
	</footer> -->





	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
<script>
	var updateUserShareArticlePointUrl = <?php echo json_encode($updateUserShareArticlePointUrl); ?>;
	var shareOrDownloadStatus = "shareEbook";
	var addthis_config = {
		data_track_clickback: true
	}
	// Alert a message when the user shares somewhere
	function eventHandler(evt) {
		switch (evt.type) {
			case "addthis.menu.open":
				// console.log('menu opened; surface=' + evt.data.pane);
				break;
			case "addthis.menu.close":
				// console.log('menu closed; surface=' + evt.data.pane);
				break;
			case "addthis.menu.share":
				// console.log('user shared to ' + evt.data.service);
				console.log(shareOrDownloadStatus);

				jQuery.ajax({
					url: updateUserShareArticlePointUrl,
					type: 'post',
					data: {
						'source': 'share-senyuman-raya-2020',
						'shareOrDownloadStatus': 'shareEbook'
					},
					success: function(result) {
						// console.log("success");

					},
					error: function() {
						// console.log('fail');
					}
				});
				break;
			case "addthis.user.clickback":
				// console.log('user clickback to ' + evt.data.service);
			default:
				// console.log('received an unexpected event', evt);
		}
	}

	// Listen to various events
	addthis.addEventListener('addthis.menu.open', eventHandler);
	addthis.addEventListener('addthis.menu.close', eventHandler);
	addthis.addEventListener('addthis.menu.share', eventHandler);

</script>
