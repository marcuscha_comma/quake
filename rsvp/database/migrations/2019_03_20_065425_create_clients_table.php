<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comp_name');
            $table->string('title');
            $table->string('name');
            $table->string('nric');
            $table->string('designation')->nullable();
            $table->string('email');
            $table->string('mobile_num');
            $table->string('office_num')->nullable();
            $table->boolean('is_attended')->default(0);
            $table->boolean('is_valid')->default(1);
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
