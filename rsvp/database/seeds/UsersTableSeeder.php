<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'type' => 'admin',
            'name' => 'Admin',
            'email' => 'admin@quake.com',
            'username' => 'admin',
            'password' => bcrypt('comma5157')
        ]);
    }
}
