<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordRequest;

use Hash;

use App\Models\User;

class ChangePasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function showPassword(){
        return view('auth.change');
    }

    public function updatePassword(PasswordRequest $request){

        if(Auth::check()){
            $current_pw = Auth::user()->password;
            if(Hash::check($request->current_pw,$current_pw)){

                $user_id = Auth::user()->id;
                $user = User::find($user_id);
                $user->password = bcrypt($request->password);
                $user->save();

                return redirect()->route('password.change')->with('success','Password updated');
            }else{
                return redirect()->route('password.change')->with('failed','Password not updated');
            }

        }
        return redirect()->route('password.change');
    }
}
