<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Models\Log;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function log($type, $message, $table, $data)
    {
        $log = new Log;
        $log->type = $type;
        $log->message = $message;
        $log->table = $table;
        $log->data = $data;
        $log->user_id = (Auth::user()) ? Auth::user()->id : 0;
        $log->save();

    }

}
