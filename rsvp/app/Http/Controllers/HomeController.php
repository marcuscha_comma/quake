<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Client;
use App\Models\Clientpoint;
use App\Models\Monthlypoint;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['countAttendanceBySession', 'customSearch', 'site']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function countAttendanceBySession() {
        $params['overall_attending'] = Client::where('is_valid', 1)->where('is_attended', 1)->where('is_deleted', 0)->count();
        $params['overall_total'] = Client::where('is_valid', 1)->where('is_deleted', 0)->count();

        return response()->json($params);
    }

    public function searchAndSort(Request $request){

        DB::enableQueryLog();
        $companyName = $request->get('company_name');
        $name = $request->get('name');
        $limit = $request->get('limit');

        if($request->has('sort_header') && $request->has('sort_by')) {
            $sortHeader = $request->get('sort_header');
            $sortBy = $request->get('sort_by');

        }

        // $events = DB::connection('mysql2')->select('select u.id, u.name, u.email, u.mobileNo, u.quakeClubUser, c.name as comp_name, c.designation from qkpe1_users u join qkpe1_cus_qperks_company_user c on c.user_id = u.id where u.quakeClubUser =-1 or u.quakeClubUser =1')->paginate($limit);
        // $events = DB::connection('mysql2')->select('u.id', 'u.name', 'u.email', 'u.mobileNo', 'c.name as comp_name', 'c.designation', 'qkpe1_users u')->join('qkpe1_cus_qperks_company_user c', 'c.user_id', '=', 'u.id')->paginate($limit)->get();

        // $events = Client::where('quakeClubUser', -1)->orWhere('quakeClubUser', 1)->company()->get();
        $events = Client::leftJoin('users', 'users.id', '=', 'cus_rsvp_tour_data.user_id')->leftJoin('cus_qperks_company_user', 'users.id', '=', 'cus_qperks_company_user.user_id')->where(function ($query) {
            $query->where('users.quakeClubUser', -1)
                ->orWhere('users.quakeClubUser', 1);
        })->where('cus_rsvp_tour_data.is_valid', 1)->select('cus_rsvp_tour_data.id','users.id as user_id', 'users.name', 'users.email', 'users.mobileNo', 'users.quakeClubUser', 'cus_qperks_company_user.name as comp_name', 'cus_qperks_company_user.designation', 'cus_rsvp_tour_data.is_attended', 'cus_rsvp_tour_data.redemption_status', 'cus_rsvp_tour_data.designation as rsvp_designation', 'cus_rsvp_tour_data.mobileNo as rsvp_mobileNo', 'cus_rsvp_tour_data.email as rsvp_email', 'cus_rsvp_tour_data.comp_name as rsvp_comp_name', 'cus_rsvp_tour_data.name as rsvp_name');

        // return $events;
        if(trim($companyName) && trim($name)){
            $events = $events->where('cus_qperks_company_user.name','LIKE', '%' . $companyName . '%')->where('users.name', 'LIKE', '%' . $name . '%');
        } else if(trim($companyName) && !trim($name) ){
            $events = $events->where('cus_qperks_company_user.name','LIKE', '%' . $companyName . '%');
        } else if(trim($name) && !trim($companyName)) {
            $events = $events->where('users.name', 'LIKE', '%' . $name . '%');
        }

        switch($sortHeader){
            case 'comp_name':
                $events = $events->{$sortBy}($sortHeader);
                break;

            case 'name' :
                $events = $events->{$sortBy}($sortHeader);
                break;

            case 'attendance' :
                $events = $events->{$sortBy}('is_attended');
                break;

            default :
                break;
        }

        $events = ( $companyName != null || $name != null ) ?  $events->paginate($limit) : $events->desc('quakeClubUser')->asc('comp_name')->paginate($limit);

        // if ($_SERVER['REMOTE_ADDR'] == '211.24.110.111') {
        //     return response()->json(['result' =>$events, 'query' => DB::getQueryLog()]);
        // }

        return response()->json(['result' =>$events]);
    }

    public function updateAttendance(Request $request, $id){
        $attend_status = $request->get('attend_status');

        $ori_client = Client::find($id);
        $client = Client::find($id);
        $user_point = Clientpoint::where("user_id" , '=', $id)->first();
        // $monthly_point = Monthlypoint::where("user_id" , '=', $id)->first();
        // return $id;
        if($attend_status == 'true'){
            $client->is_attended = 1;
            // if ($user_point) {
            //     $user_point = new Clientpoint;
            //     $user_point->state = 1;
            //     $user_point->checked_out = 0;
            //     $user_point->checked_out_time = "0000-00-00 00:00:00";
            //     $user_point->created_by = $id;
            //     $user_point->modified_by = $id;
            //     $user_point->user_id = $id;
            //     $user_point->point = 80;
            //     $user_point->type = 9;
            //     $user_point->source = "Bigbigshow2019";
            //     $user_point->save();
            // }
        }
        if($attend_status == 'false'){
            $client->is_attended = 0;
        }
        $client->save();

        // $this->log('UPDATE', 'attendance', 'client: ' . $id, '{"original_attendance":'.$ori_client->is_attended.',"updated_attendance":'.$client->is_attended.'}');
        return response()->json(['status' => '200']);
    }

    public function updateRedemption(Request $request, $id){
        $redemption_status = $request->get('redemption_status');

        // $ori_client = Client::find($id);
        $client = Client::find($id);

        if($redemption_status == 'true'){
            $client->redemption_status = 1;
        }
        if($redemption_status == 'false'){
            $client->redemption_status = 0;
        }
        $client->save();

        // $this->log('UPDATE', 'attendance', 'client: ' . $id, '{"original_attendance":'.$ori_client->is_attended.',"updated_attendance":'.$client->is_attended.'}');
        return response()->json(['status' => '200']);
    }



}
