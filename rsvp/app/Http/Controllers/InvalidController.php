<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Client;

class InvalidController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function invalidList() {
        return view('invalid');
    }

    public function updateInvalidStatus($id) {

        $ori_client = Client::find($id);
        $client = Client::find($id);

        if($client->is_valid == 1){
            $client->is_valid = 0;
            $client->is_attended = 0;
        } else {
            $client->is_valid = 1;
        }

        $client->save();

        // $this->log('UPDATE', 'invalid', 'client: ' . $id, '{"original_valid_status":'.$ori_client->is_valid.',"updated_valid_status":'.$client->is_valid.'}');

        return response()->json(['status' => '200']);
    }

    public function getInvalidClient(Request $request){
        $sortHeader = null;
        $sortBy= null;
        $isSort = false;

        if($request->has('sort_header') && $request->has('sort_by')) {
            $sortHeader = $request->get('sort_header');
            $sortBy = $request->get('sort_by');

            $isSort = true;
        }

        $clients = Client::leftJoin('users', 'users.id', '=', 'cus_rsvp_tour_data.user_id')->leftJoin('cus_qperks_company_user', 'users.id', '=', 'cus_qperks_company_user.user_id')->where(function ($query) {
            $query->where('users.quakeClubUser', -1)
                ->orWhere('users.quakeClubUser', 1);
        })->where('cus_rsvp_tour_data.is_valid', 0)->select('cus_rsvp_tour_data.id', 'users.name', 'users.email', 'users.mobileNo', 'users.quakeClubUser', 'cus_qperks_company_user.name as comp_name', 'cus_qperks_company_user.designation', 'cus_rsvp_tour_data.is_attended', 'cus_rsvp_tour_data.redemption_status');

        if($isSort == true){

            switch($sortHeader){
                case 'comp_name':
                    $clients = $clients->{$sortBy}($sortHeader);
                    break;

                case 'name':
                    $clients = $clients->{$sortBy}('name');
                    break;

                default:
                    break;
            }
        }

        $clients = $clients->get();

        return response()->json($clients);
    }

    public function deleteClient($id) {
        $client = Client::find($id);

        $client->is_attended = 0;
        $client->is_valid = 0;
        $client->is_deleted = 1;

        $client->save();

        $this->log('DELETE', 'client', 'client: ' . $id, '{"client":'.$client.'}');

        return response()->json(['status' => '200']);
    }
}
