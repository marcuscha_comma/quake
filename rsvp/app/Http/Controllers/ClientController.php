<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateRsvpRequest;

use App\Models\Client;

class ClientController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('site', 'createRsvp');
    }

    public function site() {
        return redirect('/login');
    }

    public function showRsvp() {
        return view('create-rsvp');
    }

    public function createRsvp(CreateRsvpRequest $request) {

        if($request->has('action') && $request->input('action') == 'site-register') {
            $check = Client::where('is_valid', 1)->where('is_deleted', 0)->count();
            if($check >= 40) {
                \Session::flash('error', 'Full slot');
                return redirect()->route('site.index');
            }
        }

        $client = new Client;
        $client->name = $request->name;
        $client->comp_name = $request->comp_name;
        $client->designation = $request->designation;
        $client->email = $request->email;
        $client->mobileNo = $request->mobile_num;
        $client->save();

        if($request->has('action') && $request->input('action') == 'save') {
            $this->log('CREATE', 'rsvp from admin panel', 'client: ' . $client->id, '{'.$client.'}');

            \Session::flash('message', 'rsvp created');
            return redirect()->route('admin.show-rsvp');
        } else {
            $this->log('CREATE', 'rsvp from public', 'client: ' . $client->id, '{'.$client.'}');

            \Session::flash('message', 'You have successfully registered. Look out for the confirmation email.');
            return redirect()->route('site.index');
        }
    }

}
