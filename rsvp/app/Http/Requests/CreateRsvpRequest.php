<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRsvpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'name' => 'required|max:100',
            'nric' => 'required',
            'comp_name' => 'required',
            'designation' => 'required',
            'email' => 'required|email',
            'mobile_num' => 'required|numeric|digits_between:9,12',
            'office_num' => 'nullable|numeric|digits_between:9,12',
        ];

        return $rules;
    }
}
