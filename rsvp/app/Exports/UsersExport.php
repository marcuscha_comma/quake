<?php

namespace App\Exports;

use App\Models\Client;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return Client::get(['comp_name', 'name', 'nric', 'designation', 'email', 'mobile_num', 'office_num']);
        $clients =Client::leftJoin('users', 'users.id', '=', 'cus_rsvp_tour_data.user_id')->leftJoin('cus_qperks_company_user', 'users.id', '=', 'cus_qperks_company_user.user_id')->where(function ($query) {
            $query->where('users.quakeClubUser', -1)
                ->orWhere('users.quakeClubUser', 1);
        })->where('cus_rsvp_tour_data.is_valid', 1)->select('cus_rsvp_tour_data.id', 'users.name', 'users.email', 'users.mobileNo', 'users.quakeClubUser', 'cus_qperks_company_user.name as comp_name', 'cus_qperks_company_user.designation', 'cus_rsvp_tour_data.is_attended', 'cus_rsvp_tour_data.redemption_status')->get();

        return $clients;
    }
}
