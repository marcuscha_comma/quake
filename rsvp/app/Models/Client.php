<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = ['id'];
    protected $table = 'cus_rsvp_tour_data';

    /**
     * Scope a query to only include users of a given orderBy ASC.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAsc($query, $type)
    {
        return $query->orderBy($type, 'asc');
    }

    /**
     * Scope a query to only include users of a given orderBy DESC.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDesc($query, $type)
    {
        return $query->orderBy($type, 'desc');
    }

    /**
     * Scope a query to only include invalid users .
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInvalid($query)
    {
        return $query->where('is_valid', 0)->where('is_deleted',0);
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company', 'user_id');
    }

}
