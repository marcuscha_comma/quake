@extends('layouts.master')

@section('css')
<style>
    .swal-footer {
        text-align:center;
    }
</style>
@endsection
@section('content')
    <div class="col-xl-10 col-md-12 dashboard ">
        <div class=" pl-20 pr-20 pt-30">
            <div id="dashboard-app" v-cloak>
                <div class="main-title pt-20 pb-20">
                        <h1><strong>Big Big Show 2019</strong></h1>
                </div>
                <section class="row main-date-wrapper">
                    <div class="col-4">
                        <div class="main-date-tab">
                            <div class="p-30 pb-3">
                                <div class="main-date-percent pb-15">
                                    <strong class="font-36">@{{ animatedTotal }}</strong> %
                                </div>
                                <div class="main-date-title row">
                                    <div class="col-8 font-24">
                                        Total Attendees
                                    </div>
                                    <div class="col main-date-title-number text-right font-16">@{{ overallAttend }} / @{{ overallTotal }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" :style="'width:'+ OverallRateAttendance + '%'" :aria-valuenow="OverallRateAttendance" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </section>

                <div class="tab-content mt-34 mb-30" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-16" role="tabpanel" aria-labelledby="pills-16">
                        <div class="specific-attendance">
                            <div>
                                <div class="mb-10"><strong>Search by</strong></div>

                                <div class="row mb-2">
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="Company Name" v-model="companyName" @keyup.enter="searchAndSort()">
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="Name" v-model="name" @keyup.enter="searchAndSort()">
                                    </div>
                                    <div class="col">
                                        <button type="button" class="btn btn-block search-btn" @click="searchAndSort()"><i class="fas fa-search pr-10"></i>Search</button>
                                    </div>
                                </div>

                                <table class="table table-hover mt-34" style="background: white;">
                                    <thead class="table-header-design">
                                    <tr>
                                        <th class="text-right" width="5%">No.</th>
                                        <th>
                                            <a href="javascript:void(0)" @click="searchAndSort('comp_name')">
                                                Company Name <i class="fas fa-sort"></i>
                                            </a>
                                        </th>
                                        <th width="10%">
                                            <a href="javascript:void(0)" @click="searchAndSort('name')">
                                                Name <i class="fas fa-sort"></i>
                                            </a>
                                        </th>
                                        <th>Designation</th>
                                        <th>Email</th>
                                        <th>Contact No.</th>
                                        <th>Quake Club</th>
                                        <th v-if="attendStatus != 'Not Attending'">
                                            <a href="javascript:void(0)" @click="searchAndSort('attendance')">
                                                Attendance
                                            </a><i class="fas fa-sort"></i>
                                        </th>
                                        <th>Redemption</th>
                                        <!-- <th>
                                            Action
                                        </th> -->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-if="selectAllById == 0">
                                        <td colspan="10" style="text-align: center;">No Result Found</td>
                                    </tr>
                                    <tr v-else v-for="(item,index) in selectAllById">
                                        <td class="text-right">@{{ limit*(pageNum-1) + (++index) }}</td>
                                        <td>@{{ item.comp_name == "" ? item.rsvp_comp_name : item.comp_name}}</td>
                                        <td>@{{ item.name == "" ? item.rsvp_name : item.name}}</td>
                                        <td>@{{ item.designation == "" ? item.rsvp_designation : item.designation }}</td>
                                        <td>@{{ item.email == "" ? item.rsvp_email : item.email }}</td>
                                        <td>@{{ item.mobileNo == null ? item.rsvp_mobileNo : item.mobileNo }}</td>
                                        <td>@{{ item.quakeClubUser == 1 ? 'Yes' : 'No' }}</td>
                                        <td>
                                            <div class="custom-control custom-checkbox ml-20">
                                                <input type="checkbox" class="custom-control-input " :id="'customCheck'+item.id"  v-model="item.is_attended" @change="updateAttendance(item.id,item.is_attended,item.user_id,item.quakeClubUser)">
                                                <label class="custom-control-label" :for="'customCheck'+item.id"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox ml-20">
                                                <input type="checkbox" class="custom-control-input " :id="'customCheck1'+item.id"  v-model="item.redemption_status" @change="updateRedemption(item.id,item.redemption_status)">
                                                <label class="custom-control-label" :for="'customCheck1'+item.id"></label>
                                            </div>
                                        </td>
                                        <!-- <td class="edit-btn"><a @click="invalid(item.id)"><i class="fas fa-times"></i> Invalid</a></td> -->
                                    </tr>
                                    </tbody>
                                </table>
                                <comp-pagination
                                        v-if="paginationShow"
                                        :total-page = "pagination.last_page"
                                        :current-page-limit = "limit"
                                        :current-page-num = "pageNum"
                                        @return-page-limit = "allLimitOnChanged"
                                        @return-page-num = "allPageOnChanged"
                                        @refresh-filter = "allRefreshFilter"

                                ></comp-pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        // jQuery(document).ready(function(){

        //     jQuery('.main-date-tab').matchHeight();

        // });
    </script>
@endsection
@section('vue')
    <script>

        Vue.config.devtools = true;

        Vue.use(VeeValidate);

        new Vue({
            el: '#dashboard-app',
            data: {
                showModal: false,
                limit: 10,
                pageNum: 1,
                pagination:{
                    current_page: '',
                    from: '',
                    last_page: '',
                    last_page_url: '',
                    next_page_url: '',
                    path: '',
                    per_page: '',
                    prev_page_url: '',
                    to: '',
                    total: '',
                },
                selectAllById: [],
                numberSessionAttendance: 0,
                rateSessionAttendance: 0.00,
                totalSessionAttendance: 0,
                overallTotal: 0,
                overallAttend: 0,
                OverallRateAttendance: 0.00,
                tweenedNumber: 0,
                redeemedCount: 0,
                companyName: '',
                name: '',
                currentCompanyName: '',
                currentName: '',
                currentSort: '',
                sortBy: 'asc',
                attendNumbers: '',
                paginationShow: true,
                attendStatus: 'All',
                modalClientId: '',
                modalInvalidStatus: '',
            },
            mounted: function(){
                this.getAttendanceRate();
                this.searchAndSort();

            },
            computed: {
                animatedTotal: function() {
                    return this.tweenedNumber.toFixed(2);
                }
            },
            watch: {
                OverallRateAttendance: function(newValue) {
                    TweenLite.to(this.$data, 0.5, { tweenedNumber: newValue });
                }
            },
            methods:{
                // convertDateToDate : function(date) {
                //     return moment(date).format('DD MMM YYYY');
                // },
                // convertWordToUppercase: function(string){
                //     return string.toUpperCase();
                // },
                getAttendanceRate: function(){
                    axios.get('{{ url("/") }}/api/admin/sessionAttendance/')
                        .then((res) => {
                            // console.log(res);
                            if(res.data.length == 0){
                                this.overallTotal = 0;
                                this.overallAttend = 0;
                            }else{
                                this.overallTotal =  res.data.overall_total;
                                this.overallAttend = res.data.overall_attending;
                            }

                            this.OverallRateAttendance = ((this.overallAttend / this.overallTotal) * 100).toFixed(2);
                            // if(isNaN(this.OverallRateAttendance)){
                            //     this.OverallRateAttendance = (0).toFixed(2);
                            // }
                        })
                },
                allLimitOnChanged: function(limit){
                    this.limit = limit;
                    this.pageNum = 1;
                    this.searchAndSortCurrent();
                },
                allPageOnChanged: function(pageNum){
                    this.pageNum = pageNum;
                    this.searchAndSortCurrent();
                },
                allRefreshFilter: function(){
                    this.limit = 10;
                    this.pageNum = 1;
                    this.companyName = '';
                    this.name = '';
                    this.searchAndSort();
                },
                searchAndSort: function(header){

                    if(header != null) {
                        this.currentSort = header;
                        this.sortBy = (this.sortBy == 'asc') ? 'desc':'asc';
                    }
                    else {
                        this.currentSort = this.currentSort;
                    }
                
                    if ( this.currentCompanyName != this.companyName || this.name != this.currentName ) {
                        this.pageNum = 1;
                    }

                    this.currentCompanyName = this.companyName;
                    this.currentName = this.name;

                    axios.get('{{ url("/") }}/api/admin/searchAndSort?&page=' + this.pageNum + '&limit=' + this.limit + '&company_name=' + this.companyName + '&name='+ this.name + '&sort_header=' + this.currentSort + '&sort_by=' + this.sortBy)
                        .then((res) => {
                            console.log(res.data);

                            if(res.data.length == 0){
                                this.selectAllById = [];
                            } else {
                                this.selectAllById = res.data.result.data;
                                this.getPagination(res.data.result);
                            }
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                },
                searchAndSortCurrent: function(header){

                    if(header != null) {
                        this.currentSort = header;
                        this.sortBy = (this.sortBy == 'asc') ? 'desc':'asc';
                    }
                    else {
                        this.currentSort = this.currentSort;
                    }

                    // if ( this.currentCompanyName != this.companyName || this.name != this.currentName ) {
                    //     this.pageNum = 1;
                    // }

                    // this.currentCompanyName = this.companyName;
                    // this.currentName = this.name;

                    axios.get('{{ url("/") }}/api/admin/searchAndSort?&page=' + this.pageNum + '&limit=' + this.limit + '&company_name=' + this.currentCompanyName + '&name='+ this.currentName + '&sort_header=' + this.currentSort + '&sort_by=' + this.sortBy)
                        .then((res) => {
                            console.log(res.data);

                            if(res.data.length == 0){
                                this.selectAllById = [];
                            } else {
                                this.selectAllById = res.data.result.data;
                                this.getPagination(res.data.result);
                            }
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                },
                selectByPageNum(num, id, status){
                    axios.get(this.pagination.path + '?page=' + num + '&status=' + status + '&limit=' + this.limit)
                        .then((res) => {
                            // console.log(res.data.data);
                            this.selectAllById = res.data.data;
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                },
                getPagination(data){
                    this.pagination.current_page = data.current_page;
                    this.pagination.first_page_url = data.first_page_url;
                    this.pagination.from = data.from;
                    this.pagination.last_page = data.last_page;
                    this.pagination.last_page_url = data.last_page_url;
                    this.pagination.next_page_url = data.next_page_url;
                    this.pagination.path = data.path;
                    this.pagination.per_page = data.per_page;
                    this.pagination.prev_page_url = data.prev_page_url;
                    this.pagination.to = data.to;
                    this.pagination.total = data.total;
                    // console.log('page');
                    // console.log(data);
                },
                updateAttendance: function(id,status,user_id,is_member){

                    axios.post('{{ url("/") }}/api/admin/updateAttendance/'+ id + '?attend_status=' + status)
                        .then((res) => {
                            // console.log(res.data);
                            console.log('attend');
                            this.getAttendanceRate();
                            this.searchAndSort();

                        })
                        .catch((err) => {
                            console.log(err);
                        });
                        if (true) {
                            axios.get('http://quake.com.my/api-add-point',{params: {
                            'id':user_id,
                            'status':status,
                            }})
                            .then((res) => {
                                // console.log(res.data);
                                console.log('add');

                            })
                            .catch((err) => {
                                console.log(err);
                            });
                        }
                    
                },
                updateRedemption: function(id,status){

                    axios.post('{{ url("/") }}/api/admin/updateRedemption/'+ id + '?redemption_status=' + status)
                        .then((res) => {
                            // console.log(res.data);
                            console.log('redemption');
                            // this.getAttendanceRate();
                            // this.searchAndSort();

                        })
                        .catch((err) => {
                            console.log(err);
                        })
                },
                invalid: function(id){
                    swal("Are you sure this client is invalid?", {
                        icon: 'warning',
                        buttons: {
                            catch: {
                                text: "Yes",
                                value: "yes",
                            },
                            cancel: "No",
                        },
                    })
                        .then((value) => {
                            switch (value) {

                                case "yes":
                                    axios.post('{{ url("/") }}/api/admin/updateInvalidStatus/'+ id)
                                        .then((res) => {
                                            console.log('invalid');
                                            this.getAttendanceRate();
                                            this.searchAndSort();
                                        })
                                        .catch((err) => {
                                            console.log(err);
                                        })
                                    swal("Done!","", "success");
                                    break;

                                default:

                            }
                        });
                },

            }
        })
    </script>
@endsection
