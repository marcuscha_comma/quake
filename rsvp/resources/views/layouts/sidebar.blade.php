<div class="col-xl-2 left-nav">
	<div class="row" >
	
	
		<a class="img-wrapper" href="{{ route('dashboard') }}"><img src="{{ asset('public/img/astro_logo.png') }}" alt="" width="100%"></a>
		<div id="nav-app" class="user-detail pl-30 pr-30 pb-30">
			<label class="font-12">Welcome,</label>
			<div class="user-name font-15">{{ Auth::user()->name }}</div>
			<!--<div class="user-position">
				{{Auth::user()->pluck('name')->implode(' ')}}

			</div>-->
		</div>

		<div style="width:100%;">
		
			<ul class="nav flex-column sidebar-dark">
				<li class="nav-item"><a class="nav-link active font-15 pl-30 py-3" href="{{ route('dashboard') }}"><i class="fas fa-desktop pr-3"></i>Home</a></li>

				<!-- <li class="nav-item"><a class="nav-link font-15 pl-30 py-3" href="{{ route('admin.invalid-list') }}"><i class="fas fa-user-slash pr-3"></i>Invalid List</a></li> -->
				<li class="nav-item"><a class="nav-link font-15 pl-30 py-3" href="{{ route('admin.show-rsvp') }}"><i class="fas fa-user-plus pr-3"></i>Create RSVP</a></li>
				<!-- <li class="nav-item"><a class="nav-link font-15 pl-30 py-3" href="{{ route('exportList') }}"><i class="fas fa-download pr-3"></i>Client List</a></li> -->
			</ul>
		
		</div>
		<div style="width:100%;">
			<ul class="nav flex-column">
				<!-- <li class="nav-item"><a href="{{ route('password.change') }}" class="nav-link font-15 pl-30 py-3"><i class="fas fa-user-alt pr-3"></i>Change Password</a></li> -->
				<li class="nav-item"><a href="{{ route('logout') }}" class="nav-link font-15 pl-30 py-3"><i class="fas fa-sign-out-alt pr-3"></i>Logout</a></li>
			</ul>
		</div>
	</div>
	
</div>