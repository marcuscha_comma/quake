<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
       window.Laravel = {!! json_encode([
           'csrfToken' => csrf_token(),
       ]) !!};
   </script>

	<title>{{ config('app.name', 'Astro RSVP') }}</title>

    @include('layouts.styles')

	@yield('css')
</head>
<body class="full-container">
	@include('layouts.sidebar-mobile')

	<main id="app">
		<div class="container-fluid">

			<div class="row">

				@include('layouts.sidebar')


				@yield('content')

			</div>
		</div>
	</main>

	@include('layouts.scripts')
	@yield('js')
	@yield('vue')

</body>
</html>
