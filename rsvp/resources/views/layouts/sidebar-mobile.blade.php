<div id="mobile-navbar">
    <nav class="navbar navbar-dark bg-dark" >
        <a class="navbar-brand" href="{{ route('dashboard') }}">
            <img src="{{ asset('public/img/astro_logo.png') }}" width="auto" height="30" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('dashboard') }}"><i class="fas fa-desktop pr-3"></i>Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.invalid-list') }}"><i class="fas fa-user-slash pr-3"></i>Invalid List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.show-rsvp') }}"><i class="fas fa-user-plus pr-3"></i>Create RSVP</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('exportList') }}"><i class="fas fa-user-plus pr-3"></i>Client List</a>
                </li>
                <div class="dropdown-divider"></div>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('password.change') }}"><i class="fas fa-user-alt pr-3"></i>Change Password</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt pr-3"></i>Logout</a>
                </li>
            </ul>
        </div>
    </nav>
</div>