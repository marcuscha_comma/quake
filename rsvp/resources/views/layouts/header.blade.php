<div id="header-app">
	<header v-cloak>
		<div class="container-fluid">
			<div class="row">
				<div class="col-1-5 header-left"><img src="http://www.astropowerhour.tv/dms/assets/img/astro_admin.png" title="Astro" alt="Astro" /></div>
					<div class="col-10-5 header-right">
						<!-- <div class="dashboard-search">
						<typeheader class="form-control-inline" :model-id="'client'" :url="clientUrl" v-on:trigger="triggerClientSearch" placeholder="Search Company..."></typeheader>
						<i class="search glyphicon glyphicon-search" type="button" @click="refreshFilter()"></i>
						</div> -->

						<div class="current-user">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							@auth
							<i class="glyphicon glyphicon-user" aria-hidden="true"></i>{{ Auth::user()->name }} <span class="caret"></span>
							@endauth
							</a>

							<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu">
								<li><a href="{{ route('password.change') }}">My Profile</a></li>
								@if(Auth::user()->type == 'super_admin')
									<li><a href="{{ route('privilege.index') }}">Privilege</a></li>
								@endif
								<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
							</ul>
						</div>
						<div class="current-datetime">
							@{{ time }} | &nbsp;
						</div>
						<div class="clearfix"></div>
					</div>
			</div>
		</div>
	</header>
</div>