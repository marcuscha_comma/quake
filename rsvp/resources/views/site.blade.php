<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BIG BIG Show RSVP</title>

    <link rel="stylesheet" href="{{ asset('/public/css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ asset('/public/css/quake.css') }}" />

    <link rel="stylesheet" href="{{ asset('/public/css/fontawesome52.css') }}">

    <link rel="shortcut icon" href="{{ asset('/public/img/faviconquake.ico') }}">
</head>

<body>
<section class="container" id="appController">
    <div class="page-header text-center">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-xl-8 astro-logo">
                <img src="{{ asset('/public/img/astro-logo.png') }}" alt="astro" class="responsive">
            </div>

            <div class="col-md-8 responsive-logo">
                <img src="{{ asset('/public/img/logo.png') }}" alt="logo" class="responsive">
            </div>
        </div>

        <!-- <div class="row big-title justify-content-center">
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-4">
                        <h1><span>B</span>OLD</h1>
                    </div>

                    <div class="col-lg-4">
                        <h1><span>I</span>NSPIRING</h1>
                    </div>

                    <div class="col-lg-4">
                        <h1><span>G</span>ROWTH</h1>
                    </div>
                </div>
            </div>
        </div> -->

        <hr class="mb-5">

        <h2 class="font-cn">
            <span class="yellow">VENUE : </span>Soju Sunway @ Sunway Hotel Resort & Spa
        </h2>

        <h2 class="font-cn">
            <span class="yellow">DATE : </span>3 OCTOBER 2018, WEDNESDAY
        </h2>

        <h2 class="font-cn">
            <span class="yellow">TIME : </span>12PM - 5PM <span class="small">(LUNCH WILL BE SERVED)</span>
        </h2>

        <hr class="hr-sm mt-5">
    </div>

    <div class="page-form">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-xl-8 form-container">
                {{-- <h2 class="text-center">
                    Join us and embark on a BIG BIG journey to grow your business
                </h2> --}}

                <form method="POST" action="{{route('admin.rsvp.save')}}" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">

                        <label for="your-name" class="col-sm-4 col-lg-3 col-form-label">Title <span>*</span></label>

                        <div class="col-sm-8 col-lg-9">
                            <div class="row">
                                <div class="col-md-6 col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="title" id="title-dato" value="Datuk / Dato">
                                        <label class="form-check-label" for="title-dato">
                                            Datuk / Dato
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6 col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="title" id="title-datin" value="Datin">
                                        <label class="form-check-label" for="title-datin">
                                            Datin
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="title" id="title-mr" value="Mr">
                                        <label class="form-check-label" for="title-mr">
                                            Mr
                                        </label>
                                    </div>
                                </div>

                                <div class="col col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="title" id="title-ms" value="Ms">
                                        <label class="form-check-label" for="title-ms">
                                            Ms
                                        </label>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('title'))
                                <div class="form-error">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="your-name" class="col-sm-4 col-lg-3 col-form-label">Name <span>*</span></label>
                        <div class="col-sm-8 col-lg-9">
                            <input type="text" class="form-control" name="name" id="your-name" placeholder="" maxlength="254" value="{{old('name')}}">
                            @if ($errors->has('name'))
                                <div class="form-error">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="your-name" class="col-sm-4 col-lg-3 col-form-label">NRIC <span>*</span></label>
                        <div class="col-sm-8 col-lg-9">
                            <input type="text" class="form-control" name="nric" id="your-name" placeholder="" maxlength="254" value="{{old('nric')}}">
                            @if ($errors->has('nric'))
                                <div class="form-error">{{ $errors->first('nric') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="company-name" class="col-sm-4 col-lg-3 col-form-label">Company Name <span>*</span></label>
                        <div class="col-sm-8 col-lg-9">
                            <input type="text" class="form-control" name="comp_name" id="company-name" placeholder="" maxlength="254" value="{{old('comp_name')}}">
                            @if ($errors->has('comp_name'))
                                <div class="form-error">{{ $errors->first('comp_name') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="designation" class="col-sm-4 col-lg-3 col-form-label">Designation <span>*</span></label>
                        <div class="col-sm-8 col-lg-9">
                            <input type="text" class="form-control" name="designation" id="designation" placeholder="" maxlength="254" value="{{old('designation')}}">
                            @if ($errors->has('designation'))
                                <div class="form-error">{{ $errors->first('designation') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-4 col-lg-3 col-form-label">Email <span>*</span></label>
                        <div class="col-sm-8 col-lg-9">
                            <input type="email" class="form-control" name="email" id="email" placeholder="" maxlength="254" value="{{old('email')}}">
                            @if ($errors->has('email'))
                                <div class="form-error">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="mobile" class="col-sm-4 col-lg-3 col-form-label">Mobile Number <span>*</span></label>

                        <div class="col-sm-8 col-lg-9">
                            <input type="tel" class="form-control" name="mobile_num" id="mobile" placeholder="" maxlength="254" value="{{old('mobile_num')}}">
                            @if ($errors->has('mobile_num'))
                                <div class="form-error">{{ $errors->first('mobile_num') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="office" class="col-sm-4 col-lg-3 col-form-label">Office Number</label>

                        <div class="col-sm-8 col-lg-9">
                            <input type="tel" class="form-control" id="office" placeholder="" maxlength="254" value="{{old('office_num')}}">
                            @if ($errors->has('office_num'))
                                <div class="form-error">{{ $errors->first('office_num') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row justify-content-center">
                        <button type="submit" name="action" value="site-register" class="btn btn-submit btn-pill mt-4">Register Now</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="footer mb-5">
        {{-- <img src="{{ asset('/public/img/footer.png') }}" alt="astro" class="responsive"> --}}
    </div>
</section>

<script src="{{ asset('/public/js/jquery.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if (session('message'))
    <script>
        swal({
            title: 'Thank You!',
            text: "{{session('message')}}",
            icon: "success",
            button: "Close",
        }).then(function(value) {

        });
    </script>
@endif

@if(session('error'))
    <script>
        var el = document.createElement("a");
        el.href = "mailto:mediasolutions@astro.com.my";
        el.innerText = "mediasolutions@astro.com.my";
        swal({
            title: "Oops!",
            text: '{{session('error')}}',
            content: el,
            icon: "error",
            button: "Go Back",
        }).then(function(value) {

        });
    </script>
@endif
{{--<script>--}}
    {{--case "200":--}}

    {{--break;--}}
    {{--// validation error--}}
    {{--case "400":--}}
    {{--swal({--}}
        {{--title: "Please complete the required details",--}}
        {{--icon: "error",--}}
        {{--button: "Go Back",--}}
    {{--});--}}
    {{--break;--}}
    {{--// email not in db, but registered success--}}
    {{--case "401":--}}

    {{--break;--}}
    {{--// something error--}}
    {{--case "500":--}}
    {{--swal({--}}
        {{--title: "Oops",--}}
        {{--text: "Something's wrong, please refresh or try again later",--}}
        {{--icon: "error",--}}
        {{--button: "Go Back",--}}
    {{--}).then(function(value) {--}}
        {{--location.reload();--}}
    {{--});--}}
    {{--break;--}}
    {{----}}
{{--</script>--}}
</body>

</html>