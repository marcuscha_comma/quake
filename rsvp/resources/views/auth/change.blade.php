@extends('layouts.master')

@section('css')
    <style>
        .input-btn {
            background: #e7077d;
            color: #fff;
            padding: 5px 35px;
            margin: 5px 0px;
            border: 0px;
        }

        .input-btn:hover, .input-btn.active {
            color: #333;
            text-decoration: none;
            opacity: 0.8;
            transition: opacity .25s ease-in-out;
            -moz-transition: opacity .25s ease-in-out;
            -webkit-transition: opacity .25s ease-in-out;
        }

        .form-control {
            border-radius: 0px;
            padding: 5px;
            height: auto!important;
        }

        input.form-control {
            font-size: 13px;
        }
    </style>
@endsection
@section('content')
    <div class="col-xl-10 col-md-12 dashboard ">
        <div id="app" class=" pl-20 pr-20 pt-30">
            <div class="mb-15">
                <h2 class="pull-left">Change Password</h2>
            </div>
            <div class="clearfix"></div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('failed'))
                <div class="alert alert-danger">
                    {{ session('failed') }}
                </div>
            @endif
            <form action="{{ route('password.update') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Old Password</label>
                            <input type="password" name="current_pw" placeholder="Old Password" class="form-control">
                            @if($errors->has('current_pw'))
                                <div class="alert-danger has-error">{{ $errors->first('current_pw') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>New Password</label>
                            <input type="password" name="password" placeholder="New Password" class="form-control">
                            @if($errors->has('password'))
                                <div class="alert-danger has-error">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control">
                            @if($errors->has('password_confirmation'))
                                <div class="alert-danger has-error">{{ $errors->first('password_confirmation') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <button type="submit" class="input-btn">Update</button>
                        <a href="{{route('dashboard')}}" class="input-btn">Back</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection