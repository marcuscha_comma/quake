<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" value="Astro">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <link rel="icon" type="image/png" href="{{ asset('/public/img/favicon.ico') }}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:300,400,500,700,400italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" href="{{ asset('/public/css/login.css') }}">
</head>
<body>
<div class="container">

    <div class="login-error">
        @if($errors->any())
            <p>Incorrect Login</p>
        @endif
    </div>
    <div class="login-input-container">
        <div class="row justify-content-center">
            <div class="col-sm-3 col-md-4">
                <img class="login-logo img-fluid" src="{{ asset('/public/img/login_logo.png') }}" title="Astro" alt="Astro">
            </div>
            <div class="col-sm-3 col-md-4">
                <form action="{{ route('login') }}" method="post" accept-charset="utf-8">
                    {{ csrf_field() }}
                    @if ($errors->has('username'))
                        <div class="form-control-feedback">
                            <strong>{{ $errors->first('username') }}</strong>
                        </div>
                    @endif
                    <input type="text" name="username" value="" placeholder="Username" class="login-input" autofocus>
                    <br>
                    @if ($errors->has('password'))
                        <div class="form-control-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                    <input type="password" name="password" value="" placeholder="Password" class="login-input">
                    <br>
                    <input type="submit" name="submit" value="Log In">
                </form>
            </div>
        </div>
    </div>

</div>

<p class="footer-text">© Copyright Astro. All Rights Reserved by Astro Malaysia.</p>
<footer>
    <div class="footer-container"></div>
</footer>
</body>
</html>
