@extends('layouts.master')

@section('content')
    <div class="col-xl-10 col-md-12 dashboard ">
        <div id="app" class=" pl-20 pr-20 pt-30">
            @if (session('message'))
                <div>RSVP created</div>
            @endif

            <div id="rsvp-app">
                <div class="main-title pt-20 pb-20">
                    <h1><strong>Create RSVP</strong></h1>
                </div>
                <form method="POST" action="{{route('admin.rsvp.save')}}" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-2 text-capitalize modal-input">
                            <label>Company Email</label>
                        </div>
                        <div class="col-6">
                            <input type="text" name="email" v-model="userEmail" autocomplete="off" class="form-control">
                            <div class="mb-24 mt10">
                                <span v-if="!checkComEmail" class="alert-danger has-error">
                                    Please fill in company’s email address
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 text-capitalize modal-input">
                            <label>Name</label>
                        </div>
                        <div class="col-6">
                            <input type="text" name="name" v-model="userName" autocomplete="off" class="form-control">
                            <div class="mb-24 mt10">
                                    <span v-if="!checkUserName" class="alert-danger has-error">
                                        Please fill in name
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 text-capitalize modal-input">
                            <label >Company Name</label>
                        </div>
                        <div class="col-6">
                            <input name="comp_name" v-model="vcompname" type="text" autocomplete="off" class="form-control">
                            <div class="mb-24 mt10">
                                
                                    <span v-if="!checkCompany" class="alert-danger has-error">
                                    Please fill in company’s name
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 text-capitalize modal-input">
                            <label>Designation</label>
                        </div>
                        <div class="col-6">
                            <input type="text" v-model="vdesignation" name="designation" autocomplete="off" class="form-control">
                            <div class="mb-24 mt10">
                                    <span v-if="!checkDesignation" class="alert-danger has-error">
                                        Please fill in job designation
                                    </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-2 text-capitalize modal-input">
                            <label>Mobile Number</label>
                        </div>
                        <div class="col-6">
                            <input type="text" name="mobileNo" v-model="userContactNumber" autocomplete="off" class="form-control">
                            <div class="mb-24 mt10">
                                    <span v-if="!checkMobileValid" class="alert-danger has-error">
                                    Please fill in a valid mobile number
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex mb-24">
                        <button type="button" @click="fillForm()" name="action" value="save" class="form-control mr-15 modal-save-btn"><i class="fas fa-save pr-10"></i>Create RSVP</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('vue')
    <script>

        Vue.config.devtools = true;

        Vue.use(VeeValidate);

        new Vue({
            el: '#rsvp-app',
            data: {
                userEmail:"",
				userName:"",
				vcompname:"",
                userContactNumber:"",
                vdesignation:"",
                checkComEmail:true,
				checkEmailExist:true,
				checkUserName:true,
				checkCompany:true,
				checkDesignation:true,
				checkMobile:true,
				checkMobileValid:true,
            },
            mounted: function(){
            },
            computed: {
            },
            watch: {
            },
            methods:{
                fillForm: function(){
					if (this.userEmail.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/) == null) {
						this.checkComEmail = false;
					}else{
						this.checkComEmail = true;
					}
					if (this.userName == "") {
						this.checkUserName = false;
					}else{
						this.checkUserName = true;
					}
					if (this.vcompname == "") {
						this.checkCompany = false;
					}else{
						this.checkCompany = true;
					}
					if (this.vdesignation == "") {
						this.checkDesignation = false;
					}else{
						this.checkDesignation = true;
					}
					// if (this.userContactNumber == "") {
					// 	this.checkMobile = false;
					// }else{
					// 	this.checkMobile = true;
					// }
					if (!this.userContactNumber.match(/.{9,16}/)) {
						this.checkMobileValid = false;
					}else{
						this.checkMobileValid = true;
					}
					if (!this.checkPDPAStatus) {
						this.checkPDPAStatusShow = false;
					}else{
						this.checkPDPAStatusShow =true;
					}

					if (this.checkMobileValid && this.checkMobile && this.checkDesignation && this.checkCompany && this.checkUserName && this.checkComEmail) {
						_this = this;
                        axios.get('http://quake.com.my/api-fill-form',{params: {
                            'email':_this.userEmail,
                            'name':_this.userName,
                            'comp_name':_this.vcompname,
                            'mobileNo':_this.userContactNumber,
                            'designation':_this.vdesignation
                        }})
                        .then((res) => {
                            var tmp = res.data.split("-");
                            if (tmp==2) {
                                swal("This email have been registered!","", "success");
                            }else if(tmp==0){
                                swal("This email have been registered!","", "success");
                            }else{
                                swal("This RSVP have been created!","", "success");
                            }
                            _this.userEmail = "";
                            _this.userName = "";
                            _this.vcompname = "";
                            _this.userContactNumber = "";
                            _this.vdesignation = "";
                        })
					}
				}
                // getAttendanceRate: function(){
                //     axios.get('{{ url("/") }}/api/admin/sessionAttendance/')
                //         .then((res) => {
                //             console.log(res);
                            
                //         })
                // }
            }
        })
    </script>
@endsection

