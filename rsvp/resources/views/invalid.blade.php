@extends('layouts.master')

@section('css')
    <style>
        .swal-footer {
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <div class="col-xl-10 col-md-12 dashboard ">
        <div class=" pl-20 pr-20 pt-30">
            <div id="invalid-app" >
                <div class="main-title pt-20 pb-20">
                    <h1><strong>Invalid List</strong></h1>
                </div>

                <table class="table table-hover" style="background: white;">
                    <thead class="table-header-design">
                    <th class="text-right" width="4%">No.</th>
                    <th>
                        <a href="javascript:void(0)" @click="getAndSortInvalidClient('comp_name')">
                            Company Name <i class="fas fa-sort"></i>
                        </a>
                    </th>
                    <!-- <th>Title</th> -->
                    <th>
                        <a href="javascript:void(0)" @click="getAndSortInvalidClient('name')">
                            Name <i class="fas fa-sort"></i>
                        </a>
                    </th>
                    <th>Designation</th>
                    <th>Email</th>
                    <th>Contact No.</th>
                    <th>Quake Club</th>
                    <!-- <th>Office No.</th> -->
                    <th colspan="2">Action</th>
                    </thead>
                    <tbody>
                    <tr v-if="invalidClients == 0">
                        <td colspan="9" style="text-align: center;">No Result Found</td>
                    </tr>
                    <tr v-for="(client, index) in invalidClients">
                        <td class="text-right">@{{ (index+1)  }}</td>
                        <td>@{{ client.comp_name }}</td>
                        <td>@{{ client.name }}</td>
                        <td>@{{ client.designation }}</td>
                        <td>@{{ client.email }}</td>
                        <td>@{{ client.mobileNo }}</td>
                        <td>@{{ client.quakeClubUser == 1 ? 'Yes' : 'No' }}</td>
                        <td class="edit-btn"><a @click="verify(client.id)">Verify</a></td>
                        <!-- <td class="edit-btn"><a @click="deleteInvalid(client.id)">Delete</a></td> -->

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        Vue.config.devtools = true;

        new Vue({
            el: '#invalid-app',
            data: {
                invalidClients: '',
                currentSort: '',
                sortBy: 'asc',
            },
            mounted: function(){
                this.getAndSortInvalidClient();
            },
            methods: {
                getAndSortInvalidClient: function(header = null){
                    if(header == this.currentSort) {

                        this.sortBy = (this.sortBy == 'asc') ? 'desc':'asc';
                    }

                    this.currentSort = header;

                    axios.get('{{ url("/") }}/api/admin/getInvalidClient?sort_header=' + this.currentSort + '&sort_by=' + this.sortBy)
                        .then((res) => {

                            this.invalidClients = res.data;

                        })
                        .catch((err) =>{
                            console.log(err);
                        })
                },
                verify: function(id){
                    swal("Are you sure you want to change the status for this email?", {
                        icon: 'warning',
                        buttons: {
                            catch: {
                                text: "Yes",
                                value: "yes",
                            },
                            cancel: "No",
                        },
                    })
                        .then((value) => {
                            switch (value) {

                                case "yes":
                                    axios.post('{{ url("/") }}/api/admin/updateInvalidStatus/'+ id)
                                        .then((res) => {
                                            console.log('verify');
                                            this.getAndSortInvalidClient();
                                        })
                                        .catch((err) => {
                                            console.log(err);
                                        })
                                    swal("Confirmed!","", "success");
                                    break;

                                default:

                            }
                        });
                },
                deleteInvalid: function(id){
                    swal("Are you sure you want to delete this email?", {
                        icon: 'warning',
                        buttons: {
                            catch: {
                                text: "Yes",
                                value: "yes",
                            },
                            cancel: "No",
                        },
                    })
                        .then((value) => {
                            switch (value) {

                                case "yes":
                                    axios.post('{{ url("/") }}/api/admin/deleteClient/'+ id)
                                        .then((res) => {
                                            console.log('deleted');
                                            this.getAndSortInvalidClient();
                                        })
                                        .catch((err) => {
                                            console.log(err);
                                        })
                                    swal("Deleted!","", "success");
                                    break;

                                default:

                            }
                        });
                }
            }

        });

    </script>
@endsection