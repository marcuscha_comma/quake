<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ClientController@site')->name('site.index');

//login
Route::group(['namespace' => 'Auth'], function() {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');

    // Password Reset Routes...
    Route::get('password/reset_password','ChangePasswordController@showPassword')->name('password.change');
    Route::post('password/reset_password','ChangePasswordController@updatePassword')->name('password.update');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');
});

//dashboard
Route::get('dashboard','HomeController@index')->name('dashboard');

Route::group(['prefix' => 'api/admin'], function() {
    Route::get('/sessionAttendance','HomeController@countAttendanceBySession');
    Route::get('/searchAndSort', 'HomeController@searchAndSort');
    Route::post('/updateAttendance/{id}', 'HomeController@updateAttendance');
    Route::post('/updateRedemption/{id}', 'HomeController@updateRedemption');

});

//Invalid
Route::get('invalid-list','InvalidController@invalidList')->name('admin.invalid-list');

Route::group(['prefix' => 'api/admin'], function() {
    Route::get('/getInvalidClient', 'InvalidController@getInvalidClient');
    Route::post('/updateInvalidStatus/{id}', 'InvalidController@updateInvalidStatus');
    Route::post('/deleteClient/{id}', 'InvalidController@deleteClient');
});

Route::get('create-rsvp', 'ClientController@showRsvp')->name('admin.show-rsvp');

Route::group(['prefix' => 'api/admin'], function(){
    Route::post('/createRsvp', 'ClientController@createRsvp')->name('admin.rsvp.save');
});

Route::get('export-list','ExcelController@export')->name('exportList');



