Vue.component('comp-pagination',{
	props: ['totalPage','currentPageLimit','currentPageNum'],
	template: '<div class="filter-container">\
			<div class="pagination-wrapper">\
				<div class="field-wrapper input-group d-flex">\
					<div>\
						<strong style="color:#858e98;">Entries per page:</strong>\
						<select name="" id="" class="" v-model="pageLimit" @change="limitChanged($event.target.value)">\
							<option :value="pageLimitOption" v-for="pageLimitOption in pageLimitArray">{{ pageLimitOption}}</option>\
						</select>\
					</div>\
					<div class="d-flex right-paginate">\
						<div class="input-group align-center">\
							<div id="first-page" class="pagi-control pr-10" @click="pageChanged(1)"><span class="fas fa-fast-backward font-14"></span></div>\
							<div id="prev-page" class="pagi-control" @click="pageChanged(pageNum == 1 ? 1 : parseInt(pageNum) - 1)"><span class="fas fa-backward font-14"></span></div>\
						</div>\
						\
						<div class="input-group align-center pl-20 pr-20">\
							Page\
							<div class="select-page">\
								<select name="" id="" class="ml-13 mr-13" v-model="pageNum" @change="pageChanged($event.target.value)">\
									<option :value="num" v-for="num in totalPage">{{ num }}</option>\
								</select>\
							</div>\
							of {{ totalPage }}\
						</div>\
						\
						<div class="input-group align-center">\
							<div id="next-page" class="pagi-control pr-10" @click="pageChanged(pageNum == totalPage ? totalPage : parseInt(pageNum) + 1)"><span class="fas fa-forward font-14"></span></div>\
							<div id="last-page" class="pagi-control" @click="pageChanged(totalPage)"><span class="fas fa-fast-forward font-14"></span></div>\
						</div>\
						\
						<div class="input-group align-center pl-20 pr-16">\
							<div id="btn-refresh" class="pagi-control"><span class="fas fa-sync-alt font-14" @click="refreshFilter"></span></div>\
						</div>\
					</div>\
				</div>\
			</div>\
		</div>',
	data: function(){
		return {
			pageLimit: 10,
			pageNum: 1,
			pageLimitArray: [10,25,50,100],
		}
	},
	updated: function(){
		this.pageNum = this.currentPageNum;
		this.pageLimit = this.currentPageLimit;
	},
	methods: {
		limitChanged: function(limit){
			this.pageLimit = limit;
			this.$emit('return-page-limit',limit);

		},
		pageChanged: function(pageNum){
			this.pageNum = pageNum;
			this.$emit('return-page-num',pageNum);
		},
		refreshFilter: function(){
			this.pageNum = 1;
			this.pageLimit = 10;
			this.$emit('refresh-filter');
		}
	}
});

