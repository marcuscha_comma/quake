Vue.component('datepicker',{
	template: `
		<input class="form-control datepicker"
	        ref="input"
	        v-bind:value="value"
	        v-on:input="updateValue($event.target.value)"
	        data-date-format="yyyy-mm-dd"
	        data-date-end-date="0d"
	        placeholder="yyyy-mm-dd"
	        type="text"  />`,
	props: {
		value: moment().format('YYYY-MM-DD')
	},
	mounted: function() {

	},
	methods() {
		updateValue: function(value){
			this.$emit('input', value);
			$(this.$el).datepicker('hide');
		}
	}
})
