<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
if (htmlspecialchars($_GET["ref"]) != "") {
	// echo "yes";
	$db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$db->setQuery('SELECT hits FROM #__cus_qperks_referrals_session WHERE referral_code = "'.htmlspecialchars($_GET["ref"])  .'"');
	$hits = $db->loadResult();

	$fields = array(
		$db->quoteName('hits') . ' = ' . ($hits + 1 )
	);
	$conditions = array(
		$db->quoteName('referral_code') . ' = "'.htmlspecialchars($_GET["ref"]) .'"'
	);
	$query->update($db->quoteName('#__cus_qperks_referrals_session'))->set($fields)->where($conditions);
	$db->setQuery($query);

	$result = $db->execute();
}
JHtml::_('behavior.keepalive');
?>

<div class="row justify-content-center">
	<div class="col-xl-6 col-lg-8 col-md-10">
		<div class="club-logo mx-auto mb-4">Quake Club</div>

		<div class="card rounded-card bg-grey mb-4 club-shadow">
			<div class="card-body">

				<h2 class="f-30 mb-3 mt-0 text-center">Login</h2>
				<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post"
				class="form-validate club-form">

					<div class="field-placey form-group text-center">
						<i class="fas fa-envelope form-icon"></i>
						<input type="text" name="username" id="username" placeholder="example@email.com" value="" class="validate-username required"
							size="25" required="" aria-required="true" autofocus="" autocomplete="quake" readonly onfocus="this.removeAttribute('readonly');" >
						<label id="username-lbl" for="username" class="required">Email</label>
					</div>

					<div class="field-placey form-group text-center">
						<i class="fas fa-key form-icon"></i>
						<i class="far fa-eye-slash toggle-password"></i>
						<input type="password" name="password" id="password" placeholder=" " value="" class="validate-password required form-control" size="25" maxlength="99" required="" aria-required="true" autocomplete="quake" readonly >
						<label id="password-lbl" for="password" class="required">Password</label>
					</div>

					<?php if ($this->tfa): ?>
					<div class="form-group">
						<?php echo $this->form->getField('secretkey')->label; ?>
						<?php echo $this->form->getField('secretkey')->input; ?>
					</div>
					<?php endif; ?>

					<div class="form-group py-2">
						<button type="submit" class="btn btn-pink btn-block">
							Login now
						</button>
					</div>

					<div class="text-center">
						<a class="bold link-underline font-brand" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
							<?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a>


							<div class="custom-control custom-checkbox mt-4 remember-me">
							  <input class="custom-control-input" id="remember" type="checkbox" name="remember" class="inputbox" value="yes" />
							  <label class="custom-control-label" for="remember"><?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME'); ?></label>
							</div>
					</div>

					<input type="hidden" name="return"
						value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</div>
			</div>
		</form>

		<div class="card rounded-card bg-blue mb-4 club-shadow">
			<div class="card-body">
				<h2 class="f-24 text-white mb-3">
					<div class="f-20 mb-1">
						Not a member yet?
					</div>
					Get 100 Q-Perks when you sign up!
				</h2>
				<a class="btn btn-white btn-wide" href="./notice">
					Sign up
				</a>
			</div>
		</div>

		<div class="card rounded-card bg-orange club-shadow">
			<div class="card-body text-white">
				<div class="row align-items-center">
					<div class="col-md-4">
						<!-- <img src="images/2020/club/plane-left.png" class="mb-2 mb-md-0" alt=""> -->
					</div>
					<div class="col-md-8 order-md-first">
						<h2 class="f-24 text-white">
							How it works
						</h2>
						<p class="f-14 line-height font-brand">Learn more about Quake Club through our FAQ.</p>
						<a class="btn btn-white btn-wide" href="./faq#faq-8">
							View now
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<img src="images/2020/club/girl-box-01.png" class="login-girl-1 float-deco" alt="">
<img src="images/2020/club/girl-speaker-01.png" class="login-girl-2 float-deco" alt="">
<img src="images/2020/club/men-01.png" class="login-men float-deco" alt="">
<img src="images/2020/club/balloon-01.png" class="balloon float-deco" alt="">

<script type="text/javascript">
	var x = document.referrer;
	var res = x.split("/");
	var now = new Date();
	var time = now.getTime();
	var expireTime = time + 1000*3600*3;
	now.setTime(expireTime);
	
	if (res[2] == "43.228.245.193") {
		document.cookie = "preUrl="+x+"; expires=" + now.toUTCString()+';path=/';
	}
	
	jQuery(".toggle-password").click(function() {

		jQuery(this).toggleClass("fa-eye fa-eye-slash");
		var input = jQuery(this).next();
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});

	jQuery('#password').on('click focus touchstart', function() {
		jQuery(this).attr("readonly", false);
 	});
</script>
