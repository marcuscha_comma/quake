<?php /* /home/quakeup/domains/quake-develop.com.my/public_html/administrator/components/com_admintools/tmpl/ConfigureFixPermissions/default.blade.php */ ?>
<?php
/**
 * @package   admintools
 * @copyright Copyright (c)2010-2021 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license   GNU General Public License version 3, or later
 */

defined('_JEXEC') || die;

/** @var    $this   \Akeeba\AdminTools\Admin\View\ConfigureFixPermissions\Html */

$path = $this->at_path . (empty($this->at_path) ? '' : '/');

?>
<div class="akeeba-panel--info">
	<header class="akeeba-block-header">
		<h3><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_DEFAULTS'); ?></h3>
	</header>
	<form name="defaultsForm" id="defaultsForm" action="index.php" method="post" class="akeeba-form--inline">
		<div class="akeeba-form-group">
			<label for="perms_show_hidden"><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_SHOW_HIDDEN'); ?></label>
			<?php echo \Joomla\CMS\HTML\HTMLHelper::_('FEFHelp.select.booleanswitch', 'perms_show_hidden', $this->perms_show_hidden); ?>
			&nbsp;
		</div>

		<div class="akeeba-form-group">
			<label for="dirperms"><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_DEFDIRPERM'); ?></label>
			<?php echo \Akeeba\AdminTools\Admin\Helper\Select::perms('dirperms', [], $this->dirperms); ?>

			&nbsp;
		</div>

		<div class="akeeba-form-group">
			<label for="fileperms"><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_DEFFILEPERMS'); ?></label>
			<?php echo \Akeeba\AdminTools\Admin\Helper\Select::perms('fileperms', [], $this->fileperms); ?>

			&nbsp;
		</div>

		<div class="akeeba-form-group--actions">
			<input type="submit" class="akeeba-btn--primary"
			   value="<?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_SAVEDEFAULTS'); ?>"/>
		</div>

		<input type="hidden" name="option" value="com_admintools"/>
		<input type="hidden" name="view" value="ConfigureFixPermissions"/>
		<input type="hidden" name="task" value="savedefaults"/>
		<input type="hidden" name="<?php echo $this->container->platform->getToken(true); ?>" value="1"/>
	</form>
</div>


<?php if ( ! (empty($this->listing['crumbs']))): ?>
	<ul class="breadcrumb">
		<li>
			<?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_PATH'); ?>:
			<a href="index.php?option=com_admintools&view=ConfigureFixPermissions&path=/">
				<?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_ROOT'); ?>
			</a>
			<span class="divider">/</span>
		</li>

		<?php
		$relpath = '';
		$i = 1;
		?>
		<?php foreach($this->listing['crumbs'] as $crumb): ?>
			<?php if ( ! (empty($crumb))): ?>
				<?php
				$i++;
				$relpath = ltrim($relpath . '/' . $crumb, '/');
				?>
			<?php endif; ?>
			<li>
				<a href="index.php?option=com_admintools&view=ConfigureFixPermissions&path=<?php echo $this->escape(urlencode($relpath)); ?>">
					<?php echo $this->escape($this->escape($crumb)); ?>

				</a>
				<?php if($i < (is_array($this->listing['crumbs']) || $this->listing['crumbs'] instanceof \Countable ? count($this->listing['crumbs']) : 0)): ?>
					<span class="divider">/</span>
				<?php endif; ?>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>

<form name="adminForm" id="adminForm" action="index.php" method="post">
	<input type="hidden" name="option" value="com_admintools"/>
	<input type="hidden" name="view" value="ConfigureFixPermissions"/>
	<input type="hidden" name="task" value="saveperms"/>
	<input type="hidden" name="path" value="<?php echo $this->escape($this->at_path); ?>"/>
	<input type="hidden" name="<?php echo $this->container->platform->getToken(true); ?>" value="1"/>

	<input type="submit" class="akeeba-btn--green" value="<?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_SAVEPERMS'); ?>"/>
	<input type="submit" class="akeeba-btn--orange"
		   value="<?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_SAVEAPPLYPERMS'); ?>"
		   onclick="document.forms.adminForm.task.value='saveapplyperms';"/>

	<div class="akeeba-container--50-50">
        <table class="akeeba-table--striped">
            <thead>
            <tr>
                <th><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_FOLDER'); ?></th>
                <th><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_OWNER'); ?></th>
                <th colspan="2"><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_PERMS'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($this->listing['folders'])): ?>
				<?php foreach($this->listing['folders'] as $folder): ?>
                <tr>
                    <td>
                        <a href="index.php?option=com_admintools&view=ConfigureFixPermissions&path=<?php echo $this->escape(urlencode($folder['path'])); ?>">
                            <?php echo $this->escape($this->escape($folder['item'])); ?>


                        </a>
                    </td>
                    <td>
                        <?php echo $this->escape($this->renderUGID($folder['uid'], $folder['gid'])); ?>


                    </td>
                    <td>
                        <?php echo $this->escape($this->renderPermissions($folder['realperms'])); ?>


                    </td>
                    <td align="right">
                        <?php echo \Akeeba\AdminTools\Admin\Helper\Select::perms('folders[' . $folder['path'] . ']', array('class' => 'input-mini'), $folder['perms']); ?>


                    </td>
                </tr>
            	<?php endforeach; ?>
			<?php endif; ?>
            </tbody>
        </table>

        <table class="akeeba-table--striped">
            <thead>
            <tr>
                <th><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_FILE'); ?></th>
                <th><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_OWNER'); ?></th>
                <th colspan="2"><?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_PERMS'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($this->listing['files'])): ?>
				<?php foreach($this->listing['files'] as $file): ?>
                <tr>
                    <td>
                        <?php echo $this->escape($this->escape($file['item'])); ?>


                    </td>
                    <td>
                        <?php echo $this->escape($this->renderUGID($file['uid'], $file['gid'])); ?>


                    </td>
                    <td>
                        <?php echo $this->escape($this->renderPermissions($file['realperms'])); ?>


                    </td>
                    <td align="right">
                        <?php echo \Akeeba\AdminTools\Admin\Helper\Select::perms('files[' . $file['path'] . ']', array('class' => 'input-mini'), $file['perms']); ?>

                    </td>
                </tr>
            	<?php endforeach; ?>
			<?php endif; ?>
            </tbody>
        </table>
	</div>

    <p></p>

	<p>
		<input type="submit" class="akeeba-btn--green"
			   value="<?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_SAVEPERMS'); ?>"/>
		<input type="submit" class="akeeba-btn--orange"
			   value="<?php echo \Joomla\CMS\Language\Text::_('COM_ADMINTOOLS_LBL_CONFIGUREFIXPERMISSIONS_SAVEAPPLYPERMS'); ?>"
			   onclick="document.forms.adminForm.task.value='saveapplyperms';"/>
	</p>
</form>
