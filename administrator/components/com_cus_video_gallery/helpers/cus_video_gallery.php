<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Cus_video_gallery
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;

/**
 * Cus_video_gallery helper.
 *
 * @since  1.6
 */
class Cus_video_galleryHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  string
	 *
	 * @return void
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_CUS_VIDEO_GALLERY_TITLE_GALLERIES'),
			'index.php?option=com_cus_video_gallery&view=galleries',
			$vName == 'galleries'
		);

JHtmlSidebar::addEntry(
			JText::_('COM_CUS_VIDEO_GALLERY_TITLE_VIDEOGALLERYCUSTOMISELINKS'),
			'index.php?option=com_cus_video_gallery&view=videogallerycustomiselinks',
			$vName == 'videogallerycustomiselinks'
		);

JHtmlSidebar::addEntry(
			JText::_('COM_CUS_VIDEO_GALLERY_TITLE_VIDEOGALLERYCATEGORIES'),
			'index.php?option=com_cus_video_gallery&view=videogallerycategories',
			$vName == 'videogallerycategories'
		);

	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = Factory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return    JObject
	 *
	 * @since    1.6
	 */
	public static function getActions()
	{
		$user   = Factory::getUser();
		$result = new JObject;

		$assetName = 'com_cus_video_gallery';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}

