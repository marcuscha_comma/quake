DROP TABLE IF EXISTS `#__cus_video_gallery`;
DROP TABLE IF EXISTS `#__cus_video_gallery_customise_link`;
DROP TABLE IF EXISTS `#__cus_video_gallery_categories`;

DELETE FROM `#__content_types` WHERE (type_alias LIKE 'com_cus_video_gallery.%');