<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Enquiry
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_enquiry'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Enquiry', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('EnquiryHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'enquiry.php');

$controller = BaseController::getInstance('Enquiry');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
