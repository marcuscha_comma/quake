CREATE TABLE IF NOT EXISTS `#__cus_enquiry` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL  DEFAULT 0,
`state` TINYINT(1)  NOT NULL  DEFAULT 1,
`checked_out` INT(11)  NOT NULL  DEFAULT 0,
`checked_out_time` DATETIME NOT NULL  DEFAULT "0000-00-00 00:00:00",
`created_by` INT(11)  NOT NULL  DEFAULT 0,
`modified_by` INT(11)  NOT NULL  DEFAULT 0,
`name` VARCHAR(255)  NOT NULL  DEFAULT "",
`email` VARCHAR(255)  NOT NULL  DEFAULT "",
`contact` VARCHAR(255)  NOT NULL  DEFAULT "",
`designation` VARCHAR(255)  NOT NULL  DEFAULT "",
`company_name` VARCHAR(255)  NOT NULL  DEFAULT "",
`general` VARCHAR(255)  NOT NULL  DEFAULT "",
`created_at` DATETIME NOT NULL  DEFAULT "0000-00-00 00:00:00",
`department` VARCHAR(255)  NOT NULL  DEFAULT "",
`interested` TEXT NOT NULL ,
`target_region` VARCHAR(255)  NOT NULL  DEFAULT "",
`content` TEXT NOT NULL ,
`ip` VARCHAR(255)  NOT NULL  DEFAULT "",
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `field_mappings`, `content_history_options`, `rules`)
SELECT * FROM ( SELECT 'Enquiry','com_enquiry.tenquiry','{"special":{"dbtable":"#__cus_enquiry","key":"id","type":"Tenquiry","prefix":"EnquiryTable"}}', CASE 
                                WHEN 'field_mappings' is null THEN ''
                                ELSE ''
                                END as field_mappings, '{"formFile":"administrator\/components\/com_enquiry\/models\/forms\/tenquiry.xml", "hideFields":["checked_out","checked_out_time","params","language" ,"content"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}', " ") AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_enquiry.tenquiry')
) LIMIT 1;
