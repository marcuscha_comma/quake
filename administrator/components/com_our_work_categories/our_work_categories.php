<?php
/**
 * @version    CVS: 1.0.2
 * @package    Com_Our_work_categories
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_our_work_categories'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Our_work_categories', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Our_work_categoriesHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'our_work_categories.php');

$controller = BaseController::getInstance('Our_work_categories');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
