<?php
/**
 * @version    CVS: 1.0.2
 * @package    Com_Our_work_categories
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Ourworkcategory controller class.
 *
 * @since  1.6
 */
class Our_work_categoriesControllerOurworkcategory extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'ourworkcategories';
		parent::__construct();
	}
}
