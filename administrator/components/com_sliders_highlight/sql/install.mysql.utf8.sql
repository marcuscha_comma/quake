CREATE TABLE IF NOT EXISTS `#__cus_sliders_highlight` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`title` VARCHAR(100)  NOT NULL ,
`description` TEXT NOT NULL ,
`tag` VARCHAR(50)  NOT NULL ,
`image` VARCHAR(255)  NOT NULL ,
`url_address` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

