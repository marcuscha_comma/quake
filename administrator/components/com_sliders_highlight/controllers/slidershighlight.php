<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Sliders_highlight
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Slidershighlight controller class.
 *
 * @since  1.6
 */
class Sliders_highlightControllerSlidershighlight extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'slidershighlights';
		parent::__construct();
	}
}
