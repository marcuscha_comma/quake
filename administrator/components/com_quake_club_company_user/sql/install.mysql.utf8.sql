CREATE TABLE IF NOT EXISTS `#__cus_qperks_company_user` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`user_id` INT(11)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`address` TEXT NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`mobile_no` VARCHAR(255)  NOT NULL ,
`industry` VARCHAR(255)  NOT NULL ,
`classification` VARCHAR(255)  NOT NULL ,
`designation` VARCHAR(255)  NOT NULL ,
`business_card` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

