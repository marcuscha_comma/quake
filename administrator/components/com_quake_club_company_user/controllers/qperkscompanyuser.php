<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_company_user
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Qperkscompanyuser controller class.
 *
 * @since  1.6
 */
class Quake_club_company_userControllerQperkscompanyuser extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'qperkscompanyusers';
		parent::__construct();
	}
}
