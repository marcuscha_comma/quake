<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Sliders_banner_vg
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Access\Access;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Table\Table as Table;

/**
 * slidersbannervg Table class
 *
 * @since  1.6
 */
class Sliders_banner_vgTableslidersbannervg extends Table
{
	
	/**
	 * Constructor
	 *
	 * @param   JDatabase  &$db  A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__cus_sliders_banner_vg', 'id', $db);
		JTableObserverTags::createObserver($this, array('typeAlias' => 'com_sliders_banner_vg.slidersbannervg'));
		JTableObserverContenthistory::createObserver($this, array('typeAlias' => 'com_sliders_banner_vg.slidersbannervg'));
        $this->setColumnAlias('published', 'state');
    }

	/**
	 * Overloaded bind function to pre-process the params.
	 *
	 * @param   array  $array   Named array
	 * @param   mixed  $ignore  Optional array or list of parameters to ignore
	 *
	 * @return  null|string  null is operation was satisfactory, otherwise returns an error
	 *
	 * @see     JTable:bind
	 * @since   1.5
     * @throws Exception
	 */
	public function bind($array, $ignore = '')
	{
	    $date = Factory::getDate();
		$task = Factory::getApplication()->input->get('task');
	    
		$input = JFactory::getApplication()->input;
		$task = $input->getString('task', '');

		if ($array['id'] == 0 && empty($array['created_by']))
		{
			$array['created_by'] = JFactory::getUser()->id;
		}

		if ($array['id'] == 0 && empty($array['modified_by']))
		{
			$array['modified_by'] = JFactory::getUser()->id;
		}

		if ($task == 'apply' || $task == 'save')
		{
			$array['modified_by'] = JFactory::getUser()->id;
		}

		// Support for multiple field: category
		if (isset($array['category']))
		{
			if (is_array($array['category']))
			{
				$array['category'] = implode(',',$array['category']);
			}
			elseif (strpos($array['category'], ',') != false)
			{
				$array['category'] = explode(',',$array['category']);
			}
			elseif (strlen($array['category']) == 0)
			{
				$array['category'] = '';
			}
		}
		else
		{
			$array['category'] = '';
		}

		// Support for multiple field: link_type
		if (isset($array['link_type']))
		{
			if (is_array($array['link_type']))
			{
				$array['link_type'] = implode(',',$array['link_type']);
			}
			elseif (strpos($array['link_type'], ',') != false)
			{
				$array['link_type'] = explode(',',$array['link_type']);
			}
			elseif (strlen($array['link_type']) == 0)
			{
				$array['link_type'] = '';
			}
		}
		else
		{
			$array['link_type'] = '';
		}

		// Support for multiple field: target_window
		if (isset($array['target_window']))
		{
			if (is_array($array['target_window']))
			{
				$array['target_window'] = implode(',',$array['target_window']);
			}
			elseif (strpos($array['target_window'], ',') != false)
			{
				$array['target_window'] = explode(',',$array['target_window']);
			}
			elseif (strlen($array['target_window']) == 0)
			{
				$array['target_window'] = '';
			}
		}
		else
		{
			$array['target_window'] = '';
		}
		// Support for multi file field: video_file
		if (!empty($array['video_file']))
		{
			if (is_array($array['video_file']))
			{
				$array['video_file'] = implode(',', $array['video_file']);
			}
			elseif (strpos($array['video_file'], ',') != false)
			{
				$array['video_file'] = explode(',', $array['video_file']);
			}
		}
		else
		{
			$array['video_file'] = '';
		}


		if (isset($array['params']) && is_array($array['params']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['params']);
			$array['params'] = (string) $registry;
		}

		if (isset($array['metadata']) && is_array($array['metadata']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['metadata']);
			$array['metadata'] = (string) $registry;
		}

		if (!Factory::getUser()->authorise('core.admin', 'com_sliders_banner_vg.slidersbannervg.' . $array['id']))
		{
			$actions         = Access::getActionsFromFile(
				JPATH_ADMINISTRATOR . '/components/com_sliders_banner_vg/access.xml',
				"/access/section[@name='slidersbannervg']/"
			);
			$default_actions = Access::getAssetRules('com_sliders_banner_vg.slidersbannervg.' . $array['id'])->getData();
			$array_jaccess   = array();

			foreach ($actions as $action)
			{
                if (key_exists($action->name, $default_actions))
                {
                    $array_jaccess[$action->name] = $default_actions[$action->name];
                }
			}

			$array['rules'] = $this->JAccessRulestoArray($array_jaccess);
		}

		// Bind the rules for ACL where supported.
		if (isset($array['rules']) && is_array($array['rules']))
		{
			$this->setRules($array['rules']);
		}

		return parent::bind($array, $ignore);
	}

	/**
	 * This function convert an array of JAccessRule objects into an rules array.
	 *
	 * @param   array  $jaccessrules  An array of JAccessRule objects.
	 *
	 * @return  array
	 */
	private function JAccessRulestoArray($jaccessrules)
	{
		$rules = array();

		foreach ($jaccessrules as $action => $jaccess)
		{
			$actions = array();

			if ($jaccess)
			{
				foreach ($jaccess->getData() as $group => $allow)
				{
					$actions[$group] = ((bool)$allow);
				}
			}

			$rules[$action] = $actions;
		}

		return $rules;
	}

	/**
	 * Overloaded check function
	 *
	 * @return bool
	 */
	public function check()
	{
		// If there is an ordering column and this is a new row then get the next ordering value
		if (property_exists($this, 'ordering') && $this->id == 0)
		{
			$this->ordering = self::getNextOrder();
		}
		
		
		// Support multi file field: video_file
		$app = JFactory::getApplication();
		$files = $app->input->files->get('jform', array(), 'raw');
		$array = $app->input->get('jform', array(), 'ARRAY');

		if ($files['video_file'][0]['size'] > 0)
		{
			// Deleting existing files
			$oldFiles = Sliders_banner_vgHelper::getFiles($this->id, $this->_tbl, 'video_file');

			foreach ($oldFiles as $f)
			{
				$oldFile = JPATH_ROOT . '/uploads/video-gallery/' . $f;

				if (file_exists($oldFile) && !is_dir($oldFile))
				{
					unlink($oldFile);
				}
			}

			$this->video_file = "";

			foreach ($files['video_file'] as $singleFile )
			{
				jimport('joomla.filesystem.file');

				// Check if the server found any error.
				$fileError = $singleFile['error'];
				$message = '';

				if ($fileError > 0 && $fileError != 4)
				{
					switch ($fileError)
					{
						case 1:
							$message = JText::_('File size exceeds allowed by the server');
							break;
						case 2:
							$message = JText::_('File size exceeds allowed by the html form');
							break;
						case 3:
							$message = JText::_('Partial upload error');
							break;
					}

					if ($message != '')
					{
						$app->enqueueMessage($message, 'warning');

						return false;
					}
				}
				elseif ($fileError == 4)
				{
					if (isset($array['video_file']))
					{
						$this->video_file = $array['video_file'];
					}
				}
				else
				{

					// Check for filetype
					$okMIMETypes = 'video/mp4';
					$validMIMEArray = explode(',', $okMIMETypes);
					$fileMime = $singleFile['type'];

					if (!in_array($fileMime, $validMIMEArray))
					{
						$app->enqueueMessage('This filetype is not allowed', 'warning');

						return false;
					}

					// Replace any special characters in the filename
					jimport('joomla.filesystem.file');
					$filename = JFile::stripExt($singleFile['name']);
					$extension = JFile::getExt($singleFile['name']);
					$filename = preg_replace("/[^A-Za-z0-9]/i", "-", $filename);
					$filename = $filename . '.' . $extension;
					$uploadPath = JPATH_ROOT . '/uploads/video-gallery/' . $filename;
					$fileTemp = $singleFile['tmp_name'];

					if (!JFile::exists($uploadPath))
					{
						if (!JFile::upload($fileTemp, $uploadPath))
						{
							$app->enqueueMessage('Error moving file', 'warning');

							return false;
						}
					}

					$this->video_file .= (!empty($this->video_file)) ? "," : "";
					$this->video_file .= $filename;
				}
			}
		}
		else
		{
			$this->video_file .= $array['video_file_hidden'];
		}

		return parent::check();
	}

	/**
	 * Define a namespaced asset name for inclusion in the #__assets table
	 *
	 * @return string The asset name
	 *
	 * @see Table::_getAssetName
	 */
	protected function _getAssetName()
	{
		$k = $this->_tbl_key;

		return 'com_sliders_banner_vg.slidersbannervg.' . (int) $this->$k;
	}

	/**
	 * Returns the parent asset's id. If you have a tree structure, retrieve the parent's id using the external key field
	 *
	 * @param   JTable   $table  Table name
	 * @param   integer  $id     Id
	 *
	 * @see Table::_getAssetParentId
	 *
	 * @return mixed The id on success, false on failure.
	 */
	protected function _getAssetParentId(JTable $table = null, $id = null)
	{
		// We will retrieve the parent-asset from the Asset-table
		$assetParent = Table::getInstance('Asset');

		// Default: if no asset-parent can be found we take the global asset
		$assetParentId = $assetParent->getRootId();

		// The item has the component as asset-parent
		$assetParent->loadByName('com_sliders_banner_vg');

		// Return the found asset-parent-id
		if ($assetParent->id)
		{
			$assetParentId = $assetParent->id;
		}

		return $assetParentId;
	}

	
    /**
     * Delete a record by id
     *
     * @param   mixed  $pk  Primary key value to delete. Optional
     *
     * @return bool
     */
    public function delete($pk = null)
    {
        $this->load($pk);
        $result = parent::delete($pk);
        
		if ($result)
		{
			jimport('joomla.filesystem.file');

			$checkImageVariableType = gettype($this->video_file);

			switch ($checkImageVariableType)
			{
			case 'string':
				JFile::delete(JPATH_ROOT . '/uploads/video-gallery/' . $this->video_file);
			break;
			default:
			foreach ($this->video_file as $video_fileFile)
			{
				JFile::delete(JPATH_ROOT . '/uploads/video-gallery/' . $video_fileFile);
			}
			}
		}

        return $result;
    }

	
}
