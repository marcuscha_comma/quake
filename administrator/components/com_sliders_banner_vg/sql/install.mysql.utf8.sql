CREATE TABLE IF NOT EXISTS `#__cus_sliders_banner_vg` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL DEFAULT 0,
`state` TINYINT(1)  NOT NULL DEFAULT 1,
`checked_out` INT(11)  NOT NULL DEFAULT 0,
`checked_out_time` DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00",
`created_by` INT(11)  NOT NULL DEFAULT 0,
`modified_by` INT(11)  NOT NULL DEFAULT 0,
`title` VARCHAR(80)  NOT NULL ,
`category` TEXT NOT NULL ,
`button_text` VARCHAR(20)  NOT NULL ,
`button_url` VARCHAR(255)  NOT NULL ,
`publish_date` VARCHAR(255)  NOT NULL ,
`desktop_image` VARCHAR(255)  NOT NULL ,
`link_type` VARCHAR(255)  NOT NULL ,
`url_address` VARCHAR(255)  NOT NULL ,
`youtube_link` VARCHAR(100)  NOT NULL ,
`target_window` VARCHAR(255)  NOT NULL ,
`video_file` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `field_mappings`, `content_history_options`)
SELECT * FROM ( SELECT 'Sliders banner vg','com_sliders_banner_vg.slidersbannervg','{"special":{"dbtable":"#__cus_sliders_banner_vg","key":"id","type":"Slidersbannervg","prefix":"Sliders_banner_vgTable"}}', CASE 
                                WHEN 'field_mappings' is null THEN ''
                                ELSE ''
                                END as field_mappings, '{"formFile":"administrator\/components\/com_sliders_banner_vg\/models\/forms\/slidersbannervg.xml", "hideFields":["checked_out","checked_out_time","params","language" ,"video_file"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_sliders_banner_vg.slidersbannervg')
) LIMIT 1;
