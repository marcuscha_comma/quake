<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_cart
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_quake_club_cart'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Quake_club_cart', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Quake_club_cartHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'quake_club_cart.php');

$controller = JControllerLegacy::getInstance('Quake_club_cart');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
