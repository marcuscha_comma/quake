<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Banner_v1ewership
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_banner_v1ewership'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Banner_v1ewership', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Banner_v1ewershipHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'banner_v1ewership.php');

$controller = JControllerLegacy::getInstance('Banner_v1ewership');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
