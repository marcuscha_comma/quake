<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Banner_v1ewership
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Bannerv1ewership controller class.
 *
 * @since  1.6
 */
class Banner_v1ewershipControllerBannerv1ewership extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'bannerv1ewerships';
		parent::__construct();
	}
}
