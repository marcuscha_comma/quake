<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rate_card_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_rate_card_files'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Rate_card_files', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Rate_card_filesHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'rate_card_files.php');

$controller = BaseController::getInstance('Rate_card_files');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
