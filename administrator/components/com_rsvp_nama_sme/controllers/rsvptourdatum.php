<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_nama_sme
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Rsvptourdatum controller class.
 *
 * @since  1.6
 */
class Rsvp_nama_smeControllerRsvptourdatum extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'rsvptourdata';
		parent::__construct();
	}
}
