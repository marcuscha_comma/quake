<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Digitals
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_digitals'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Digitals', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('DigitalsHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'digitals.php');

$controller = BaseController::getInstance('Digitals');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
