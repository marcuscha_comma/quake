<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Digitals
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Digital controller class.
 *
 * @since  1.6
 */
class DigitalsControllerDigital extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'digitals';
		parent::__construct();
	}
}
