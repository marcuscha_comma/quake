<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_qperks_monthly
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_quake_club_qperks_monthly/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.year').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('yearhidden')){
			js('#jform_year option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_year").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'quakeclubqperksmonthly.cancel') {
			Joomla.submitform(task, document.getElementById('quakeclubqperksmonthly-form'));
		}
		else {
			
			if (task != 'quakeclubqperksmonthly.cancel' && document.formvalidator.isValid(document.id('quakeclubqperksmonthly-form'))) {
				
				Joomla.submitform(task, document.getElementById('quakeclubqperksmonthly-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_quake_club_qperks_monthly&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="quakeclubqperksmonthly-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_QUAKE_CLUB_QPERKS_MONTHLY_TITLE_QUAKECLUBQPERKSMONTHLY', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>				<?php echo $this->form->renderField('user_id'); ?>
				<?php echo $this->form->renderField('year'); ?>

			<?php
				foreach((array)$this->item->year as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="year" name="jform[yearhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('month01'); ?>
				<?php echo $this->form->renderField('month02'); ?>
				<?php echo $this->form->renderField('month03'); ?>
				<?php echo $this->form->renderField('month04'); ?>
				<?php echo $this->form->renderField('month05'); ?>
				<?php echo $this->form->renderField('month06'); ?>
				<?php echo $this->form->renderField('month07'); ?>
				<?php echo $this->form->renderField('month08'); ?>
				<?php echo $this->form->renderField('month09'); ?>
				<?php echo $this->form->renderField('month10'); ?>
				<?php echo $this->form->renderField('month11'); ?>
				<?php echo $this->form->renderField('month12'); ?>
				<?php echo $this->form->renderField('total'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
