CREATE TABLE IF NOT EXISTS `#__cus_quake_club_qperks_monthly` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`user_id` INT(11)  NOT NULL ,
`year` TEXT NOT NULL ,
`month01` DOUBLE,
`month02` DOUBLE,
`month03` DOUBLE,
`month04` DOUBLE,
`month05` DOUBLE,
`month06` DOUBLE,
`month07` DOUBLE,
`month08` DOUBLE,
`month09` DOUBLE,
`month10` DOUBLE,
`month11` DOUBLE,
`month12` DOUBLE,
`total` DOUBLE,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

