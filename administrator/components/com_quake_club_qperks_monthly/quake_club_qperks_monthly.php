<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_qperks_monthly
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_quake_club_qperks_monthly'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Quake_club_qperks_monthly', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Quake_club_qperks_monthlyHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'quake_club_qperks_monthly.php');

$controller = JControllerLegacy::getInstance('Quake_club_qperks_monthly');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
