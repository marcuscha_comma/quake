<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Clients_data
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_clients_data'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Clients_data', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Clients_dataHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'clients_data.php');

$controller = JControllerLegacy::getInstance('Clients_data');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
