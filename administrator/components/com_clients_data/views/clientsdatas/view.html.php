<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Clients_data
 * @author     CM <marcus.cha@comma.com.my>
 * @copyright  2021 CM
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

use \Joomla\CMS\Language\Text;

/**
 * View class for a list of Clients_data.
 *
 * @since  1.6
 */
class Clients_dataViewClientsdatas extends \Joomla\CMS\MVC\View\HtmlView //extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		Clients_dataHelper::addSubmenu('clientsdatas');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = Clients_dataHelper::getActions();

		JToolBarHelper::title(JText::_('COM_CLIENTS_DATA_TITLE_CLIENTSDATAS'), 'clientsdatas.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/clientsdata';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('clientsdata.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('clientsdatas.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
					JToolBarHelper::custom('clientsdatas.export'/*clientsdatas.exportCsv'*/, 'copy.png', 'copy_f2.png', 'Export', false);//export function
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('clientsdata.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('clientsdatas.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('clientsdatas.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'clientsdatas.delete', 'JTOOLBAR_DELETE');
				JToolBarHelper::custom('clientsdatas.export', 'copy.png', 'copy_f2.png', 'Export', false);//Export funtion
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('clientsdatas.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('clientsdatas.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'clientsdatas.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('clientsdatas.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_clients_data');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_clients_data&view=clientsdatas');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`email`' => JText::_('COM_CLIENTS_DATA_CLIENTSDATAS_EMAIL'),
			'a.`name`' => JText::_('COM_CLIENTS_DATA_CLIENTSDATAS_NAME'),
			'a.`phone`' => JText::_('COM_CLIENTS_DATA_CLIENTSDATAS_PHONE'),
			'a.`company`' => JText::_('COM_CLIENTS_DATA_CLIENTSDATAS_COMPANY'),
			'a.`designation`' => JText::_('COM_CLIENTS_DATA_CLIENTSDATAS_DESIGNATION'),
			'a.`user_type`' => JText::_('COM_CLIENTS_DATA_CLIENTSDATAS_USER_TYPE'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
			'a.`redeem`' => JText::_('COM_CLIENTS_DATA_CLIENTSDATAS_REDEEM'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
