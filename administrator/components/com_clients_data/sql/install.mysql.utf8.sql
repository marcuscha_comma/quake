CREATE TABLE IF NOT EXISTS `#__cus_clients_data` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`email` VARCHAR(255)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`phone` VARCHAR(50)  NOT NULL ,
`company` VARCHAR(100)  NOT NULL ,
`designation` VARCHAR(50)  NOT NULL ,
`user_type` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`created_time` DATETIME NOT NULL ,
`redeem` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

