<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Clients_data
 * @author     CM <marcus.cha@comma.com.my>
 * @copyright  2021 CM
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

use Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Session\session;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

/**
 * Clientsdatas list controller class.
 *
 * @since  1.6
 */
class Clients_dataControllerClientsdatas extends JControllerAdmin
{
    /**
     * Method to clone existing Clientsdatas
     *
     * @return void
     */
    public function duplicate()
    {
        // Check for request forgeries
        Jsession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Get id(s)
        $pks = $this->input->post->get('cid', array(), 'array');

        try
        {
            if (empty($pks))
            {
                throw new Exception(JText::_('COM_CLIENTS_DATA_NO_ELEMENT_SELECTED'));
            }

            ArrayHelper::toInteger($pks);
            $model = $this->getModel();
            $model->duplicate($pks);
            $this->setMessage(Jtext::_('COM_CLIENTS_DATA_ITEMS_SUCCESS_DUPLICATED'));
        }
        catch (Exception $e)
        {
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'warning');
        }

        $this->setRedirect('index.php?option=com_clients_data&view=clientsdatas');
    }

    /**
     * Proxy for getModel.
     *
     * @param   string  $name    Optional. Model name
     * @param   string  $prefix  Optional. Class prefix
     * @param   array   $config  Optional. Configuration array for model
     *
     * @return  object  The Model
     *
     * @since    1.6
     */
    public function getModel($name = 'clientsdata', $prefix = 'Clients_dataModel', $config = array())
    {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));

        return $model;
    }

    /**
     * Method to save the submitted ordering values for records via AJAX.
     *
     * @return  void
     *
     * @since   3.0
     */
    public function saveOrderAjax()
    {
        // Get the input
        $input = JFactory::getApplication()->input;
        $pks   = $input->post->get('cid', array(), 'array');
        $order = $input->post->get('order', array(), 'array');

        // Sanitize the input
        ArrayHelper::toInteger($pks);
        ArrayHelper::toInteger($order);

        // Get the model
        $model = $this->getModel();

        // Save the ordering
        $return = $model->saveorder($pks, $order);

        if ($return)
        {
            echo "1";
        }

        // Close the application
        JFactory::getApplication()->close();
    }  
    
    public function export()
    {

        ob_end_clean();
        $app = JFactory::getApplication();

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=subscriber_list.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $model = $this->getModel('clientsdatas');
        $result = $model->getCsv();
        echo "ID, Email, Name, Company, Designation,User Type, Ordering, State, Checked_out, Export_state, Checked_out_time, Created_by, Modified_by, Created_time, Redeem, Phone, Phone_number_1, uEditor\r\n";
        
        foreach ($result as $user) {
            
            echo ucwords(trim($user->id)). ", ";
            echo $user->email . ", ";
            
            echo $user->name . ", ";
            if ($user->name==null){
                echo " - ";
            }
            echo $user->company . ", ";
            if ($user->company==null){
                echo " - ";
            }
            echo $user->designation . ", ";
            if ($user->designation==null){
                echo " - ";
            }
            echo $user->user_type . ", ";
            
            echo $user->ordering . ", ";
            
            echo $user->state . ", ";
            
            echo $user->checked_out . ", ";
            if ($user->checked_out==null){
                echo " - ";
            }
            echo $user->export_state . ", ";
            if ($user->export_state==null){
                echo " - ";
            }
            echo $user->checked_out_time . ", ";
            if ($user->checked_out_time==null){
                echo " - ";
            }
            echo $user->created_by . ", ";
            
            echo $user->modified_by . ", ";
            
            echo $user->created_time . ", ";
            
            echo $user->redeem . ", ";
            if ($user->redeem==null){
                echo " - ";
            }
            echo $user->phone . ", ";
            if ($user->phone==null){
                echo " - ";
            }
            echo $user->phone_number_1 . ", ";
            if ($user->phone_number_1==null){
                echo " - ";
            }
            echo $user->uEditor . "\r\n";
            if ($user->uEditor==null){
                echo " - ";
            }
        }

        $app->close();
      $this->setRedirect('index.php?option=com_clients_data&view=clientsdatas');
    }

    public function exportCsv()
    {
        $this->download_send_headers("subscriber_list_export_" . date("Y-m-d") . ".csv");
        $model = $this->getModel('clientsdatas');
        $array = $model->getCsvArray();
        echo $this->array2csv($array);  
        die();
    }

    function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            $row['id'] = '="' . $row['id'] . '"';
            fputcsv($df, $row);

        }

        fclose($df);
        return ob_get_clean();
    }

    function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");
    
        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
    
        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
}
