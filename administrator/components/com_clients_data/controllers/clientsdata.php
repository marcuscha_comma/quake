<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Clients_data
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Clientsdata controller class.
 *
 * @since  1.6
 */
class Clients_dataControllerClientsdata extends \Joomla\CMS\MVC\Controller\FormController //extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'clientsdatas';
		parent::__construct();
	}
}
