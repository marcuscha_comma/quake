<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_dashboard
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_qperk_dashboard'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Qperk_dashboard', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Qperk_dashboardHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'qperk_dashboard.php');

$controller = JControllerLegacy::getInstance('Qperk_dashboard');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
