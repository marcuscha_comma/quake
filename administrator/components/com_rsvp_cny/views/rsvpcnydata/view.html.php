<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_cny
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Rsvp_cny.
 *
 * @since  1.6
 */
class Rsvp_cnyViewRsvpcnydata extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		
		$count = 0;

		//get count of cny data
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( 'count(id) as count' )
			->from( $db->quoteName( '#__cus_rsvp_cny_data' ) )
			->where($db->quoteName('state')." > 0");
		$db->setQuery( $query );
		$result_array = $db->loadObjectList();

		//get count off selected cny data
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( 'count(id) as count' )
			->from( $db->quoteName( '#__cus_rsvp_cny_data' ) )
			->where($db->quoteName('state')." > 0")
			->where($db->quoteName('selected')." = 1");
		$db->setQuery( $query );
		$result2_array = $db->loadObjectList();

		$this->count_result['total_count'] = $result_array[0]->count;
		$this->count_result['selected_count'] = $result2_array[0]->count;

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		Rsvp_cnyHelper::addSubmenu('rsvpcnydata');

		$this->addToolbar();

		// $this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = Rsvp_cnyHelper::getActions();

		JToolBarHelper::title('BangDuDu2019', 'rsvpcnydata.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/rsvpcnydatum';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('rsvpcnydatum.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('rsvpcnydata.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('rsvpcnydatum.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('rsvpcnydata.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('rsvpcnydata.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'rsvpcnydata.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('rsvpcnydata.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('rsvpcnydata.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'rsvpcnydata.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('rsvpcnydata.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_rsvp_cny');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_rsvp_cny&view=rsvpcnydata');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
			'a.`company_name`' => JText::_('COM_RSVP_CNY_RSVPCNYDATA_COMPANY_NAME'),
			'a.`title`' => JText::_('COM_RSVP_CNY_RSVPCNYDATA_TITLE'),
			'a.`name`' => JText::_('COM_RSVP_CNY_RSVPCNYDATA_NAME'),
			'a.`designation`' => JText::_('COM_RSVP_CNY_RSVPCNYDATA_DESIGNATION'),
			'a.`email`' => JText::_('COM_RSVP_CNY_RSVPCNYDATA_EMAIL'),
			'a.`phone_number`' => JText::_('COM_RSVP_CNY_RSVPCNYDATA_PHONE_NUMBER'),
			'a.`selected`' => JText::_('COM_RSVP_CNY_RSVPCNYDATA_SELECTED'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
