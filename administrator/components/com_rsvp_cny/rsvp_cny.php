<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_cny
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_rsvp_cny'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Rsvp_cny', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Rsvp_cnyHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'rsvp_cny.php');

$controller = JControllerLegacy::getInstance('Rsvp_cny');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
