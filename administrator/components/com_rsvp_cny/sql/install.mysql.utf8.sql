CREATE TABLE IF NOT EXISTS `#__cus_rsvp_cny_data` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`company_name` VARCHAR(255)  NOT NULL ,
`title` VARCHAR(255)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`designation` VARCHAR(255)  NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`phone_number` VARCHAR(255)  NOT NULL ,
`selected` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

