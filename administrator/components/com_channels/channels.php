<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Channels
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_channels'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Channels', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('ChannelsHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'channels.php');

$controller = JControllerLegacy::getInstance('Channels');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
