CREATE TABLE IF NOT EXISTS `#__cus_channels` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`title` VARCHAR(100)  NOT NULL ,
`description` TEXT NOT NULL ,
`segment` TEXT NOT NULL ,
`channel_popup_label` VARCHAR(50)  NOT NULL ,
`channel_popup_text` VARCHAR(50)  NOT NULL ,
`image` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

