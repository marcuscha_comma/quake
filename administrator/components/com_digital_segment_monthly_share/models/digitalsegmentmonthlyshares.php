<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Digital_segment_monthly_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Digital_segment_monthly_share records.
 *
 * @since  1.6
 */
class Digital_segment_monthly_shareModelDigitalsegmentmonthlyshares extends \Joomla\CMS\MVC\Model\ListModel
{
    
        
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
				'title', 'a.`title`',
				'year', 'a.`year`',
				'month', 'a.`month`',
				'unique_users', 'a.`unique_users`',
				'page_views', 'a.`page_views`',
				'fb_page_reach', 'a.`fb_page_reach`',
				'social_media_followers', 'a.`social_media_followers`',
				'malay_top_1_channel', 'a.`malay_top_1_channel`',
				'malay_top_1_unique_users', 'a.`malay_top_1_unique_users`',
				'malay_top_2_channel', 'a.`malay_top_2_channel`',
				'malay_top_2_unique_users', 'a.`malay_top_2_unique_users`',
				'malay_top_3_channel', 'a.`malay_top_3_channel`',
				'malay_top_3_unique_users', 'a.`malay_top_3_unique_users`',
				'malay_top_4_channel', 'a.`malay_top_4_channel`',
				'malay_top_4_unique_users', 'a.`malay_top_4_unique_users`',
				'malay_top_5_channel', 'a.`malay_top_5_channel`',
				'malay_top_5_unique_users', 'a.`malay_top_5_unique_users`',
			);
		}

		parent::__construct($config);
	}

    
        
    
        
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
        // List state information.
        parent::populateState('', 'ASC');

        $context = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
        $this->setState('filter.search', $context);

        // Split context into component and optional section
        $parts = FieldsHelper::extract($context);

        if ($parts)
        {
            $this->setState('filter.component', $parts[0]);
            $this->setState('filter.section', $parts[1]);
        }
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

                
                    return parent::getStoreId($id);
                
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__cus_digital_segment_monthly_share` AS a');
                
		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');
                

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				
			}
		}
                

		// Filtering year
		$filter_year = $this->state->get("filter.year");

		if ($filter_year !== null && (is_numeric($filter_year) || !empty($filter_year)))
		{
			$query->where("a.`year` = '".$db->escape($filter_year)."'");
		}

		// Filtering month
		$filter_month = $this->state->get("filter.month");

		if ($filter_month !== null && (is_numeric($filter_month) || !empty($filter_month)))
		{
			$query->where("a.`month` = '".$db->escape($filter_month)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering', '');
		$orderDirn = $this->state->get('list.direction', 'ASC');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
                
		foreach ($items as $oneItem)
		{

			if (isset($oneItem->year))
			{
				$values    = explode(',', $oneItem->year);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select * from #__cus_year y where y.key =".$value;
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$oneItem->year = !empty($textValue) ? implode(', ', $textValue) : $oneItem->year;
			}

			if (isset($oneItem->month))
			{
				$values    = explode(',', $oneItem->month);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "select * from #__cus_month m where m.key =".$value;
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->value;
						}
					}
				}

				$oneItem->month = !empty($textValue) ? implode(', ', $textValue) : $oneItem->month;
			}
		}

		return $items;
	}
}
