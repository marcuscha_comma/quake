CREATE TABLE IF NOT EXISTS `#__cus_digital_segment_monthly_share` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT 1,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00",
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`title` VARCHAR(255)  NOT NULL ,
`year` TEXT NOT NULL ,
`month` TEXT NOT NULL ,
`unique_users` VARCHAR(255)  NOT NULL ,
`page_views` VARCHAR(255)  NOT NULL ,
`fb_page_reach` VARCHAR(255)  NOT NULL ,
`social_media_followers` VARCHAR(255)  NOT NULL ,
`malay_top_1_channel` TEXT NOT NULL ,
`malay_top_1_unique_users` VARCHAR(255)  NOT NULL ,
`malay_top_2_channel` TEXT NOT NULL ,
`malay_top_2_unique_users` VARCHAR(255)  NOT NULL ,
`malay_top_3_channel` TEXT NOT NULL ,
`malay_top_3_unique_users` VARCHAR(255)  NOT NULL ,
`malay_top_4_channel` TEXT NOT NULL ,
`malay_top_4_unique_users` VARCHAR(255)  NOT NULL ,
`malay_top_5_channel` TEXT NOT NULL ,
`malay_top_5_unique_users` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'Digital segment monthly share','com_digital_segment_monthly_share.digitalsegmentmonthlyshare','{"special":{"dbtable":"#__cus_digital_segment_monthly_share","key":"id","type":"Digitalsegmentmonthlyshare","prefix":"Digital_segment_monthly_shareTable"}}', '{"formFile":"administrator\/components\/com_digital_segment_monthly_share\/models\/forms\/digitalsegmentmonthlyshare.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_digital_segment_monthly_share.digitalsegmentmonthlyshare')
) LIMIT 1;
