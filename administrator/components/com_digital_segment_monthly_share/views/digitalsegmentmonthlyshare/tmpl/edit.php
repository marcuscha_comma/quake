<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Digital_segment_monthly_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_digital_segment_monthly_share/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.year').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('yearhidden')){
			js('#jform_year option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_year").trigger("liszt:updated");
	js('input:hidden.month').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('monthhidden')){
			js('#jform_month option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_month").trigger("liszt:updated");
	js('input:hidden.malay_top_1_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('malay_top_1_channelhidden')){
			js('#jform_malay_top_1_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_malay_top_1_channel").trigger("liszt:updated");
	js('input:hidden.malay_top_2_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('malay_top_2_channelhidden')){
			js('#jform_malay_top_2_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_malay_top_2_channel").trigger("liszt:updated");
	js('input:hidden.malay_top_3_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('malay_top_3_channelhidden')){
			js('#jform_malay_top_3_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_malay_top_3_channel").trigger("liszt:updated");
	js('input:hidden.malay_top_4_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('malay_top_4_channelhidden')){
			js('#jform_malay_top_4_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_malay_top_4_channel").trigger("liszt:updated");
	js('input:hidden.malay_top_5_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('malay_top_5_channelhidden')){
			js('#jform_malay_top_5_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_malay_top_5_channel").trigger("liszt:updated");
	js("#jform_month").trigger("liszt:updated");
	
	js('input:hidden.chinese_top_1_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('chinese_top_1_channelhidden')){
			js('#jform_chinese_top_1_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_chinese_top_1_channel").trigger("liszt:updated");
	js('input:hidden.chinese_top_2_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('chinese_top_2_channelhidden')){
			js('#jform_chinese_top_2_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_chinese_top_2_channel").trigger("liszt:updated");
	js('input:hidden.chinese_top_3_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('chinese_top_3_channelhidden')){
			js('#jform_chinese_top_3_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_chinese_top_3_channel").trigger("liszt:updated");
	js('input:hidden.chinese_top_4_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('chinese_top_4_channelhidden')){
			js('#jform_chinese_top_4_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_chinese_top_4_channel").trigger("liszt:updated");
	js('input:hidden.chinese_top_5_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('chinese_top_5_channelhidden')){
			js('#jform_chinese_top_5_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_chinese_top_5_channel").trigger("liszt:updated");

	js("#jform_month").trigger("liszt:updated");
	js('input:hidden.indian_top_1_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('indian_top_1_channelhidden')){
			js('#jform_indian_top_1_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_indian_top_1_channel").trigger("liszt:updated");
	js('input:hidden.indian_top_2_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('indian_top_2_channelhidden')){
			js('#jform_indian_top_2_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_indian_top_2_channel").trigger("liszt:updated");
	js('input:hidden.indian_top_3_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('indian_top_3_channelhidden')){
			js('#jform_indian_top_3_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_indian_top_3_channel").trigger("liszt:updated");
	js('input:hidden.indian_top_4_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('indian_top_4_channelhidden')){
			js('#jform_indian_top_4_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_indian_top_4_channel").trigger("liszt:updated");
	js('input:hidden.indian_top_5_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('indian_top_5_channelhidden')){
			js('#jform_indian_top_5_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_indian_top_5_channel").trigger("liszt:updated");

	js("#jform_month").trigger("liszt:updated");
	js('input:hidden.english_top_1_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('english_top_1_channelhidden')){
			js('#jform_english_top_1_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_english_top_1_channel").trigger("liszt:updated");
	js('input:hidden.english_top_2_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('english_top_2_channelhidden')){
			js('#jform_english_top_2_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_english_top_2_channel").trigger("liszt:updated");
	js('input:hidden.english_top_3_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('english_top_3_channelhidden')){
			js('#jform_english_top_3_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_english_top_3_channel").trigger("liszt:updated");
	js('input:hidden.english_top_4_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('english_top_4_channelhidden')){
			js('#jform_english_top_4_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_english_top_4_channel").trigger("liszt:updated");
	js('input:hidden.english_top_5_channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('english_top_5_channelhidden')){
			js('#jform_english_top_5_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_english_top_5_channel").trigger("liszt:updated");
});

	

	Joomla.submitbutton = function (task) {
		if (task == 'digitalsegmentmonthlyshare.cancel') {
			Joomla.submitform(task, document.getElementById('digitalsegmentmonthlyshare-form'));
		}
		else {
			
			if (task != 'digitalsegmentmonthlyshare.cancel' && document.formvalidator.isValid(document.id('digitalsegmentmonthlyshare-form'))) {
				
				Joomla.submitform(task, document.getElementById('digitalsegmentmonthlyshare-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_digital_segment_monthly_share&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="digitalsegmentmonthlyshare-form" class="form-validate form-horizontal">

	
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
	<?php echo $this->form->renderField('created_by'); ?>
	<?php echo $this->form->renderField('modified_by'); ?>
	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'segmentmonthlyshare')); ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'segmentmonthlyshare', JText::_('COM_DIGITAL_SEGMENT_MONTHLY_SHARE_TAB_SEGMENTMONTHLYSHARE', true)); ?>
	<?php JHtml::_('bootstrap.endTab'); ?>
	<?php JHtml::_('bootstrap.addTab', 'myTab', 'menu2', JText::_('COM_DIGITAL_SEGMENT_MONTHLY_SHARE_FORM_DESC_DIGITALSEGMENTMONTHLYSHARE_MAYLAY_TOP_5', true)); ?>
	<?php JHtml::_('bootstrap.endTab'); ?>
	<?php JHtml::_('bootstrap.addTab', 'myTab', 'menu3', JText::_('COM_DIGITAL_SEGMENT_MONTHLY_SHARE_FORM_DESC_DIGITALSEGMENTMONTHLYSHARE_CHINESE_TOP_5', true)); ?>
	<?php JHtml::_('bootstrap.endTab'); ?>
	<?php JHtml::_('bootstrap.addTab', 'myTab', 'menu4', JText::_('COM_DIGITAL_SEGMENT_MONTHLY_SHARE_FORM_DESC_DIGITALSEGMENTMONTHLYSHARE_INDIAN_TOP_5', true)); ?>
	<?php JHtml::_('bootstrap.endTab'); ?>
	<?php JHtml::_('bootstrap.addTab', 'myTab', 'menu5', JText::_('COM_DIGITAL_SEGMENT_MONTHLY_SHARE_FORM_DESC_DIGITALSEGMENTMONTHLYSHARE_ENGLISH_TOP_5', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
				<legend><?php echo JText::_('COM_DIGITAL_SEGMENT_MONTHLY_SHARE_FIELDSET_SEGMENTMONTHLYSHARE'); ?></legend>
				<?php echo $this->form->renderField('title'); ?>
				<?php echo $this->form->renderField('year'); ?>
			<?php
				foreach((array)$this->item->year as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="year" name="jform[yearhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php echo $this->form->renderField('month'); ?>
			<?php
				foreach((array)$this->item->month as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="month" name="jform[monthhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php echo $this->form->renderField('unique_users'); ?>
				<?php echo $this->form->renderField('page_views'); ?>
				<?php echo $this->form->renderField('fb_page_reach'); ?>
				<?php echo $this->form->renderField('social_media_followers'); ?>
				
				<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
				<?php endif; ?>
			
			</fieldset>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

		<div id="menu2" class="tab-pane fade">
		<fieldset class="adminform">
		<?php echo $this->form->renderField('malay_total_unique_users'); ?>
		<?php echo $this->form->renderField('malay_top_1_channel'); ?>
			<?php
				foreach((array)$this->item->malay_top_1_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="malay_top_1_channel" name="jform[malay_top_1_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php echo $this->form->renderField('malay_top_1_unique_users'); ?>
				<?php echo $this->form->renderField('malay_top_2_channel'); ?>
			<?php
				foreach((array)$this->item->malay_top_2_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="malay_top_2_channel" name="jform[malay_top_2_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php echo $this->form->renderField('malay_top_2_unique_users'); ?>
				<?php echo $this->form->renderField('malay_top_3_channel'); ?>
			<?php
				foreach((array)$this->item->malay_top_3_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="malay_top_3_channel" name="jform[malay_top_3_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php echo $this->form->renderField('malay_top_3_unique_users'); ?>
				<?php echo $this->form->renderField('malay_top_4_channel'); ?>
			<?php
				foreach((array)$this->item->malay_top_4_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="malay_top_4_channel" name="jform[malay_top_4_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php echo $this->form->renderField('malay_top_4_unique_users'); ?>
				<?php echo $this->form->renderField('malay_top_5_channel'); ?>
			<?php
				foreach((array)$this->item->malay_top_5_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="malay_top_5_channel" name="jform[malay_top_5_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php echo $this->form->renderField('malay_top_5_unique_users'); ?>
				</fieldset>
		</div>
		<div id="menu3" class="tab-pane fade">
		<fieldset class="adminform">
		<?php echo $this->form->renderField('chinese_total_unique_users'); ?>
		<?php echo $this->form->renderField('chinese_top_1_channel'); ?>
		<?php
				foreach((array)$this->item->chinese_top_1_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="chinese_top_1_channel" name="jform[chinese_top_1_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php echo $this->form->renderField('chinese_top_1_unique_users'); ?>
				
		<?php echo $this->form->renderField('chinese_top_2_channel'); ?>
		<?php
						foreach((array)$this->item->chinese_top_2_channel as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="chinese_top_2_channel" name="jform[chinese_top_2_channelhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
		<?php echo $this->form->renderField('chinese_top_2_unique_users'); ?>
		<?php echo $this->form->renderField('chinese_top_3_channel'); ?>
		<?php
						foreach((array)$this->item->chinese_top_3_channel as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="chinese_top_3_channel" name="jform[chinese_top_3_channelhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
		<?php echo $this->form->renderField('chinese_top_3_unique_users'); ?>
		<?php echo $this->form->renderField('chinese_top_4_channel'); ?>
		<?php
						foreach((array)$this->item->chinese_top_4_channel as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="chinese_top_4_channel" name="jform[chinese_top_4_channelhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
		<?php echo $this->form->renderField('chinese_top_4_unique_users'); ?>
		<?php echo $this->form->renderField('chinese_top_5_channel'); ?>
		<?php
						foreach((array)$this->item->chinese_top_5_channel as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="chinese_top_5_channel" name="jform[chinese_top_5_channelhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
		<?php echo $this->form->renderField('chinese_top_5_unique_users'); ?>
		</fieldset>
		</div>
		<div id="menu4" class="tab-pane fade">
		<fieldset class="adminform">
		<?php echo $this->form->renderField('indian_total_unique_users'); ?>

		<?php echo $this->form->renderField('indian_top_1_channel'); ?>
		<?php
						foreach((array)$this->item->indian_top_1_channel as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="indian_top_1_channel" name="jform[indian_top_1_channelhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
		<?php echo $this->form->renderField('indian_top_1_unique_users'); ?>
		<?php echo $this->form->renderField('indian_top_2_channel'); ?>
		<?php
						foreach((array)$this->item->indian_top_2_channel as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="indian_top_2_channel" name="jform[indian_top_2_channelhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
		<?php echo $this->form->renderField('indian_top_2_unique_users'); ?>
		<?php echo $this->form->renderField('indian_top_3_channel'); ?>
		<?php
						foreach((array)$this->item->indian_top_3_channel as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="indian_top_3_channel" name="jform[indian_top_3_channelhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
		<?php echo $this->form->renderField('indian_top_3_unique_users'); ?>
		<?php echo $this->form->renderField('indian_top_4_channel'); ?>
		<?php
						foreach((array)$this->item->indian_top_4_channel as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="indian_top_4_channel" name="jform[indian_top_4_channelhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
		<?php echo $this->form->renderField('indian_top_4_unique_users'); ?>
		<?php echo $this->form->renderField('indian_top_5_channel'); ?>
		<?php
						foreach((array)$this->item->indian_top_5_channel as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="indian_top_5_channel" name="jform[indian_top_5_channelhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
		<?php echo $this->form->renderField('indian_top_5_unique_users'); ?>
		</fieldset>
		</div>
		<div id="menu5" class="tab-pane fade">
		<fieldset class="adminform">
		<?php echo $this->form->renderField('english_total_unique_users'); ?>
		<?php echo $this->form->renderField('english_top_1_channel'); ?>
<?php
				foreach((array)$this->item->english_top_1_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="english_top_1_channel" name="jform[english_top_1_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
		<?php echo $this->form->renderField('english_top_1_unique_users'); ?>
<?php echo $this->form->renderField('english_top_2_channel'); ?>
<?php
				foreach((array)$this->item->english_top_2_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="english_top_2_channel" name="jform[english_top_2_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
<?php echo $this->form->renderField('english_top_2_unique_users'); ?>
<?php echo $this->form->renderField('english_top_3_channel'); ?>
<?php
				foreach((array)$this->item->english_top_3_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="english_top_3_channel" name="jform[english_top_3_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
<?php echo $this->form->renderField('english_top_3_unique_users'); ?>
<?php echo $this->form->renderField('english_top_4_channel'); ?>
<?php
				foreach((array)$this->item->english_top_4_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="english_top_4_channel" name="jform[english_top_4_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
<?php echo $this->form->renderField('english_top_4_unique_users'); ?>
<?php echo $this->form->renderField('english_top_5_channel'); ?>
<?php
				foreach((array)$this->item->english_top_5_channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="english_top_5_channel" name="jform[english_top_5_channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
<?php echo $this->form->renderField('english_top_5_unique_users'); ?>

		</fieldset>
		</div>

	
	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>

</form>
