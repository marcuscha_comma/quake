CREATE TABLE IF NOT EXISTS `#__cus_quake_club_download_centre_reports` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT 1,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00",
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`credentials_by_month` LONGTEXT NOT NULL ,
`credentials_by_week` LONGTEXT NOT NULL ,
`brand_profiles_by_month` LONGTEXT NOT NULL ,
`brand_profiles_by_week` LONGTEXT NOT NULL ,
`rate_cards_by_month` LONGTEXT NOT NULL ,
`rate_cards_by_week` LONGTEXT NOT NULL ,
`programme_schedules_by_month` LONGBLOB NOT NULL ,
`programme_schedules_by_week` LONGBLOB NOT NULL ,
`packages_by_month` LONGTEXT NOT NULL ,
`packages_by_week` LONGTEXT NOT NULL ,
`year` VARCHAR(255)  NOT NULL ,
`month` VARCHAR(255)  NOT NULL ,
`week` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'Quake club download centre report','com_quake_club_download_centre_reports.quakeclubdownloadcentrereport','{"special":{"dbtable":"#__cus_quake_club_download_centre_reports","key":"id","type":"Quakeclubdownloadcentrereport","prefix":"Quake_club_download_centre_reportsTable"}}', '{"formFile":"administrator\/components\/com_quake_club_download_centre_reports\/models\/forms\/quakeclubdownloadcentrereport.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_quake_club_download_centre_reports.quakeclubdownloadcentrereport')
) LIMIT 1;
