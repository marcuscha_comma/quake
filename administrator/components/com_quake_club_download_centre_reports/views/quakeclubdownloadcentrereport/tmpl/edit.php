<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_download_centre_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_quake_club_download_centre_reports/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'quakeclubdownloadcentrereport.cancel') {
			Joomla.submitform(task, document.getElementById('quakeclubdownloadcentrereport-form'));
		}
		else {
			
			if (task != 'quakeclubdownloadcentrereport.cancel' && document.formvalidator.isValid(document.id('quakeclubdownloadcentrereport-form'))) {
				
				Joomla.submitform(task, document.getElementById('quakeclubdownloadcentrereport-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_quake_club_download_centre_reports&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="quakeclubdownloadcentrereport-form" class="form-validate form-horizontal">

	
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
	<?php echo $this->form->renderField('created_by'); ?>
	<?php echo $this->form->renderField('modified_by'); ?>
	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'quakeclubdownloadcentrereport')); ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'quakeclubdownloadcentrereport', JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_TAB_QUAKECLUBDOWNLOADCENTREREPORT', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
				<legend><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FIELDSET_QUAKECLUBDOWNLOADCENTREREPORT'); ?></legend>
				<?php echo $this->form->renderField('credentials_by_month'); ?>
				<?php echo $this->form->renderField('credentials_by_week'); ?>
				<?php echo $this->form->renderField('brand_profiles_by_month'); ?>
				<?php echo $this->form->renderField('brand_profiles_by_week'); ?>
				<?php echo $this->form->renderField('rate_cards_by_month'); ?>
				<?php echo $this->form->renderField('rate_cards_by_week'); ?>
				<?php echo $this->form->renderField('programme_schedules_by_month'); ?>
				<?php echo $this->form->renderField('programme_schedules_by_week'); ?>
				<?php echo $this->form->renderField('packages_by_month'); ?>
				<?php echo $this->form->renderField('packages_by_week'); ?>
				<?php echo $this->form->renderField('year'); ?>
				<?php echo $this->form->renderField('month'); ?>
				<?php echo $this->form->renderField('week'); ?>
				<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
				<?php endif; ?>
			</fieldset>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	
	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>

</form>
