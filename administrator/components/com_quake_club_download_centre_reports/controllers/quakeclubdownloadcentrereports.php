<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_download_centre_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');
jimport('phpspreadsheet.phpspreadsheet');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Session\session;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

/**
 * Quakeclubdownloadcentrereports list controller class.
 *
 * @since  1.6
 */
class Quake_club_download_centre_reportsControllerQuakeclubdownloadcentrereports extends \Joomla\CMS\MVC\Controller\AdminController
{
	/**
	 * Method to clone existing Quakeclubdownloadcentrereports
	 *
	 * @return void
     *
     * @throws Exception
	 */
	public function duplicate()
	{
		// Check for request forgeries
		session::checkToken() or jexit(Text::_('JINVALID_TOKEN'));

		// Get id(s)
		$pks = $this->input->post->get('cid', array(), 'array');

		try
		{
			if (empty($pks))
			{
				throw new Exception(Text::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_NO_ELEMENT_SELECTED'));
			}

			ArrayHelper::toInteger($pks);
			$model = $this->getModel();
			$model->duplicate($pks);
			$this->setMessage(Text::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_ITEMS_SUCCESS_DUPLICATED'));
		}
		catch (Exception $e)
		{
			Factory::getApplication()->enqueueMessage($e->getMessage(), 'warning');
		}

		$this->setRedirect('index.php?option=com_quake_club_download_centre_reports&view=quakeclubdownloadcentrereports');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    Optional. Model name
	 * @param   string  $prefix  Optional. Class prefix
	 * @param   array   $config  Optional. Configuration array for model
	 *
	 * @return  object	The Model
	 *
	 * @since    1.6
	 */
	public function getModel($name = 'quakeclubdownloadcentrereport', $prefix = 'Quake_club_download_centre_reportsModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 * @since   3.0
     *
     * @throws Exception
     */
	public function saveOrderAjax()
	{
		// Get the input
		$input = Factory::getApplication()->input;
		$pks   = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		ArrayHelper::toInteger($pks);
		ArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Close the application
		Factory::getApplication()->close();
	}

	public function exportCsv()
	{
		$pks = $this->input->post->get('cid', array(), 'array');

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

			$db = JFactory::getDBO();
			$db->setQuery('SELECT * FROM #__cus_quake_club_download_centre_reports where id IN('.implode(",",$pks).')');
			$result = $db->loadAssocList();
			
			$styleArrayWeekDate = array(
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => '404040' ),
				],
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => 'FFFFFF' ),
					'size' => 16
				));

			$styleArrayWeekDateValue = array(
				'fill' => array(
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => 'D9D9D9' )
				),
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_LEFT
				],
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => '000000' ),
					'size' => 16
			));

			$styleArrayTitle = array(
				'fill' => array(
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => '000000' )
				),
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => 'EC008C' ),
					'size' => 14
			));

			$styleArraySubTitle = array(
				'fill' => array(
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => '404040' )
				),
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => 'FFFFFF' ),
					'size' => 11
			));

			$styleArrayValue = array(
				'fill' => array(
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => 'D9D9D9' )
				),
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_LEFT
				],
				'font'  => array(
					'color' => array('rgb' => '000000' ),
					'size' => 11
			));

			$objWorkSheet = $spreadsheet->createSheet(0);
			$objWorkSheet->setTitle("Credentials Report");

			$count = 1;
			foreach ($result as $key => $value) {
				$currentWeekNumber = $value['week'];
				$dto = new DateTime();
				$dto->setISODate($value['year'], $currentWeekNumber);
				$ret['week_start'] = $dto->format('Y-m-d');
				$dto->modify('+6 days');
				$ret['week_end'] = $dto->format('Y-m-d');

				$objWorkSheet->setCellValue('A'.$count, 'Week');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayWeekDate);

				$objWorkSheet->setCellValue('B'.$count, $currentWeekNumber);
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayWeekDateValue);

				$count++;

				$objWorkSheet->setCellValue('A'.$count, 'Date');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayWeekDate);

				$objWorkSheet->setCellValue('B'.$count, $ret['week_start']. ' ~ '. $ret['week_end']);
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayWeekDateValue);
				$count = $count+2;

				$objWorkSheet->setCellValue('A'.$count, 'Credentials by month');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayTitle);
				$count ++;

				$objWorkSheet->setCellValue('A'.$count, 'Total');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('B'.$count, 'Title');
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('C'.$count, 'Attach file name');
				$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArraySubTitle);
				$count ++;

				$tmp = json_decode($value['credentials_by_month']);
				for ($i=0; $i < count($tmp); $i++) {
					$objWorkSheet->setCellValue('A'.$count, $tmp[$i]->total);
					$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('B'.$count, $tmp[$i]->title);
					$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('C'.$count, $tmp[$i]->attach_file);
					$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArrayValue);
					$count++;
				}
				$count = $count+2;

				$objWorkSheet->setCellValue('A'.$count, 'Credentials by week');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayTitle);
				$count ++;
				$objWorkSheet->setCellValue('A'.$count, 'Total');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('B'.$count, 'Title');
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('C'.$count, 'Attach file name');
				$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArraySubTitle);
				$count ++;

				$tmp = json_decode($value['credentials_by_week']);
				for ($i=0; $i < count($tmp); $i++) {
					$objWorkSheet->setCellValue('A'.$count, $tmp[$i]->total);
					$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('B'.$count, $tmp[$i]->title);
					$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('C'.$count, $tmp[$i]->attach_file);
					$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArrayValue);
					$count++;
				}
				$count = $count+2;

			}

			$objWorkSheet = $spreadsheet->createSheet(1);
			$objWorkSheet->setTitle("Brand profiles Report");

			$count = 1;
			foreach ($result as $key => $value) {
				$currentWeekNumber = $value['week'];
				$dto = new DateTime();
				$dto->setISODate($value['year'], $currentWeekNumber);
				$ret['week_start'] = $dto->format('Y-m-d');
				$dto->modify('+6 days');
				$ret['week_end'] = $dto->format('Y-m-d');

				$objWorkSheet->setCellValue('A'.$count, 'Week');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayWeekDate);
				$objWorkSheet->setCellValue('B'.$count, $currentWeekNumber);
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayWeekDateValue);
				$count++;

				$objWorkSheet->setCellValue('A'.$count, 'Date');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayWeekDate);
				$objWorkSheet->setCellValue('B'.$count, $ret['week_start']. ' ~ '. $ret['week_end']);
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayWeekDateValue);
				$count = $count+2;

				$objWorkSheet->setCellValue('A'.$count, 'Brand profiles by month');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayTitle);
				$count ++;
				$objWorkSheet->setCellValue('A'.$count, 'Total');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('B'.$count, 'Title');
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('C'.$count, 'Attach file name');
				$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArraySubTitle);
				$count ++;

				$tmp = json_decode($value['brand_profiles_by_month']);
				for ($i=0; $i < count($tmp); $i++) {
					$objWorkSheet->setCellValue('A'.$count, $tmp[$i]->total);
					$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('B'.$count, $tmp[$i]->title);
					$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('C'.$count, $tmp[$i]->attach_file);
					$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArrayValue);
					$count++;
				}
				$count = $count+2;

				$objWorkSheet->setCellValue('A'.$count, 'Brand profiles by week');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayTitle);
				$count ++;
				$objWorkSheet->setCellValue('A'.$count, 'Total');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('B'.$count, 'Title');
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('C'.$count, 'Attach file name');
				$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArraySubTitle);
				$count ++;

				$tmp = json_decode($value['brand_profiles_by_week']);
				for ($i=0; $i < count($tmp); $i++) {
					$objWorkSheet->setCellValue('A'.$count, $tmp[$i]->total);
					$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('B'.$count, $tmp[$i]->title);
					$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('C'.$count, $tmp[$i]->attach_file);
					$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArrayValue);
					$count++;
				}
				$count = $count+2;
			}

			$objWorkSheet = $spreadsheet->createSheet(2);
			$objWorkSheet->setTitle("Rate Cards Report");

			$count = 1;
			foreach ($result as $key => $value) {
				$currentWeekNumber = $value['week'];
				$dto = new DateTime();
				$dto->setISODate($value['year'], $currentWeekNumber);
				$ret['week_start'] = $dto->format('Y-m-d');
				$dto->modify('+6 days');
				$ret['week_end'] = $dto->format('Y-m-d');

				$objWorkSheet->setCellValue('A'.$count, 'Week');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayWeekDate);
				$objWorkSheet->setCellValue('B'.$count, $currentWeekNumber);
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayWeekDateValue);
				$count++;

				$objWorkSheet->setCellValue('A'.$count, 'Date');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayWeekDate);
				$objWorkSheet->setCellValue('B'.$count, $ret['week_start']. ' ~ '. $ret['week_end']);
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayWeekDateValue);
				$count = $count+2;

				$objWorkSheet->setCellValue('A'.$count, 'Rate cards by month');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayTitle);
				$count ++;
				$objWorkSheet->setCellValue('A'.$count, 'Total');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('B'.$count, 'Title');
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('C'.$count, 'Attach file name');
				$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArraySubTitle);
				$count ++;

				$tmp = json_decode($value['rate_cards_by_month']);
				for ($i=0; $i < count($tmp); $i++) {
					$objWorkSheet->setCellValue('A'.$count, $tmp[$i]->total);
					$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('B'.$count, $tmp[$i]->title);
					$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('C'.$count, $tmp[$i]->attach_file);
					$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArrayValue);
					$count++;
				}
				$count = $count+2;

				$objWorkSheet->setCellValue('A'.$count, 'Rate cards by week');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayTitle);
				$count ++;
				$objWorkSheet->setCellValue('A'.$count, 'Total');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('B'.$count, 'Title');
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('C'.$count, 'Attach file name');
				$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArraySubTitle);
				$count ++;

				$tmp = json_decode($value['rate_cards_by_week']);
				for ($i=0; $i < count($tmp); $i++) {
					$objWorkSheet->setCellValue('A'.$count, $tmp[$i]->total);
					$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('B'.$count, $tmp[$i]->title);
					$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('C'.$count, $tmp[$i]->attach_file);
					$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArrayValue);
					$count++;
				}
				$count = $count+2;
			}

			$objWorkSheet = $spreadsheet->createSheet(3);
			$objWorkSheet->setTitle("Programme Schedules Report");

			$count = 1;
			foreach ($result as $key => $value) {
				$currentWeekNumber = $value['week'];
				$dto = new DateTime();
				$dto->setISODate($value['year'], $currentWeekNumber);
				$ret['week_start'] = $dto->format('Y-m-d');
				$dto->modify('+6 days');
				$ret['week_end'] = $dto->format('Y-m-d');

				$objWorkSheet->setCellValue('A'.$count, 'Week');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayWeekDate);
				$objWorkSheet->setCellValue('B'.$count, $currentWeekNumber);
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayWeekDateValue);
				$count++;

				$objWorkSheet->setCellValue('A'.$count, 'Date');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayWeekDate);
				$objWorkSheet->setCellValue('B'.$count, $ret['week_start']. ' ~ '. $ret['week_end']);
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayWeekDateValue);
				$count = $count+2;

				$objWorkSheet->setCellValue('A'.$count, 'Programme schedules by month');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayTitle);
				$count ++;
				$objWorkSheet->setCellValue('A'.$count, 'Total');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('B'.$count, 'Title');
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('C'.$count, 'Segment');
				$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('D'.$count, 'Channel');
				$objWorkSheet->getStyle('D'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('E'.$count, 'Attach file name');
				$objWorkSheet->getStyle('E'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('F'.$count, 'Year');
				$objWorkSheet->getStyle('F'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('G'.$count, 'Month');
				$objWorkSheet->getStyle('G'.$count)->applyFromArray($styleArraySubTitle);
				$count ++;

				$tmp = json_decode($value['programme_schedules_by_month']);
				for ($i=0; $i < count($tmp); $i++) {
					$objWorkSheet->setCellValue('A'.$count, $tmp[$i]->total);
					$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('B'.$count, $tmp[$i]->file_title);
					$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('C'.$count, $tmp[$i]->segment);
					$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('D'.$count, $tmp[$i]->channel);
					$objWorkSheet->getStyle('D'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('E'.$count, $tmp[$i]->attach_file);
					$objWorkSheet->getStyle('E'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('F'.$count, $tmp[$i]->year);
					$objWorkSheet->getStyle('F'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('G'.$count, $tmp[$i]->month);
					$objWorkSheet->getStyle('G'.$count)->applyFromArray($styleArrayValue);
					$count++;
				}
				$count = $count+2;

				$objWorkSheet->setCellValue('A'.$count, 'Programme schedules by week');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayTitle);
				$count ++;
				$objWorkSheet->setCellValue('A'.$count, 'Total');
				$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('B'.$count, 'Title');
				$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('C'.$count, 'Segment');
				$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('D'.$count, 'Channel');
				$objWorkSheet->getStyle('D'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('E'.$count, 'Attach file name');
				$objWorkSheet->getStyle('E'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('F'.$count, 'Year');
				$objWorkSheet->getStyle('F'.$count)->applyFromArray($styleArraySubTitle);
				$objWorkSheet->setCellValue('G'.$count, 'Month');
				$objWorkSheet->getStyle('G'.$count)->applyFromArray($styleArraySubTitle);
				$count ++;

				$tmp = json_decode($value['programme_schedules_by_week']);
				for ($i=0; $i < count($tmp); $i++) {
					$objWorkSheet->setCellValue('A'.$count, $tmp[$i]->total);
					$objWorkSheet->getStyle('A'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('B'.$count, $tmp[$i]->file_title);
					$objWorkSheet->getStyle('B'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('C'.$count, $tmp[$i]->segment);
					$objWorkSheet->getStyle('C'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('D'.$count, $tmp[$i]->channel);
					$objWorkSheet->getStyle('D'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('E'.$count, $tmp[$i]->attach_file);
					$objWorkSheet->getStyle('E'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('F'.$count, $tmp[$i]->year);
					$objWorkSheet->getStyle('F'.$count)->applyFromArray($styleArrayValue);
					$objWorkSheet->setCellValue('G'.$count, $tmp[$i]->month);
					$objWorkSheet->getStyle('G'.$count)->applyFromArray($styleArrayValue);
					$count++;
				}
				$count = $count+2;
			}
			
		$writer = new Xlsx($spreadsheet);
			
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="weekly report.xlsx"');
		$writer->save("php://output");
		die();
	}
}
