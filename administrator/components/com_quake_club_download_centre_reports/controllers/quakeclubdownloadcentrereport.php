<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_download_centre_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Quakeclubdownloadcentrereport controller class.
 *
 * @since  1.6
 */
class Quake_club_download_centre_reportsControllerQuakeclubdownloadcentrereport extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'quakeclubdownloadcentrereports';
		parent::__construct();
	}
}
