<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Casestudiespage
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_casestudiespage'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Casestudiespage', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('CasestudiespageHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'casestudiespage.php');

$controller = JControllerLegacy::getInstance('Casestudiespage');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
