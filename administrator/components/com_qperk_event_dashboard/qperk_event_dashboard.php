<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_event_dashboard
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_qperk_event_dashboard'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Qperk_event_dashboard', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Qperk_event_dashboardHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'qperk_event_dashboard.php');

$controller = BaseController::getInstance('Qperk_event_dashboard');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
