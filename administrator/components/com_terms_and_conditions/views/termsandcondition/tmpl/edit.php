<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Terms_and_conditions
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_terms_and_conditions/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'termsandcondition.cancel') {
			Joomla.submitform(task, document.getElementById('termsandcondition-form'));
		}
		else {
			
			if (task != 'termsandcondition.cancel' && document.formvalidator.isValid(document.id('termsandcondition-form'))) {
				
				Joomla.submitform(task, document.getElementById('termsandcondition-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_terms_and_conditions&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="termsandcondition-form" class="form-validate form-horizontal">

	
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
	<?php echo $this->form->renderField('created_by'); ?>
	<?php echo $this->form->renderField('modified_by'); ?>
	<legend><?php echo JText::_('COM_TERMS_AND_CONDITIONS_FIELDSET_PRIVACYPOLICY'); ?></legend>

	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'privacypolicy')); ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'privacypolicy', JText::_('COM_TERMS_AND_CONDITIONS_TAB_PRIVACYPOLICY', true)); ?>
	<?php JHtml::_('bootstrap.endTab'); ?>
	<?php JHtml::_('bootstrap.addTab', 'myTab', 'menu2', JText::_('COM_TERMS_AND_CONDITIONS_TAB_RADIO_PRIVACYPOLICY', true)); ?>
	<?php JHtml::_('bootstrap.endTab'); ?>
	<?php JHtml::_('bootstrap.addTab', 'myTab', 'menu3', JText::_('COM_TERMS_AND_CONDITIONS_TAB_DIGITAL_PRIVACYPOLICY', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
				<!-- <?php echo $this->form->renderField('title'); ?> -->
				<?php echo $this->form->renderField('tv_content'); ?>
				<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
				<?php endif; ?>
			</fieldset>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
	<div id="menu2" class="tab-pane fade">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
			<?php echo $this->form->renderField('radio_content'); ?>
			</fieldset>
		</div>
	</div>

	<div id="menu3" class="tab-pane fade">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
			<?php echo $this->form->renderField('digital_content'); ?>
			</fieldset>
		</div>
	</div>

	
	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>

</form>
