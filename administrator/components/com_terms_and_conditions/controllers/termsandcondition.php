<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Terms_and_conditions
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Termsandcondition controller class.
 *
 * @since  1.6
 */
class Terms_and_conditionsControllerTermsandcondition extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'termsandconditions';
		parent::__construct();
	}
}
