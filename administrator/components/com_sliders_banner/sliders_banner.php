<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Sliders_banner
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_sliders_banner'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Sliders_banner', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Sliders_bannerHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'sliders_banner.php');

$controller = JControllerLegacy::getInstance('Sliders_banner');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
