<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Package_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Packagefile controller class.
 *
 * @since  1.6
 */
class Package_filesControllerPackagefile extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'packagefiles';
		parent::__construct();
	}
}
