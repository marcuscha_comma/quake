<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_data
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Rsvpdatum controller class.
 *
 * @since  1.6
 */
class Rsvp_dataControllerRsvpdatum extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'rsvpdata';
		parent::__construct();
	}
}
