CREATE TABLE IF NOT EXISTS `#__cus_qperk_event_questions` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`question` TEXT NOT NULL ,
`answer_1` VARCHAR(255)  NOT NULL ,
`answer_2` VARCHAR(255)  NOT NULL ,
`correct_answer` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

