<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Programme_scheduler
 * @author     ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Programmeschedulerfile controller class.
 *
 * @since  1.6
 */
class Programme_schedulerControllerProgrammeschedulerfile extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'programmeschedulerfiles';
		parent::__construct();
	}
}
