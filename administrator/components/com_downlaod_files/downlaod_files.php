<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Downlaod_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_downlaod_files'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Downlaod_files', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Downlaod_filesHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'downlaod_files.php');

$controller = JControllerLegacy::getInstance('Downlaod_files');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
