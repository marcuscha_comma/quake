<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Downlaod_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Dowloadfile controller class.
 *
 * @since  1.6
 */
class Downlaod_filesControllerDowloadfile extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'dowloadfiles';
		parent::__construct();
	}
}
