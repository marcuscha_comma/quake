<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_event_products
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

use \Joomla\CMS\Language\Text;

/**
 * View class for a list of Quake_club_event_products.
 *
 * @since  1.6
 */
class Quake_club_event_productsViewQperkseventproducts extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		Quake_club_event_productsHelper::addSubmenu('qperkseventproducts');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = Quake_club_event_productsHelper::getActions();

		JToolBarHelper::title(Text::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_TITLE_QPERKSEVENTPRODUCTS'), 'qperkseventproducts.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/qperkseventproduct';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('qperkseventproduct.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('qperkseventproducts.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('qperkseventproduct.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('qperkseventproducts.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('qperkseventproducts.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'qperkseventproducts.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('qperkseventproducts.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('qperkseventproducts.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'qperkseventproducts.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('qperkseventproducts.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_quake_club_event_products');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_quake_club_event_products&view=qperkseventproducts');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
			'a.`name`' => JText::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_QPERKSEVENTPRODUCTS_NAME'),
			'a.`point`' => JText::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_QPERKSEVENTPRODUCTS_POINT'),
			'a.`promo`' => JText::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_QPERKSEVENTPRODUCTS_PROMO'),
			'a.`promo_qperks`' => JText::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_QPERKSEVENTPRODUCTS_PROMO_QPERKS'),
			'a.`quantity`' => JText::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_QPERKSEVENTPRODUCTS_QUANTITY'),
			'a.`category`' => JText::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_QPERKSEVENTPRODUCTS_CATEGORY'),
			'a.`max_redemption`' => JText::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_QPERKSEVENTPRODUCTS_MAX_REDEMPTION'),
			'a.`highlight`' => JText::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_QPERKSEVENTPRODUCTS_HIGHLIGHT'),
			'a.`promo_label`' => JText::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_QPERKSEVENTPRODUCTS_PROMO_LABEL'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
