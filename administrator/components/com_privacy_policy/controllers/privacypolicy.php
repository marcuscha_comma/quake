<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Privacy_policy
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Privacypolicy controller class.
 *
 * @since  1.6
 */
class Privacy_policyControllerPrivacypolicy extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'privacypolicys';
		parent::__construct();
	}
}
