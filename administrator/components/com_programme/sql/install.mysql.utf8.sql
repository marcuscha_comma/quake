CREATE TABLE IF NOT EXISTS `#__cus_programme` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`title` VARCHAR(255)  NOT NULL ,
`description` TEXT NOT NULL ,
`segment` TEXT NOT NULL ,
`channel` TEXT NOT NULL ,
`image` VARCHAR(255)  NOT NULL ,
`content` TEXT NOT NULL ,
`create_date` DATE NOT NULL ,
`url` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

