<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Programme
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_programme'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Programme', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('ProgrammeHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'programme.php');

$controller = JControllerLegacy::getInstance('Programme');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
