<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Programme
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Programme controller class.
 *
 * @since  1.6
 */
class ProgrammeControllerProgramme extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'programmes';
		parent::__construct();
	}
}
