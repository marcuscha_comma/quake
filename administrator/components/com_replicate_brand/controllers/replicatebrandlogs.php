<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Replicate_brand
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');
jimport('phpspreadsheet.phpspreadsheet');

use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Session\session;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

/**
 * Replicatebrandlogs list controller class.
 *
 * @since  1.6
 */
class Replicate_brandControllerReplicatebrandlogs extends \Joomla\CMS\MVC\Controller\AdminController
{
	/**
	 * Method to clone existing Replicatebrandlogs
	 *
	 * @return void
     *
     * @throws Exception
	 */
	public function duplicate()
	{
		// Check for request forgeries
		session::checkToken() or jexit(Text::_('JINVALID_TOKEN'));

		// Get id(s)
		$pks = $this->input->post->get('cid', array(), 'array');
		
		try
		{
			if (empty($pks))
			{
				throw new Exception(Text::_('COM_REPLICATE_BRAND_NO_ELEMENT_SELECTED'));
			}

			ArrayHelper::toInteger($pks);
			$model = $this->getModel();
			$model->duplicate($pks);
			$this->setMessage(Text::_('COM_REPLICATE_BRAND_ITEMS_SUCCESS_DUPLICATED'));
		}
		catch (Exception $e)
		{
			Factory::getApplication()->enqueueMessage($e->getMessage(), 'warning');
		}

		$this->setRedirect('index.php?option=com_replicate_brand&view=replicatebrandlogs');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    Optional. Model name
	 * @param   string  $prefix  Optional. Class prefix
	 * @param   array   $config  Optional. Configuration array for model
	 *
	 * @return  object	The Model
	 *
	 * @since    1.6
	 */
	public function getModel($name = 'replicatebrandlog', $prefix = 'Replicate_brandModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 * @since   3.0
     *
     * @throws Exception
     */
	public function saveOrderAjax()
	{
		// Get the input
		$input = Factory::getApplication()->input;
		$pks   = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		ArrayHelper::toInteger($pks);
		ArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Close the application
		Factory::getApplication()->close();
	}

	public function export()
	{

		$pks = $this->input->post->get('cid', array(), 'array');
		$pks = implode(",",$pks);
		
		ob_end_clean();
		$app = JFactory::getApplication();

		

		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=Replication My Brand Report.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		$model = $this->getModel('replicatebrandlogs');
		$result = $model->getCsv($pks);
		echo "Name, Email, Mobile no, Company name, Designation, Article, Brand Name, Product Category, Objective, Date \r\n";
		
		foreach ($result as $user) {
			echo ucwords(trim($user->user_name)). ", ";
			echo $user->user_email . ", ";
			echo $user->user_contact . ", ";
			echo $user->user_company_name . ", ";
			echo $user->user_designation . ", ";
			echo $user->article_title . ", ";
			echo $user->brand_name . ", ";
			echo $user->product_category . ", ";
			echo $user->opinion . ", ";
			echo $user->created_at . "\r\n";
		}

		$app->close();
		$this->setRedirect('index.php?option=com_replicate_brand&view=replicatebrandlogs');
	}

	public function exportCsv2()
	{
		$this->download_send_headers("data_export_" . date("Y-m-d") . ".csv");
		$model = $this->getModel('replicatebrandlogs');
		$array = $model->getCsvArray();
		echo $this->array2csv($array);	
		die();
	}

	function array2csv(array &$array)
	{
		if (count($array) == 0) {
			return null;
		}
		ob_start();
		$df = fopen("php://output", 'w');
		fputcsv($df, array_keys(reset($array)));
		$tmp = [];
		foreach ($array as $row) {
			$row['mobileNo'] = '="' . $row['mobileNo'] . '"';
			$tmp = array(
				"name" => $row["user_name"],
				"email" => $row["user_email"],
				"contact" => $row["user_contact"],
				"email" => $row["user_company_name"],
				"email" => $row["user_designation"],
				"email" => $row["brand_name"],
				"email" => $row["product_category"],
				"email" => $row["opinion"],
				"email" => $row["created_at"],
			);
			fputcsv($df, $row);
		}
		fclose($df);
		return ob_get_clean();
	}

	function download_send_headers($filename) {
		// disable caching
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");
	
		// force download  
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
	
		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");
	}

	public function exportCsv()
	{
		$pks = $this->input->post->get('cid', array(), 'array');
		$pks = implode(",",$pks);
		echo "<pre>";
		print_r($pks);
		echo "</pre>";
		die();
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_replicate_brand_log where id in('.$pks.'"');
		$result = $db->loadAssocList();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$objWorkSheet = $sheet;
		$objWorkSheet->setTitle("Report");
		$objWorkSheet->setCellValue('A1', 'Date');
		$objWorkSheet->setCellValue('A2', 'Name');
		$objWorkSheet->setCellValue('A3', 'Email');
		$objWorkSheet->setCellValue('A4', 'Contact');
		$objWorkSheet->setCellValue('A5', 'Company Name');
		$objWorkSheet->setCellValue('A6', 'Designation');
		$objWorkSheet->setCellValue('A7', 'Brand Name');
		$objWorkSheet->setCellValue('A8', 'Product Category');
		$objWorkSheet->setCellValue('A9', 'Opinion');
		$objWorkSheet->setCellValue('A10', 'Article');

		echo ucwords(trim($user->user_name)). ", ";
			echo $user->user_email . ", ";
			echo $user->user_contact . ", ";
			echo $user->user_company_name . ", ";
			echo $user->user_designation . ", ";
			echo $user->brand_name . ", ";
			echo $user->product_category . ", ";
			echo $user->opinion . ", ";
			echo $user->created_at . "\r\n";
		foreach ($result as $key => $value) {
			$objWorkSheet->setCellValue('B1', $result[0]['week']);
		}
		$writer = new Xlsx($spreadsheet);
			
		// $writer->save('../uploads/'.$value.'.xlsx');
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="weekly report.xlsx"');
		$writer->save("php://output");
		die();

		// $db = JFactory::getDBO();
		// $db->setQuery('SELECT * FROM #__cus_quake_club_reports where id ='.$pks);
		// $result = $db->loadAssocList();

		// $spreadsheet = new Spreadsheet();
		// $styleArray = array(
		// 	'font'  => array(
		// 		'bold'  => true,
		// 	));
		// $sheet = $spreadsheet->getActiveSheet();
		

		// $writer = new Xlsx($spreadsheet);
		// $writer->save('../uploads/hello_world.xlsx');
		// die();
	}
}
