<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Blank_page
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_blank_page'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Blank_page', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Blank_pageHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'blank_page.php');

$controller = JControllerLegacy::getInstance('Blank_page');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
