<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Privacy_notice
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_privacy_notice'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Privacy_notice', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Privacy_noticeHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'privacy_notice.php');

$controller = JControllerLegacy::getInstance('Privacy_notice');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
