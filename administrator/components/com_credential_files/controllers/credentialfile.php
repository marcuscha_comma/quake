<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Credential_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Credentialfile controller class.
 *
 * @since  1.6
 */
class Credential_filesControllerCredentialfile extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'credentialfiles';
		parent::__construct();
	}
}
