<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_buka_lembaran_baharu
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Access\Access;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Table\Table as Table;

/**
 * rsvptourdatum Table class
 *
 * @since  1.6
 */
class Rsvp_buka_lembaran_baharuTablersvptourdatum extends Table
{
	
	/**
	 * Constructor
	 *
	 * @param   JDatabase  &$db  A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__cus_rsvp_tour_data', 'id', $db);
		
		JTableObserverTags::createObserver($this, array('typeAlias' => 'com_rsvp_buka_lembaran_baharu.rsvptourdatum'));
		JTableObserverContenthistory::createObserver($this, array('typeAlias' => 'com_rsvp_buka_lembaran_baharu.rsvptourdatum'));
		$this->setColumnAlias('published', 'state');
		
	}

	/**
	 * Overloaded bind function to pre-process the params.
	 *
	 * @param   array  $array   Named array
	 * @param   mixed  $ignore  Optional array or list of parameters to ignore
	 *
	 * @return  null|string  null is operation was satisfactory, otherwise returns an error
	 *
	 * @see     JTable:bind
	 * @since   1.5
     * @throws Exception
	 */
	public function bind($array, $ignore = '')
	{
	    $date = Factory::getDate();
		$task = Factory::getApplication()->input->get('task');
	    
		$input = JFactory::getApplication()->input;
		$task = $input->getString('task', '');

		if ($array['id'] == 0 && empty($array['created_by']))
		{
			$array['created_by'] = JFactory::getUser()->id;
		}

		if ($array['id'] == 0 && empty($array['modified_by']))
		{
			$array['modified_by'] = JFactory::getUser()->id;
		}

		if ($task == 'apply' || $task == 'save')
		{
			$array['modified_by'] = JFactory::getUser()->id;
		}

		// Support for checkbox field: redemption_status
		if (!isset($array['redemption_status']))
		{
			$array['redemption_status'] = 0;
		}

		// Support for checkbox field: is_attended
		if (!isset($array['is_attended']))
		{
			$array['is_attended'] = 0;
		}

		// Support for checkbox field: is_valid
		if (!isset($array['is_valid']))
		{
			$array['is_valid'] = 0;
		}

		// Support for checkbox field: is_deleted
		if (!isset($array['is_deleted']))
		{
			$array['is_deleted'] = 0;
		}

		if ($task == 'apply' || $task == 'save')
		{
			$array['updated_at'] = $date->toSql();
		}

		// Support for multiple field: source
		if (isset($array['source']))
		{
			if (is_array($array['source']))
			{
				$array['source'] = implode(',',$array['source']);
			}
			elseif (strpos($array['source'], ',') != false)
			{
				$array['source'] = explode(',',$array['source']);
			}
			elseif (strlen($array['source']) == 0)
			{
				$array['source'] = '';
			}
		}
		else
		{
			$array['source'] = '';
		}

		// Support for checkbox field: joingamestatus
		if (!isset($array['joingamestatus']))
		{
			$array['joingamestatus'] = 0;
		}

		if (isset($array['params']) && is_array($array['params']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['params']);
			$array['params'] = (string) $registry;
		}

		if (isset($array['metadata']) && is_array($array['metadata']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['metadata']);
			$array['metadata'] = (string) $registry;
		}

		if (!Factory::getUser()->authorise('core.admin', 'com_rsvp_buka_lembaran_baharu.rsvptourdatum.' . $array['id']))
		{
			$actions         = Access::getActionsFromFile(
				JPATH_ADMINISTRATOR . '/components/com_rsvp_buka_lembaran_baharu/access.xml',
				"/access/section[@name='rsvptourdatum']/"
			);
			$default_actions = Access::getAssetRules('com_rsvp_buka_lembaran_baharu.rsvptourdatum.' . $array['id'])->getData();
			$array_jaccess   = array();

			foreach ($actions as $action)
			{
                if (key_exists($action->name, $default_actions))
                {
                    $array_jaccess[$action->name] = $default_actions[$action->name];
                }
			}

			$array['rules'] = $this->JAccessRulestoArray($array_jaccess);
		}

		// Bind the rules for ACL where supported.
		if (isset($array['rules']) && is_array($array['rules']))
		{
			$this->setRules($array['rules']);
		}

		return parent::bind($array, $ignore);
	}

	/**
	 * This function convert an array of JAccessRule objects into an rules array.
	 *
	 * @param   array  $jaccessrules  An array of JAccessRule objects.
	 *
	 * @return  array
	 */
	private function JAccessRulestoArray($jaccessrules)
	{
		$rules = array();

		foreach ($jaccessrules as $action => $jaccess)
		{
			$actions = array();

			if ($jaccess)
			{
				foreach ($jaccess->getData() as $group => $allow)
				{
					$actions[$group] = ((bool)$allow);
				}
			}

			$rules[$action] = $actions;
		}

		return $rules;
	}

	/**
	 * Overloaded check function
	 *
	 * @return bool
	 */
	public function check()
	{
		// If there is an ordering column and this is a new row then get the next ordering value
		if (property_exists($this, 'ordering') && $this->id == 0)
		{
			$this->ordering = self::getNextOrder();
		}
		
		

		return parent::check();
	}

	/**
	 * Define a namespaced asset name for inclusion in the #__assets table
	 *
	 * @return string The asset name
	 *
	 * @see Table::_getAssetName
	 */
	protected function _getAssetName()
	{
		$k = $this->_tbl_key;

		return 'com_rsvp_buka_lembaran_baharu.rsvptourdatum.' . (int) $this->$k;
	}

	/**
	 * Returns the parent asset's id. If you have a tree structure, retrieve the parent's id using the external key field
	 *
	 * @param   JTable   $table  Table name
	 * @param   integer  $id     Id
	 *
	 * @see Table::_getAssetParentId
	 *
	 * @return mixed The id on success, false on failure.
	 */
	protected function _getAssetParentId(JTable $table = null, $id = null)
	{
		// We will retrieve the parent-asset from the Asset-table
		$assetParent = Table::getInstance('Asset');

		// Default: if no asset-parent can be found we take the global asset
		$assetParentId = $assetParent->getRootId();

		// The item has the component as asset-parent
		$assetParent->loadByName('com_rsvp_buka_lembaran_baharu');

		// Return the found asset-parent-id
		if ($assetParent->id)
		{
			$assetParentId = $assetParent->id;
		}

		return $assetParentId;
	}

	
    /**
     * Delete a record by id
     *
     * @param   mixed  $pk  Primary key value to delete. Optional
     *
     * @return bool
     */
    public function delete($pk = null)
    {
        $this->load($pk);
        $result = parent::delete($pk);
        
        return $result;
    }

	

	
}
