<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_buka_lembaran_baharu
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_rsvp_buka_lembaran_baharu'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Rsvp_buka_lembaran_baharu', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Rsvp_buka_lembaran_baharuHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'rsvp_buka_lembaran_baharu.php');

$controller = BaseController::getInstance('Rsvp_buka_lembaran_baharu');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
