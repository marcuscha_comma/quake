<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Channel_profile_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_channel_profile_files/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.segment').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('segmenthidden')){
			js('#jform_segment option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_segment").trigger("liszt:updated");
	js('input:hidden.category').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('categoryhidden')){
			js('#jform_category option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_category").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'channelprofilefile.cancel') {
			Joomla.submitform(task, document.getElementById('channelprofilefile-form'));
		}
		else {
			
			if (task != 'channelprofilefile.cancel' && document.formvalidator.isValid(document.id('channelprofilefile-form'))) {
				
	if(js('#jform_segment option:selected').length == 0){
		js("#jform_segment option[value=0]").attr('selected','selected');
	}
	if(js('#jform_category option:selected').length == 0){
		js("#jform_category option[value=0]").attr('selected','selected');
	}
				Joomla.submitform(task, document.getElementById('channelprofilefile-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_channel_profile_files&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="channelprofilefile-form" class="form-validate form-horizontal">

	
	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'channelprofilefile')); ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'channelprofilefile', JText::_('COM_CHANNEL_PROFILE_FILES_TAB_CHANNELPROFILEFILE', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
				<legend><?php echo JText::_('COM_CHANNEL_PROFILE_FILES_FIELDSET_CHANNELPROFILEFILE'); ?></legend>
				<?php echo $this->form->renderField('title'); ?>
				<?php echo $this->form->renderField('period'); ?>
				<?php echo $this->form->renderField('attach_file'); ?>
				<?php if (!empty($this->item->attach_file)) : ?>
					<?php $attach_fileFiles = array(); ?>
					<?php foreach ((array)$this->item->attach_file as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a href="<?php echo JRoute::_(JUri::root() . 'uploads/channel-profile' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a> | 
							<?php $attach_fileFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<input type="hidden" name="jform[attach_file_hidden]" id="jform_attach_file_hidden" value="<?php echo implode(',', $attach_fileFiles); ?>" />
				<?php endif; ?>
				<?php echo $this->form->renderField('type'); ?>
				<?php echo $this->form->renderField('segment'); ?>
			<?php
				foreach((array)$this->item->segment as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="segment" name="jform[segmenthidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php echo $this->form->renderField('category'); ?>
			<?php
				foreach((array)$this->item->category as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="category" name="jform[categoryhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
				<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
				<?php endif; ?>
			</fieldset>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
	<?php echo $this->form->renderField('created_by'); ?>
	<?php echo $this->form->renderField('modified_by'); ?>

	
	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>

</form>
