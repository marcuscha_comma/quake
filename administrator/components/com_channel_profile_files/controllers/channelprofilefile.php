<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Channel_profile_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Channelprofilefile controller class.
 *
 * @since  1.6
 */
class Channel_profile_filesControllerChannelprofilefile extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'channelprofilefiles';
		parent::__construct();
	}
}
