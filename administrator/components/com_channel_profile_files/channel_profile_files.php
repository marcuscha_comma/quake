<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Channel_profile_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_channel_profile_files'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Channel_profile_files', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Channel_profile_filesHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'channel_profile_files.php');

$controller = BaseController::getInstance('Channel_profile_files');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
