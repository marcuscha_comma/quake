<?php
/**
 * @version    CVS: 1.0.2
 * @package    Com_Segments_digital
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_segments_digital'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Segments_digital', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Segments_digitalHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'segments_digital.php');

$controller = BaseController::getInstance('Segments_digital');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
