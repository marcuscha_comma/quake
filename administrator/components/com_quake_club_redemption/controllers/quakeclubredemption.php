<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_redemption
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Quakeclubredemption controller class.
 *
 * @since  1.6
 */
class Quake_club_redemptionControllerQuakeclubredemption extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'quakeclubredemptions';
		parent::__construct();
	}

	public function pointReimbursement(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$app = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;
		$jdate = new JDate;
		$id = $jinput->get('id','', 'String');
		$user_id = $jinput->get('user_id','', 'String');
		$product_id = $jinput->get('product_id','', 'String');
		$reimburse_point = $jinput->get('reimburse_point','', 'String');
		$order_number = $jinput->get('order_number','', 'String');
		$quantity = $jinput->get('quantity','', 'String');
		$variation = $jinput->get('variation','', 'String');
		$variation_index = $jinput->get('variation_index','', 'String');
		$redemption_count_status = $jinput->get('redemption_count_status','', 'String');
		$config = JFactory::getConfig();
		$offset = $config->get('offset');
		$date = new JDate('now', $offset);
		// $convertedDate = $date->format('Y-m-d');
		$convertedDate = date('Y-m-d h:i:s');


		// echo "<pre>";
		// print_r($jinput );
		// echo "</pre>";
		// die();

		$db = JFactory::getDbo();
		$adminUser   = JFactory::getUser();
		$adminUserId = (int) $adminUser->get('id');

		$db->setQuery('SELECT reimbursement_status FROM #__cus_quake_club_redemption where id ='. $id);
		$reimburse_status           = $db->loadResult();

		if ( $reimburse_status ) {
			echo 0;
		}else{
			$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_user_point');
			$max             = $db->loadResult();

			$profile1 = new stdClass();
			$profile1->user_id = $user_id;
			$profile1->point=$reimburse_point * $quantity;
			$profile1->type=10;
			$profile1->state=1;
			$profile1->source=$order_number;
			$profile1->created_by=$adminUserId;
			$profile1->created_on=$convertedDate;
			$profile1->modified_by=$adminUserId;
			$profile1->ordering=$max + 1;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_qperks_user_point', $profile1);

			$month = 'month'.date('m');
			$db->setQuery('SELECT id FROM #__cus_quake_club_qperks_monthly where user_id = '. $user_id .' AND year ="'.date('Y').'"' );
			$monthly_id             = $db->loadResult();
			$db->setQuery('SELECT '.$month.' FROM #__cus_quake_club_qperks_monthly where user_id = '. $user_id .' AND year ="'.date('Y').'"' );
			$monthly_point            = $db->loadResult();

			$monthly = new stdClass();
			$monthly->$month = $monthly_point + ($reimburse_point * $quantity);
			$monthly->id = $monthly_id;

			// Insert the object into the user profile table.
			$resultInsertMonth = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');

			$db->setQuery('SELECT quantity,list_templates,variation FROM #__cus_qperks_products where id = '. $product_id);
			$product_detail             = $db->loadAssoc();

			

			if ($variation) {
				$tmp = "";
				$tmp = json_decode($product_detail['list_templates']);

				$tmp->quantity[$variation_index] = (string)((int)$tmp->quantity[$variation_index] + (int) $quantity);
				$product_detail['list_templates'] = json_encode($tmp);
			}

			// $product = new stdClass();
			// $product->quantity = (int)$product_detail['quantity'] + (int)$quantity;
			// $product->list_templates = $product_detail['list_templates'];
			// $product->id = $product_id;

			// // Insert the object into the user profile table.
			// $resultUpdate = JFactory::getDbo()->updateObject('#__cus_qperks_products', $product, 'id');

			$redemption = new stdClass();
			$redemption->reimbursement_status = 1;
			$redemption->redemption_count_status = $redemption_count_status;
			$redemption->reimbursement_point = $reimburse_point * $quantity;
			$redemption->status = 0;
			$redemption->id = $id;

			// Insert the object into the user profile table.
			$resultUpdate = JFactory::getDbo()->updateObject('#__cus_quake_club_redemption', $redemption, 'id');

			if ($redemption_count_status) {
				$db->setQuery('SELECT redemption_num FROM #__users where id = '. $user_id);
				$redemption_num            = $db->loadResult();
				$userProfile = new stdClass();
				$userProfile->redemption_num = $redemption_num + 1;
				$userProfile->id = $user_id;
				$resultUpdate = JFactory::getDbo()->updateObject('#__users', $userProfile, 'id');
			}
			


			if ($resultUpdate) {
				echo 1;
			}else{
				echo 2;
			}
		}

		JFactory::getApplication()->close();
	}
}
