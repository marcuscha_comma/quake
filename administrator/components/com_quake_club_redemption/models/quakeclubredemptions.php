<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_redemption
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Quake_club_redemption records.
 *
 * @since  1.6
 */
class Quake_club_redemptionModelQuakeclubredemptions extends \Joomla\CMS\MVC\Model\ListModel
{
    
        
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
				'order_number', 'a.`order_number`',
				'user_id', 'a.`user_id`',
				'product_id', 'a.`product_id`',
				'quantity', 'a.`quantity`',
				'point', 'a.`point`',
				'promo', 'a.`promo`',
				'promo_qperks', 'a.`promo_qperks`',
				'status', 'a.`status`',
				'created_on', 'a.`created_on`',
			);
		}

		parent::__construct($config);
	}

    
        
    
        
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
        // List state information.
        parent::populateState("a.id", "ASC");

        $context = $this->getUserStateFromRequest($this->context . '.context', 'context', 'com_content.article', 'CMD');
        $this->setState('filter.context', $context);

        // Split context into component and optional section
        $parts = FieldsHelper::extract($context);

        if ($parts)
        {
            $this->setState('filter.component', $parts[0]);
            $this->setState('filter.section', $parts[1]);
        }
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

                
                    return parent::getStoreId($id);
                
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__cus_quake_club_redemption` AS a');
                
		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');

		// Join over the user field 'user_id'
		$query->select('`user_id`.name AS `user_id`, `user_id`.email AS `user_email`');
		$query->join('LEFT', '#__users AS `user_id` ON `user_id`.id = a.`user_id`');

		// Join over the user field 'user_id'
		$query->select('`company_user`.business_card AS `business_card`');
		$query->join('LEFT', '#__cus_qperks_company_user AS `company_user` ON `company_user`.user_id = a.`user_id`');
                

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				// $query->where('( a.order_number LIKE ' . $search . ' )');
				$query->where('(a.order_number LIKE ' . $search . ' OR `user_id`.name LIKE ' . $search .')');
			}
		}
                

		// Filtering user_id
		$filter_user_id = $this->state->get("filter.user_id");

		if ($filter_user_id !== null && !empty($filter_user_id))
		{
			$query->where("a.`user_id` = '".$db->escape($filter_user_id)."'");
		}

		// Filtering status
		$filter_status = $this->state->get("filter.status");

		if ($filter_status !== null && (is_numeric($filter_status) || !empty($filter_status)))
		{
			$query->where("a.`status` = '".$db->escape($filter_status)."'");
		}

		// Filtering created_on
		$filter_created_on_from = $this->state->get("filter.created_on.from");

		if ($filter_created_on_from !== null && !empty($filter_created_on_from))
		{
			$query->where("a.`created_on` >= '".$db->escape($filter_created_on_from)."'");
		}
		$filter_created_on_to = $this->state->get("filter.created_on.to");

		if ($filter_created_on_to !== null  && !empty($filter_created_on_to))
		{
			$query->where("a.`created_on` <= '".$db->escape($filter_created_on_to)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering', "a.id");
		$orderDirn = $this->state->get('list.direction', "ASC");

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
                
		foreach ($items as $oneItem)
		{

			if (isset($oneItem->product_id))
			{
				$values    = explode(',', $oneItem->product_id);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						if ($oneItem->event_type=="2020 New Year Event") {
							$db = JFactory::getDbo();
							$query = "SELECT * FROM #__cus_qperks_event_products WHERE #__cus_qperks_event_products.id LIKE '" . $value . "'";
							$db->setQuery($query);
							$results = $db->loadObject();
						}else{
							$db = JFactory::getDbo();
							$query = "SELECT * FROM #__cus_qperks_products WHERE #__cus_qperks_products.id LIKE '" . $value . "'";
							$db->setQuery($query);
							$results = $db->loadObject();
						}
						

						if ($results)
						{
							$textValue[] = $results->name;
						}
					}
				}

				$oneItem->product_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->product_id;
			}
					$oneItem->status = ($oneItem->status == '') ? '' : JText::_('COM_QUAKE_CLUB_REDEMPTION_QUAKECLUBREDEMPTIONS_STATUS_OPTION_' . strtoupper($oneItem->status));
		}

		return $items;
	}
}
