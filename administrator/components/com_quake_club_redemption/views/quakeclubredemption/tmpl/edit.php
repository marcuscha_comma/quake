<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_redemption
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_quake_club_redemption/css/form.css');
?>
<style>
	 .line{
		width: 30%;
		/* height: 47px; */
		border-bottom: 1px solid black;
		margin-bottom:10px;
		/* position: absolute; */
		}
</style>
<script type="text/javascript">
	js = jQuery.noConflict();
	var id = <?php echo json_encode($this->item->id); ?>;
	var user_id = <?php echo json_encode($this->item->user_id); ?>;
	var product_id = <?php echo json_encode($this->item->product_id); ?>;
	var order_number = <?php echo json_encode($this->item->order_number); ?>;
	var quantity = <?php echo json_encode($this->item->quantity); ?>;
	var variation = <?php echo json_encode($this->item->variation); ?>;
	var variation_index = <?php echo json_encode($this->item->variation_index); ?>;
	js(document).ready(function () {
		
	js('input:hidden.product_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('product_idhidden')){
			js('#jform_product_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_product_id").trigger("liszt:updated");
	});

	function pointReimbursement(){
		// console.log(jQuery( "#jform_redemption_count_status" ).val());
		var reimburse_point = jQuery( "#reimburse_point" ).val();
		var redemption_count_status = jQuery( "#jform_redemption_count_status" ).val();
		
		jQuery.ajax({
		url: "index.php?option=com_quake_club_redemption&task=quakeclubredemption.pointReimbursement",
		type: 'post',
		data: {
			'id': id,
			'user_id': user_id,
			'product_id': product_id,
			'reimburse_point' : reimburse_point,
			'order_number' : order_number,
			'quantity' : quantity,
			'variation' : variation,
			'variation_index' : variation_index,
			'redemption_count_status' : redemption_count_status
		},
		success: function (result) {
			if (result == 0) {
				alert('Points have been reimbursed to the user, please refer to the user details page.');
			}else if(result == 1){
				alert('Redemption is rejected and points are reimbursed to the user.');
			}else{
				alert('Something wrong please contact admin.');
			}

		},
		error: function () {
			console.log('fail');
		}
	});
	}

	Joomla.submitbutton = function (task) {
		if (task == 'quakeclubredemption.cancel') {
			Joomla.submitform(task, document.getElementById('quakeclubredemption-form'));
		}
		else {
			
			if (task != 'quakeclubredemption.cancel' && document.formvalidator.isValid(document.id('quakeclubredemption-form'))) {
				console.log(task);
				
				Joomla.submitform(task, document.getElementById('quakeclubredemption-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo Route::_('index.php?option=com_quake_club_redemption&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="quakeclubredemption-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo HTMLHelper::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', Text::_('COM_QUAKE_CLUB_REDEMPTION_TITLE_QUAKECLUBREDEMPTION', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>				
				<?php echo $this->form->renderField('order_number'); ?>
				<?php echo $this->form->renderField('user_id'); ?>
				<!-- <?php echo $this->item->event_type == "2020 New Year Event"?$this->form->renderField('product_id_1'):$this->form->renderField('product_id'); ?> -->

				<?php if($this->item->event_type == "2020 New Year Event"){ ?>
				<div class="control-group">
				
					<div class="control-label"><label id="jform_product_id-lbl" for="jform_product_id">
						Product</label>
					</div>
					<div class="controls">
						<select id="jform_product_id" name="jform[product_id]" style="display: none;">
							<option value="1">Aeon Cash Voucher</option>
							<option value="2">Astro Go Shop Voucher</option>
							<option value="4">Ikea Gift Card </option>
							<option value="5">Starbucks Gift Card</option>
							<option value="8">Joey Yap's Feng Shui &amp; Astrology 2020 (Kuala Lumpur) - English Session</option>
							<option value="9">Astro 2020 Plush Toy</option>
							<option value="10">Astro 2020 CNY Album</option>
							<option value="11">Astro 2020 CNY T-shirt </option>
							<option value="12">KOI Cash Voucher</option>
						</select>
					</div>
					</div>
				<?php } else {
					echo $this->form->renderField('product_id');
				} ?>

			<?php
				foreach((array)$this->item->product_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="product_id" name="jform[product_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('quantity'); ?>
				<?php echo $this->form->renderField('point'); ?>
				<?php echo $this->form->renderField('status'); ?>
				<!-- <?php echo $this->form->renderField('redemption_count_status'); ?> -->
			<div style="padding:15px; background-color:#eee; margin-bottom:25px;" data-showon='[{"field":"jform[status]","values":["0"],"sign":"=","op":""}]'>

				<div class="control-group" data-showon='[{"field":"jform[status]","values":["0"],"sign":"=","op":""}]'>
				
					<div class="control-label"><label id="jform_redemption_count_status-lbl" for="jform_redemption_count_status">
						Return redemption Count</label>
					</div>
					<div class="controls">
						<select id="jform_redemption_count_status" name="jform[redemption_count_status]" style="display: none;">
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select>
					</div>
				</div>
				<?php if(!$this->item->reimbursement_status){ ?>
				<div class="control-group" data-showon='[{"field":"jform[status]","values":["0"],"sign":"=","op":""}]'>
					<div class="control-label"><label id="jform_block-lbl" for="jform_block">
							Point Reimbursement</label>
					</div>
					<div class="controls">
						<input id="reimburse_point" type="number" value="<?php echo $this->item->point; ?>" />
						<div class="btn" onclick="pointReimbursement('<?php echo $this->item->id; ?>')" >Submit Now</div>
					</div>
				</div>
				<?php }else{ ?>
					<div class="control-group" data-showon='[{"field":"jform[status]","values":["0"],"sign":"=","op":""}]'>
					<div class="control-label"><label id="jform_block-lbl" for="jform_block">
							Point Reimbursement</label>
					</div>
					<div class="controls">
						<?php echo $this->item->reimbursement_point .' Points have been reimbursed to the user';?>
					</div>
				</div>
				<?php } ?>
				</div>

				
				<?php echo $this->form->renderField('delivery_status'); ?>
				<?php echo $this->form->renderField('delivery_on'); ?>
				<?php echo $this->form->renderField('created_on'); ?>
				<?php echo $this->form->renderField('remark'); ?>
				<?php echo $this->form->renderField('recipient_name'); ?>
				<?php echo $this->form->renderField('delivery_address'); ?>
				<div class="control-group">
					<div class="control-label"><label id="jform_block-lbl" for="jform_block">
							Business card</label>
					</div>
					<div class="controls">
						<img style="max-width:600px;border:1px solid grey;" src="<?php echo $base_url.'.'. $this->item->business_card;?>" alt="">
					</div>
				</div>



					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo HTMLHelper::_('bootstrap.endTab'); ?>

		

		<?php echo HTMLHelper::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo HTMLHelper::_('form.token'); ?>

	</div>
</form>
