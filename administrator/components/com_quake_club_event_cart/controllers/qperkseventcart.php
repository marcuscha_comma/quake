<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_event_cart
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Qperkseventcart controller class.
 *
 * @since  1.6
 */
class Quake_club_event_cartControllerQperkseventcart extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'qperkseventcarts';
		parent::__construct();
	}
}
