<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Newsroompage
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_newsroompage'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Newsroompage', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('NewsroompageHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'newsroompage.php');

$controller = JControllerLegacy::getInstance('Newsroompage');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
