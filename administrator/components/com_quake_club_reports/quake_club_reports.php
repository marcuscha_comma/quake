<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_quake_club_reports'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Quake_club_reports', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Quake_club_reportsHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'quake_club_reports.php');

$controller = BaseController::getInstance('Quake_club_reports');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
