CREATE TABLE IF NOT EXISTS `#__cus_quake_club_reports` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT 1,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00",
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`article_total_shared` VARCHAR(255)  NOT NULL ,
`article_top_ten_shared` VARCHAR(255)  NOT NULL ,
`download_total_each_segments` VARCHAR(255)  NOT NULL ,
`download_top_ten_all_segments` VARCHAR(255)  NOT NULL ,
`user_activites` VARCHAR(255)  NOT NULL ,
`growth_of_activities` VARCHAR(255)  NOT NULL ,
`article_most_shared` VARCHAR(255)  NOT NULL ,
`total_members` VARCHAR(255)  NOT NULL ,
`total_new_members` VARCHAR(255)  NOT NULL ,
`top_ten_users_earned_qperks` VARCHAR(255)  NOT NULL ,
`user_profile_completeness` VARCHAR(255)  NOT NULL ,
`total_product_redeemed` VARCHAR(255)  NOT NULL ,
`top_ten_product_redeemed` VARCHAR(255)  NOT NULL ,
`total_status_redemption_count` VARCHAR(255)  NOT NULL ,
`total_redeemed_qperks` VARCHAR(255)  NOT NULL ,
`total_qperks_earned` VARCHAR(255)  NOT NULL ,
`total_reimbursement_qperks` VARCHAR(255)  NOT NULL ,
`top_ten_friend_referral` VARCHAR(255)  NOT NULL ,
`total_success_friend_referral` VARCHAR(255)  NOT NULL ,
`most_shared_download_by_day` VARCHAR(255)  NOT NULL ,
`year` VARCHAR(255)  NOT NULL ,
`month` VARCHAR(255)  NOT NULL ,
`week` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'Quake club report','com_quake_club_reports.quakeclubreport','{"special":{"dbtable":"#__cus_quake_club_reports","key":"id","type":"Quakeclubreport","prefix":"Quake_club_reportsTable"}}', '{"formFile":"administrator\/components\/com_quake_club_reports\/models\/forms\/quakeclubreport.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_quake_club_reports.quakeclubreport')
) LIMIT 1;
