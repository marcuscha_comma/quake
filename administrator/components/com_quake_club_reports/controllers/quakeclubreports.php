<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');
jimport('phpspreadsheet.phpspreadsheet');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Session\session;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

/**
 * Quakeclubreports list controller class.
 *
 * @since  1.6
 */
class Quake_club_reportsControllerQuakeclubreports extends \Joomla\CMS\MVC\Controller\AdminController
{
	/**
	 * Method to clone existing Quakeclubreports
	 *
	 * @return void
     *
     * @throws Exception
	 */
	public function duplicate()
	{
		// Check for request forgeries
		session::checkToken() or jexit(Text::_('JINVALID_TOKEN'));

		// Get id(s)
		$pks = $this->input->post->get('cid', array(), 'array');

		try
		{
			if (empty($pks))
			{
				throw new Exception(Text::_('COM_QUAKE_CLUB_REPORTS_NO_ELEMENT_SELECTED'));
			}

			ArrayHelper::toInteger($pks);
			$model = $this->getModel();
			$model->duplicate($pks);
			$this->setMessage(Text::_('COM_QUAKE_CLUB_REPORTS_ITEMS_SUCCESS_DUPLICATED'));
		}
		catch (Exception $e)
		{
			Factory::getApplication()->enqueueMessage($e->getMessage(), 'warning');
		}

		$this->setRedirect('index.php?option=com_quake_club_reports&view=quakeclubreports');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    Optional. Model name
	 * @param   string  $prefix  Optional. Class prefix
	 * @param   array   $config  Optional. Configuration array for model
	 *
	 * @return  object	The Model
	 *
	 * @since    1.6
	 */
	public function getModel($name = 'quakeclubreport', $prefix = 'Quake_club_reportsModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 * @since   3.0
     *
     * @throws Exception
     */
	public function saveOrderAjax()
	{
		// Get the input
		$input = Factory::getApplication()->input;
		$pks   = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		ArrayHelper::toInteger($pks);
		ArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Close the application
		Factory::getApplication()->close();
	}

	public function exportCsv()
	{
		$pks = $this->input->post->get('cid', array(), 'array');
		
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		foreach ($pks as $key => $value) {

			$db = JFactory::getDBO();
			$db->setQuery('SELECT * FROM #__cus_quake_club_reports where id ='.$value);
			$result = $db->loadAssocList();

			$styleArrayWeekDate = array(
				'fill' => [
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => '404040' ),
				],
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => 'FFFFFF' ),
					'size' => 16
				));

			$styleArrayWeekDateValue = array(
				'fill' => array(
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => 'D9D9D9' )
				),
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_LEFT
				],
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => '000000' ),
					'size' => 16
			));

			$styleArrayTitle = array(
				'fill' => array(
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => '000000' )
				),
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => 'EC008C' ),
					'size' => 14
			));

			$styleArraySubTitle = array(
				'fill' => array(
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => '404040' )
				),
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => 'FFFFFF' ),
					'size' => 11
			));

			$styleArrayValue = array(
				'fill' => array(
					'fillType' => Fill::FILL_SOLID,
					'color' => array('rgb' => 'D9D9D9' )
				),
				'alignment' => [
					'horizontal' => Alignment::HORIZONTAL_LEFT
				],
				'font'  => array(
					'color' => array('rgb' => '000000' ),
					'size' => 11
			));

			$currentWeekNumber = $result[0]['week'];
			$dto = new DateTime();
			$dto->setISODate($result[0]['year'], $currentWeekNumber-1);
			$ret['week_start'] = $dto->format('Y-m-d');
			$dto->modify('+6 days');
			$ret['week_end'] = $dto->format('Y-m-d');

			$objWorkSheet = $spreadsheet->createSheet($key);
			$objWorkSheet->setTitle("Weekly Report");
			$objWorkSheet->setCellValue('A1', 'Week');
			$objWorkSheet->setCellValue('B1', $result[0]['week']);

			$objWorkSheet->setCellValue('A2', 'Date');
			$objWorkSheet->setCellValue('B2', $ret['week_start']. ' ~ '. $ret['week_end']);

			$objWorkSheet->setCellValue('A5', 'Total shared articles by month');
			$objWorkSheet->setCellValue('A6', $result[0]['article_total_shared']);

			$objWorkSheet->setCellValue('A8', 'Total shared articles by week');
			$objWorkSheet->setCellValue('A9', $result[0]['article_total_shared_by_week']);

			$objWorkSheet->setCellValue('A11', 'Top 10 article shared by month');
			$objWorkSheet->setCellValue('A12', 'Total');
			$objWorkSheet->setCellValue('B12', 'Source');

			$tmp = json_decode($result[0]['article_top_ten_shared']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+13), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+13), $tmp[$i]->source);
			}

			$objWorkSheet->setCellValue('A25', 'Top 10 article shared by week');
			$objWorkSheet->setCellValue('A26', 'Total');
			$objWorkSheet->setCellValue('B26', 'Source');

			$tmp = json_decode($result[0]['article_top_ten_shared_by_week']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+27), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+27), $tmp[$i]->source);
			}

			$objWorkSheet->setCellValue('A39', 'Total download for each segments by month');
			$objWorkSheet->setCellValue('A40', 'Total');
			$objWorkSheet->setCellValue('B40', 'Segment');
			$objWorkSheet->setCellValue('C40', 'Source');

			$tmp = json_decode($result[0]['download_total_each_segments']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+41), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+41), $tmp[$i]->value);
				$objWorkSheet->setCellValue('C'.($i+41), $tmp[$i]->source);
			}

			$objWorkSheet->setCellValue('A52', 'Top 10 download for all segments by month');
			$objWorkSheet->setCellValue('A53', 'Total');
			$objWorkSheet->setCellValue('B53', 'Segment');
			$objWorkSheet->setCellValue('C53', 'Source');

			$tmp = json_decode($result[0]['download_top_ten_all_segments']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+54), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+54), $tmp[$i]->value);
				$objWorkSheet->setCellValue('C'.($i+54), $tmp[$i]->source);
			}

			$objWorkSheet->setCellValue('A66', 'Top 10 download for all segments by week');
			$objWorkSheet->setCellValue('A67', 'Total');
			$objWorkSheet->setCellValue('B67', 'Segment');
			$objWorkSheet->setCellValue('C67', 'Source');

			$tmp = json_decode($result[0]['download_top_ten_all_segments_by_week']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+68), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+68), $tmp[$i]->value);
				$objWorkSheet->setCellValue('C'.($i+68), $tmp[$i]->source);
			}

			$objWorkSheet->setCellValue('A80', 'Top user’s activities by month');
			$objWorkSheet->setCellValue('A81', 'Total');
			$objWorkSheet->setCellValue('B81', 'Type');

			$tmp_array = [
				"Profile update", "Friend referral", "Article sharing", "Birthday gift redemption", "Event attendance", "First-time sign up", "Article download", "Social media linking", "Event attendance",  "Q-Perks reimbursement","Winning partnership sharing", "Join Quake WhatsApp", "Ebook sharing", "Ebook download"
			]; 
			$tmp = json_decode($result[0]['user_activites']);
			
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+82), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+82), $tmp_array[$tmp[$i]->type-1]);
			}

			$objWorkSheet->setCellValue('A98', 'Top user’s activities by week');
			$objWorkSheet->setCellValue('A99', 'Total');
			$objWorkSheet->setCellValue('B99', 'Type');

			$tmp_array = [
				"Profile update", "Friend referral", "Article sharing", "Birthday gift redemption", "Event attendance", "First-time sign up", "Article download", "Social media linking", "Event attendance",  "Q-Perks reimbursement","Winning partnership sharing", "Join Quake WhatsApp", "Ebook sharing", "Ebook download"
			]; 
			$tmp = json_decode($result[0]['user_activites_by_week']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+100), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+100), $tmp_array[$tmp[$i]->type-1]);
			}

			$objWorkSheet->setCellValue('A116', 'Growth percentage of activities (vs previous week)');
			$objWorkSheet->setCellValue('A117', number_format($result[0]['growth_of_activities'],2) ."%");

			$objWorkSheet->setCellValue('A119', 'Most Shared article by segments');
			$objWorkSheet->setCellValue('A120', 'Total');
			$objWorkSheet->setCellValue('B120', 'Segment');
			$objWorkSheet->setCellValue('C120', 'Source');

		
			$tmp = json_decode($result[0]['article_most_shared']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+121), $tmp[$i][0]->total);
				$objWorkSheet->setCellValue('B'.($i+121), $tmp[$i][0]->value);
				$objWorkSheet->setCellValue('C'.($i+121), $tmp[$i][0]->source);
			}

			$objWorkSheet->setCellValue('A133', 'Total Members (up-to-date)');
			$objWorkSheet->setCellValue('A134', $result[0]['total_members']);

			$objWorkSheet->setCellValue('A136', 'Total New members by week');
			$objWorkSheet->setCellValue('A137', $result[0]['total_new_members']);

			$objWorkSheet->setCellValue('A139', 'Top 10 users (most Q-perks earned) by week');
			$objWorkSheet->setCellValue('A140', 'Total');
			$objWorkSheet->setCellValue('B140', 'Name');
			$objWorkSheet->setCellValue('C140', 'Email');

			$tmp = json_decode($result[0]['top_ten_users_earned_qperks']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+141), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+141), $tmp[$i]->name);
				$objWorkSheet->setCellValue('C'.($i+141), $tmp[$i]->email);
			}

			$objWorkSheet->setCellValue('A153', 'Total Number and percentage for user profile completeness (up-to-date)');
			$objWorkSheet->setCellValue('A154', number_format($result[0]['user_profile_completeness'],2).'%');

			$objWorkSheet->setCellValue('A157', 'Total number of product redeemed transaction by month');
			$objWorkSheet->setCellValue('A158', $result[0]['total_product_redeemed']);

			$objWorkSheet->setCellValue('A161', 'Total number of product redeemed transaction by week');
			$objWorkSheet->setCellValue('A162', $result[0]['total_product_redeemed_by_week']);

			$objWorkSheet->setCellValue('A165', 'Top 10 product redeemed by month');
			$objWorkSheet->setCellValue('A166', 'Total');
			$objWorkSheet->setCellValue('B166', 'Name');

			$tmp = json_decode($result[0]['top_ten_product_redeemed']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+167), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+167), $tmp[$i]->name);
			}

			$objWorkSheet->setCellValue('A179', 'Top 10 product redeemed by week');
			$objWorkSheet->setCellValue('A180', 'Total');
			$objWorkSheet->setCellValue('B180', 'Name');

			$tmp = json_decode($result[0]['top_ten_product_redeemed_by_week']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+181), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+181), $tmp[$i]->name);
			}

			$objWorkSheet->setCellValue('A193', 'Total Number of success, rejected and pending redemption by month');
			$objWorkSheet->setCellValue('A194', 'Total');
			$objWorkSheet->setCellValue('B194', 'Status');
 
			$tmp = json_decode($result[0]['total_status_redemption_count']);

			$objWorkSheet->setCellValue('A'.($i+195), $tmp[0]->total?$tmp[0]->total:0);
			$objWorkSheet->setCellValue('A'.($i+196), $tmp[1]->total?$tmp[1]->total:0);
			$objWorkSheet->setCellValue('A'.($i+197), $tmp[2]->total?$tmp[1]->total:0);

			$objWorkSheet->setCellValue('B'.($i+195), $tmp[0]->status?$tmp[0]->status:"Pending");
			$objWorkSheet->setCellValue('B'.($i+196), $tmp[1]->status?$tmp[1]->status:"Sucess");
			$objWorkSheet->setCellValue('B'.($i+197), $tmp[2]->status?$tmp[1]->status:"Rejected");

			$objWorkSheet->setCellValue('A200', 'Total Number of success, rejected and pending redemption by week');
			$objWorkSheet->setCellValue('A201', 'Total');
			$objWorkSheet->setCellValue('B201', 'Status');
 
			$tmp = json_decode($result[0]['total_status_redemption_count_by_week']);
		
			$objWorkSheet->setCellValue('A'.($i+202), $tmp[0]->total?$tmp[0]->total:0);
			$objWorkSheet->setCellValue('A'.($i+203), $tmp[1]->total?$tmp[1]->total:0);
			$objWorkSheet->setCellValue('A'.($i+204), $tmp[2]->total?$tmp[1]->total:0);

			$objWorkSheet->setCellValue('B'.($i+202), $tmp[0]->status?$tmp[0]->status:"Pending");
			$objWorkSheet->setCellValue('B'.($i+203), $tmp[1]->status?$tmp[1]->status:"Sucess");
			$objWorkSheet->setCellValue('B'.($i+204), $tmp[2]->status?$tmp[1]->status:"Rejected");

			$objWorkSheet->setCellValue('A207', 'Total redeemed/used Q-Perks - success and rejected by month');
			$objWorkSheet->setCellValue('A208', 'Total');
			$objWorkSheet->setCellValue('B208', 'Status');
			
			$tmp = json_decode($result[0]['total_redeemed_qperks']);

			$objWorkSheet->setCellValue('A'.($i+209), $tmp[0]->total?$tmp[0]->total:0);
			$objWorkSheet->setCellValue('A'.($i+210), $tmp[1]->total?$tmp[1]->total:0);
			$objWorkSheet->setCellValue('A'.($i+211), $tmp[2]->total?$tmp[1]->total:0);

			$objWorkSheet->setCellValue('B'.($i+209), $tmp[0]->status?$tmp[0]->status:"Pending");
			$objWorkSheet->setCellValue('B'.($i+210), $tmp[1]->status?$tmp[1]->status:"Sucess");
			$objWorkSheet->setCellValue('B'.($i+211), $tmp[2]->status?$tmp[1]->status:"Rejected");

			$objWorkSheet->setCellValue('A214', 'Total redeemed/used Q-Perks - success and rejected by week');
			$objWorkSheet->setCellValue('A215', 'Total');
			$objWorkSheet->setCellValue('B215', 'Status');

			$tmp = json_decode($result[0]['total_redeemed_qperks_by_week']);
			
			$objWorkSheet->setCellValue('A'.($i+216), $tmp[0]->total?$tmp[0]->total:0);
			$objWorkSheet->setCellValue('A'.($i+217), $tmp[1]->total?$tmp[1]->total:0);
			$objWorkSheet->setCellValue('A'.($i+218), $tmp[2]->total?$tmp[1]->total:0);

			$objWorkSheet->setCellValue('B'.($i+216), $tmp[0]->status?$tmp[0]->status:"Pending");
			$objWorkSheet->setCellValue('B'.($i+217), $tmp[1]->status?$tmp[1]->status:"Sucess");
			$objWorkSheet->setCellValue('B'.($i+218), $tmp[2]->status?$tmp[1]->status:"Rejected");

			$objWorkSheet->setCellValue('A221', 'Total Q-Perks earned by month');
			$objWorkSheet->setCellValue('A222', $result[0]['total_qperks_earned']);

			$objWorkSheet->setCellValue('A225', 'Total Q-Perks earned by week');
			$objWorkSheet->setCellValue('A226', $result[0]['total_qperks_earned_by_week']);	
			
			$objWorkSheet->setCellValue('A229', 'Total reimbursement Q-perks by month');
			$objWorkSheet->setCellValue('A230', $result[0]['total_reimbursement_qperks']?$result[0]['total_reimbursement_qperks']:0);	

			$objWorkSheet->setCellValue('A233', 'Total reimbursement Q-perks by week');
			$objWorkSheet->setCellValue('A234', $result[0]['total_reimbursement_qperksby_week']?$result[0]['total_reimbursement_qperksby_week']:0);	

			$objWorkSheet->setCellValue('A237', 'Top 10 friend referral (up-to-date)');
			$objWorkSheet->setCellValue('A238', 'Total');
			$objWorkSheet->setCellValue('B238', 'Name');
			$objWorkSheet->setCellValue('C238', 'Email');

			$tmp = json_decode($result[0]['top_ten_friend_referral']);
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+239), $tmp[$i]->total);
				$objWorkSheet->setCellValue('B'.($i+239), $tmp[$i]->name);
				$objWorkSheet->setCellValue('C'.($i+239), $tmp[$i]->email);
			}

			$objWorkSheet->setCellValue('A251', 'Total Success friend referral (up-to-date)');
			$objWorkSheet->setCellValue('A252', $result[0]['total_success_friend_referral']);

			$objWorkSheet->setCellValue('A255', 'Most shared by day');
			$tmp = json_decode($result[0]['most_shared_download_by_day']);
			$objWorkSheet->setCellValue('A256', 'Date');
			$objWorkSheet->setCellValue('B256', 'Total');
			$objWorkSheet->setCellValue('C256', 'Source');
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+257), $tmp[$i]->date);
				$objWorkSheet->setCellValue('B'.($i+257), $tmp[$i]->share[0]->total);
				$objWorkSheet->setCellValue('C'.($i+257), $tmp[$i]->share[0]->source);
			}

			$objWorkSheet->setCellValue('A266', 'Most download by day');
			$tmp = json_decode($result[0]['most_shared_download_by_day']);
			$objWorkSheet->setCellValue('A267', 'Date');
			$objWorkSheet->setCellValue('B267', 'Total');
			$objWorkSheet->setCellValue('C267', 'Source');
			for ($i=0; $i < count($tmp); $i++) {
				$objWorkSheet->setCellValue('A'.($i+268), $tmp[$i]->date);
				$objWorkSheet->setCellValue('B'.($i+268), $tmp[$i]->download[0]->total);
				$objWorkSheet->setCellValue('C'.($i+268), $tmp[$i]->download[0]->source);
			}

			$objWorkSheet->getStyle('A1:A2')->applyFromArray($styleArrayWeekDate);
			$objWorkSheet->getStyle('B1:B2')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A5:B5')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A6:B6')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A8:B8')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A9:B9')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A11:B11')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A12:B12')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A13:B22')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A25:B25')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A26:B26')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A27:B36')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A39:C39')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A40:C40')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A41:C49')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A52:C52')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A53:C53')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A54:C63')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A66:C66')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A67:C67')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A68:C77')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A80:B80')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A81:B81')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A82:B95')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A98:B98')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A99:B99')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A100:B113')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A116:B116')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A117:B117')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A119:C119')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A120:C120')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A121:C130')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A133:B133')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A134:B134')->applyFromArray($styleArrayWeekDateValue);
			
			$objWorkSheet->getStyle('A136:B136')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A137:B137')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A139:C139')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A140:C140')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A141:C150')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A153:B153')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A154:B154')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A157:B157')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A158:B158')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A161:B161')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A162:B162')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A165:B165')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A166:B166')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A167:B176')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A179:B179')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A180:B180')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A181:B190')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A193:B193')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A194:B194')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A195:B197')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A200:B200')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A201:B201')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A202:B204')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A207:B207')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A208:B208')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A209:B211')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A214:B214')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A215:B215')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A216:B218')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A221:B221')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A222:B222')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A225:B225')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A226:B226')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A229:B229')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A230:B230')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A233:B233')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A234:B234')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A237:C237')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A238:C238')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A239:C248')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A251:B251')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A252:B252')->applyFromArray($styleArrayWeekDateValue);

			$objWorkSheet->getStyle('A255:C255')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A256:C256')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A257:C263')->applyFromArray($styleArrayValue);

			$objWorkSheet->getStyle('A266:C266')->applyFromArray($styleArrayTitle);
			$objWorkSheet->getStyle('A267:C267')->applyFromArray($styleArraySubTitle);
			$objWorkSheet->getStyle('A268:C274')->applyFromArray($styleArrayValue);
		}
		$writer = new Xlsx($spreadsheet);
			
		// $writer->save('../uploads/'.$value.'.xlsx');
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="weekly report.xlsx"');
		$writer->save("php://output");
		die();

		// $db = JFactory::getDBO();
		// $db->setQuery('SELECT * FROM #__cus_quake_club_reports where id ='.$pks);
		// $result = $db->loadAssocList();

		// $spreadsheet = new Spreadsheet();
		// $styleArray = array(
		// 	'font'  => array(
		// 		'bold'  => true,
		// 	));
		// $sheet = $spreadsheet->getActiveSheet();
		

		// $writer = new Xlsx($spreadsheet);
		// $writer->save('../uploads/hello_world.xlsx');
		// die();
	}
}
