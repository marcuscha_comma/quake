<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_quake_club_reports/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'quakeclubreport.cancel') {
			Joomla.submitform(task, document.getElementById('quakeclubreport-form'));
		}
		else {
			
			if (task != 'quakeclubreport.cancel' && document.formvalidator.isValid(document.id('quakeclubreport-form'))) {
				
				Joomla.submitform(task, document.getElementById('quakeclubreport-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_quake_club_reports&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="quakeclubreport-form" class="form-validate form-horizontal">

	
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
	<?php echo $this->form->renderField('created_by'); ?>
	<?php echo $this->form->renderField('modified_by'); ?>
	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'quakeclubreport')); ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'quakeclubreport', JText::_('COM_QUAKE_CLUB_REPORTS_TAB_QUAKECLUBREPORT', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
				<legend><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FIELDSET_QUAKECLUBREPORT'); ?></legend>
				<?php echo $this->form->renderField('article_total_shared'); ?>
				<?php echo $this->form->renderField('article_top_ten_shared'); ?>
				<?php echo $this->form->renderField('download_total_each_segments'); ?>
				<?php echo $this->form->renderField('download_top_ten_all_segments'); ?>
				<?php echo $this->form->renderField('user_activites'); ?>
				<?php echo $this->form->renderField('growth_of_activities'); ?>
				<?php echo $this->form->renderField('article_most_shared'); ?>
				<?php echo $this->form->renderField('total_members'); ?>
				<?php echo $this->form->renderField('total_new_members'); ?>
				<?php echo $this->form->renderField('top_ten_users_earned_qperks'); ?>
				<?php echo $this->form->renderField('user_profile_completeness'); ?>
				<?php echo $this->form->renderField('total_product_redeemed'); ?>
				<?php echo $this->form->renderField('top_ten_product_redeemed'); ?>
				<?php echo $this->form->renderField('total_status_redemption_count'); ?>
				<?php echo $this->form->renderField('total_redeemed_qperks'); ?>
				<?php echo $this->form->renderField('total_qperks_earned'); ?>
				<?php echo $this->form->renderField('total_reimbursement_qperks'); ?>
				<?php echo $this->form->renderField('top_ten_friend_referral'); ?>
				<?php echo $this->form->renderField('total_success_friend_referral'); ?>
				<?php echo $this->form->renderField('most_shared_download_by_day'); ?>
				<?php echo $this->form->renderField('year'); ?>
				<?php echo $this->form->renderField('month'); ?>
				<?php echo $this->form->renderField('week'); ?>
				<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
				<?php endif; ?>
			</fieldset>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	
	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>

</form>
