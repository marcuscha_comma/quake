<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Quake_club_reports records.
 *
 * @since  1.6
 */
class Quake_club_reportsModelQuakeclubreports extends \Joomla\CMS\MVC\Model\ListModel
{
    
        
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
				'article_total_shared', 'a.`article_total_shared`',
				'article_top_ten_shared', 'a.`article_top_ten_shared`',
				'download_total_each_segments', 'a.`download_total_each_segments`',
				'download_top_ten_all_segments', 'a.`download_top_ten_all_segments`',
				'user_activites', 'a.`user_activites`',
				'growth_of_activities', 'a.`growth_of_activities`',
				'article_most_shared', 'a.`article_most_shared`',
				'total_members', 'a.`total_members`',
				'total_new_members', 'a.`total_new_members`',
				'top_ten_users_earned_qperks', 'a.`top_ten_users_earned_qperks`',
				'user_profile_completeness', 'a.`user_profile_completeness`',
				'total_product_redeemed', 'a.`total_product_redeemed`',
				'top_ten_product_redeemed', 'a.`top_ten_product_redeemed`',
				'total_status_redemption_count', 'a.`total_status_redemption_count`',
				'total_redeemed_qperks', 'a.`total_redeemed_qperks`',
				'total_qperks_earned', 'a.`total_qperks_earned`',
				'total_reimbursement_qperks', 'a.`total_reimbursement_qperks`',
				'top_ten_friend_referral', 'a.`top_ten_friend_referral`',
				'total_success_friend_referral', 'a.`total_success_friend_referral`',
				'most_shared_download_by_day', 'a.`most_shared_download_by_day`',
				'year', 'a.`year`',
				'month', 'a.`month`',
				'week', 'a.`week`',
			);
		}

		parent::__construct($config);
	}

    
        
    
        

        
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
        // List state information.
        parent::populateState("a.id", "ASC");

        $context = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
        $this->setState('filter.search', $context);

        // Split context into component and optional section
        $parts = FieldsHelper::extract($context);

        if ($parts)
        {
            $this->setState('filter.component', $parts[0]);
            $this->setState('filter.section', $parts[1]);
        }
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

                
                    return parent::getStoreId($id);
                
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__cus_quake_club_reports` AS a');
                
		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');
                

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif (empty($published))
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				
			}
		}
                
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering', "a.id");
		$orderDirn = $this->state->get('list.direction', "ASC");

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
                

		return $items;
	}

	public function getCsvArray()
	{
		$db  = $this->getDbo();
		$db->setQuery('SELECT * FROM #__cus_quake_club_reports where year = 2020 and month = 05 and week = 21');
		$result = $db->loadAssocList();
		return $result[0];
	}
}
