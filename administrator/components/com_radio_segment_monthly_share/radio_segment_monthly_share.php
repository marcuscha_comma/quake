<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Radio_segment_monthly_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_radio_segment_monthly_share'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Radio_segment_monthly_share', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Radio_segment_monthly_shareHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'radio_segment_monthly_share.php');

$controller = BaseController::getInstance('Radio_segment_monthly_share');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
