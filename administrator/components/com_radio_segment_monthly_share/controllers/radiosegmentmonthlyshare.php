<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Radio_segment_monthly_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Radiosegmentmonthlyshare controller class.
 *
 * @since  1.6
 */
class Radio_segment_monthly_shareControllerRadiosegmentmonthlyshare extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'radiosegmentmonthlyshares';
		parent::__construct();
	}
}
