<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Radio_segment_monthly_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Access\Access;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Table\Table;

/**
 * radiosegmentmonthlyshare Table class
 *
 * @since  1.6
 */
class Radio_segment_monthly_shareTableradiosegmentmonthlyshare extends \Joomla\CMS\Table\Table
{
	
	/**
	 * Constructor
	 *
	 * @param   JDatabase  &$db  A database connector object
	 */
	public function __construct(&$db)
	{
		JObserverMapper::addObserverClassToClass('JTableObserverContenthistory', 'Radio_segment_monthly_shareTableradiosegmentmonthlyshare', array('typeAlias' => 'com_radio_segment_monthly_share.radiosegmentmonthlyshare'));
		parent::__construct('#__cus_radio_segment_monthly_share', 'id', $db);
        $this->setColumnAlias('published', 'state');
    }

	/**
	 * Overloaded bind function to pre-process the params.
	 *
	 * @param   array  $array   Named array
	 * @param   mixed  $ignore  Optional array or list of parameters to ignore
	 *
	 * @return  null|string  null is operation was satisfactory, otherwise returns an error
	 *
	 * @see     JTable:bind
	 * @since   1.5
     * @throws Exception
	 */
	public function bind($array, $ignore = '')
	{
	    $date = Factory::getDate();
		$task = Factory::getApplication()->input->get('task');
	    
		$input = JFactory::getApplication()->input;
		$task = $input->getString('task', '');

		if ($array['id'] == 0 && empty($array['created_by']))
		{
			$array['created_by'] = JFactory::getUser()->id;
		}

		if ($array['id'] == 0 && empty($array['modified_by']))
		{
			$array['modified_by'] = JFactory::getUser()->id;
		}

		if ($task == 'apply' || $task == 'save')
		{
			$array['modified_by'] = JFactory::getUser()->id;
		}

		// Support for multiple field: year
		if (isset($array['year']))
		{
			if (is_array($array['year']))
			{
				$array['year'] = implode(',',$array['year']);
			}
			elseif (strpos($array['year'], ',') != false)
			{
				$array['year'] = explode(',',$array['year']);
			}
			elseif (strlen($array['year']) == 0)
			{
				$array['year'] = '';
			}
		}
		else
		{
			$array['year'] = '';
		}

		// Support for multiple field: month
		if (isset($array['month']))
		{
			if (is_array($array['month']))
			{
				$array['month'] = implode(',',$array['month']);
			}
			elseif (strpos($array['month'], ',') != false)
			{
				$array['month'] = explode(',',$array['month']);
			}
			elseif (strlen($array['month']) == 0)
			{
				$array['month'] = '';
			}
		}
		else
		{
			$array['month'] = '';
		}
		// Support for multi file field: attach_file_1
		if (!empty($array['attach_file_1']))
		{
			if (is_array($array['attach_file_1']))
			{
				$array['attach_file_1'] = implode(',', $array['attach_file_1']);
			}
			elseif (strpos($array['attach_file_1'], ',') != false)
			{
				$array['attach_file_1'] = explode(',', $array['attach_file_1']);
			}
		}
		else
		{
			$array['attach_file_1'] = '';
		}

		// Support for multi file field: attach_file_2
		if (!empty($array['attach_file_2']))
		{
			if (is_array($array['attach_file_2']))
			{
				$array['attach_file_2'] = implode(',', $array['attach_file_2']);
			}
			elseif (strpos($array['attach_file_2'], ',') != false)
			{
				$array['attach_file_2'] = explode(',', $array['attach_file_2']);
			}
		}
		else
		{
			$array['attach_file_2'] = '';
		}


		if (isset($array['params']) && is_array($array['params']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['params']);
			$array['params'] = (string) $registry;
		}

		if (isset($array['metadata']) && is_array($array['metadata']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['metadata']);
			$array['metadata'] = (string) $registry;
		}

		if (!Factory::getUser()->authorise('core.admin', 'com_radio_segment_monthly_share.radiosegmentmonthlyshare.' . $array['id']))
		{
			$actions         = Access::getActionsFromFile(
				JPATH_ADMINISTRATOR . '/components/com_radio_segment_monthly_share/access.xml',
				"/access/section[@name='radiosegmentmonthlyshare']/"
			);
			$default_actions = Access::getAssetRules('com_radio_segment_monthly_share.radiosegmentmonthlyshare.' . $array['id'])->getData();
			$array_jaccess   = array();

			foreach ($actions as $action)
			{
                if (key_exists($action->name, $default_actions))
                {
                    $array_jaccess[$action->name] = $default_actions[$action->name];
                }
			}

			$array['rules'] = $this->JAccessRulestoArray($array_jaccess);
		}

		// Bind the rules for ACL where supported.
		if (isset($array['rules']) && is_array($array['rules']))
		{
			$this->setRules($array['rules']);
		}

		return parent::bind($array, $ignore);
	}

	/**
	 * This function convert an array of JAccessRule objects into an rules array.
	 *
	 * @param   array  $jaccessrules  An array of JAccessRule objects.
	 *
	 * @return  array
	 */
	private function JAccessRulestoArray($jaccessrules)
	{
		$rules = array();

		foreach ($jaccessrules as $action => $jaccess)
		{
			$actions = array();

			if ($jaccess)
			{
				foreach ($jaccess->getData() as $group => $allow)
				{
					$actions[$group] = ((bool)$allow);
				}
			}

			$rules[$action] = $actions;
		}

		return $rules;
	}

	/**
	 * Overloaded check function
	 *
	 * @return bool
	 */
	public function check()
	{
		// If there is an ordering column and this is a new row then get the next ordering value
		if (property_exists($this, 'ordering') && $this->id == 0)
		{
			$this->ordering = self::getNextOrder();
		}
		
		
		// Support multi file field: attach_file_1
		$app = JFactory::getApplication();
		$files = $app->input->files->get('jform', array(), 'raw');
		$array = $app->input->get('jform', array(), 'ARRAY');

		if ($files['attach_file_1'][0]['size'] > 0)
		{
			// Deleting existing files
			$oldFiles = Radio_segment_monthly_shareHelper::getFiles($this->id, $this->_tbl, 'attach_file_1');

			foreach ($oldFiles as $f)
			{
				$oldFile = JPATH_ROOT . '/uploads/radio-pdf/' . $f;

				if (file_exists($oldFile) && !is_dir($oldFile))
				{
					unlink($oldFile);
				}
			}

			$this->attach_file_1 = "";

			foreach ($files['attach_file_1'] as $singleFile )
			{
				jimport('joomla.filesystem.file');

				// Check if the server found any error.
				$fileError = $singleFile['error'];
				$message = '';

				if ($fileError > 0 && $fileError != 4)
				{
					switch ($fileError)
					{
						case 1:
							$message = JText::_('File size exceeds allowed by the server');
							break;
						case 2:
							$message = JText::_('File size exceeds allowed by the html form');
							break;
						case 3:
							$message = JText::_('Partial upload error');
							break;
					}

					if ($message != '')
					{
						$app->enqueueMessage($message, 'warning');

						return false;
					}
				}
				elseif ($fileError == 4)
				{
					if (isset($array['attach_file_1']))
					{
						$this->attach_file_1 = $array['attach_file_1'];
					}
				}
				else
				{
					// Check for filesize
					$fileSize = $singleFile['size'];

					if ($fileSize > 26214400)
					{
						$app->enqueueMessage('File bigger than 25MB', 'warning');

						return false;
					}

					// Check for filetype
					$okMIMETypes = 'application/pdf';
					$validMIMEArray = explode(',', $okMIMETypes);
					$fileMime = $singleFile['type'];

					if (!in_array($fileMime, $validMIMEArray))
					{
						$app->enqueueMessage('This filetype is not allowed', 'warning');

						return false;
					}

					// Replace any special characters in the filename
					jimport('joomla.filesystem.file');
					$filename = JFile::stripExt($singleFile['name']);
					$extension = JFile::getExt($singleFile['name']);
					$filename = preg_replace("/[^A-Za-z0-9]/i", "-", $filename);
					$filename = $filename . '.' . $extension;
					$uploadPath = JPATH_ROOT . '/uploads/radio-pdf/' . $filename;
					$fileTemp = $singleFile['tmp_name'];

					if (!JFile::exists($uploadPath))
					{
						if (!JFile::upload($fileTemp, $uploadPath))
						{
							$app->enqueueMessage('Error moving file', 'warning');

							return false;
						}
					}

					$this->attach_file_1 .= (!empty($this->attach_file_1)) ? "," : "";
					$this->attach_file_1 .= $filename;
				}
			}
		}
		else
		{
			$this->attach_file_1 .= $array['attach_file_1_hidden'];
		}
		// Support multi file field: attach_file_2
		$app = JFactory::getApplication();
		$files = $app->input->files->get('jform', array(), 'raw');
		$array = $app->input->get('jform', array(), 'ARRAY');

		if ($files['attach_file_2'][0]['size'] > 0)
		{
			// Deleting existing files
			$oldFiles = Radio_segment_monthly_shareHelper::getFiles($this->id, $this->_tbl, 'attach_file_2');

			foreach ($oldFiles as $f)
			{
				$oldFile = JPATH_ROOT . '/uploads/radio-pdf/' . $f;

				if (file_exists($oldFile) && !is_dir($oldFile))
				{
					unlink($oldFile);
				}
			}

			$this->attach_file_2 = "";

			foreach ($files['attach_file_2'] as $singleFile )
			{
				jimport('joomla.filesystem.file');

				// Check if the server found any error.
				$fileError = $singleFile['error'];
				$message = '';

				if ($fileError > 0 && $fileError != 4)
				{
					switch ($fileError)
					{
						case 1:
							$message = JText::_('File size exceeds allowed by the server');
							break;
						case 2:
							$message = JText::_('File size exceeds allowed by the html form');
							break;
						case 3:
							$message = JText::_('Partial upload error');
							break;
					}

					if ($message != '')
					{
						$app->enqueueMessage($message, 'warning');

						return false;
					}
				}
				elseif ($fileError == 4)
				{
					if (isset($array['attach_file_2']))
					{
						$this->attach_file_2 = $array['attach_file_2'];
					}
				}
				else
				{
					// Check for filesize
					$fileSize = $singleFile['size'];

					if ($fileSize > 26214400)
					{
						$app->enqueueMessage('File bigger than 25MB', 'warning');

						return false;
					}

					// Check for filetype
					$okMIMETypes = 'application/pdf';
					$validMIMEArray = explode(',', $okMIMETypes);
					$fileMime = $singleFile['type'];

					if (!in_array($fileMime, $validMIMEArray))
					{
						$app->enqueueMessage('This filetype is not allowed', 'warning');

						return false;
					}

					// Replace any special characters in the filename
					jimport('joomla.filesystem.file');
					$filename = JFile::stripExt($singleFile['name']);
					$extension = JFile::getExt($singleFile['name']);
					$filename = preg_replace("/[^A-Za-z0-9]/i", "-", $filename);
					$filename = $filename . '.' . $extension;
					$uploadPath = JPATH_ROOT . '/uploads/radio-pdf/' . $filename;
					$fileTemp = $singleFile['tmp_name'];

					if (!JFile::exists($uploadPath))
					{
						if (!JFile::upload($fileTemp, $uploadPath))
						{
							$app->enqueueMessage('Error moving file', 'warning');

							return false;
						}
					}

					$this->attach_file_2 .= (!empty($this->attach_file_2)) ? "," : "";
					$this->attach_file_2 .= $filename;
				}
			}
		}
		else
		{
			$this->attach_file_2 .= $array['attach_file_2_hidden'];
		}

		return parent::check();
	}

	/**
	 * Method to set the publishing state for a row or list of rows in the database
	 * table.  The method respects checked out rows by other users and will attempt
	 * to checkin rows that it can after adjustments are made.
	 *
	 * @param   mixed    $pks     An optional array of primary key values to update.  If not
	 *                            set the instance property value is used.
	 * @param   integer  $state   The publishing state. eg. [0 = unpublished, 1 = published]
	 * @param   integer  $userId  The user id of the user performing the operation.
	 *
	 * @return   boolean  True on success.
	 *
	 * @since    1.0.4
	 *
	 * @throws Exception
	 */
	public function publish($pks = null, $state = 1, $userId = 0)
	{
		// Initialise variables.
		$k = $this->_tbl_key;

		// Sanitize input.
		ArrayHelper::toInteger($pks);
		$userId = (int) $userId;
		$state  = (int) $state;

		// If there are no primary keys set check to see if the instance key is set.
		if (empty($pks))
		{
			if ($this->$k)
			{
				$pks = array($this->$k);
			}
			// Nothing to set publishing state on, return false.
			else
			{
				throw new Exception(500, Text::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
			}
		}

		// Build the WHERE clause for the primary keys.
		$where = $k . '=' . implode(' OR ' . $k . '=', $pks);

		// Determine if there is checkin support for the table.
		if (!property_exists($this, 'checked_out') || !property_exists($this, 'checked_out_time'))
		{
			$checkin = '';
		}
		else
		{
			$checkin = ' AND (checked_out = 0 OR checked_out = ' . (int) $userId . ')';
		}

		// Update the publishing state for rows with the given primary keys.
		$this->_db->setQuery(
			'UPDATE `' . $this->_tbl . '`' .
			' SET `state` = ' . (int) $state .
			' WHERE (' . $where . ')' .
			$checkin
		);
		$this->_db->execute();

		// If checkin is supported and all rows were adjusted, check them in.
		if ($checkin && (count($pks) == $this->_db->getAffectedRows()))
		{
			// Checkin each row.
			foreach ($pks as $pk)
			{
				$this->checkin($pk);
			}
		}

		// If the JTable instance value is in the list of primary keys that were set, set the instance.
		if (in_array($this->$k, $pks))
		{
			$this->state = $state;
		}

		return true;
	}

	/**
	 * Define a namespaced asset name for inclusion in the #__assets table
	 *
	 * @return string The asset name
	 *
	 * @see Table::_getAssetName
	 */
	protected function _getAssetName()
	{
		$k = $this->_tbl_key;

		return 'com_radio_segment_monthly_share.radiosegmentmonthlyshare.' . (int) $this->$k;
	}

	/**
	 * Returns the parent asset's id. If you have a tree structure, retrieve the parent's id using the external key field
	 *
	 * @param   JTable   $table  Table name
	 * @param   integer  $id     Id
	 *
	 * @see Table::_getAssetParentId
	 *
	 * @return mixed The id on success, false on failure.
	 */
	protected function _getAssetParentId(JTable $table = null, $id = null)
	{
		// We will retrieve the parent-asset from the Asset-table
		$assetParent = Table::getInstance('Asset');

		// Default: if no asset-parent can be found we take the global asset
		$assetParentId = $assetParent->getRootId();

		// The item has the component as asset-parent
		$assetParent->loadByName('com_radio_segment_monthly_share');

		// Return the found asset-parent-id
		if ($assetParent->id)
		{
			$assetParentId = $assetParent->id;
		}

		return $assetParentId;
	}

	/**
	 * Delete a record by id
	 *
	 * @param   mixed  $pk  Primary key value to delete. Optional
	 *
	 * @return bool
	 */
	public function delete($pk = null)
	{
		$this->load($pk);
		$result = parent::delete($pk);
		
		if ($result)
		{
			jimport('joomla.filesystem.file');

			$checkImageVariableType = gettype($this->attach_file_1);

			switch ($checkImageVariableType)
			{
			case 'string':
				JFile::delete(JPATH_ROOT . '/uploads/radio-pdf/' . $this->attach_file_1);
			break;
			default:
			foreach ($this->attach_file_1 as $attach_file_1File)
			{
				JFile::delete(JPATH_ROOT . '/uploads/radio-pdf/' . $attach_file_1File);
			}
			}
			jimport('joomla.filesystem.file');

			$checkImageVariableType = gettype($this->attach_file_2);

			switch ($checkImageVariableType)
			{
			case 'string':
				JFile::delete(JPATH_ROOT . '/uploads/radio-pdf/' . $this->attach_file_2);
			break;
			default:
			foreach ($this->attach_file_2 as $attach_file_2File)
			{
				JFile::delete(JPATH_ROOT . '/uploads/radio-pdf/' . $attach_file_2File);
			}
			}
		}

		return $result;
	}
}
