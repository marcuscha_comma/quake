CREATE TABLE IF NOT EXISTS `#__cus_radio_segment_monthly_share` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT 1,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00",
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`title` VARCHAR(255)  NOT NULL ,
`year` TEXT NOT NULL ,
`month` TEXT NOT NULL ,
`attach_file_1` TEXT NOT NULL ,
`attach_file_2` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'Radio segment monthly share','com_radio_segment_monthly_share.radiosegmentmonthlyshare','{"special":{"dbtable":"#__cus_radio_segment_monthly_share","key":"id","type":"Radiosegmentmonthlyshare","prefix":"Radio_segment_monthly_shareTable"}}', '{"formFile":"administrator\/components\/com_radio_segment_monthly_share\/models\/forms\/radiosegmentmonthlyshare.xml", "hideFields":["checked_out","checked_out_time","params","language" ,"attach_file_2"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_radio_segment_monthly_share.radiosegmentmonthlyshare')
) LIMIT 1;
