<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Programme_highlights
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_programme_highlights'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Programme_highlights', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Programme_highlightsHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'programme_highlights.php');

$controller = JControllerLegacy::getInstance('Programme_highlights');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
