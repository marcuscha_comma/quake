<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Segment_monthly_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_segment_monthly_share'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Segment_monthly_share', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Segment_monthly_shareHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'segment_monthly_share.php');

$controller = JControllerLegacy::getInstance('Segment_monthly_share');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
