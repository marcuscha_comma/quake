<?php
/**
 * @version    CVS: 1.0.2
 * @package    Com_Segments_radio
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_segments_radio'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Segments_radio', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Segments_radioHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'segments_radio.php');

$controller = BaseController::getInstance('Segments_radio');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
