<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_V1ewership_network_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_v1ewership_network_share'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('V1ewership_network_share', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('V1ewership_network_shareHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'v1ewership_network_share.php');

$controller = JControllerLegacy::getInstance('V1ewership_network_share');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
