<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_V1ewership_network_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * V1ewershipnetworkshare controller class.
 *
 * @since  1.6
 */
class V1ewership_network_shareControllerV1ewershipnetworkshare extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'v1ewershipnetworkshares';
		parent::__construct();
	}
}
