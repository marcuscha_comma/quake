<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_V1ewership_highlight
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_v1ewership_highlight/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.channel').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('channelhidden')){
			js('#jform_channel option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_channel").trigger("liszt:updated");
	js('input:hidden.month').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('monthhidden')){
			js('#jform_month option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_month").trigger("liszt:updated");
	js('input:hidden.year').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('yearhidden')){
			js('#jform_year option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_year").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'v1ewershiphighlight.cancel') {
			Joomla.submitform(task, document.getElementById('v1ewershiphighlight-form'));
		}
		else {
			
			if (task != 'v1ewershiphighlight.cancel' && document.formvalidator.isValid(document.id('v1ewershiphighlight-form'))) {
				
				Joomla.submitform(task, document.getElementById('v1ewershiphighlight-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_v1ewership_highlight&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="v1ewershiphighlight-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_V1EWERSHIP_HIGHLIGHT_TITLE_V1EWERSHIPHIGHLIGHT', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('channel'); ?>

			<?php
				foreach((array)$this->item->channel as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="channel" name="jform[channelhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('amount'); ?>
				<?php echo $this->form->renderField('month'); ?>

			<?php
				foreach((array)$this->item->month as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="month" name="jform[monthhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('year'); ?>

			<?php
				foreach((array)$this->item->year as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="year" name="jform[yearhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
