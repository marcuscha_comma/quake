CREATE TABLE IF NOT EXISTS `#__cus_v1ewership_highlight` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`channel` TEXT NOT NULL ,
`amount` DOUBLE,
`month` TEXT NOT NULL ,
`year` TEXT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

