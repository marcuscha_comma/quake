CREATE TABLE IF NOT EXISTS `#__cus_rsvp_tour_data` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL DEFAULT 0,
`state` TINYINT(1)  NOT NULL DEFAULT 1,
`checked_out` INT(11)  NOT NULL DEFAULT 0,
`checked_out_time` DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00",
`created_by` INT(11)  NOT NULL DEFAULT 0,
`modified_by` INT(11)  NOT NULL DEFAULT 0,
`user_id` INT(11)  NOT NULL ,
`redemption_status` VARCHAR(255)  NOT NULL ,
`is_attended` VARCHAR(255)  NOT NULL ,
`is_valid` VARCHAR(255)  NOT NULL ,
`is_deleted` VARCHAR(255)  NOT NULL ,
`updated_at` DATETIME NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`comp_name` VARCHAR(255)  NOT NULL ,
`designation` VARCHAR(255)  NOT NULL ,
`mobileNo` VARCHAR(255)  NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`source` VARCHAR(255)  NOT NULL ,
`external_link` VARCHAR(255)  NOT NULL ,
`delivery` TEXT NOT NULL ,
`joinGameStatus` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `field_mappings`, `content_history_options`)
SELECT * FROM ( SELECT 'Rsvp tour datum','com_rsvp_mmd.rsvptourdatum','{"special":{"dbtable":"#__cus_rsvp_tour_data","key":"id","type":"Rsvptourdatum","prefix":"Rsvp_mmdTable"}}', CASE 
                                WHEN 'field_mappings' is null THEN ''
                                ELSE ''
                                END as field_mappings, '{"formFile":"administrator\/components\/com_rsvp_mmd\/models\/forms\/rsvptourdatum.xml", "hideFields":["checked_out","checked_out_time","params","language" ,"delivery"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_rsvp_mmd.rsvptourdatum')
) LIMIT 1;
