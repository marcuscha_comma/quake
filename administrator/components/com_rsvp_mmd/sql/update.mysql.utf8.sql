
INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'Rsvp tour datum','com_rsvp_mmd.rsvptourdatum','{"special":{"dbtable":"#__cus_rsvp_tour_data","key":"id","type":"Rsvptourdatum","prefix":"Rsvp_mmdTable"}}', '{"formFile":"administrator\/components\/com_rsvp_mmd\/models\/forms\/rsvptourdatum.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_rsvp_mmd.rsvptourdatum')
) LIMIT 1;

UPDATE `#__content_types` SET
	`type_title` = 'Rsvp tour datum', 
	`table` = '{"special":{"dbtable":"#__cus_rsvp_tour_data","key":"id","type":"Rsvp tour datum","prefix":"Rsvp_mmdTable"}}', 
	`content_history_options` = '{"formFile":"administrator\/components\/com_rsvp_mmd\/models\/forms\/rsvptourdatum.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'
WHERE (`type_alias` = 'com_rsvp_mmd.rsvptourdatum');
