<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_mmd
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Session\session;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

/**
 * Rsvptourdata list controller class.
 *
 * @since  1.6
 */
class Rsvp_mmdControllerRsvptourdata extends \Joomla\CMS\MVC\Controller\AdminController
{
	/**
	 * Method to clone existing Rsvptourdata
	 *
	 * @return void
     *
     * @throws Exception
	 */
	public function duplicate()
	{
		// Check for request forgeries
		session::checkToken() or jexit(Text::_('JINVALID_TOKEN'));

		// Get id(s)
		$pks = $this->input->post->get('cid', array(), 'array');

		try
		{
			if (empty($pks))
			{
				throw new Exception(Text::_('COM_RSVP_MMD_NO_ELEMENT_SELECTED'));
			}

			ArrayHelper::toInteger($pks);
			$model = $this->getModel();
			$model->duplicate($pks);
			$this->setMessage(Text::_('COM_RSVP_MMD_ITEMS_SUCCESS_DUPLICATED'));
		}
		catch (Exception $e)
		{
			Factory::getApplication()->enqueueMessage($e->getMessage(), 'warning');
		}

		$this->setRedirect('index.php?option=com_rsvp_mmd&view=rsvptourdata');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    Optional. Model name
	 * @param   string  $prefix  Optional. Class prefix
	 * @param   array   $config  Optional. Configuration array for model
	 *
	 * @return  object	The Model
	 *
	 * @since    1.6
	 */
	public function getModel($name = 'rsvptourdatum', $prefix = 'Rsvp_mmdModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 * @since   3.0
     *
     * @throws Exception
     */
	public function saveOrderAjax()
	{
		// Get the input
		$input = Factory::getApplication()->input;
		$pks   = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		ArrayHelper::toInteger($pks);
		ArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Close the application
		Factory::getApplication()->close();
	}

	public function export()
	{	
		ob_end_clean();
		$app = JFactory::getApplication();

		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=Moo Moo Da 2021 Report.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		$model = $this->getModel('rsvptourdata');
		$result = $model->getCsv();
		echo "ID, Name, Email, Mobile no, Company name, Designation, Join game status, Member,Attended live date, Time to enter live room, Time in live room, Delivery Address, Date \r\n";
		
		foreach ($result as $user) {
			echo ($user->user_id) . ", ";
			echo str_replace(",","",($user->user_name!= ""?$user->user_name:$user->name)). ", ";
			echo ($user->user_email != ""?$user->user_email:$user->email) . ", ";
			echo ($user->user_mobile != ''?$user->user_mobile:$user->mobileNo) . ", ";
			echo str_replace(",","",($user->company_name != ""?$user->company_name:$user->comp_name)) . ", ";
			echo str_replace(",","",($user->company_designation!= ""?$user->company_designation:$user->designation)) . ", ";
			echo ($user->joinGameStatus=="1"?"Yes":"No") . ", ";
			echo ($user->member=="1"?"Yes":"No") . ", ";
			echo str_replace(",","",($user->attended_live_date)) . ", ";
			echo str_replace(",","",($user->time_to_enter_live_room)) . ", ";
			echo str_replace(",","",($user->time_in_live_room)) . ", ";
			echo str_replace(",","-",($user->delivery)) . ", ";
			echo $user->updated_at . "\r\n";
		}

		$app->close();
		$this->setRedirect('index.php?option=com_rsvp_mmd&view=rsvptourdata');
	}
}
