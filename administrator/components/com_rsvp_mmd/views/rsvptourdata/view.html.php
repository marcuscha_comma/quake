<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_mmd
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

use \Joomla\CMS\Language\Text;

/**
 * View class for a list of Rsvp_mmd.
 *
 * @since  1.6
 */
class Rsvp_mmdViewRsvptourdata extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		Rsvp_mmdHelper::addSubmenu('rsvptourdata');

		$this->addToolbar();

		// $this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = Rsvp_mmdHelper::getActions();

		JToolBarHelper::title(Text::_('COM_RSVP_MMD_TITLE_RSVPTOURDATA'), 'rsvptourdata.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/rsvptourdatum';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('rsvptourdatum.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('rsvptourdata.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('rsvptourdatum.edit', 'JTOOLBAR_EDIT');
				JToolBarHelper::custom('rsvptourdata.export', 'copy.png', 'copy_f2.png', 'Export', false);

			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('rsvptourdata.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('rsvptourdata.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'rsvptourdata.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('rsvptourdata.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('rsvptourdata.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'rsvptourdata.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('rsvptourdata.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_rsvp_mmd');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_rsvp_mmd&view=rsvptourdata');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
			'a.`user_id`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_USER_ID'),
			'a.`redemption_status`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_REDEMPTION_STATUS'),
			'a.`is_attended`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_IS_ATTENDED'),
			'a.`is_valid`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_IS_VALID'),
			'a.`is_deleted`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_IS_DELETED'),
			'a.`name`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_NAME'),
			'a.`comp_name`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_COMP_NAME'),
			'a.`designation`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_DESIGNATION'),
			'a.`mobileNo`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_MOBILENO'),
			'a.`email`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_EMAIL'),
			'a.`joinGameStatus`' => JText::_('COM_RSVP_MMD_RSVPTOURDATA_JOINGAMESTATUS'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
