<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Event_mircosite
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_event_mircosite/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'rsvp.cancel') {
			Joomla.submitform(task, document.getElementById('rsvp-form'));
		}
		else {
			
			if (task != 'rsvp.cancel' && document.formvalidator.isValid(document.id('rsvp-form'))) {
				
				Joomla.submitform(task, document.getElementById('rsvp-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_event_mircosite&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="rsvp-form" class="form-validate form-horizontal">

	
	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'Event')); ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Event', JText::_('COM_EVENT_MIRCOSITE_TAB_EVENT', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
				<?php echo $this->form->renderField('title'); ?>
				<?php echo $this->form->renderField('alias'); ?>
				<?php echo $this->form->renderField('logo'); ?>
				<?php echo $this->form->renderField('subtitle'); ?>
				<?php echo $this->form->renderField('subtitle_description'); ?>
				<?php echo $this->form->renderField('venue'); ?>
				<?php echo $this->form->renderField('date'); ?>
				<?php echo $this->form->renderField('time'); ?>
			</fieldset>
		</div>
	</div>

	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Speakers', JText::_('COM_EVENT_MIRCOSITE_FIELDSET_SPEAKERS', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<?php echo $this->form->renderField('speakers_title'); ?>
			<?php echo $this->form->renderField('speakers_sort'); ?>
			<?php echo $this->form->renderField('speakers'); ?>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Event-highlight', JText::_('COM_EVENT_MIRCOSITE_FIELDSET_EVENTHIGHTLIGHT', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<?php echo $this->form->renderField('event_highlight_title'); ?>
			<?php echo $this->form->renderField('event_highlight_description'); ?>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Agenda', JText::_('COM_EVENT_MIRCOSITE_FIELDSET_AGENDA', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<?php echo $this->form->renderField('agenda_sort'); ?>
			<?php echo $this->form->renderField('agenda_title'); ?>
			<?php echo $this->form->renderField('agenda_col_title_1'); ?>
			<?php echo $this->form->renderField('agenda_col_title_2'); ?>
			<?php echo $this->form->renderField('agenda_col_title_3'); ?>
			<?php echo $this->form->renderField('agenda_col_title_4'); ?>
			
			<?php echo $this->form->renderField('agenda_2_cols'); ?>
			<?php echo $this->form->renderField('agenda_3_cols'); ?>
			<?php echo $this->form->renderField('agenda_4_cols'); ?>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Audio', JText::_('COM_EVENT_MIRCOSITE_FIELDSET_AUDIO', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<div>
			<?php echo $this->form->renderField('bgm_file_normal'); ?>
				<?php if (!empty($this->item->bgm_file_normal)) : ?>
					<?php $bgm_file_normalFiles = array(); ?>
					<?php foreach ((array)$this->item->bgm_file_normal as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a style="margin-left: 180px;" href="<?php echo JRoute::_(JUri::root() . 'uploads/events/audio' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a>
							<?php $bgm_file_normalFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<input type="hidden" name="jform[bgm_file_normal_hidden]" id="jform_bgm_file_normal_hidden" value="<?php echo implode(',', $bgm_file_normalFiles); ?>" />
				<?php endif; ?>
			</div>
				
				<div>
				<?php echo $this->form->renderField('bgm_file_ios'); ?>
				<?php if (!empty($this->item->bgm_file_ios)) : ?>
					<?php $bgm_file_iosFiles = array(); ?>
					<?php foreach ((array)$this->item->bgm_file_ios as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a style="margin-left: 180px;" href="<?php echo JRoute::_(JUri::root() . 'uploads/events/audio' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a>
							<?php $bgm_file_iosFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<input type="hidden" name="jform[bgm_file_ios_hidden]" id="jform_bgm_file_ios_hidden" value="<?php echo implode(',', $bgm_file_iosFiles); ?>" />
				<?php endif; ?>
				</div>
				
			<div>
			<?php echo $this->form->renderField('bgm_file_android'); ?>
				<?php if (!empty($this->item->bgm_file_android)) : ?>
					<?php $bgm_file_androidFiles = array(); ?>
					<?php foreach ((array)$this->item->bgm_file_android as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a style="margin-left: 180px;" href="<?php echo JRoute::_(JUri::root() . 'uploads/events/audio' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a>
							<?php $bgm_file_androidFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<input type="hidden" name="jform[bgm_file_android_hidden]" id="jform_bgm_file_android_hidden" value="<?php echo implode(',', $bgm_file_androidFiles); ?>" />
				<?php endif; ?>
			</div>
				
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'General', JText::_('COM_EVENT_MIRCOSITE_TAB_GENERAL', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
				<?php echo $this->form->renderField('background_image'); ?>
				<?php echo $this->form->renderField('background_image_size'); ?>
				<?php echo $this->form->renderField('background_color'); ?>
				<?php echo $this->form->renderField('theme_color'); ?>
				<?php echo $this->form->renderField('body_text_color'); ?>
				<?php echo $this->form->renderField('rsvp_bar_background_color'); ?>
				<?php echo $this->form->renderField('rsvp_bar_button_color'); ?>
				<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
				<?php endif; ?>
			</fieldset>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
	<?php echo $this->form->renderField('created_by'); ?>
	<?php echo $this->form->renderField('modified_by'); ?>
	<?php echo $this->form->renderField('created_at'); ?>
	<?php echo $this->form->renderField('updated_at'); ?>

	
	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>

</form>
