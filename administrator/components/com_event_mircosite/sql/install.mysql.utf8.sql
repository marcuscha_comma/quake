CREATE TABLE IF NOT EXISTS `#__cus_event_rsvp` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL DEFAULT 0,
`state` TINYINT(1)  NOT NULL DEFAULT 1,
`checked_out` INT(11)  NOT NULL DEFAULT 0,
`checked_out_time` DATETIME NOT NULL DEFAULT "0000-00-00 00:00:00",
`created_by` INT(11)  NOT NULL DEFAULT 0,
`modified_by` INT(11)  NOT NULL DEFAULT 0,
`title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255) COLLATE utf8_bin NOT NULL ,
`logo` VARCHAR(255)  NOT NULL ,
`subtitle` VARCHAR(255)  NOT NULL ,
`subtitle_description` TEXT NOT NULL ,
`venue` TEXT NOT NULL ,
`date` TEXT NOT NULL ,
`time` TEXT NOT NULL ,
`created_at` DATETIME NOT NULL ,
`updated_at` DATETIME NOT NULL ,
`speakers_title` VARCHAR(255)  NOT NULL ,
`speakers_sort` VARCHAR(255)  NOT NULL ,
`speakers` TEXT NOT NULL ,
`event_highlight_title` VARCHAR(255)  NOT NULL ,
`event_highlight_description` TEXT NOT NULL ,
`agenda_title` VARCHAR(255)  NOT NULL ,
`agenda` TEXT NOT NULL ,
`background_image` VARCHAR(255)  NOT NULL ,
`background_color` VARCHAR(7)  NOT NULL ,
`theme_color` VARCHAR(7)  NOT NULL ,
`body_text_color` VARCHAR(7)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `field_mappings`, `content_history_options`)
SELECT * FROM ( SELECT 'Event rsvp','com_event_mircosite.event','{"special":{"dbtable":"#__cus_event_rsvp","key":"id","type":"Event","prefix":"Event_mircositeTable"}}', CASE 
                                WHEN 'field_mappings' is null THEN ''
                                ELSE ''
                                END as field_mappings, '{"formFile":"administrator\/components\/com_event_mircosite\/models\/forms\/event.xml", "hideFields":["checked_out","checked_out_time","params","language" ,"event_highlight_description"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_event_mircosite.event')
) LIMIT 1;
