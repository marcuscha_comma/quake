<?php
/**
 * @version    CVS: 1.0.2
 * @package    Com_Segments
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_segments'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Segments', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('SegmentsHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'segments.php');

$controller = JControllerLegacy::getInstance('Segments');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
