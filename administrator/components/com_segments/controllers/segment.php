<?php
/**
 * @version    CVS: 1.0.2
 * @package    Com_Segments
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Segment controller class.
 *
 * @since  1.6
 */
class SegmentsControllerSegment extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'segments';
		parent::__construct();
	}
}
