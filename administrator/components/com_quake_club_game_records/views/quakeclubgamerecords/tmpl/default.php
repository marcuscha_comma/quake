<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_game_records
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;


use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Layout\LayoutHelper;
use \Joomla\CMS\Language\Text;

HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/');
HTMLHelper::_('bootstrap.tooltip');
HTMLHelper::_('behavior.multiselect');
HTMLHelper::_('formbehavior.chosen', 'select');

JFactory::getDocument()->addScriptDeclaration('
	Joomla.submitbutton = function(pressbutton)
	{
		if (pressbutton == "quakeclubgamerecords.exportCsv")
		{
			window.location = "index.php?option=com_quake_club_game_records&task=quakeclubgamerecords.exportCsv";
		}else {
			// If not follow the normal path
			document.adminForm.task.value=pressbutton;
			Joomla.submitform(pressbutton);
		  }
		// Joomla.submitform(pressbutton);
	};
');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'administrator/components/com_quake_club_game_records/assets/css/quake_club_game_records.css');
$document->addStyleSheet(Uri::root() . 'media/com_quake_club_game_records/css/list.css');

$user      = Factory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_quake_club_game_records');
$saveOrder = $listOrder == 'a.`ordering`';

// echo "<pre>";
// print_r($this->pagination->getResultsCounter());
// echo "</pre>";

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_quake_club_game_records&task=quakeclubgamerecords.saveOrderAjax&tmpl=component';
    HTMLHelper::_('sortablelist.sortable', 'quakeclubgamerecordList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>

<form action="<?php echo Route::_('index.php?option=com_quake_club_game_records&view=quakeclubgamerecords'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

            <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>
			<table class="table table-striped" id="quakeclubgamerecordList">
				<thead>
				<tr>
					<?php if (isset($this->items[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone">
                            <?php echo HTMLHelper::_('searchtools.sort', '', 'a.`ordering`', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                        </th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo Text::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
					<?php //if (isset($this->items[0]->state)): ?>
						<!-- <th width="1%" class="nowrap center">
								<?php //echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.`state`', $listDirn, $listOrder); ?>
</th> -->
					<?// endif; ?>

									<!-- <th class='left'>
				<?php // echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_GAME_RECORDS_QUAKECLUBGAMERECORDS_ID', 'a.`id`', $listDirn, $listOrder); ?>
				</th> -->
				<!-- <th class='left'>
				<?php //echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_GAME_RECORDS_QUAKECLUBGAMERECORDS_CREATED_ON', 'a.`created_on`', $listDirn, $listOrder); ?>
				</th> -->
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_GAME_RECORDS_QUAKECLUBGAMERECORDS_USER_ID', 'a.`user_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_GAME_RECORDS_QUAKECLUBGAMERECORDS_USER_EMAIL', 'a.`email`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_GAME_RECORDS_QUAKECLUBGAMERECORDS_QCOINS', 'sum(gp.qcoins)', $listDirn, $listOrder); ?>
				</th>
				<!-- <th class='left'>
				<?php //echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_GAME_RECORDS_QUAKECLUBGAMERECORDS_MODIFIED_ON', 'a.`modified_on`', $listDirn, $listOrder); ?>
				</th> -->
				<!-- <th class='left'>
				<?php // echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_GAME_RECORDS_QUAKECLUBGAMERECORDS_IS_WINNER', 'a.`is_winner`', $listDirn, $listOrder); ?>
				</th> -->
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_GAME_RECORDS_QUAKECLUBGAMERECORDS_PLAY_TIMES_COUNT', 'count(gp.id)', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_GAME_RECORDS_QUAKECLUBGAMERECORDS_DATE', 'gp.`created_on`', $listDirn, $listOrder); ?>
				</th>
					
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$ordering   = ($listOrder == 'a.ordering');
					$canCreate  = $user->authorise('core.create', 'com_quake_club_game_records');
					$canEdit    = $user->authorise('core.edit', 'com_quake_club_game_records');
					$canCheckin = $user->authorise('core.manage', 'com_quake_club_game_records');
					$canChange  = $user->authorise('core.edit.state', 'com_quake_club_game_records');
					?>
					<tr class="row<?php echo $i % 2; ?>">

						<?php if (isset($this->items[0]->ordering)) : ?>
							<td class="order nowrap center hidden-phone">
								<?php if ($canChange) :
									$disableClassName = '';
									$disabledLabel    = '';

									if (!$saveOrder) :
										$disabledLabel    = Text::_('JORDERINGDISABLED');
										$disableClassName = 'inactive tip-top';
									endif; ?>
									<span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
										  title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
									<input type="text" style="display:none" name="order[]" size="5"
										   value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
								<?php else : ?>
									<span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
								<?php endif; ?>
							</td>
						<?php endif; ?>
						<td class="hidden-phone">
							<?php echo HTMLHelper::_('grid.id', $i, $item->id); ?>
						</td>
						<?// if (isset($this->items[0]->state)): ?>
							<!-- <td class="center">
								<?// echo JHtml::_('jgrid.published', $item->state, $i, 'quakeclubgamerecords.', $canChange, 'cb'); ?>
</td> -->
						<?// endif; ?>

										<!-- <td>

					<?php echo $item->id; ?>
				</td>				 -->
				<!-- <td>

					<?// echo $item->created_on; ?>
				</td>-->
				<td>

					<?php echo $item->user_id; ?>
				</td>	
				<td>

					<?php echo $item->user_email; ?>
				</td>				
				<td>

					<?php echo $item->t_qcoins?$item->t_qcoins:0; ?>
				</td>				
				<!-- <td>

					<?php // echo $item->modified_on; ?>
				</td>				 -->
				<!-- <td>
					<?php // echo $item->is_winner; ?>
				</td>				 -->
				<td>
					<?php echo $item->play_times; ?>
				</td>

				<td>
					<?php echo $item->date_now?$item->date_now:"-"; ?>
				</td>
				

					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo HTMLHelper::_('form.token'); ?>
		</div>
</form>
<script>

	
    window.toggleField = function (id, task, field) {

        var f = document.adminForm, i = 0, cbx, cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = document.createElement('input');

        inputField.type  = 'hidden';
        inputField.name  = 'field';
        inputField.value = field;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };
	jQuery( document ).ready(function() {
		// console.log(countFromPhp);
		jQuery( ".js-stools-container-bar" ).append( '<div class="btn-wrapper input-append"><input type="text" name="filter[search]" id="filter_search" value="" class="js-stools-search-string" placeholder="Number of player - '+<?php echo json_encode($this->pagination->getResultsCounter()); ?>+'" style="width:250px; text-align:center;" disabled></div>' );
	});
</script>