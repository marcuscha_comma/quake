CREATE TABLE IF NOT EXISTS `#__cus_quake_club_game_records` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`created_on` DATETIME NOT NULL ,
`user_id` INT(11)  NOT NULL ,
`qcoins` DOUBLE,
`modified_on` DATETIME NOT NULL ,
`is_winner` VARCHAR(255)  NOT NULL ,
`play_times_count` DOUBLE,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

