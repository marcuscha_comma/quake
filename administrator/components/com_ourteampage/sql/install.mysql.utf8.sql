CREATE TABLE IF NOT EXISTS `#__cus_contact_persons` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`title` VARCHAR(50)  NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`position` VARCHAR(50)  NOT NULL ,
`phone` VARCHAR(50)  NOT NULL ,
`image` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

