<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Ourteampage
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_ourteampage'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Ourteampage', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('OurteampageHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'ourteampage.php');

$controller = JControllerLegacy::getInstance('Ourteampage');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
