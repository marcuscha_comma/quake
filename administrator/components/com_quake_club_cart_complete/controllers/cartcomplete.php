<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_cart_complete
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Cartcomplete controller class.
 *
 * @since  1.6
 */
class Quake_club_cart_completeControllerCartcomplete extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'cartcompletes';
		parent::__construct();
	}
}
