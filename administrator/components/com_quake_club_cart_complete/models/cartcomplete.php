<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_cart_complete
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

use \Joomla\CMS\Table\Table;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Plugin\PluginHelper;

/**
 * Quake_club_cart_complete model.
 *
 * @since  1.6
 */
class Quake_club_cart_completeModelCartcomplete extends \Joomla\CMS\MVC\Model\AdminModel
{
	public function getForm ($data = array(), $loadData = true)
	{
	
	}
}
