CREATE TABLE IF NOT EXISTS `#__cus_qperks_products` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`point` DOUBLE,
`promo` VARCHAR(255)  NOT NULL ,
`promo_qperks` DOUBLE,
`quantity` DOUBLE,
`category` VARCHAR(255)  NOT NULL ,
`max_redemption` DOUBLE,
`highlight` VARCHAR(255)  NOT NULL ,
`image1` VARCHAR(255)  NOT NULL ,
`image2` VARCHAR(255)  NOT NULL ,
`image3` VARCHAR(255)  NOT NULL ,
`image4` VARCHAR(255)  NOT NULL ,
`image5` VARCHAR(255)  NOT NULL ,
`description` TEXT NOT NULL ,
`promo_label` VARCHAR(255)  NOT NULL ,
`promo_description` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

