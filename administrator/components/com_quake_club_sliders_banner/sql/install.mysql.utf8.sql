CREATE TABLE IF NOT EXISTS `#__cus_qperks_sliders_banner` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`title` VARCHAR(100)  NOT NULL ,
`image` VARCHAR(255)  NOT NULL ,
`mobile_image` VARCHAR(255)  NOT NULL ,
`link_type` VARCHAR(255)  NOT NULL ,
`url_address` VARCHAR(255)  NOT NULL ,
`youtube_link` VARCHAR(100)  NOT NULL ,
`target_window` VARCHAR(255)  NOT NULL ,
`publish_date` DATETIME NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

