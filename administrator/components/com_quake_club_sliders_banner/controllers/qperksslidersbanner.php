<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_sliders_banner
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Qperksslidersbanner controller class.
 *
 * @since  1.6
 */
class Quake_club_sliders_bannerControllerQperksslidersbanner extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'qperksslidersbanners';
		parent::__construct();
	}
}
