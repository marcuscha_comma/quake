<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_users
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

use \Joomla\CMS\Language\Text;

/**
 * View class for a list of Quake_club_users.
 *
 * @since  1.6
 */
class Quake_club_usersViewQperksusers extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		Quake_club_usersHelper::addSubmenu('qperksusers');

		$this->addToolbar();

		// $this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = Quake_club_usersHelper::getActions();

		JToolBarHelper::title(Text::_('COM_QUAKE_CLUB_USERS_TITLE_QPERKSUSERS'), 'qperksusers.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/qperksuser';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('qperksuser.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('qperksusers.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);				
					// $bar = & JToolBar::getInstance('toolbar');
					// $bar->appendButton( 'qperksusers', 'Export', 'Export', 'index.php?option=com_quake_club_users&task=export');
					JToolBarHelper::custom('qperksusers.exportCsv', 'copy.png', 'copy_f2.png', 'Export', false);


				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('qperksuser.edit', 'JTOOLBAR_EDIT');

			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('qperksusers.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('qperksusers.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);

			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'qperksusers.delete', 'JTOOLBAR_DELETE');
				JToolBarHelper::custom('qperksusers.export', 'copy.png', 'copy_f2.png', 'Export', false);

			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('qperksusers.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('qperksusers.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'qperksusers.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('qperksusers.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_quake_club_users');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_quake_club_users&view=qperksusers');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`name`' => JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_NAME'),
			'a.`email`' => JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_EMAIL'),
			'a.`mobileNo`' => JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_mobileNo'),
			'a.`dob`' => JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_DOB'),
			'a.`gender`' => JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_GENDER'),
			'a.`race`' => JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_RACE'),
			'a.`business_card_status`' => JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_BUSINESS_CARD_STATUS'),
			'a.`block`' => JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_BLOCK'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
