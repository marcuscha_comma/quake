<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_users
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;


use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Layout\LayoutHelper;
use \Joomla\CMS\Language\Text;

HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/');
HTMLHelper::_('bootstrap.tooltip');
HTMLHelper::_('behavior.multiselect');
HTMLHelper::_('formbehavior.chosen', 'select');

JFactory::getDocument()->addScriptDeclaration('
	Joomla.submitbutton = function(pressbutton)
	{
		if (pressbutton == "qperksusers.export")
		{
			window.location = "index.php?option=com_quake_club_users&task=qperksusers.export";
		}else {
			// If not follow the normal path
			document.adminForm.task.value=pressbutton;
			Joomla.submitform(pressbutton);
		  }
		// Joomla.submitform(pressbutton);
	};
');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'administrator/components/com_quake_club_users/assets/css/quake_club_users.css');
$document->addStyleSheet(Uri::root() . 'media/com_quake_club_users/css/list.css');

$user      = Factory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_quake_club_users');
$saveOrder = $listOrder == 'a.`ordering`';

// echo "<pre>";
// print_r($this->items);
// echo "</pre>";

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_quake_club_users&task=qperksusers.saveOrderAjax&tmpl=component';
    HTMLHelper::_('sortablelist.sortable', 'qperksuserList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>

<form action="<?php echo Route::_('index.php?option=com_quake_club_users&view=qperksusers'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

            <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>
			<table class="table table-striped" id="qperksuserList">
				<thead>
				<tr>
					<?php if (isset($this->items[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone">
                            <?php echo HTMLHelper::_('searchtools.sort', '', 'a.`ordering`', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                        </th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo Text::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
					<?php if (isset($this->items[0]->state)): ?>
						
					<?php endif; ?>

									<!-- <th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_ID', 'a.`id`', $listDirn, $listOrder); ?>
				</th> -->
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_NAME', 'a.`name`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_EMAIL', 'a.`email`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_mobileNo', 'a.`mobileNo`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_POINT', 'total_point', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_DOB', 'a.`dob`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_GENDER', 'a.`gender`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_SUBSCRIBE', 'a.`unsubscribed_contact`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_RACE', 'a.`race`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_BUSINESS_CARD_STATUS', 'a.`business_card_status`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_BLOCK', 'a.`block`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_SOURCE', 'a.`source`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_QUAKE_CLUB_MEMBER', 'a.`quakeClubUser`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_QUAKE_CLUB_WAS', 'a.`whatsapp_subscriber`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_QUAKE_CLUB_INDUSTRY', 'c.`industry`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_QUAKE_CLUB_CLASSIFICATION', 'c.`classification`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_QUAKE_CLUB_USERS_QPERKSUSERS_QUAKE_CLUB_DESIGNATION', 'c.`designation`', $listDirn, $listOrder); ?>
				</th>
				

					
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$ordering   = ($listOrder == 'a.ordering');
					$canCreate  = $user->authorise('core.create', 'com_quake_club_users');
					$canEdit    = $user->authorise('core.edit', 'com_quake_club_users');
					$canCheckin = $user->authorise('core.manage', 'com_quake_club_users');
					$canChange  = $user->authorise('core.edit.state', 'com_quake_club_users');
					?>
					<tr class="row<?php echo $i % 2; ?>">

						<?php if (isset($this->items[0]->ordering)) : ?>
							<td class="order nowrap center hidden-phone">
								<?php if ($canChange) :
									$disableClassName = '';
									$disabledLabel    = '';

									if (!$saveOrder) :
										$disabledLabel    = Text::_('JORDERINGDISABLED');
										$disableClassName = 'inactive tip-top';
									endif; ?>
									<span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
										  title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
									<input type="text" style="display:none" name="order[]" size="5"
										   value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
								<?php else : ?>
									<span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
								<?php endif; ?>
							</td>
						<?php endif; ?>
						<td class="hidden-phone">
							<?php echo HTMLHelper::_('grid.id', $i, $item->id); ?>
						</td>
						<?php if (isset($this->items[0]->state)): ?>
							
						<?php endif; ?>
										<!-- <td>

					<?php echo $item->id; ?>
				</td>				 -->
				<td>
				<?php if (isset($item->checked_out) && $item->checked_out && ($canEdit || $canChange)) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'qperksusers.', $canCheckin); ?>
				<?php endif; ?>
				<?php if ($canEdit) : ?>
					<a href="<?php echo JRoute::_('index.php?option=com_quake_club_users&task=qperksuser.edit&id='.(int) $item->id); ?>">
					<?php echo $this->escape($item->name); ?></a>
				<?php else : ?>
					<?php echo $this->escape($item->name); ?>
				<?php endif; ?>

				</td>				<td>

					<?php echo $item->email; ?>
				</td>				<td>

					<?php echo $item->mobileNo; ?>
				</td>
				<td>
					<?php echo $item->total_point == "" ? 0:$item->total_point; ?>
				</td>				<td>

					<?php echo $item->dob; ?>
				</td>				<td>

					<?php echo $item->gender; ?>
				</td>
				<td>

					<?php echo $item->unsubscribed_contact; ?>
				</td>				<td>

					<?php echo $item->race; ?>
				</td>				<td>

					<?php echo $item->business_card_status; ?>
				</td>				<td>

					<?php echo $item->block; ?>
				</td>
				<td>
					<?php echo $item->source; ?>
				</td>
				<td>
					<?php echo $item->quakeClubUser; ?>
				</td>
				<td>
					<?php echo $item->whatsapp_subscriber; ?>
				</td>
				<td>
					<?php echo $item->industry; ?>
				</td>
				<td>
					<?php echo $item->classification; ?>
				</td>
				<td>
					<?php echo $item->designation; ?>
				</td>

					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo HTMLHelper::_('form.token'); ?>
		</div>
</form>
<script>
	var getResultsCounter = <?php echo json_encode($this->pagination->getResultsCounter()); ?>

    window.toggleField = function (id, task, field) {

        var f = document.adminForm, i = 0, cbx, cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = document.createElement('input');

        inputField.type  = 'hidden';
        inputField.name  = 'field';
        inputField.value = field;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };

	jQuery( document ).ready(function() {
		// console.log(countFromPhp);
		
		jQuery( ".js-stools-container-bar" ).append( '<div class="btn-wrapper input-append"><input type="text" name="filter[search]" id="filter_search" value="" class="js-stools-search-string" placeholder="'+getResultsCounter+'" style="width:170px; text-align:center;" disabled></div>' );
	});
</script>