<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_users
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_quake_club_users/css/form.css');
// echo "<pre>";
// print_r($this->userEventHistory);
// echo "</pre>";

?>
<script type="text/javascript">
	js = jQuery.noConflict();
	var user_email = <?php echo json_encode($this->item->email); ?>;
	js(document).ready(function () {
		
	});

	function sendEmail(){
		jQuery.ajax({
		url: "index.php?option=com_quake_club_users&task=qperksuser.sendEmail",
		type: 'post',
		data: {
			'email': user_email
		},
		success: function (result) {
			console.log("success");

		},
		error: function () {
			console.log('fail');
		}
	});
	}
	

	Joomla.submitbutton = function (task) {
		if (task == 'qperksuser.cancel') {
			Joomla.submitform(task, document.getElementById('qperksuser-form'));
		}
		else {
			
			if (task != 'qperksuser.cancel' && document.formvalidator.isValid(document.id('qperksuser-form'))) {
				
				Joomla.submitform(task, document.getElementById('qperksuser-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo Route::_('index.php?option=com_quake_club_users&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="qperksuser-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo HTMLHelper::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', 'Quake Club User'); ?>
		<?php JHtml::_('bootstrap.endTab'); ?>
		<?php JHtml::_('bootstrap.addTab', 'myTab', 'menu2', JText::_('COM_QUAKE_CLUB_USERS_TITLE_POINTS', true)); ?>
		
		<?php JHtml::_('bootstrap.endTab'); ?>
		<?php JHtml::_('bootstrap.addTab', 'myTab', 'menu3', JText::_('COM_QUAKE_CLUB_USERS_TITLE_REDEMPTION', true)); ?>

		<?php JHtml::_('bootstrap.endTab'); ?>
		<?php JHtml::_('bootstrap.addTab', 'myTab', 'menu4', JText::_('COM_QUAKE_CLUB_USERS_TITLE_EVENT', true)); ?>
		
		<div class="row-fluid" id="general">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[id]" value="<?php echo $this->item->username; ?>" />
				<input type="hidden" name="jform[id]" value="<?php echo $this->item->password; ?>" />
				<!-- <?php echo $this->form->renderField('username'); ?>
				<?php echo $this->form->renderField('password'); ?> -->
				<?php echo $this->form->renderField('name'); ?>
				<?php echo $this->form->renderField('email'); ?>
				<?php echo $this->form->renderField('mobileNo'); ?>
				<?php echo $this->form->renderField('dob'); ?>
				<?php echo $this->form->renderField('gender'); ?>
				<?php echo $this->form->renderField('unsubscribed_contact'); ?>
				<?php echo $this->form->renderField('race'); ?>
				<?php echo $this->form->renderField('address'); ?>
				<?php echo $this->form->renderField('facebook'); ?>
				<?php echo $this->form->renderField('business_card_status'); ?>
				<?php echo $this->form->renderField('block'); ?>
				<?php echo $this->form->renderField('whatsapp_subscriber'); ?>
				
				<!-- <div onclick="sendEmail()">The time is?</div> -->
				<div class="control-group" data-showon="[{'field':'jform[business_card_status]','values':['1'],'sign':'=','op':''}]" style="display: block;">
				<!-- <div class="control-label">
					<label id="jform_delivery_status-lbl" for="jform_delivery_status">Delivery Status</label>
				</div>
					<div class="controls">
						<fieldset id="jform_delivery_status" class="radio">
							<input type="radio" id="jform_delivery_status0" name="jform[delivery_status]" value="0">			<label for="jform_delivery_status0">No</label>
							<input type="radio" id="jform_delivery_status1" name="jform[delivery_status]" value="1">			<label for="jform_delivery_status1">Yes</label>
						</fieldset>
					</div>
				</div> -->

				<div class="control-group">
					<div class="control-label"><label id="jform_block-lbl" for="jform_block">
							Business card</label>
					</div>
					<div class="controls">
						<img style="max-width:600px;border:1px solid grey;" src="<?php echo $base_url.'.'. $this->item->business_card;?>" alt="">
					</div>
				</div>
				<div class="control-group">
					<div class="control-label"><label id="jform_comp_name-lbl" for="jform_comp_name">
						Company name</label>
					</div>
					<div class="controls"><input type="text" name="jform[comp_name]" id="jform_comp_name" value="<?php echo $this->item->comp_name;?>" disabled="" placeholder="Company name">
					</div>
				</div>
				<div class="control-group">
					<div class="control-label"><label id="jform_comp_email-lbl" for="jform_comp_email">
						Company email</label>
					</div>
					<div class="controls"><input type="text" name="jform[comp_email]" id="jform_comp_email" value="<?php echo $this->item->comp_email;?>" disabled="" placeholder="Company email">
					</div>
				</div>
				<div class="control-group">
					<div class="control-label"><label id="jform_comp_address-lbl" for="jform_comp_address">
						Company address</label>
					</div>
					<div class="controls"><textarea name="jform[comp_address]" id="jform_comp_address" disabled="" placeholder="Company address"><?php echo $this->item->comp_address;?></textarea>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label"><label id="jform_comp_mobile-lbl" for="jform_comp_mobile">
						Company mobile</label>
					</div>
					<div class="controls"><input type="text" name="jform[comp_mobile]" id="jform_comp_mobile" value="<?php echo $this->item->comp_mobile;?>" disabled="" placeholder="Company mobile">
					</div>
				</div>
				<div class="control-group">
					<div class="control-label"><label id="jform_industry-lbl" for="jform_industry">
						Industry</label>
					</div>
					<div class="controls"><input type="text" name="jform[industry]" id="jform_industry" value="<?php echo $this->item->industry;?>" disabled="" placeholder="Industry">
					</div>
				</div>
				<div class="control-group">
					<div class="control-label"><label id="jform_classification-lbl" for="jform_classification">
						Classification</label>
					</div>
					<div class="controls"><input type="text" name="jform[classification]" id="jform_classification" value="<?php echo $this->item->classification;?>" disabled="" placeholder="Classification">
					</div>
				</div>
				<div class="control-group">
					<div class="control-label"><label id="jform_designation-lbl" for="jform_designation">
					Designation</label>
					</div>
					<div class="controls"><input type="text" name="jform[designation]" id="jform_designation" value="<?php echo $this->item->designation;?>" disabled="" placeholder="Designation">
					</div>
				</div>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo HTMLHelper::_('bootstrap.endTab'); ?>

		<div id="menu2" class="tab-pane fade">
			<!-- <h3>Menu 2</h3> -->
			<table class="table table-striped" id="privacypolicyList">
				<thead>
					<tr>
						<th class="nowrap" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="up.`type`"
								data-direction="ASC" data-name="Status" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="Status">
								Type</a>
						</th>

						<th class="left" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="up.`point`"
								data-direction="ASC" data-name="ID" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="ID">
								Point</a>
						</th>
						<th class="left">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="created_date"
								data-direction="ASC" data-name="Title" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="Title">
								Date</a>
						</th>


					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="10">

							<div class="pagination pagination-toolbar clearfix">



								<input type="hidden" name="limitstart" value="0">

							</div>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($this->userPointHistory as $key => $hisory) {
						echo '<tr class="row0">';
						echo '<td>';
						if ($hisory->type == 2) {
							echo Text::_('COM_QUAKE_CLUB_QPERKS_QPERKSUSERPOINTS_TYPE_OPTION_'.$hisory->type, true) .'-' . $hisory->referred_name;
						}else{
							echo Text::_('COM_QUAKE_CLUB_QPERKS_QPERKSUSERPOINTS_TYPE_OPTION_'.$hisory->type, true);
						}
						echo '</td>';
						
						echo '<td>';
						echo $hisory->point;
						echo '</td>';

						echo '<td>';
						echo $hisory->created_date;
						echo '</td>';
						echo '</tr>';
					} ?>
					
				</tbody>
			</table>
		</div>

		<div id="menu3" class="tab-pane fade">
		<!-- <h3>Menu 3</h3> -->
			<table class="table table-striped" id="privacypolicyList">
				<thead>
					<tr>
						<th class="nowrap" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`state`"
								data-direction="ASC" data-name="Status" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="Status">
								Order number</a>
						</th>

						<th class="left" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`id`"
								data-direction="ASC" data-name="ID" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="ID">
								Status</a>
						</th>

						<th class="left" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`id`"
								data-direction="ASC" data-name="ID" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="ID">
								Product name</a>
						</th>
						<th class="left" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`id`"
								data-direction="ASC" data-name="ID" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="ID">
								Product quantity</a>
						</th>

						<th class="left" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`id`"
								data-direction="ASC" data-name="ID" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="ID">
								Promo</a>
						</th>

						<th class="left" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`id`"
								data-direction="ASC" data-name="ID" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="ID">
								Point</a>
						</th>

						<th class="left">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`title`"
								data-direction="ASC" data-name="Title" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="Title">
								Date</a>
						</th>


					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="10">

							<div class="pagination pagination-toolbar clearfix">



								<input type="hidden" name="limitstart" value="0">

							</div>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($this->userRedemptionHistory as $key => $hisory) {
						echo '<tr class="row0">';
						echo '<td>';
						echo $hisory->order_number;
						echo '</td>';

						echo '<td>';
						echo Text::_('COM_QUAKE_CLUB_REDEMPTION_QUAKECLUBREDEMPTIONS_STATUS_OPTION_'.$hisory->status, true);
						echo '</td>';
						
						echo '<td>';
						echo $hisory->product_name;
						echo '</td>';

						echo '<td>';
						echo $hisory->quantity;
						echo '</td>';

						echo '<td>';
						echo $hisory->promo?"Yes":"-";
						echo '</td>';

						echo '<td>';
						echo $hisory->promo? $hisory->promo_qperks:$hisory->point;
						echo '</td>';

						echo '<td>';
						echo $hisory->created_date;
						echo '</td>';
						echo '</tr>';
					} ?>
					
				</tbody>
			</table>
		</div>

		<div id="menu4" class="tab-pane fade">
		<!-- <h3>Menu 3</h3> -->
			<table class="table table-striped" id="privacypolicyList">
				<thead>
					<tr>
						<th class="nowrap" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`state`"
								data-direction="ASC" data-name="Status" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="Status">
								Event</a>
						</th>
						<th class="left" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`id`"
								data-direction="ASC" data-name="ID" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="ID">
								Attendance</a>
						</th>
						<th class="left" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`id`"
								data-direction="ASC" data-name="ID" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="ID">
								Redemption</a>
						</th>
						<th class="left" width="15%">
							<a href="#" onclick="return false;" class="js-stools-column-order hasPopover" data-order="a.`id`"
								data-direction="ASC" data-name="ID" title="" data-content="Select to sort by this column"
								data-placement="top" data-original-title="ID">
								Date</a>
						</th>

					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="10">

							<div class="pagination pagination-toolbar clearfix">



								<input type="hidden" name="limitstart" value="0">

							</div>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($this->userEventHistory as $key => $event) {
						echo '<tr class="row0">';
						echo '<td>';
						echo $event['name'];
						echo '</td>';
						echo '<td>';
						echo $event['is_attended']==1?"Yes":"No";
						echo '</td>';
						echo '<td>';
						echo $event['redemption_status']==1?"Yes":"No";
						echo '</td>';
						echo '<td>';
						echo $event['value'];
						echo '</td>';

						echo '</tr>';
					} ?>
					
				</tbody>
			</table>
		</div>

		<?php echo HTMLHelper::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo HTMLHelper::_('form.token'); ?>

	</div>
</form>
