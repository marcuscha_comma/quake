<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_users
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

/**
 * View to edit
 *
 * @since  1.6
 */
class Quake_club_usersViewQperksuser extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $state;

	protected $item;

	protected $form;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->item  = $this->get('Item');
		$this->form  = $this->get('Form');

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT business_card, address as comp_address, email as comp_email, mobile_no as comp_mobile, name as comp_name, industry, classification, designation FROM #__cus_qperks_company_user WHERE state > 0 and user_id ='.$this->item->id);
		$result = $db->loadRow();

		$db->setQuery('SELECT up.*, date(up.created_on) as created_date, u.name as referred_name FROM #__cus_qperks_user_point up left join #__users u on u.id = up.source WHERE user_id = '.$this->item->id.' and state > 0 order by up.ordering desc');
		$this->userPointHistory = $db->loadObjectList();

		$db->setQuery('SELECT r.*, date(r.created_on) as created_date, p.name as product_name FROM #__cus_quake_club_redemption r left join #__cus_qperks_products p on p.id = r.product_id WHERE r.user_id = '.$this->item->id.' order by r.ordering desc');
		$this->userRedemptionHistory = $db->loadObjectList();

		$db->setQuery('SELECT date(updated_at), is_attended, redemption_status FROM #__cus_rsvp_tour_data WHERE user_id = '.$this->item->id.' order by ordering desc');
		$eventResult = $db->loadRow();
		if ($eventResult!="") {
			$this->userEventHistory[] = array('name'=>'BigBigShow 2019','is_attended'=>$eventResult[1],'redemption_status'=>$eventResult[2], 'value' => $eventResult[0]);
		}

		if ($result) {
			$this->item->business_card = $result[0];
			$this->item->comp_address = $result[1];
			$this->item->comp_email = $result[2];
			$this->item->comp_mobile = $result[3];
			$this->item->comp_name = $result[4];
			$this->item->industry = $result[5];
			$this->item->classification = $result[6];
			$this->item->designation = $result[7];
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function addToolbar()
	{
		Factory::getApplication()->input->set('hidemainmenu', true);

		$user  = Factory::getUser();
		$isNew = ($this->item->id == 0);

		if (isset($this->item->checked_out))
		{
			$checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		}
		else
		{
			$checkedOut = false;
		}

		$canDo = Quake_club_usersHelper::getActions();

		JToolBarHelper::title(Text::_('COM_QUAKE_CLUB_USERS_TITLE_QPERKSUSER'), 'qperksuser.png');

		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.edit') || ($canDo->get('core.create'))))
		{
			JToolBarHelper::apply('qperksuser.apply', 'JTOOLBAR_APPLY');
			JToolBarHelper::save('qperksuser.save', 'JTOOLBAR_SAVE');
		}

		if (!$checkedOut && ($canDo->get('core.create')))
		{
			JToolBarHelper::custom('qperksuser.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
		}

		// If an existing item, can save to a copy.
		if (!$isNew && $canDo->get('core.create'))
		{
			JToolBarHelper::custom('qperksuser.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
		}

		// Button for version control
		if ($this->state->params->get('save_history', 1) && $user->authorise('core.edit')) {
			JToolbarHelper::versions('com_quake_club_users.qperksuser', $this->item->id);
		}

		if (empty($this->item->id))
		{
			JToolBarHelper::cancel('qperksuser.cancel', 'JTOOLBAR_CANCEL');
		}
		else
		{
			JToolBarHelper::cancel('qperksuser.cancel', 'JTOOLBAR_CLOSE');
		}
	}
}
