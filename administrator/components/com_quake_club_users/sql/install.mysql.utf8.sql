CREATE TABLE IF NOT EXISTS `#__cus_qperks_users` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`username` VARCHAR(255)  NOT NULL ,
`password` VARCHAR(255)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`mobileNo` VARCHAR(255)  NOT NULL ,
`dob` DATE NOT NULL ,
`gender` VARCHAR(255)  NOT NULL ,
`race` VARCHAR(255)  NOT NULL ,
`address` TEXT NOT NULL ,
`facebook` VARCHAR(255)  NOT NULL ,
`business_card_status` VARCHAR(255)  NOT NULL ,
`block` VARCHAR(255)  NOT NULL ,
`business_card` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

