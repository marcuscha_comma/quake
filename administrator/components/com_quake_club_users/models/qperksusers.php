<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_users
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Quake_club_users records.
 *
 * @since  1.6
 */
class Quake_club_usersModelQperksusers extends \Joomla\CMS\MVC\Model\ListModel
{
    
        
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'username', 'a.`username`',
				'password', 'a.`password`',
				'name', 'a.`name`',
				'email', 'a.`email`',
				'mobileNo', 'a.`mobileNo`',
				'point', 'total_point',
				'dob', 'a.`dob`',
				'gender', 'a.`gender`',
				'unsubscribed_contact', 'a.`unsubscribed_contact`',
				'race', 'a.`race`',
				'address', 'a.`address`',
				'facebook', 'a.`facebook`',
				'business_card_status', 'a.`business_card_status`',
				'block', 'a.`block`',
				'business_card', 'a.`business_card`',
				'source', 'a.`source`',
				'quakeMember', 'a.`quakeClubUser`',
				'whatsapp_subscriber', 'a.`whatsapp_subscriber`',
				'industry', 'c.`industry`',
				'classification', 'c.`classification`',
				'designation', 'c.`designation`',
			);
		}

		parent::__construct($config);
	}

    
        
    
        
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
        // List state information.
        parent::populateState("a.id", "ASC");

        $context = $this->getUserStateFromRequest($this->context . '.context', 'context', 'com_content.article', 'CMD');
        $this->setState('filter.context', $context);

        // Split context into component and optional section
        $parts = FieldsHelper::extract($context);

        if ($parts)
        {
            $this->setState('filter.component', $parts[0]);
            $this->setState('filter.section', $parts[1]);
        }
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

                
                    return parent::getStoreId($id);
                
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		$subQuery = $db->getQuery(true);

		$subQuery->select('sum(b.month01+b.month02+b.month03+b.month04+b.month05+b.month06+b.month07+b.month08+b.month09+b.month10+b.month11+b.month12) as total_point, b.user_id')
		->from('`#__cus_quake_club_qperks_monthly` as b')
		->group($db->quoteName('b.user_id'));
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.id,a.name, a.email, a.block, a.mobileNo, a.gender,a.dob,a.race,a.quakeClubUser,a.whatsapp_subscriber, a.unsubscribed_contact,a.business_card_status,a.source, c.designation, c.classification, c.industry, c.address as comp_address, c.email as comp_email, c.mobile_no as comp_mobile, c.name as comp_name, bb.total_point'
			)
		);
		$query->from('('.$subQuery.') AS bb');
		$query->join('right', $db->quoteName('#__users', 'a') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('bb.user_id') . ')');
		$query->join('inner', $db->quoteName('#__cus_qperks_company_user', 'c') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('c.user_id') . ')');
        $query->where('(a.quakeClubUser = -1 or a.quakeClubUser = 1)');

		// Filter by search in title
		$search = $this->getState('filter.search');
	
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				// $search = $db->Quote('%' . $db->escape($search, true) . '%');
				$search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
				$query->where('(a.name LIKE ' . $search . ' OR a.email LIKE ' . $search . ' OR a.mobileNo LIKE'. $search.')');
			}
		}
                

		// Filtering gender
		$filter_gender = $this->state->get("filter.gender");

		if ($filter_gender !== null && (is_numeric($filter_gender) || !empty($filter_gender)))
		{
			$query->where("a.`gender` = '".$db->escape($filter_gender)."'");
		}

		// Filtering gender
		$filter_unsubscribed_contact = $this->state->get("filter.unsubscribed_contact");

		if ($filter_unsubscribed_contact !== null && (is_numeric($filter_unsubscribed_contact) || !empty($filter_unsubscribed_contact)))
		{
			$query->where("a.`unsubscribed_contact` = '".$db->escape($filter_unsubscribed_contact)."'");
		}

		// Filtering race
		$filter_race = $this->state->get("filter.race");

		if ($filter_race !== null && (is_numeric($filter_race) || !empty($filter_race)))
		{
			$query->where("a.`race` = '".$db->escape($filter_race)."'");
		}

		// Filtering block
		$filter_block = $this->state->get("filter.block");

		if ($filter_block !== null && (is_numeric($filter_block) || !empty($filter_block)))
		{
			$query->where("a.`block` = '".$db->escape($filter_block)."'");
		}

		$filter_source = $this->state->get("filter.source");

		if ($filter_source !== null && (is_numeric($filter_source) || !empty($filter_source)))
		{
			if ($filter_source == 99) {
				$query->where("a.`source` IN (2,3,7)");				
			}else{
				$query->where("a.`source` = '".$db->escape($filter_source)."'");
			}
		}

		$filter_quakeClubUser = $this->state->get("filter.quakeClubUser");

		if ($filter_quakeClubUser !== null && (is_numeric($filter_quakeClubUser) || !empty($filter_quakeClubUser)))
		{
			$query->where("a.`quakeClubUser` = '".$db->escape($filter_quakeClubUser)."'");
		}

		$filter_industry = $this->state->get("filter.industry");

		if ($filter_industry !== null && (is_numeric($filter_industry) || !empty($filter_industry)))
		{
			$query->where("c.`industry` = '".$db->escape($filter_industry)."'");
		}

		$filter_classification = $this->state->get("filter.classification");

		if ($filter_classification !== null && (is_numeric($filter_classification) || !empty($filter_classification)))
		{
			$query->where("c.`classification` = '".$db->escape($filter_classification)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering', "a.id");
		$orderDirn = $this->state->get('list.direction', "ASC");
	
		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		$query->group('a.id');
		
		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
		
		foreach ($items as $oneItem)
		{
					$oneItem->gender = ($oneItem->gender == '') ? '' : JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_GENDER_OPTION_' . strtoupper($oneItem->gender));
					$oneItem->unsubscribed_contact = ($oneItem->unsubscribed_contact == '') ? '' : JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_SUBSCRIBE_OPTION_' . strtoupper($oneItem->unsubscribed_contact));
					$oneItem->race = ($oneItem->race == '') ? '' : JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_RACE_OPTION_' . strtoupper($oneItem->race));
					$oneItem->business_card_status = ($oneItem->business_card_status == '') ? '' : JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_BUSINESS_CARD_STATUS_OPTION_' . strtoupper($oneItem->business_card_status));
					$oneItem->block = ($oneItem->block == '') ? '' : JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_BLOCK_OPTION_' . strtoupper($oneItem->block));
					$oneItem->source = ($oneItem->source == '') ? '' : JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_SOURCE_OPTION_' . strtoupper($oneItem->source));
					$oneItem->quakeClubUser = ($oneItem->quakeClubUser == '') ? '' : JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_MEMBER_OPTION_' . strtoupper($oneItem->quakeClubUser));
					$oneItem->whatsapp_subscriber = ($oneItem->whatsapp_subscriber == '') ? '' : JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_WAS_OPTION_' . strtoupper($oneItem->whatsapp_subscriber));
		}

		return $items;
	}

	public function getCsv()
	{
		$this->populateState();
		$db  = $this->getDbo();
		$items = $db->setQuery($this->getListQuery())->loadObjectList();
		return $items;
	}

	public function getCsvArray()
	{
		$this->populateState();
		$db  = $this->getDbo();
		$items = $db->setQuery($this->getListQuery())->loadAssocList();
		return $items;
	}
}
