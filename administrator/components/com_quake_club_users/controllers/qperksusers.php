<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_users
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Session\session;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

/**
 * Qperksusers list controller class.
 *
 * @since  1.6
 */
class Quake_club_usersControllerQperksusers extends \Joomla\CMS\MVC\Controller\AdminController
{
	/**
	 * Method to clone existing Qperksusers
	 *
	 * @return void
     *
     * @throws Exception
	 */
	public function duplicate()
	{
		// Check for request forgeries
		session::checkToken() or jexit(Text::_('JINVALID_TOKEN'));

		// Get id(s)
		$pks = $this->input->post->get('cid', array(), 'array');

		try
		{
			if (empty($pks))
			{
				throw new Exception(Text::_('COM_QUAKE_CLUB_USERS_NO_ELEMENT_SELECTED'));
			}

			ArrayHelper::toInteger($pks);
			$model = $this->getModel();
			$model->duplicate($pks);
			$this->setMessage(Text::_('COM_QUAKE_CLUB_USERS_ITEMS_SUCCESS_DUPLICATED'));
		}
		catch (Exception $e)
		{
			Factory::getApplication()->enqueueMessage($e->getMessage(), 'warning');
		}

		$this->setRedirect('index.php?option=com_quake_club_users&view=qperksusers');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    Optional. Model name
	 * @param   string  $prefix  Optional. Class prefix
	 * @param   array   $config  Optional. Configuration array for model
	 *
	 * @return  object	The Model
	 *
	 * @since    1.6
	 */
	public function getModel($name = 'qperksuser', $prefix = 'Quake_club_usersModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 * @since   3.0
     *
     * @throws Exception
     */
	public function saveOrderAjax()
	{
		// Get the input
		$input = Factory::getApplication()->input;
		$pks   = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		ArrayHelper::toInteger($pks);
		ArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Close the application
		Factory::getApplication()->close();
	}

	public function export()
	{

		ob_end_clean();
		$app = JFactory::getApplication();

		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=Master_list_users.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		$model = $this->getModel('qperksusers');
		$result = $model->getCsv();
		echo "Name, Email, Mobile no, Point, Birthday, Gender, Mailchimp subscriber, Race, Business card, User status, Source, Quake Club, Whatsapp subscriber, Company name, Company email, Company mobile no, Industry, Classification, Designation \r\n";
		
		foreach ($result as $user) {
			// echo $user->email . ", " . ucwords(trim($user->name)) . "\r\n";
			echo ucwords(trim($user->name)). ", ";
			echo $user->email . ", ";
			echo $user->mobileNo . ", ";
			echo $user->total_point . ", ";
			echo $user->dob . ", ";
			if ($user->gender==1) {
				echo "Male, ";
			}elseif ($user->gender==0) {
				echo "Female, ";
			}elseif ($user->gender==null) {
				echo " , ";
			}
			if ($user->unsubscribed_contact==1) {
				echo "Yes, ";
			}elseif ($user->unsubscribed_contact==2) {
				echo "No, ";
			}elseif ($user->unsubscribed_contact==0) {
				echo "-, ";
			}
			if ($user->race!=null) {
				echo JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_RACE_OPTION_'.$user->race). ", ";
			}else{
				echo " , ";
			}

			if ($user->business_card_status==1) {
				echo "Yes, ";
			}else{
				echo "No, ";
			}
			if ($user->block==0) {
				echo "Yes, ";
			}else{
				echo "No, ";
			}
			echo JText::_('COM_QUAKE_CLUB_USERS_QPERKSUSERS_SOURCE_OPTION_'.$user->source). ", ";
			if ($user->quakeClubUser==1) {
				echo "Yes, ";
			}elseif ($user->quakeClubUser==-1) {
				echo "No, ";
			}
			if ($user->whatsapp_subscriber==1) {
				echo "Yes, ";
			}elseif ($user->whatsapp_subscriber==0) {
				echo "No, ";
			}

			echo $user->comp_name . ", ";
			echo $user->comp_email . ", ";
			echo $user->comp_mobile . ", ";
			echo $user->industry . ", ";
			echo $user->classification . ", ";
			echo $user->designation . "\r\n";
			// echo ucwords(trim($user->name)) . "\r\n";
		}

		$app->close();
		$this->setRedirect('index.php?option=com_quake_club_users&view=qperksusers');
	}

	public function exportCsv()
	{
		$this->download_send_headers("data_export_" . date("Y-m-d") . ".csv");
		$model = $this->getModel('qperksusers');
		$array = $model->getCsvArray();
		echo $this->array2csv($array);	
		die();
	}

	function array2csv(array &$array)
	{
		if (count($array) == 0) {
			return null;
		}
		ob_start();
		$df = fopen("php://output", 'w');
		fputcsv($df, array_keys(reset($array)));
		foreach ($array as $row) {
			$row['mobileNo'] = '="' . $row['mobileNo'] . '"';
			fputcsv($df, $row);
		}
		fclose($df);
		return ob_get_clean();
	}

	function download_send_headers($filename) {
		// disable caching
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");
	
		// force download  
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
	
		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");
	}
}
