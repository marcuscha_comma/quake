<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_User_notice_page
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Usernoticepage controller class.
 *
 * @since  1.6
 */
class User_notice_pageControllerUsernoticepage extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'usernoticepages';
		parent::__construct();
	}
}
