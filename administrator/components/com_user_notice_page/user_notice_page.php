<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_User_notice_page
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_user_notice_page'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('User_notice_page', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('User_notice_pageHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'user_notice_page.php');

$controller = JControllerLegacy::getInstance('User_notice_page');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
