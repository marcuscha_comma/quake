<?php
/**
 * @package    Joomla.Site
 *
 * @copyright  Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
$allowedip = [
"60.48.247.119",
// "211.24.110.111"
];

$ip = $_SERVER['REMOTE_ADDR'];

$count = 0;
foreach ($allowedip as $key => $value) {
	
	if ($ip == $value) {
		$count++;
	}
}
// echo "<pre>";
// print_r($count);
// echo "</pre>";
// die();
if (false) { ?>
		<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
  </head>
  <body>

    <div class="" style="text-align: center;">
      <img src="logo1.png" alt="Astro Quake" style="margin: 200px auto 40px; display: block;">
    </div>
    <h1 style="text-align: center; color: #E70F8A; font-size: 38px;font-family: 'Helvetica', Arial, sans-serif; margin-bottom: 10px;">STAY TUNED</h1>
    <h2 style="text-align: center; color: #000; font-size: 24px; font-family: 'Helvetica', Arial, sans-serif; font-weight: 400; margin-top: 10px;">We're upgrading our website.</h2>

  </body>
</html>
<?php }else{

define('JOOMLA_MINIMUM_PHP', '5.3.10');

if (version_compare(PHP_VERSION, JOOMLA_MINIMUM_PHP, '<'))
{
	die('Your host needs to use PHP ' . JOOMLA_MINIMUM_PHP . ' or higher to run this version of Joomla!');
}

// Saves the start time and memory usage.
$startTime = microtime(1);
$startMem  = memory_get_usage();

/**
 * Constant that is checked in included files to prevent direct access.
 * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
 */
define('_JEXEC', 1);

if (file_exists(__DIR__ . '/defines.php'))
{
	include_once __DIR__ . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	define('JPATH_BASE', __DIR__);
	require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_BASE . '/includes/framework.php';

// Set profiler start time and memory usage and mark afterLoad in the profiler.
JDEBUG ? JProfiler::getInstance('Application')->setStart($startTime, $startMem)->mark('afterLoad') : null;

// Instantiate the application.
$app = JFactory::getApplication('site');

// Execute the application.
$app->execute();
}
