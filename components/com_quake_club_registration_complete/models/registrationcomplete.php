<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_registration_complete
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use \Joomla\CMS\Factory;
use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Table\Table;

/**
 * Quake_club_registration_complete model.
 *
 * @since  1.6
 */
class Quake_club_registration_completeModelRegistrationcomplete extends \Joomla\CMS\MVC\Model\ItemModel
{

}
