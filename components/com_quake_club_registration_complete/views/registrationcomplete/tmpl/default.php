<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_registration_complete
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>

<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>

<div class="row justify-content-center">
	<div class="col-xl-6 col-lg-8 col-md-10">
		<div class="club-logo mx-auto mb-4">Quake Club</div>

		<div class="card rounded-card bg-grey mb-4 club-shadow text-center">
			<div class="card-body p-5">

				<h2 class="f-30 mb-4 mt-0">Thank you for signing up</h2>
				<p class="font-brand mb-4">You can now start collecting Q-Perks to redeem amazing rewards!</p>

				<div class="row justify-content-center">
					<div class="col-md-7">
					<?php if( ($_COOKIE['preUrl'] != "" || $_COOKIE['preUrl'] != NULL) && $_COOKIE['preUrl'] != JUri::base() ){ ?>
						<a href="<?php echo $_COOKIE['preUrl']; ?>"><button class="btn btn-pink btn-wide">Let's start</button></a>
					<?php }else{ ?>
						<a href="./login"><button class="btn btn-pink btn-wide">Let's start</button></a>
					<?php } ?>
					</div>
				</div>

			</div>
		</div>


	</div>
</div>

<img src="images/club/girl-box-02.png" class="login-girl-1 float-deco" alt="">
<img src="images/club/girl-speaker-02.png" class="login-girl-2 float-deco" alt="">
<img src="images/club/balloon-01.png" class="balloon float-deco" alt="">


<script>
	jQuery( document ).ready(function() {
		jQuery( "#user-top-banner" ).hide();
	});
</script>
