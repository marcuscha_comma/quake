<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_youtubespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<div id="app">
    
</div>

<script type="text/javascript">
    var segmentsObjFromPhp = <?php echo json_encode($this->segments); ?>;

    var app = new Vue({
        el: '#app',
        data: {
            segmentsArray: segmentsObjFromPhp,
        },
        mounted: function () {            
        },
        methods: {
        }
    })
</script>