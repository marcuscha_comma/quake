<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_youtubespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the Youtube Page Component
 *
 * @since  0.0.1
 */
class youtubespageViewyoutubepage extends JViewLegacy
{
	/**
	 * Display the Youtube Page view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		
		// Assign data to the view
		$this->msg = 'Youtube Page';

		//get list of segments
		$db_segments    = JFactory::getDBO();
		$query_segments = $db_segments->getQuery( true );
		$query_segments
			->select( 'id, title' )
			->from( $db_segments->quoteName( '#__cus_segments' ) )
			->order( 'id asc' );
		$db_segments->setQuery( $query_segments );
		$segments = $db_segments->loadObjectList();

		foreach ($articles as $key => $articlevalue) {
			$articlevalue->images = json_decode($articlevalue->images);
			$articlevalue->imagesDetails = array();
			foreach ($articlevalue->images as $imagevalue){
				$articlevalue->imagesDetails[] = $imagevalue;
			}
		}

		$this->segments = $segments;

		// Display the view
		parent::display($tpl);
	}
}
