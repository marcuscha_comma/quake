<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Digitals
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Digitals', JPATH_COMPONENT);
JLoader::register('DigitalsController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Digitals');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
