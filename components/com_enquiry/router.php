<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Enquiry
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class EnquiryRouter
 *
 */
class EnquiryRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = JComponentHelper::getComponent('com_enquiry')->params;
		$this->noIDs = (bool) $params->get('sef_ids');
		
		$tenquiries = new RouterViewConfiguration('tenquiries');
		$this->registerView($tenquiries);
			$tenquiry = new RouterViewConfiguration('tenquiry');
			$tenquiry->setKey('id')->setParent($tenquiries);
			$this->registerView($tenquiry);
			$tenquiryform = new RouterViewConfiguration('tenquiryform');
			$tenquiryform->setKey('id');
			$this->registerView($tenquiryform);

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('EnquiryRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('EnquiryHelpersEnquiry', __DIR__ . '/helpers/enquiry.php');
			$this->attachRule(new EnquiryRulesLegacy($this));
		}
	}


	
		/**
		 * Method to get the segment(s) for an tenquiry
		 *
		 * @param   string  $id     ID of the tenquiry to retrieve the segments for
		 * @param   array   $query  The request that is built right now
		 *
		 * @return  array|string  The segments of this item
		 */
		public function getTenquirySegment($id, $query)
		{
			return array((int) $id => $id);
		}
			/**
			 * Method to get the segment(s) for an tenquiryform
			 *
			 * @param   string  $id     ID of the tenquiryform to retrieve the segments for
			 * @param   array   $query  The request that is built right now
			 *
			 * @return  array|string  The segments of this item
			 */
			public function getTenquiryformSegment($id, $query)
			{
				return $this->getTenquirySegment($id, $query);
			}

	
		/**
		 * Method to get the segment(s) for an tenquiry
		 *
		 * @param   string  $segment  Segment of the tenquiry to retrieve the ID for
		 * @param   array   $query    The request that is parsed right now
		 *
		 * @return  mixed   The id of this item or false
		 */
		public function getTenquiryId($segment, $query)
		{
			return (int) $segment;
		}
			/**
			 * Method to get the segment(s) for an tenquiryform
			 *
			 * @param   string  $segment  Segment of the tenquiryform to retrieve the ID for
			 * @param   array   $query    The request that is parsed right now
			 *
			 * @return  mixed   The id of this item or false
			 */
			public function getTenquiryformId($segment, $query)
			{
				return $this->getTenquiryId($segment, $query);
			}
}
