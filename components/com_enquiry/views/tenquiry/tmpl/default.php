<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Enquiry
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
use \Joomla\CMS\HTML\HTMLHelper;

$emailStatus = JRequest::getVar('email')==""?"":JRequest::getVar('email');
$emailFromOurTeam = JRequest::getVar('contact-me')==null?'':JRequest::getVar('contact-me');

if ($emailFromOurTeam == 'su-yen_hew@astro.com.my' || $emailFromOurTeam == 'christina_tan@astro.com.my') {
	$emailFromOurTeam = array('christina_tan@astro.com.my','su-yen_hew@astro.com.my');
}elseif($emailFromOurTeam == 'hazel_lee@astro.com.my' || $emailFromOurTeam == 'jeffrey-kin-keong_woo@astro.com.my' || $emailFromOurTeam == 'linda_wong@astro.com.my' || $emailFromOurTeam == 'shahrizal_rahim@astro.com.my' || $emailFromOurTeam == 'th_chong@astro.com.my' || $emailFromOurTeam == 'yen-lee_tham@astro.com.my' || $emailFromOurTeam == 'pek-fui_wong@astro.com.my' || $emailFromOurTeam == 'see-wei_wong@astro.com.my' ){
	$emailFromOurTeam = 'mediasolutions@astro.com.my';
}

?>

<!-- <div class="tab-pills scrollable-tab">
	<div class="row">
	<div class="col"><a href="./about-quake/about" class="tab-pill">About Quake</a></div>
	<div class="col"><a href="./about-quake/our-team" class="tab-pill">Our Team</a></div>
	<div class="col"><a href="./about-quake/get-in-touch" class="tab-pill active">Get In Touch</a></div>
	</div>
</div> -->

<form id="app" class="text-center enquiry" action="<?php echo JRoute::_('index.php?option=com_enquiry&task=tenquiry.show'); ?>" method="post">
	<h2>Contact Quake</h2>
	<h5 class="section-desc">Please provide your contact details and our representative will get back to you.</h5>

	<div class="row justify-content-center">
		<div class="col-lg-6 col-md-8">
			<div class="form-group">
				<p class="form-text text-center">
					Please fill out these required * fields.
				</p>
				<!-- <input class="form-control" name="name" type="text" placeholder="Name *" required> -->
			</div>
			<h5 class="mb-3 mt-5">Contact details</h5>

			<div class="field-placey form-group">
				<input class="form-control" id="name" name="name" type="text" placeholder="Your name" required>
		    <label for="name">Name *</label>
		  </div>
			<div class="field-placey form-group">
				<input class="form-control" id="email" name="email" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="example@email.com" required>
		    <label for="email">Email address *</label>
		  </div>

		  <div class="field-placey form-group">
				<input class="form-control" id="contact" name="contact" type="tel" pattern="[0-9]{10,16}" placeholder="Contact number" required>
		    <label for="contact">Contact number *</label>
		  </div>

		  <div class="field-placey form-group">
				<input class="form-control" id="designation" name="designation" type="text" placeholder="Designation" required>
		    <label for="designation">Designation *</label>
		  </div>

		  <div class="field-placey form-group">
				<input class="form-control" id="company_name" name="company_name" type="text" placeholder="Company name" required>
		    <label for="company_name">Company's name *</label>
		  </div>

		  <div class="d-flex">
				<div class="field-placey form-group sixecaptcha-wraper required flex-grow-1">
						<input type="text" name="jform[captcha]" id="jform_captcha" autocomplete="off" placeholder="Type the code" class="form-control" required>
						<label for="jform_captcha">Type the code *</label>
				</div>
				<span class="sixecaptcha-image"><img class="border" src="/~quakesta/plugins/captcha/sixecaptcha/picture.php?t=1618286594" title="Click to refresh" style="cursor:pointer" onclick="this.src='/~quakesta/plugins/captcha/sixecaptcha/picture.php?r='+Math.random()"></span>
			</div>

			<h5 class="mb-4 mt-5">How can we help?</h5>

			<div class="mb-4">
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="general" name="general" class="custom-control-input" value="general" v-model="mediaType">
					<label class="custom-control-label" for="general">General</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="tv" class="custom-control-input" name="general" value="tv" v-model="mediaType">
					<label class="custom-control-label" for="tv">TV</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="radio" class="custom-control-input" name="general" value="radio" v-model="mediaType">
					<label class="custom-control-label" for="radio">Radio</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="digital" class="custom-control-input" name="general" value="digital" v-model="mediaType">
					<label class="custom-control-label" for="digital">Digital</label>
				</div>
			</div>


			<div class="mb-2">
				<select name="department" id="department" class="selectpicker white-dropdown" required v-model="selectedEmail" data-width="100%">
					<option value="">What is your target market?</option>
					<option v-for="item in emailList" :value="item.email">{{item.departmentName}}</option>
				</select>

			</div>

			<div class="field-placey form-group" v-if="mediaType=='tv'">
				<input class="form-control" name="interested" type="text" placeholder="Interested TV channels">
				<label for="interested_tv">Interested TV channels</label>
			</div>

			<div class="field-placey form-group" v-if="mediaType=='digital'">
				<input class="form-control" name="interested" type="text" placeholder="Interested Digital channels">
				<label for="interested_digital">Interested Digital channels</label>
			</div>
			<div  v-if="mediaType=='radio'">
				<div class="mb-2">				
					<select name="target_region" id="target_region" class="selectpicker white-dropdown" required data-width="100%">
						<option value="">Interested target region?</option>
						<option v-for="item in regionList" :value="item">{{item}}</option>
					</select>
				</div>

				<div class="field-placey form-group">
					<input class="form-control" name="interested" type="text" placeholder="Interested Radio channels">
					<label for="interested_radio">Interested Radio channels</label>
				</div>
			</div>
			
			
			<div class="field-placey form-group">
				<textarea class="form-control" rows="6" name="content" placeholder="Tell us how we can help you"></textarea>
				<label for="content">Your question or message</label>
			</div>
			<!-- <button style="width: 180px;" class="btn btn-pink mt-3" type="submit">Submit</button> -->
			<!-- <div 
				class="g-recaptcha" data-sitekey="6Ld86cIZAAAAAMey2h1xWmE4jAnDgYVqhPFZJ2-L" 
				data-callback='onSubmit' 
				data-action='submit'
				data-size="invisible">
			</div> -->
			<button style="width: 180px;" class="btn btn-pink mt-3 g-recaptcha" data-sitekey="6Ld86cIZAAAAAMey2h1xWmE4jAnDgYVqhPFZJ2-L" 
				data-callback='onSubmit' 
				data-action='submit'>Submit</button>
			<?php echo HTMLHelper::_('form.token'); ?>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">

var emailFromOurTeam = <?php echo json_encode($emailFromOurTeam); ?>;
var emailStatus = <?php echo json_encode($emailStatus); ?>;
function onSubmit(token) {
	var name = document.getElementById("name");
	var email = document.getElementById("email");
	var contact = document.getElementById("contact");
	var designation = document.getElementById("designation");
	var company_name = document.getElementById("company_name");
	var department = document.getElementById("department");
	var target_region = document.getElementById("target_region");
	if (
		name.reportValidity() &&
		email.reportValidity() &&
		contact.reportValidity() &&
		designation.reportValidity() &&
		company_name.reportValidity()
		) {
		
     document.getElementById("app").submit();
	}

   }

if (emailStatus != "" && emailStatus == "sent") {
	swal("Thank you for your message!", "Our representative will get back to you as soon as possible.", "success", {
	  button: {
	    text: "Done",
			className: "btn btn-pink",
	  },
	});
}else if( emailStatus == "fail"){
	swal("Oops!", "There's something wrong, kindly resubmit your enquiry.","error", {
	  button: {
	    text: "Try Again",
			className: "btn btn-pink",
	  },
	});
}

var app = new Vue({
	el: '#app',
	data: {
		selectedEmail: emailFromOurTeam,
		mediaType: "general",
		emailList:[
			{
				departmentName:'All segments',
				email:['mediasolutions@astro.com.my', 'Wendy_yaw@astro.com.my']
			},
			{
				departmentName:'Malay',
				email:['nicholas_teh@astro.com.my', 'mediasolutions@astro.com.my', 'Wendy_yaw@astro.com.my']
			},
			{
				departmentName:'Chinese',
				email:['christina_tan@astro.com.my', 'su-yen_hew@astro.com.my', 'mediasolutions@astro.com.my', 'Wendy_yaw@astro.com.my']
			},
			{
				departmentName:'Indian',
				email:['kavin_sadasseevan@astro.com.my', 'mediasolutions@astro.com.my', 'Wendy_yaw@astro.com.my']
			},
			{
				departmentName:'English',
				email:['sueharn_chan@astro.com.my', 'mediasolutions@astro.com.my', 'Wendy_yaw@astro.com.my']
			},
			// {
			// departmentName:'News',
			// email:'liang-soon_teh@astro.com.my'},{
			// departmentName:'GenNext',
			// email:'nicholas_teh@astro.com.my'},{
			// departmentName:'Sports',
			// email:'jeffrey-kin-keong_woo@astro.com.my'}
			],
		regionList:["Northern Region", "East Coast Region", "Central Region", "East Malaysia Region"]
	},
	mounted: function () {
	},
	updated: function () {
		jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
	},
	methods: {
		checkRecaptcha: function(){
			grecaptcha.execute();
		}

	}
})
</script>
