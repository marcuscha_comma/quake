<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Enquiry
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

/**
 * Tenquiry controller class.
 *
 * @since  1.6
 */
class EnquiryControllerTenquiry extends \Joomla\CMS\MVC\Controller\BaseController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit()
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_enquiry.edit.tenquiry.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_enquiry.edit.tenquiry.id', $editId);

		// Get the model.
		$model = $this->getModel('Tenquiry', 'EnquiryModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_enquiry&view=tenquiryform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.edit', 'com_enquiry') || $user->authorise('core.edit.state', 'com_enquiry'))
		{
			$model = $this->getModel('Tenquiry', 'EnquiryModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_enquiry.edit.tenquiry.id', null);

			// Flush the data from the session.
			$app->setUserState('com_enquiry.edit.tenquiry.data', null);

			// Redirect to the list screen.
			$this->setMessage(Text::_('COM_ENQUIRY_ITEM_SAVED_SUCCESSFULLY'));
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(Route::_('index.php?option=com_enquiry&view=tenquiries', false));
			}
			else
			{
                $this->setRedirect(Route::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			if($user->guest) {
                throw new \Exception(Text::_('JGLOBAL_YOU_MUST_LOGIN_FIRST'), 401);
            } else{
                throw new \Exception(Text::_('JERROR_ALERTNOAUTHOR'), 403);		
            }
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.delete', 'com_enquiry'))
		{
			$model = $this->getModel('Tenquiry', 'EnquiryModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_enquiry.edit.tenquiry.id', null);
                $app->setUserState('com_enquiry.edit.tenquiry.data', null);

                $app->enqueueMessage(Text::_('COM_ENQUIRY_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(Route::_('index.php?option=com_enquiry&view=tenquiries', false));
			}

			// Redirect to the list screen.
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(Route::_($item->link, false));
		}
		else
		{
			if($user->guest) {
                throw new \Exception(Text::_('JGLOBAL_YOU_MUST_LOGIN_FIRST'), 401);
            } else{
                throw new \Exception(Text::_('JERROR_ALERTNOAUTHOR'), 403);		
            }
		}
	}

	public function show2(){
		// $this->checkToken();
		// Session::checkToken() or jexit(Text::_('JINVALID_TOKEN'));
		// Session::getFormToken(true);

		$app   = Factory::getApplication();
		$session = JFactory::getSession();
		$model = $this->getModel('TenquiryForm', 'EnquiryModel');

		// Get the user data.
		$data = Factory::getApplication()->input->get('jform', array(), 'array');

		// Validate the posted data.
		$form = $model->getForm();
		$data = $model->validate($form, $data);
		$jinput = JFactory::getApplication()->input;
		$name = $jinput->get('name');
		$email = $jinput->get('email', '', 'String');
		$contact = $jinput->get('contact', '', 'String');
		$designation = $jinput->get('designation', '', 'String');
		$company_name = $jinput->get('company_name', '', 'String');
		$general = $jinput->get('general', '', 'String');
		$department = $jinput->get('department', '', 'String');
		$interested = $jinput->get('interested', '', 'String');
		$target_region = $jinput->get('target_region', '', 'String');
		$content = $jinput->get('content','', 'String');
		$ip = $_SERVER['REMOTE_ADDR'];

		$session->set('Ename', $name);
		$session->set('Eemail', $email);
		$session->set('Econtact', $contact);
		$session->set('Edesignation', $designation);
		$session->set('Ecompany_name', $company_name);
		if ($data === false) {
			// $this->setRedirect(JRoute::_('index.php?option=com_enquiry&view=tenquiry&email=fail', false));
			$app->redirect('https://quake.com.my/test-enquiry', false);


		}else{

			$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
			$recaptcha_secret = '6Ld86cIZAAAAAHggMzBIMqAKIlusvS7W4OnpyTO8';
			$recaptcha_response = $_POST['g-recaptcha-response'];

			// Make and decode POST request:
			$recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
			$recaptcha = json_decode($recaptcha);

			$enquiry = new stdClass();
			$enquiry->name = $name;
			$enquiry->email = $email;
			$enquiry->contact = $contact;
			$enquiry->designation = $designation;
			$enquiry->company_name = $company_name;
			$enquiry->general = $general;
			$enquiry->department = $department;
			$enquiry->interested = $interested;
			$enquiry->target_region = $target_region;
			$enquiry->content= $content;
			$enquiry->ip= $ip;
			$enquiry->google_score=$recaptcha->score;
			$enquiry->created_at= date('Y-m-d h:i:s');

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_enquiry', $enquiry);

			// Use JSON encoded string and converts
			// it into a PHP variable
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

				$countryList = [
					'China',
					'India',
					'Indonesia',
					'Pakistan',
					'Bangladesh',
					'Japan',
					'Philippines',
					'Vietnam',
					'Turkey',
					'Iran',
					'Thailand',
					'Myanmar',
					'South Korea',
					'Iraq',
					'Afghanistan',
					'Saudi Arabia',
					'Uzbekistan',
					'Malaysia',
					'Yemen',
					'Nepal',
					'North Korea',
					'Sri Lanka',
					'Kazakhstan',
					'Syria',
					'Cambodia',
					'Jordan',
					'Azerbaijan',
					'United Arab Emirates',
					'Tajikistan',
					'Israel',
					'Laos',
					'Lebanon',
					'Kyrgyzstan',
					'Turkmenistan',
					'Singapore',
					'Oman',
					'State of Palestine',
					'Kuwait',
					'Georgia',
					'Mongolia',
					'Armenia',
					'Qatar',
					'Bahrain',
					'Timor-Leste',
					'Cyprus',
					'Bhutan',
					'Maldives',
					'Brunei',
					'Hong Kong',
					'Taiwan'
				];

				$check = 0;
				foreach ($countryList as $key => $value) {
					if ($ipdat->geoplugin_countryName == $value) {
						$check++;
					}
				}
				if ($check) {
					$sent = $this->_sendEmail2();

				}else{
					$sent = false;
				}

			if ($sent && $recaptcha->score >= 0.5) {
				$this->setRedirect(JRoute::_('index.php?option=com_enquiry&view=tenquiry&email=sent', false));
			}else {
				$this->setRedirect(JRoute::_('index.php?option=com_enquiry&view=tenquiry&email=fail', false));
			}

		}
	}

	public function show(){
		// $this->checkToken();
		Session::checkToken() or jexit(Text::_('JINVALID_TOKEN'));
		Session::getFormToken(true);

		$jinput = JFactory::getApplication()->input;
		$name = $jinput->get('name');
		$email = $jinput->get('email', '', 'String');
		$contact = $jinput->get('contact', '', 'String');
		$designation = $jinput->get('designation', '', 'String');
		$company_name = $jinput->get('company_name', '', 'String');
		$general = $jinput->get('general', '', 'String');
		$department = $jinput->get('department', '', 'String');
		$interested = $jinput->get('interested', '', 'String');
		$target_region = $jinput->get('target_region', '', 'String');
		$content = $jinput->get('content','', 'String');
		$ip = $_SERVER['REMOTE_ADDR'];

		$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
		$recaptcha_secret = '6Ld86cIZAAAAAHggMzBIMqAKIlusvS7W4OnpyTO8';
		$recaptcha_response = $_POST['g-recaptcha-response'];

		// Make and decode POST request:
		$recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
		$recaptcha = json_decode($recaptcha);

		// echo "<pre>";
		// print_r($recaptcha);
		// echo "</pre>";
		// die();
		$enquiry = new stdClass();
		$enquiry->name = $name;
		$enquiry->email = $email;
		$enquiry->contact = $contact;
		$enquiry->designation = $designation;
		$enquiry->company_name = $company_name;
		$enquiry->general = $general;
		$enquiry->department = $department;
		$enquiry->interested = $interested;
		$enquiry->target_region = $target_region;
		$enquiry->content= $content;
		$enquiry->ip= $ip;
		$enquiry->created_at= date('Y-m-d h:i:s');

		// Insert the object into the user profile table.
		$result = JFactory::getDbo()->insertObject('#__cus_enquiry_log', $enquiry);

		// Use JSON encoded string and converts
		// it into a PHP variable
		$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

			$countryList = [
				'China',
				'India',
				'Indonesia',
				'Pakistan',
				'Bangladesh',
				'Japan',
				'Philippines',
				'Vietnam',
				'Turkey',
				'Iran',
				'Thailand',
				'Myanmar',
				'South Korea',
				'Iraq',
				'Afghanistan',
				'Saudi Arabia',
				'Uzbekistan',
				'Malaysia',
				'Yemen',
				'Nepal',
				'North Korea',
				'Sri Lanka',
				'Kazakhstan',
				'Syria',
				'Cambodia',
				'Jordan',
				'Azerbaijan',
				'United Arab Emirates',
				'Tajikistan',
				'Israel',
				'Laos',
				'Lebanon',
				'Kyrgyzstan',
				'Turkmenistan',
				'Singapore',
				'Oman',
				'State of Palestine',
				'Kuwait',
				'Georgia',
				'Mongolia',
				'Armenia',
				'Qatar',
				'Bahrain',
				'Timor-Leste',
				'Cyprus',
				'Bhutan',
				'Maldives',
				'Brunei',
				'Hong Kong',
				'Taiwan'
			];

			$check = 0;
			foreach ($countryList as $key => $value) {
				if ($ipdat->geoplugin_countryName == $value) {
					$check++;
				}
			}
			if ($check) {
				$sent = $this->_sendEmail();

			}else{
				$sent = false;
			}

		if ($sent && $recaptcha->score >= 0.5) {
			$this->setRedirect(JRoute::_('index.php?option=com_enquiry&view=enquiry&email=sent', false));
		}else {
			$this->setRedirect(JRoute::_('index.php?option=com_enquiry&view=enquiry&email=fail', false));
		}
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   array     $data                  The data to send in the email.
	 * @param   stdClass  $contact               The user information to send the email to
	 * @param   boolean   $copy_email_activated  True to send a copy of the email to the user.
	 *
	 * @return  boolean  True on success sending the email, false on failure.
	 *
	 * @since   1.6.4
	 */
	private function _sendEmail()
	{

		$config = JFactory::getConfig();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');

		$jinput = JFactory::getApplication()->input;
		$name = $jinput->get('name');
		$email = $jinput->get('email', '', 'String');
		$contact = $jinput->get('contact', '', 'String');
		$designation = $jinput->get('designation', '', 'String');
		$company_name = $jinput->get('company_name', '', 'String');
		$general = $jinput->get('general', '', 'String');
		$department = $jinput->get('department', '', 'String');
		$interested = $jinput->get('interested', '', 'String');
		$target_region = $jinput->get('target_region', '', 'String');
		$content = $jinput->get('content','', 'String');
		$departmentArray =(explode(",",$department));
		$tableHtmlStart = "<table>";
		$mName = '<tr><td>NAME:</td><td>'.$name.'</td>';
		$mEmail = '<tr><td>EMAIL:</td><td>'.$email.'</td>';
		$mContact = '<tr><td>CONTACT:</td><td>'.$contact.'</td>';
		$mDesignation = '<tr><td>DESINATION:</td><td>'.$designation.'</td>';
		$mCname = '<tr><td>COMPANY NAME:</td><td>'.$company_name.'</td>';
		$mGeneral = '<tr><td>GENERAL:</td><td>'.$general.'</td>';
		$mTargetRegion = '<tr><td>TARGET REGION:</td><td>'.$target_region.'</td>';
		$mInterested = '<tr><td>INTERESTED:</td><td>'.$interested.'</td>';
		$mContent = '<tr><td>MESSAGE:</td><td>'.$content.'</td>';
		$tableHtmlEnd = "</table>";
		$body = $tableHtmlStart . $mName . $mEmail . $mContact . $mDesignation . $mCname . $mGeneral . $mTargetRegion . $mInterested . $mContent .$tableHtmlEnd;

		// switch ($general) {
		// 	case 'general':
		// 		$to = array("mediasolutions@astro.com.my", "Wendy_yaw@astro.com.my");
		// 		break;
		// 	case 'tv':
		// 		$to = $departmentArray;
		// 		break;
		// 	case 'radio':
		// 		$to = array("jeyapuvan_somasundram@astro.com.my","mediasolutions@astro.com.my", "Wendy_yaw@astro.com.my");		
		// 		break;
		// 	case 'digital':
		// 		$to = array("jeffrey_woo@astro.com.my", "mediasolutions@astro.com.my", "Wendy_yaw@astro.com.my");		
		// 		break;
		// 	default:
		// 		$to = array("mediasolutions@astro.com.my" ,"Wendy_yaw@astro.com.my");
		// 		break;
		// }

		$to = array("mediasolutions@astro.com.my", "Wendy_yaw@astro.com.my");
		// $to = array("midoff1@gmail.com");
		// $from = array("midoff1@gmail.com", "Quake");
		$from = array($data[ 'mailfrom' ], "Quake");



		# Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject("Enquiry");
		$mailer->setBody($body);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();

		return $mailer->send();
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   array     $data                  The data to send in the email.
	 * @param   stdClass  $contact               The user information to send the email to
	 * @param   boolean   $copy_email_activated  True to send a copy of the email to the user.
	 *
	 * @return  boolean  True on success sending the email, false on failure.
	 *
	 * @since   1.6.4
	 */
	private function _sendEmail2()
	{

		$config = JFactory::getConfig();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');

		$jinput = JFactory::getApplication()->input;
		$name = $jinput->get('name');
		$email = $jinput->get('email', '', 'String');
		$contact = $jinput->get('contact', '', 'String');
		$designation = $jinput->get('designation', '', 'String');
		$company_name = $jinput->get('company_name', '', 'String');
		$general = $jinput->get('general', '', 'String');
		$department = $jinput->get('department', '', 'String');
		$interested = $jinput->get('interested', '', 'String');
		$target_region = $jinput->get('target_region', '', 'String');
		$content = $jinput->get('content','', 'String');
		$departmentArray =(explode(",",$department));
		$tableHtmlStart = "<table>";
		$mName = '<tr><td>NAME:</td><td>'.$name.'</td>';
		$mEmail = '<tr><td>EMAIL:</td><td>'.$email.'</td>';
		$mContact = '<tr><td>CONTACT:</td><td>'.$contact.'</td>';
		$mDesignation = '<tr><td>DESINATION:</td><td>'.$designation.'</td>';
		$mCname = '<tr><td>COMPANY NAME:</td><td>'.$company_name.'</td>';
		$mGeneral = '<tr><td>GENERAL:</td><td>'.$general.'</td>';
		$mTargetRegion = '<tr><td>TARGET REGION:</td><td>'.$target_region.'</td>';
		$mInterested = '<tr><td>INTERESTED:</td><td>'.$interested.'</td>';
		$mContent = '<tr><td>MESSAGE:</td><td>'.$content.'</td>';
		$tableHtmlEnd = "</table>";
		$body = $tableHtmlStart . $mName . $mEmail . $mContact . $mDesignation . $mCname . $mGeneral . $mTargetRegion . $mInterested . $mContent .$tableHtmlEnd;

		// switch ($general) {
		// 	case 'general':
		// 		$to = array("mediasolutions@astro.com.my", "Wendy_yaw@astro.com.my");
		// 		break;
		// 	case 'tv':
		// 		$to = $departmentArray;
		// 		break;
		// 	case 'radio':
		// 		$to = array("jeyapuvan_somasundram@astro.com.my","mediasolutions@astro.com.my", "Wendy_yaw@astro.com.my");		
		// 		break;
		// 	case 'digital':
		// 		$to = array("jeffrey_woo@astro.com.my", "mediasolutions@astro.com.my", "Wendy_yaw@astro.com.my");		
		// 		break;
		// 	default:
		// 		$to = array("mediasolutions@astro.com.my" ,"Wendy_yaw@astro.com.my");
		// 		break;
		// }

		// $to = array("mediasolutions@astro.com.my", "Wendy_yaw@astro.com.my");
		$to = array("midoff1@gmail.com");
		// $from = array("midoff1@gmail.com", "Quake");
		$from = array($data[ 'mailfrom' ], "Quake");



		# Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject("Enquiry");
		$mailer->setBody($body);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();

		return $mailer->send();
	}
}
