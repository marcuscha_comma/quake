<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Cus_video_gallery
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class Cus_video_galleryRouter
 *
 */
class Cus_video_galleryRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = JComponentHelper::getComponent('com_cus_video_gallery')->params;
		$this->noIDs = (bool) $params->get('sef_ids');
		
		$galleries = new RouterViewConfiguration('galleries');
		$this->registerView($galleries);
			$gallery = new RouterViewConfiguration('gallery');
			$gallery->setKey('id')->setParent($galleries);
			$this->registerView($gallery);
			$galleryform = new RouterViewConfiguration('galleryform');
			$galleryform->setKey('id');
			$this->registerView($galleryform);

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('Cus_video_galleryRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('Cus_video_galleryHelpersCus_video_gallery', __DIR__ . '/helpers/cus_video_gallery.php');
			$this->attachRule(new Cus_video_galleryRulesLegacy($this));
		}
	}


	
		/**
		 * Method to get the segment(s) for an gallery
		 *
		 * @param   string  $id     ID of the gallery to retrieve the segments for
		 * @param   array   $query  The request that is built right now
		 *
		 * @return  array|string  The segments of this item
		 */
		public function getGallerySegment($id, $query)
		{
			if (!strpos($id, ':'))
			{
				$db = Factory::getDbo();
				$dbquery = $db->getQuery(true);
				$dbquery->select($dbquery->qn('alias'))
					->from($dbquery->qn('#__cus_video_gallery'))
					->where('id = ' . $dbquery->q($id));
				$db->setQuery($dbquery);

				$id .= ':' . $db->loadResult();
			}

			if ($this->noIDs)
			{
				list($void, $segment) = explode(':', $id, 2);

				return array($void => $segment);
			}
			return array((int) $id => $id);
		}
			/**
			 * Method to get the segment(s) for an galleryform
			 *
			 * @param   string  $id     ID of the galleryform to retrieve the segments for
			 * @param   array   $query  The request that is built right now
			 *
			 * @return  array|string  The segments of this item
			 */
			public function getGalleryformSegment($id, $query)
			{
				return $this->getGallerySegment($id, $query);
			}

	
		/**
		 * Method to get the segment(s) for an gallery
		 *
		 * @param   string  $segment  Segment of the gallery to retrieve the ID for
		 * @param   array   $query    The request that is parsed right now
		 *
		 * @return  mixed   The id of this item or false
		 */
		public function getGalleryId($segment, $query)
		{
			if ($this->noIDs)
			{
				$db = JFactory::getDbo();
				$dbquery = $db->getQuery(true);
				$dbquery->select($dbquery->qn('id'))
					->from($dbquery->qn('#__cus_video_gallery'))
					->where('alias = ' . $dbquery->q($segment));
				$db->setQuery($dbquery);

				return (int) $db->loadResult();
			}
			return (int) $segment;
		}
			/**
			 * Method to get the segment(s) for an galleryform
			 *
			 * @param   string  $segment  Segment of the galleryform to retrieve the ID for
			 * @param   array   $query    The request that is parsed right now
			 *
			 * @return  mixed   The id of this item or false
			 */
			public function getGalleryformId($segment, $query)
			{
				return $this->getGalleryId($segment, $query);
			}
}
