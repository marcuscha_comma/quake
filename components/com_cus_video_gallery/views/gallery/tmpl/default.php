<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$app   = JFactory::getApplication();

// if($userId == 0){
// 	$app->redirect(JRoute::_('index.php?option=com_users&view=login', false));	
// }

$doc = JFactory::getDocument();



$fbimage = JURI::base() . $this->item->slider->slider0->image;
$doc->addCustomTag( '<meta property="og:image" content="' . $fbimage . '">' );

$recordCustomiseLinkUrl = JRoute::_('index.php?option=com_cus_video_gallery&task=gallery.recordCustomiseLink');
$earnPointUrl = JRoute::_('index.php?option=com_cus_video_gallery&task=gallery.earnPoint');


?>

<style>
	#overlay-div{
		width: 100%;
		position: absolute;
		top: 0;
		bottom: 0;
		z-index: 10;
	}
</style>
<div id="app">

	<div class="article-details quakecast-details">
		<div class="row justify-content-between">
			<div class="col-md-4 col-lg-3 col-xl-2">
				<a class="btn btn-pink d-lg-block mb-3" href="./quakecast">
					Back to Listing
				</a>
			</div>
			<div class="col-md col-xl-4 d-none d-md-block desktop-nav mb-3">
				<div class="row no-gutters justify-content-end">
					<div class="col-6">
						<a class="pagination-side prev" :href="item.pre"  v-if="item.pre !== '-' ">
							<div class="nav-label">
								<i class="fas fa-chevron-left"></i> Previous
							</div>
							<div class="related-article-title">
								{{item.pre_title}}
							</div>
						</a>
					</div>
					<div class="col-6">
						<a class="text-right pagination-side next" :href="item.next"  v-if="item.next != '-' ">
							<div class="nav-label">
								Next <i class="fas fa-chevron-right"></i>
							</div>
							<div class="related-article-title">
								{{item.next_title}}
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
				
		<div>
			<div class="demo">
				<ul id="lightSlider">
					<li v-for="slider in item.slider" style="text-align: center;"
						:data-thumb="'<?php echo JUri::root(true) .'/'; ?>'+slider.image">
		
						<a v-if="slider.type == 2" :href='slider.url' data-lity class="video-thumb">
							<img :src="'<?php echo JUri::root(true) .'/'; ?>'+slider.image" onContextMenu="return false;"/>
						</a>
		
						<a v-if="slider.type == 1" href="" data-lity
							:data-lity-target="'<?php echo JUri::root(true) .'/'; ?>'+slider.image">
							<img :src="'<?php echo JUri::root(true) .'/'; ?>'+slider.image" onContextMenu="return false;"/>
						</a>
					</li>
				</ul>
			</div>
			<div class="article-wrapper">
				<!-- <hr /> -->
		
				<div class="editor-content">
					<?php echo $this->item->content; ?>
				</div>


				<div class="related-links mt-4" v-if="item.customise_link.length!=0">
					<!-- <h4 class="section-desc">Link Button</h4> -->
					<!-- <div style="position:relative;"> -->

					<div class="row">
						<div class="col-lg-4 col-md-6 mb-3" v-for="link in item.customise_link">
							<a href="#"
							rel="noopener noreferrer" @click="checkCustomiseLink(link.link)">
								<div class="row no-gutters">
									<div class="col-md-4  col-4">
										<div class="bg-pink icon-holder"><img src="images/astro-file.png" alt="" onContextMenu="return false;"></div>
									</div>
									<div class="col-md-8 col-8">
										<div class="download-center-div">
											<h5>{{link.title}}</h5>
											<span class="download-but">View</span>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- <div v-if="loginStatus == '0'" id="overlay-div" data-toggle="modal" data-target="#downloadCenterModal">
						</div>
					</div> -->
				</div>

				<hr>


				<div class="mt-4" v-if="item.video_slider.length!=0">
					<h4 class="section-desc">QuakeCast Studio</h4>
					<!-- <div style="position:relative;"> -->

					<div class="video-slider mt-4 mb-5">
						<div class="owl-carousel owl-theme" id="quakecast-video-gallery" >
							<div class="item" v-for="(video, index) in item.video_slider">
								<a @click="earnPoint(video.url,index)" :href="video.url" data-lity class="hover-area"> <!--click to open youtube lightbox-->
									<div class="hover-overlayer">
										<div class="content-holder">
											<i class="far fa-play-circle"></i>
											{{video.caption}}
										</div>
									</div>
									<img :src="'../../'+video.image" class="placeholder" onContextMenu="return false;"/>
								</a>
							</div>
						</div>
					</div>
					<!-- <div v-if="loginStatus == '0'" id="overlay-div" data-toggle="modal" data-target="#downloadCenterModal">
						</div>
					</div> -->
				</div>
		
				<div class="clearfix"></div>

				<!-- <div class="masonry-gallery mt-4" v-if="item.images!=null">
					<h4 class="section-desc">QuakeCast Gallery</h4>
					<div style="position:relative;">

					<div class="bricklayer">
						<div class="card" v-for="image in item.images">
							<a class="hover-area" :href="'../../'+image.image" data-lity>
								<div class="hover-overlayer"></div>
							
								<img :src="'../../'+image.image" alt="">
							</a>
						</div>
					</div>
					<div v-if="loginStatus == '0'" id="overlay-div" data-toggle="modal" data-target="#downloadCenterModal">
						</div>
					</div>
				</div> -->
		
				<div class="masonry-gallery quakecast-album mt-4" v-if="item.albums.length!=0">
					<h4 class="section-desc">QuakeCast Gallery</h4>
					<!-- <div style="position:relative;"> -->

						<div class="bricklayer">
							<div class="card" v-for="album in item.albums">
								<a class="hover-area" :href="album.url">
									<div class="hover-overlayer">
										<div class="content-holder">
											{{album.title}}
										</div>
									</div>
								
									<img :src="'../../'+album.intro_image" alt="" onContextMenu="return false;" />
								</a>
							</div>
						</div>

						<!-- <div class="row small-gutters">
							<div class="col-6 col-md-4" v-for="album in item.albums">
								<div class="card">
									<a class="hover-area" :href="album.url">
										<div class="hover-overlayer">
											<div class="content-holder">
												{{album.title}}
											</div>
										</div>
										<img :src="'../../'+album.intro_image" alt="">
									</a>
								</div>
							</div>
						</div> -->
						<!-- <div v-if="loginStatus == '0'" id="overlay-div" data-toggle="modal" data-target="#downloadCenterModal">
						</div>
					</div> -->
				</div>
		
			</div>
		
		
			<div class="mobile-pagination d-lg-none">
				<div class="row">
					<div class="col" v-if="item.pre != '-' ">
						<a :href="item.pre">
							<div class="nav-label">
								<i class="fas fa-chevron-left"></i> Previous
							</div>
							<div class="related-article-title">
								{{item.pre_title}}
							</div>
						</a>
					</div>
		
					<div class="col" v-if="item.next != '-' ">
						<a class="text-right" :href="item.next">
							<div class="nav-label">
								Next <i class="fas fa-chevron-right"></i>
							</div>
							<div class="related-article-title">
								{{item.next_title}}
							</div>
						</a>
					</div>
				</div>
		
		
			</div>
		</div>
	</div>
</div>

<div id="inline" style="background:#fff" class="lity-hide">
	Inline content
</div>

<div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" hidden="true"></div>

			<div class="modal-body">
				<div>
					<button @click="closeModal" type="button" class="close font-brand" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">Close</span>
					</button>

					<div style="clear: both;"></div>
				</div>
				<div class="text-center">
					<h3 class="text-white">Login in to earn points</h3>
					<p class="text-white mb-4">Hey, we know you're excited to redeem exclusive merchandise! Log in now or sign up to earn Q-Perks when you share this article.</p>
					<a href="./login" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
					<a href="#" data-dismiss="modal" class="btn btn-white">Skip</a>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

</div></div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#lightSlider').lightSlider({
			gallery: true,
			item: 1,
			loop: true,
			slideMargin: 0,
			thumbItem: 6,
			enableTouch:true,
			enableDrag:true,
			freeMove:true,
			swipeThreshold: 40,
			autoWidth: false,
			controls: false,
			adaptiveHeight:true,
			thumbMargin: 10,
			video_slider: []
		});
	});

	var shareOrDownloadStatus = <?php echo json_encode($shareOrDownloadStatus); ?>;
	var itemFromObject = <?php echo json_encode($this->item); ?>;
	var userIdFromObject = <?php echo json_encode($userId); ?>;
	var recordCustomiseLinkUrl = <?php echo json_encode($recordCustomiseLinkUrl); ?>;
	var earnPointUrl = <?php echo json_encode($earnPointUrl); ?>;

	var app = new Vue({
				el: '#app',
				data: {
					item: itemFromObject,
					loginStatus: 0,
					user_id : 0
				},
				mounted: function () {
					this.user_id = userIdFromObject;
					if (parseInt(this.user_id) > 0) {
						this.loginStatus = 1;
					}
					// console.log(this.item.video_slider.length);
					
					// window.setInterval(() => {
					// 	this.checkVideoLog(this.item.id);
					// }, 10000);

					jQuery(this.$refs.vuemodal).on("hidden.bs.modal", this.closeModal);
					// plugin for Masonry gallery, to be run last
					setTimeout(function() { 
						var bricklayer = new Bricklayer(document.querySelector('.bricklayer'));
    			}, 100);

				},
				updated: function () {
				},
				methods: {
					goToLink: function () {
						window.location.href = '/login';
					},
					checkCustomiseLink: function (link_id) {
						_this = this;
						jQuery.ajax({
							url: recordCustomiseLinkUrl,
							type: 'post',
							data: {
								'link_id': link_id,
								'video_id': _this.item.id
							},
							success: function (result) {
								window.location.href = result;
							},
							error: function () {
								console.log('fail');
							}
						});
					},
					earnPoint: function (url, index) {
							_this = this;
							console.log("asd", index);
							console.log("url", url);

							if (_this.user_id > 0) {
								jQuery.ajax({
									url: earnPointUrl,
									type: 'post',
									data: {
										'title': _this.item.title + "-" + index
									},
									success: function (result) {

									},
									error: function () {
										console.log('fail');
									}
								});
							}
					},
					closeModal: function () {
						this.loginStatus = 1
					}
				}
			})
				

</script>
