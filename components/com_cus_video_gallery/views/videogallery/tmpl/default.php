<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$app   = JFactory::getApplication();

if($userId == 0){
	$app->redirect(JRoute::_('index.php?option=com_users&view=login', false));	
}

$doc = JFactory::getDocument();



$fbimage = JURI::base() . $this->item->slider->slider0->image;
$doc->addCustomTag( '<meta property="og:image" content="' . $fbimage . '">' );

$recordCustomiseLinkUrl = JRoute::_('index.php?option=com_cus_video_gallery&task=videogallery.recordCustomiseLink');
$recordVideoLogUrl = JRoute::_('index.php?option=com_cus_video_gallery&task=videogallery.recordVideoLog');


?>

<style>
	#overlay-div{
		width: 100%;
		position: absolute;
		top: 0;
		bottom: 0;
		z-index: 10;
	}
</style>
<div id="app">

	<div class="article-details">
		<div class="row">
			<div class="col-lg-2">
				<div class="sticky-box">
					<a id="back-article" class="btn btn-pink d-lg-block mb-3"
						href="./video">
						Back to Listing
					</a>

					
					<a v-if="item.pre !== '-' " class="text-right pagination-side prev" :href="item.pre">
						<div class="nav-label">
							<i class="fas fa-chevron-left"></i> Previous
						</div>
						<div class="related-article-title">
							{{item.pre_title}}
						</div>
					</a>

				</div>
			</div>

			<div class="col-lg-8">
				
			<div class="demo">
				<ul id="lightSlider">
					<li v-for="slider in item.slider" style="text-align: center;"
						:data-thumb="<?php echo JUri::root(true) .'/'; ?>+slider.image">

						<a v-if="slider.type == 2" :href='slider.url' data-lity>
							<img :src="<?php echo JUri::root(true) .'/'; ?>+slider.image" />
						</a>

						<a v-if="slider.type == 1" href="" data-lity
							:data-lity-target="<?php echo JUri::root(true) .'/'; ?>+slider.image">
							<img :src="<?php echo JUri::root(true) .'/'; ?>+slider.image" />
						</a>
					</li>
				</ul>
			</div>
			<div class="article-wrapper">

				<div class="date">
					{{item.start_publishing}}
				</div>

				<h2 class="title">
					{{item.title}}

					<div style="position:relative;">
						<div id="addthis_button" style="padding-top:10px;" class="addthis_inline_share_toolbox"></div>

						<div v-if="loginStatus == '0'" id="overlay-div" data-toggle="modal"
							data-target="#downloadCenterModal"></div>
					</div>

					<?php if($this->articleShared == 1){ ?>
						<div class="sharing-status">
							<i class="far fa-check-circle"></i> You have collected some Q-Perks by sharing!
						</div>
					<?php } ?>
				</h2>

				<hr />

				<div class="editor-content">
					<?php echo $this->item->content; ?>
				</div>

				<div>
					<div v-for="link in item.customise_link">
						<button @click="checkCustomiseLink(link.link)" target="_blank" rel="noopener noreferrer">{{link.title}}</button>
					</div>
				</div>

				<div class="clearfix"></div>

				<div class="mobile-pagination d-lg-none">
					<div class="row">
						<div class="col" v-if="item.pre != '-' ">
							<a :href="item.pre">
								<div class="nav-label">
									<i class="fas fa-chevron-left"></i> Previous
								</div>
								<div class="related-article-title">
									{{item.pre_title}}
								</div>
							</a>
						</div>

						<div class="col" v-if="item.next != '-' ">
							<a class="text-right" :href="item.next">
								<div class="nav-label">
									Next <i class="fas fa-chevron-right"></i>
								</div>
								<div class="related-article-title">
									{{item.next_title}}
								</div>
							</a>
						</div>
					</div>


				</div>
			</div>
		</div>

		<div class="col-lg-2 d-lg-inline-block d-none">
			<div class="sticky-box">
				
				<a v-if="item.next != '-' " class="pagination-side next" :href="item.next">
					<div class="nav-label">
						Next <i class="fas fa-chevron-right"></i>
					</div>
					<div class="related-article-title">
						{{item.next_title}}
					</div>
				</a>

			</div>
		</div>
	</div>
</div>

<div id="inline" style="background:#fff" class="lity-hide">
	Inline content
</div>

<div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" hidden="true"></div>

			<div class="modal-body">
				<div>
					<button @click="closeModal" type="button" class="close font-brand" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">Close</span>
					</button>

					<div style="clear: both;"></div>
				</div>
				<div class="text-center">
					<h3 class="text-white">Login in to earn points</h3>
					<p class="text-white mb-4">Hey, we know you're excited to redeem exclusive merchandise! Log in now or sign up to earn Q-Perks when you share this article.</p>
					<a href="#" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
					<a href="#" data-dismiss="modal" class="btn btn-white">Skip</a>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

</div></div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#lightSlider').lightSlider({
			gallery: true,
			item: 1,
			loop: true,
			slideMargin: 0,
			thumbItem: 6,
			enableTouch:true,
			enableDrag:true,
			freeMove:true,
			swipeThreshold: 40,
			autoWidth: false,
			controls: false,
			adaptiveHeight:true,
			thumbMargin: 10,

		});
	});
	var shareOrDownloadStatus = <?php echo json_encode($shareOrDownloadStatus); ?>;
	var itemFromObject = <?php echo json_encode($this->item); ?>;
	var userIdFromObject = <?php echo json_encode($userId); ?>;
	var recordCustomiseLinkUrl = <?php echo json_encode($recordCustomiseLinkUrl); ?>;
	var recordVideoLogUrl = <?php echo json_encode($recordVideoLogUrl); ?>;

	var app = new Vue({
				el: '#app',
				data: {
					item: itemFromObject,
					loginStatus: 0
				},
				mounted: function () {

					if (parseInt(this.item) > 0) {
						this.loginStatus = 1;
					}

					// window.setInterval(() => {
					// 	this.checkVideoLog(this.item.id);
					// }, 10000);

					jQuery(this.$refs.vuemodal).on("hidden.bs.modal", this.closeModal);

				},
				updated: function () {

				},
				methods: {
					goToLink: function () {
						window.location.href = '../login';
					},
					checkCustomiseLink: function (link_id) {
						_this = this;
						jQuery.ajax({
							url: recordCustomiseLinkUrl,
							type: 'post',
							data: {
								'link_id': link_id,
								'video_id': _this.item.id
							},
							success: function (result) {
								window.location.href = result;
							},
							error: function () {
								console.log('fail');
							}
						});
					},
					checkVideoLog: function (video_id) {

						jQuery.ajax({
									url: recordVideoLogUrl,
									type: 'post',
									data: {
										'video_id': video_id
										},                
										success: function (result) {

										},
										error: function () {
											console.log('fail');
										}
									});
							},
							closeModal: function () {
								this.loginStatus = 1
							}
					}
				})
</script>
