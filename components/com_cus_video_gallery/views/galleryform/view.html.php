<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Cus_video_gallery
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Cus_video_galleryViewGalleryform extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	protected $canSave;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = Factory::getApplication();
		$user = Factory::getUser();

		$this->state   = $this->get('State');
		$this->item    = $this->get('Item');
		$this->params  = $this->state->get('params');
		$this->form		= $this->get('Form');
		$input = $app->input;
		$this->albums_title = $input->get('albums', '0', 'string');
		$this->item_id = $input->get('id', 0, 'integer');

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_video_gallery where id = '.$this->item_id);
		$gallery             = $db->loadObjectList();
		
		$this->albums = json_decode($gallery[0]->albums);
		$this->albums_unique = [];
		
		$checkAlbumTitle = 0;
		foreach ($this->albums as $key => $album) {
			if ($this->albums_title == $album->album_title) {
				$checkAlbumTitle++;
			}
			$this->albums_unique[$album->title] = array(
				'title' => $album->title,
				'album_title' =>$album->album_title
			);
		}
		if ($checkAlbumTitle == 0) {
			$app->redirect(JUri::base());
			die();
		}
		$this->albums = $this->albums_unique;
		// echo "<pre>";
		// print_r();
		// echo "</pre>";

		$path = "images/video-galleries/albums/" .$this->albums_title;

		$images = JFolder::files($path);

		foreach($images as $image)
		{
			if ($image != "index.html") {
				$this->images[] = JURI::base() ."images/video-galleries/albums/" .$this->albums_title .'/'. $image;
			}
		}


		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = Factory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', Text::_('COM_CUS_VIDEO_GALLERY_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = Text::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = Text::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
