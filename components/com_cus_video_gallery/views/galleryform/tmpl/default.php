<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Cus_video_gallery
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');

$user    = Factory::getUser();
?>
<div id="app">
	
	<div class="albums-box">

		<button @click="goToLink()" class="close-box" type="button">×</button>

		<ul class="nav justify-content-center">
			<li class="nav-item" v-for="album in albums">
				<a :class="{active : album.album_title == albumTitle}" class="nav-link" :href="'/~quakesta/quakecast/albums?id='+gallery_id+'&albums='+album.album_title">{{album.title}}</a>
			</li>
		</ul>
		<div class="albums-area masonry-gallery">
			<div class="bricklayer">
				<div class="card" v-for="image in images">
					<img :src="image" class="card-img-top" data-lity onContextMenu="return false;" />
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var albumsFromObject = <?php echo json_encode($this->albums); ?>;
	var imagesFromObject = <?php echo json_encode($this->images); ?>;
	var itemIdFromObject = <?php echo json_encode($this->item_id); ?>;
	var albumTitleFromObject = <?php echo json_encode($this->albums_title); ?>;

	

	jQuery( document ).ready(function() {
		// jQuery('.content .container').addClass('container-fluid');
		// jQuery('.content .container').removeClass('container');
		// jQuery('body').addClass('cny-body');
		jQuery(".btm-footer").hide();
		// document.title = 'bangdudu2019';
	});

	var app = new Vue({
				el: '#app',
				data: {
					albums: [],
					images: [],
					gallery_id: 0,
					albumTitle: "",
					albumsUnique: []
				},
				mounted: function () {
					this.images = imagesFromObject;
					this.albums = albumsFromObject;
					this.gallery_id = itemIdFromObject;
					this.albumTitle = albumTitleFromObject;
				},
				updated: function () {
					setTimeout(function() { 
						var bricklayer = new Bricklayer(document.querySelector('.bricklayer'));
    			}, 500);
				},
				methods: {
					goToLink: function () {
						window.location.href = '../quakecast/gallery/'+this.gallery_id;
					}
				}
			})
</script>