<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_programmeupdatespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;

$user       = JFactory::getUser();
$userId     = $user->get('id');
$document = Factory::getDocument();

$document->addStyleSheet(Uri::root() . 'components/com_cus_video_gallery/views/galleries/tmpl/dist/css/lightgallery.css');

// swal2 and owl carousel
$document->addScript(Uri::root() . 'components/com_cus_video_gallery/views/galleries/tmpl/dist/js/lightgallery.min.js', 'text/javascript');
// $document->addScript(Uri::root() . 'components/com_cus_video_gallery/views/galleries/tmpl/dist/js/lg-thumbnail.min.js', 'text/javascript');
// $document->addScript(Uri::root() . 'components/com_cus_video_gallery/views/galleries/tmpl/dist/js/lg-fullscreen.min.js', 'text/javascript');

//get all slider
$db_cus_sliders_banner    = JFactory::getDBO();
$query_cus_sliders_banner = $db_cus_sliders_banner->getQuery( true );
$query_cus_sliders_banner
  ->select( '*' )
  ->from( $db_cus_sliders_banner->quoteName( '#__cus_sliders_banner_vg' ) )
  ->where($db_cus_sliders_banner->quoteName('state')." > 0")
  ->order( 'ordering asc' );
$db_cus_sliders_banner->setQuery( $query_cus_sliders_banner );
$cus_sliders_banner = $db_cus_sliders_banner->loadObjectList();



?>
<div id="app">
  <!-- <div class="demo" id="lightgallery">
    <ul id="lightSlider">
        <li style="text-align: center;" data-thumb="images/2020/1--EDM.jpg">
        <a href="images/2020/1--EDM.jpg">
          <img src="images/2020/1--EDM.jpg" />
        </a>
        </li>
        <li style="text-align: center;" data-thumb="images/2020/1--Exorcist-TVB_WhatsOn.jpg">
        <a href="images/2020/1--Exorcist-TVB_WhatsOn.jpg">
          <img src="images/2020/1--Exorcist-TVB_WhatsOn.jpg" />
        </a>
        </li>
    </ul>
  </div> -->

    <!-- <div id="aniimated-thumbnials">
      <a href="images/2020/1--EDM.jpg">
        <img src="images/2020/1--EDM.jpg" />
      </a>
      <a href="img/img2.jpg">
        <img src="images/2020/1--EDM.jpg" />
      </a>
    </div> -->

        <!-- <div class="d-none d-md-block">
          <div class="filter-options">
          <label class="form-label">Filter By</label>
             <select class="selectpicker" v-model="selectedYear" @change="selectOnChanged" data-width="fit">
              <option value="0">All Year</option>
              <option v-for="n in currentYear" v-if="n>=2016" :value="n">{{n}}</option>
            </select> 
            
            <select class="selectpicker" v-model="selectedCategory" @change="selectOnChanged" data-width="fit">
              <option value="0">All Category</option>
              <option v-for="category in categoriesArray" :value="category.id">{{category.title}}</option>
            </select>
          </div>
        </div> -->
        

        <!-- <div class="mobile-filter d-block d-sm-block d-md-none">
          <div id="filter-trigger" class="btn btn-grey btn-block">
            Filter By
          </div>

          <div class="mobile-filter-container">
             <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Year</label>
              </div>
              <div class="col-7">
                <select class="selectpicker" v-model="selectedYear" @change="selectOnChanged" data-mobile="true">
                  <option value="0">All Year</option>
                  <option v-for="n in currentYear" v-if="n>=2016" :value="n">{{n}}</option>
                </select>
              </div>
            </div> 

            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Category</label>
              </div>
              <div class="col-7">
                <select class="selectpicker" v-model="selectedCategory" @change="selectOnChanged" data-mobile="true">
                  <option value="0">All Category</option>
                  <option v-for="category in categoriesArray" :value="category.id">{{category.title}}</option>
                </select>
              </div>
            </div>



            <div id="filter-close" class="btn btn-pink">
              Done
            </div>
          </div>
        </div> -->

        <div>
          <div class="row" v-show="showStatus">
            <div class="col-md-4 col-sm-6 article article-listing" v-for="(article, index) in articlesArray" v-if="(index+1) <= loadMoreNumber">
              <div class="hover-area pointer" @click="goToLink(article.url);">
                <div class="pointer img-holder" :style="{ 'background-image': 'url(' + article.intro_image + ')' }">

                  <img class="placeholder" src="images/2020/card-guide.png" alt="">
                  <div class="overlayer">

                  </div>
                  
                </div>
                <div class="article-date">{{article.convDate}}</div>
                <div class="article-title pointer">{{article.title}}</div>
                <!-- <div class="article-date mt-3">Client</div> -->
                <div class="article-date">{{article.client}}</div>
                <div class="article-link mt-3" >Catch up now</div>
              </div>
            </div>
          </div>
          <div v-show="!showStatus" style="text-align:center;">
                <h5 class="section-desc text-muted">No results have been found.</h5>
            </div>
          <div class="loading-bar" v-if="loadMoreNumber < articlesArray.length">
            <div @click="loadMore(loadMoreNumber)">Load More</div>
          </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var articlesObjFromPhp = <?php echo json_encode($this->items); ?>;
    var categoriesObjFromPhp = <?php echo json_encode($this->categories); ?>;
    var userId = <?php echo json_encode($userId); ?>;
    var cus_sliders_banner = <?php echo json_encode($cus_sliders_banner); ?>;

    jQuery(document).ready(function() {
		jQuery('#lightSlider').lightSlider({
			gallery: true,
			item: 1,
			loop: true,
			slideMargin: 0,
			thumbItem: 6,
			enableTouch:true,
			enableDrag:true,
			freeMove:true,
			swipeThreshold: 40,
			autoWidth: false,
			controls: false,
			adaptiveHeight:true,
      thumbMargin: 10,
		});
	});

    var app = new Vue({
        el: '#app',
        data: {
            articlesArray: articlesObjFromPhp,
            categoriesArray: categoriesObjFromPhp,
            selectedYear: 0,
            selectedCategory: 0,
            currentYear: new Date().getFullYear(),
            loadMoreNumber: 6,
            showStatus: true,
            userId : 0,
            sliders_banner: []
        },
        mounted: function () {
            this.selectOnChanged();
            this.userId = userId;
            lightGallery(document.getElementById('lightgallery'), {
                selector: 'a' 
            });
            this.sliders_banner = cus_sliders_banner;
        },
        updated: function () {
            jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
            selectOnChanged :function(){
                _this = this;
                _this.articlesArray = articlesObjFromPhp;

                if (_this.selectedCategory != 0) {
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                      if (article.category != "") {
                        if (article.category.length) {
                          article.categoryArray = article.category.split(",");

                          return article.categoryArray.filter(function(category){
                          return category == _this.selectedCategory;
                          }).length > 0;
                        }
                      } 
                    });
                }

                // if (_this.selectedYear != 0) {
                //     _this.articlesArray = _this.articlesArray.filter(function(article){
                //         return article.year == _this.selectedYear;
                //     });
                // }
                if (_this.articlesArray.length > 0) {
                    _this.showStatus = true;
                }else{
                    _this.showStatus = false;
                }
            },
            loadMore :function(loadMoreNumber){
                this.loadMoreNumber = loadMoreNumber +6;
            },
            goToLink :function(link){
                window.location.href = link;
            }
        }
    })

    var app = new Vue({
        el: '#vg-banner',
        data: {
            sliders_banner: [],
            userId : 0
        },
        mounted: function () {
            this.userId = userId;
            this.sliders_banner = cus_sliders_banner;
        },
        updated: function () {
        },
        methods: {
        }
    })
</script>
