<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Cus_video_gallery
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use \Joomla\CMS\Factory;
use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Table\Table;
use \Joomla\CMS\Helper\TagsHelper;

/**
 * Cus_video_gallery model.
 *
 * @since  1.6
 */
class Cus_video_galleryModelGallery extends \Joomla\CMS\MVC\Model\ItemModel
{
    public $_item;

        
    
        
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return void
	 *
	 * @since    1.6
	 *
     * @throws Exception
	 */
	protected function populateState()
	{
		$app  = Factory::getApplication('com_cus_video_gallery');
		$user = Factory::getUser();

		// Check published state
		if ((!$user->authorise('core.edit.state', 'com_cus_video_gallery')) && (!$user->authorise('core.edit', 'com_cus_video_gallery')))
		{
			$this->setState('filter.published', 1);
			$this->setState('filter.archived', 2);
		}

		// Load state from the request userState on edit or from the passed variable on default
		if (Factory::getApplication()->input->get('layout') == 'edit')
		{
			$id = Factory::getApplication()->getUserState('com_cus_video_gallery.edit.gallery.id');
		}
		else
		{
			$id = Factory::getApplication()->input->get('id');
			Factory::getApplication()->setUserState('com_cus_video_gallery.edit.gallery.id', $id);
		}

		$this->setState('gallery.id', $id);

		// Load the parameters.
		$params       = $app->getParams();
		$params_array = $params->toArray();

		if (isset($params_array['item_id']))
		{
			$this->setState('gallery.id', $params_array['item_id']);
		}

		$this->setState('params', $params);
	}

	/**
	 * Method to get an object.
	 *
	 * @param   integer $id The id of the object to get.
	 *
	 * @return  mixed    Object on success, false on failure.
     *
     * @throws Exception
	 */
	public function getItem($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id))
			{
				$id = $this->getState('gallery.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load($id))
			{
				if(empty($result) || $this->isAdminOrSuperUser() || $table->created_by == JFactory::getUser()->id){

				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if (isset($table->state) && $table->state != $published)
					{
						throw new Exception(Text::_('COM_CUS_VIDEO_GALLERY_ITEM_NOT_LOADED'), 403);
					}
				}

				// Convert the JTable to a clean JObject.
				$properties  = $table->getProperties(1);
				$this->_item = ArrayHelper::toObject($properties, 'JObject');

				} else {
											throw new Exception(JText::_("JERROR_ALERTNOAUTHOR"), 401);
									  }
			}

			if (empty($this->_item))
			{
				throw new Exception(Text::_('COM_CUS_VIDEO_GALLERY_ITEM_NOT_LOADED'), 404);
			}
		}
	
		

	if (isset($this->_item->created_by))
	{
		$this->_item->created_by_name = JFactory::getUser($this->_item->created_by)->name;
	}

	if (isset($this->_item->modified_by))
	{
		$this->_item->modified_by_name = JFactory::getUser($this->_item->modified_by)->name;
	}

	if (isset($this->_item->start_publishing))
	{
		$this->_item->start_publishing = date("M d,Y",strtotime($this->_item->start_publishing));
	}

	if (isset($this->_item->publish_date))
	{
		$this->_item->publish_date = date("M d,Y",strtotime($this->_item->publish_date));
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		$db->setQuery('SELECT id,title FROM #__cus_video_gallery where state > 0 and id >'. $this->_item->id. ' ORDER BY id LIMIT 1');
		$next_video             = $db->loadObjectList();

		$db->setQuery('SELECT id,title FROM #__cus_video_gallery where state > 0 and id<'.$this->_item->id. ' ORDER BY id DESC LIMIT 1');
		$pre_video             = $db->loadObjectList();

		// echo "<pre>";
		// print_r( $next_video );
		// echo "</pre>";
		// die();

		if ($next_video) {
			// $this->_item->next = JRoute::_('index.php?option=com_cus_video_gallery&view=gallery&id='.(int) $next_video[0]->id);
			$this->_item->next = JURI::base() .'quakecast/gallery/'. $next_video[0]->id;
			// $this->_item->next = './webinar/gallery/' . $next_video[0]->id;
			
			$this->_item->next_title = $next_video[0]->title;
		}else{
			$this->_item->next = "-";
			$this->_item->next_titles = "-";
		}

		if ($pre_video) {
			$this->_item->pre = JURI::base() .'quakecast/gallery/'. $pre_video[0]->id;
			// $this->_item->pre = './webinar/gallery/' . $pre_video[0]->id;

			$this->_item->pre_title = $pre_video[0]->title;
		}else{
			$this->_item->pre = "-";
			$this->_item->pre_title = "-";
		}
	}
		return $this->_item;
	}

	/**
	 * Get an instance of JTable class
	 *
	 * @param   string $type   Name of the JTable class to get an instance of.
	 * @param   string $prefix Prefix for the table class name. Optional.
	 * @param   array  $config Array of configuration values for the JTable object. Optional.
	 *
	 * @return  JTable|bool JTable if success, false on failure.
	 */
	public function getTable($type = 'Gallery', $prefix = 'Cus_video_galleryTable', $config = array())
	{
		$this->addTablePath(JPATH_ADMINISTRATOR . '/components/com_cus_video_gallery/tables');

		return Table::getInstance($type, $prefix, $config);
	}

	/**
	 * Get the id of an item by alias
	 *
	 * @param   string $alias Item alias
	 *
	 * @return  mixed
	 */
	public function getItemIdByAlias($alias)
	{
            $table      = $this->getTable();
            $properties = $table->getProperties();
            $result     = null;
            $aliasKey   = null;
            if (method_exists($this, 'getAliasFieldNameByView'))
            {
            	$aliasKey   = $this->getAliasFieldNameByView('gallery');
            }
            

            if (key_exists('alias', $properties))
            {
                $table->load(array('alias' => $alias));
                $result = $table->id;
            }
            elseif (isset($aliasKey) && key_exists($aliasKey, $properties))
            {
                $table->load(array($aliasKey => $alias));
                $result = $table->id;
            }
            
                return $result;
            
	}

	/**
	 * Method to check in an item.
	 *
	 * @param   integer $id The id of the row to check out.
	 *
	 * @return  boolean True on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int) $this->getState('gallery.id');
                
		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
			if (method_exists($table, 'checkin'))
			{
				if (!$table->checkin($id))
				{
					return false;
				}
			}
		}

		return true;
                
	}

	/**
	 * Method to check out an item for editing.
	 *
	 * @param   integer $id The id of the row to check out.
	 *
	 * @return  boolean True on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int) $this->getState('gallery.id');

                
		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = Factory::getUser();

			// Attempt to check the row out.
			if (method_exists($table, 'checkout'))
			{
				if (!$table->checkout($user->get('id'), $id))
				{
					return false;
				}
			}
		}

		return true;
                
	}

	/**
	 * Publish the element
	 *
	 * @param   int $id    Item id
	 * @param   int $state Publish state
	 *
	 * @return  boolean
	 */
	public function publish($id, $state)
	{
		$table = $this->getTable();
                
		$table->load($id);
		$table->state = $state;

		return $table->store();
                
	}

	/**
	 * Method to delete an item
	 *
	 * @param   int $id Element id
	 *
	 * @return  bool
	 */
	public function delete($id)
	{
		$table = $this->getTable();

                
                    return $table->delete($id);
                
	}

	public function getAliasFieldNameByView($view)
	{
		switch ($view)
		{
			case 'gallery':
			case 'galleryalbum':
				return 'alias';
			break;
		}
	}
}
