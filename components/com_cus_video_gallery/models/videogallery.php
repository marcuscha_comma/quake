<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Cus_video_gallery
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use \Joomla\CMS\Factory;
use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Table\Table;

/**
 * Cus_video_gallery model.
 *
 * @since  1.6
 */
class Cus_video_galleryModelVideogallery extends \Joomla\CMS\MVC\Model\ItemModel
{
    public $_item;

        
       /**
        * Checks whether or not a user is manager or super user
        *
        * @return bool
        */
        public function isAdminOrSuperUser()
        {
            try{
                $user = JFactory::getUser();
                return in_array("8", $user->groups) || in_array("7", $user->groups);
            }catch(Exception $exc){
                return false;
            }
        }
    
        
        /**
         * This method revises if the $id of the item belongs to the current user
         * @param   integer     $id     The id of the item
         * @return  boolean             true if the user is the owner of the row, false if not.
         *
         */
        public function userIDItem($id){
            try{
                $user = JFactory::getUser();
                $db   = JFactory::getDbo();

                $query = $db->getQuery(true);
                $query->select("id")
                      ->from($db->quoteName('#__cus_video_gallery_customise_link'))
                      ->where("id = " . $db->escape($id))
                      ->where("created_by = " . $user->id);

                $db->setQuery($query);

                $results = $db->loadObject();
                if ($results){
                    return true;
                }else{
                    return false;
                }
            }catch(Exception $exc){
                return false;
            }
        }
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return void
	 *
	 * @since    1.6
	 *
     * @throws Exception
	 */
	protected function populateState()
	{
		$app  = Factory::getApplication('com_cus_video_gallery');
		$user = Factory::getUser();

		// Check published state
		if ((!$user->authorise('core.edit.state', 'com_cus_video_gallery')) && (!$user->authorise('core.edit', 'com_cus_video_gallery')))
		{
			$this->setState('filter.published', 1);
			$this->setState('filter.archived', 2);
		}

		// Load state from the request userState on edit or from the passed variable on default
		if (Factory::getApplication()->input->get('layout') == 'edit')
		{
			$id = Factory::getApplication()->getUserState('com_cus_video_gallery.edit.videogallery.id');
		}
		else
		{
			$id = Factory::getApplication()->input->get('id');
			Factory::getApplication()->setUserState('com_cus_video_gallery.edit.videogallery.id', $id);
		}

		$this->setState('videogallery.id', $id);

		// Load the parameters.
		$params       = $app->getParams();
		$params_array = $params->toArray();

		if (isset($params_array['item_id']))
		{
			$this->setState('videogallery.id', $params_array['item_id']);
		}

		$this->setState('params', $params);
	}

	/**
	 * Method to get an object.
	 *
	 * @param   integer $id The id of the object to get.
	 *
	 * @return  mixed    Object on success, false on failure.
     *
     * @throws Exception
	 */
	public function getItem($id = null)
	{
            if ($this->_item === null)
            {
                $this->_item = false;

                if (empty($id))
                {
                    $id = $this->getState('videogallery.id');
                }

                // Get a level row instance.
                $table = $this->getTable();

                // Attempt to load the row.
                if ($table->load($id))
                {
                    if(empty($result) || $this->isAdminOrSuperUser() || $table->created_by == JFactory::getUser()->id){

                    // Check published state.
                    if ($published = $this->getState('filter.published'))
                    {
                        if (isset($table->state) && $table->state != $published)
                        {
                            throw new Exception(Text::_('COM_CUS_VIDEO_GALLERY_ITEM_NOT_LOADED'), 403);
                        }
                    }

                    // Convert the JTable to a clean JObject.
                    $properties  = $table->getProperties(1);
                    $this->_item = ArrayHelper::toObject($properties, 'JObject');

                    } else {
                                                throw new Exception(JText::_("JERROR_ALERTNOAUTHOR"), 401);
                                          }
                }

                if (empty($this->_item))
				{
					throw new Exception(Text::_('COM_CUS_VIDEO_GALLERY_ITEM_NOT_LOADED'), 404);
				}
            }
        
            

		if (isset($this->_item->created_by))
		{
			$this->_item->created_by_name = JFactory::getUser($this->_item->created_by)->name;
		}

		if (isset($this->_item->modified_by))
		{
			$this->_item->modified_by_name = JFactory::getUser($this->_item->modified_by)->name;
		}

		if (isset($this->_item->start_publishing))
		{
			$this->_item->start_publishing = date("M d,Y",strtotime($this->_item->start_publishing));
		}

		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		$db->setQuery('SELECT id,title FROM #__cus_video_gallery where id='.((int)$this->_item->id+1));
		$next_video             = $db->loadObjectList();

		$db->setQuery('SELECT id,title FROM #__cus_video_gallery where id='.((int)$this->_item->id-1));
		$pre_video             = $db->loadObjectList();

		if ($next_video) {
			$this->_item->next = JRoute::_('index.php?option=com_cus_video_gallery&view=videogallery&id='.(int) $next_video[0]->id);
			$this->_item->next_title = $next_video[0]->title;
		}else{
			$this->_item->next = "-";
			$this->_item->next_titles = "-";
		}

		if ($pre_video) {
			$this->_item->pre = JRoute::_('index.php?option=com_cus_video_gallery&view=videogallery&id='.(int) $pre_video[0]->id);
			$this->_item->pre_title = $pre_video[0]->title;
		}else{
			$this->_item->pre = "-";
			$this->_item->pre_title = "-";
		}

            return $this->_item;
        }

	/**
	 * Get an instance of JTable class
	 *
	 * @param   string $type   Name of the JTable class to get an instance of.
	 * @param   string $prefix Prefix for the table class name. Optional.
	 * @param   array  $config Array of configuration values for the JTable object. Optional.
	 *
	 * @return  JTable|bool JTable if success, false on failure.
	 */
	public function getTable($type = 'Videogallery', $prefix = 'Cus_video_galleryTable', $config = array())
	{
		$this->addTablePath(JPATH_ADMINISTRATOR . '/components/com_cus_video_gallery/tables');

		return Table::getInstance($type, $prefix, $config);
	}

	/**
	 * Get the id of an item by alias
	 *
	 * @param   string $alias Item alias
	 *
	 * @return  mixed
	 */
	public function getItemIdByAlias($alias)
	{
            $table      = $this->getTable();
            $properties = $table->getProperties();
            $result     = null;
            $aliasKey   = null;
            if (method_exists($this, 'getAliasFieldNameByView'))
            {
            	$aliasKey   = $this->getAliasFieldNameByView('videogallery');
            }
            

            if (key_exists('alias', $properties))
            {
                $table->load(array('alias' => $alias));
                $result = $table->id;
            }
            elseif (isset($aliasKey) && key_exists($aliasKey, $properties))
            {
                $table->load(array($aliasKey => $alias));
                $result = $table->id;
            }
            if(empty($result) || $this->isAdminOrSuperUser() || $table->created_by == JFactory::getUser()->id){
                return $result;
            } else {
                                                throw new Exception(JText::_("JERROR_ALERTNOAUTHOR"), 401);
                                          }
	}

	/**
	 * Method to check in an item.
	 *
	 * @param   integer $id The id of the row to check out.
	 *
	 * @return  boolean True on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int) $this->getState('videogallery.id');
                if(!$id || $this->userIDItem($id) || $this->isAdminOrSuperUser()){
		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
			if (method_exists($table, 'checkin'))
			{
				if (!$table->checkin($id))
				{
					return false;
				}
			}
		}

		return true;
                }else{
                               throw new Exception(JText::_("JERROR_ALERTNOAUTHOR"), 401);
                           }
	}

	/**
	 * Method to check out an item for editing.
	 *
	 * @param   integer $id The id of the row to check out.
	 *
	 * @return  boolean True on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int) $this->getState('videogallery.id');

                if(!$id || $this->userIDItem($id) || $this->isAdminOrSuperUser()){
		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = Factory::getUser();

			// Attempt to check the row out.
			if (method_exists($table, 'checkout'))
			{
				if (!$table->checkout($user->get('id'), $id))
				{
					return false;
				}
			}
		}

		return true;
                }else{
                               throw new Exception(JText::_("JERROR_ALERTNOAUTHOR"), 401);
                           }
	}

	/**
	 * Publish the element
	 *
	 * @param   int $id    Item id
	 * @param   int $state Publish state
	 *
	 * @return  boolean
	 */
	public function publish($id, $state)
	{
		$table = $this->getTable();
                if(!$id || $this->userIDItem($id) || $this->isAdminOrSuperUser()){
		$table->load($id);
		$table->state = $state;

		return $table->store();
                }else{
                               throw new Exception(JText::_("JERROR_ALERTNOAUTHOR"), 401);
                           }
	}

	/**
	 * Method to delete an item
	 *
	 * @param   int $id Element id
	 *
	 * @return  bool
	 */
	public function delete($id)
	{
		$table = $this->getTable();

                if(empty($result) || $this->isAdminOrSuperUser() || $table->created_by == JFactory::getUser()->id){
                    return $table->delete($id);
                } else {
                                                throw new Exception(JText::_("JERROR_ALERTNOAUTHOR"), 401);
                                          }
	}

	
}
