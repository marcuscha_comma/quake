<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Cus_video_gallery
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

/**
 * Gallery controller class.
 *
 * @since  1.6
 */
class Cus_video_galleryControllerGallery extends \Joomla\CMS\MVC\Controller\BaseController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit()
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_cus_video_gallery.edit.gallery.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_cus_video_gallery.edit.gallery.id', $editId);

		// Get the model.
		$model = $this->getModel('Gallery', 'Cus_video_galleryModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_cus_video_gallery&view=galleryform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.edit', 'com_cus_video_gallery') || $user->authorise('core.edit.state', 'com_cus_video_gallery'))
		{
			$model = $this->getModel('Gallery', 'Cus_video_galleryModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_cus_video_gallery.edit.gallery.id', null);

			// Flush the data from the session.
			$app->setUserState('com_cus_video_gallery.edit.gallery.data', null);

			// Redirect to the list screen.
			$this->setMessage(Text::_('COM_CUS_VIDEO_GALLERY_ITEM_SAVED_SUCCESSFULLY'));
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(Route::_('index.php?option=com_cus_video_gallery&view=galleries', false));
			}
			else
			{
                $this->setRedirect(Route::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.delete', 'com_cus_video_gallery'))
		{
			$model = $this->getModel('Gallery', 'Cus_video_galleryModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_cus_video_gallery.edit.gallery.id', null);
                $app->setUserState('com_cus_video_gallery.edit.gallery.data', null);

                $app->enqueueMessage(Text::_('COM_CUS_VIDEO_GALLERY_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(Route::_('index.php?option=com_cus_video_gallery&view=galleries', false));
			}

			// Redirect to the list screen.
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(Route::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function recordCustomiseLink(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$user_id     = $user->get('id');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		$jinput = JFactory::getApplication()->input;
		$link_id = $jinput->get('link_id','', 'String');
		$video_id = $jinput->get('video_id','', 'String');

		$log = new stdClass();
		$log->user_id = $user_id;
		$log->video_id = $video_id;
		$log->link_id = $link_id;
		$log->created_at = date('Y-m-d h:i:s');

		JFactory::getDbo()->insertObject('#__cus_video_gallery_customise_link_log', $log);

		$db->setQuery('SELECT link FROM #__cus_video_gallery_customise_link where id='.$link_id);
		$customise_link             = $db->loadResult();

		if ($customise_link) {
			echo $customise_link;
		}else{
			echo false;
		}

		JFactory::getApplication()->close();
	}

	public function checkIsView(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$userId     = $user->get('id');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		$jinput = $app->input;
		$title = $jinput->get('title','', 'String');

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT count(*) as source FROM #__cus_qperks_user_point WHERE type = 16 and state > 0 and user_id ='.$userId.' AND source = "'.$title.'"');
		$checkSouceDownload = $db->loadResult();

		if ($checkSouceDownload==1) {
			echo 0;
		}else{
			echo 1;
		}

		JFactory::getApplication()->close();
	}

	public function earnPoint(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$userId     = $user->get('id');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		$jinput = $app->input;
		$title = $jinput->get('title','', 'String');

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT count(source) as source FROM #__cus_qperks_user_point WHERE type = 16 and state > 0 and user_id ='.$userId.' AND source = "'.$title.'"');
		$checkSouceDownload = $db->loadResult();

		if ($checkSouceDownload<1) {
			$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_user_point');
			$max             = $db->loadResult();
			$convertedDate = date('Y-m-d h:i:s');

			$profile1 = new stdClass();
			$profile1->user_id = $userId;
			$profile1->point=20;
			$profile1->type=16;
			$profile1->state=1;
			$profile1->source=$title;
			$profile1->created_by=$userId;
			$profile1->created_on=$convertedDate;
			$profile1->modified_by=$userId;
			$profile1->ordering=$max + 1;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_qperks_user_point', $profile1);

			$month = 'month'.date('m');
			$db->setQuery('SELECT id FROM #__cus_quake_club_qperks_monthly where user_id = '. $userId .' AND year ="'.date('Y').'"' );
			$monthly_id             = $db->loadResult();
			$db->setQuery('SELECT '.$month.' FROM #__cus_quake_club_qperks_monthly where user_id = '. $userId .' AND year ="'.date('Y').'"' );
			$monthly_point            = $db->loadResult();

			if ($monthly_id) {
				$monthly = new stdClass();
				$monthly->$month = $monthly_point + 20;
				$monthly->id = $monthly_id;
				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');
			}else{
				$monthly = new stdClass();
				$monthly->$month = 0 + 20;
				$monthly->year = date('Y');
				$monthly->user_id = $userId;
				$monthly->state = 1;
				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__cus_quake_club_qperks_monthly', $monthly);
			}
		}

		if ($result) {
			echo true;
		}else{
			echo false;
		}

		JFactory::getApplication()->close();
	}

	
}
