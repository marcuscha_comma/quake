<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Cus_video_gallery
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

/**
 * Videogallery controller class.
 *
 * @since  1.6
 */
class Cus_video_galleryControllerVideogallery extends \Joomla\CMS\MVC\Controller\BaseController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit()
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_cus_video_gallery.edit.videogallery.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_cus_video_gallery.edit.videogallery.id', $editId);

		// Get the model.
		$model = $this->getModel('Videogallery', 'Cus_video_galleryModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_cus_video_gallery&view=videogalleryform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.edit', 'com_cus_video_gallery') || $user->authorise('core.edit.state', 'com_cus_video_gallery'))
		{
			$model = $this->getModel('Videogallery', 'Cus_video_galleryModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_cus_video_gallery.edit.videogallery.id', null);

			// Flush the data from the session.
			$app->setUserState('com_cus_video_gallery.edit.videogallery.data', null);

			// Redirect to the list screen.
			$this->setMessage(Text::_('COM_CUS_VIDEO_GALLERY_ITEM_SAVED_SUCCESSFULLY'));
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(Route::_('index.php?option=com_cus_video_gallery&view=videogalleries', false));
			}
			else
			{
                $this->setRedirect(Route::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.delete', 'com_cus_video_gallery'))
		{
			$model = $this->getModel('Videogallery', 'Cus_video_galleryModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_cus_video_gallery.edit.videogallery.id', null);
                $app->setUserState('com_cus_video_gallery.edit.videogallery.data', null);

                $app->enqueueMessage(Text::_('COM_CUS_VIDEO_GALLERY_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(Route::_('index.php?option=com_cus_video_gallery&view=videogalleries', false));
			}

			// Redirect to the list screen.
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(Route::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function recordCustomiseLink(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$user_id     = $user->get('id');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		$jinput = JFactory::getApplication()->input;
		$link_id = $jinput->get('link_id','', 'String');
		$video_id = $jinput->get('video_id','', 'String');

		$log = new stdClass();
		$log->user_id = $user_id;
		$log->video_id = $video_id;
		$log->link_id = $link_id;
		$log->created_at = date('Y-m-d h:i:s');

		JFactory::getDbo()->insertObject('#__cus_video_gallery_customise_link_log', $log);

		$db->setQuery('SELECT link FROM #__cus_video_gallery_customise_link where id='.$link_id);
		$customise_link             = $db->loadResult();

		if ($customise_link) {
			echo $customise_link;
		}else{
			echo false;
		}

		JFactory::getApplication()->close();
	}

	public function recordVideoLog(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$user_id     = $user->get('id');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		$jinput = JFactory::getApplication()->input;
		$video_id = $jinput->get('video_id','', 'String');

		$db->setQuery('SELECT id, duration, created_at FROM #__cus_video_gallery_log where user_id='.$user_id.' and video_id ="'.$video_id.'" and date(created_at) = "'.date('Y-m-d').'"');
		$video_log             = $db->loadObjectList();

		// $zzz = $video_log[0]->created_at->diff(date('Y-m-d h:i:s'));
		$date1 = $video_log[0]->created_at;
		$date2 = date('Y-m-d h:i:s');
		echo "<pre>";
		print_r(date_diff($date2, $date1));
		echo "</pre>";
		// $origin->diff($target)
		// if ($video_log) {
		// 	$log = new stdClass();
		// 	$log->id =$video_log[0]->id;
		// 	$log->duration = $user->redemption_num-1;
		// 	JFactory::getDbo()->updateObject('#__users', $userObject, 'id');
		// }else{
		// 	$log = new stdClass();
		// 	$log->user_id = $user_id;
		// 	$log->video_id = $video_id;
		// 	$log->created_at = date('Y-m-d h:i:s');

		// 	JFactory::getDbo()->insertObject('#__cus_video_gallery_log', $log);
		// }

		// $log = new stdClass();
		// $log->user_id = $user_id;
		// $log->video_id = $video_id;
		// $log->created_at = date('Y-m-d h:i:s');

		// JFactory::getDbo()->insertObject('#__cus_video_gallery_customise_link_log', $log);

		// if ($customise_link) {
		// 	echo $customise_link;
		// }else{
		// 	echo false;
		// }

		JFactory::getApplication()->close();
	}
}
