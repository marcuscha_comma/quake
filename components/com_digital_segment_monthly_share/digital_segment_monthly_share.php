<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Digital_segment_monthly_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Digital_segment_monthly_share', JPATH_COMPONENT);
JLoader::register('Digital_segment_monthly_shareController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Digital_segment_monthly_share');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
