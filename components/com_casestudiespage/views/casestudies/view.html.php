<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Casestudiespage
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
if(!class_exists('ContentHelperRoute')) require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

jimport('joomla.application.component.view');

/**
 * View class for a list of Casestudiespage.
 *
 * @since  1.6
 */
class CasestudiespageViewCasestudies extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();

		// get list of article
		$db_article    = JFactory::getDBO();
		$query_article = $db_article->getQuery( true );
		$query_article
			->select( 'c.*, DATE_FORMAT(c.publish_up,"%M %d,%Y") as convDate, fv3.value as image_link, fv5.value as award1, fv4.value as client, YEAR(c.publish_up) as year , group_concat(fv6.value) as category, fv7.value as award2, fv8.value as award3, fv9.value as award4, fv10.value as award5, fv11.value as award6, fv12.value as award7, fv13.value as award8, fv14.value as award9, fv15.value as award10' )
			->from( $db_article->quoteName( '#__content', 'c') )
			->join('INNER', $db_article->quoteName('#__fields_values', 'fv3') . ' ON (' . $db_article->quoteName('fv3.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv3.field_id = 10')
			->join('INNER', $db_article->quoteName('#__fields_values', 'fv4') . ' ON (' . $db_article->quoteName('fv4.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv4.field_id = 8')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv5') . ' ON (' . $db_article->quoteName('fv5.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv5.field_id = 34')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv6') . ' ON (' . $db_article->quoteName('fv6.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv6.field_id = 37')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv7') . ' ON (' . $db_article->quoteName('fv7.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv7.field_id = 44')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv8') . ' ON (' . $db_article->quoteName('fv8.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv8.field_id = 46')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv9') . ' ON (' . $db_article->quoteName('fv9.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv9.field_id = 47')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv10') . ' ON (' . $db_article->quoteName('fv10.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv10.field_id = 48')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv11') . ' ON (' . $db_article->quoteName('fv11.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv11.field_id = 49')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv12') . ' ON (' . $db_article->quoteName('fv12.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv12.field_id = 50')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv13') . ' ON (' . $db_article->quoteName('fv13.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv13.field_id = 51')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv14') . ' ON (' . $db_article->quoteName('fv14.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv14.field_id = 52')
			->join('LEFT', $db_article->quoteName('#__fields_values', 'fv15') . ' ON (' . $db_article->quoteName('fv15.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv15.field_id = 53')
			->where($db_article->quoteName('c.catid')." = 11")
			->where($db_article->quoteName('c.state')." > 0")
			->where($db_article->quoteName('c.publish_up')." < '" .JHtml::date('now', 'Y-m-d H:i:s')."'")
			->group('c.id')
			->order( 'publish_up desc' );
		$db_article->setQuery( $query_article );
		$articles = $db_article->loadObjectList();

		foreach ($articles as $key => $articlevalue) {
			$articlevalue->category = explode(',',$articlevalue->category);
			$articlevalue->url = JRoute::_(ContentHelperRoute::getArticleRoute(  $articlevalue->id,  $articlevalue->catid ));

			$awards['Gold'] = 0;
			$awards['Silver'] = 0;
			$awards['Bronze'] = 0;
			$awards['pGold'] = 1;
			$awards['pSilver'] = 2;
			$awards['pBronze'] = 3;
			for ($i=1; $i <= 10; $i++) {
				$text = 'award'.$i;
				switch ($articlevalue->$text) {
					case '1':
						$awards['Gold']++;
						break;
					case '2':
						$awards['Silver']++;
						break;
					case '3':
						$awards['Bronze']++;
						break;
					default:
						# code...
						break;
				}
			}
			if ($awards['Gold'] == 0 && $awards['Silver'] == 0) {
				$awards['pBronze'] = 1;	
			}elseif ($awards['Silver'] == 0){
				$awards['pGold'] = 1;
				$awards['pBronze'] = 2;
			}elseif ($awards['Gold'] == 0) {
				$awards['pSilver'] = 1;
				$awards['pBronze'] = 2;
			}else{
				$awards['pGold'] = 1;
			}
			$articlevalue->awards = $awards;
		}

		//get list of our work category
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_our_work_categories WHERE state > 0 order by title asc');
		$our_work_categories = $db->loadObjectList();

		$this->state = $this->get('State');
		$this->params = $app->getParams('com_newsroompage');
		

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		$this->_prepareDocument();
		$this->articles = $articles;
		$this->our_work_categories = $our_work_categories;

		// echo "<pre>";
		// print_r($articles);
		// echo "</pre>";
		parent::display($tpl);

	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_CASESTUDIESPAGE_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
