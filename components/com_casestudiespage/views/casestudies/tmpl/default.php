<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_programmeupdatespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<div id="app">
    <div>
        <div class="d-none d-md-block">
          <div class="filter-options">
          <label class="form-label">Filter By</label>
            <select class="selectpicker" v-model="selectedYear" @change="selectOnChanged" data-width="fit">
              <option value="0">All Year</option>
              <option v-for="n in currentYear" v-if="n>=2016" :value="n">{{n}}</option>
            </select>

            <select class="selectpicker" v-model="selectedCategory" @change="selectOnChanged" data-width="fit">
              <option value="0">All Category</option>
              <option v-for="category in categoriesArray" :value="category.id">{{category.title}}</option>
            </select>

          </div>
        </div>
        

        <div class="mobile-filter d-block d-sm-block d-md-none">
          <div id="filter-trigger" class="btn btn-grey btn-block">
            Filter By
          </div>

          <div class="mobile-filter-container">
            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Year</label>
              </div>
              <div class="col-7">
                <select class="selectpicker" v-model="selectedYear" @change="selectOnChanged" data-mobile="true">
                  <option value="0">All Year</option>
                  <option v-for="n in currentYear" v-if="n>=2016" :value="n">{{n}}</option>
                </select>
              </div>
            </div>

            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Category</label>
              </div>
              <div class="col-7">
                <select class="selectpicker" v-model="selectedCategory" @change="selectOnChanged" data-mobile="true">
                  <option value="0">All Category</option>
                  <option v-for="category in categoriesArray" :value="category.id">{{category.title}}</option>
                </select>
              </div>
            </div>



            <div id="filter-close" class="btn btn-pink">
              Done
            </div>
          </div>
        </div>

        <div>
          <div class="row" v-show="showStatus">
            <div class="col-md-4 col-sm-6 article article-listing" v-for="(article, index) in articlesArray" v-if="(index+1) <= loadMoreNumber">
              <div class="hover-area pointer" @click="goToLink(article.url);">
              <a :href="article.url">
                <div @click="goToLink(article.url);" class="pointer img-holder" :style="{ 'background-image': 'url(' + article.image_link + ')' }">
                  
                  <!-- Badge -->
                  <div class="ribbon-holder" :class="'ribbon-'+article.awards.pGold" v-if="article.awards.Gold != 0">
                    <div class="ribbon gold"> <!-- .gold / .silver / .bronze --->
                      <i class="fas fa-trophy"></i>
                      <!-- <span class="amount">x{{article.awards.Gold}}</span> -->
                    </div>
                  </div>
                  <div class="ribbon-holder" :class="'ribbon-'+article.awards.pSilver" v-if="article.awards.Silver != 0">
                    <div class='ribbon silver'>
                      <i class="fas fa-trophy"></i>
                      <!-- <span class="amount">x{{article.awards.Silver}}</span> -->
                    </div>
                  </div>
                  <div class="ribbon-holder" :class="'ribbon-'+article.awards.pBronze" v-if="article.awards.Bronze != 0">
                    <div class='ribbon bronze'>
                      <i class="fas fa-trophy"></i>
                      <!-- <span class="amount">x{{article.awards.Bronze}}</span> -->
                    </div>
                  </div>


                  <img class="placeholder" src="images/2020/card-guide.png" alt="">
                  <div class="overlayer">

                  </div>
                  
                </div>
                <div class="article-date">{{article.convDate}}</div>
                <div class="article-title pointer" @click="goToLink(article.url);">{{article.title}}</div>
                <!-- <div class="article-date mt-3">Client</div> -->
                <div class="article-date">{{article.client}}</div>
                <div class="article-link mt-3" @click="goToLink(article.url);">Read More</div>
                </a>
              </div>
            </div>
          </div>
          <div v-show="!showStatus" style="text-align:center;">
                <h5 class="section-desc text-muted">No results have been found.</h5>
            </div>
          <div class="loading-bar" v-if="loadMoreNumber < articlesArray.length">
            <div @click="loadMore(loadMoreNumber)">Load More</div>
          </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var articlesObjFromPhp = <?php echo json_encode($this->articles); ?>;
    var categoriesObjFromPhp = <?php echo json_encode($this->our_work_categories); ?>;
    

    var app = new Vue({
        el: '#app',
        data: {
            articlesArray: articlesObjFromPhp,
            categoriesArray: categoriesObjFromPhp,
            selectedYear: 0,
            selectedCategory: 0,
            currentYear: new Date().getFullYear(),
            loadMoreNumber: 6,
            showStatus: true,
        },
        mounted: function () {
            this.selectOnChanged();
        },
        updated: function () {
            jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        computed: {
          classObjectSilver: function (index) {
            // return {
            //   'ribbon-1': this.article[index].awards.Gold == 0
            // }
          },
          classObjectBronze: function (index) {
            // return {
            //   'ribbon-1': this.article[index].awards.Gold == 0 && this.article.awards.Silver == 0
            // }
          }
        },
        methods: {
            selectOnChanged :function(){
                _this = this;
                _this.articlesArray = articlesObjFromPhp;

                if (_this.selectedCategory != 0) {
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.category.filter(function(category){
                            return category == _this.selectedCategory;
                        }).length > 0;
                        
                    });
                }

                if (_this.selectedYear != 0) {
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.year == _this.selectedYear;
                    });
                }
                if (_this.articlesArray.length > 0) {
                    _this.showStatus = true;
                }else{
                    _this.showStatus = false;
                }
            },
            loadMore :function(loadMoreNumber){
                this.loadMoreNumber = loadMoreNumber +6;
            },
            goToLink :function(link){
                window.location.href = link;
            }
        }
    })
</script>
