<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_V1ewership_network_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

echo "<pre>";
print_r($this->v1ewership_network_share);
print_r($this->items);
echo "</pre>";

?>
<div id="app">
	<div class="tab-pills scrollable-tab">
    <div class="row">
      <div class="col"><a href="./activities-list" class="tab-pill">Activities</a></div>
      <div class="col"><a href="./activities/viewership-highlights" class="tab-pill active">Viewship Highlights</a></div>
    </div>
  </div>

  <div>
    <p>Lorem ipsum dolor sit amet...</p>
    <div class="d-none d-md-block">
      <div class="filter-options text-left text-md-right">
        <label class="form-label">Filter By</label>
        <select  class="selectpicker" v-model="selectedMonth" @change="selectOnChanged">
          <option value="0">Months</option>
          <option value="1">January</option>
          <option value="2">Febuary</option>
          <option value="3">March</option>
          <option value="4">April</option>
          <option value="5">May</option>
          <option value="6">June</option>
          <option value="7">July</option>
          <option value="8">August</option>
          <option value="9">September</option>
          <option value="10">October</option>
          <option value="11">November</option>
          <option value="12">December</option>
        </select>
      </div>
    </div>
  </div>
  <div>
    <div class="progress">
    <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
    <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  </div>

</div>

<script type="text/javascript">
    // var articlesObjFromPhp = <?php echo json_encode($this->articles); ?>;

    var app = new Vue({
        el: '#app',
        data: {
            // articlesArray: articlesObjFromPhp,
            selectedMonth: new Date().getMonth()+1,
        },
        mounted: function () {
        },
        updated() {
            jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
          selectOnChanged: function(){
            console.log('asd');
            
          }
        }
    })
</script>


