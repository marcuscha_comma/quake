<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_V1ewership_network_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * V1ewershipnetworkshares list controller class.
 *
 * @since  1.6
 */
class V1ewership_network_shareControllerV1ewershipnetworkshares extends V1ewership_network_shareController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'V1ewershipnetworkshares', $prefix = 'V1ewership_network_shareModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
