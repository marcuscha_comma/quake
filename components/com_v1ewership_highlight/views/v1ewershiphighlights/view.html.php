<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_V1ewership_highlight
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of V1ewership_highlight.
 *
 * @since  1.6
 */
class V1ewership_highlightViewV1ewershiphighlights extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();

		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->params = $app->getParams('com_v1ewership_highlight');
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');

		// get list of segment share date
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( 'sms.year, sms.month' )
			->from( $db->quoteName( '#__cus_segment_monthly_share', 'sms') )
			->where($db->quoteName('sms.year')." IN (". (date("Y")-1) .",".(date("Y")).")")
			->where($db->quoteName('sms.state')." > 0")
			->order('sms.id DESC')
			->setLimit('6');
		$db->setQuery( $query );
		$segment_monthly_share_date = $db->loadObjectList();

		// get list of digital segment share date
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( 'sms.year, sms.month' )
			->from( $db->quoteName( '#__cus_digital_segment_monthly_share', 'sms') )
			->where($db->quoteName('sms.year')." IN (". (date("Y")-1) .",".(date("Y")).")")
			->where($db->quoteName('sms.state')." > 0")
			->order('sms.id DESC')
			->setLimit('6');
		$db->setQuery( $query );
		$digital_segment_monthly_share_date = $db->loadObjectList();

		// get list of radio segment share date
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( 'sms.year, sms.month' )
			->from( $db->quoteName( '#__cus_radio_segment_monthly_share', 'sms') )
			->where($db->quoteName('sms.year')." IN (". (date("Y")-1) .",".(date("Y")).")")
			->where($db->quoteName('sms.state')." > 0")
			->order('sms.id DESC')
			->setLimit('6');
		$db->setQuery( $query );
		$radio_segment_monthly_share_date = $db->loadObjectList();

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}
		$this->segment_monthly_share_date = $segment_monthly_share_date;
		$this->digital_segment_monthly_share_date = $digital_segment_monthly_share_date;
		$this->radio_segment_monthly_share_date = $radio_segment_monthly_share_date;
		
		$this->_prepareDocument();
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_V1EWERSHIP_HIGHLIGHT_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
