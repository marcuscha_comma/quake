<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_V1ewership_highlight
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$getSegmentsDataByMonth = JRoute::_('index.php?option=com_v1ewership_highlight&task=v1ewershiphighlights.getSegmentsDataByMonth');
$getSegmentMonthlyShare = JRoute::_('index.php?option=com_v1ewership_highlight&task=v1ewershiphighlights.getSegmentMonthlyShare');
$getDigitalSegmentsDataByMonth = JRoute::_('index.php?option=com_v1ewership_highlight&task=v1ewershiphighlights.getDigitalSegmentsDataByMonth');
$getRadioSegmentsDataByMonth = JRoute::_('index.php?option=com_v1ewership_highlight&task=v1ewershiphighlights.getRadioSegmentsDataByMonth');


//detect mobile or desktop
$isMobile = "";
if (stristr($_SERVER['HTTP_USER_AGENT'],'mobi')!==FALSE) {
	$isMobile = True;
}else{
	$isMobile = False;
}

?>

<div id="app">
	<div class="tab-pills scrollable-tab">
		<div class="row">
			<div class="col"><a href="./our-brands" class="tab-pill">Brand Profiles</a></div>
			<div class="col"><a href="./our-brands/reach" class="tab-pill active">Audience Reach</a></div>
		</div>
	</div>

	<div class="row align-items-center">
		<div class="col-lg-9 col-md-8">
			<b>Audience Reach</b>
			<p>Get an overview of our coverage by top channels and brands across the platforms.</p>
		</div>
		<div class="col-lg-3 col-md-4">
			<a href="./channel-profile" class="btn btn-pink btn-block">Download Centre</a>
		</div>
	</div>

	<nav class="nav medium-tab">
		<a class="nav-link" @click="tabOnCLick('tv')" :class="{ active : tvTabIsShow }">
		<div class="medium-icon">
			<svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-420 -641)"><g transform="translate(424 601.06)"><g transform="translate(0 42.667)"><path d="M26.376,42.667H1.758A1.761,1.761,0,0,0,0,44.425V60.837A1.761,1.761,0,0,0,1.758,62.6h9.964v1.263l-6.544,1.09a.586.586,0,0,0,.1,1.164.544.544,0,0,0,.1-.008l6.985-1.164h3.422L22.762,66.1a.552.552,0,0,0,.1.008.586.586,0,0,0,.095-1.164l-6.542-1.09V62.6h9.964a1.761,1.761,0,0,0,1.758-1.758V44.425A1.761,1.761,0,0,0,26.376,42.667ZM15.24,63.768H12.9V62.6H15.24Zm11.723-2.931a.586.586,0,0,1-.586.586H1.758a.586.586,0,0,1-.586-.586V44.425a.586.586,0,0,1,.586-.586H26.376a.586.586,0,0,1,.586.586Z" transform="translate(0 -42.667)" fill="#989898"/><path d="M65.526,85.333H43.252a.586.586,0,0,0-.586.586V99.987a.586.586,0,0,0,.586.586H65.526a.586.586,0,0,0,.586-.586V85.919A.586.586,0,0,0,65.526,85.333ZM64.939,99.4h-21.1v-12.9h21.1Z" transform="translate(-40.321 -82.988)" fill="#989898"/></g></g><rect width="36" height="28" transform="translate(420 641)" fill="none"/></g></svg>
		</div>
		TV
		</a>
		<a class="nav-link" @click="tabOnCLick('radio')" :class="{ active : radioTabIsShow }">
		<div class="medium-icon">
			<svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-518 -641)"><g transform="translate(522.861 610.73)"><g transform="translate(0 34.005)"><path d="M23.642,38.546H6.888L17.7,35.212a.637.637,0,0,0,.394-.758l0-.015a.606.606,0,0,0-.753-.409L2.788,38.515a.647.647,0,0,0-.068.03h-.3A2.425,2.425,0,0,0,0,40.971V53.7a2.425,2.425,0,0,0,2.425,2.425H23.642A2.425,2.425,0,0,0,26.066,53.7V40.971A2.425,2.425,0,0,0,23.642,38.546ZM24.854,53.7a1.212,1.212,0,0,1-1.212,1.212H2.425A1.212,1.212,0,0,1,1.212,53.7V40.971q0-.031,0-.061a1.182,1.182,0,0,1,1.212-1.151H23.7a1.182,1.182,0,0,1,1.151,1.212Z" transform="translate(0 -34.005)" fill="#989898"/><path d="M56.4,245.091c-.061-.061-.091-.091-.152-.091a3.82,3.82,0,0,0-2-.546,3.729,3.729,0,0,0-2,.546.273.273,0,0,0-.182.091,4,4,0,0,0,0,6.7.273.273,0,0,0,.182.091,3.728,3.728,0,0,0,2,.546,3.819,3.819,0,0,0,2-.546c.061,0,.091-.03.152-.091a3.993,3.993,0,0,0,0-6.7Zm-4.516,4.789a2.365,2.365,0,0,1-.394-1.425A2.394,2.394,0,0,1,51.885,247Zm1.819,1.273c-.3-.03-.606-.121-.606-.182V245.91c0-.061.3-.152.606-.182Zm1.819-.182a2.031,2.031,0,0,0-.606.182v-5.425a2.033,2.033,0,0,0,.606.182Zm1.212-1.091V247c0,.424.394.909.394,1.455S56.734,249.456,56.734,249.88Z" transform="translate(-47.338 -232.245)" fill="#989898"/><path d="M317.957,271.11a2.425,2.425,0,1,0,2.455,2.455c0-.02,0-.04,0-.06A2.425,2.425,0,0,0,317.957,271.11Zm.06,3.637a1.212,1.212,0,1,1,1.006-1.388A1.212,1.212,0,0,1,318.018,274.747Z" transform="translate(-297.255 -257.354)" fill="#989898"/><path d="M223.915,271.11a2.425,2.425,0,1,0,2.455,2.455c0-.02,0-.04,0-.06A2.425,2.425,0,0,0,223.915,271.11Zm.06,3.637a1.212,1.212,0,1,1,1.006-1.388A1.212,1.212,0,0,1,223.976,274.747Z" transform="translate(-208.669 -257.354)" fill="#989898"/><path d="M57.788,151.466H37.177a.606.606,0,0,0-.606.606v3.637a.606.606,0,0,0,.606.606H57.788a.606.606,0,0,0,.606-.606v-3.637A.606.606,0,0,0,57.788,151.466Zm-.606,3.637H51.423v-1.212a.606.606,0,1,0-1.212,0V155.1H37.783v-2.425h19.4V155.1Z" transform="translate(-34.449 -144.651)" fill="#989898"/></g></g><rect width="36" height="28" transform="translate(518 641)" fill="none"/></g></svg>
		</div>
		Radio
		</a>
		<a class="nav-link" @click="tabOnCLick('digital')" :class="{ active : digitalTabIsShow }">
		<div class="medium-icon">
			<svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-630 -641)"><g transform="translate(40.41 527.503)"><path d="M607.021,118.4a.676.676,0,1,0,.674.676A.677.677,0,0,0,607.021,118.4Z" fill="#989898"/><path d="M615.579,116.5H603.217a1.631,1.631,0,0,0-1.627,1.626v21.493a1.631,1.631,0,0,0,1.627,1.626h12.362a1.633,1.633,0,0,0,1.627-1.626V118.123A1.632,1.632,0,0,0,615.579,116.5Zm.276,20.941v2.177a.276.276,0,0,1-.229.272H603.217a.276.276,0,0,1-.276-.275v-2.174ZM602.941,120.3v-2.173a.277.277,0,0,1,.276-.276h12.362a.276.276,0,0,1,.275.275V120.3Zm12.914,1.355v14.434H602.941V121.652Z" fill="#989898"/><path d="M607.972,139.34h2.852a.675.675,0,1,0,0-1.351h-2.852a.675.675,0,1,0,0,1.351Z" fill="#989898"/><path d="M611.775,118.4h-2.852a.676.676,0,0,0,0,1.352h2.852a.676.676,0,0,0,0-1.352Z" fill="#989898"/></g><rect width="36" height="28" transform="translate(630 641)" fill="none"/></g></svg>
		</div>
		Digital
		</a>
	</nav>

	<div>
		<div class="d-none d-md-block text-center text-lg-left">
			<div>
				<div class="filter-options">
					<label class="form-label">Filter By</label>
					
					<div class="filter-tab" v-show="tvTabIsShow">
						<select class="selectpicker" v-model="selectedMonth" @change="selectOnChanged" data-width="fit">
							<option value="-">Months</option>
							<option v-for="(item,index) in monthArray" :value="index">{{item.year}},
								{{convertMonthToString(item.month)}}</option>
						</select>
					</div>

					<div class="filter-tab" v-show="radioTabIsShow">
						<select class="selectpicker" v-model="selectedMonth1" @change="selectOnChanged1"
							data-width="fit">
							<option value="-">Months</option>
							<option v-for="(item,index) in monthArray1" :value="index">{{item.year}},
								{{convertMonthToString(item.month)}}</option>
						</select>
					</div>

					<div class="filter-tab" v-show="digitalTabIsShow">
						<select class="selectpicker" v-model="selectedMonth2" @change="selectOnChanged2"
							data-width="fit">
							<option value="-">Months</option>
							<option v-for="(item,index) in monthArray2" :value="index">{{item.year}},
								{{convertMonthToString(item.month)}}</option>
						</select>
					</div>

				</div>
			</div>
		</div>

		<div class="mobile-filter d-block d-sm-block d-md-none">
			<div id="filter-trigger" class="btn btn-grey btn-block">
				Filter By
			</div>

			<div class="mobile-filter-container">


				<div class="row border-bottom">
					<div class="col-5">
						<label class="form-label">Month</label>
					</div>
					<div class="col-7">
						<div class="" v-show="tvTabIsShow">
							<select class="selectpicker" v-model="selectedMonth" @change="selectOnChanged"
								data-mobile="true">
								<option value="-">Months</option>
								<option v-for="(item,index) in monthArray" :value="index">{{item.year}},
									{{convertMonthToString(item.month)}}</option>
							</select>
						</div>

						<div class="" v-show="radioTabIsShow">
							<select class="selectpicker" v-model="selectedMonth1" @change="selectOnChanged1"
							data-mobile="true">
								<option value="-">Months</option>
								<option v-for="(item,index) in monthArray1" :value="index">{{item.year}},
									{{convertMonthToString(item.month)}}</option>
							</select>
						</div>

						<div class="" v-show="digitalTabIsShow">
							<select class="selectpicker" v-model="selectedMonth2" @change="selectOnChanged2"
								data-mobile="true">
								<option value="-">Months</option>
								<option v-for="(item,index) in monthArray2" :value="index">{{item.year}},
									{{convertMonthToString(item.month)}}</option>
							</select>
						</div>
					</div>
				</div>

				<div id="filter-close" class="btn btn-pink">
					Done
				</div>
			</div>
		</div>
	</div>

	<!-- TV Segment Share -->
	<div v-show="tvTabIsShow">

		<div class="row justify-content-center mt-7" v-show="showStatus">
			<div class="col-xl-5  col-lg-6 col-sm-8">

				<canvas id="myChart" width="400" height="400"></canvas>

				<div class="custom-legend-wrap row no-gutters justify-content-center">
					<div class="col-2">
						<div class="custom-legend" style="background-color:#7BB636;"></div>
						Malay
					</div>
					<div class="col-2">
						<div class="custom-legend" style="background-color:#E8168B;"></div>
						Chinese
					</div>
					<div class="col-2">
						<div class="custom-legend" style="background-color:#944E96;"></div>
						Indian
					</div>
					<div class="col-2">
						<div class="custom-legend" style="background-color:#1380C3;"></div>
						English
					</div>
					<div class="col-2">
						<div class="custom-legend" style="background-color:#EFE029;"></div>
						GenNext
					</div>
					<div class="col-2">
						<div class="custom-legend" style="background-color:#22C9CD;"></div>
						Sports
					</div>
					<div class="col-2">
						<div class="custom-legend" style="background-color:#f9ae00;"></div>
						News
					</div>
					<div class="col-2">
						<div class="custom-legend" style="background-color:#eb4e00;"></div>
						Korean
					</div>
				</div>
			</div>
			<div class="col-xl-6 offset-xl-1 col-lg-6 mt-5 mt-lg-0">

				<div class="accordion-holder mt-0">
					<div class="accordion" id="segment-channel">
						<h5 class="mb-3 text-center text-lg-left">Top 5 Channels by Segment</h5>
						<!-- Collapse start -->
						<div class="card border-0" :id="'viewership'+segment.title" v-for="segment in segmentsArray">
							<div class="card-header" :id="'heading'+segment.title" data-toggle="collapse"
								:data-target="'#'+segment.title" aria-expanded="false" :aria-controls="segment.title">
								<h5>
									{{segment.title}}
								</h5>
								<i class="fas fa-plus indicator"></i>
							</div>

							<div :id="segment.title" class="collapse" :aria-labelledby="'heading'+segment.title"
								data-parent="#segment-channel">
								<div class="card-body">
									<div class="row">
										<div class="col-12">
											<table class="table table-bordered">
												<tr>
													<th scope="col-7">
														<h6 class="m-0">Channel</h6>
													</th>
													<th scope="col-5">
														<h6 class="m-0">Monthly Reach</h6>
													</th>
												</tr>
												<tr v-for="(channel, index) in segment.channel_array">
													<td scope="col-7">
														<h6 class="m-0">{{channel.reach.split(":")[0]}}</h6>
													</td>
													<td scope="col-5">{{channel.reach.split(":")[1]}} mil</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Collapse End -->
					</div>
				</div>

			</div>
		</div>
		<div v-show="!showStatus" style="text-align:center;">
			<h5 class="section-desc text-muted">No results have been found.</h5>
		</div>
	</div>

	<!-- Radio -->
	<div v-show="radioTabIsShow">

		<div>
			<div>
				<div class="row align-items-center mb-3">
					<div class="col-7">
						<h5 class="mb-0">{{radioSegmentsArray.attach_file_1_title}} </h5>
					</div>
					<div class="col-5 text-right">
						<button class="btn btn-pink" @click="downloadUrl(radioSegmentsArray.attach_file_1)">
							Download
						</button>
					</div>
				</div>

				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" :src="'../uploads/radio-pdf/'+radioSegmentsArray.attach_file_1"></iframe>
				</div>
			</div>

			<div v-if="radioSegmentsArray.attach_file_2_title != null && radioSegmentsArray.attach_file_2_title != '' && radioSegmentsArray.attach_file_2_title != 'PDF File Name'" class="mt-5">
				<div  class="row align-items-center mb-3">
					<div class="col-7">
						<h5 class="mb-0">{{radioSegmentsArray.attach_file_2_title}} </h5>
					</div>
					<div class="col-5 text-right">
						<button class="btn btn-pink" @click="downloadUrl(radioSegmentsArray.attach_file_2)">
							Download
						</button>
					</div>
				</div>

				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" :src="'../uploads/radio-pdf/'+radioSegmentsArray.attach_file_2"></iframe>
				</div>
			</div>
			
		</div>

	</div>

	<!-- Digital -->
	<div v-show="digitalTabIsShow">
	
		<div class="row mt-5 justify-content-lg-between justify-content-center" v-show="showStatus2">
			
			<div class="col-lg-5 col-sm-8 mb-5">
				<h4 class="text-center mb-3">Total Monthly Digital Reach</h4>
				
				<div class="reach-card bg-pink text-right">
					<div class="row align-items-center">
						<div class="col-4 text-center">
							<img src="images/2020/i-uni-user.svg" alt="">
						</div>
						<div class="col-8">
							<h3>{{digitalSegmentsArray.unique_users}}</h3>
							<h5>Unique Users</h5>
						</div>
					</div>					
				</div>
				<div class="reach-card bg-blue text-right">
					<div class="row align-items-center">
						<div class="col-4 text-center">
							<img src="images/2020/i-view.svg" alt="">
						</div>
						<div class="col-8">
							<h3>{{digitalSegmentsArray.page_views}}</h3>
							<h5>Page Views</h5>
						</div>
					</div>					
				</div>
				<div class="reach-card bg-yellow text-right">
					<div class="row align-items-center">
						<div class="col-4 text-center">
							<img src="images/2020/i-fb-reach.svg" alt="">
						</div>
						<div class="col-8">
							<h3>{{digitalSegmentsArray.fb_page_reach}}</h3>
							<h5>FB Page Reach</h5>
						</div>
					</div>					
				</div>
				<div class="reach-card bg-orange text-right">
					<div class="row align-items-center">
						<div class="col-4 text-center">
							<img src="images/2020/i-follower.svg" alt="">
						</div>
						<div class="col-8">
							<h3>{{digitalSegmentsArray.social_media_followers}}</h3>
							<h5>Social Media Followers</h5>
						</div>
					</div>					
				</div>
			</div>
			<div class="col-lg-6">
				<h5 class="mb-4 text-center text-lg-left">Top 5 Sites by Segment</h5>
				
				<div class="accordion-holder mt-0">
					<div class="accordion" id="digital-segment">
						<!-- Collapse start -->
						<div class="card border-0" :id="'digital'+segment.title"
							v-for="(segment,index) in segmentTitle2">
							<div class="card-header" :id="'digital-heading'+segment.title" data-toggle="collapse"
								:data-target="'#d'+segment.title" aria-expanded="false"
								:aria-controls="'d'+segment.title">
								<h5>
									{{segment.title}} 
									<span class="badge">{{segment.total_unique_users}}</span>
								</h5>
								<i class="fas fa-plus indicator"></i>
							</div>

							<div :id="'d'+segment.title" class="collapse"
								:aria-labelledby="'digital-heading'+segment.title" data-parent="#digital-segment">
								<div class="card-body">
									<div class="row">
										<div class="col-12">
											<table class="table table-bordered">
												<tr>
													<th class="bg-light" width="60%">
														<h6 class="m-0">Website</h6>
													</th>
													<th class="bg-light">
														<h6 class="m-0">Unique Monthly Visitors</h6>
													</th>
												</tr>
												<tr v-for="channel in segment.channels">
													<td>
														<div class="row align-items-center">
															<!-- <div class="col-sm-5 text-center">
																<img class="reach-channel" :src="'../'+channel.image" alt="">
															</div> -->
															<div class="col-12">
																<h6 class="m-0">{{channel.title}}</h6>
															</div>
														</div>
													</td>
													<td>{{channel.unique_users}}</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Collapse End -->
					</div>
					<div style="float: right;
    margin-top: 10px; font-size: 9px;">
						<p>{{digitalSegmentsArray.source}}</p>
					</div>
				</div>
			</div>
		</div>

		<div v-show="!showStatus2" style="text-align:center;">
			<h5 class="section-desc text-muted">No results have been found.</h5>
		</div>
	</div>



</div>

<script type="text/javascript">
	var getSegmentsDataByMonthUrl = <?php echo json_encode($getSegmentsDataByMonth); ?>;
	var getSegmentMonthlyShare = <?php echo json_encode($getSegmentMonthlyShare); ?>;
	var getDigitalSegmentsDataByMonth = <?php echo json_encode($getDigitalSegmentsDataByMonth); ?>;
	var getRadioSegmentsDataByMonth = <?php echo json_encode($getRadioSegmentsDataByMonth); ?>;
	var isMobile = <?php echo json_encode($isMobile); ?>;
	var segment_monthly_share_date = <?php echo json_encode($this->segment_monthly_share_date); ?>;
	var digital_segment_monthly_share_date = <?php echo json_encode($this->digital_segment_monthly_share_date); ?>;
	var radio_segment_monthly_share_date = <?php echo json_encode($this->radio_segment_monthly_share_date); ?>;

	var chartJsEvent = [];
	if (isMobile) {
		chartJsEvent = ['click', 'touchend'];
	} else {
		chartJsEvent = ["mousemove", "mouseout", "click", "touchstart", "touchmove", "touchend"];
	}

	var app = new Vue({
		el: '#app',
		data: {
			segmentShareArray: [],
			segmentsArray: [],
			digitalSegmentsArray: [],
			radioSegmentsArray: [],
			segmentTitle: [' Korean', ' News', ' Sports', ' GenNext', ' English', ' Indian', ' Chinese', ' Malay'],
			segmentTitle2: [],
			selectedMonth: segment_monthly_share_date.length - 1,
			selectedMonth1: radio_segment_monthly_share_date.length - 1,
			selectedMonth2: digital_segment_monthly_share_date.length - 1,
			showStatus: true,
			showStatus1: true,
			showStatus2: true,
			myChart: "",
			monthArray: segment_monthly_share_date.reverse(),
			monthArray1: radio_segment_monthly_share_date.reverse(),
			monthArray2: digital_segment_monthly_share_date.reverse(),
			segmentsColorCode: [
				'#eb4e00', //korean
				'#f9ae00', //news
				'#22C9CD', //sports
				'#EFE029', //gennex
				'#1380C3', //english
				'#944E96', //indian
				'#E8168B', //chinese
				'#7BB636', //malay
			],
			tvTabIsShow: true,
			radioTabIsShow: false,
			digitalTabIsShow: false
		},
		beforeMount: function () {},
		mounted: function () {
			this.selectOnChanged();
			this.selectOnChanged1();
			this.selectOnChanged2();
		},
		updated: function () {
			jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
		},
		methods: {
			selectOnChanged: function () {
				_this = this;
				//get Segments
				console.log(_this.monthArray[this.selectedMonth].month);
				console.log(_this.monthArray[this.selectedMonth].year);


				jQuery.ajax({
					url: getSegmentsDataByMonthUrl,
					type: 'get',
					data: {
						month: _this.monthArray[this.selectedMonth].month,
						year: _this.monthArray[this.selectedMonth].year
					},
					success: function (result) {
						if (result != 0) {
							_this.segmentsArray = JSON.parse(result);
							_this.showStatus = true;
						} else {
							console.log('Fail');
							_this.showStatus = false;
						}
					},
					error: function () {
						console.log('ajax fail');
						_this.showStatus = false;
					}
				});
				//  get segment Monthly Share
				jQuery.ajax({
					url: getSegmentMonthlyShare,
					type: 'get',
					data: {
						month: _this.monthArray[this.selectedMonth].month,
						year: _this.monthArray[this.selectedMonth].year
					},
					success: function (result) {
						if (result != 0) {
							_this.segmentShareArray = JSON.parse(result)[0];
							_this.drawChart();
							_this.showStatus = true;
						} else {
							console.log('Fail');
							_this.showStatus = false;
						}
					},
					error: function () {
						console.log('ajax fail');
						_this.showStatus = false;
					}
				});
			},
			selectOnChanged1: function () {
				_this = this;
				//get Segments
				jQuery.ajax({
					url: getRadioSegmentsDataByMonth,
					type: 'get',
					data: {
						month: _this.monthArray1[this.selectedMonth1].month,
						year: _this.monthArray1[this.selectedMonth1].year
					},
					success: function (result) {
						if (result != 0) {
							_this.radioSegmentsArray = JSON.parse(result)[0];

							_this.showStatus1 = true;
						} else {
							_this.showStatus1 = false;
						}
					},
					error: function () {
						_this.showStatus1 = false;
					}
				});
			},
			selectOnChanged2: function () {
				_this = this;
				//get Segments
				jQuery.ajax({
					url: getDigitalSegmentsDataByMonth,
					type: 'get',
					data: {
						month: _this.monthArray2[this.selectedMonth2].month,
						year: _this.monthArray2[this.selectedMonth2].year
					},
					success: function (result) {
						if (result != 0) {
							_this.digitalSegmentsArray = JSON.parse(result)[0];
							_this.segmentTitle2 = JSON.parse(result)["segments"];

							_this.showStatus2 = true;
						} else {
							_this.showStatus2 = false;
						}
					},
					error: function () {
						_this.showStatus2 = false;
					}
				});
			},
			countMonthNumber: function () {
				var periodMonth = 6;
				var currentMonth = new Date().getMonth() + 1;
				var lastYearMonth = 0;
				var count = 1;
				if (currentMonth - periodMonth < 0) {
					lastYearMonth = 12 + currentMonth - periodMonth;
					for (var i = 0; i < periodMonth; i++) {
						if (lastYearMonth <= 12) {
							this.monthArray[i] = [{
								'year': new Date().getYear() - 1 + 1900,
								'month': lastYearMonth++
							}];
						} else {
							this.monthArray[i] = [{
								'year': new Date().getYear() + 1900,
								'month': count++
							}];
						}
					}

				} else {
					count = currentMonth - periodMonth + 1;
					for (var i = 0; i < periodMonth; i++) {
						this.monthArray[i] = [{
							'year': new Date().getYear() + 1900,
							'month': count++
						}];
					}
				}
			},
			convertMonthToString: function (month) {
				//   console.log(month); 

				switch (month) {
					case '1':
						return "January";
						break;
					case '2':
						return "Febuary";
						break;
					case '3':
						return "March";
						break;
					case '4':
						return "April";
						break;
					case '5':
						return "May";
						break;
					case '6':
						return "June";
						break;
					case '7':
						return "July";
						break;
					case '8':
						return "August";
						break;
					case '9':
						return "September";
						break;
					case '10':
						return "October";
						break;
					case '11':
						return "November";
						break;
					case '12':
						return "December";
						break;

					default:
						break;
				}
			},
			downloadUrl :function(urlLink){
              this.urlLink = '../uploads/radio-pdf/'+ urlLink;
              window.open(this.urlLink);
            },
			convertNumberIntoSuffix: function (number) {
				return Math.abs(Number(number)) >= 1.0e+9

					?
					Math.abs(Number(number)) / 1.0e+9 + " bil"
					// Six Zeroes for Millions
					:
					Math.abs(Number(number)) >= 1.0e+6

					?
					Math.abs(Number(number)) / 1.0e+6 + " mil"
					// Three Zeroes for Thousands
					:
					Math.abs(Number(number)) >= 1.0e+3

					?
					Math.abs(Number(number)) / 1.0e+3 + " k"

					:
					Math.abs(Number(number));
			},
			tabOnCLick: function (tab) {
				switch (tab) {
					case "tv":
						this.tvTabIsShow = true;
						this.radioTabIsShow = false;
						this.digitalTabIsShow = false;
						break;
					case "radio":
						this.tvTabIsShow = false;
						this.radioTabIsShow = true;
						this.digitalTabIsShow = false;
						break;
					case "digital":
						this.tvTabIsShow = false;
						this.radioTabIsShow = false;
						this.digitalTabIsShow = true;
						break;

					default:
						break;
				}
			},
			drawChart: function () {
				_this = this;
				Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

				var helpers = Chart.helpers;
				var defaults = Chart.defaults;

				Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
					updateElement: function (arc, index, reset) {
						var _this = this;
						var chart = _this.chart,
							chartArea = chart.chartArea,
							opts = chart.options,
							animationOpts = opts.animation,
							arcOpts = opts.elements.arc,
							centerX = (chartArea.left + chartArea.right) / 2,
							centerY = (chartArea.top + chartArea.bottom) / 2,
							startAngle = opts.rotation, // non reset case handled later
							endAngle = opts.rotation, // non reset case handled later
							dataset = _this.getDataset(),
							circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ?
							0 : _this.calculateCircumference(dataset.data[index]) * (opts
								.circumference / (2.0 * Math.PI)),
							innerRadius = reset && animationOpts.animateScale ? 0 : _this
							.innerRadius,
							outerRadius = reset && animationOpts.animateScale ? 0 : _this
							.outerRadius,
							custom = arc.custom || {},
							valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

						helpers.extend(arc, {
							// Utility
							_datasetIndex: _this.index,
							_index: index,

							// Desired view properties
							_model: {
								x: centerX + chart.offsetX,
								y: centerY + chart.offsetY,
								startAngle: startAngle,
								endAngle: endAngle,
								circumference: circumference,
								outerRadius: outerRadius,
								innerRadius: innerRadius,
								label: valueAtIndexOrDefault(dataset.label, index, chart
									.data.labels[index]),
								value: dataset.data[index]
							},

							draw: function () {
								var ctx = this._chart.ctx,
									vm = this._view,
									sA = vm.startAngle,
									eA = vm.endAngle,
									opts = this._chart.config.options;

								var labelPos = this.tooltipPosition();
								var segmentLabel = vm.circumference / opts
									.circumference * 100;

								ctx.beginPath();

								ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
								ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);

								ctx.closePath();
								ctx.strokeStyle = vm.borderColor;
								ctx.lineWidth = vm.borderWidth;

								ctx.fillStyle = vm.backgroundColor;

								ctx.fill();
								ctx.lineJoin = 'bevel';

								if (vm.borderWidth) {
									ctx.stroke();
								}

								if (vm.circumference >
									0.15) { // Trying to hide label when it doesn't fit in segment
									ctx.beginPath();
									ctx.font = helpers.fontString(15, 'sans-serif',
										'sans-serif');
									ctx.fillStyle = "#fff";
									ctx.textBaseline = "top";
									ctx.textAlign = "center";
									// console.log(ctx.font);


									// Round percentage in a way that it always adds up to 100%
									var segmentLabelText = this._model.value;
									ctx.fillText(segmentLabelText + "%", labelPos.x,
										labelPos.y);

									//ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
								}
							}
						});

						var model = arc._model;
						model.backgroundColor = custom.backgroundColor ? custom.backgroundColor :
							valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts
								.backgroundColor);
						model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom
							.hoverBackgroundColor : valueAtIndexOrDefault(dataset
								.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
						model.borderWidth = custom.borderWidth ? custom.borderWidth :
							valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
						model.borderColor = custom.borderColor ? custom.borderColor :
							valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

						// Set correct angles if not resetting
						if (!reset || !animationOpts.animateRotate) {
							if (index === 0) {
								model.startAngle = opts.rotation;
							} else {
								model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
							}

							model.endAngle = model.startAngle + model.circumference;
						}

						arc.pivot();
					}
				});

				var config = {
					type: 'doughnutLabels',
					data: {
						datasets: [{
							data: [
								parseInt(_this.segmentShareArray.segment_share_8),
								parseInt(_this.segmentShareArray.segment_share_7),
								parseInt(_this.segmentShareArray.segment_share_6),
								parseInt(_this.segmentShareArray.segment_share_5),
								parseInt(_this.segmentShareArray.segment_share_4),
								parseInt(_this.segmentShareArray.segment_share_3),
								parseInt(_this.segmentShareArray.segment_share_2),
								parseInt(_this.segmentShareArray.segment_share_1),
							],
							backgroundColor: _this.segmentsColorCode,
							label: 'Dataset 1',
							borderWidth: 0,
						}],
						labels: _this.segmentTitle
					},
					options: {
						events: chartJsEvent,
						onClick: function (e) {
							// console.log(myNewChart.getElementsAtEvent(e));

						},

						responsive: true,
						legend: {
							display: false,
							position: 'bottom',
							// reverse: true,
						},
						animation: {
							animateScale: true,
							animateRotate: true
						},
						tooltips: {
							mode: 'point',
							// enabled: false,
							custom: function (tooltipModel) {
								var tooltipEl = document.getElementById('chartjs-tooltip');
								if (tooltipModel.body != undefined) {
									str = tooltipModel.body[0].lines[0].split(':');

									str = str[0];
									tooltipModel.body[0].lines[0] = str;

								}
								// console.log(tooltipModel.body[0].lines[0]);

							}
						},
					}
				};

				var ctx = document.getElementById("myChart").getContext("2d");
				console.log(ctx);

				if (_this.myChart != "") {
					_this.myChart.destroy();
				}
				_this.myChart = new Chart(ctx, config);

				Chart.pluginService.register({
					beforeDraw: function (chart) {
						var width = chart.chart.width,
							height = chart.chart.height,
							ctx = chart.chart.ctx;

						ctx.restore();
						var fontSize = (height / 340).toFixed(2);
						ctx.font = "bold " + fontSize + "em 'FS Albert Pro', sans-serif";
						ctx.textBaseline = "middle";

						var text = "Viewing share",
							textX = Math.round((width - ctx.measureText(text).width) / 2),
							textY = (height / 2) - 10,
							text2 = "by segment";

						ctx.fillText(text, textX, textY);
						ctx.fillText(text2, textX + 10, textY + 25);
						ctx.save();
					}
				});

			}
		}
	})
</script>
