<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_V1ewership_highlight
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * V1ewershiphighlights list controller class.
 *
 * @since  1.6
 */
class V1ewership_highlightControllerV1ewershiphighlights extends V1ewership_highlightController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'V1ewershiphighlights', $prefix = 'V1ewership_highlightModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	public function getSegmentsDataByMonth(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$jinput = JFactory::getApplication()->input;
		$month = $jinput->get('month');
		$year = $jinput->get('year');

		//get list from table
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( 's.*, group_concat(cmr.title) as channel_title, group_concat(cmr.title,":",cmr.reach ORDER BY cmr.id ASC ) as channel_reach' )
			->from( $db->quoteName( '#__cus_segments', 's' ) )
			->join('INNER', $db->quoteName('#__cus_channel_monthly_reach', 'cmr') . ' ON (' . $db->quoteName('s.id') . ' = ' . $db->quoteName('cmr.segment') . ')')
			->where($db->quoteName('cmr.month') . ' = '. $db->quote($month))
			->where($db->quoteName('cmr.year') . ' = '. $db->quote($year))
			->group($db->quoteName('cmr.segment'))
			->order('cmr.segment ASC');
		$db->setQuery( $query );
		$db->execute();
		$clientDataList = $db->getNumRows();
		
		if ($clientDataList > 0) {
			$segment_array = $db->loadObjectList();
			
			foreach ($segment_array as $key => $value) {
				
				$value->channel_title = explode(',',$value->channel_title);
				$value->channel_reach = explode(',', $value->channel_reach);
				$title = explode(':',$value->channel_reach);
				
				$tmp_array = array();
				for ($i=0; $i < 5; $i++) { 
					array_push($tmp_array,array('title'=>$value->channel_title[$i],'reach'=>$value->channel_reach[$i]));
				}
				$value->channel_array = $tmp_array;
			}
		

			echo json_encode($segment_array);
		}else{
			echo 0;
		}
		JFactory::getApplication()->close(); 
	}

	public function getRadioSegmentsDataByMonth(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$jinput = JFactory::getApplication()->input;
		$month = $jinput->get('month');
		$year = $jinput->get('year');

		//get list from table
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( '*' )
			->from( $db->quoteName( '#__cus_radio_segment_monthly_share', 'r' ) )
			->where($db->quoteName('r.month') . ' = '. $db->quote($month))
			->where($db->quoteName('r.year') . ' = '. $db->quote($year));
		$db->setQuery( $query );
		$db->execute();
		$radioDataList = $db->getNumRows();
		
		if ($radioDataList > 0) {
			$radio_array = $db->loadObjectList();
	
			echo json_encode($radio_array);
		}else{
			echo 0;
		}
		JFactory::getApplication()->close(); 
	}

	public function getDigitalSegmentsDataByMonth(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$jinput = JFactory::getApplication()->input;
		$month = $jinput->get('month');
		$year = $jinput->get('year');

		$db = JFactory::getDBO();
		$db->setQuery('SELECT title FROM #__cus_segments_digital WHERE state > 0 and id <> 1 order by id asc');
		$segments_digital = $db->loadObjectList();

		//get list from table
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( 'dsms.*' )
			->from( $db->quoteName( '#__cus_digital_segment_monthly_share', 'dsms' ) )
			->where($db->quoteName('dsms.month') . ' = '. $db->quote($month))
			->where($db->quoteName('dsms.year') . ' = '. $db->quote($year));
		$db->setQuery( $query );
		$db->execute();
		$digitalSegmentDataList = $db->getNumRows();
		
		if ($digitalSegmentDataList > 0) {
			$digital_segment_array = $db->loadObjectList();
			foreach ($digital_segment_array[0] as $key => $value) {

				switch ($key) {
					case 'malay_total_unique_users':
						$segments_digital[0]->total_unique_users = $value;
						break;
					case 'chinese_total_unique_users':
						$segments_digital[1]->total_unique_users = $value;
						break;
					case 'indian_total_unique_users':
						$segments_digital[2]->total_unique_users = $value;
						break;
					case 'english_total_unique_users':
						$segments_digital[3]->total_unique_users = $value;
						break;
					default:
						break;
				}
				
				switch ($key) {
					case "malay_top_1_channel":
					case "malay_top_2_channel":
					case "malay_top_3_channel":
					case "malay_top_4_channel":
					case "malay_top_5_channel":
						$db = JFactory::getDBO();
						$db->setQuery('SELECT title,image FROM #__cus_digitals WHERE id ='.$value);
						$tmp = $db->loadAssocList();
						$segments_digital[0]->channels[] = $tmp[0];
						break;
					case "chinese_top_1_channel":
					case "chinese_top_2_channel":
					case "chinese_top_3_channel":
					case "chinese_top_4_channel":
					case "chinese_top_5_channel":
						$db = JFactory::getDBO();
						$db->setQuery('SELECT title,image FROM #__cus_digitals WHERE id ='.$value);
						$tmp = $db->loadAssocList();
						$segments_digital[1]->channels[] = $tmp[0];
						break;
					case "indian_top_1_channel":
					case "indian_top_2_channel":
					// case "indian_top_3_channel":
					// case "indian_top_4_channel":
					// case "indian_top_5_channel":
						$db = JFactory::getDBO();
						$db->setQuery('SELECT title,image FROM #__cus_digitals WHERE id ='.$value);
						$tmp = $db->loadAssocList();
						$segments_digital[2]->channels[] = $tmp[0];
						break;
					case "english_top_1_channel":
					case "english_top_2_channel":
					case "english_top_3_channel":
					case "english_top_4_channel":
					case "english_top_5_channel":
						$db = JFactory::getDBO();
						$db->setQuery('SELECT title,image FROM #__cus_digitals WHERE id ='.$value);
						$tmp = $db->loadAssocList();
						$segments_digital[3]->channels[] = $tmp[0];
						break;
					case "malay_top_1_unique_users":
						$segments_digital[0]->channels[0]["unique_users"] = $value;
						break;
					case "malay_top_2_unique_users":
						$segments_digital[0]->channels[1]["unique_users"] = $value;
						break;
					case "malay_top_3_unique_users":
						$segments_digital[0]->channels[2]["unique_users"] = $value;
						break;
					case "malay_top_4_unique_users":
						$segments_digital[0]->channels[3]["unique_users"] = $value;
						break;
					case "malay_top_5_unique_users":
						$segments_digital[0]->channels[4]["unique_users"] = $value;
						break;
					case "chinese_top_1_unique_users":
						$segments_digital[1]->channels[0]["unique_users"] = $value;
						break;
					case "chinese_top_2_unique_users":
						$segments_digital[1]->channels[1]["unique_users"] = $value;
						break;
					case "chinese_top_3_unique_users":
						$segments_digital[1]->channels[2]["unique_users"] = $value;
						break;
					case "chinese_top_4_unique_users":
						$segments_digital[1]->channels[3]["unique_users"] = $value;
						break;
					case "chinese_top_5_unique_users":
						$segments_digital[1]->channels[4]["unique_users"] = $value;
						break;
					case "indian_top_1_unique_users":
						$segments_digital[2]->channels[0]["unique_users"] = $value;
						break;
					case "indian_top_2_unique_users":
						$segments_digital[2]->channels[1]["unique_users"] = $value;
						break;
					case "indian_top_3_unique_users":
						// $segments_digital[2]->channels[2]["unique_users"] = $value;
						break;
					case "indian_top_4_unique_users":
						// $segments_digital[2]->channels[3]["unique_users"] = $value;
						break;
					case "indian_top_5_unique_users":
						// $segments_digital[2]->channels[4]["unique_users"] = $value;
						break;
					case "english_top_1_unique_users":
						$segments_digital[3]->channels[0]["unique_users"] = $value;
						break;
					case "english_top_2_unique_users":
						$segments_digital[3]->channels[1]["unique_users"] = $value;
						break;
					case "english_top_3_unique_users":
						$segments_digital[3]->channels[2]["unique_users"] = $value;
						break;
					case "english_top_4_unique_users":
						$segments_digital[3]->channels[3]["unique_users"] = $value;
						break;
					case "english_top_5_unique_users":
						$segments_digital[3]->channels[4]["unique_users"] = $value;
						break;
					default:
						
						break;
				}
			}
			$digital_segment_array['segments'] = $segments_digital;
			echo json_encode($digital_segment_array);
		}else{
			echo 0;
		}
		JFactory::getApplication()->close(); 
	}

	public function getNetworkShareByMonth(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$jinput = JFactory::getApplication()->input;
		$month = $jinput->get('month');
		$year = date("Y");

		//get list from table
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( '*' )
			->from( $db->quoteName( '#__cus_v1ewership_network_share' ) )
			->where($db->quoteName('month') . ' = '. $db->quote($month))
			->where($db->quoteName('year') . ' = '. $db->quote($year));
		$db->setQuery( $query );
		$db->execute();
		$clientDataList = $db->getNumRows();
		
		if ($clientDataList > 0) {
			echo json_encode($db->loadObjectList());
		}else{
			echo 0;
		}

		JFactory::getApplication()->close(); 
	}

	public function getSegmentMonthlyShare(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$jinput = JFactory::getApplication()->input;
		$month = $jinput->get('month');
		$year = $jinput->get('year');

		//get list from table
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( '*' )
			->from( $db->quoteName( '#__cus_segment_monthly_share' ) )
			->where($db->quoteName('month') . ' = '. $db->quote($month))
			->where($db->quoteName('year') . ' = '. $db->quote($year));
		$db->setQuery( $query );
		$db->execute();
		$dataList = $db->getNumRows();
		
		if ($dataList > 0) {
			echo json_encode($db->loadObjectList());
		}else{
			echo 0;
		}

		JFactory::getApplication()->close(); 
	}

}
