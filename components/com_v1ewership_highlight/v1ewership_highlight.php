<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_V1ewership_highlight
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('V1ewership_highlight', JPATH_COMPONENT);
JLoader::register('V1ewership_highlightController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('V1ewership_highlight');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
