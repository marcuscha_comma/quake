<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_tac
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Quake_club_tac', JPATH_COMPONENT);
JLoader::register('Quake_club_tacController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Quake_club_tac');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
