<div id="app" class="">
  <div class="pink-page-title">
    <div class="container">
      <h2>Quake Club Terms & Conditions</h2>
    </div>
  </div>

  <div class="mt-5 f-14 line-height">

    <h5><strong>1.0  DEFINITION</strong></h5>
    <p>These terms and conditions (“Terms &amp; Conditions”) are intended to regulate the use of the Quake Club Reward
      Programme.</p>
    <p>1.1 In these Terms &amp; Conditions, except to the extent that the context requires otherwise, the following
      expressions shall have the following meanings.</p>

    <p><strong>“Astro” </strong>means Astro Media Solutions (708931-H);</p>

    <p><strong>“Quake”</strong> means a brand name registered under Astro Media Solutions to engage with the advertising
      and marketing industry via multiple platforms – Website, Facebook, WhatsApp and more coming. Formed by a strong
      team of over 100 professionals, we provide end-to-end advertising and marketing solutions, driven by 5 core values
      – Quality, Uncommon, Amazing, Keen and Effective. Quake believes in collaboration with like-minded partners to
      advocate marketing excellence and drive business growth;<strong>  </strong></p>

    <p><strong>“Data” </strong>means the Transaction Data and the Member Data;</p>

    <p><strong>“Database” </strong>means the computerised records of information consisting of inter alia, Member Data,
      Transaction Data, Members' Points (also known as Q-Perks) allocation, Rewards Requests,  (also known as Q-Perks)
      redemption and fulfillment of Rewards Requests, maintained and operated by Quake Club;</p>

    <p><strong>“Laws” </strong>means any local state or federal laws, statute, rule, or order governed by and construed
      in accordance with the laws of Malaysia;</p>

    <p><strong>“Member” </strong>means a Quake Club Member unless otherwise specified. Membership is non-transferable;
    </p>

    <p><strong>“Membership” </strong>means those arrangements by which a Member agrees to participate in the programme
      and receives  (also known as Q-Perks) by performing activities listed under Quake Club Rewards, and so qualifies
      for Gifts redemption under these Terms and Conditions;</p>

    <p><strong>“Member’s Data” </strong>means data relating to the Member’s identification, address, phone number, email
      address and other personal and household information provided during the Member’s initial registration and/or from
      time to time by the Member to Quake Club;</p>

    <p><strong>“Personal Data” </strong>means personal data and other information collected by Quake Club from the
      member during the Member’s initial registration and/or from time to time by the Member to Quake Club;</p>

    <p><strong>“Q-Perks”</strong> means the  allocated by Quake to Members pursuant to these Terms &amp; Conditions;</p>

    <p><strong>“Interactive Services”</strong> means the function to share and download on the website;</p>

    <p><strong>“Content”</strong> means the submission of profile information, text, or other materials;</p>

    <p><strong>“Programme” </strong>means Quake Club Rewards Programme, a -based (also known as Q-Perks)  loyalty
      programme owned and operated by Quake, whereby Q-Perks are given to Members based on the activities performed;
      Q-Perks are redeemable by the Members for Rewards from Quake and the Rewards will be sent via postage delivery.
    </p>

    <p><strong>“Rewards” </strong>means the merchandise and/or other forms of goods and/or services which Members may
      redeem Q-Perks for;</p>

    <p><strong>"Redemption Catalogue" </strong>means the relevant catalogue displaying the Rewards available to Members
      at any point in time and accessible on the Quake Club Rewards website at quake.com.my;</p>

    <p><strong>“Partner/Supplier” </strong>means those companies, entities and other persons participating in the
      Programme who supply goods and/or services, which may be made available for redemption of Q-Perks;</p>

    <p><strong>“Campaign” </strong>means the campaigns organised by the “Programme” according to the schedule and during
      the time period specified;</p>

    <p>1.2  A Member is defined as “Active” on a particular date if the said Member has logged in 4 times in a
      month<strong>.</strong></p>


    <h5 style="margin-top: 50px;"><strong>2.0 THE QUAKE CLUB REWARDS PROGRAMME</strong></h5>

    <p>2.1 The Quake Club Rewards programme (“Programme”) is a proprietary -based (also known as Q-Perks) loyalty
      programme which is owned and operated by Quake, whereby Members are allocated Q-Perks at the discretion of Quake
      based on, but not limited to, Members’ activities that will be specified at a later clause and Q-Perks allocated
      are redeemable for Rewards offered under this Programme and will be delivered via postage.</p>

    <h5 style="margin-top: 50px;"><strong>3.0  MEMBERSHIP &amp; ELIGIBILITY</strong></h5>

    <p>3.1 Membership of the programme is free.</p>

    <p>3.2 Only individuals aged 18 years and above, who hold a profession in the advertising and marketing industry,
      can be members.</p>

    <table class="table table-bordered">
      <tbody>
        <tr>
          <td width="204">
            Type of profession
          </td>
          <td width="195">
            Job level
          </td>
          <td width="169">
            Designation
          </td>
        </tr>
      </tbody>
    </table>



    <p>3.3 Membership is eligible for both Malaysian and non-Malaysian.</p>

    <p>3.4 Membership of the Programme is limited to individuals only and is limited to one account per individual.</p>

    <p>3.5 Employees, officers, directors, agents and representatives of Quake Club Rewards Programme, Astro employees
      and its affiliates are eligible for Membership. However, they’re not allowed to redeem the rewards on Quake Club
      Rewards Programme.</p>

    <p>3.6 Besides Clause 3.5, only professionals in the advertising and marketing industry are allowed to become a
      member. Non-professionals and those from other industries are not eligible for Quake Club Rewards Programme
      membership.</p>

    <p>3.7 Any individual who wishes to become a Member must agree to be bound by these Terms and Conditions, as varied
      from time to time.</p>

    <p>3.8 <strong>Your Account</strong></p>

    <p>You agree that you shall only submit or provide information that is accurate and true, and that you will keep the
      information provided up-to-date. Further, you agree that you shall not misuse the website by creating multiple
      Quake Club Rewards Programme membership accounts.</p>

    <p>Upon registration for Quake Club Rewards Programme membership at our website, you will need a password for your
      account. You are responsible for ensuring your account details including your password remain confidential,
      current, complete and accurate. You are responsible for all activities that occur under your account and/or
      password (authorised or not) as if such activities were carried out by you. You shall notify the Programme
      immediately if you become aware of or have reason for suspecting that the confidentiality of your account details,
      including your password, have been compromised or if there has been any unauthorised use of your account or if
      your personal information requires updating.</p>

    <p>Should you leave the company or industry, your account will be deactivated. If you sign in with your existing
      details, you will be required to update your current details to be able to use your account as usual and resume
      collecting Q-Perks.</p>

    <p>Quake Club Rewards Programme reserves the right to suspend or terminate your account without prior notice for any
      reason including if these Terms are violated or if it is in Quake’s best interests to do so.</p>

    <h5 style="margin-top: 50px;"><strong>4.0 Q-PERKS ALLOCATION</strong></h5>

    <p>4.1 Members will be allocated Q-Perks based on the activities that they performed when they log in their account.
    </p>

    <p>4.2 Below are the said activities. Members will earn Q-Perks every time they perform any of the activities below.
      Each Member can only earn Q-Perks for a maximum of 3 activities per day. No Q-Perks will be allocated for the 4th
      activity and onwards performed by the Member within the day.</p>
    
    <div style="margin-left: 15px;">
      <p>
        <b>a.</b> Via Activities Performed  By The Members
      </p>
  
      <table class="table table-bordered">
        <tbody>
          <tr>
            <td width="284">
              <strong>Activities</strong>
            </td>
            <td width="284">
              <strong>Q-Perks</strong>
            </td>
          </tr>
          <tr>
            <td width="284">
              First Sign Up
            </td>
            <td width="284">
              100
            </td>
          </tr>
          <tr>
            <td width="284">
              Profile Update
            </td>
            <td width="284">
              10
            </td>
          </tr>
          <!-- <tr>
          <td width="284">
          Share Article
          </td>
          <td width="284">
          20
          </td>
          </tr> -->
                  <!-- <tr>
          <td width="284">
          Download Content
          </td>
          <td width="284">
          20
          </td>
          </tr> -->
                  <!-- <tr>
          <td width="284">
          Winning Partnership Sharing
          </td>
          <td width="284">
          30
          </td>
          </tr> -->
          <tr>
            <td width="284">
              Social Media Linking
            </td>
            <td width="284">
              10
            </td>
          </tr>
          <tr>
            <td width="284">
              Join Quake WhatsApp
            </td>
            <td width="284">
              20
            </td>
          </tr>
          <tr>
            <td width="284">
              Friend Referral
            </td>
            <td width="284">
              50
            </td>
          </tr>
          <tr>
            <td width="284">
              Event attendance
            </td>
            <td width="284">
              80
            </td>
          </tr>
          <tr>
            <td width="284">
              Birthday Gift Redemption
            </td>
            <td width="284">
              200
            </td>
          </tr>
          <tr>
            <td width="284">
              Watch video on QuakeCast
            </td>
            <td width="284">
              20 (for each video) 
            </td>
          </tr>
          <tr>
            <td width="284">
              Share QuakeCast video on Social Media
            </td>
            <td width="284">
              50 (for each video)
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    
    <div style="margin-left: 15px;">
      <p>
        <b>b.</b> Via Social Media Sharing
      </p>
      <p>Members can share Quake articles and videos on social media account to earn Q-Perks.</p>
      
      <p>
        Calculate your total Q-Perks using the formula:<br>
        <div class="p-2 bg-light border"><em>
          1 engagement = 3 Q-Perks<br>
          No. of likes + No. of comments + No. of shares = Total No. of engagements<br>
          Total No. of engagements X 3 Q-Perks = Total No. of Q-Perks
        </em>
        </div>
      </p>
      
      <p>All articles and videos shared has to be shared within that quarter (Q1: 1 Jan – 31 Mar; Q2: 1 April – 30 June; Q3: 1 July – 30 September; Q4: 1 October – 31 December)</p>
      
    </div>

    <p>4.2 (a) Members can earn Q-Perks for “Profile Update” only once when they reach 100% of the profile completion.
    </p>

    <p>4.2 (b) Effective 1 October 2020, for article sharing, Members can share the same article as many times as they like, but Members will no longer earn Q-Perks for the first-time sharing.</p>

    <p>4.2 (c) Effective 1 October 2020, for content download, Members can download the same content as many times as they would like, but Members will no longer earn Q-Perks for the first-time download.
    </p>

    <p>4.3 For friend referral, a maximum of 10 successfully referred friend will entitle Member for points.</p>

    <p>4.4 Q-Perks allocated to a particular Member’s Account can neither be transferred to any other Member’s Account
      nor aggregated with Q-Perks from any other Member’s Account.</p>

    <p>4.4 For content download, Members can download the same content as many times as they would like.</p>

    <p>4.5 Q-Perks allocated in this Programme have no cash or monetary value and cannot be exchanged for cash.</p>

    <p>4.6 Q-Perks are NOT transferable to any other person or entity.</p>

    <p>4.7 From time to time, Q-Perks may also be allocated to a Member based on a promotional offer or campaign. Quake
      will, at its sole discretion, determine which campaign to allocate Q-Perks to and the number of Q-Perks to be
      accorded to the Member’s Account. Q-Perks campaigns may be announced from time to time. Members should refer to
      the Quake website quake.com.my for the latest update(s) as such from time to time.</p>

    <p>4.8 The Programme reserves the right, at its absolute discretion and without prior notice to the Members, to
      re-compute any Q-Perks allocated to the Member’s Account at any time and from time to time to correct any errors
      or inaccuracies in the allocation of the Q-Perks or due to a change in the conversion rate of the Q-Perks or in
      such other circumstances as may be determined by Quake from time to time.</p>

    <h5 style="margin-top: 50px;"><strong>5.0 Q-PERKS DEDUCTION, EXPIRATION &amp; FORFEITURE</strong></h5>

    <p>5.1 When a Member uses Q-Perks to redeem a Reward, the amount of Q-Perks used will be deducted from the then
      total amount of Q-Perks in the Member’s Account, with the Q-Perks allocated earlier being deducted first. Any
      Q-Perks, which are not used by a Member to redeem a Reward within the next 12 months</p>
    <p>in which the Q-Perks were allocated, will automatically expire and be deducted from the Q-Perks balance in a
      Member’s Account. Any unused Q-Perks allocated and accumulated will NOT be carried forward upon their expiry.
      Expired Q-Perks cannot be redeemed.</p>

    <p>5.2 Members are only eligible to redeem once every quarter of the year. In the event of their birthday, they are
      allowed to redeem within that birthday month regardless if they have redeemed in that quarter or not.</p>

    <p>5.3 Their birthday Q-Perks will be expired in the subsequent month should they choose not to redeem within their
      birthday month.</p>

    <p>5.4 Quake reserves the right, at its absolute discretion and without prior notice to the Members, to deduct or
      forfeit any Q-Perks allocated to the Member’s Account at any time and from time to time under the following
      circumstances and such other circumstances as may be determined by Quake from time to time:</p>

    <p>(a) any Q-Perks suspected to be fraudulently recorded; or</p>
    <p>(b) any Q-Perks that are inaccurately allocated or allocated in error; or</p>
    <p>(c) any Q-Perks relating to a transaction which is cancelled; or</p>
    <p>(d) if the Member’s Account is voluntarily cancelled, closed or terminated by the</p>
    <p>Member or cancelled, closed or terminated by Quake</p>

    <p>5.5 All Q-Perks should be redeemed prior to the expiry date. There will be no extension given to any expired
      Q-Perks.</p>

    <p>5.6 Q-Perks allocated to any Member’s Account do not constitute property of the Member and are not transferable
      by operation of law or otherwise to any person or entity and cannot be transferred to any other Member’s Account.
    </p>


    <h5 style="margin-top: 50px;"><strong>6.0 NOTICE OF ACCUMULATED Q-PERKS</strong></h5>

    <p>6.1 Members will be notified about their Q-Perks balance every quarter via EDM.</p>

    <p>6.2 Q-Perks balance can also be viewed at quake.com.my</p>

    <p>6.3 Disputes arising over the Q-Perks summary details must be notified to Quake within 1 (one) month from the
      date of the relevant information being published otherwise the Q-Perks summary details shall be deemed to be
      correct and binding on the Member. Members are requested to provide supporting documents where applicable to
      assist Quake in resolving the dispute. The dispute details can be emailed to <a
        href="mailto:quakeclub@astro.com.my"><u>quakeclub@astro.com.my</u></a>.</p>

    <p>6.4 Quake’s decision on any such dispute is final and binding. Quake will not be responsible for any delay in the
      posting of the transactions and/or the allocation of Q-Perks during the Programme.</p>

    <h5 style="margin-top: 50px;"><strong>7.0 GIFT REDEMPTION</strong></h5>

    <p>7.1 Quake Club will send a notification email after each redemption about the order summary. The Member will be
      sent a confirmation email within 3 working days after the notification email to inform them whether they are
      eligible for the Reward or otherwise. In the case that the Reward redeemed is not available, an item of similar
      value will replace it. Member will be informed as such in the confirmation email.</p>

    <p>7.2 Members may redeem their Q-Perks for only one transaction for every quarter with unlimited Rewards. Reward
      options include, but are not limited to, the options listed below:</p>

    <p>(a) Quake-owned products</p>
    <p>(b) Tangible merchandise or memorabilia</p>
    <p>(c) Tickets, passes, or access to shows, performances, concerts, theme parks, parties, events, etc</p>
    <p>(d) Immersive experiences organised by Quake</p>
    <p>(e) Goods and services offered by Merchants</p>
    <p>(f) Airline travel tickets</p>
    <p>(g) vouchers &amp; certificates</p>

    <p>7.3 Any props, accessories or equipment featured together with any of the Rewards in the Redemption Catalogue or
      in any other marketing materials are for ornamental purposes only and shall not form part of the Rewards.</p>

    <p>7.4 In order to redeem any particular Reward, a Member must have, at the time of the intended redemption, a
      number of Q-Perks that is equivalent to or more than the number of Q-Perks prescribed as the number of Q-Perks for
      the redemption of the said Reward. Redemption by Members with insufficient Q-Perks will be rejected.</p>

    <p>7.5 Quake reserves the right at any time and from time to time to change the Q-Perks-Reward conversion rate
      without prior notice to Members.</p>

    <p>7.6 Redemption orders once accepted by Quake cannot be revoked, cancelled, returned or exchanged, and the
      affected Q-Perks will not be reinstated.</p>

    <p>7.7 All Rewards are subject to availability. We reserve the right to modify or cancel any Reward at any time.
      Certain Rewards are available only during the time periods described in the Programme communications (including
      the Programme website). Certain restrictions apply to Rewards. Terms and conditions of each Reward are set forth
      in the Programme communications and/or on the certificates/vouchers. Merchants participating in the Programme are
      subject to change. Some Rewards have limited availability.</p>

    <p>7.8 There is NO trial period for all Rewards redeemed from this Programme. Members are required to inspect the
      Reward(s) redeemed (where applicable) immediately upon receipt of the Reward(s).</p>

    <p>7.9 For all the electrical items redeemed with Quake, members can liaise directly with the merchant for any
      enquiries or issues. Quake gives no representation or warranty with respect to any goods and/or services featured
      in the Redemption Catalogue or other channels of redemption. In particular, Quake gives no warranty with respect
      to the quality of the redemption item or their suitability for any purpose. However, Members may liaise directly
      with the Merchant in respect of any warranty relating to the relevant goods and/or services. Quake will not be
      liable for any death, injury, direct or consequential loss, theft or damage of any nature that the Member may
      suffer arising from redemption of the Rewards.</p>

    <p>7.10 All Rewards unclaimed after 2 months from the redemption date will be deemed as void and forfeited. Q-Perks
      will not be reinstated to the Member’s Account nor will the validity of existing Q-Perks be extended. Members are
      advised to contact Quake if the redemption item or such other applicable form of redemption notification by Quake
      has not been received after a lapse of 14 days from the redemption date.</p>

    <p>7.11 All Rewards are subject to such terms and conditions as are mentioned in the current redemption item
      schedule or in any other terms and conditions relating to the redemption item, as determined by the Merchants,
      including any ticket for airline travel. It is the Member’s responsibility to satisfy any Terms and Conditions
      imposed by an airline or other Merchants including advance booking requirements and any restrictions and/or fees
      payable in respect of the cancellation or alteration of tickets.</p>

    <p>7.12 Rewards in the form of certificates/vouchers are valid for use only at participating outlets or Merchants as
      mentioned on the certificates/vouchers and only for the specific item/reward mentioned therein. The
      certificates/vouchers are valid for use until the date specified and subject to the Terms and Conditions (which
      includes booking requirements, cancellation restrictions, warranties and limitations of liability) therein. If
      certificates/vouchers remain unused after their respective specified date, the certificates/vouchers will lapse
      and will not be replaced. Issuance of dining, travel or hotel accommodation voucher does not constitute a
      reservation. The entitled Member is responsible for notifying and making all reservations. Quake does not accept
      liability whatsoever (including negligence) with respect to the Rewards supplied nor in connection with any
      Merchant’s refusal to accept certificates/vouchers issued by Quake for the purpose of redeeming these items. Any
      disputes arising from this are solely between the Members and Merchants. Members are strongly advised to adhere to
      safety precautions and instructions when participating in outdoor activities, as Quake will not be held
      responsible for any outcome as a result of Members’ participation in the activity. In any cases of lost vouchers,
      Quake will not be held responsible either under such circumstances. Other general terms and conditions on
      certificates/vouchers are as follows: </p>

    <p>▪ The certificate/voucher is only valid for use at participating outlets or Merchants as mentioned on the
      certificate/voucher and only on specific Rewards mentioned therein.</p>
    <p>▪ The certificate/voucher is only valid for use before the expiry date indicated on it and cannot be extended.
    </p>
    <p>▪ Cancellation or expiry of certificate/voucher shall not result in the reinstatement of Q-Perks.</p>
    <p>▪ The certificate/voucher is not refundable or exchangeable for cash.</p>
    <p>▪ If the Ringgit value of the item purchased by the use of the certificate/voucher is less than the Ringgit value
      of the certificate/voucher, Quake shall not in any way be liable to refund the Ringgit equivalent of the balance
      amount.</p>
    <p>▪ It is the Member’s responsibility to make their own reservation with the participating outlets or Merchants
      where required.</p>
    <p>▪ Members may be requested to present their current Identification Card/Passport, original certificates/vouchers
      upon redemption at the selected participating outlets or Merchants.</p>
    <p>▪ Only original certificates/vouchers will be accepted. Participating outlets or Merchants will not accept
      damaged, defaced or photocopied certificates/vouchers.</p>
    <p>▪ The participating outlet or Merchant also reserves the right to decline a certificate/voucher if found to be
      forged, tampered, expired and if the certificate/voucher details do not match the Member's identification
      card/passport.</p>
    <p>▪ Quake or the participating outlets/ Merchants will not replace lost, stolen, damaged and expired
      certificate/vouchers.</p>
    <p>▪ Participating outlets or Merchants’ own Terms and Conditions (which includes reservation requirements,
      cancellation restrictions, warranties and limitations) will also apply to the use of the certificates/vouchers.
    </p>

    <p>7.13 Redeemed Rewards are not refundable, exchangeable, replaceable, redeemable or transferable for cash, credit,
      other rewards or Q-Perks under any circumstances.</p>

    <p>7.14 Quake will not be liable for any death, injury, special, punitive, direct, indirect or consequential loss,
      theft or damage of any nature howsoever arising that the Member may suffer arising from redemption of the
      Reward(s).</p>

    <h5 style="margin-top: 50px;"><strong>8.0 HOW TO REDEEM THE REWARDS</strong></h5>

    <p>8.1 Any Member who wishes to use Q-Perks to redeem a Reward is required to log on to quake.com.my/login . Upon
      successful sign-in, the following steps apply:</p>

    <ol type="i">
      <li>Browse and select desired Reward item to redeem</li>
      <li>Check if you have sufficient points</li>
      <li>Add to cart</li>
      <li>Check out</li>
      <li>Fill up the details and upload name card if you have yet to do so in “My Account” section</li>
      <li>Item is processing</li>
    </ol>


    <p>Member will subsequently receive a notification from Quake acknowledging receipt of the redemption request.</p>

    <p>8.2 The Rewards redeemed will be delivered to the address registered with Quake subject to eligibility and stock
      availability (refer to Clause 7.1).</p>

    <p>8.3 Once Quake issues to the Member the receipt acknowledgment notification in respect of the Member’s redemption
      request, there shall be no revocation or cancellation of the said redemption request or return or exchange of the
      redeemed Rewards</p>

    <h5 style="margin-top: 50px;"><strong>9.0 DELIVERY OF REDEMPTION REQUEST </strong></h5>

    <p>9.1 All Rewards or, as the case may be, Reward redemption notifications, would be delivered or sent on an “as is”
      and “as available” basis within 14 days of receiving the relevant redemption request from the Member. Rewards will
      be delivered to the current address of the Member shown in Quake’s record or any other address as authorised by
      the Member.</p>

    <p>The delivery charges for the first attempt of delivery will be borne by Quake to East and West Malaysia.</p>

    <p>Quake will not deliver to PO Box addresses and addresses outside Malaysia. Member/recipient of Reward(s) is
      obliged to present necessary ID documentation to the delivery staff, failing which the delivery staff has the
      right to refuse delivery and will return the redemption item to Quake as unclaimed.</p>

    <p>Delivery will only be made against a written acknowledgement of receipt of the Rewards and of satisfaction with
      its physical condition by any occupant at the address of delivery or where such address is an office address, by
      any personnel at the office. Such acknowledgement shall be deemed to be the acknowledgement by the Member. All
      charges of second and subsequent delivery attempts due to unsuccessful delivery by the courier agent will be borne
      by the Member.</p>

    <p> 9.2 Members are advised to examine all Rewards upon receipt. All goods and services supplied will be covered by
      the Merchant’s normal delivery terms of business. Except where the law provides otherwise, Quake will not be
      responsible for the quality or suitability of the goods or services or for any delay in delivery. If a Member
      finds the redemption item faulty/damaged, he/she is requested to contact directly with the respective Merchants
      according to the warranty information.</p>

    <p>9.3 As for Quake merchandise, if a Member finds the redemption item faulty/damaged, the Member can email <u><a
          href="mailto:quakeclub@astro.com.my">quakeclub@astro.com.my</a></u> 3 days from the received date. Any
      disputes after 3 days will not be entertained.</p>

    <p>9.4 If neither a Member nor any person on the Member’s behalf is available to receive a Reward at the delivery
      address on the date of the delivery of the Reward, the Member is advised to liaise directly with the relevant
      courier service company within the specified time frame and at the location as stated in the “sorry card” (dropped
      or left by the courier service company at the delivery address).</p>

    <p>9.5 The delivery of certain Rewards shall require an additional charge for shipping and handling which charge
      shall be borne by the Member. Quake may impose a separate delivery or courier charge or, where appropriate, deduct
      Q-Perks from Member’s account for delivery charges under the following circumstances:</p>

    <p> (a) Re-delivery of Rewards that have been returned as a result of being unclaimed; or</p>
    <p>(b) Re-delivery of Rewards that have been returned under the following circumstances i.e. incomplete address,
      non-Malaysian addresses, person has shifted, no such person or for any other failed delivery reasons.</p>

    <p>9.6 Quake gives no warranty (whether expressed or implied) whatsoever with respect to Rewards items acquired
      under the Programme. In particular, Quake gives no warranty with respect to the quality of items acquired or their
      suitability for any purpose.</p>

    <p>9.7 Quake shall not be liable for any loss or damage whatsoever that may be suffered, or for any personal injury
      that may be suffered, to a Member, directly or indirectly, by use or non-use of Rewards redeemed under the
      Programme. For the purpose of covering the delivery of redemption Rewards under these Terms and Conditions, "force
      majeure" shall be deemed to be any cause affecting the performance of these Terms and Conditions arising from or
      attributable to acts, events, omissions or accidents beyond the reasonable control of the party and without
      limiting the generality thereof shall include the following:</p>

    <p>(a) strikes, lock-outs or other industrial action;</p>
    <p>(b) civil commotion, riot, invasion, war threat or preparation for war;</p>
    <p>(c) fire, explosion, storm, flood, earthquake, subsidence, epidemic or other natural physical disaster; or</p>
    <p>(d) any changes in law and regulations, which have a material impact on the performing abilities of either Party.
    </p>

    <p>9.8 Quake reserves the right at any time, without prior notice, to add / alter / modify / change (or) vary all of
      these Terms &amp; Conditions or to replace wholly, or in part, the Reward(s) with other Reward(s), whether similar
      or otherwise to the first-mentioned Rewards, or to withdraw any Reward altogether.</p>

    <p>9.9 Any dispute and/or complaints regarding the goods or services received as a Reward under the Programme shall
      be settled between the Member and the Merchants which had supplied the goods or services. Quake will bear no
      responsibility for resolving such disputes and/or complaints, or for the dispute/complaint itself.</p>

    <p>(a) if it is given by such Party or its solicitors by post in a registered letter addressed to the other Party at
      its address herein mentioned or to its solicitors’ address and such notice shall be deemed to have been duly
      served at the time when such registered letter would in the ordinary course of post be delivered; or</p>

    <p>(b) if it is dispatched by such Party or its solicitors by hand or by courier to the other Party at its address
      hereinbefore mentioned or to its solicitors’ address and such notice shall be deemed to have been received at the
      time when the same is delivered; or</p>

    <p>(c) If it is transmitted by such Party or its solicitors by facsimile to the other Party to its facsimile number
      or to its solicitors’ facsimile from time to time notified by the same and such notice shall be deemed to have
      been received at the time of transmission provided that there has been a confirmed answerback.</p>

    <h5 style="margin-top: 50px;"><strong>10.0 YOUR USE OF CONTENT ON THE WEBSITE</strong></h5>

    <p>10.1. To the fullest extent permitted by law, Quake is not and shall not be liable for any statements,
      representations, or Content provided by its users in any public forum or any Social sites.</p>

    <p>10.2 More generally, Content posted via any Social sites is not controlled by Quake. We cannot guarantee the
      accuracy, integrity or quality of such Content. You understand that Quake shall not be liable for any such
      Content.</p>

    <p>10.3 To the maximum extent permitted by law, Quake will not be liable in any way for any Content, including, but
      not limited to, for (i) any errors or omissions in any Content; or (ii) any loss or damage (including, without
      limitation, personal injury or property damage) of any kind incurred as a result of any Content posted, emailed or
      otherwise transmitted via or to the website and Social sites.</p>

    <p>10.4 You may access the Content and any other content on the website only as permitted under these Terms and the
      Privacy Policy and you agree to not engage in the use, copying or distribution of any of the Content other than as
      expressly provided herein.</p>

    <p>10.5 You agree not to circumvent, disable or otherwise interfere with security-related features of the website or
      features that prevent or restrict use of any Content or enforce limitations on use of the website or the Content.
      You may not interfere with or disrupt the website, or servers or networks connected to the website, or disobey any
      requirements, procedures, policies or regulations of networks connected to the website, including by using any
      device, software or routine to bypass robot exclusion headers.</p>

    <h5 style="margin-top: 50px;"><strong>11.0 YOUR CONTENT SUBMISSIONS</strong></h5>

    <p>11.1 You are solely responsible for all content that you upload, email or otherwise transmit via or to the
      website, or otherwise, including the submission of profile information, or other materials (collectively,
      “Content”). We will not accept Content from you unless you are a registered user of the website. By submitting
      Content to Quake, you represent and warrant that:</p>

    <p>11.2 Quake does not endorse any Content or any opinion, recommendation or advice expressed therein, and Quake
      disclaims all liability with respect to the Content.</p>

    <p>11.3 If your Content includes ideas, suggestions, documents or proposals to Quake through Quake communication
      channel(s):</p>
    <ul>
      <li>such Content is not confidential or proprietary and Quake has no obligation of confidentiality, express or
        implied, with respect thereto;</li>
      <li>Quake may have something similar to that Content already under consideration or development; and</li>
      <li>you are not entitled to compensation, payment or reimbursement of any kind for such Content from Quake under
        any circumstances unless you are otherwise notified by Quake in writing.</li>
    </ul>

    <h5 style="margin-top: 50px;"><strong>12.0 TERMINATION OF PROGRAMME </strong></h5>

    <p>12.1 Quake reserves the right to suspend or terminate the Programme at any time giving 1 month prior notice. In
      such a case, Quake shall give advance written notice to Members in the manner it deems appropriate.</p>

    <p>12.2 Termination of the Programme will take effect on the date stated in the notice. Members will be advised to
      use any outstanding Q-Perks within a stipulated period. All outstanding Q-Perks will be automatically cancelled
      upon the expiry of this period.</p>

    <h5 style="margin-top: 50px;"><strong>13.0 POLICY ON PERSONAL DATA </strong></h5>

    <p>13.1 By participating in this Programme, you consent to the processing of your Personal Data in Clause 13.2.</p>

    <p><strong>13.2 Privacy Notice</strong></p>

    <p><strong>Information we collect</strong></p>
    <p>Information you provide to us: In the course of engaging with Quake Club, you may provide Personal Information
      about you. Personal information is often, but not exclusively provided to us when you sign up for and use to
      redeem, send us an email or communicate with us in any other way. We will let you know prior to collection whether
      the provision of Personal Information we are collecting is compulsory or if it may be provided on a voluntary
      basis and the consequences, if any, of not providing the information. By giving us this information, you agree to
      this information being collected, used and disclosed.</p>

    <p><strong>Information from website browser</strong></p>
    <p>If you’re browsing the website, we collect the same basic information that most website collect. We use common
      Internet technologies, such as cookies and web server logs. We collect this from all the users whether they have
      an account or otherwise. We collect this information to better understand how our website visitors use Quake Club,
      and to monitor and protect the security of the website.</p>

    <p><strong>Information from Members</strong></p>
    <p>If you create an account, we require some basic information at the time of account creation. You will create your
      login details with your own email and password and we will request for a valid email account. You also have the
      option to give us more information and this may include Member’s Data. User personal information does not include
      aggregated, non-personally identifying information. We may use aggregated, non-personally identifying information
      to operate, improve, and optimise our website and service.</p>

    <p><strong>How Members’ information is processed and shared</strong></p>
    <p>Quake Club may access and use the data we collect as necessary (a) to provide and maintain the reward redemption;
      (b) to address and respond to service, security, and customer support issues; (c) to detect, prevent, or otherwise
      address fraud, security, unlawful, or technical issues; (d) as required by law; (e) to fulfill our contracts; (f)
      to improve and enhance the redemption; (g) to provide analysis or valuable information back to our Members.</p>

    <p><strong>Information Sharing</strong></p>
    <p>Ensuring your privacy is important to us. We do not share your personal information with third parties except as
      described in this privacy policy. We may share your personal information with (a) third party service providers;
      (b) business partners; (c) affiliated companies within our corporate structure and (d) as needed for legal
      purposes. Third party service providers have access to personal information only as needed to perform their
      functions and they must process the personal information in accordance with this Privacy Policy.</p>
    <p>Examples of how we may share information with service providers include:</p>
    <ul>
      <li>Fulfilling redemption orders</li>
      <li>Providing Member support</li>
      <li>Sending marketing communications</li>
      <li>Conducting research and analysis</li>
      <li>Providing cloud computing infrastructure</li>
    </ul>
    <p>Examples of how we may disclose data for legal reasons include:</p>
    <ul>
      <li>As part of a merger, sale of company assets, financing or acquisition of all or a portion of our business by
        another company where customer information will be one of the transferred assets.</li>
      <li>As required by law, for example, to comply with a valid subpoena or other legal process; when we believe in
        good faith that disclosure is necessary to protect our rights, or to protect your safety (or the safety of
        others); to investigate fraud; or to respond to a government request.</li>
    </ul>
    <p>We may also disclose your personal information to any third party with your prior consent.</p>
    <p><strong>Cookies Information</strong></p>
    <p>When you visit Quake Club, we may send one or more cookies — a small text file containing a string of
      alphanumeric characters — to your computer that uniquely identifies your browser and lets Quake Club help you log
      in faster and enhance your navigation through the website. A cookie may also convey information to us about how
      you use Quake Club (e.g., the pages you view, the links you click and other actions you take on the Quake Club),
      and allow us or our business partners to track your usage of Quake Club over time.</p>
    <p>You can control or reset your cookies through your web browser, which will allow you to customise your cookie
      preferences and to refuse all cookies or to indicate when a cookie is being sent. However, some features of the
      Quake Club may not function properly if the ability to accept cookies is disabled.</p>
    <p>13.3 Member may request for limiting the processing of the Member’s Personal Data at any time hereafter by
      submitting such request to Quake in writing via email to <a
        href="mailto:quakeclub@astro.com.my"><u>quakeclub@astro.com.my</u></a>. Any inquiries or complaints with respect
      to the Member’s Personal Data should also be channeled to Quake in this manner.</p>

    <p>13.4 Member may refer to Astro's Privacy Notice at this link [ <a
        style="text-decoration:underline;color;transition:"
        href="http://www.quake.com.my/privacy-notice">http://www.quake.com.my/privacy-notice</a> ]</p>

    <h5 style="margin-top: 50px;"><b>14.0 MISCELLANEOUS</b></h5>

    <p>14.1 Q-Perks, and any rights they confer, cannot be sold, transferred, assigned or otherwise dealt with except in
      accordance with these Terms and Conditions. Q-Perks have no cash or monetary value. The redemption Q-Perks
      displayed in the website are correct at the time of publication and are subject to change from time to time. Quake
      may, at its absolute discretion, allow the transfer of Q-Perks under certain exceptional circumstances. Suspected
      or actual fraud and/or suspected or actual misuse relating to the accumulation of Reward Q-Perks in the Programme
      may result in forfeiture of accumulated Q-Perks as well as cancellation of a Member’s participation in the
      Programme. All questions or disputes regarding eligibility for the Programme or the eligibility of Q-Perks of
      accrual will be determined by Quake at its sole discretion.</p>

    <p>14.2 Quake is not liable under the following circumstances:-</p>
    <p>▪ Any unauthorised redemption of Q-Perks</p>
    <p>▪ Any unauthorised use of Q-Perks or unauthorised sale of Q-Perks</p>
    <p>▪ Any loss, theft or damage to any Rewards in the course of delivery</p>
    <p>▪ Any Rewards not being available for any reason</p>
    <p>▪ Any failure to notify Members of any changes in these Terms and Conditions, the Rewards offered, qualifying
      goods and services and/or the number of Q-Perks</p>
    <p>▪ The suspension and/or termination of the Programme</p>
    <p>▪ Any technical failure of system, including Quake’s webwebsite, servicing hotline and other systems which may
      impede a Member’s transaction.</p>

    <p>14.3 All conditions and warranties whether expressed or implied and whether arising under legislation or
      otherwise, as to the condition, suitability, quality, fitness or safety of any redemption Rewards supplied under
      the Programme are expressly excluded to the fullest extent permitted by law. Any liability Quake may have to a
      Member under legislation in respect of such redemption Rewards which cannot be excluded is limited, where
      permitted, to supplying, or paying the cost of supplying the goods or services again or repairing, or paying the
      costs of repairing the goods, at Quake’s option. Neither Merchants nor Programme Partners shall have any
      authority, expressed or implied, to make any representation, warranty or statement on behalf of Quake.</p>

    <p>14.4 Quake reserves the right to assign, sell or transfer the Programme or any of the rights and obligations
      under these Terms &amp; Conditions without the Member’s consent and may disclose or transfer all information held
      about Members to such third party.</p>

    <p><strong>14.5 Variation of Rules </strong></p>
    <p>Quake reserves the right at its absolute discretion to amend, delete or add to any of these Terms and Conditions
      from time to time without prior notice, including but not limited to conversion rate of the Q-Perks. By
      participating in this redemption Programme, the Member is deemed to have agreed to be bound by these Terms &amp;
      Conditions and any decisions of Quake. Quake may change, at any time and from time to time, without prior notice
      to the Members, these Terms &amp; Conditions, the Rewards and/or the Merchants and/or Programme Partners.</p>

    <p><strong>14.6 Governing Law </strong></p>
    <p>These Terms &amp; Conditions shall be governed by the laws of Malaysia.</p>

    <p><strong>14.7 Language </strong></p>
    <p>The Parties agree that all notices and other communications under or in connection with these Terms &amp;
      Conditions shall be in English. In the event where there is a discrepancy in communication to Members using Bahasa
      Malaysia, Mandarin, Tamil or any other language, the English version will prevail.</p>


    <h5 style="margin-top: 50px;"><strong>15.0 Contacting Quake</strong></h5>

    <p>In the event that the Member, who is the subject of the personal data supplied pursuant to these Terms and
      Conditions, becomes aware that their related information is inaccurate, incomplete, misleading or needs updating
      in any respect, or wishes to access such information, they should make such request in writing by:</p>

    <p>Logging on to our website and updating your most recent details at ‘Update Profile’ or email to <a
        href="mailto:quakeclub@astro.com.my"><u>quakeclub@astro.com.my</u></a></p>
  </div>
</div>