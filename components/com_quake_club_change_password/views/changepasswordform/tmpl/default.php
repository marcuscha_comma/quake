<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_change_password
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
?>
<div id="app">
	<div class="pink-page-title">
		 <div class="container">
			 <h2>Change Password</h2>
		 </div>
	</div>

	<div class="row">
		<div class="col-md-8 col-lg-6">
			<form id="" @submit="checkFormValidate" action="<?php echo JRoute::_('index.php?option=com_quake_club_change_password&task=changepasswordform.changePassword'); ?>" method="post" class="form-validate club-form" enctype="multipart/form-data" novalidate>
				<p class="font-brand mb-4">Fill in the details below to reset your password.</p>
				<div class="field-placey form-group">
					<div class="form-text form-error" v-if="!checkExistingPassword">Please fill in your existing password</div>
					<i class="far fa-eye-slash toggle-password" ></i>

					<input id="epassword" class="form-control" :class="{'check-validate': !checkExistingPassword}"
						name="jform[epassword]" type="password" placeholder=" " required>
					<label for="epassword">Existing password</label>
				</div>
				<div class="field-placey form-group">
					<div class="form-text form-error" v-if="!checkPassword1">Please fill in your new password</div>
					<div class="form-text">Must contain at least one number and one alphabet, and at least 8 or more characters. Password is case sensitive.</div>
					<i class="far fa-eye-slash toggle-password" ></i>

					<input id="jform_password1" class="form-control" :class="{'check-validate': !checkPassword1}"
						name="jform[jform_password1]" type="password" placeholder=" " required pattern="(?=.*\d)(?=.*[a-z]).{8,}">
					<label for="jform_password1">New password</label>
				</div>
				<div class="field-placey form-group">
					<div class="form-text form-error" v-if="!checkPassword2">Please retype your new password</div>
					<i class="far fa-eye-slash toggle-password" ></i>
					<input id="jform_password2" class="form-control" :class="{'check-validate': !checkPassword2}"
						name="jform[jform_password2]" type="password" placeholder=" " required pattern="(?=.*\d)(?=.*[a-z]).{8,}">
					<label for="jform_password2">Retype your password</label>
				</div>
				<button type="submit" class="btn btn-pink btn-wide">
					Update now
				</button>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">

    var app = new Vue({
        el: '#app',
        data: {
			checkExistingPassword : true,
			checkPassword1 : true,
			checkPassword2 : true
        },
        mounted: function () {
        },
        updated: function () {
        },
        methods: {
			checkFormValidate: function(e){
				this.checkExistingPassword = epassword.checkValidity();
				this.checkPassword1 = jform_password1.checkValidity();
				this.checkPassword2 = jform_password2.checkValidity();
				if (epassword.checkValidity() && jform_password1.checkValidity() && jform_password2.checkValidity()) {
					return true;
				}
				e.preventDefault();
			}
        }
    })

		jQuery(".toggle-password").click(function() {

			jQuery(this).toggleClass("fa-eye fa-eye-slash");
			var input = jQuery(this).next();
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});
</script>
