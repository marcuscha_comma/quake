<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_change_password
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Session\Session;
use \Joomla\CMS\Language\Text;

/**
 * Changepassword controller class.
 *
 * @since  1.6
 */
class Quake_club_change_passwordControllerChangepasswordForm extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit($key = NULL, $urlVar = NULL)
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_quake_club_change_password.edit.changepassword.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_quake_club_change_password.edit.changepassword.id', $editId);

		// Get the model.
		$model = $this->getModel('ChangepasswordForm', 'Quake_club_change_passwordModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_quake_club_change_password&view=changepasswordform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return void
	 *
	 * @throws Exception
	 * @since  1.6
	 */
	public function save($key = NULL, $urlVar = NULL)
	{
		// Check for request forgeries.
		Session::checkToken() or jexit(Text::_('JINVALID_TOKEN'));

		// Initialise variables.
		$app   = Factory::getApplication();
		$model = $this->getModel('ChangepasswordForm', 'Quake_club_change_passwordModel');

		// Get the user data.
		$data = Factory::getApplication()->input->get('jform', array(), 'array');

		// Validate the posted data.
		$form = $model->getForm();

		if (!$form)
		{
			throw new Exception($model->getError(), 500);
		}

		// Validate the posted data.
		$data = $model->validate($form, $data);

		// Check for errors.
		if ($data === false)
		{
			// Get the validation messages.
			$errors = $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				}
				else
				{
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			$input = $app->input;
			$jform = $input->get('jform', array(), 'ARRAY');

			// Save the data in the session.
			$app->setUserState('com_quake_club_change_password.edit.changepassword.data', $jform);

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_quake_club_change_password.edit.changepassword.id');
			$this->setRedirect(Route::_('index.php?option=com_quake_club_change_password&view=changepasswordform&layout=edit&id=' . $id, false));

			$this->redirect();
		}

		// Attempt to save the data.
		$return = $model->save($data);

		// Check for errors.
		if ($return === false)
		{
			// Save the data in the session.
			$app->setUserState('com_quake_club_change_password.edit.changepassword.data', $data);

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_quake_club_change_password.edit.changepassword.id');
			$this->setMessage(Text::sprintf('Save failed', $model->getError()), 'warning');
			$this->setRedirect(Route::_('index.php?option=com_quake_club_change_password&view=changepasswordform&layout=edit&id=' . $id, false));
		}

		// Check in the profile.
		if ($return)
		{
			$model->checkin($return);
		}

		// Clear the profile id from the session.
		$app->setUserState('com_quake_club_change_password.edit.changepassword.id', null);

		// Redirect to the list screen.
		$this->setMessage(Text::_('COM_QUAKE_CLUB_CHANGE_PASSWORD_ITEM_SAVED_SUCCESSFULLY'));
		$menu = Factory::getApplication()->getMenu();
		$item = $menu->getActive();
		$url  = (empty($item->link) ? 'index.php?option=com_quake_club_change_password&view=changepasswords' : $item->link);
		$this->setRedirect(Route::_($url, false));

		// Flush the data from the session.
		$app->setUserState('com_quake_club_change_password.edit.changepassword.data', null);
	}

	/**
	 * Method to abort current operation
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function cancel($key = NULL)
	{
		$app = Factory::getApplication();

		// Get the current edit id.
		$editId = (int) $app->getUserState('com_quake_club_change_password.edit.changepassword.id');

		// Get the model.
		$model = $this->getModel('ChangepasswordForm', 'Quake_club_change_passwordModel');

		// Check in the item
		if ($editId)
		{
			$model->checkin($editId);
		}

		$menu = Factory::getApplication()->getMenu();
		$item = $menu->getActive();
		$url  = (empty($item->link) ? 'index.php?option=com_quake_club_change_password&view=changepasswords' : $item->link);
		$this->setRedirect(Route::_($url, false));
	}

	/**
	 * Method to remove data
	 *
	 * @return void
	 *
	 * @throws Exception
     *
     * @since 1.6
	 */
	public function remove()
    {
        $app   = Factory::getApplication();
        $model = $this->getModel('ChangepasswordForm', 'Quake_club_change_passwordModel');
        $pk    = $app->input->getInt('id');

        // Attempt to save the data
        try
        {
            $return = $model->delete($pk);

            // Check in the profile
            $model->checkin($return);

            // Clear the profile id from the session.
            $app->setUserState('com_quake_club_change_password.edit.changepassword.id', null);

            $menu = $app->getMenu();
            $item = $menu->getActive();
            $url = (empty($item->link) ? 'index.php?option=com_quake_club_change_password&view=changepasswords' : $item->link);

            // Redirect to the list screen
            $this->setMessage(Text::_('COM_QUAKE_CLUB_CHANGE_PASSWORD_ITEM_DELETED_SUCCESSFULLY'));
            $this->setRedirect(Route::_($url, false));

            // Flush the data from the session.
            $app->setUserState('com_quake_club_change_password.edit.changepassword.data', null);
        }
        catch (Exception $e)
        {
            $errorType = ($e->getCode() == '404') ? 'error' : 'warning';
            $this->setMessage($e->getMessage(), $errorType);
            $this->setRedirect('index.php?option=com_quake_club_change_password&view=changepasswords');
        }
	}
	
	public function changePassword(){

		$app    = JFactory::getApplication();
		$user   = JFactory::getUser();
		$userId = (int) $user->get('id');
		$db = JFactory::getDbo();
		$jdate = new JDate;

		$requestData = $app->input->post->get('jform', array(), 'array');
		$passwordMatch = JUserHelper::verifyPassword($requestData['epassword'], $user->password, $user->id);

		if ($passwordMatch && ($requestData['jform_password1'] == $requestData['jform_password2'])) {
			$userObj = JFactory::getUser($userId);
	
			$password = array('password' => $requestData['jform_password1'], 'password2' => $requestData['jform_password2']);
			$userObj->bind($password);
			$userObj->save();

			$app->enqueueMessage(Text::_('COM_QUAKE_CLUB_CHANGE_PASSWORD_SAVED_SUCCESSFULLY'));
			$app->redirect(JUri::base().'user-profile/profile');
		}else{
			$app->enqueueMessage(Text::_('COM_QUAKE_CLUB_CHANGE_PASSWORD_SAVED_FAIL'), 'warning');
			$app->redirect(JUri::base().'quake-club/change-password');
		}
	}
}
