<?php
/**
 * @version    CVS: 1.0.2
 * @package    Com_Our_work_categories
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Our_work_categories', JPATH_COMPONENT);
JLoader::register('Our_work_categoriesController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Our_work_categories');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
