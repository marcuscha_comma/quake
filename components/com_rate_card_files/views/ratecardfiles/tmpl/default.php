<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Programme_scheduler
 * @author     ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user        = JFactory::getUser();
$userId      = $user->get('id');
$checkEmailUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.checkEmailExist');
$createClientUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.createClient');
$preSetTab = $_COOKIE['_tab'];
// echo $downloadUrl;
?>


<div id="app">
  <div class="tab-pills scrollable-tab">
    <div class="row">
      <div class="col"><a href="./credentials" class="tab-pill">Credentials</a></div>
      <div class="col"><a href="./channel-profile" class="tab-pill">Brand Profiles</a></div>
      <div class="col"><a href="./rate-card" class="tab-pill active">Rate Cards</a></div>
      <div class="col"><a href="./programme-schedules" class="tab-pill">TV Programme Schedules</a></div>
      <div class="col"><a href="./packages" class="tab-pill">Advertising Packages</a></div>
    </div>
  </div>

  <div>
    <h2 class="section-title">Rate Cards</h2>
    <p>Download our latest rate cards here.</p>
  </div>

  <nav class="nav medium-tab">
    <a class="nav-link" @click="tabOnCLick('rctv')" :class="{ active : tvTabIsShow }">
      <div class="medium-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-420 -641)"><g transform="translate(424 601.06)"><g transform="translate(0 42.667)"><path d="M26.376,42.667H1.758A1.761,1.761,0,0,0,0,44.425V60.837A1.761,1.761,0,0,0,1.758,62.6h9.964v1.263l-6.544,1.09a.586.586,0,0,0,.1,1.164.544.544,0,0,0,.1-.008l6.985-1.164h3.422L22.762,66.1a.552.552,0,0,0,.1.008.586.586,0,0,0,.095-1.164l-6.542-1.09V62.6h9.964a1.761,1.761,0,0,0,1.758-1.758V44.425A1.761,1.761,0,0,0,26.376,42.667ZM15.24,63.768H12.9V62.6H15.24Zm11.723-2.931a.586.586,0,0,1-.586.586H1.758a.586.586,0,0,1-.586-.586V44.425a.586.586,0,0,1,.586-.586H26.376a.586.586,0,0,1,.586.586Z" transform="translate(0 -42.667)" fill="#989898"/><path d="M65.526,85.333H43.252a.586.586,0,0,0-.586.586V99.987a.586.586,0,0,0,.586.586H65.526a.586.586,0,0,0,.586-.586V85.919A.586.586,0,0,0,65.526,85.333ZM64.939,99.4h-21.1v-12.9h21.1Z" transform="translate(-40.321 -82.988)" fill="#989898"/></g></g><rect width="36" height="28" transform="translate(420 641)" fill="none"/></g></svg>
      </div>
      TV
    </a>
    <a class="nav-link" @click="tabOnCLick('rcradio')" :class="{ active : radioTabIsShow }">
      <div class="medium-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-518 -641)"><g transform="translate(522.861 610.73)"><g transform="translate(0 34.005)"><path d="M23.642,38.546H6.888L17.7,35.212a.637.637,0,0,0,.394-.758l0-.015a.606.606,0,0,0-.753-.409L2.788,38.515a.647.647,0,0,0-.068.03h-.3A2.425,2.425,0,0,0,0,40.971V53.7a2.425,2.425,0,0,0,2.425,2.425H23.642A2.425,2.425,0,0,0,26.066,53.7V40.971A2.425,2.425,0,0,0,23.642,38.546ZM24.854,53.7a1.212,1.212,0,0,1-1.212,1.212H2.425A1.212,1.212,0,0,1,1.212,53.7V40.971q0-.031,0-.061a1.182,1.182,0,0,1,1.212-1.151H23.7a1.182,1.182,0,0,1,1.151,1.212Z" transform="translate(0 -34.005)" fill="#989898"/><path d="M56.4,245.091c-.061-.061-.091-.091-.152-.091a3.82,3.82,0,0,0-2-.546,3.729,3.729,0,0,0-2,.546.273.273,0,0,0-.182.091,4,4,0,0,0,0,6.7.273.273,0,0,0,.182.091,3.728,3.728,0,0,0,2,.546,3.819,3.819,0,0,0,2-.546c.061,0,.091-.03.152-.091a3.993,3.993,0,0,0,0-6.7Zm-4.516,4.789a2.365,2.365,0,0,1-.394-1.425A2.394,2.394,0,0,1,51.885,247Zm1.819,1.273c-.3-.03-.606-.121-.606-.182V245.91c0-.061.3-.152.606-.182Zm1.819-.182a2.031,2.031,0,0,0-.606.182v-5.425a2.033,2.033,0,0,0,.606.182Zm1.212-1.091V247c0,.424.394.909.394,1.455S56.734,249.456,56.734,249.88Z" transform="translate(-47.338 -232.245)" fill="#989898"/><path d="M317.957,271.11a2.425,2.425,0,1,0,2.455,2.455c0-.02,0-.04,0-.06A2.425,2.425,0,0,0,317.957,271.11Zm.06,3.637a1.212,1.212,0,1,1,1.006-1.388A1.212,1.212,0,0,1,318.018,274.747Z" transform="translate(-297.255 -257.354)" fill="#989898"/><path d="M223.915,271.11a2.425,2.425,0,1,0,2.455,2.455c0-.02,0-.04,0-.06A2.425,2.425,0,0,0,223.915,271.11Zm.06,3.637a1.212,1.212,0,1,1,1.006-1.388A1.212,1.212,0,0,1,223.976,274.747Z" transform="translate(-208.669 -257.354)" fill="#989898"/><path d="M57.788,151.466H37.177a.606.606,0,0,0-.606.606v3.637a.606.606,0,0,0,.606.606H57.788a.606.606,0,0,0,.606-.606v-3.637A.606.606,0,0,0,57.788,151.466Zm-.606,3.637H51.423v-1.212a.606.606,0,1,0-1.212,0V155.1H37.783v-2.425h19.4V155.1Z" transform="translate(-34.449 -144.651)" fill="#989898"/></g></g><rect width="36" height="28" transform="translate(518 641)" fill="none"/></g></svg>
      </div>
      Radio
    </a>
    <a class="nav-link" @click="tabOnCLick('rcdigital')" :class="{ active : digitalTabIsShow }">
      <div class="medium-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-630 -641)"><g transform="translate(40.41 527.503)"><path d="M607.021,118.4a.676.676,0,1,0,.674.676A.677.677,0,0,0,607.021,118.4Z" fill="#989898"/><path d="M615.579,116.5H603.217a1.631,1.631,0,0,0-1.627,1.626v21.493a1.631,1.631,0,0,0,1.627,1.626h12.362a1.633,1.633,0,0,0,1.627-1.626V118.123A1.632,1.632,0,0,0,615.579,116.5Zm.276,20.941v2.177a.276.276,0,0,1-.229.272H603.217a.276.276,0,0,1-.276-.275v-2.174ZM602.941,120.3v-2.173a.277.277,0,0,1,.276-.276h12.362a.276.276,0,0,1,.275.275V120.3Zm12.914,1.355v14.434H602.941V121.652Z" fill="#989898"/><path d="M607.972,139.34h2.852a.675.675,0,1,0,0-1.351h-2.852a.675.675,0,1,0,0,1.351Z" fill="#989898"/><path d="M611.775,118.4h-2.852a.676.676,0,0,0,0,1.352h2.852a.676.676,0,0,0,0-1.352Z" fill="#989898"/></g><rect width="36" height="28" transform="translate(630 641)" fill="none"/></g></svg>
      </div>
      Digital
    </a>
    <a class="nav-link" @click="tabOnCLick('rcott')" :class="{ active : ottTabIsShow }">
      <div class="medium-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-630 -641)"><g transform="translate(40.41 527.503)"><path d="M607.021,118.4a.676.676,0,1,0,.674.676A.677.677,0,0,0,607.021,118.4Z" fill="#989898"/><path d="M615.579,116.5H603.217a1.631,1.631,0,0,0-1.627,1.626v21.493a1.631,1.631,0,0,0,1.627,1.626h12.362a1.633,1.633,0,0,0,1.627-1.626V118.123A1.632,1.632,0,0,0,615.579,116.5Zm.276,20.941v2.177a.276.276,0,0,1-.229.272H603.217a.276.276,0,0,1-.276-.275v-2.174ZM602.941,120.3v-2.173a.277.277,0,0,1,.276-.276h12.362a.276.276,0,0,1,.275.275V120.3Zm12.914,1.355v14.434H602.941V121.652Z" fill="#989898"/><path d="M607.972,139.34h2.852a.675.675,0,1,0,0-1.351h-2.852a.675.675,0,1,0,0,1.351Z" fill="#989898"/><path d="M611.775,118.4h-2.852a.676.676,0,0,0,0,1.352h2.852a.676.676,0,0,0,0-1.352Z" fill="#989898"/></g><rect width="36" height="28" transform="translate(630 641)" fill="none"/></g></svg>
      </div>
      OTT
    </a>
  </nav>

  <div class="row mt-4">
    <div v-for="item in itemsArray" v-if="item.type == typeValue" class="col-12 col-sm-6 download-center-listing">
      <div class="row no-gutters">
        <div class="col-md-4 col-sm-12 col-4">
          <div class="bg-pink icon-holder">
            <img src="./images/astro-rate.png" alt="" class="">
          </div>
        </div>
        <div class="col-md-8 col-sm-12 col-8">
          <div class="download-center-div">
            <div class="download-center-date">{{item.period}}</div>
            <h5>{{item.title}}</h5>
            <div class="download-holder">
              <button v-if="loginStatus != 1" type="button" class="btn btn-text download-center-button"
                data-toggle="modal" data-target="#downloadCenterModal">
                Download
              </button>
              <button v-if="loginStatus == 1" type="button" class="btn btn-text download-center-button"
                @click="downloadUrl(item.attach_file)">
                <span v-if="item.source_id != 0"><i class="far fa-check-circle"></i> Downloaded</span>
                <span v-if="item.source_id == 0">Download</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--
    Overlay CSS design
    @version 1.2
    @author Toures Tiu <toures.tiu@comma.com.my>
    @created at 21/8/2018
    @updated at 26/9/2018 by SyuQian
  -->
  <!-- Modal Start -->
  <div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" hidden="true"></div>

        <div class="modal-body">
          <div>
            <button type="button" class="close font-brand" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">Close</span>
            </button>

            <div style="clear: both;"></div>
          </div>
          <div class="text-center">
            <h3 class="text-white">Login to Download</h3>
            <p class="text-white mb-4">Please login or signup download this file.</p>
            <a href="./login" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
            <!-- <a href="#" data-dismiss="modal" class="btn btn-white">Skip</a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal End -->
</div>



<script type="text/javascript">
    var itemsObjFromPhp = <?php echo json_encode($this->items); ?>;
    var checkEmailUrl = <?php echo json_encode($checkEmailUrl); ?>;
    var createClientUrl = <?php echo json_encode($createClientUrl); ?>;
	  var userIdFromObject = <?php echo json_encode($userId); ?>;
    var updateUserShareArticlePointUrl = <?php echo json_encode($updateUserShareArticlePointUrl); ?>;
	  var preSetTab = <?php echo json_encode($preSetTab); ?>;

    var app = new Vue({
        el: '#app',
        data: {
          itemsArray: itemsObjFromPhp,
          urlLink : "",
          firstRegisterUser: false,
          userEmail: "",
          userName: "",
          userPhone: "",
          userCompany: "",
          userDesignation: "",
          clientDataCreateStatus: false,
          item : userIdFromObject,
          loginStatus : 0,
          typeValue: "TV",
          tvTabIsShow: true,
          radioTabIsShow: false,
          digitalTabIsShow: false,
          ottTabIsShow: false,
          preSetTab : preSetTab
        },
        mounted: function () {
          if (parseInt(this.item) > 0) {
            this.loginStatus = 1;
          }
          //sort Array by ordering
          this.itemsArray.sort(function (a, b) {
            return a.ordering - b.ordering;
          });
          if (this.preSetTab != "") {
            this.tabOnCLick(this.preSetTab);
          }else{
            this.tabOnCLick("rctv");
          }
        },
        updated: function () {
          jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
            goToLink :function(link){
              window.location.href = link;
            },
            checkEmail : function(){
              _this = this;

              jQuery.ajax({
                url: checkEmailUrl,
                type: 'post',
                data: { email: _this.userEmail },
                success: function (result) {

                  if (result == 1) {
                    window.location.href = _this.urlLink;
                  }else{
                    if (_this.firstRegisterUser) {
                      jQuery.ajax({
                      url: createClientUrl,
                      type: 'post',
                      data: {
                        email: _this.userEmail,
                        name: _this.userName,
                        phone: _this.userPhone,
                        company: _this.userCompany,
                        designation: _this.userDesignation
                      },
                      success: function (result) {
                          window.location.href = _this.urlLink;
                          _this.clientDataCreateStatus = true;
                      },
                      error: function () {
                        console.log('fail');
                      }
                      });
                    }
                    _this.firstRegisterUser = true;
                  }

                },
                error: function () {
                  console.log('fail');
                }
              });
            },
            downloadUrl :function(urlLink){
              this.urlLink = './uploads/rate-card/'+ urlLink;
              window.open(this.urlLink);

              jQuery.ajax({
                url: updateUserShareArticlePointUrl,
                type: 'post',
                data: {'source':urlLink},
                success: function (result) {
                  console.log("success");

                },
                error: function () {
                  console.log('fail');
                }
              });
            },
            tabOnCLick: function(tab){
              var now = new Date();
              var time = now.getTime();
              var expireTime = time + 1000*3600*3;
              now.setTime(expireTime);
		          document.cookie = "_tab="+tab+"; expires=" + now.toUTCString()+';path=/';

              switch (tab) {
                case "rctv":
                  this.typeValue="TV";
                  this.tvTabIsShow= true;
                  this.radioTabIsShow= false;
                  this.digitalTabIsShow= false;
                  this.ottTabIsShow= false;
                  break;
                case "rcradio":
                  this.typeValue="Radio";
                  this.tvTabIsShow= false;
                  this.radioTabIsShow= true;
                  this.digitalTabIsShow= false;
                  this.ottTabIsShow= false;
                  break;
                case "rcdigital":
                  this.typeValue="Digital";
                  this.tvTabIsShow= false;
                  this.radioTabIsShow= false;
                  this.digitalTabIsShow= true;
                  this.ottTabIsShow= false;
                  break;
                case "rcott":
                  this.typeValue="OTT";
                  this.tvTabIsShow= false;
                  this.radioTabIsShow= false;
                  this.digitalTabIsShow= false;
                  this.ottTabIsShow= true;
                  break;
                default:
                  this.typeValue="TV";
                  break;
              }
            }
        }
    })
</script>
