<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_cart
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Quake_club_cart.
 *
 * @since  1.6
 */
class Quake_club_cartViewQperkscarts extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();
		$user       = JFactory::getUser();
		$userId     = $user->get('id');
		$this->filtered_items = array();
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->params = $app->getParams('com_quake_club_cart');

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT c.id as cart_id, c.quantity as cart_quantity, c.remark as cart_remark, p.* FROM #__cus_qperks_cart c join #__cus_qperks_products p on p.id = c.product_id WHERE c.state > 0 and c.user_id ='.$userId);
		$this->filtered_items = $db->loadObjectList();

		$db->setQuery('SELECT (month01+month02+month03+month04+month05+month06+month07+month08+month09+month10+month11+month12) as total FROM #__cus_quake_club_qperks_monthly m WHERE m.state > 0 and m.user_id ='.$userId);
		$this->user_qperks = $db->loadRow();

		$db->setQuery('SELECT business_card FROM #__cus_qperks_company_user WHERE state > 0 and user_id ='.$userId);
		$this->business_card = $db->loadRow();
		
		// $this->filtered_items = $carts;

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}


		$this->_prepareDocument();
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_QUAKE_CLUB_CART_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
