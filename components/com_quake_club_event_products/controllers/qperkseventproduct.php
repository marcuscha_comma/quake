<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_event_products
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

/**
 * Qperkseventproduct controller class.
 *
 * @since  1.6
 */
class Quake_club_event_productsControllerQperkseventproduct extends \Joomla\CMS\MVC\Controller\BaseController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit()
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_quake_club_event_products.edit.qperkseventproduct.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_quake_club_event_products.edit.qperkseventproduct.id', $editId);

		// Get the model.
		$model = $this->getModel('Qperkseventproduct', 'Quake_club_event_productsModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_quake_club_event_products&view=qperkseventproductform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.edit', 'com_quake_club_event_products') || $user->authorise('core.edit.state', 'com_quake_club_event_products'))
		{
			$model = $this->getModel('Qperkseventproduct', 'Quake_club_event_productsModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_quake_club_event_products.edit.qperkseventproduct.id', null);

			// Flush the data from the session.
			$app->setUserState('com_quake_club_event_products.edit.qperkseventproduct.data', null);

			// Redirect to the list screen.
			$this->setMessage(Text::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_ITEM_SAVED_SUCCESSFULLY'));
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(Route::_('index.php?option=com_quake_club_event_products&view=qperkseventproducts', false));
			}
			else
			{
                $this->setRedirect(Route::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.delete', 'com_quake_club_event_products'))
		{
			$model = $this->getModel('Qperkseventproduct', 'Quake_club_event_productsModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_quake_club_event_products.edit.inventory.id', null);
                $app->setUserState('com_quake_club_event_products.edit.inventory.data', null);

                $app->enqueueMessage(Text::_('COM_QUAKE_CLUB_EVENT_PRODUCTS_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(Route::_('index.php?option=com_quake_club_event_products&view=qperkseventproducts', false));
			}

			// Redirect to the list screen.
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(Route::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function addToCart(){
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$app    = JFactory::getApplication();
		$user   = JFactory::getUser();
		$userId = (int) $user->get('id');
		$requestData = $app->input->post->get('jform', array(), 'array');

		$db->setQuery('SELECT id,quantity FROM #__cus_qperks_event_cart where user_id ='.$userId.' and product_id='.$requestData['product_id']);
		$checkCarts             = $db->loadAssoc();
		
		if ($checkCarts) {
			$cart = new stdClass();
			$cart->quantity = $checkCarts['quantity'] + $requestData['product_quantity'];
			$cart->remark = $requestData['remark_description_value'];
			$cart->id = $checkCarts['id'];
			$result = JFactory::getDbo()->updateObject('#__cus_qperks_event_cart', $cart, 'id');

		}else{
			$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_event_cart');
			$max             = $db->loadResult();

			$cart = new stdClass();
			$cart->quantity = $requestData['product_quantity'];
			$cart->user_id = $userId;
			$cart->product_id = $requestData['product_id'];
			$cart->remark = $requestData['remark_description_value'];
			$cart->state=1;
			$cart->created_by=$userId;
			$cart->modified_by=$userId;
			$cart->ordering=$max + 1;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_qperks_event_cart', $cart);
		}
		
		if ($result) {

			$product = new stdClass();
			$product->quantity = (int) $requestData['quantity'] - (int)$requestData['product_quantity'];
			$product->id = $requestData['product_id'];

			// Insert the object into the user profile table.
			$resultUpdate = JFactory::getDbo()->updateObject('#__cus_qperks_event_products', $product, 'id');

			// $app->enqueueMessage(JText::_('COM_USERS_CART_COMPLETE_FAILED'));
			$app->redirect(JUri::base().'event-cart');
		}else{
			// $app->enqueueMessage(JText::_('COM_USERS_CART_COMPLETE_FAILED'),'warning');
		}
	}
}
