<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Ourteampage
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
    $about_quake_article = $this->about_quake[0]->content;
?>

<div id="app">
    <div class="about-content editor-content position-relative">

      <?php echo $about_quake_article; ?>

        <div class="deco-bg">
                <img class="frame-left deco" src="images/2020/frame-left.png">
                <img class="frame-right deco" src="images/2020/frame-right.png">
        </div>
    </div>
    <div class="row our-team-row justify-content-center">
        <div class="col-lg-3 col-md-4 col-6 our-team-listing" v-for="item in itemsArray" @mouseover="mouseOver(item)" @mouseleave="mouseLeave(item)">
            <div class="our-team-wrapper">
                <img :src="'./'+item.image" :alt="item.title">

                <div class="overlayer same-height">
                    <h5>{{item.title}}</h5>
                    <div class="our-team-postion">{{item.position}}</div>
                    <!-- <a :href="'./get-in-touch?contact-me='+item.email"><div class="our-team-contact-me">Contact me</div></a> -->
                    <a :href="'mailto:'+item.email"><div class="our-team-contact-me">Contact me</div></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var itemsObjFromPhp = <?php echo json_encode($this->items); ?>;

    var app = new Vue({
        el: '#app',
        data: {
            itemsArray: itemsObjFromPhp,
            onHover: false,
        },
        mounted: function () {
        },
        updated: function () {
        },
        methods: {
            mouseOver:function(item){
                item.onHover = true;
            },
            mouseLeave:function(item){
                item.onHover = false;
            }
        }
    })
</script>
