<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_qperks
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Quake_club_qperks', JPATH_COMPONENT);
JLoader::register('Quake_club_qperksController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Quake_club_qperks');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
