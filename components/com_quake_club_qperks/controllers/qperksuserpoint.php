<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_qperks
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Qperksuserpoint controller class.
 *
 * @since  1.6
 */
class Quake_club_qperksControllerQperksuserpoint extends JControllerLegacy
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	public function edit()
	{
		$app = JFactory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_quake_club_qperks.edit.qperksuserpoint.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_quake_club_qperks.edit.qperksuserpoint.id', $editId);

		// Get the model.
		$model = $this->getModel('Qperksuserpoint', 'Quake_club_qperksModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_quake_club_qperks&view=qperksuserpointform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Checking if the user can remove object
		$user = JFactory::getUser();

		if ($user->authorise('core.edit', 'com_quake_club_qperks') || $user->authorise('core.edit.state', 'com_quake_club_qperks'))
		{
			$model = $this->getModel('Qperksuserpoint', 'Quake_club_qperksModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(JText::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_quake_club_qperks.edit.qperksuserpoint.id', null);

			// Flush the data from the session.
			$app->setUserState('com_quake_club_qperks.edit.qperksuserpoint.data', null);

			// Redirect to the list screen.
			$this->setMessage(JText::_('COM_QUAKE_CLUB_QPERKS_ITEM_SAVED_SUCCESSFULLY'));
			$menu = JFactory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(JRoute::_('index.php?option=com_quake_club_qperks&view=qperksuserpoints', false));
			}
			else
			{
                $this->setRedirect(JRoute::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function addPlaytimes($user_id){
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$config = JFactory::getConfig();
		$offset = $config->get('offset');
		$date = new JDate('now', $offset);
		$convertedDate = date('Y-m-d h:i:s');


		$db->setQuery('SELECT play_times_count,play_times_earn FROM #__cus_quake_club_game_records where user_id ="'.$user_id .'"');
		$record             = $db->loadRow();

		if (!$record) {

			$db->setQuery('SELECT MAX(ordering) FROM #__cus_quake_club_game_records');
			$max             = $db->loadResult();
			$gameRecord = new stdClass();
			$gameRecord->user_id = $user_id;
			$gameRecord->modified_by = $user_id;
			$gameRecord->created_by = $user_id;
			$gameRecord->state=1;
			$gameRecord->ordering=$max + 1;
			$gameRecord->created_on=$convertedDate;

			JFactory::getDbo()->insertObject('#__cus_quake_club_game_records', $gameRecord);
		}else{

			if ($record[1]>0) {

				$gameRecord = new stdClass();
				$gameRecord->user_id = $user_id;
				$gameRecord->play_times_count = $record[0] +1;
				$gameRecord->play_times_earn = $record[1] -1;
				JFactory::getDbo()->updateObject('#__cus_quake_club_game_records', $gameRecord, 'user_id');

			}

		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Checking if the user can remove object
		$user = JFactory::getUser();

		if ($user->authorise('core.delete', 'com_quake_club_qperks'))
		{
			$model = $this->getModel('Qperksuserpoint', 'Quake_club_qperksModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(JText::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_quake_club_qperks.edit.inventory.id', null);
                $app->setUserState('com_quake_club_qperks.edit.inventory.data', null);

                $app->enqueueMessage(JText::_('COM_QUAKE_CLUB_QPERKS_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(JRoute::_('index.php?option=com_quake_club_qperks&view=qperksuserpoints', false));
			}

			// Redirect to the list screen.
			$menu = JFactory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(JRoute::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function addPoint(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$userId     = $user->get('id');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jdate = new JDate;
		$config = JFactory::getConfig();
		$offset = $config->get('offset');
		$jinput = JFactory::getApplication()->input;
		$source = $jinput->get('source','', 'String');
		$shareOrDownloadStatus = $jinput->get('shareOrDownloadStatus','', 'String');
		
		$convertedDate = date('Y-m-d');
		
		$db->setQuery('SELECT count(id) as total FROM #__cus_qperks_user_point WHERE type in(7,3,11,13,14) and state > 0 and user_id = '.$userId.' and date(created_on) = "'.$convertedDate.'"');
		$times = $db->loadObjectList();

		$db->setQuery('SELECT count(source) as source FROM #__cus_qperks_user_point WHERE type in(7,3,11,13,14) and state > 0 and user_id = '.$userId.' and source= "'.$source.'"');
		$checkSouceDownload = $db->loadObjectList();
		$_shareOrDownloadStatus = "";
		$_shareOrDownloadPoint = 20;

		switch ($shareOrDownloadStatus) {
			case 'share':
					$_shareOrDownloadStatus = 3;
					$_shareOrDownloadPoint = 0;
				break;
				case 'shareWining':
					$_shareOrDownloadStatus = 11;
					$_shareOrDownloadPoint = 0;
				break;
				case 'shareEbook':
					$_shareOrDownloadStatus = 13;
					$_shareOrDownloadPoint = 20;
				break;
				case 'downloadEbook':
					$_shareOrDownloadStatus = 14;
					$_shareOrDownloadPoint = 20;
				break;
			default:
					$_shareOrDownloadStatus = 7;
					$_shareOrDownloadPoint = 0;
				break;
		}

		if ($times[0]->total < 3 && $checkSouceShare[0]->source == 0 && $checkSouceDownload[0]->source == 0) {
			$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_user_point');
			$max             = $db->loadResult();
			$convertedDate = date('Y-m-d h:i:s');

			$profile1 = new stdClass();
			$profile1->user_id = $userId;
			$profile1->point=$_shareOrDownloadPoint;
			$profile1->type=$_shareOrDownloadStatus;
			$profile1->state=1;
			$profile1->source=$source;
			$profile1->created_by=$userId;
			$profile1->created_on=$convertedDate;
			$profile1->modified_by=$userId;
			$profile1->ordering=$max + 1;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_qperks_user_point', $profile1);

			$month = 'month'.date('m');
			$db->setQuery('SELECT id FROM #__cus_quake_club_qperks_monthly where user_id = '. $userId .' AND year ="'.date('Y').'"' );
			$monthly_id             = $db->loadResult();
			$db->setQuery('SELECT '.$month.' FROM #__cus_quake_club_qperks_monthly where user_id = '. $userId .' AND year ="'.date('Y').'"' );
			$monthly_point            = $db->loadResult();

			if ($monthly_id) {
				$monthly = new stdClass();
				$monthly->$month = $monthly_point + $_shareOrDownloadPoint;
				$monthly->id = $monthly_id;
				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');
			}else{
				$monthly = new stdClass();
				$monthly->$month = 0 + $_shareOrDownloadPoint;
				$monthly->year = date('Y');
				$monthly->user_id = $userId;
				$monthly->state = 1;
				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__cus_quake_club_qperks_monthly', $monthly);
			}
			

			// $this->addPlaytimes($userId);
		}else{
			echo "full";
		}


		JFactory::getApplication()->close();
	}

	public function test(){
		$this->_sendTest();
	}

	public function resetDobRedemptionNum()
	{
		$db    = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__users where quakeClubUser = 1');
		$userList             = $db->loadAssocList();

		foreach ($userList as $key => $user) {
			$userObject = new stdClass();
			$userObject->id =$user['id'];
			$userObject->dob_redemption_num = 0;
			$resultUpdateUser = JFactory::getDbo()->updateObject('#__users', $userObject, 'id');
		}
	}

	public function setEventPoint()
	{
		$userList = [
			502,
504,
510,
524,
528,
544,
547,
552,
555,
574,
578,
580,
583,
588,
591,
593,
596,
604,
624,
675,
682,
704,
714,
752,
756,
763,
769,
786,
790,
818,
830,
839,
844,
859,
882,
888,
917,
921,
925,
926,
962,
975,
995,
1016,
1025,
1057,
1186,
1226,
1237,
1351,
1362,
1363,
1394,
1398,
1422,
1427,
1500,
1515,
1531,
1552,
1629,
1633,
1641,
1670,
1701,
1706,
1709,
1838,
2266,
2534,
2812,
2847,
3172,
3270,
3284,
4447,
4786,
4794,
4808,
4999,
5224,
6592,
6593,
6609,
6683,
6708,
7119,
7158,
7209,
7286,
7314,
7398,
7440,
7472,
7487,
7502,
7767,
7786,
7790,
7819,
7837,
7862,
7875,
7906,
7952,
7979,
8013,
8026,
8176,
8248,
8257,
8260,
8263,
8264,
8265,
8267,
8269,
8271,
8273,
8274,
8276,
8277,
8278,
8279,
8286,
8292,
8293,
8294,
8295,
8296,
8297,
8302,
8306,
8316,
8317,
8323,
8328,
8329,
8331,
8332,
8333,
8334,
8335,
8336,
8338,
8339,
8340,
8341,
8344,
8345,
8350,
8352,
8353,
8355,
8357,
8358,
8359,
8360,
367,
368,
371,
529,
566,
573,
586,
605,
653,
654,
710,
754,
757,
775,
796,
862,
985,
1084,
1119,
1224,
1238,
1260,
1262,
1272,
1304,
1306,
1331,
1356,
1411,
1448,
1497,
1533,
1577,
1627,
1664,
1694,
1744,
2276,
2382,
2622,
2640,
2724,
2825,
3244,
3338,
3399,
3684,
4148,
5033,
5946,
6141,
6307,
6411,
6437,
6865,
6904,
6917,
7066,
7198,
7333,
7343,
7381,
7392,
7397,
7484,
7546,
7547,
7554,
7704,
7842,
7852,
7898,
7965,
7991,
8132,
8211,
8223,
8268,
8300,
8365,
8366,
8367,
8368,
8369,
8370,
8371,
8374,
8376,
8377,
8379,
8381,
8382,
8383,
8385,
8386,
8387,
8391,
8393,
8394,
8395,
8402,
7370
		];
		$count = 0;
		foreach ($userList as $key => $user) {
		$db    = JFactory::getDBO();
		$db->setQuery('SELECT id, month01 FROM #__cus_quake_club_qperks_monthly where user_id = '.$user.' order by id desc limit 1');
		$userList             = $db->loadAssocList();
		
			if ($userList) {
				$monthly = new stdClass();
				$monthly->month01 = 80 + (int)$userList[0]['month01'];
				$monthly->id = $userList[0]['id'];
				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');
			}else{
				$monthly = new stdClass();
				$monthly->month01 = 80;
				$monthly->year = date('Y');
				$monthly->user_id = $user;
				$monthly->state = 1;
				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__cus_quake_club_qperks_monthly', $monthly);
			}
		}
		
		
	}

	public function updateRedemptionNum()
	{
		$db    = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__users where quakeClubUser = 1');
		$userList             = $db->loadAssocList();

		foreach ($userList as $key => $user) {
			$userObject = new stdClass();
			$userObject->id =$user['id'];
			$userObject->redemption_num = 1;
			$resultUpdateUser = JFactory::getDbo()->updateObject('#__users', $userObject, 'id');
		}
	}

	public function checkQPerksBalance()
	{
		$db    = JFactory::getDBO();
		$jdate = new JDate;
		die();
		$quarter_date_html = "";
		$quarter_end_date_html = "";
		switch (date('m')) {
			case 1:
			case 2:
			case 3:
				$quarter_date_html ='01 Jan '.date("Y");
				$quarter_end_date_html = '30 Mar '.date("Y", strtotime('+1 year'));
				break;
			case 4:
			case 5:
			case 6:
				$quarter_date_html ='01 Apr '.date("Y");
				$quarter_end_date_html = '30 Jun '.date("Y", strtotime('+1 year'));
				break;
			case 7:
			case 8:
			case 9:
				$quarter_date_html ='01 Jul '.date("Y");
				$quarter_end_date_html = '30 Sept '.date("Y", strtotime('+1 year'));
				break;
			case 10:
			case 11:
			case 12:
				$quarter_date_html ='01 Oct '.date("Y");
				$quarter_end_date_html = '31 Dec '.date("Y", strtotime('+1 year'));
				break;
			default:
				# code...
				break;
		}

		$users = "";

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT id, email, name, MONTH(registerDate) as month, YEAR(registerDate) as year  FROM #__users WHERE quakeClubUser = 1 and block = 0 and id=1352');
		$users = $db->loadObjectList();
// 			echo "<pre>";
// 			print_r($users);
// 			echo "</pre>";
// die();
		foreach ($users as $key => $user) {

			$monthString = date('F', mktime(0, 0, 0, $user->month, 10));
			$joinDate = $monthString. " " . $user->year;
			
			$db->setQuery('SELECT sum(month01+month02+month03+month04+month05+month06+month07+month08+month09+month10+month11+month12) as total FROM #__cus_quake_club_qperks_monthly WHERE state > 0 and user_id ='.$user->id);
			$user_qperks = $db->loadResult();

			$db->setQuery('SELECT expired_point, year, quarter FROM #__cus_qperks_user_point_expired WHERE user_id ='.$user->id .' order by id desc limit 6');
			$user_expired_qperks = $db->loadObjectList();

			// echo "<pre>";
			// print_r($user_expired_qperks);
			// echo "</pre>";
			// echo "user_qperks=" . $user_qperks ."<br>";
			// echo "quarter_date_html=" . $quarter_date_html ."<br>";
			// echo "quarter_end_date_html=" . $quarter_end_date_html ."<br>";
			// echo "monthString=" . $monthString ."<br>";

			$sent = $this->_sendEmailBalance($user->email, $user->name, $user_qperks, $quarter_date_html, $quarter_end_date_html, $joinDate, $user_expired_qperks);
		}
	}


	public function updateUserPointByHistory(){
			$db    = JFactory::getDBO();
			$jdate = new JDate;

			// $db->setQuery('SELECT u.id as id FROM #__users u where u.quakeClubUser = 1 and u.block = 0');
			// $userList             = $db->loadAssocList();
			$userList = array(
				502
			);
			
			foreach ($userList as $key => $value) {
				// echo "<pre>";
				// print_r(	$value );
				// echo "</pre>";
				// die();
				// $db->setQuery('SELECT sum(p.point) as point FROM #__cus_qperks_user_point p where p.user_id = '.$value["id"].' and p.type not in(10) and Month(p.created_on) in(7) and year(p.created_on) = 2019');
				// $userPoint            = $db->loadResult();

				$db->setQuery('SELECT sum(p.point) as point FROM #__cus_qperks_user_point p where p.user_id = '.$value);
				$userPoint            = $db->loadResult();

				// $db->setQuery('SELECT sum(case when promo = 1 then r.promo_qperks else r.point end) as point FROM #__cus_quake_club_redemption r where r.user_id = '.$value["id"].' and r.status = 2 and r.state =1 AND r.event_type is null and Month(r.created_on) in(7) and year(r.created_on) = 2019');
				// $userSpent            = $db->loadResult();

				$db->setQuery('SELECT sum(case when promo = 1 then r.promo_qperks else r.point end) as point FROM #__cus_quake_club_redemption r where r.user_id = '.$value.' AND r.event_type is null');
				$userSpent            = $db->loadResult();

				$expired_point = ($userPoint?$userPoint:0)-($userSpent?$userSpent:0);
				$pointExpired = new stdClass();
				$pointExpired->user_id = $value;
				$pointExpired->quater = 7;
				$pointExpired->year = 2020;
				$pointExpired->state=1;
				$pointExpired->earned_point=$userPoint?$userPoint:0;
				$pointExpired->spent_point=$userSpent?$userSpent:0;
				$pointExpired->expired_point=($expired_point);
	
				JFactory::getDbo()->insertObject('#__cus_qperks_user_point_expired', $pointExpired);
				
			}
			echo "yes";
	}

	public function testtest(){
			$db    = JFactory::getDBO();
			$jdate = new JDate;
			$db->setQuery('SELECT u.id as id FROM #__users u where u.quakeClubUser = 1 and u.block = 0');
			$userList             = $db->loadAssocList();

			$zzz =array(
				array(3,2,2019,2019),
				array(4,3,2019,2019),
				array(1,4,2020,2019),
				array(2,1,2020,2020),
				array(3,2,2020,2020),
				array(4,3,2020,2020),
				array(1,4,2021,2020),
				array(2,1,2021,2021),
				array(3,2,2021,2021),
				array(4,3,2021,2021),
				array(1,4,2022,2021),
				array(2,1,2022,2022)
			);

				foreach ($zzz as $key => $value) {
					$quater = $value[0];
					$prequater = $value[1];
					$year = $value[2];
					$preyear = $value[3];

					foreach ($userList as $key => $value) {

						// earned
						$db->setQuery('SELECT sum(p.point) as point FROM #__cus_qperks_user_point p where p.user_id = '.$value["id"].' and p.type not in(10) and QUARTER(p.created_on) = '.$quater.' and year(p.created_on) = '.$year);
						$userPoint            = $db->loadResult();
		
						// spend
						$db->setQuery('SELECT sum(case when promo = 1 then r.promo_qperks else r.point end * quantity) as point FROM #__cus_quake_club_redemption r where r.user_id = '.$value["id"].' and r.status in(1,2) AND r.event_type is null and QUARTER(r.created_on) = '.$quater.' and year(r.created_on) = '.$year);
						$userSpent            = $db->loadResult();
		
						// pre earned
						$db->setQuery('SELECT ex.earned_point FROM #__cus_qperks_user_point_expired ex where ex.user_id = '.$value["id"].' and ex.quarter = '.$quater.' and ex.year = '.($year-1));
						$prePoint            = $db->loadResult();
		
						// pre expired_point
						$db->setQuery('SELECT ex.expired_point FROM #__cus_qperks_user_point_expired ex where ex.user_id = '.$value["id"].' and ex.quarter = '.($prequater).' and ex.year = '.$preyear);
						$preExpired            = $db->loadResult();
		
						
		
						$expired_point = ($prePoint?$prePoint:0)+($preExpired?($preExpired>0?0:$preExpired):0)+($userSpent?$userSpent*-1:0);
						$pointExpired = new stdClass();
						$pointExpired->user_id = $value["id"];
						$pointExpired->quarter = $quater;
						$pointExpired->year = $year;
						$pointExpired->state=1;
						$pointExpired->earned_point=$userPoint?$userPoint:0;
						$pointExpired->spent_point=$userSpent?($userSpent*-1):0;
						$pointExpired->expired_point=($expired_point);
			
						JFactory::getDbo()->insertObject('#__cus_qperks_user_point_expired', $pointExpired);
						
					}
					echo $quater;
				}
			echo "yes";
	}

	public function testtest1(){
		$db    = JFactory::getDBO();
		$jdate = new JDate;
		$db->setQuery('SELECT ex.expired_point, ex.user_id FROM #__cus_qperks_user_point_expired ex where ex.quarter = 4 and ex.year = 2020');
		$expired_point             = $db->loadAssocList();

		foreach ($expired_point as $key => $value) {
			
			if ($value['expired_point']>0) {
				$db->setQuery('SELECT * FROM #__cus_quake_club_qperks_monthly m where m.user_id ='.$value["user_id"].' and m.year = 2020');
				$monthly             = $db->loadAssoc();
				// echo "<pre>";
				// print_r($monthly);
				// echo "</pre>";

				$monthlyObject = new stdClass();
				$monthlyObject->id = $monthly['id'];
				$monthlyObject->month12 = $monthly['month12']-$value['expired_point'];
				JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthlyObject, 'id');
			}
			
		}

		
}

	public function checkBirthday()
	{
		$db    = JFactory::getDBO();
		$jdate = new JDate;

		$db->setQuery('SELECT * FROM #__users where quakeClubUser = 1 and dob_redemption_num = 0 and Month(dob) ='.date('m'));
		$userList             = $db->loadAssocList();
		$db->setQuery('SELECT * FROM #__users where quakeClubUser = 1 and dob_redemption_num != -1 and Month(dob) !='.date('m'));
		$userListNotDob             = $db->loadAssocList();

		foreach ($userList as $key => $user) {
			$userObject = new stdClass();
			$userObject->id =$user['id'];
			$userObject->dob_redemption_num = 1;
			$resultUpdateUser = JFactory::getDbo()->updateObject('#__users', $userObject, 'id');
			$sent = $this->_sendEmail($user['name'],$user['email']);
		}

		foreach ($userListNotDob as $key => $user) {

			$userObject = new stdClass();
			$userObject->id =$user['id'];
			$userObject->dob_redemption_num = 0;
			$resultUpdateUser = JFactory::getDbo()->updateObject('#__users', $userObject, 'id');

		}
	}

	private function _sendEmail($username, $email)
	{
		$config = JFactory::getConfig();
		$user   = JFactory::getUser();
		$data = $user->getProperties();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$emailSubject = "Happy Birthday! Here is a small gift from us.";

		$emailBody = '<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

		<head>
		  <!-- NAME: 1 COLUMN -->
		  <!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
		  <meta charset="UTF-8">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <title>Astro Quake Club - It&#39;s Your Birthday Month!</title>

		  <style type="text/css">
			p {
			  margin: 5px 0;
			  padding: 0;
			}

			table {
			  border-collapse: collapse;
			}

			h1,
			h2,
			h3,
			h4,
			h5,
			h6 {
			  display: block;
			  margin: 0;
			  padding: 0;
			}

			img,
			a img {
			  border: 0;
			  height: auto;
			  outline: none;
			  text-decoration: none;
			}

			body,
			#bodyTable,
			#bodyCell {
			  height: 100%;
			  margin: 0;
			  padding: 0;
			  width: 100%;
			}

			.mcnPreviewText {
			  display: none !important;
			}

			#outlook a {
			  padding: 0;
			}

			img {
			  -ms-interpolation-mode: bicubic;
			}

			table {
			  mso-table-lspace: 0pt;
			  mso-table-rspace: 0pt;
			}

			.ReadMsgBody {
			  width: 100%;
			}

			.ExternalClass {
			  width: 100%;
			}

			p,
			a,
			li,
			td,
			blockquote {
			  mso-line-height-rule: exactly;
			}

			a[href^=tel],
			a[href^=sms] {
			  color: inherit;
			  cursor: default;
			  text-decoration: none;
			}

			p,
			a,
			li,
			td,
			body,
			table,
			blockquote {
			  -ms-text-size-adjust: 100%;
			  -webkit-text-size-adjust: 100%;
			}

			.ExternalClass,
			.ExternalClass p,
			.ExternalClass td,
			.ExternalClass div,
			.ExternalClass span,
			.ExternalClass font {
			  line-height: 100%;
			}

			a[x-apple-data-detectors] {
			  color: inherit !important;
			  text-decoration: none !important;
			  font-size: inherit !important;
			  font-family: inherit !important;
			  font-weight: inherit !important;
			  line-height: inherit !important;
			}

			#bodyCell {
			  padding: 20px;
			}

			.templateContainer {
			  max-width: 600px !important;
			}

			a.mcnButton {
			  display: block;
			}

			.mcnImage,
			.mcnRetinaImage {
			  vertical-align: bottom;
			}

			.mcnTextContent {
			  word-break: break-word;
			}

			.mcnTextContent img {
			  height: auto !important;
			}

			.mcnDividerBlock {
			  table-layout: fixed !important;
			}

			body,
			#bodyTable {

			  background-color: #fafafa;
			}

			#bodyCell {

			  border-top: 0;
			}
			.templateContainer {

			  border: 0;
			}

			h1 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 26px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h2 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 22px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h3 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 20px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h4 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 18px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}
			#templatePreheader {

			  background-color: #fafafa;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0px;
			}

			#templatePreheader .mcnTextContent,
			#templatePreheader .mcnTextContent p {

			  color: #000000;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 10px;

			  line-height: 150%;

			  text-align: center;
			}


			#templatePreheader .mcnTextContent a,
			#templatePreheader .mcnTextContent p a {

			  color: #000000;

			  font-weight: normal;

			  text-decoration: underline;
			}

			#templateHeader {

			  background-color: #ffffff;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0;
			}

			#templateHeader .mcnTextContent,
			#templateHeader .mcnTextContent p {

			  color: #202020;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 16px;

			  line-height: 150%;

			  text-align: left;
			}

			#templateHeader .mcnTextContent a,
			#templateHeader .mcnTextContent p a {

			  color: #007C89;

			  font-weight: normal;

			  text-decoration: underline;
			}

			#templateBody {

			  background-color: #ffffff;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 2px solid #EAEAEA;

			  padding-top: 20px;

			  padding-bottom: 20px;
			}
			#templateBody .mcnTextContent,
			#templateBody .mcnTextContent p {

			  color: #000000;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 16px;

			  line-height: 150%;

			  text-align: center;
			}


			#templateBody .mcnTextContent a,
			#templateBody .mcnTextContent p a {

			  color: #000000;

			  font-weight: normal;

			  text-decoration: none;
			}

			#templateFooter {

			  background-color: #000000;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0px;
			}

			#templateFooter .mcnTextContent,
			#templateFooter .mcnTextContent p {

			  color: #ffffff;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 9px;

			  line-height: 150%;

			  text-align: center;
			}
			#templateFooter .mcnTextContent a,
			#templateFooter .mcnTextContent p a {

			  color: #ffffff;

			  font-weight: normal;

			  text-decoration: none;
			}

			@media only screen and (min-width:768px) {
			  .templateContainer {
				width: 600px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  body,
			  table,
			  td,
			  p,
			  a,
			  li,
			  blockquote {
				-webkit-text-size-adjust: none !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  body {
				width: 100% !important;
				min-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #bodyCell {
				padding-top: 10px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnRetinaImage {
				max-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImage {
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnCartContainer,
			  .mcnCaptionTopContent,
			  .mcnRecContentContainer,
			  .mcnCaptionBottomContent,
			  .mcnTextContentContainer,
			  .mcnBoxedTextContentContainer,
			  .mcnImageGroupContentContainer,
			  .mcnCaptionLeftTextContentContainer,
			  .mcnCaptionRightTextContentContainer,
			  .mcnCaptionLeftImageContentContainer,
			  .mcnCaptionRightImageContentContainer,
			  .mcnImageCardLeftTextContentContainer,
			  .mcnImageCardRightTextContentContainer,
			  .mcnImageCardLeftImageContentContainer,
			  .mcnImageCardRightImageContentContainer {
				max-width: 100% !important;
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnBoxedTextContentContainer {
				min-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupContent {
				padding: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnCaptionLeftContentOuter .mcnTextContent,
			  .mcnCaptionRightContentOuter .mcnTextContent {
				padding-top: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardTopImageContent,
			  .mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,
			  .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
				padding-top: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardBottomImageContent {
				padding-bottom: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockInner {
				padding-top: 0 !important;
				padding-bottom: 0 !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockOuter {
				padding-top: 9px !important;
				padding-bottom: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnTextContent,
			  .mcnBoxedTextContentColumn {
				padding-right: 18px !important;
				padding-left: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardLeftImageContent,
			  .mcnImageCardRightImageContent {
				padding-right: 18px !important;
				padding-bottom: 0 !important;
				padding-left: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcpreview-image-uploader {
				display: none !important;
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  h1 {

				font-size: 22px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  h2 {

				font-size: 20px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  h3 {

				font-size: 18px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  h4 {

				font-size: 16px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnBoxedTextContentContainer .mcnTextContent,
			  .mcnBoxedTextContentContainer .mcnTextContent p {

				font-size: 14px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #templatePreheader {

				display: block !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #templatePreheader .mcnTextContent,
			  #templatePreheader .mcnTextContent p {

				font-size: 10px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templateHeader .mcnTextContent,
			  #templateHeader .mcnTextContent p {

				font-size: 16px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templateBody .mcnTextContent,
			  #templateBody .mcnTextContent p {

				font-size: 14px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #templateFooter .mcnTextContent,
			  #templateFooter .mcnTextContent p {

				font-size: 9px !important;

				line-height: 150% !important;
			  }

			}
		  </style>
		</head>

		<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fafafa;">
		  <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Happy Birthday! To celebrate, we’d like to reward you with Q-Perks</span>
		  <!--<![endif]-->
		  <center>
			<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #fafafa;">
			  <tr>
				<td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 20px;width: 100%;border-top: 0;">
				  <!-- BEGIN TEMPLATE // -->
				  <!--[if (gte mso 9)|(IE)]>
								<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
								<tr>
								<td align="center" valign="top" width="600" style="width:600px;">
								<![endif]-->
				  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
					<tr>
					  <td valign="top" id="templateHeader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnImageBlockOuter">
							<tr>
							  <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
								<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


										<img align="center" alt="" src="http://quake.com.my/images/edm/edm-header-ams.png" width="600" style="max-width: 600px;padding-bottom: 0px;vertical-align: bottom;display: inline !important;border-radius: 0%;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


									  </td>
									</tr>
								  </tbody>
								</table>
							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnImageBlockOuter">
							<tr>
							  <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
								<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


										<img align="center" alt="It&#39;s your birthday month!" src="http://quake.com.my/images/edm/main-birthday.jpg" width="600" style="max-width: 600px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


									  </td>
									</tr>
								  </tbody>
								</table>
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
					<tr>
					  <td valign="top" id="templateBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 2px solid #EAEAEA;padding-top: 20px;padding-bottom: 20px;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">

										<p style="margin: 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Dear <strong>'.$username.'</strong>,</p>

										<p style="margin: 20px 0 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Happy Birthday!</p>

										<p style="margin: 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">To celebrate, we’d like to reward you with</p>

										<p style="margin: 5px 0 20px 0;font-weight: 600;font-size: 26px;color: #EC008C;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;line-height: 150%;text-align: center;">*200 Q-Perks</p>

										<p style="margin: 5px 0 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;"><a target="_blank" href="http://quake.com.my/login" style="font-weight: bold;color: #EC008C;text-decoration: underline;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">Sign in</a> to start redeeming!</p>

										<p style="margin: 5px 0 50px 0;font-size: 12px;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;line-height: 150%;text-align: center;">*Terms & conditions apply</p>


									  </td>
									</tr>
									<tr>
									<td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">
										<p style="font-size: 15px;margin: 50px 0 0 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;line-height: 150%;text-align: center;">You received this message because you&apos;re our registered member or accepted our invitation to receive emails from Quake Club.</p>

									</td>
								</tr>
								<tr>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                  <tbody class="mcnFollowBlockOuter">
                    <tr>
                      <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowBlockInner">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tbody>
                            <tr>
                              <td align="center" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
                                  <tbody>
                                    <tr>
                                      <td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                          <tbody>
                                            <tr>
                                              <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->

                                                <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                  <tbody>
                                                    <tr>
                                                      <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                  <tbody>
                                                                    <tr>

                                                                      <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <a href="https://www.facebook.com/QuakeMY" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-fb.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
                                                                      </td>


                                                                    </tr>
                                                                  </tbody>
                                                                </table>
                                                              </td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>

                                                <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                                <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                  <tbody>
                                                    <tr>
                                                      <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                  <tbody>
                                                                    <tr>

                                                                      <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <a href="http://quake.com.my/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-web.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
                                                                      </td>


                                                                    </tr>
                                                                  </tbody>
                                                                </table>
                                                              </td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>

                                                <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                                <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>

                      </td>
                    </tr>
                  </tbody>
                </table>
</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
				  </table>
				  <!--[if (gte mso 9)|(IE)]>
								</td>
								</tr>
								</table>
								<![endif]-->
				  <!-- // END TEMPLATE -->
				</td>
			  </tr>
			</table>
		  </center>
		</body>

		</html>
		';

		$to = $email;
		$from = array($data['mailfrom'], $data['fromname']);

		// # Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject($emailSubject);
		$mailer->setBody($emailBody);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();
		$mailer->Encoding = 'base64';
		// $mailer->AddEmbeddedImage( './images/Cockoo.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
		return $mailer->send();
	}

	private function _sendEmailBalance($email, $name, $point, $startDate, $endDate, $joinDate, $expired_qperks)
	{
		// echo "<pre>";
		// print_r($expired_qperks);
		// echo "</pre>";
		// die();
		$config = JFactory::getConfig();
		$user   = JFactory::getUser();
		$data = $user->getProperties();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$emailSubject = "[Quake Club] Your Q-Perks balance.";

		$emailBody = '<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <!-- NAME: 1 COLUMN -->
  <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Astro Quake Club - Q-Perks Balance</title>

  <style type="text/css">
    p {
      margin: 5px 0;
      padding: 0;
    }

    table {
      border-collapse: collapse;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      display: block;
      margin: 0;
      padding: 0;
    }

    img,
    a img {
      border: 0;
      height: auto;
      outline: none;
      text-decoration: none;
    }

    body,
    #bodyTable,
    #bodyCell {
      height: 100%;
      margin: 0;
      padding: 0;
			width: 600px !important;
			
    }

    .mcnPreviewText {
      display: none !important;
    }

    #outlook a {
      padding: 0;
    }

    img {
      -ms-interpolation-mode: bicubic;
    }

    table {
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    p,
    a,
    li,
    td,
    blockquote {
      mso-line-height-rule: exactly;
    }

    a[href^=tel],
    a[href^=sms] {
      color: inherit;
      cursor: default;
      text-decoration: none;
    }

    p,
    a,
    li,
    td,
    body,
    table,
    blockquote {
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
    }

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass td,
    .ExternalClass div,
    .ExternalClass span,
    .ExternalClass font {
      line-height: 100%;
    }

    a[x-apple-data-detectors] {
      color: inherit !important;
      text-decoration: none !important;
      font-size: inherit !important;
      font-family: inherit !important;
      font-weight: inherit !important;
      line-height: inherit !important;
    }

    #bodyCell {
      padding: 20px;
    }

    .templateContainer {
      max-width: 600px !important;
    }

    a.mcnButton {
      display: block;
    }

    .mcnImage,
    .mcnRetinaImage {
      vertical-align: bottom;
    }

    .mcnTextContent {
      word-break: break-word;
    }

    .mcnTextContent img {
      height: auto !important;
    }

    .mcnDividerBlock {
      table-layout: fixed !important;
    }

    body,
    #bodyTable {
      background-color: #fafafa;
    }

    #bodyCell {
      border-top: 0;
    }

    .templateContainer {
      border: 0;
    }

    h1 {
      color: #202020;
      font-family: Helvetica;

      font-size: 26px;

      font-style: normal;

      font-weight: bold;

      line-height: 125%;

      letter-spacing: normal;

      text-align: left;
    }

    h2 {

      color: #202020;

      font-family: Helvetica;

      font-size: 22px;

      font-style: normal;

      font-weight: bold;

      line-height: 125%;

      letter-spacing: normal;

      text-align: left;
    }

    h3 {

      color: #202020;

      font-family: Helvetica;

      font-size: 20px;

      font-style: normal;

      font-weight: bold;

      line-height: 125%;

      letter-spacing: normal;

      text-align: left;
    }

    h4 {

      color: #202020;

      font-family: Helvetica;

      font-size: 18px;

      font-style: normal;

      font-weight: bold;

      line-height: 125%;

      letter-spacing: normal;

      text-align: left;
    }

    #templatePreheader {

      background-color: #fafafa;

      background-image: none;

      background-repeat: no-repeat;

      background-position: center;

      background-size: cover;

      border-top: 0;

      border-bottom: 0;

      padding-top: 0px;

      padding-bottom: 0px;
    }


    #templatePreheader .mcnTextContent,
    #templatePreheader .mcnTextContent p {

      color: #000000;

      font-family: Helvetica, Arial, sans-serif;

      font-size: 10px;

      line-height: 150%;

      text-align: center;
    }


    #templatePreheader .mcnTextContent a,
    #templatePreheader .mcnTextContent p a {

      color: #000000;

      font-weight: normal;

      text-decoration: underline;
    }

    #templateHeader {

      background-color: #ffffff;

      background-image: none;

      background-repeat: no-repeat;

      background-position: center;

      background-size: cover;

      border-top: 0;

      border-bottom: 0;

      padding-top: 0px;

      padding-bottom: 0;
    }


    #templateHeader .mcnTextContent,
    #templateHeader .mcnTextContent p {

      color: #202020;

      font-family: Helvetica, Arial, sans-serif;

      font-size: 16px;

      line-height: 150%;

      text-align: left;
    }

    #templateHeader .mcnTextContent a,
    #templateHeader .mcnTextContent p a {

      color: #007C89;

      font-weight: normal;

      text-decoration: underline;
    }

    #templateBody {

      background-color: #ffffff;

      background-image: none;

      background-repeat: no-repeat;

      background-position: center;

      background-size: cover;

      border-top: 0;

      border-bottom: 2px solid #EAEAEA;

      padding-top: 20px;

      padding-bottom: 20px;
    }


    #templateBody .mcnTextContent,
    #templateBody .mcnTextContent p {

      color: #000000;

      font-family: Helvetica, Arial, sans-serif;

      font-size: 16px;

      line-height: 150%;

      text-align: center;
    }

    #templateBody .mcnTextContent a,
    #templateBody .mcnTextContent p a {

      color: #000000;

      font-weight: normal;

      text-decoration: none;
    }

    #templateFooter {

      background-color: #000000;

      background-image: none;

      background-repeat: no-repeat;

      background-position: center;

      background-size: cover;

      border-top: 0;

      border-bottom: 0;

      padding-top: 0px;

      padding-bottom: 0px;
    }


    #templateFooter .mcnTextContent,
    #templateFooter .mcnTextContent p {

      color: #ffffff;

      font-family: Helvetica, Arial, sans-serif;

      font-size: 9px;

      line-height: 150%;

      text-align: center;
    }

    #templateFooter .mcnTextContent a,
    #templateFooter .mcnTextContent p a {

      color: #ffffff;

      font-weight: normal;

      text-decoration: none;
    }

    @media only screen and (min-width:768px) {
      .templateContainer {
        width: 600px !important;
      }

    }

    @media only screen and (max-width: 480px) {
      body,
      table,
      td,
      p,
      a,
      li,
      blockquote {
        -webkit-text-size-adjust: none !important;
      }

    }

    @media only screen and (max-width: 480px) {
      body {
        width: 100% !important;
        min-width: 100% !important;
      }

    }

    @media only screen and (max-width: 480px) {
      #bodyCell {
        padding-top: 10px !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnRetinaImage {
        max-width: 100% !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnImage {
        width: 100% !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnCartContainer,
      .mcnCaptionTopContent,
      .mcnRecContentContainer,
      .mcnCaptionBottomContent,
      .mcnTextContentContainer,
      .mcnBoxedTextContentContainer,
      .mcnImageGroupContentContainer,
      .mcnCaptionLeftTextContentContainer,
      .mcnCaptionRightTextContentContainer,
      .mcnCaptionLeftImageContentContainer,
      .mcnCaptionRightImageContentContainer,
      .mcnImageCardLeftTextContentContainer,
      .mcnImageCardRightTextContentContainer,
      .mcnImageCardLeftImageContentContainer,
      .mcnImageCardRightImageContentContainer {
        max-width: 100% !important;
        width: 100% !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnBoxedTextContentContainer {
        min-width: 100% !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnImageGroupContent {
        padding: 9px !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnCaptionLeftContentOuter .mcnTextContent,
      .mcnCaptionRightContentOuter .mcnTextContent {
        padding-top: 9px !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnImageCardTopImageContent,
      .mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,
      .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
        padding-top: 18px !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnImageCardBottomImageContent {
        padding-bottom: 9px !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnImageGroupBlockInner {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnImageGroupBlockOuter {
        padding-top: 9px !important;
        padding-bottom: 9px !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnTextContent,
      .mcnBoxedTextContentColumn {
        padding-right: 18px !important;
        padding-left: 18px !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcnImageCardLeftImageContent,
      .mcnImageCardRightImageContent {
        padding-right: 18px !important;
        padding-bottom: 0 !important;
        padding-left: 18px !important;
      }

    }

    @media only screen and (max-width: 480px) {
      .mcpreview-image-uploader {
        display: none !important;
        width: 100% !important;
      }

    }

    @media only screen and (max-width: 480px) {

      h1 {

        font-size: 22px !important;

        line-height: 125% !important;
      }

    }

    @media only screen and (max-width: 480px) {

      h2 {

        font-size: 20px !important;

        line-height: 125% !important;
      }

    }

    @media only screen and (max-width: 480px) {

      h3 {

        font-size: 18px !important;

        line-height: 125% !important;
      }

    }

    @media only screen and (max-width: 480px) {

      h4 {

        font-size: 16px !important;

        line-height: 150% !important;
      }

    }

    @media only screen and (max-width: 480px) {

      .mcnBoxedTextContentContainer .mcnTextContent,
      .mcnBoxedTextContentContainer .mcnTextContent p {

        font-size: 14px !important;

        line-height: 150% !important;
      }

    }

    @media only screen and (max-width: 480px) {

      #templatePreheader {

        display: block !important;
      }

    }

    @media only screen and (max-width: 480px) {

      #templatePreheader .mcnTextContent,
      #templatePreheader .mcnTextContent p {

        font-size: 10px !important;

        line-height: 150% !important;
      }

    }

    @media only screen and (max-width: 480px) {

      #templateHeader .mcnTextContent,
      #templateHeader .mcnTextContent p {

        font-size: 16px !important;

        line-height: 150% !important;
      }

    }

    @media only screen and (max-width: 480px) {
      #templateBody .mcnTextContent,
      #templateBody .mcnTextContent p {

        font-size: 14px !important;

        line-height: 150% !important;
      }

    }

    @media only screen and (max-width: 480px) {
      #templateFooter .mcnTextContent,
      #templateFooter .mcnTextContent p {

        font-size: 9px !important;

        line-height: 150% !important;
      }

    }
  </style>
</head>

<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fafafa;">
  <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Thank you for being a Quake Club member</span>
  <!--<![endif]-->
  <center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #fafafa;">
      <tr>
        <td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 20px;width: 100%;border-top: 0;">
          <!-- BEGIN TEMPLATE // -->
          <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                        <tr>
                        <td align="center" valign="top" width="600" style="width:600px;">
                        <![endif]-->
          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
            <tr>
              <td valign="top" id="templateHeader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                  <tbody class="mcnImageBlockOuter">
                    <tr>
                      <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tbody>
                            <tr>
                              <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


                                <img align="center" alt="Astro Quake Club" src="http://quake.com.my/images/edm/edm-header-ams.png" width="600" style="max-width: 600px;padding-bottom: 0px;vertical-align: bottom;display: inline !important;border-radius: 0%;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                  <tbody class="mcnImageBlockOuter">
                    <tr>
                      <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tbody>
                            <tr>
                              <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


                                <img align="center" alt="Q-Perks Balance!" src="http://quake.com.my/images/edm/main-qperks-balance_v2.jpg" width="600" style="max-width: 600px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td valign="top" id="templateBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 2px solid #EAEAEA;padding-top: 20px;padding-bottom: 20px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                  <tbody class="mcnTextBlockOuter">
                    <tr>
                      <td valign="top" class="mcnTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

                        <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                          <tbody>
                            <tr>

                              <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">

                                <p style="margin: 5px 0 20px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Dear <strong>'.$name.'</strong>,</p>

                                <p style="margin: 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Thank you for being a Quake Club member since<br> '.$joinDate.'.</p>

                              </td>
                            </tr>
                            <tr>
                              <td valign="center" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 0;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">

															<table id="order-table" align="top" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border: 1px solid #000000;text-align: left;font-size: 14px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; font-family: Helvetica, Arial, sans-serif;" width="100%">
															<tbody>
															<tr>
															<td rowspan="2" valign="center" style="padding: 0px;border: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-size: 12px;font-family: Helvetica, Arial, sans-serif;width: 12%;">
																<b>Current Balance</b>
																</td>
																<td colspan="5" valign="top" style="padding: 10px;border: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
																<b>Unused Q-Perks will expire</b>
																</td>
															</tr>
															<tr>
																<td valign="top" style="padding: 10px;border: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;font-size: 12px;-webkit-text-size-adjust: 70%;font-family: Helvetica, Arial, sans-serif;">
																30 Jun 2021
																</td>
																<td valign="top" style="padding: 8px;border: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;font-size: 12px;-webkit-text-size-adjust: 80%;font-family: Helvetica, Arial, sans-serif;">
																30 Sep 2021
																</td>
																<td valign="top" style="padding: 8px;border: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;font-size: 12px;-webkit-text-size-adjust: 90%;font-family: Helvetica, Arial, sans-serif;">
																31 Dec 2021
																</td>
																<td valign="top" style="padding: 8px;border: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;font-size: 12px;-webkit-text-size-adjust: 50%;font-family: Helvetica, Arial, sans-serif;">
																31 Mar 2021
																</td>
																<td valign="top" style="padding: 8px;border: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;font-size: 12px;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
																30 Jun 2022
																</td>
															</tr>
															<tr>
															<td valign="top" style="padding: 10px;border-right: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
																'.$point.'
															</td>
															<td valign="top" style="padding: 10px;border-right: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
															'.(($expired_qperks[4]->expired_point>0?$expired_qperks[4]->expired_point:0)).'
															</td>
															<td valign="top" style="padding: 10px;border-right: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
															'.($expired_qperks[3]->expired_point>0?$expired_qperks[3]->expired_point:0).'															
															</td>
															<td valign="top" style="padding: 10px;border-right: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
															'.($expired_qperks[2]->expired_point>0?$expired_qperks[2]->expired_point:0).'															
															</td>
															<td valign="top" style="padding: 10px;border-right: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
															'.($expired_qperks[1]->expired_point>0?$expired_qperks[1]->expired_point:0).'															
															</td>
															<td valign="top" style="padding: 10px;border-right: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
															'.($expired_qperks[0]->expired_point>0?$expired_qperks[0]->expired_point:0).'															
															</td>
															</tr>
															</tbody>
														</table>

                              </td>
                            </tr>

                            <tr>
                              <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 14px;line-height: 150%;text-align: center;">
                                

                              </td>
														</tr>
														
														<tr>
                              <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">
                                <p style="font-size: 15px;margin: 50px 0 0 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;line-height: 150%;text-align: center;">You received this message because you&apos;re our registered member or accepted our invitation to receive emails from Quake Club.</p>

                              </td>
                            </tr>
<tr>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                  <tbody class="mcnFollowBlockOuter">
                    <tr>
                      <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowBlockInner">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <tbody>
                            <tr>
                              <td align="center" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
                                  <tbody>
                                    <tr>
                                      <td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                          <tbody>
                                            <tr>
                                              <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->

                                                <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                  <tbody>
                                                    <tr>
                                                      <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                  <tbody>
                                                                    <tr>

                                                                      <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <a href="https://www.facebook.com/QuakeMY" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-fb.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
                                                                      </td>


                                                                    </tr>
                                                                  </tbody>
                                                                </table>
                                                              </td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>

                                                <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                                <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                  <tbody>
                                                    <tr>
                                                      <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                          <tbody>
                                                            <tr>
                                                              <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                  <tbody>
                                                                    <tr>

                                                                      <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <a href="http://quake.com.my/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-web.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
                                                                      </td>


                                                                    </tr>
                                                                  </tbody>
                                                                </table>
                                                              </td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>

                                                <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                                <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>

                      </td>
                    </tr>
                  </tbody>
                </table>
</tr>
                          </tbody>
                        </table>
                        <!--[if mso]>
				</td>
				<![endif]-->

                        <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
						</tr>
						
            
          </table>
          <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
          <!-- // END TEMPLATE -->
        </td>
      </tr>
    </table>
  </center>
</body>

</html>
';

		// $to = $email;

		$to = array("midoff1@gmail.com");
		// $to = array("midoff1@gmail.com","samuel.tan@comma.com.my");
		// $to = array("midoff1@gmail.com","bee-lui_tan@astro.com.my");
		$from = array($data['mailfrom'], $data['fromname']);

		// # Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject($emailSubject);
		$mailer->setBody($emailBody);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();
		$mailer->Encoding = 'base64';
		// $mailer->AddEmbeddedImage( './images/Cockoo.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
		return $mailer->send();
	}

	private function _sendTest()
	{
		$config = JFactory::getConfig();
		$user   = JFactory::getUser();
		$data = $user->getProperties();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$emailSubject = "Sorry, your ProspeRATty rush redemption has been rejected";

		$emailBody = '<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
		
		<head>
			<!-- NAME: 1 COLUMN -->
			<!--[if gte mso 15]>
						<xml>
								<o:OfficeDocumentSettings>
								<o:AllowPNG/>
								<o:PixelsPerInch>96</o:PixelsPerInch>
								</o:OfficeDocumentSettings>
						</xml>
						<![endif]-->
			<meta charset="UTF-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>Quake Club</title>
		
			<style type="text/css">
				p {
					margin: 5px 0;
					padding: 0;
				}
		
				table {
					border-collapse: collapse;
				}
		
				h1,
				h2,
				h3,
				h4,
				h5,
				h6 {
					display: block;
					margin: 0;
					padding: 0;
				}
		
				img,
				a img {
					border: 0;
					height: auto;
					outline: none;
					text-decoration: none;
				}
		
				body,
				#bodyTable,
				#bodyCell {
					height: 100%;
					margin: 0;
					padding: 0;
					width: 100%;
				}
		
				.mcnPreviewText {
					display: none !important;
				}
		
				#outlook a {
					padding: 0;
				}
		
				img {
					-ms-interpolation-mode: bicubic;
				}
		
				table {
					mso-table-lspace: 0pt;
					mso-table-rspace: 0pt;
				}
		
				.ReadMsgBody {
					width: 100%;
				}
		
				.ExternalClass {
					width: 100%;
				}
		
				p,
				a,
				li,
				td,
				blockquote {
					mso-line-height-rule: exactly;
				}
		
				a[href^=tel],
				a[href^=sms] {
					color: inherit;
					cursor: default;
					text-decoration: none;
				}
		
				p,
				a,
				li,
				td,
				body,
				table,
				blockquote {
					-ms-text-size-adjust: 100%;
					-webkit-text-size-adjust: 100%;
				}
		
				.ExternalClass,
				.ExternalClass p,
				.ExternalClass td,
				.ExternalClass div,
				.ExternalClass span,
				.ExternalClass font {
					line-height: 100%;
				}
		
				a[x-apple-data-detectors] {
					color: inherit !important;
					text-decoration: none !important;
					font-size: inherit !important;
					font-family: inherit !important;
					font-weight: inherit !important;
					line-height: inherit !important;
				}
		
				#bodyCell {
					padding: 20px;
				}
		
				.templateContainer {
					max-width: 600px !important;
				}
		
				a.mcnButton {
					display: block;
				}
		
				.mcnImage,
				.mcnRetinaImage {
					vertical-align: bottom;
				}
		
				.mcnTextContent {
					word-break: break-word;
				}
		
				.mcnTextContent img {
					height: auto !important;
				}
		
				.mcnDividerBlock {
					table-layout: fixed !important;
				}
		
				body,
				#bodyTable {
		
					background-color: #fafafa;
				}
		
				#bodyCell {
		
					border-top: 0;
				}
				.templateContainer {
		
					border: 0;
				}
		
				h1 {
		
					color: #202020;
		
					font-family: Helvetica;
		
					font-size: 26px;
		
					font-style: normal;
		
					font-weight: bold;
		
					line-height: 125%;
		
					letter-spacing: normal;
		
					text-align: left;
				}
		
				h2 {
		
					color: #202020;
		
					font-family: Helvetica;
		
					font-size: 22px;
		
					font-style: normal;
		
					font-weight: bold;
		
					line-height: 125%;
		
					letter-spacing: normal;
		
					text-align: left;
				}
		
				h3 {
		
					color: #202020;
		
					font-family: Helvetica;
		
					font-size: 20px;
		
					font-style: normal;
		
					font-weight: bold;
		
					line-height: 125%;
		
					letter-spacing: normal;
		
					text-align: left;
				}
		
				h4 {
		
					color: #202020;
		
					font-family: Helvetica;
		
					font-size: 18px;
		
					font-style: normal;
		
					font-weight: bold;
		
					line-height: 125%;
		
					letter-spacing: normal;
		
					text-align: left;
				}
				#templatePreheader {
		
					background-color: #fafafa;
		
					background-image: none;
		
					background-repeat: no-repeat;
		
					background-position: center;
		
					background-size: cover;
		
					border-top: 0;
		
					border-bottom: 0;
		
					padding-top: 0px;
		
					padding-bottom: 0px;
				}
		
				#templatePreheader .mcnTextContent,
				#templatePreheader .mcnTextContent p {
		
					color: #000000;
		
					font-family: Helvetica, Arial, sans-serif;
		
					font-size: 10px;
		
					line-height: 150%;
		
					text-align: center;
				}
		
		
				#templatePreheader .mcnTextContent a,
				#templatePreheader .mcnTextContent p a {
		
					color: #000000;
		
					font-weight: normal;
		
					text-decoration: underline;
				}
		
				#templateHeader {
		
					background-color: #ffffff;
		
					background-image: none;
		
					background-repeat: no-repeat;
		
					background-position: center;
		
					background-size: cover;
		
					border-top: 0;
		
					border-bottom: 0;
		
					padding-top: 0px;
		
					padding-bottom: 0;
				}
		
				#templateHeader .mcnTextContent,
				#templateHeader .mcnTextContent p {
		
					color: #202020;
		
					font-family: Helvetica, Arial, sans-serif;
		
					font-size: 16px;
		
					line-height: 150%;
		
					text-align: left;
				}
		
				#templateHeader .mcnTextContent a,
				#templateHeader .mcnTextContent p a {
		
					color: #007C89;
		
					font-weight: normal;
		
					text-decoration: underline;
				}
		
				#templateBody {
		
					background-color: #ffffff;
		
					background-image: none;
		
					background-repeat: no-repeat;
		
					background-position: center;
		
					background-size: cover;
		
					border-top: 0;
		
					border-bottom: 2px solid #EAEAEA;
		
					padding-top: 20px;
		
					padding-bottom: 20px;
				}
				#templateBody .mcnTextContent,
				#templateBody .mcnTextContent p {
		
					color: #000000;
		
					font-family: Helvetica, Arial, sans-serif;
		
					font-size: 16px;
		
					line-height: 150%;
		
					text-align: center;
				}
		
		
				#templateBody .mcnTextContent a,
				#templateBody .mcnTextContent p a {
		
					color: #000000;
		
					font-weight: normal;
		
					text-decoration: none;
				}
		
				#templateFooter {
		
					background-color: #000000;
		
					background-image: none;
		
					background-repeat: no-repeat;
		
					background-position: center;
		
					background-size: cover;
		
					border-top: 0;
		
					border-bottom: 0;
		
					padding-top: 0px;
		
					padding-bottom: 0px;
				}
		
				#templateFooter .mcnTextContent,
				#templateFooter .mcnTextContent p {
		
					color: #ffffff;
		
					font-family: Helvetica, Arial, sans-serif;
		
					font-size: 9px;
		
					line-height: 150%;
		
					text-align: center;
				}
				#templateFooter .mcnTextContent a,
				#templateFooter .mcnTextContent p a {
		
					color: #ffffff;
		
					font-weight: normal;
		
					text-decoration: none;
				}
		
				@media only screen and (min-width:768px) {
					.templateContainer {
						width: 600px !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					body,
					table,
					td,
					p,
					a,
					li,
					blockquote {
						-webkit-text-size-adjust: none !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					body {
						width: 100% !important;
						min-width: 100% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					#bodyCell {
						padding-top: 10px !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnRetinaImage {
						max-width: 100% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnImage {
						width: 100% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnCartContainer,
					.mcnCaptionTopContent,
					.mcnRecContentContainer,
					.mcnCaptionBottomContent,
					.mcnTextContentContainer,
					.mcnBoxedTextContentContainer,
					.mcnImageGroupContentContainer,
					.mcnCaptionLeftTextContentContainer,
					.mcnCaptionRightTextContentContainer,
					.mcnCaptionLeftImageContentContainer,
					.mcnCaptionRightImageContentContainer,
					.mcnImageCardLeftTextContentContainer,
					.mcnImageCardRightTextContentContainer,
					.mcnImageCardLeftImageContentContainer,
					.mcnImageCardRightImageContentContainer {
						max-width: 100% !important;
						width: 100% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnBoxedTextContentContainer {
						min-width: 100% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnImageGroupContent {
						padding: 9px !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnCaptionLeftContentOuter .mcnTextContent,
					.mcnCaptionRightContentOuter .mcnTextContent {
						padding-top: 9px !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnImageCardTopImageContent,
					.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,
					.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
						padding-top: 18px !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnImageCardBottomImageContent {
						padding-bottom: 9px !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnImageGroupBlockInner {
						padding-top: 0 !important;
						padding-bottom: 0 !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnImageGroupBlockOuter {
						padding-top: 9px !important;
						padding-bottom: 9px !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnTextContent,
					.mcnBoxedTextContentColumn {
						padding-right: 18px !important;
						padding-left: 18px !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnImageCardLeftImageContent,
					.mcnImageCardRightImageContent {
						padding-right: 18px !important;
						padding-bottom: 0 !important;
						padding-left: 18px !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcpreview-image-uploader {
						display: none !important;
						width: 100% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					h1 {
		
						font-size: 22px !important;
		
						line-height: 125% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					h2 {
		
						font-size: 20px !important;
		
						line-height: 125% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					h3 {
		
						font-size: 18px !important;
		
						line-height: 125% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					h4 {
		
						font-size: 16px !important;
		
						line-height: 150% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					.mcnBoxedTextContentContainer .mcnTextContent,
					.mcnBoxedTextContentContainer .mcnTextContent p {
		
						font-size: 14px !important;
		
						line-height: 150% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					#templatePreheader {
		
						display: block !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					#templatePreheader .mcnTextContent,
					#templatePreheader .mcnTextContent p {
		
						font-size: 10px !important;
		
						line-height: 150% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
		
					#templateHeader .mcnTextContent,
					#templateHeader .mcnTextContent p {
		
						font-size: 16px !important;
		
						line-height: 150% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
		
					#templateBody .mcnTextContent,
					#templateBody .mcnTextContent p {
		
						font-size: 14px !important;
		
						line-height: 150% !important;
					}
		
				}
		
				@media only screen and (max-width: 480px) {
					#templateFooter .mcnTextContent,
					#templateFooter .mcnTextContent p {
		
						font-size: 9px !important;
		
						line-height: 150% !important;
					}
		
				}
			</style>
		</head>
		
		<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fafafa;">
			<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Sorry, your ProspeRATty rush redemption has been rejected.</span>
			<!--<![endif]-->
			<center>
				<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #fafafa;">
					<tr>
						<td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 20px;width: 100%;border-top: 0;">
							<!-- BEGIN TEMPLATE // -->
							<!--[if (gte mso 9)|(IE)]>
														<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
														<tr>
														<td align="center" valign="top" width="600" style="width:600px;">
														<![endif]-->
							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
								<tr>
									<td valign="top" id="templateHeader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
											<tbody class="mcnImageBlockOuter">
												<tr>
													<td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
														<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
															<tbody>
																<tr>
																	<td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
		
		
																		<img align="center" alt="" src="http://quake.com.my/images/edm/edm-header-ams.png" width="600" style="max-width: 600px;padding-bottom: 0px;vertical-align: bottom;display: inline !important;border-radius: 0%;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">
		
		
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
										<!-- <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
											<tbody class="mcnImageBlockOuter">
												<tr>
													<td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
														<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
															<tbody>
																<tr>
																	<td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
		
		
																		<img align="center" alt="" src="http://quake.com.my/images/edm/main-birthday.jpg" width="600" style="max-width: 600px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">
		
		
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table> -->
									</td>
								</tr>
								<tr>
									<td valign="top" id="templateBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 2px solid #EAEAEA;padding-top: 20px;padding-bottom: 20px;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
											<tbody class="mcnTextBlockOuter">
												<tr>
													<td valign="top" class="mcnTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->
		
														<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
														<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%; min-height: 200px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
															<tbody>
																<tr>
		
																	<td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">
		
																		<p style="margin: 5px 0 15px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Dear member,</p>
		 
																		<p style="margin: 5px 0 15px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">
																			We apologise that we had found a slight error due to a bug in our system. Following this, we noticed that you have over-utilised your Q-Coins collected, hence your pending redemption(s) will be rejected.
																		</p>
		
																		<p style="margin: 5px 0 15px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">
																			We are sorry for any inconvenience caused.<br>
																			You can forward any enquiries to us.
																		</p>
		
																	</td>
																</tr>
															</tbody>
														</table>
														<!--[if mso]>
						</td>
						<![endif]-->
		
														<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" id="templateFooter" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #000000;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0px;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
											<tbody class="mcnTextBlockOuter">
												<tr>
													<td valign="top" class="mcnTextBlockInner" style="padding-top: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->
		
														<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
														<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
															<tbody>
																<tr>
		
																	<td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #ffffff;font-family: Helvetica, Arial, sans-serif;font-size: 9px;line-height: 150%;text-align: center;">
		
																	You received this message because you&apos;re our registered member or accepted our invitation to receive emails from Quake Club. ​No longer want to receive these emails?<a href=”mailto:#managingemail# ?Subject=Unsubscribe”>Unsubscribe</a>
																		<br>
																		<img height="71" alt="Astro Quake" src="http://quake.com.my/images/edm/footer-quake.png" style="border: 0px;width: 53px;height: 71px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="53">
																	</td>
																</tr>
															</tbody>
														</table>
														<!--[if mso]>
						</td>
						<![endif]-->
		
														<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
													</td>
												</tr>
											</tbody>
										</table>
										<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
											<tbody class="mcnFollowBlockOuter">
												<tr>
													<td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowBlockInner">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
															<tbody>
																<tr>
																	<td align="center" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
																			<tbody>
																				<tr>
																					<td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																						<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																							<tbody>
																								<tr>
																									<td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																										<!--[if mso]>
																				<table align="center" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																				<![endif]-->
		
																										<!--[if mso]>
																						<td align="center" valign="top">
																						<![endif]-->
		
		
																										<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																											<tbody>
																												<tr>
																													<td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																														<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																															<tbody>
																																<tr>
																																	<td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																																			<tbody>
																																				<tr>
		
																																					<td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																																						<a href="https://www.facebook.com/QuakeMY" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-fb.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
																																					</td>
		
		
																																				</tr>
																																			</tbody>
																																		</table>
																																	</td>
																																</tr>
																															</tbody>
																														</table>
																													</td>
																												</tr>
																											</tbody>
																										</table>
		
																										<!--[if mso]>
																						</td>
																						<![endif]-->
		
																										<!--[if mso]>
																						<td align="center" valign="top">
																						<![endif]-->
		
		
																										<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																											<tbody>
																												<tr>
																													<td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																														<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																															<tbody>
																																<tr>
																																	<td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																																			<tbody>
																																				<tr>
		
																																					<td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																																						<a href="https://api.whatsapp.com/send?phone=60126040968&text=Hi,%20send%20me%20the%20monthly%20issue%20of%20Winning%20Partnership%20Series%20by%20MARKETING" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-whatsapp.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
																																					</td>
		
		
																																				</tr>
																																			</tbody>
																																		</table>
																																	</td>
																																</tr>
																															</tbody>
																														</table>
																													</td>
																												</tr>
																											</tbody>
																										</table>
		
																										<!--[if mso]>
																						</td>
																						<![endif]-->
		
																										<!--[if mso]>
																						<td align="center" valign="top">
																						<![endif]-->
		
		
																										<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																											<tbody>
																												<tr>
																													<td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																														<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																															<tbody>
																																<tr>
																																	<td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																																			<tbody>
																																				<tr>
		
																																					<td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																																						<a href="http://quake.com.my/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-web.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
																																					</td>
		
		
																																				</tr>
																																			</tbody>
																																		</table>
																																	</td>
																																</tr>
																															</tbody>
																														</table>
																													</td>
																												</tr>
																											</tbody>
																										</table>
		
																										<!--[if mso]>
																						</td>
																						<![endif]-->
		
																										<!--[if mso]>
																				</tr>
																				</table>
																				<![endif]-->
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
															</tbody>
														</table>
		
													</td>
												</tr>
											</tbody>
										</table>
										<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
											<tbody class="mcnTextBlockOuter">
												<tr>
													<td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->
		
														<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
														<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
															<tbody>
																<tr>
		
																	<td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #ffffff;font-family: Helvetica, Arial, sans-serif;font-size: 9px;line-height: 150%;text-align: center;">
		
																		<!-- <span style="font-size:12px"><a href="#" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #ffffff;font-weight: normal;text-decoration: none;">Unsubscribe </a>| <a href="http://quake.com.my/user-profile/profile" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #ffffff;font-weight: normal;text-decoration: none;">Update Profile</a></span><br> -->
																		<br> © Astro Quake Club. All Right Reserved
																	</td>
																</tr>
															</tbody>
														</table>
														<!--[if mso]>
						</td>
						<![endif]-->
		
														<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</table>
							<!--[if (gte mso 9)|(IE)]>
														</td>
														</tr>
														</table>
														<![endif]-->
							<!-- // END TEMPLATE -->
						</td>
					</tr>
				</table>
			</center>
		</body>
		
		</html>
		';

		$to = 'khaiyee.wong@senheng.com.my';
		$from = array($data['mailfrom'], $data['fromname']);

		// # Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject($emailSubject);
		$mailer->setBody($emailBody);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();
		$mailer->Encoding = 'base64';
		// $mailer->AddEmbeddedImage( './images/Cockoo.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
		return $mailer->send();
	}
}
