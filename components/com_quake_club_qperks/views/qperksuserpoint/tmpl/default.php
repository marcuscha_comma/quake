<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_qperks
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_quake_club_qperks');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_quake_club_qperks'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_QPERKS_FORM_LBL_QPERKSUSERPOINT_USER_ID'); ?></th>
			<td><?php echo $this->item->user_id_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_QPERKS_FORM_LBL_QPERKSUSERPOINT_POINT'); ?></th>
			<td><?php echo $this->item->point; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_QPERKS_FORM_LBL_QPERKSUSERPOINT_TYPE'); ?></th>
			<td><?php echo $this->item->type; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_QPERKS_FORM_LBL_QPERKSUSERPOINT_CREATED_ON'); ?></th>
			<td><?php echo $this->item->created_on; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_quake_club_qperks&task=qperksuserpoint.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_QUAKE_CLUB_QPERKS_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_quake_club_qperks.qperksuserpoint.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_QUAKE_CLUB_QPERKS_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_QUAKE_CLUB_QPERKS_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_QUAKE_CLUB_QPERKS_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_quake_club_qperks&task=qperksuserpoint.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_QUAKE_CLUB_QPERKS_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>