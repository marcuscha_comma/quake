<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_qperks
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
?>

<div id="app">
	<div class="pink-page-title">
		<div class="container">
			<h2>Points History</h2>
		</div>
	</div>

  <div class="mt-4">
    <h2 class="f-20 mb-4">All activities</h2>

    <ul class="list-group point-history" >
      <li class="list-group-item"  v-for="(item, index) in itemsArray"
				v-if="(index+1) <= loadMoreNumber">
      <!-- <li class="list-group-item" v-for="item in itemsArray"> -->
        <div class="row align-items-center">
          <div class="col-md-10 col-12">
            <h6>{{item.type}} <span v-if="item.referred_name != null">- {{item.referred_name}}</span><span v-if="item.type == 'Event attendance' || item.type == 'Bonus Q-Perks'">- {{item.source}}</span></h6>
            <span class="date">{{item.created_on}}</span>
          </div>
          <div class="col-md-2 col-12">
            <h5 class="f-20 text-highlight bold text-md-right mt-3 mt-md-0">
              +{{item.point}}
            </h5>
          </div>
        </div>
      </li>
    </ul>
  </div>

	<div class="loading-bar" v-if="loadMoreNumber < itemsArray.length">
    <div @click="loadMore(loadMoreNumber)">Load more</div>
  </div>
</div>

<script type="text/javascript">
    var itemsObjFromPhp = <?php echo json_encode($this->filtered_items); ?>;

    var app = new Vue({
      el: '#app',
      data: {
        itemsArray: itemsObjFromPhp,
        loadMoreNumber: 10,
        showStatus: true,
      },
      mounted: function () {
        this.selectOnChanged();
      },
      updated: function () {
        jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
      },
      methods: {
        loadMore: function (loadMoreNumber) {
          this.loadMoreNumber = loadMoreNumber + 10;
        }
      }
    })
</script>
