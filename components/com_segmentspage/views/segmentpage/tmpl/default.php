<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_segmentspage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<div id="app">
    asd
</div>




<script type="text/javascript">
    var programmesObjFromPhp = <?php echo json_encode($this->programmes); ?>;


    var app = new Vue({
        el: '#app',
        data: {
            programmesArray: programmesObjFromPhp,
        },
        mounted: function () {
        },
        methods: {
        }
    })
</script>