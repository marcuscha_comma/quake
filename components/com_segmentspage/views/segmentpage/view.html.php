<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_segmentspage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the Segment Page Component
 *
 * @since  0.0.1
 */
class SegmentsPageViewSegmentPage extends JViewLegacy
{
	/**
	 * Display the Segment Page view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Assign data to the view
		$this->msg = 'Segment Page';

		//get list of programme
		$db_programme    = JFactory::getDBO();
		$query_programme = $db_programme->getQuery( true );
		$query_programme
			->select( '*' )
			->from( $db_programme->quoteName( '#__cus_programme' ) )
			->order( 'id asc' );
		$db_programme->setQuery( $query_programme );
		$programmes = $db_programme->loadObjectList();
		$this->programmes = $programmes;


		// Display the view
		parent::display($tpl);
	}
}
