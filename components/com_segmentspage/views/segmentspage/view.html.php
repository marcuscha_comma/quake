<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_segmentspage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
if(!class_exists('ContentHelperRoute')) require_once (JPATH_SITE . '/components/com_content/helpers/route.php');
/**
 * HTML View class for the Segments Page Component
 *
 * @since  0.0.1
 */
class SegmentsPageViewSegmentsPage extends JViewLegacy
{
	/**
	 * Display the Segments Page view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Assign data to the view
		$this->msg = 'Segments Page';

		//get list of segments
		$db_segments    = JFactory::getDBO();
		$query_segments = $db_segments->getQuery( true );
		$query_segments
			->select( '*' )
			->from( $db_segments->quoteName( '#__cus_segments' ) )
			->order( 'id asc' );
		$db_segments->setQuery( $query_segments );
		$segments = $db_segments->loadObjectList();
		
		//get list of channels
		$db_channel    = JFactory::getDBO();
		$query_channel = $db_channel->getQuery( true );
		$query_channel
			->select( '*' )
			->from( $db_channel->quoteName( '#__cus_channels' ) )
			->order( 'id asc' );
		$db_channel->setQuery( $query_channel );
		$channels = $db_channel->loadObjectList();

		//get list of programme
		// $db_programme    = JFactory::getDBO();
		// $query_programme = $db_programme->getQuery( true );
		// $query_programme
		// 	->select( '*' )
		// 	->from( $db_programme->quoteName( '#__cus_programme' ) )
		// 	->order( 'id asc' );
		// $db_programme->setQuery( $query_programme );
		// $programmes = $db_programme->loadObjectList();

		// get list of article
		$db_article    = JFactory::getDBO();
		$query_article = $db_article->getQuery( true );
		$query_article
			->select( '*, DATE_FORMAT(publish_up,"%M %d,%Y") as convDate' )
			->from( $db_article->quoteName( '#__content' ) )
			->order( 'id asc' );
		$db_article->setQuery( $query_article );
		$articles = $db_article->loadObjectList();

		foreach ($segments as $key => $segmentvalue) {
			$segmentvalue->channels = array();
			$segmentvalue->title = strtolower($segmentvalue->title);
			foreach ($channels as $key => $channelvalue) {
				$channelvalue->showTooltip = False;
				if ($segmentvalue->id == $channelvalue->segment) {
					array_push($segmentvalue->channels,$channelvalue);
				}
			}
			// echo "<pre>";
			// 	print_r($channelvalue);
			// 	echo "</pre>";
		}

		foreach ($articles as $key => $articlevalue) {
			$articlevalue->images = json_decode($articlevalue->images);
			$articlevalue->imagesDetails = array();
			foreach ($articlevalue->images as $imagevalue){
				$articlevalue->imagesDetails[] = $imagevalue;
			}
		}
		$this->segments = $segments;
		$this->channels = $channels;
		$this->articles = $articles;
		// $itemId = 1;

		// $link   = new JUri(JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId, false));
		// $link   = new JUri(JRoute::_('index.php?option=com_segmentpage&view=segmentpage&Itemid=' . $itemId, false));
		// foreach ($articles as $key => $articlevalue) {
		// 	$haha = rtrim(JUri::base(), '/') . JRoute::_(ContentHelperRoute::getArticleRoute(  $articlevalue->id,  $articlevalue->catid ));
			// echo "<pre>";
			// print_r($articles);
			// echo "</pre>";
			// $link->setVar('return', base64_encode(JRoute::_((ContentHelperRoute::getArticleRoute($articlevalue->id, $articlevalue->catid)))));
		// }
		
	



		// Display the view
		parent::display($tpl);
	}
}
