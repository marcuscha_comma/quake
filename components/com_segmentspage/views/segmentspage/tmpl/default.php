<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_segmentspage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<div id="app">
    <div class="row">
        <div class="col" @click="channelProfilesTabOnClick()">Channel Profiles</div>
        <div class="col" @click="segmentsHighlightsTabOnClick()">Programme Updates</div>
    </div>

    <div v-if="channelProfilesShow">
        <div class="row">
            <div class="col">
                <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit</b>
                <p>Cras condimentum efficitur eros, et pharetra lorem iaculis non. Duis sem lorem, pretium vitae faucibus id, molestie quis augue. Suspendisse imperdiet varius nulla, non malesuada sapien fringilla ut. Duis molestie nunc eu imperdiet egestas. Ut auctor sapien eget purus laoreet, et facilisis orci tristique. Nulla tincidunt, lorem eget rhoncus eleifend, metus nunc iaculis nibh, et finibus nisl mauris a enim. Nam ut odio dapibus, vehicula sem id, dignissim neque.</p>
            </div>
            <div class="col">Go to Download Center</div>
        </div>
        <div>
            <!-- Collapse start -->
            <div class="accordion" :id="'accordion'+segment.title" v-for="segment in segmentsArray">
                <div class="card">
                    <div class="card-header" :id="'heading'+segment.title">
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" :data-target="'#'+segment.title" aria-expanded="false" :aria-controls="segment.title">
                        {{segment.title}}
                        </button>
                    </h5>
                    </div>

                    <div :id="segment.title" class="collapse" :aria-labelledby="'heading'+segment.title" :data-parent="'#accordion'+segment.title">
                        <div class="card-body">
                            <div v-for="channel in segment.channels" style="margin-left:20px;">
                                <img :src="channel.image" alt="" @mouseover="mouseOver(channel)" @mouseleave="mouseLeave(channel)">
                                <div v-show="channel.showTooltip && channel.showTooltip!==''" :class="{ show : channel.showTooltip }">
                                    <div>{{channel.description}}</div>
                                    <div class="row">
                                        <div class="col">{{channel.channel_popup_label}}</div>
                                        <div class="col">{{channel.channel_popup_text}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Collapse End -->
        </div>
    </div>

    <div v-if="segmentsHighlightsShow">
        <div>Filter By</div>
        <select v-model="selectedSegment">
            <option value="">All Segments</option>
            <option v-for="segment in segmentsArray" :value="segment.id">
                {{ segment.title }}
            </option>
        </select>
        <select >
            <option value="">All Channels</option>
            <option v-if="channel.segment == selectedSegment" v-for="channel in channelsArray" :value="channel.id">
                {{ channel.title }}
            </option>
        </select>
        <select >
            <option value="2018">2018</option>
            <option value="2017">2017</option>
            <option value="2016">2016</option>
            <option value="2015">2015</option>
        </select>
        <select >
            <option value="0">Months</option>
            <option value="1">January</option>
            <option value="2">Febuary</option>
            <option value="3">March</option>
            <option value="4">April</option>
            <option value="5">May</option>
            <option value="6">June</option>
            <option value="7">July</option>
            <option value="8">August</option>
            <option value="9">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
        </select>

        <div>
            <div class="row">
                <div class="col" v-for="article in articlesArray">
                    <img :src="article.imagesDetails[0]" alt="">
                <div>{{article.convDate}}</div>
                <div>{{article.title}}</div>
                <div>Read More</div>
                </div>
            </div>
            <div>Load More</div>
        </div>
    </div>
</div>




<script type="text/javascript">
    var segmentsObjFromPhp = <?php echo json_encode($this->segments); ?>;
    var channelsObjFromPhp = <?php echo json_encode($this->channels); ?>;
    var articlesObjFromPhp = <?php echo json_encode($this->articles); ?>;


    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!',
            segmentsArray: segmentsObjFromPhp,
            channelsArray: channelsObjFromPhp,
            articlesArray: articlesObjFromPhp,
            channelProfilesShow: true,
            segmentsHighlightsShow: false,
            selectedSegment: ""
        },
        mounted: function () {
            console.log(this.articlesArray);
            
        },
        methods: {
            channelProfilesTabOnClick: function(){
                this.channelProfilesShow = true;
                this.segmentsHighlightsShow = false;
            },
            segmentsHighlightsTabOnClick: function(){
                this.channelProfilesShow = false;
                this.segmentsHighlightsShow = true;                
            },
            mouseOver: function(channel){
                channel.showTooltip = true;
            },
            mouseLeave: function(channel){
                channel.showTooltip = false;
            }
        }
    })
</script>