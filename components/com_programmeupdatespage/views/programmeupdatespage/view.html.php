<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_programmeupdatespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
if(!class_exists('ContentHelperRoute')) require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

/**
 * HTML View class for the Programme Updates Page Component
 *
 * @since  0.0.1
 */
class programmeupdatespageViewprogrammeupdatespage extends JViewLegacy
{
	/**
	 * Display the Programme Updates Page view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Assign data to the view
		$this->msg = 'Programme Updates Page';

		//get list of segments
		$db_segments    = JFactory::getDBO();
		$query_segments = $db_segments->getQuery( true );
		$query_segments
			->select( 'id, title' )
			->from( $db_segments->quoteName( '#__cus_segments' ) )
			->order( 'id asc' );
		$db_segments->setQuery( $query_segments );
		$segments = $db_segments->loadObjectList();
		
		//get list of channels
		$db_channel    = JFactory::getDBO();
		$query_channel = $db_channel->getQuery( true );
		$query_channel
			->select( 'id, title, segment' )
			->from( $db_channel->quoteName( '#__cus_channels' ) )
			->order( 'id asc' );
		$db_channel->setQuery( $query_channel );
		$channels = $db_channel->loadObjectList();

		// get list of article
		$db_article    = JFactory::getDBO();
		$query_article = $db_article->getQuery( true );
		$query_article
			->select( 'c.*, DATE_FORMAT(c.publish_up,"%M %d,%Y") as convDate, fv.value as segment, fv2.value as channel, fv3.value as image_link, MONTH(c.publish_up) as month, YEAR(c.publish_up) as year' )
			->from( $db_article->quoteName( '#__content', 'c') )
			->join('INNER', $db_article->quoteName('#__fields_values', 'fv') . ' ON (' . $db_article->quoteName('fv.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv.field_id = 1')
			->join('INNER', $db_article->quoteName('#__fields_values', 'fv2') . ' ON (' . $db_article->quoteName('fv2.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv2.field_id = 2')
			->join('INNER', $db_article->quoteName('#__fields_values', 'fv3') . ' ON (' . $db_article->quoteName('fv3.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv3.field_id = 7')
			->where($db_article->quoteName('c.catid')." =8 ")
			->order( 'id asc' );
		$db_article->setQuery( $query_article );
		$articles = $db_article->loadObjectList();

		foreach ($articles as $key => $articlevalue) {
			// $articlevalue->images = json_decode($articlevalue->images);
			// $articlevalue->imagesDetails = array();
			$articlevalue->url = JRoute::_(ContentHelperRoute::getArticleRoute(  $articlevalue->id,  $articlevalue->catid ));
			// foreach ($articlevalue->images as $imagevalue){
			// 	$articlevalue->imagesDetails[] = $imagevalue;
			// }		
		}

		$this->segments = $segments;
		$this->channels = $channels;
		$this->articles = $articles;

		// Display the view
		parent::display($tpl);
	}
}
