<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_programmeupdatespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<div id="app">
    <div class="tab-pills scrollable-tab">
      <div class="row">
        <div class="col"><a href="./channel-profiles" class="tab-pill">Channel Profiles</a></div>
        <div class="col"><a href="./programme-updates" class="tab-pill active">Programme Updates</a></div>
      </div>
    </div>

    <div>
        <div class="d-none d-md-block">
          <div class="filter-options text-left text-md-right">
            <label class="form-label">Filter By</label>
            <select class="selectpicker" v-model="selectedSegment" @change="selectOnChanged();selectedChannel=''">
                <option value="">All Segments</option>
                <option v-for="segment in segmentsArray" :value="segment.id">
                    {{ segment.title }}
                </option>
            </select>
            <select  class="selectpicker" v-model="selectedChannel" @change="selectOnChanged">
                <option value="">All Channels</option>
                <option v-for="channel in channelsArray" v-show="channel.segment == selectedSegment || selectedSegment ==''" :value="channel.id">
                    {{ channel.title }}
                </option>
            </select>
            <select class="selectpicker" v-model="selectedYear" @change="selectOnChanged">
                <option v-for="n in currentYear" v-if="n>=2016" :value="n">{{n}}</option>
            </select>
            <select  class="selectpicker" v-model="selectedMonth" @change="selectOnChanged">
                <option value="0">Months</option>
                <option value="1">January</option>
                <option value="2">Febuary</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
          </div>
        </div>

        <div class="mobile-filter d-block d-sm-block d-md-none">
          <div id="filter-trigger" class="btn btn-grey btn-block">
            Filter By
          </div>

          <div class="mobile-filter-container">
            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Segments</label>
              </div>
              <div class="col-7">
                <select class="selectpicker" v-model="selectedSegment" @change="selectOnChanged();selectedChannel=''" data-mobile="true">
                    <option value="">All Segments</option>
                    <option v-for="segment in segmentsArray" :value="segment.id">
                        {{ segment.title }}
                    </option>
                </select>
              </div>
            </div>

            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Channels</label>
              </div>
              <div class="col-7">
                <select  class="selectpicker" v-model="selectedChannel" @change="selectOnChanged" data-mobile="true">
                    <option value="">All Channels</option>
                    <option v-for="channel in channelsArray" v-show="channel.segment == selectedSegment || selectedSegment ==''" :value="channel.id">
                        {{ channel.title }}
                    </option>
                </select>
              </div>
            </div>

            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Year</label>
              </div>
              <div class="col-7">
                <select class="selectpicker" v-model="selectedYear" @change="selectOnChanged" data-mobile="true">
                    <option v-for="n in currentYear" v-if="n>=2016" :value="n">{{n}}</option>
                </select>
              </div>
            </div>

            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Months</label>
              </div>
              <div class="col-7">
                <select  class="selectpicker" v-model="selectedMonth" @change="selectOnChanged" data-mobile="true">
                    <option value="0">Months</option>
                    <option value="1">January</option>
                    <option value="2">Febuary</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
              </div>
            </div>

            <div id="filter-close" class="btn btn-pink">
              Done
            </div>
          </div>
        </div>

        <div>
            <div class="row">
                <div class="col-md-4 col-sm-6 article article-listing" v-for="(article, index) in articlesArray">
                    <div v-if="(index+1) <= loadMoreNumber">
                        <div @click="goToLink(article.url);" class="pointer img-holder" :style="{ 'background-image': 'url(' + article.image_link + ')' }">
                        <img class="placeholder" src="images/2020/card-guide.png" alt="">
                        </div>
                        <div class="article-date">{{article.convDate}}</div>
                        <div class="article-title">{{article.title}}</div>
                        <div class="article-link" @click="goToLink(article.url);">Read More</div>
                    </div>
                </div>
            </div>

            <div class="loading-bar" v-if="loadMoreNumber < articlesArray.length">
              <div @click="loadMore(loadMoreNumber)">Load More</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var segmentsObjFromPhp = <?php echo json_encode($this->segments); ?>;
    var channelsObjFromPhp = <?php echo json_encode($this->channels); ?>;
    var articlesObjFromPhp = <?php echo json_encode($this->articles); ?>;

    var app = new Vue({
        el: '#app',
        data: {
            segmentsArray: segmentsObjFromPhp,
            channelsArray: channelsObjFromPhp,
            articlesArray: articlesObjFromPhp,
            selectedSegment: "",
            selectedChannel: "",
            selectedYear: new Date().getFullYear(),
            selectedMonth: new Date().getMonth()+1,
            currentYear: new Date().getFullYear(),
            loadMoreNumber: 6,
        },
        mounted: function () {
            this.selectOnChanged();
        },
        updated() {
            var JS =jQuery.noConflict();
            JS(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
            selectOnChanged :function(){
                // this.selectedChannel = "";
                _this = this;
                _this.articlesArray = articlesObjFromPhp;

                if (this.selectedSegment != "") {
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.segment == _this.selectedSegment;
                    });
                    if (this.selectedChannel != "") {
                        _this.articlesArray = _this.articlesArray.filter(function(article){
                            return article.channel == _this.selectedChannel;
                        });
                    }
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.year == _this.selectedYear;
                    });
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.month == _this.selectedMonth;
                    });
                }else{
                    _this.articlesArray =articlesObjFromPhp;

                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.year == _this.selectedYear;
                    });
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.month == _this.selectedMonth;
                    });
                }
                console.log(this.selectedSegment,this.selectedChannel,this.selectedYear,this.selectedMonth);
            },
            loadMore :function(loadMoreNumber){
                this.loadMoreNumber = loadMoreNumber + 6;
            },
            goToLink :function(link){
                window.location.href = link;
            }
        }
    })
</script>
