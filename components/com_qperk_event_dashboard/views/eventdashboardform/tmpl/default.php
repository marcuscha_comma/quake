<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_dashboard
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
$user = JFactory::getUser();

$doc = JFactory::getDocument();
$fbimage = JURI::base() . "images/cny2020/sharer.jpg";
$doc->addCustomTag( '<meta property="og:image" content="' . $fbimage . '">' );

?>
<div id="app" class="cny-rat">

	<div class="cny-redemption">

		<h2 class="f-30 text-center mb-3 position-relative">
			<img src="images/cny2020/fan.svg" class="ml-2 d-none d-md-inline-block" width="60"/>
			Redemption Catalogue
			<img src="images/cny2020/fan.svg" class="mr-2 d-none d-md-inline-block" width="60"/>

			<div class="cny-blurb">
				<span>Redemption closing date - 31st December 2019</span>
			</div>
		</h2>
    <p class="text-center font-brand mt-4">
      Use your collected Q-Coins to redeem Chinese New Year rewards below!
    </p>

    <!-- CNY CART / BASKET -->
    <!-- <div class="basket-label-wrapper" v-if="userId!=0">
      <div @click="goToBasket()" class="pointer basket-label">
        <img src="images/cny2020/basket.svg"/>
        Basket
        <figure>
          <?php// echo $this->basket_num;?>
        </figure>
      </div>
    </div> -->
    <!-- END OF CNY CART / BASKET -->

    <div class="row">
      <div class="col-6 col-md-4"  v-for="product in products">
        <div @click="goToLink(product.id)" class="item product-item">
           <div class="image-holder"
              :style="{ 'background-image': 'url(./' + product.image1 + ')' }">
              <div v-if="product.promo == 1 && product.promo_label != ''" class="product-promo" >
                 {{product.promo_label}}
              </div>
            </div>

          <div class="price mt-3">
            <h3 v-if="product.promo == 1" class="product-points"><img src="images/cny2020/qcoin-slant.svg" class="qcoin"/> {{numberConvert(product.promo_qperks)}} Q-Coins</h3>
            <h3 class="product-points" :class="{'ori-price': product.promo == 1 }"><img src="images/cny2020/qcoin-slant.svg" class="qcoin"/> {{numberConvert(product.point)}} Q-Coins</h3>
          </div>
          <div class="promo-note">
            {{product.promo_description}}
          </div>
          <h3 class="product-name">{{product.name}}</h3>
        </div>
      </div>
    </div>
	</div>


  <div class="text-center mt-5">
    <h2 class="f-30 text-center mb-2">Share the ProspeRATty</h2>
    <p class="text-center font-brand">Spread the news! Share ProspeRATty Rush with your friends now.</p>

    <div id="addthis_button" style="padding-top:10px;" class="bold addthis_inline_share_toolbox"></div>
  </div>

  <div class="mt-5">
    <h3 class="f-20 mb-2">Campaign Terms & Conditions</h3>
    <div class="tnc-box">
      <div class="tnc-scrollbox">
				<p>Welcome to the ProspeRATty Rush video game service (hereinafter referred to as &ldquo;Service&rdquo;) which is
					accessible via the Quake website (&ldquo;Quake&rdquo;) that is owned by Astro Entertainment Sdn Bhd (“ASTRO”).
					Please carefully read the Terms of Service (&ldquo;Terms&rdquo;) and Privacy Statement &amp;
					Privacy Notice (&ldquo;Privacy Policy&rdquo;) below.</p>

				<h6>1. The Service Period</h6>

				<p><strong>1.1</strong> The Service shall run from 18 November 2019 until 8 December 2019, both dates inclusive.
				</p>

				<p><strong>1.2</strong> Quake reserves the right, upon giving adequate prior notice of THREE working days, to change
					the duration and/or the commencement and/or expiry dates of the Service Period.</p>

				<h6>2. The Acceptance of Terms</h6>
				</ol>

				<p><strong>2.1</strong> By using the Service, you are agreeing to these Terms. Quake may modify these Terms at any with and/or without any notification to you of the modified Terms on Quake (“Modified Terms”). It&rsquo;s important that you review any Modified Terms before you continue using the Service. If you continue to use the Service, you are bound by the Modified Terms. </p>

				<h6>3. Account Creation</h6>

				<p><strong>3.1</strong> To access the Service, you are not required to create a Quake Club account (“Account”). However, it will limit you from accessing the full features of the Service and enables you to save the amount of Q-Coins that you have collected during the Service. When you register for an Account, the Terms for Quake Club that you may find at<p><u><a
							href="http://quake.com.my/quake-club/terms-and-conditions">http://quake.com.my/quake-club/terms-and-conditions</a></u>
					applies.</p>


				<p><strong>3.2</strong> Should you intend to register for an Account you must take reasonable steps to protect your Login details private. You are responsible for all activities performed using your Account.</p>

				<h6>4. Service Rules and Conditions</h6>

				<p>Below are the rules and conditions for the Service. For &ldquo;how to play&rdquo; instructions, you may refer to
					the interface of the Service when you start playing.</p>

				<p><strong>4.1 Q-Coins</strong></p>

				<ul>
					<li>The Q-Coins collected will determine your placement in the weekly leaderboard (by using the
						Service, you will be hereinafter referred to as the &ldquo;Player&rdquo;).</li>
					<li>The Q-Coins collected will be reset every week. However, the Player can review accumulated Q-Coins on the
						Player&rsquo;s Quake Club dashboard. Q-Coins will not be stacked within the Player&rsquo;s accumulated Q-Perks.
					</li>
					<li>The Q-Coins accumulated can be used to redeem the Chinese New Year rewards only.</li>
				</ul>

				<p><strong>4.2 Frequency of Playtime</strong></p>

				<p><strong>4.2.1 For Quake Club Members:</strong></p>

				<ul>
					<li>During the Service Campaign Period, Quake Club members can get up to a maximum of THREE chances to play per day.</li>
					<li>ONE free chance is automatically deposited into the Player&rsquo;s Quake Club Account daily. This free chance cannot be brought forward to the next day and must be used on the same day.
						<ul>
							<li>While the other TWO chances will be deposited into the Player&rsquo;s Quake Club Account when they perform actions on Quake, i.e., ONE chance per document download or per article shared.</li>
							<li>When the Player performs ONE action (downloading document/sharing article), ONE additional chance is recorded in the Service. This life must be used on the same day. </li>
							<li>Only the first time download of any document or the first time sharing of any article will be awarded with ONE chance. Any download or sharing of the same file after the first time will NOT be eligible for any chance. </li>
						</ul>
					</li>

				</ul>

				<p><strong>4.2.2 For Non-Members of Quake Club:</strong></p>

				<ul>
					<li>Any non-member of Quake Club has ONE chance to play the Service daily, but any Q-Coins they accumulated can only be activated by signing up for an Account as a Quake Club member;</li>
					<li>Any Quake Club member who has not logged in will be considered as a non-member until they login with their Account. A Quake Club member who plays the game without logging in can save the Q-Coins that they accumulated in the Service by signing in as prompted before the Service starts or after the Service is over.</li>
				</ul>

				<h6>5. Chinese New Year Rewards Redemption, Redemption Limits and Delivery</h6>

				<p><strong>5.1</strong> Q-Coins accumulated in the Service can be used to redeem the Chinese New Year rewards only.
				</p>

				<p><strong>5.2</strong> All Players are advised to use all their accumulated Q-Coins by 31 December 2019 as all
					Q-Coins will expire on 1 January 2020.</p>



				<p><strong>5.3</strong> The redemption is unlimited (while stocks last) only for selected Chinese New Year rewards, for the other rewards, a limit of redemption will be imposed for each Player (or Quake Club member). The Player can refer to the redemption limit as per the table below:</p>

				<div class="table-responsive-md">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>
									<p><strong>Items for Redemption Catalouge</strong></p>
								</td>
								<td>
									<p><strong>Units</strong></p>
								</td>
								<td>
									<p><strong>Q-Coins Redemption per Item</strong></p>
								</td>
								<td>
									<p><strong>Redemption limit per person</strong></p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Joey Yap's Feng Shui &amp; Astrology 2020 (Kuala Lumpur) - English Session</p>
								</td>
								<td>
									<p>10</p>
								</td>
								<td>
									<p>3800</p>
								</td>
								<td>
									<p>2</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Aeon Cash Voucher</p>
								</td>
								<td>
									<p>100</p>
								</td>
								<td>
									<p>1200</p>
								</td>
								<td>
									<p>1</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Ikea Gift Card</p>
								</td>
								<td>
									<p>40</p>
								</td>
								<td>
									<p>3000</p>
								</td>
								<td>
									<p>1</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Starbucks Gift Card</p>
								</td>
								<td>
									<p>50</p>
								</td>
								<td>
									<p>1200</p>
								</td>
								<td>
									<p>1</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Astro Go Shop voucher</p>
								</td>
								<td>
									<p>50</p>
								</td>
								<td>
									<p>1200</p>
								</td>
								<td>
									<p>1</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>KOI Cash Voucher</p>
								</td>
								<td>
									<p>59</p>
								</td>
								<td>
									<p>600</p>
								</td>
								<td>
									<p>2</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Astro 2020 Plush Toy</p>
								</td>
								<td>
									<p>350</p>
								</td>
								<td>
									<p>1300</p>
								</td>
								<td>
									<p>-</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Astro 2020 CNY Album</p>
								</td>
								<td>
									<p>350</p>
								</td>
								<td>
									<p>500</p>
								</td>
								<td>
									<p>-</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>Astro 2020 CNY T-shirt</p>
								</td>
								<td>
									<p>100</p>
								</td>
								<td>
									<p>1500</p>
								</td>
								<td>
									<p>-</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<p><strong>5.4 </strong>Quake will send a notification email after each redemption about the order summary. Player (or Quake Club member) will be sent a confirmation email within three (3) working days after the notification email to inform them whether they are eligible for the rewards or otherwise. </p>

				<p><strong>5.5 </strong>The rewards will be delivered to the current address of the Player shown in Quake&rsquo;s
					record or any other address as authorised by the Player.</p>

				<p><strong>5.6</strong> Delivery of all the redemptions will be completed by 29 February 2020.</p>

				<p><strong>5.7 </strong>The delivery charges for the first attempt of delivery will be borne by Quake to the East
					and West Malaysia.</p>
				<p><strong>5.8</strong> Quake will not deliver to P.O. Box addresses and addresses outside Malaysia. The
					Player/recipient of reward(s) is obliged to present necessary ID documentation to the delivery staff, failing
					which the delivery staff has the right to refuse delivery and will return the redemption item to Quake as
					unclaimed.</p>
				<p><strong>5.9</strong> Delivery will only be made against a written acknowledgement of receipt of the rewards and of satisfaction with its physical condition by any occupant at the address of delivery or where such address is an office address, by any personnel at the office. Such acknowledgement shall be deemed to be the acknowledgement by the Player. All charges of second and subsequent delivery attempts due to unsuccessful delivery by the courier agent will be borne by the Player.p>
				<p><strong>5.10</strong> The Player is advised to examine all rewards upon receipt. All goods and services supplied will be covered by the Merchant&rsquo;s normal delivery terms of business. Except where the law provides otherwise, Quake and ASTRO will not be responsible for the quality or suitability of the goods or services or for any delay in delivery. If a Player finds the redemption item faulty/damaged, he/she is requested to contact directly with the respective merchants according to the warranty information.</p>
				<p><strong>5.11 </strong>As for Quake merchandise, if a Player finds the redemption item faulty/damaged, the Player can email quakeclub@astro.com.my within three (3) days from the received date. Any disputes after three (3) days will not be entertained.
				</p>
				<p><strong>5.12</strong> neither a Player nor any person on the Player&rsquo;s behalf is available to receive the redeemed reward at the delivery address on the date of the delivery of the rewards, the Player is advised to liaise directly with the relevant courier service company within the specified time frame and at the location as stated in the “sorry card” (dropped or left by the courier service company at the delivery address).p>
				<p><strong>5.13</strong> The delivery of certain rewards shall require an additional charge for shipping and handling which charge shall be borne by the Player. Quake may impose a separate delivery or courier charge or, where appropriate, deduct Q-Coins from a Quake Club member&rsquo;s account for delivery charges under the following circumstances:</p>
				<p>&nbsp;(a) Re-delivery of rewards that have been returned as a result of being unclaimed; or</p>
				<p>(b) Re-delivery of rewards that have been returned under the following circumstances i.e. incomplete address,
					non-Malaysian addresses, person has shifted, no such person or for any other failed delivery reasons.</p>
				<p><strong>5.14 </strong>Quake gives no warranty (whether expressed or implied) whatsoever with respect to rewards
					items acquired. In particular, Quake gives no warranty with respect to the quality of items acquired or their
					suitability for any purpose.</p>
				<p><strong>5.15</strong> Quake shall not be liable for any loss or damage whatsoever that may be suffered, or for
					any personal injury that may be suffered, to a Player, directly or indirectly, by use or non-use of rewards
					redeemed. For the purpose of covering the delivery of redemption rewards under these Terms, "force majeure" shall
					be deemed to be any cause affecting the performance of these Terms arising from or attributable to acts, events,
					omissions or accidents beyond the reasonable control of the party and without limiting the generality thereof
					shall include the following:</p>
				<p>(a) strikes, lock-outs or other industrial action;</p>
				<p>(b) civil commotion, riot, invasion, war threat or preparation for war;</p>
				<p>(c) fire, explosion, storm, flood, earthquake, subsidence, epidemic or other natural physical disaster; or</p>
				<p>(d) any changes in law and regulations, which have a material impact on the performing abilities of either party.
				</p>
				<p><strong>5.16</strong> Quake reserves the right at any time, without prior notice, to add / alter / modify /
					change (or) vary all of these Terms or to replace wholly, or in part, the reward(s) with other reward(s), whether
					similar or otherwise to the first-mentioned rewards, or to withdraw any reward altogether.</p>
				<p><strong>5.17</strong> Any dispute and/or complaints regarding the goods or services received as a reward shall be
					settled between the Player and the merchants which had supplied the goods or services. Quake will bear no
					responsibility for resolving such disputes and/or complaints, or for the dispute/complaint itself.</p>
				<p>(a) if it is given by such Party or its solicitors by post in a registered letter addressed to the other Party at
					its address herein mentioned or to its solicitors&rsquo; address and such notice shall be deemed to have been duly
					served at the time when such registered letter would in the ordinary course of post be delivered; or</p>
				<p>(b) if it is dispatched by such Party or its solicitors by hand or by courier to the other Party at its address
					hereinbefore mentioned or to its solicitors&rsquo; address and such notice shall be deemed to have been received
					at the time when the same is delivered; or</p>
				<p>(c) If it is transmitted by such Party or its solicitors by facsimile to the other Party to its facsimile number
					or to its solicitors&rsquo; facsimile from time to time notified by the same and such notice shall be deemed to
					have been received at the time of transmission provided that there has been a confirmed answerback.</p>
				<h6>6. Leaderboard</h6>

				<p><strong>6.1</strong> The leaderboard in the Service will display the Top THREE ranking the Players who have
					accumulated the most Q-Coins weekly as they play in the Service. The ranking will be recorded weekly for three
					weeks as per the duration recorded below:</p>

					<div class="table-responsive-md">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<td>
										<p><strong>Service Week</strong></p>
									</td>
									<td>
										<p><strong>Duration Dates</strong></p>
									</td>
								</tr>
								<tr>
									<td>
										<p>Week 1</p>
									</td>
									<td>
										<p>18 November 2019 &ndash; 24 November 2019</p>
									</td>
								</tr>
								<tr>
									<td>
										<p>Week 2</p>
									</td>
									<td>
										<p>25 November 2019 &ndash; 1 December 2019</p>
									</td>
								</tr>
								<tr>
									<td>
										<p>Week 3</p>
									</td>
									<td>
										<p>2 December 2019 &ndash; 8 December 2019</p>
									</td>
								</tr>
							</tbody>
						</table>

					</div>

				<ul>
					<li>Players who are logged as Quake Club members can save their accumulated coins throughout each Service week. The Leaderboard will be reset weekly for each Service week.</li>
				</ul>

				<p><strong>6.2</strong> Should there be a tie in the ranking, the Player who achieved the tying score at an earlier time than the other Player who achieved the same score will take the higher position.</p>

				<h6>7. Prizes</h6>

				<p><strong>7.1</strong> THREE weekly prizes will be given away to the Top THREE Players for each of the THREE weeks
					of Service Campaign Duration at the end of each Service week.</p>

				<p><strong>7.2 </strong>These weekly Top THREE Players&rsquo; names will be displayed on the leaderboard in the
					Service at the end of each Service week.</p>

				<p><strong>7.3 </strong>Once the Player has attained the title of one of the Top THREE ranking Players and earned a
					weekly prize, they will NOT be eligible for any other weekly prizes for the duration of Service. However, the
					Player can still play the Service as per usual to collect more Q-Coins to redeem the Chinese New Year rewards.</p>

				<p><strong>7.4 The prizes consist of the following and may be subject to changes from time to time:</strong></p>

				<div class="table-responsive-md">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td width="10%">
									<p><strong>Items</strong></p>
								</td>
								<td>
									<p><strong>First Place</strong></p>
								</td>
								<td>
									<p><strong>Second Place</strong></p>
								</td>
								<td>
									<p><strong>Third Place</strong></p>
								</td>
							</tr>
							<tr>
								<td>
									<p><strong>Week 1</strong></p>

								</td>
								<td>
									<p>Fitbit FB410GMBK-CJK Charge 3 - Graphite/Black</p>

								</td>
								<td>
									<p>Muji Diffuser (Large) + 2 Assorted Muji Essential Oil</p>

								</td>
								<td>
									<p>RM300 Uniqlo Voucher</p>

								</td>
							</tr>
							<tr>
								<td>
									<p><strong>Week 2</strong></p>

								</td>
								<td>
									<p>Sony SRS-XB41 Extra Bass Portable Bluetooth Speaker - Black</p>
								</td>
								<td>
									<p>RM500 JD Sports Cash Voucher</p>
								</td>
								<td>
									<p>RM300 Healthland Voucher</p>
								</td>
							</tr>
							<tr>
								<td>
									<p><strong>Week 3</strong></p>

								</td>
								<td>
									<p>RM700 Aeon Cash voucher</p>
								</td>
								<td>
									<p>Sony WF-1000X Wireless Noise Cancelling Headphones - Black</p>
								</td>
								<td>
									<p>RM300 Thai Odyssey Voucher</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<h6>8. Allowed Uses and Restrictions</h6>

				<p><strong>8.1</strong> The Player may not use any technological or other means (such as by cheating or using bugs
					or glitches in the Service, or by using third party tools or software) to use the Service in a way that is not
					within the spirit of fair play of these Terms. The Player specifically agree that they will NOT:</p>
				<ul>
					<li>use the Service for fraudulent or abusive purposes (including, without limitation, by using the Service to
						impersonate any person or entity);</li>
					<li>disguise, anonymise or hide the Player&rsquo;s IP address or the source of any material or content that the
						Player uploads into the Service;</li>
					<li>interfere with or disrupt the Service, servers or networks that provide the Service;</li>
					<li>attempt to decompile, reverse engineer, disassemble or hack any of the Service, or to defeat or overcome any
						encryption technology or security measures or data transmitted, processed or stored by Quake;</li>
					<li>disrupt the normal flow of a Service or otherwise act in a manner that is likely to negatively affect other
						Players&rsquo; ability to compete fairly when playing the Service;</li>
					<li>instigate, assist, or become involved in any type of attack, including without limitation distribution of a
						virus, denial of Service attacks upon the Service, or other attempts to disrupt the Service or any other
						person&rsquo;s use or enjoyment of the Service;</li>
					<li>disobey any requirements or regulations of any network connected to the Service;</li>
					<li>circumvent technological measures designed to control access to, or elements of, the Service; or</li>
					<li>do anything else that Quake deems not to be within the spirit of fair play or intent of the Service.</li>
				</ul>

				<h6>9. Account Suspension/Termination</h6>

				<p><strong>9.1</strong> For the Player who is member we may terminate their Account and their access to
					the Service at our sole discretion and without prior notice if they violate the Terms for this Service or the
					Terms of Quake Club: <u><a
							href="http://quake.com.my/quake-club/terms-and-conditions">http://quake.com.my/quake-club/terms-and-conditions</a></u>
				</p>

				<h6>10. Privacy</h6>

				<p><strong>10.1</strong> Please refer to our:</p>
				<p>Privacy Statement - <u><a href="http://quake.com.my/privacy-policy">http://quake.com.my/privacy-policy</a></u> ;
				</p>
				<p>Privacy Notice - <u><a href="http://quake.com.my/privacy-notice">http://quake.com.my/privacy-notice</a></u> and
				</p>
				<p>Policy on Personal Data - <u><a
							href="http://quake.com.my/quake-club/terms-and-conditions">http://quake.com.my/quake-club/terms-and-conditions</a></u>
					for how we collect, use, and disclose user information.</p>

				<h6>11. Updates &amp; Changes</h6>

				<p><strong>11.1</strong> The Service may be updated from time to time. Quake may require that the Players accept the
					updates to our Service. While Quake will make all reasonable efforts to inform the Players of any such updates, by
					playing the Service the Players hereby acknowledge and agree that Quake may update our Service, with or without
					informing or otherwise notifying the Players.</p>

				<h6>12. Governing Law</h6>

				<p><strong>12.1</strong> These Terms shall be governed by the laws of Malaysia.</p>

				<h6>13. Contacting Information</h6>

				<p><strong>13.1</strong> If the Player have any questions about the Terms, please contact us at
					quakeclub@astro.com.my</p>


      </div>
    </div>
  </div>
</div>

<!--- CNY Modal: PRIZE -->
<div class="modal fade cny-rat-modal" id="modalPrize" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><img src="images/cny2020/times.png" /></span>
      </button>
      <div class="modal-body">
				<div class="modal-scroll">
					<h3 class="modal-header">Weekly Prizes</h3>
					<p>Top 3 weekly players on the leaderboard will win the prizes below:</p>

					<h4 class="h-week mb-1">WEEK 1</h4>
					<p class="f-14 mb-3 font-weight-bold">18 Nov - 24 Nov</p>

					<div class="row mb-4 justify-content-center">
						<div class="col-lg-4 col-6 mb-3 prize-grid">
							<h4 class="h-rank">1st Prize</h4>

							<div class="prize-wrapper" style="background-image: url(images/cny2020/prize-0101.jpg)">
								<div class="prize-frame"></div>
								<div class="prize-name">
									Fitbit FB410GMBK-CJK Charge 3 - Graphite/Black
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-6 mb-3 prize-grid">
							<h4 class="h-rank">2nd Prize</h4>

							<div class="prize-wrapper" style="background-image: url(images/cny2020/prize-0102.jpg)">
								<div class="prize-frame"></div>
								<div class="prize-name">
									Muji Diffuser (Large) + 2 Assorted Muji Essential Oil
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-6 mb-3 prize-grid">
							<h4 class="h-rank">3rd Prize</h4>

							<div class="prize-wrapper" style="background-image: url(images/cny2020/prize-0103.jpg)">
								<div class="prize-frame"></div>
								<div class="prize-name">
									RM300<br />Uniqlo Voucher
								</div>
							</div>
						</div>
					</div>

					<h4 class="h-week mb-1">WEEK 2</h4>
					<p class="f-14 mb-3 font-weight-bold">25 Nov - 1 Dec</p>

					<div class="row mb-4 justify-content-center">
						<div class="col-lg-4 col-6 mb-3 prize-grid">
							<h4 class="h-rank">1st Prize</h4>

							<div class="prize-wrapper" style="background-image: url(images/cny2020/prize-0302.jpg)">
								<div class="prize-frame"></div>
								<div class="prize-name">
									Sony SRS-XB41 Extra Bass Portable Bluetooth Speaker - Black
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-6 mb-3 prize-grid">
							<h4 class="h-rank">2nd Prize</h4>

							<div class="prize-wrapper" style="background-image: url(images/cny2020/prize-0202_1.jpg)">
								<div class="prize-frame"></div>
								<div class="prize-name">
									RM500 <br />JD Sports  Cash Voucher
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-6 mb-3 prize-grid">
							<h4 class="h-rank">3rd Prize</h4>

							<div class="prize-wrapper" style="background-image: url(images/cny2020/prize-0203.jpg)">
								<div class="prize-frame"></div>
								<div class="prize-name">
									RM300 <br />Healthland Voucher
								</div>
							</div>
						</div>
					</div>

					<h4 class="h-week mb-1">WEEK 3</h4>
					<p class="f-14 mb-3 font-weight-bold">2 Dec - 8 Dec</p>

					<div class="row mb-4 justify-content-center">
						<div class="col-lg-4 col-6 mb-3 prize-grid">
							<h4 class="h-rank">1st Prize</h4>

							<div class="prize-wrapper" style="background-image: url(images/cny2020/prize-0301_1.jpg)">
								<div class="prize-frame"></div>
								<div class="prize-name">
									RM700 <br />Aeon Cash voucher
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-6 mb-3 prize-grid">
							<h4 class="h-rank">2nd Prize</h4>

							<div class="prize-wrapper" style="background-image: url(images/cny2020/prize-0201.jpg)">
								<div class="prize-frame"></div>
								<div class="prize-name">
									Sony WF-1000X Wireless Noise Cancelling Headphones - Black
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-6 mb-3 prize-grid">
							<h4 class="h-rank">3rd Prize</h4>

							<div class="prize-wrapper" style="background-image: url(images/cny2020/prize-0303.jpg)">
								<div class="prize-frame"></div>
								<div class="prize-name">
									RM300 <br />Thai Odyssey Voucher
								</div>
							</div>
						</div>
					</div>

					<div class="row justify-content-center">
						<div class="col-lg-8">
							<p>You can also redeem our special CNY rewards with <span class="d-inline-block">Q-Coins</span> by going to the campaign page and check out the catalogue!</p>
						</div>
					</div>
				</div>

        <img src="images/cny2020/corner-1.png" class="corner-1">
        <img src="images/cny2020/corner-2.png" class="corner-2">
      </div>
    </div>
  </div>
</div>

<!--- CNY Modal: RANKING -->
<div class="modal fade cny-rat-modal" id="modalRanking" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><img src="images/cny2020/times.png" /></span>
      </button>
      <div class="modal-body">
				<div class="modal-scroll">
					<h3 class="modal-header">Leaderboard</h3>

					<ul class="nav nav-justified" id="rankingTab">
						<li class="nav-item">
							<a class="nav-link <?php echo $this->leaderboard_1?"":"disabled";?>" id="week1-tab" data-toggle="tab" href="#week1" role="tab" aria-controls="week1" aria-selected="true">
								WEEK 1
								<small>18 Nov – 24 Nov</small>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo $this->leaderboard_2?"":"disabled";?>" id="week2-tab" data-toggle="tab" href="#week2" role="tab" aria-controls="week2" aria-selected="false">
								WEEK 2
								<small>25 Nov – 1 Dec</small>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo $this->leaderboard_3?"active":"disabled";?>" id="week3-tab" data-toggle="tab" href="#week3" role="tab" aria-controls="week3" aria-selected="false">
								WEEK 3
								<small>2 Dec – 8 Dec</small>
							</a>
						</li>
					</ul>
					<div class="tab-content" id="rankingTabContent">
						<div class="tab-pane fade" id="week1" role="tabpanel" aria-labelledby="week1-tab">
							<div class="ranking-table container-fluid">
								<div class="row table-head d-none d-md-flex">
									<div class="col-md-3">Rank</div>
									<div class="col-md-6">Name</div>
									<div class="col-md-3">Q-Coins</div>
								</div>
								<div class="row align-items-center mb-3 mb-md-0">
									<div class="col-md-3"><img src="images/cny2020/badge-1.png"/></div>
									<div class="col-md-6"><?php echo $this->leaderboard_1[0]['name']?$this->leaderboard_1[0]['name']:"-"; ?></div>
									<div class="col-md-3"><?php echo $this->leaderboard_1[0]['qcoins']?number_format($this->leaderboard_1[0]['qcoins'])." ":" -"; ?><span class="d-inline-block d-md-none">Q-Coins</span></div>
								</div>
								<div class="row align-items-center mb-3 mb-md-0">
									<div class="col-md-3"><img src="images/cny2020/badge-2.png"/></div>
									<div class="col-md-6"><?php echo $this->leaderboard_1[1]['name']?$this->leaderboard_1[1]['name']:"-"; ?></div>
									<div class="col-md-3"><?php echo $this->leaderboard_1[1]['qcoins']?number_format($this->leaderboard_1[1]['qcoins'])." ":" -"; ?><span class="d-inline-block d-md-none">Q-Coins</span></div>
								</div>
								<div class="row align-items-center mb-3 mb-md-0">
									<div class="col-md-3"><img src="images/cny2020/badge-3.png"/></div>
									<div class="col-md-6"><?php echo $this->leaderboard_1[2]['name']?$this->leaderboard_1[2]['name']:"-"; ?></div>
									<div class="col-md-3"><?php echo $this->leaderboard_1[2]['qcoins']?number_format($this->leaderboard_1[2]['qcoins'])." ":" -"; ?><span class="d-inline-block d-md-none">Q-Coins</span></div>
								</div>

								<?php if ($user->id != 0) { ?>
										<div class="row my-rank align-items-center">
										<div class="col-md-3">#<?php echo $this->leaderboard_1_own['week_num']==$this->leaderboard_1_own['is_winner']?" ":$this->leaderboard_1_own['rank']; ?></div>
										<div class="col-md-6">You</div>
										<div class="col-md-3"><?php echo number_format($this->leaderboard_1_own['qcoins'])." "; ?> <span class="d-inline-block d-md-none">Q-Coins</span></div>
									</div>
								<?php }?>

							</div>
						</div>
						<div class="tab-pane fade" id="week2" role="tabpanel" aria-labelledby="week2-tab">
						<div class="ranking-table container-fluid">
								<div class="row table-head d-none d-md-flex">
									<div class="col-md-3">Rank</div>
									<div class="col-md-6">Name</div>
									<div class="col-md-3">Q-Coins</div>
								</div>
								<div class="row align-items-center mb-3 mb-md-0">
									<div class="col-md-3"><img src="images/cny2020/badge-1.png"/></div>
									<div class="col-md-6"><?php echo $this->leaderboard_2[0]['name']?$this->leaderboard_2[0]['name']:"-"; ?></div>
									<div class="col-md-3"><?php echo $this->leaderboard_2[0]['qcoins']?number_format($this->leaderboard_2[0]['qcoins'])." ":" -"; ?><span class="d-inline-block d-md-none">Q-Coins</span></div>
								</div>
								<div class="row align-items-center mb-3 mb-md-0">
									<div class="col-md-3"><img src="images/cny2020/badge-2.png"/></div>
									<div class="col-md-6"><?php echo $this->leaderboard_2[1]['name']?$this->leaderboard_2[1]['name']:"-"; ?></div>
									<div class="col-md-3"><?php echo $this->leaderboard_2[1]['qcoins']?number_format($this->leaderboard_2[1]['qcoins'])." ":" -"; ?><span class="d-inline-block d-md-none">Q-Coins</span></div>
								</div>
								<div class="row align-items-center mb-3 mb-md-0">
									<div class="col-md-3"><img src="images/cny2020/badge-3.png"/></div>
									<div class="col-md-6"><?php echo $this->leaderboard_2[2]['name']?$this->leaderboard_2[2]['name']:"-"; ?></div>
									<div class="col-md-3"><?php echo $this->leaderboard_2[2]['qcoins']?number_format($this->leaderboard_2[2]['qcoins'])." ":" -"; ?><span class="d-inline-block d-md-none">Q-Coins</span></div>
								</div>
								<?php if ($user->id != 0) { ?>
										<div class="row my-rank align-items-center">
										<div class="col-md-3">#<?php echo $this->leaderboard_2_own['week_num']==$this->leaderboard_2_own['is_winner']?" ":$this->leaderboard_2_own['rank']; ?></div>
										<div class="col-md-6">You</div>
										<div class="col-md-3"><?php echo number_format($this->leaderboard_2_own['qcoins'])." "; ?> <span class="d-inline-block d-md-none">Q-Coins</span></div>
									</div>
								<?php }?>
							</div>
						</div>

						<div class="tab-pane fade show active" id="week3" role="tabpanel" aria-labelledby="week3-tab">
						<div class="ranking-table container-fluid">
								<div class="row table-head d-none d-md-flex">
									<div class="col-md-3">Rank</div>
									<div class="col-md-6">Name</div>
									<div class="col-md-3">Q-Coins</div>
								</div>
								<div class="row align-items-center mb-3 mb-md-0">
									<div class="col-md-3"><img src="images/cny2020/badge-1.png"/></div>
									<div class="col-md-6"><?php echo $this->leaderboard_3[0]['name']?$this->leaderboard_3[0]['name']:"-"; ?></div>
									<div class="col-md-3"><?php echo $this->leaderboard_3[0]['qcoins']?number_format($this->leaderboard_3[0]['qcoins'])." ":" -"; ?><span class="d-inline-block d-md-none">Q-Coins</span></div>
								</div>
								<div class="row align-items-center mb-3 mb-md-0">
									<div class="col-md-3"><img src="images/cny2020/badge-2.png"/></div>
									<div class="col-md-6"><?php echo $this->leaderboard_3[1]['name']?$this->leaderboard_3[1]['name']:"-"; ?></div>
									<div class="col-md-3"><?php echo $this->leaderboard_3[1]['qcoins']?number_format($this->leaderboard_3[1]['qcoins'])." ":" -"; ?><span class="d-inline-block d-md-none">Q-Coins</span></div>
								</div>
								<div class="row align-items-center mb-3 mb-md-0">
									<div class="col-md-3"><img src="images/cny2020/badge-3.png"/></div>
									<div class="col-md-6"><?php echo $this->leaderboard_3[2]['name']?$this->leaderboard_3[2]['name']:"-"; ?></div>
									<div class="col-md-3"><?php echo $this->leaderboard_3[2]['qcoins']?number_format($this->leaderboard_3[2]['qcoins'])." ":" -"; ?><span class="d-inline-block d-md-none">Q-Coins</span></div>
								</div>
								<?php if ($user->id != 0) { ?>
										<div class="row my-rank align-items-center">
										<div class="col-md-3">#<?php echo $this->leaderboard_3_own['week_num']==$this->leaderboard_3_own['is_winner']?" ":$this->leaderboard_3_own['rank']; ?></div>
										<div class="col-md-6">You</div>
										<div class="col-md-3"><?php echo number_format($this->leaderboard_3_own['qcoins'])." "; ?> <span class="d-inline-block d-md-none">Q-Coins</span></div>
									</div>
								<?php }?>
							</div>
						</div>
					</div>

					<p>Once you&rsquo;ve earned a position above, you will not be listed again, but you can still play to earn more Q-Coins and redeem special CNY rewards.</p>
				</div>

        <img src="images/cny2020/corner-1.png" class="corner-1">
        <img src="images/cny2020/corner-2.png" class="corner-2">
      </div>
    </div>
  </div>
</div>

<!--- CNY Modal: EARN MORE LIFE -->
<div class="modal fade cny-rat-modal" id="modalEarnLife" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><img src="images/cny2020/times.png" /></span>
      </button>
      <div class="modal-body">
        <h3 class="modal-header">Earn More Life</h3>
        <h4>How?</h4>
        <p>When you complete the game, you have more chances to earn more lives by performing these actions on Quake website* <br />(max 2 actions for 2 lives per day):</p>

        <div class="row justify-content-center">
          <div class="col-lg-3 col-md-6 mb-3 mb-md-0">
            <a href="./download-centre?s=2" class="btn btn-cny btn-game">Download Document</a>
          </div>
          <div class="col-lg-3 col-md-6">
            <a href="./whats-on?s=2" class="btn btn-cny btn-game">Share Article</a>
          </div>
        </div>

        <p class="mt-4">
          Number of Life Left:
          <span class="life-badge"><?php echo $this->records;?></span>
        </p>

        <p class="small">*you will not earn another chance if you download/share the same article that you have downloaded/shared previously.</p>

        <img src="images/cny2020/corner-1.png" class="corner-1">
        <img src="images/cny2020/corner-2.png" class="corner-2">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    var itemsObjFromPhp = <?php  echo json_encode($this->products); ?>;
    var recordsObjFromPhp = <?php  echo json_encode($this->records); ?>;
    var userIdObjFromPhp = <?php  echo json_encode($user->id); ?>;
    var leaderboardsObjFromPhp = <?php  echo json_encode($this->leaderboards); ?>;

    var app = new Vue({
        el: '#app',
        data: {
          products : itemsObjFromPhp,
          records : recordsObjFromPhp,
          userId : userIdObjFromPhp,
          leaderboards : leaderboardsObjFromPhp
        },
        mounted: function () {
			this.owlCarousel();
        },
        updated: function () {
        },
        methods: {
			owlCarousel: function(){
				jQuery('.owl-carousel1').owlCarousel({
					loop:true,
					margin:10,
					nav:true,
					responsive:{
						0:{
							items:1
						},
						600:{
							items:3
						},
						1000:{
							items:5
						}
					}
				})
			},
			rewriteUrl: function(url){
				return '../' + url;
			},
			goToLink :function(link){
          window.location.href = 'event-product/qperkseventproduct/'+link;
      },
      goToBasket :function(){
          window.location.href = 'event-cart';

      },
			numberConvert :function(number){
					return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				}
        }
    })
</script>
