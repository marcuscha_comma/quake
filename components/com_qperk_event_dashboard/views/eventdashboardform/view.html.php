<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_event_dashboard
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Qperk_event_dashboardViewEventdashboardform extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	protected $canSave;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = Factory::getApplication();
		$user = Factory::getUser();

		$this->state   = $this->get('State');
		$this->item    = $this->get('Item');
		$this->params  = $app->getParams('com_qperk_event_dashboard');
		$this->canSave = $this->get('CanSave');

		//get product list
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_qperks_event_products WHERE state > 0 order by ordering asc');
		$this->products = $db->loadObjectList();

		if ($user->id != 0) {
			$db = JFactory::getDBO();
			$db->setQuery('SELECT sum(quantity) FROM #__cus_qperks_event_cart WHERE state > 0 and user_id='.$user->id);
			$basket_num = $db->loadResult();
			$this->basket_num = $basket_num?$basket_num:0;
		}else{
			$this->basket_num = 0;
		}
		if ($user->id != 0) {
			$db = JFactory::getDBO();
			$db->setQuery('SELECT play_times_earn,is_winner FROM #__cus_quake_club_game_records WHERE state > 0 and user_id='.$user->id);
			$records = $db->loadRow();
		}
		$this->records = $records?$records[0]:0;

		$db = JFactory::getDBO();
		$db->setQuery('select u.name, sum(gp.qcoins) as qcoins, gp.week_num, u.id
		from #__cus_quake_club_game_points gp 
		join #__users u
		on u.id = gp.user_id
		join #__cus_quake_club_game_records gr
		on gp.user_id = gr.user_id
		where gp.user_id != 0 and gp.week_num = 47 and gr.is_winner not in (48,49)
		group by u.id
		order by sum(gp.qcoins) desc, gp.created_on asc');
		$this->leaderboard_1 = $db->loadAssocList();

		$db = JFactory::getDBO();
		$db->setQuery('select u.name, sum(gp.qcoins) as qcoins, gp.week_num
		from #__cus_quake_club_game_points gp 
		join #__users u
		on u.id = gp.user_id
		join #__cus_quake_club_game_records gr
		on gp.user_id = gr.user_id
		where gp.user_id != 0 and gp.week_num = 48 and gr.is_winner not in (47,49)
		group by u.id
		order by sum(gp.qcoins) desc, gp.created_on asc');
		$this->leaderboard_2 = $db->loadAssocList();

		$db = JFactory::getDBO();
		$db->setQuery('select u.name, sum(gp.qcoins) as qcoins, gp.week_num
		from qkpe1_cus_quake_club_game_points gp 
		join qkpe1_users u
		on u.id = gp.user_id
		join #__cus_quake_club_game_records gr
		on gp.user_id = gr.user_id
		where gp.user_id != 0 and gp.week_num = 49 and gr.is_winner not in (47,48)
		group by u.id
		order by sum(gp.qcoins) desc, gp.created_on asc');
		$this->leaderboard_3 = $db->loadAssocList();

		if ($user->id != 0) {

			$db = JFactory::getDBO();
			$db->setQuery('select sum(gp.qcoins) as qcoins
			from #__cus_quake_club_game_points gp
			where gp.user_id = '.$user->id.' and gp.week_num = 47
			group by gp.user_id');
			$leaderboard_1_own_point = $db->loadResult();

			$db = JFactory::getDBO();
			$db->setQuery('select sum(gp.qcoins) as qcoins
			from #__cus_quake_club_game_points gp
			where gp.user_id = '.$user->id.' and gp.week_num = 48
			group by gp.user_id');
			$leaderboard_2_own_point = $db->loadResult();

			$db = JFactory::getDBO();
			$db->setQuery('select sum(gp.qcoins) as qcoins
			from #__cus_quake_club_game_points gp
			where gp.user_id = '.$user->id.' and gp.week_num = 49
			group by gp.user_id');
			$leaderboard_3_own_point = $db->loadResult();
			
			$count = 1;
			if ($this->leaderboard_1) {
				foreach ($this->leaderboard_1 as $key => $value) {
				
					if ($user->id == $value['id']) {
						$this->leaderboard_1_own = array(
							'rank' => $count,
							'name' => $value['name'],
							'qcoins' => $value['qcoins'],
							'week_num' => $value['week_num'],
							'is_winner' => $records[1]
						);
					}
					$this->leaderboard_1_own['qcoins'] = $leaderboard_1_own_point;
					$count++;
				}
			}
			if ($this->leaderboard_2) {
				foreach ($this->leaderboard_2 as $key => $value) {
				
					if ($user->id == $value['id']) {
						$this->leaderboard_2_own = array(
							'rank' => $count,
							'name' => $value['name'],
							'qcoins' => $value['qcoins'],
							'week_num' => $value['week_num'],
							'is_winner' => $records[1]
						);
					}
					$this->leaderboard_2_own['qcoins'] = $leaderboard_2_own_point;
					
					$count++;
				}
			}
			if ($this->leaderboard_3) {
				foreach ($this->leaderboard_3 as $key => $value) {
				
					if ($user->id == $value['id']) {
						$this->leaderboard_3_own = array(
							'rank' => $count,
							'name' => $value['name'],
							'qcoins' => $value['qcoins'],
							'week_num' => $value['week_num'],
							'is_winner' => $records[1]
						);
					}
					$this->leaderboard_3_own['qcoins'] = $leaderboard_3_own_point;
					
					$count++;
				}
			}
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = Factory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', Text::_('COM_QPERK_EVENT_DASHBOARD_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = Text::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = Text::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
