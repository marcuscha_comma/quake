<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_event_dashboard
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Eventdashboards list controller class.
 *
 * @since  1.6
 */
class Qperk_event_dashboardControllerEventdashboards extends Qperk_event_dashboardController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Eventdashboards', $prefix = 'Qperk_event_dashboardModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
