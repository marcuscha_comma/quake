<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Blank_page
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>
<div class="container mt-30">
	<div class="row ">
		<div class="col-12 text-right">
			<img src="../../../../../images/2020/rsvp/astro.png" alt="">
		</div>
		
	</div>
</div>

<div class="container tnc-wrapper">
	
	<h1 class="text-left">CONTEST STANDARD TERMS AND CONDITIONS</h1>
	<br>
	<h6>1. <u>INTRODUCTION</u></h6>
	<p>1.1 This Contest Standard Terms and Conditions (“Standard T&C”) shall govern the Contest that is organised by the Organiser as named in the Basic Terms. The Standard T&C and shall be in addition to the terms as set out in the Basic Terms (the Basic Terms and the Standard T&C shall collectively be referred to as “Terms and Conditions”).</p>        

	<p>1.2 The Contest will be held during the Contest Period as set out in the Basic Terms. The Organiser reserves the right to vary, postpone or re-schedule the dates of the Contest or extend the Contest Period at its sole discretion.</p>

	<p>1.3 The brief mechanism of the Contest is set out in the Basic Terms. The Contestants must adhere to the mechanism of the Contest as may be briefed and communicated to the Contestants by the Organiser from time to time during the Contest Period.</p>

	<p>1.4 The Contest will be organized by the Organiser in the Contest Language as set out in the Basic Terms.</p>

	<p>1.5 The Entry Procedure of the Contest is as set out in the Basic Terms.</p>

	<p>1.6 The Organiser reserves the right to at any time, change, amend, delete or add to the Terms and Conditions and other rules and regulations including the mechanism of the Contest at its absolute discretion.</p>

	<p>1.7 The Organiser may terminate or suspend the Contest at any time at its absolute discretion in which case, the Organiser may elect not to award any prize. Such termination or suspension will not give rise to any claim by the Contestants. If the Contest is resumed by the Organiser, the Contestant shall abide by the Organiser’s decision regarding resumption of the Contest and disposition of the Prizes.</p>
	

	<h6>2. <u>CONTEST ENTRY</u></h6>

	<p>2.1 The Contest Entry must be complete, accurate and sent to the Organiser vide the Mode and to the Address together with such other documents as may be required as set out in the Basic Terms. Entries must be received by the Organiser on or before the Entry Deadline as set out in the Basic Terms.</p>

	<p>2.2 Where the Mode of entry is via short messaging service (“SMS”) or multimedia messaging service (“MMS”), each Contest Entry sent by the Contestant will be subject to a premium Charges as stipulated in the Basic Terms. This Charges is <b>in addition to</b> the standard fee charged by the Contestant’s telecommunications service provider.</p>

	<p>2.3 By submitting the Contest Entry, the Contestant shall be deemed to have read, understood, accepted and agree to be bound by the Terms and Conditions of the Contest.</p>

	<p>2.4 Submission of the Contest Entry does not guarantee the Contestant the opportunity to participate in the Contest. The Organiser shall be entitled to reject or refuse participation by the Contestants for reasons, including (without limitation) where the Contest Entry is not complete or any provisions in the Terms and Conditions is not fulfilled or adhered to by the Contestant.</p>

	<p>2.5 Entries must be received by the Organiser on or before the Entry Deadline as set out in the Basic Terms. Entries received after the stipulated time will be disqualified and ineligible for consideration for prizes.</p>

	<h6>3. <u>ELIGIBILITY</u></h6>

	<p>3.1 The Contestant Eligibility Criteria are as set out in the Basic Terms.</p>

	<p>3.2 Contestants may be required to submit further proof of their eligibility within such timeframe as may be required by the Organiser failing which the Organiser shall be entitled to disqualify the Contestant.</p>

	<h6>4. <u>INELIGIBILITY</u></h6>

	<p>4.1 Persons who are ineligible to participate in the Contest are as set out in the Basic Terms.</p>

	<h6>5. <u>DISQUALIFICATION</u></h6>

	<p>5.1 The Organiser reserves the right to disqualify Contestants and/or revoke the Prize (at any stage of the Contest) if:-</p>

	<p>5.1.1 The Contestant are ineligible or does not meet any of the Eligibility Criteria; or</p>

	<p>5.1.2 The Contestant breaches of the Terms and Conditions or other rules and regulations of the Contest or violated any applicable laws or regulations; or</p>

	<p>5.1.3 in the Organiser’s sole determination, it believes that the Contestant has attempted to undermine the operation of the Contest by fraud, cheating or deception.</p>

	<p>5.2 In the event of a disqualification after the Prize has been awarded, the Organiser reserves the right to demand for the return of the Prize or payment of its value from the ineligible Contestant.</p>

	<p>5.3 Whilst the Organiser will endeavour to conduct necessary verifications on the eligibility of contestants, failure to disqualify any ineligible contestants shall not be deemed a breach by the Organiser.</p>

	<h6>6. <u>WARRANTIES</u></h6>

	<p>6.1 The Contestant represents and warrants with the Organiser that -</p>

	<p>6.1.1 the Contestant has met all the eligibility criteria and has the right, authority and power to enter into the Contest in accordance with Terms and Conditions and shall provide such proof as the Organiser requires;</p>

	<p>6.1.2 all the statements (if any and if so required) made by the Contestant to the Organiser are true correct accurate and complete.
 </p>

	<p>6.2 In consideration of the Organiser offering to the Contestant the opportunity to participate in a Contest, the Contestant hereby unconditionally and irrevocably;</p>

	<p>6.2.1 agrees that if so required by the Organiser, the winner shall make himself/herself available
(without compensation) for the production, recording and publicity of the Contest during the such time and production schedule as may be notified by the Organiser:-
<ol type="i">
	<li>interview (which shall be recorded); and/or</li>
	<li>taking of still photos, audio and/or visual recording for promotions and publicity use.
(collectively “Recording”).</li>
</ol>
</p>

	<p>6.2.2 agrees and consents that the Organiser shall have right and absolute discretion to broadcast the Recording and/or use the slogan, names or nicknames on any of its programmes/channels in whole or in part at the Organiser’s discretion. All copyrights subsisting in the Recording shall belong to the Organiser absolutely.</p>

	<p>6.2.3 agrees that where Contestants are required to submit any photographs, drawings, pictures, slogans, any materials or other creative works, including voice or video recordings (collectively “Intellectual Property”) with the Contest Entry, the Contestant warrants that all Intellectual Property Rights in such submission does not infringe any third party intellectual property rights.
</p>

	<p>6.2.4 agrees that the Organiser reserves the right, at its sole and absolute discretion, to use and exploit the Intellectual Property via any means or media and in any manner and anytime that it deems fit without first obtaining any consent nor making any payment whatsoever to the Contestant and/or the Contest winner(s) and/or representatives.</p>

	<p>6.2.5 confirms that the Contestant has read and understood Terms and Conditions of the Contest and the Contestant agrees to abide by the said terms and conditions accordingly and agrees to cooperate and to follow all directions given to the Contestant.</p>

	<p>6.2.6 agrees that all Prizes to be awarded in the Contest is contingent upon the accuracy of the information provided and disclosures made by the Contestant and the full and complete performance of the Contestants warranties, undertakings and obligations hereunder.</p>

	<p>6.2.7 agrees that the Contestant shall not by act or omission, directly or indirectly bring the Organiser or the Sponsor into disrepute.</p>

	<p>6.2.8 agrees that the Contestant shall not without the prior written consent from the Organiser publish or disclose any information in connection with the Contest or Prize (including without limitation, to any representatives of media in any form whatsoever).</p>

	<p>6.2.9 agrees that the Contestant shall not give any product endorsement, any interviews or be involved in any articles or reports in respect of the Contest or the Prize with any third party.</p>

	<p>6.2.10 agrees that the Contestant’s participation in the Contest and/or Programme does not entitle the Contestant to wages, salary or any other compensation.</p>

	<h6>7. <u>PRIZES</u></h6>

	<p>7.1 The Prizes for the Contest shall be as set out in the Basic Terms.</p>

	<p>7.2 The winners’ names will be notified or announced by the Organiser by such mode and in such manner as set out in the Basic Terms.</p>

	<p>7.3 All Prizes must be collected within the Collection Period and at such Collection Venue as set out in the Basic Terms. Failure to claim Prizes shall result in the Prizes being forfeited by the Organiser and the Organiser, its agents, sponsors and representatives shall have no liability to the Winners in any respect whatsoever.</p>

	<p>7.4 Where the Prizes awarded non-cash prizes, the Contestant shall not be entitled to redeem the same for cash or other alternatives.
</p>

	<p>7.5 The Organiser does not guarantee the availability of non cash-Prizes and the Organiser shall be entitled to replace and/or substitute such prize(s) with any other prize(s) of similar value as determined by the Organiser, its agents or sponsors at its sole discretion.</p>

	<p>7.6 All prizes are strictly not transferable, assignable exchangeable or redeemable by the Contestant in any other form or manner other than that specified by the Organiser. All specific or special terms and conditions that are attached to the Prize (whether by the Organiser or its agent or sponsor must be adhered to by the Contestant.
</p>
	
	<p>7.7 Prizes must be claimed in person unless the Organiser prescribes other mode of collection. Where the Organiser elects to post a prize to a Contest winner, no responsibility will be accepted by the Organiser for the safe and effective postal delivery of the Prize.
</p>

	<p>7.8 In special situations, and subject to the absolute discretion of the Organiser, a Contest winner may nominate a designated representative to collect a prize. The representative will be required to present written authorisation from the Contest winner and identification which includes a photograph for both the Contest winner and his/her representative.</p>

	<p>7.9 The Contestant is responsible for any and all taxes payable as a result of a Prize being awarded or received (if applicable).
</p>

	<p>7.10 In the event that the Contestant chooses not to accept a Prize, the Prize shall be forfeited and the Prize which will be dealt with according to the absolute discretion of the Organiser.</p>

	<p>7.11 All Prizes are accepted entirely at the risk of the Contestant Prizes and are awarded by the Organiser and/or sponsors without any warranty of any kind express or implied. The Contestant shall execute a deed of release and indemnity in a form prescribed by the Organiser, if so required, in order to receive the Prize.</p>

	<p>7.12 <u>Holiday Prizes</u> If the Prizes awarded by the Organiser are travel/holiday prizes (“Holiday Prizes”):-</p>

	<p>7.12.1 Holiday Prizes must be taken in accordance with the dates and destinations specified by the Organiser, its travel agencies or sponsors failing which it shall be forfeited. No cash alternative or alternative destination will be offered by the Organiser, its agents or sponsor.</p>

	<p>7.12.2 Holiday Prizes must be taken in accordance with terms and conditions as may be set by the Organiser, travel agencies or sponsor including (without limitation) the following:-
		<ol type="i">
			<li>the terms and conditions respective airlines/carrier;</li>
			<li>the terms and limitations of any insurance policy relating to the Holiday Prize;</li>
			<li>the terms and conditions in connection all health and safety guidelines and instructions and all applicable legal and regulatory requirements.</li>
		</ol></p>

	<p>7.12.3 Any travel prizes won by minors shall be in accordance with the terms and conditions of the respective carrier and where specified, accompanied by the parent(s) or legal guardian at the parent(s) or legal guardian own cost and expense.</p>

	<p>7.12.4 Winners must hold a valid passport with at least six (6) months’ validity period. It shall be the responsibility of winners to obtain the necessary visa and other travel documents at their sole cost and expense. No compensation whatsoever will be given should the winner fail to obtain such documents, regardless of the circumstances.</p>

	<p>7.12.5 It shall be the responsibility of winners to obtain the necessary visa and other travel documents (if so required) at their sole cost and expense. Passport control and in-country authorities reserve the right to refuse entry. If the winner of a Holiday Prize is refused passage, entry or exit to or from the country being visited, the Organiser will not be responsible in any way to compensate the winner for such refusal of passage, entry or exit and any additional costs incurred will be at the sole cost and responsibility of the Winner.
</p>

	<p>7.12.6 In the event of unforeseen circumstances or circumstances outside the reasonable control of the Organiser and/or the sponsor, the Organiser and/or sponsor reserves the right to offer alternative destination of approximately similar value. No cash alternative will be offered.</p>

	<p>7.12.7 Any flights, other transport, airport details, accommodation or other aspects of the Holiday Prize, dates and times quoted by the Organiser, its agencies, sponsors or representatives are for guidance only and are subject to change without notice with no liability arising.</p>

	<p>7.12.8 Unless otherwise specified, the class of travel for any Holiday Prize incorporating an airfare is economy class.</p>

	<p>7.12.9 Holiday prize winner must have sufficient financial resources to meet any financial commitment which they may incur in connection with the travel prize (including, without limitation, transfers to and from any airport specified in the travel prize, meals and drinks, room services, laundry, excess baggage, personal, medical and/or baggage insurance, all items of personal nature, custom tax and airport tax) beyond those included in the travel prize itself.</p>

	<p>7.13 <u>Ticket Prizes or other give-aways</u><br>If the Prizes awarded by the Organiser are in the form of free tickets or other forms of give-aways,
winner shall be bound by the event promoter’s terms and conditions. In the case of ticket prizes, the winner must adhere to those terms and conditions set out on the ticket and the rules and regulations of the venue thereof.</p>

	<p>7.14 <u>Cash Prizes</u><br>Where applicable, cash prizes shall be issued to the winners in the form of a cheque. Cash prize
winners will be responsible for all related banking charges (including outstation cheque charges) imposed by banks in clearing his/her cheque.</p>

	<h6>8. <u>DECISIONS OF THE JUDGES/ORGANISER</u></h6>

	<p>8.1 The criteria for the Selection of Winners shall be as set out in the Basic Terms.</p> 

	<p>8.2 Notification of Winners will via the means as set out in the Basic Terms.</p>

	<p>8.3 The Judges/Organiser’s decisions on all matters relating to the Contest (including without limitation, the selection of Contestants, play of the contest and/or any resolutions made) shall be final and absolute and binding on the Contestants. No discussion, correspondence, enquiry, appeal or challenge in respect of any decision of the Organiser will be entertained.
</p>

	<p>8.4 The Contestant shall not dispute nor make any oral or written complaints, public announcements or statements on the same whether during or after the Contest Period.</p>

	<h6>9. <u>APROMOTIONAL ACTIVITES</u></h6>

	<p>9.1 The Organiser and/or its affiliates reserve the right to send SMS messages or email notification to the Contestants mobile phone numbers or email address containing information and promotional activities regarding any other Astro promotions.</p>
		
	<p>9.2 If a Contestant does not wish to receive such SMS messages or email the Contestant is required to call and inform the Organiser accordingly.</p>

	<h6>10. <u>GOVERNING LAW</u></h6>

	<p>10.1 The Terms and Conditions of the Contest shall be construed, governed and interpreted in accordance with the laws of Malaysia.</p>

	<h6>11. <u>CONFIDENTIALITY</u></h6>

	<p>11.1 The Contestant shall treat all Terms and Conditions and rules and regulations of the Contest and all information and knowledge obtained by the Contestant in relation to and/or in connection with the Contest and/or Programme and/or derived as a result of his/her participation in the same, including without limitation the Organiser’s business and operational details, the contest mechanics, the judging/selection criteria for the Contest (hereafter collectively known as “Confidential Information”), as confidential and the Contestant shall, during and after the Contest Period, take all reasonable precautions to prevent disclosures of the Confidential Information to unauthorized persons or entities for any reason whatsoever and undertakes to deliver to the Organiser all tangible materials embodying the Confidential Information including any documentation, records, listings, notes, sketches, drawings, memoranda, models accounts, reference materials, samples and machines readable media and equipment that is in any way related to the Confidential Information including all duplicates and copies thereof.</p>

	<h6>12. <u>INDEMNITY</u></h6>

	<p>12.1 The Contestants forever waive, release and discharge the Organiser, its agencies, sponsors and representatives from and against, any and all liabilities, costs, loss, damages or expenses which the Contestant or any party claiming through the Contestant hereafter may have arising out of acceptance of any Prize(s) or participation in the Contest including (but not limited to) personal injury and damage to property and whether or not direct, consequential or foreseeable.</p>

	<p>12.2 The Contestant shall indemnify the Organiser, its affiliates, agent and sponsors from and against all liability, cost, loss or expenses sufferred thereby as a result of the Contestant’s breach of the Contestant’s warranties and undertakings and any breach of the Terms and Conditions and/or the rules and regulations of the Contest.</p>

	<h6>13. <u>COSTS</u></h6>

	<p>13.1 All costs incurred by the Contestant in relation to and/or with respect to the Contest including without limitation postal charges or Internet Service Provider (ISP) charges (if applicable), all transport costs, communication charges, accommodation, meal costs and other related costs incurred by the Contestant as a result of and/or pursuant to his/her participation in the Contest shall be solely borne by the Contestant. The Organiser shall not be under any obligation to reimburse the Contestant for any of such costs and expenses incurred thereof.
</p>

	<h6>14. <u>LIMITATION OF LIABILITY</u></h6>

	<p>14.1 The Contestant acknowledges that his/her participation in the Contest shall be at his/her own risks.</p>

	<p>14.2 The Organiser, its agents, sponsors, representatives, affiliates and their respective directors, officers and employees, agents and assigns shall not be liable to any Contestant in respect of any failure to win a Prize in the Contest, defective Prizes or misuse of Prizes or any other loss, damages, costs, expenses, claims, liabilities, injury, death, accidents suffered by the Contestant during the Contest or arising out of or in connection with the Contest, the participation by the Contestant in the Contest and/or the Prizes awarded.</p>

	<p>14.3 The Oganiser will not be responsible or liable for</p>

	<p>14.3.1 any problem, loss or damage of whatsoever nature suffered by the Contestant or any party due
to any delay and/or failure in receiving and sending a Contest Entry as a result of any network, communication, ISP or system error, interruption and/or failure experienced by the Organiser or the Contestant’s telecommunication service provider and/or resulting from participation or the downloading of any materials in the Contest. In the event of such error, interruption and/or failure, the Organiser shall not be responsible or liable for any failure encountered by any Contestant to participate in the Contest or any failure encountered by the Organiser in fulfilling its obligations hereunder.</p>

	<p>14.3.2 any error (including error in notification of Contest winners), omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft, destruction, alteration of, or unauthorized access to entries, or entries lost or delayed whether or not arising during operation or transmission as a result of server functions, virus, bugs or other causes outside the Organiser’s control.
</p>
	
	<h6>15. <u>GENERAL</u></h6>

	<p>15.1 The Organiser, its agents, sponsors and representatives shall not be liable to perform any of their obligations in respect of the Contest and this Terms and Conditions, rules and regulations in respect of the Contest where they are unable to do so as a result of circumstances beyond its control and shall not be liable to compensate the Contestants in any manner whatsoever in such circumstances.</p>

	<p>15.2 The Contestants shall not be entitled to assign any of the rights or sub-contract any of the obligations herein. The Organiser shall be entitled to assign or sub-license the whole or any part of its rights hereunder to any third party as may be determined by the Organiser.</p>

	<p>15.3 All rights and privileges herein granted to the Organiser irrevocable and not subjected to rescission, restraint or injunction under any and all circumstances. Under no circumstances shall the Contestants have the right to injunctive relief or to restrain or otherwise interfere with the organization of the Contest, the production, distribution, exhibition and/or exploitation of the Contest and/or Programme and/or any product based on and/or derived from the Contest and/or Programme.</p>

	<p>15.4 The invalidity, illegality or unenforceability of any terms hereunder shall not affect or impair the continuation in force of the remainder of the Terms and Conditions of the Contest.</p>

	<p>15.5 The main language of the Terms and Conditions shall be the English language. Any translation to any other language than English shall be for convenience only. In the event of any inconsistency between this English language and any other languages, the English language version shall prevail and govern in all respects.
</p>
</div>

<img class="cloud cloud-left img-fluid" src="../../../../../images/2020/rsvp/footer-left.png" alt="">
<img class="cloud cloud-right img-fluid" src="../../../../../images/2020/rsvp/footer-right.png" alt="">

<script>
	jQuery( document ).ready(function() {
		document.title = 'Terms and Conditions';
	});
</script>