<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Blank_page
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

// JHtml::_('behavior.keepalive');
// JHtml::_('behavior.tooltip');
// JHtml::_('behavior.formvalidation');
// JHtml::_('formbehavior.chosen', 'select');

// // Load admin language file
// $lang = JFactory::getLanguage();
// $lang->load('com_blank_page', JPATH_SITE);
// $doc = JFactory::getDocument();
// $doc->addScript(JUri::base() . '/media/com_blank_page/js/form.js');

// $user    = JFactory::getUser();
// $canEdit = Blank_pageHelpersBlank_page::canUserEdit($this->item, $user);


?>
<style>
    /* style the elements with CSS */
    #pleaserotate-graphic{
        fill: #fff;
    }

    #pleaserotate-backdrop {
        color: #fff;
        background-color: #000;
	}
	/* body{
		overflow:hidden;
	} */

#flipbook{
    width:400px;
    height:300px;
}

#flipbook .page{
    width:400px;
    height:300px;
    background-color:white;
    line-height:300px;
    font-size:20px;
    text-align:center;
}

#flipbook .page-wrapper{
    -webkit-perspective:2000px;
    -moz-perspective:2000px;
    -ms-perspective:2000px;
    -o-perspective:2000px;
    perspective:2000px;
}

#flipbook .hard{
    background:#ccc !important;
    color:#333;
    -webkit-box-shadow:inset 0 0 5px #666;
    -moz-box-shadow:inset 0 0 5px #666;
    -o-box-shadow:inset 0 0 5px #666;
    -ms-box-shadow:inset 0 0 5px #666;
    box-shadow:inset 0 0 5px #666;
    font-weight:bold;
}

#flipbook .odd{
    background:-webkit-gradient(linear, right top, left top, color-stop(0.95, #FFF), color-stop(1, #DADADA));
    background-image:-webkit-linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    background-image:-moz-linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    background-image:-ms-linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    background-image:-o-linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    background-image:linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    -webkit-box-shadow:inset 0 0 5px #666;
    -moz-box-shadow:inset 0 0 5px #666;
    -o-box-shadow:inset 0 0 5px #666;
    -ms-box-shadow:inset 0 0 5px #666;
    box-shadow:inset 0 0 5px #666;

}

#flipbook .even{
    background:-webkit-gradient(linear, left top, right top, color-stop(0.95, #fff), color-stop(1, #dadada));
    background-image:-webkit-linear-gradient(left, #fff 95%, #dadada 100%);
    background-image:-moz-linear-gradient(left, #fff 95%, #dadada 100%);
    background-image:-ms-linear-gradient(left, #fff 95%, #dadada 100%);
    background-image:-o-linear-gradient(left, #fff 95%, #dadada 100%);
    background-image:linear-gradient(left, #fff 95%, #dadada 100%);
    -webkit-box-shadow:inset 0 0 5px #666;
    -moz-box-shadow:inset 0 0 5px #666;
    -o-box-shadow:inset 0 0 5px #666;
    -ms-box-shadow:inset 0 0 5px #666;
    box-shadow:inset 0 0 5px #666;
}

.btn-submit {
  font-size: 12px;
}

.btn-submit i {
  margin-right: 6px;
}

#nav-left, #nav-right {
  cursor: pointer;
  height: 100%;
}

#nav-left i, #nav-right i {
  -webkit-transition: all 0.3s ease-out;
  -moz-transition: all 0.3s ease-out;
  -o-transition: all 0.3s ease-out;
  transition: all 0.3s ease-out;
  margin-top: 100px;
  margin-bottom: 100px;
}

#nav-left:hover i {
  margin-right: 10px;
}
#nav-right:hover i {
  margin-left: 10px;
}

</style>
<div class="container-fluid mt-5">
<div class="row">
	<div class="col-3 offset-1" >
		<img id="logo-click" src="../../../../../images/rsvp/astro.png" alt="" style="margin-bottom: 15px;">
	</div>
  <div class="col-7 text-right">
    <div id="expand-click" class="d-inline-block mt-2 mr-2">
      <div class="btn btn-sm btn-submit">
        <i class="fas fa-expand" style="cursor:pointer;"></i> Fullscreen
      </div>
    </div>
    <div class="d-inline-block mt-2 mr-2">
      <div class="btn btn-sm btn-submit" id="download-click">
        <i class="fas fa-arrow-down" style="cursor:pointer;"></i> Download
      </div>
	  <div class="btn btn-sm btn-submit" id="close-click">
        <i class="fas fa-window-close" style="cursor:pointer;"></i> Close
      </div>
    </div>
  </div>
</div>
<div class="row align-items-center" style="padding-top:5px; max-height: 85%;" id="middle-content-height">

	<div class="col-1 text-right " id="nav-left" onclick='$("#flipbook").turn("previous");'>
		<i class="fas fa-chevron-left"></i>
	</div>

	<div class="col-10" id="middle-content">
		<div id="flipbook" class="flipbook">
			<!-- <div class="hard"> Turn.js </div> -->
			<!-- <div class="hard"></div> -->
			<div class="cny-box-img-1 cny-box"></div>
			<div class="cny-box-img-2 cny-box"></div>
			<div class="cny-box-img-3 cny-box"></div>
			<div class="cny-box-img-4 cny-box"></div>
			<div class="cny-box-img-5 cny-box"></div>
			<div class="cny-box-img-6 cny-box"></div>
			<div class="cny-box-img-7 cny-box"></div>
			<div class="cny-box-img-8 cny-box"></div>
			<div class="cny-box-img-9 cny-box"></div>
			<div class="cny-box-img-10 cny-box"></div>

			<!-- <div class="hard"></div>
			<div class="hard"></div> -->
		</div>
	</div>

	<div class="col-1" id="nav-right" onclick='$("#flipbook").turn("next");'>
		<i class="fas fa-chevron-right"></i>
	</div>

</div>

<img class="cloud cloud-left img-fluid" src="../../../../../images/rsvp/footer-left.png" alt="">
<img class="cloud cloud-right img-fluid" src="../../../../../images/rsvp/footer-right.png" alt="">

<script type="text/javascript">

	var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
	if (iOS) {
		$( "#expand-click div" ).hide();
		$('#close-click').hide();
	}
	$('#close-click').hide();

	


	window.onorientationchange = function() {
        var orientation = window.orientation;
            switch(orientation) {
                case 0:
                case 90:
                case -90: window.location.reload();
                break; }
    };

	document.body.addEventListener('touchmove', function(e) {
		e.preventDefault();
		// e.stopPropagation();
		});

		function loadApp() {

			var size = getSize();

		// Create the flipbook

		$('.flipbook').turn({

			// Width
			width: size.width,

			// Height
			height: size.height,

			// Elevation
			elevation: 50,

			// Enable gradients
			gradients: true,

			// Auto center this flipbook
			autoCenter: true,

			duration : 2000,


		});
		$('.flipbook').turn('display', 'single');


		}

		function getSize() {
			// var width = document.body.clientWidth;
			// var height = document.body.clientHeight;
			var width = (window.innerWidth > 0) ? $("#middle-content").width() : screen.width;
			var height = (window.innerHeight > 0) ? '40vw' : '40vw';
			return {
				width: width,
				height: height
			}
		}

		function resize() {
		// console.log('resize event triggered');

		var size = getSize();
		// console.log(size);

		if (size.width > size.height) { // landscape
			$('.flipbook').turn('display', 'single');
		}
		else {
			$('.flipbook').turn('display', 'single');
		}

		$('.flipbook').turn('size', size.width, size.height);
		}

		// Load App
		loadApp();

		var scrollPosition = [
			self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
			self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
		];

		var download_button = document.getElementById("download-click");

		download_button.onclick = function(){
			location.href = "../../../../../uploads/cny-dl-files/ASTRO CNY Story (8 facts).pdf";
		}

		var elem = document.getElementById("expand-click");

		elem.onclick = function() {
			$('#expand-click div').hide();
			$('#logo-click').hide();
			$('#download-click').hide();
			var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
			if (iOS) {
				$('#close-click').hide();
			}else{
				$('#close-click').show();
			}
			
			if (!document.fullscreenElement &&    // alternative standard method
				!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
				if (document.documentElement.requestFullscreen) {
				document.documentElement.requestFullscreen();
				} else if (document.documentElement.msRequestFullscreen) {
				document.documentElement.msRequestFullscreen();
				} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
				} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
				}
			} else {
				if (document.exitFullscreen) {
				document.exitFullscreen();
				} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
				} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
				} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
				}
			}
			screen.orientation.lock("landscape");
		}

		var elem2 = document.getElementById("close-click");

		elem2.onclick = function() {
			$('#expand-click div').hide();
			$('#logo-click').show();
			$('#download-click').hide();
			var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
			if (iOS) {
				$('#close-click').hide();
			}else{
				$('#close-click').show();
			}
			if (!document.fullscreenElement &&    // alternative standard method
				!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
				if (document.documentElement.requestFullscreen) {
				document.documentElement.requestFullscreen();
				} else if (document.documentElement.msRequestFullscreen) {
				document.documentElement.msRequestFullscreen();
				} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
				} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
				}
			} else {
				if (document.exitFullscreen) {
				document.exitFullscreen();
				} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
				} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
				} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
				}
			}
			screen.orientation.lock("landscape-primary");
		}

		document.addEventListener('fullscreenchange', exitHandler);
		document.addEventListener('webkitfullscreenchange', exitHandler);
		document.addEventListener('mozfullscreenchange', exitHandler);
		document.addEventListener('MSFullscreenChange', exitHandler);
		function exitHandler() {
			if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
				$('#expand-click div').show();
				$('#download-click').show();
				$('#logo-click').show();
				$('#close-click').hide();
				var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
				if (iOS) {
					$('#close-click').hide();
				}else{
					$('#close-click').hide();
				}
			}
		}

</script>
