<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Blank_page
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Blank_page model.
 *
 * @since  1.6
 */
class Blank_pageModelBlankpage extends JModelItem
{

}
