<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Channels
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Channels', JPATH_COMPONENT);
JLoader::register('ChannelsController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Channels');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
