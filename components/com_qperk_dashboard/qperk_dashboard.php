<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_dashboard
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Qperk_dashboard', JPATH_COMPONENT);
JLoader::register('Qperk_dashboardController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Qperk_dashboard');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
