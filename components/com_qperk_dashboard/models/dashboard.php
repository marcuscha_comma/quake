<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_dashboard
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Qperk_dashboard model.
 *
 * @since  1.6
 */
class Qperk_dashboardModelDashboard extends JModelItem
{

}
