<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_dashboard
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class Qperk_dashboardViewDashboard extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();
		$jdate = new JDate;
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		$this->state  = $this->get('State');
		$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_qperk_dashboard');

		//for event game
		// if ($user->id != 0) {

		// 	$db->setQuery('SELECT id FROM #__cus_quake_club_game_records where user_id ="'.$user->id .'"');
		// 	$gameRecordId             = $db->loadResult();

		// 	if (!$gameRecordId) {
		// 		$db->setQuery('SELECT MAX(ordering) FROM #__cus_quake_club_game_records');
		// 		$max             = $db->loadResult();
		// 		$gameRecord = new stdClass();
		// 		$gameRecord->user_id = $user->id;
		// 		$gameRecord->modified_by = $user->id;
		// 		$gameRecord->created_by = $user->id;
		// 		$gameRecord->play_times_count = 1;
		// 		$gameRecord->state=1;
		// 		$gameRecord->ordering=$max + 1;
		// 		$gameRecord->created_on=$jdate->toSql();

		// 		JFactory::getDbo()->insertObject('#__cus_quake_club_game_records', $gameRecord);
		// 	}
		// }

		//get product list
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_qperks_products WHERE state > 0 and highlight = 1 order by ordering asc');
		$this->products = $db->loadObjectList();

		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_qperk_dashboard');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_QPERK_DASHBOARD_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
