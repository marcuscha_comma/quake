<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_dashboard
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
$user = JFactory::getUser();
$config = JFactory::getConfig();
$offset = $config->get('offset');
// $date = new JDate('now', $offset);
// $convertedDate = $date->format('Y-m-d');
//get all slider
$user_total_qperks_point_dob = 0;
$db_cus_quake_club_qperks_monthly    = JFactory::getDBO();
$query_cus_quake_club_qperks_monthly = $db_cus_quake_club_qperks_monthly->getQuery( true );
$query_cus_quake_club_qperks_monthly
  ->select( 'sum(month01+month02+month03+month04+month05+month06+month07+month08+month09+month10+month11+month12) as total' )
  ->from( $db_cus_quake_club_qperks_monthly->quoteName( '#__cus_quake_club_qperks_monthly' ) )
  ->where($db_cus_quake_club_qperks_monthly->quoteName('state')." > 0")
  ->where($db_cus_quake_club_qperks_monthly->quoteName('user_id')." = " .$user->id)
  ->order( 'ordering asc' );
$db_cus_quake_club_qperks_monthly->setQuery( $query_cus_quake_club_qperks_monthly );
$user_total_qperks_point = $db_cus_quake_club_qperks_monthly->loadResult();

if ($user->dob_redemption_num == 1) {
  $user_total_qperks_point_dob = $user_total_qperks_point + 200;
}else{
  $user_total_qperks_point_dob = $user_total_qperks_point;
}



// $db = JFactory::getDBO();
// $db->setQuery('SELECT count(id) as total FROM #__cus_qperks_user_point WHERE type in(7,3,11,13,14) and state > 0 and user_id = '.$user->id.' and date(created_on) = "'.$convertedDate.'"');
// $times = $db->loadObjectList();
// echo "<pre>";
// print_r($user->dob_redemption_num);
// echo "</pre>";
?>
<div id="app">

  <div class="mt-5 owl-carousel owl-theme profile-overview-slider ">
    <div class="mb-4 mb-lg-0">
      <div class="profile-points font-brand">
        <div class="row align-items-center">
          <div class="col-sm-5 col-md-4 col-lg-5 col-xl-4">
            <div class="perks-status mx-auto">
              <div class="">
                <h3><?php echo number_format($user_total_qperks_point_dob); ?></h3>
                Q-Perks
              </div>
            </div>
          </div>
          <div class="col-sm-7 col-md-8 col-lg-7 col-xl-8">
            <div class="text-area text-center text-sm-left">
              <h4 class="f-24">Hi there <?php echo $user->name; ?>,</h4>
              <p>You have collected a total of <span class="text-highlight bold"><?php echo number_format($user_total_qperks_point); ?></span> Q-Perks. </p>

              <?php if($user->dob_redemption_num == 1){ ?>
              <div class="birthday-alert mr-lg-4">
                <div class="row align-items-center no-gutters">
                  <div class="col-2 text-center">
                    <img class="mx-auto" src="images/club/birthday-icon.png" alt="Birthday">
                  </div>
                  <div class="col-10 pl-2 text-left">
                    Happy Birthday! We are giving you <b>200</b> Q-Perks to celebrate your Birthday. Redeem before it expires at the end of the month.
                  </div>
                </div>
              </div>
              <?php } ?>


            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mb-4 mb-lg-0">
      <!-- <div class="profile-points font-brand">
        <div class="row align-items-center">
          <div class="col-sm-5 col-md-4 col-lg-5 col-xl-4">
            <div class="perks-status daily mx-auto">
              <div class="">
                <h3><?php echo 3-$times[0]->total;?></h3>
                Time(s)
              </div>
            </div>
          </div>
          <div class="col-sm-7 col-md-8 col-lg-7 col-xl-8">
            <div class="text-area text-center text-sm-left">
              <h4 class="f-24">Balance of daily activities</h4>
              <p>Maximum <span class="text-highlight bold">3</span> activities per day  to earn Q-Perks.</p>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>


	<div class="rewards-highlights">
		<h2 class="f-30 text-center mb-4">Rewards highlights</h2>
		<div class="owl-carousel owl-theme slider-nav" id="product-slider">
				<div @click="goToLink(product.id)" v-for="product in products" class="item product-item">
		       <div class="image-holder"
		          :style="{ 'background-image': 'url(../' + product.image1 + ')' }">
              <div v-if="product.promo == 1 && product.promo_label != ''" class="product-promo" >
   							 {{product.promo_label}}
   						</div>

               <div v-if="product.quantity == 0 || product.stock_status == 0" class="product-promo out-stock" >
										OUT OF STOCK
									</div>
            </div>

          <div class="price mt-3">
            <h3 v-if="product.promo == 1" class="product-points">{{numberConvert(product.promo_qperks)}} Q-Perks</h3>
            <h3 class="product-points" :class="{'ori-price': product.promo == 1 }">{{numberConvert(product.point)}} Q-Perks</h3>
          </div>
          <div class="promo-note">
            {{product.promo_description}}
          </div>
					<h3 class="product-name">{{product.name}}</h3>
				</div>
		</div>
		<div class="text-center mt-5 mt-lg-3">
      <a href="quake-club/rewards-catalogue" class="btn btn-pink">
  			View catalogue
  		</a>
    </div>
	</div>

</div>

<script type="text/javascript">
    var itemsObjFromPhp = <?php  echo json_encode($this->products); ?>;

    var app = new Vue({
        el: '#app',
        data: {
			products : itemsObjFromPhp
        },
        mounted: function () {
			this.owlCarousel();
        },
        updated: function () {
        },
        methods: {
			owlCarousel: function(){
				jQuery('.owl-carousel1').owlCarousel({
					loop:true,
					margin:10,
					nav:true,
					responsive:{
						0:{
							items:1
						},
						600:{
							items:3
						},
						1000:{
							items:5
						}
					}
				})
			},
			goToLink :function(link){
                window.location.href = 'rewards-catalogue/qperksproduct/'+link;
      },numberConvert :function(number){
				return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
        }
    })
</script>
