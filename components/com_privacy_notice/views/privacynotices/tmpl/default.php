<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Privacy_notice
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
?>

<div class="terms-content">
	<?php echo $this->items[0]->content; ?>
</div>
