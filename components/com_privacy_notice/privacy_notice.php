<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Privacy_notice
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Privacy_notice', JPATH_COMPONENT);
JLoader::register('Privacy_noticeController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Privacy_notice');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
