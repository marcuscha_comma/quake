<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Replicate_brand
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_replicate_brand');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_replicate_brand'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_REPLICATE_BRAND_FORM_LBL_REPLICATEBRANDLOG_ARTICLE_TITLE'); ?></th>
			<td><?php echo $this->item->article_title; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_REPLICATE_BRAND_FORM_LBL_REPLICATEBRANDLOG_USER_ID'); ?></th>
			<td><?php echo $this->item->user_id; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_REPLICATE_BRAND_FORM_LBL_REPLICATEBRANDLOG_USER_EMAIL'); ?></th>
			<td><?php echo $this->item->user_email; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_REPLICATE_BRAND_FORM_LBL_REPLICATEBRANDLOG_QUESTION'); ?></th>
			<td><?php echo nl2br($this->item->question); ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_REPLICATE_BRAND_FORM_LBL_REPLICATEBRANDLOG_CREATED_AT'); ?></th>
			<td><?php echo $this->item->created_at; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_replicate_brand&task=replicatebrandlog.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_REPLICATE_BRAND_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_replicate_brand.replicatebrandlog.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_REPLICATE_BRAND_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_REPLICATE_BRAND_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_REPLICATE_BRAND_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_replicate_brand&task=replicatebrandlog.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_REPLICATE_BRAND_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>