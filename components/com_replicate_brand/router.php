<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Replicate_brand
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class Replicate_brandRouter
 *
 */
class Replicate_brandRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = Factory::getApplication()->getParams('com_replicate_brand');
		$this->noIDs = (bool) $params->get('sef_ids');
		
		$replicatebrandlogs = new RouterViewConfiguration('replicatebrandlogs');
		$this->registerView($replicatebrandlogs);
			$replicatebrandlog = new RouterViewConfiguration('replicatebrandlog');
			$replicatebrandlog->setKey('id')->setParent($replicatebrandlogs);
			$this->registerView($replicatebrandlog);
			$replicatebrandlogform = new RouterViewConfiguration('replicatebrandlogform');
			$replicatebrandlogform->setKey('id');
			$this->registerView($replicatebrandlogform);

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('Replicate_brandRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('Replicate_brandHelpersReplicate_brand', __DIR__ . '/helpers/replicate_brand.php');
			$this->attachRule(new Replicate_brandRulesLegacy($this));
		}
	}


	
		/**
		 * Method to get the segment(s) for an replicatebrandlog
		 *
		 * @param   string  $id     ID of the replicatebrandlog to retrieve the segments for
		 * @param   array   $query  The request that is built right now
		 *
		 * @return  array|string  The segments of this item
		 */
		public function getReplicatebrandlogSegment($id, $query)
		{
			return array((int) $id => $id);
		}
			/**
			 * Method to get the segment(s) for an replicatebrandlogform
			 *
			 * @param   string  $id     ID of the replicatebrandlogform to retrieve the segments for
			 * @param   array   $query  The request that is built right now
			 *
			 * @return  array|string  The segments of this item
			 */
			public function getReplicatebrandlogformSegment($id, $query)
			{
				return $this->getReplicatebrandlogSegment($id, $query);
			}

	
		/**
		 * Method to get the segment(s) for an replicatebrandlog
		 *
		 * @param   string  $segment  Segment of the replicatebrandlog to retrieve the ID for
		 * @param   array   $query    The request that is parsed right now
		 *
		 * @return  mixed   The id of this item or false
		 */
		public function getReplicatebrandlogId($segment, $query)
		{
			return (int) $segment;
		}
			/**
			 * Method to get the segment(s) for an replicatebrandlogform
			 *
			 * @param   string  $segment  Segment of the replicatebrandlogform to retrieve the ID for
			 * @param   array   $query    The request that is parsed right now
			 *
			 * @return  mixed   The id of this item or false
			 */
			public function getReplicatebrandlogformId($segment, $query)
			{
				return $this->getReplicatebrandlogId($segment, $query);
			}
}
