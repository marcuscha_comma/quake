<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Replicate_brand
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Session\Session;
use \Joomla\CMS\Language\Text;

/**
 * Replicatebrandlog controller class.
 *
 * @since  1.6
 */
class Replicate_brandControllerReplicatebrandlogForm extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit($key = NULL, $urlVar = NULL)
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_replicate_brand.edit.replicatebrandlog.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_replicate_brand.edit.replicatebrandlog.id', $editId);

		// Get the model.
		$model = $this->getModel('ReplicatebrandlogForm', 'Replicate_brandModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_replicate_brand&view=replicatebrandlogform&layout=edit', false));
	}

	public function checkCaptcha()
	{
		$data = Factory::getApplication()->input->get('jform', array(), 'array');
		$form = $model->getForm();
		$data = $model->validate($form, $data);
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		die();
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return void
	 *
	 * @throws Exception
	 * @since  1.6
	 */
	public function save($key = NULL, $urlVar = NULL)
	{
		// Check for request forgeries.
		// Session::checkToken() or jexit(Text::_('JINVALID_TOKEN'));
		// Session::getFormToken(true);

		// Initialise variables.
		$app   = Factory::getApplication();
		$model = $this->getModel('ReplicatebrandlogForm', 'Replicate_brandModel');
		$session = JFactory::getSession();

		// Get the user data.
		$data = Factory::getApplication()->input->get('jform', array(), 'array');

		$session->set('Ruser_name', $data['user_name'] );
		$session->set('Ruser_email', $data['user_email']);
		$session->set('Ruser_contact', $data['user_contact']);
		$session->set('Ruser_designation', $data['user_designation']);
		$session->set('Ruser_company_name', $data['user_company_name']);

		$preUrl = $_SERVER['HTTP_REFERER'];
		$form = $model->getForm();
		// $data = $model->validate($form, $data);
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		die();
		
		if ($data === false) {
			$this->setMessage("Oops! Please try again.");

			$this->setRedirect($preUrl);

			$this->redirect();
		}else{
			$return = $model->save($data);
		}
		// Attempt to save the data.
		

		// Check for errors.
		if ($return === false)
		{
			$this->setMessage("Oops! Please try again.");

			$this->setRedirect($preUrl);

			$this->redirect();
		}

		// Check in the profile.
		if ($return)
		{
			$ip = $_SERVER['REMOTE_ADDR'];
			// Use JSON encoded string and converts
			// it into a PHP variable
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

			$countryList = [
				'China',
				'India',
				'Indonesia',
				'Pakistan',
				'Bangladesh',
				'Japan',
				'Philippines',
				'Vietnam',
				'Turkey',
				'Iran',
				'Thailand',
				'Myanmar',
				'South Korea',
				'Iraq',
				'Afghanistan',
				'Saudi Arabia',
				'Uzbekistan',
				'Malaysia',
				'Yemen',
				'Nepal',
				'North Korea',
				'Sri Lanka',
				'Kazakhstan',
				'Syria',
				'Cambodia',
				'Jordan',
				'Azerbaijan',
				'United Arab Emirates',
				'Tajikistan',
				'Israel',
				'Laos',
				'Lebanon',
				'Kyrgyzstan',
				'Turkmenistan',
				'Singapore',
				'Oman',
				'State of Palestine',
				'Kuwait',
				'Georgia',
				'Mongolia',
				'Armenia',
				'Qatar',
				'Bahrain',
				'Timor-Leste',
				'Cyprus',
				'Bhutan',
				'Maldives',
				'Brunei',
				'Hong Kong',
				'Taiwan'
			];

			$check = 0;
			foreach ($countryList as $key => $value) {
				if ($ipdat->geoplugin_countryName == $value) {
					$check++;
				}
			}
			$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
			$recaptcha_secret = '6Ld86cIZAAAAAHggMzBIMqAKIlusvS7W4OnpyTO8';
			$recaptcha_response = $_POST['g-recaptcha-response'];

			// Make and decode POST request:
			$recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
			$recaptcha = json_decode($recaptcha);

			if ($check && $recaptcha->score >= 0.5) {
				$this->_sendEmail($data);
			}
			if (strpos($preUrl, 'sent') !== false) {
				$this->setRedirect($preUrl);
			}else{
				$this->setRedirect($preUrl."?email=sent");
			}

			// $this->setMessage("Thank You. We will contact you soon!");

			// $this->setRedirect(JRoute::_('index.php?option=com_enquiry&view=enquiry&email=sent', false));

			// $this->setRedirect(JRoute::_('index.php?option=com_enquiry&view=enquiry&email=sent', false));

			$this->redirect();


		}

		// Redirect to the list screen.
		$this->setMessage(Text::_('COM_REPLICATE_BRAND_ITEM_SAVED_SUCCESSFULLY'));
		// $url  = (empty($item->link) ? 'index.php?option=com_replicate_brand&view=replicatebrandlogs' : $item->link);
		// $this->setRedirect(Route::_($url, false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return void
	 *
	 * @throws Exception
	 * @since  1.6
	 */
	public function saveWithAjax()
	{
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="save-replicate-brand.json"');

		// Initialise variables.
		$app   = Factory::getApplication();
		$model = $this->getModel('ReplicatebrandlogForm', 'Replicate_brandModel');

		// Get the user data.
		$data = Factory::getApplication()->input->get('jform', array(), 'array');

		echo "<pre>";
		print_r( $data );
		echo "</pre>";
		die();

		// Attempt to save the data.
		$return = $model->save($data);

		// Check for errors.
		if ($return === false)
		{
		}

		// Check in the profile.
		if ($return)
		{
		}

	}

	/**
	 * Method to abort current operation
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function cancel($key = NULL)
	{
		$app = Factory::getApplication();

		// Get the current edit id.
		$editId = (int) $app->getUserState('com_replicate_brand.edit.replicatebrandlog.id');

		// Get the model.
		$model = $this->getModel('ReplicatebrandlogForm', 'Replicate_brandModel');

		// Check in the item
		if ($editId)
		{
			$model->checkin($editId);
		}

		$menu = Factory::getApplication()->getMenu();
		$item = $menu->getActive();
		$url  = (empty($item->link) ? 'index.php?option=com_replicate_brand&view=replicatebrandlogs' : $item->link);
		$this->setRedirect(Route::_($url, false));
	}

	/**
	 * Method to remove data
	 *
	 * @return void
	 *
	 * @throws Exception
     *
     * @since 1.6
	 */
	public function remove()
    {
        $app   = Factory::getApplication();
        $model = $this->getModel('ReplicatebrandlogForm', 'Replicate_brandModel');
        $pk    = $app->input->getInt('id');

        // Attempt to save the data
        try
        {
            $return = $model->delete($pk);

            // Check in the profile
            $model->checkin($return);

            // Clear the profile id from the session.
            $app->setUserState('com_replicate_brand.edit.replicatebrandlog.id', null);

            $menu = $app->getMenu();
            $item = $menu->getActive();
            $url = (empty($item->link) ? 'index.php?option=com_replicate_brand&view=replicatebrandlogs' : $item->link);

            // Redirect to the list screen
            $this->setMessage(Text::_('COM_REPLICATE_BRAND_ITEM_DELETED_SUCCESSFULLY'));
            $this->setRedirect(Route::_($url, false));

            // Flush the data from the session.
            $app->setUserState('com_replicate_brand.edit.replicatebrandlog.data', null);
        }
        catch (Exception $e)
        {
            $errorType = ($e->getCode() == '404') ? 'error' : 'warning';
            $this->setMessage($e->getMessage(), $errorType);
            $this->setRedirect('index.php?option=com_replicate_brand&view=replicatebrandlogs');
        }
	}
	
	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   array     $data                  The data to send in the email.
	 * @param   stdClass  $contact               The user information to send the email to
	 * @param   boolean   $copy_email_activated  True to send a copy of the email to the user.
	 *
	 * @return  boolean  True on success sending the email, false on failure.
	 *
	 * @since   1.6.4
	 */
	private function _sendEmail($dataArray)
	{

		$config = JFactory::getConfig();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');

		$jinput = JFactory::getApplication()->input;

		$mlogo = '<img width="50" src="https://quake.com.my/images/logo.png" alt="quake">';
		$header = '<p>Hi Quake team,</>';
		$header2 = '<p>This campaign has piqued my interest and I would like to learn more about how we can create a great campaign together.</>';
		$tableHtmlStart = "<table>";
		$mArticleTitle = '<tr><td>Article Title:</td><td>'.$dataArray['article_title'].'</td>';
		$mBrandName = '<tr><td>Brand Name:</td><td>'.$dataArray['brand_name'].'</td>';
		$mProductCategory = '<tr><td>Product Category:</td><td>'.$dataArray['product_category'].'</td>';
		$mOpinion = '<tr><td>Objective:</td><td>'.$dataArray['opinion'].'</td>';
		$mName = '<tr><td>FULL NAME:</td><td>'.$dataArray['user_name'].'</td>';
		$mEmail = '<tr><td>EMAIL:</td><td>'.$dataArray['user_email'].'</td>';
		$mContact = '<tr><td>CONTACT:</td><td>'.$dataArray['user_contact'].'</td>';
		$mDesignation = '<tr><td>DESINATION:</td><td>'.$dataArray['user_designation'].'</td>';
		$mCname = '<tr><td>COMPANY NAME:</td><td>'.$dataArray['user_company_name'].'</td>';
		$tableHtmlEnd = "</table>";
		$body = $mlogo . $header . $header2 . $tableHtmlStart . $mArticleTitle . $mBrandName . $mProductCategory . $mOpinion  . $mName . $mEmail . $mContact . $mDesignation . $mCname .$tableHtmlEnd;

		// $to = array("sharifah_idid@astro.com.my", "sok-kwan_lim@astro.com.my");
		// $to = array("samuel.tan@comma.com.my");
		// $to = array("mediasolutions@astro.com.my");
		$to = array("midoff1@gmail.com");
		// $from = array("midoff1@gmail.com", "Quake");
		$from = array($data[ 'mailfrom' ], "Quake");



		# Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject("Want to know more about this campaign?");
		$mailer->setBody($body);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();

		return $mailer->send();
	}
}
