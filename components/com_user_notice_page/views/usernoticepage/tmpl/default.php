<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_User_notice_page
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>
<?php echo nl2br($this->item->content); ?>
<div class="mt-5">
	<a class="btn btn-pink mr-3 mb-3" href="./login">
		Back to Quake home page
	</a>
	<a class="btn btn-pink mb-3" href="./registration">
		Agreed, proceed to sign up
	</a>
</div>
