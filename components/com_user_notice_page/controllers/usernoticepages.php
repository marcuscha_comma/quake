<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_User_notice_page
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Usernoticepages list controller class.
 *
 * @since  1.6
 */
class User_notice_pageControllerUsernoticepages extends User_notice_pageController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Usernoticepages', $prefix = 'User_notice_pageModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
