<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_User_notice_page
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('User_notice_page', JPATH_COMPONENT);
JLoader::register('User_notice_pageController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('User_notice_page');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
