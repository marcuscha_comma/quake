<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Sliders_highlight
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Sliders_highlight', JPATH_COMPONENT);
JLoader::register('Sliders_highlightController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Sliders_highlight');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
