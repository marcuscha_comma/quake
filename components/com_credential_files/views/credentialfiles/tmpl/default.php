<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Programme_scheduler
 * @author     ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user        = JFactory::getUser();
$userId      = $user->get('id');
$checkEmailUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.checkEmailExist');
$createClientUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.createClient');
// echo $downloadUrl;
?>


<div id="app">
  <div class="tab-pills scrollable-tab">
    <div class="row">
      <div class="col"><a href="./credentials" class="tab-pill active">Credentials</a></div>
      <div class="col"><a href="./channel-profile" class="tab-pill">Brand Profiles</a></div>
      <div class="col"><a href="./rate-card" class="tab-pill">Rate Cards</a></div>
      <div class="col"><a href="./programme-schedules" class="tab-pill">TV Programme Schedules</a></div>
      <div class="col"><a href="./packages" class="tab-pill">Advertising Packages</a></div>
    </div>
   </div>

	<div>
		<h2 class="section-title">Credentials</h2>
		<p>Download our latest quarterly performance here. </p>
	</div>

  <div class="row mt-4">
    <div v-for="item in itemsArray" class="col-12 col-sm-6 download-center-listing">
      <div class="row no-gutters">
        <div class="col-md-4 col-sm-12 col-4">
          <div class="bg-pink icon-holder">
            <img src="./images/astro-credential.png" alt="" class="">
          </div>
        </div>
        <div class="col-md-8 col-sm-12 col-8">
          <div class="download-center-div">
            <div class="download-center-date">{{item.period}}</div>
            <h5>{{item.title}}</h5>

            <div class="download-holder">
              <button v-if="loginStatus != 1" type="button" class="btn btn-text download-center-button" data-toggle="modal" data-target="#downloadCenterModal">
                Download
              </button>
              <button v-if="loginStatus == 1" type="button" class="btn btn-text download-center-button" @click="downloadUrl(item.attach_file)">
                <span v-if="item.source_id != 0"><i class="far fa-check-circle"></i> Downloaded</span>
                <span v-if="item.source_id == 0">Download</span>    
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!--
    Overlay CSS design
    @version 1.2
    @author Toures Tiu <toures.tiu@comma.com.my>
    @created at 21/8/2018
    @updated at 26/9/2018 by SyuQian
  -->
  <!-- Modal Start -->
  <!-- <div class="modal fade" id="downloadCenterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" hidden="true"></div>

        <div class="modal-body">
          <div>
            <button type="button" class="close font-brand" data-dismiss="modal" aria-label="Close">
              <span>Close</span>
            </button>

            <div style="clear: both;"></div>
          </div>

          <form action="" id="modal-form" @submit.prevent="checkEmail()">
            <h4 class="form-title">Key In Your Email Address To Start Downloading</h5>

            <div class="field-placey form-user-email">
              <input type="email" name="email1" required v-model="userEmail" placeholder="example@email.com" class="text-center" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                :disabled="clientDataCreateStatus">
              <label for="email1">Email Address</label>
            </div>
            <button class="btn-white btn btn-block" :class="{'disabled':firstRegisterUser}" type="submit" :disabled="firstRegisterUser" >Download Now</button>

            <div class="form-register" v-if="firstRegisterUser">
              <p class="form-register-warning form-text">Opps! Your Email address is not in our system.</p>

              <div class="form-white">
                <h4 class="form-white-title">Hi! Seems like it is your first time here.</h4>

                <h6 class="form-white-subtitle">We need your personal details as per below:</h6>

                <div class="field-placey form-user-email">
                  <input class="form-control" type="text" v-model="userName" name="name" placeholder=" " required :disabled="clientDataCreateStatus">
                  <label for="name">Name *</label>
                </div>

                <div class="field-placey form-user-email">
                  <input class="form-control" type="text" v-model="userPhone" name="mobile" placeholder=" " required :disabled="clientDataCreateStatus">
                  <label for="mobile">Mobile No. *</label>
                </div>

                <div class="field-placey form-user-email">
                  <input class="form-control" type="text" v-model="userCompany" name="company" placeholder=" " required :disabled="clientDataCreateStatus">
                  <label for="company">Company *</label>
                </div>

                <div class="field-placey form-user-email">
                  <input class="form-control" type="text" v-model="userDesignation" name="designation" placeholder=" " required :disabled="clientDataCreateStatus">
                  <label for="designation">Designation *</label>
                </div>
                <button class="btn btn-block btn-pink " type="submit">Download Now</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div> -->
  <div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" hidden="true"></div>

			<div class="modal-body">
				<div>
					<button type="button" class="close font-brand" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">Close</span>
					</button>

					<div style="clear: both;"></div>
				</div>
				<div class="text-center">
					<h3 class="text-white">Login to Download</h3>
					<p class="text-white mb-4">Please login or signup download this file.</p>
          <a href="./login" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
					<!-- <a href="#" data-dismiss="modal" class="btn btn-white">Skip</a> -->
				</div>
			</div>
		</div>
	</div>
</div>
  <!-- Modal End -->
</div>



<script type="text/javascript">
    var itemsObjFromPhp = <?php echo json_encode($this->items); ?>;
    var checkEmailUrl = <?php echo json_encode($checkEmailUrl); ?>;
    var createClientUrl = <?php echo json_encode($createClientUrl); ?>;
	  var userIdFromObject = <?php echo json_encode($userId); ?>;
    var updateUserShareArticlePointUrl = <?php echo json_encode($updateUserShareArticlePointUrl); ?>;

    var app = new Vue({
        el: '#app',
        data: {
          itemsArray: itemsObjFromPhp,
          urlLink : "",
          firstRegisterUser: false,
          userEmail: "",
          userName: "",
          userPhone: "",
          userCompany: "",
          userDesignation: "",
          clientDataCreateStatus: false,
          item : userIdFromObject,
          loginStatus : 0
        },
        mounted: function () {
          if (parseInt(this.item) > 0) {
            this.loginStatus = 1;
          }
          //sort Array by ordering
          this.itemsArray.sort(function (a, b) {
            return a.ordering - b.ordering;
          });
        },
        updated: function () {
          jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
            goToLink :function(link){
              window.location.href = link;
            },
            checkEmail : function(){
              _this = this;

              jQuery.ajax({
                url: checkEmailUrl,
                type: 'post',
                data: { email: _this.userEmail },
                success: function (result) {

                  if (result == 1) {
                    window.location.href = _this.urlLink;
                  }else{
                    if (_this.firstRegisterUser) {
                      jQuery.ajax({
                      url: createClientUrl,
                      type: 'post',
                      data: {
                        email: _this.userEmail,
                        name: _this.userName,
                        phone: _this.userPhone,
                        company: _this.userCompany,
                        designation: _this.userDesignation
                      },
                      success: function (result) {
                          window.location.href = _this.urlLink;
                          _this.clientDataCreateStatus = true;
                      },
                      error: function () {
                        console.log('fail');
                      }
                      });
                    }
                    _this.firstRegisterUser = true;
                  }

                },
                error: function () {
                  console.log('fail');
                }
              });
            },
            downloadUrl :function(urlLink){
              this.urlLink = './uploads/credentials/'+ urlLink;
              window.open(this.urlLink);

              jQuery.ajax({
                url: updateUserShareArticlePointUrl,
                type: 'post',
                data: {'source':urlLink},
                success: function (result) {
                  console.log("success");

                },
                error: function () {
                  console.log('fail');
                }
              });
              // this.userEmail = "";
              // this.userName = "";
              // this.userPhone = "";
              // this.userCompany = "";
              // this.userDesignation = "";
            }
        }
    })
</script>
