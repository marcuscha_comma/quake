<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Banner_v1ewership
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Banner_v1ewership', JPATH_COMPONENT);
JLoader::register('Banner_v1ewershipController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Banner_v1ewership');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
