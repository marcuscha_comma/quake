<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Channel_profile_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_CHANNEL_PROFILE_FILES_FORM_LBL_CHANNELPROFILEFILE_TITLE'); ?></th>
			<td><?php echo $this->item->title; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_CHANNEL_PROFILE_FILES_FORM_LBL_CHANNELPROFILEFILE_PERIOD'); ?></th>
			<td><?php echo $this->item->period; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_CHANNEL_PROFILE_FILES_FORM_LBL_CHANNELPROFILEFILE_ATTACH_FILE'); ?></th>
			<td>
			<?php
			foreach ((array) $this->item->attach_file as $singleFile) : 
				if (!is_array($singleFile)) : 
					$uploadPath = 'uploads/channel-profile' . DIRECTORY_SEPARATOR . $singleFile;
					 echo '<a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank">' . $singleFile . '</a> ';
				endif;
			endforeach;
		?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_CHANNEL_PROFILE_FILES_FORM_LBL_CHANNELPROFILEFILE_TYPE'); ?></th>
			<td><?php echo $this->item->type; ?></td>
		</tr>

	</table>

</div>

