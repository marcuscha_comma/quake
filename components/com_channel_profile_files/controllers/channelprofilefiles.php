<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Channel_profile_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Channelprofilefiles list controller class.
 *
 * @since  1.6
 */
class Channel_profile_filesControllerChannelprofilefiles extends Channel_profile_filesController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Channelprofilefiles', $prefix = 'Channel_profile_filesModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
