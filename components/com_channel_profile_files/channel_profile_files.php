<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Channel_profile_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Channel_profile_files', JPATH_COMPONENT);
JLoader::register('Channel_profile_filesController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Channel_profile_files');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
