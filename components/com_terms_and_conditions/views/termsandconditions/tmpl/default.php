<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Terms_and_conditions
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
?>

<div id="app">
  <div class="tab-pills scrollable-tab">
		<div class="row">
			<div class="col"><a class="tab-pill" @click="tabOnCLick('tv')" :class="{ active : tvTabIsShow }">TV</a></div>
			<div class="col"><a class="tab-pill" @click="tabOnCLick('radio')" :class="{ active : radioTabIsShow }">Radio</a></div>
			<div class="col"><a class="tab-pill" @click="tabOnCLick('digital')" :class="{ active : digitalTabIsShow }">Digital</a></div>
		</div>
  </div>

  <div class="tnc-content">
    <div class="terms-content" v-if="tvTabIsShow">
      <?php echo $this->items[0]->tv_content; ?>
    </div>
  
    <div class="terms-content" v-if="radioTabIsShow">
      <?php echo $this->items[0]->radio_content; ?>
    </div>
  
    <div class="terms-content" v-if="digitalTabIsShow">
      <?php echo $this->items[0]->digital_content; ?>
    </div>

  </div>

</div>
 
<script type="text/javascript">

    var app = new Vue({
      el: '#app',
      data: {
          tvTabIsShow: true,
          radioTabIsShow: false,
          digitalTabIsShow: false
      },
      mounted: function () {
      },
      methods: {
        tabOnCLick: function(tab){
          switch (tab) {
            case "tv":
              this.tvTabIsShow= true;
              this.radioTabIsShow= false;
              this.digitalTabIsShow= false;
              break;
            case "radio":
              this.tvTabIsShow= false;
              this.radioTabIsShow= true;
              this.digitalTabIsShow= false;
              break;
            case "digital":
              this.tvTabIsShow= false;
              this.radioTabIsShow= false;
              this.digitalTabIsShow= true;
              break;
          
            default:
              break;
          }
        }
      }
    })
</script>


