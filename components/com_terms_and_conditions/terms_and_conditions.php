<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Terms_and_conditions
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Terms_and_conditions', JPATH_COMPONENT);
JLoader::register('Terms_and_conditionsController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Terms_and_conditions');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
