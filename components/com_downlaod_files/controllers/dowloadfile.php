<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Downlaod_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Dowloadfile controller class.
 *
 * @since  1.6
 */
class Downlaod_filesControllerDowloadfile extends JControllerLegacy
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	public function edit()
	{
		$app = JFactory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_downlaod_files.edit.dowloadfile.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_downlaod_files.edit.dowloadfile.id', $editId);

		// Get the model.
		$model = $this->getModel('Dowloadfile', 'Downlaod_filesModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_downlaod_files&view=dowloadfileform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Checking if the user can remove object
		$user = JFactory::getUser();

		if ($user->authorise('core.edit', 'com_downlaod_files') || $user->authorise('core.edit.state', 'com_downlaod_files'))
		{
			$model = $this->getModel('Dowloadfile', 'Downlaod_filesModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(JText::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_downlaod_files.edit.dowloadfile.id', null);

			// Flush the data from the session.
			$app->setUserState('com_downlaod_files.edit.dowloadfile.data', null);

			// Redirect to the list screen.
			$this->setMessage(JText::_('COM_DOWNLAOD_FILES_ITEM_SAVED_SUCCESSFULLY'));
			$menu = JFactory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(JRoute::_('index.php?option=com_downlaod_files&view=dowloadfiles', false));
			}
			else
			{
                $this->setRedirect(JRoute::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Checking if the user can remove object
		$user = JFactory::getUser();

		if ($user->authorise('core.delete', 'com_downlaod_files'))
		{
			$model = $this->getModel('Dowloadfile', 'Downlaod_filesModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(JText::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_downlaod_files.edit.inventory.id', null);
                $app->setUserState('com_downlaod_files.edit.inventory.data', null);

                $app->enqueueMessage(JText::_('COM_DOWNLAOD_FILES_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(JRoute::_('index.php?option=com_downlaod_files&view=dowloadfiles', false));
			}

			// Redirect to the list screen.
			$menu = JFactory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(JRoute::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function createClient(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$jinput = JFactory::getApplication()->input;

		$client = new stdClass();
		$client->email = $jinput->get('email','', 'String');
		$client->name = $jinput->get('name','', 'String');
		$client->phone = $jinput->get('phone','', 'String');
		$client->company = $jinput->get('company','', 'String');
		$client->designation = $jinput->get('designation','', 'String');
		$client->user_type = $jinput->get('user_type','', 'String') != ''?$jinput->get('user_type','', 'String'):0;
		$client->ordering = 1;
		$client->state = 1;
		$client->checked_out = 0;
		$client->created_by = 363;
		$client->modified_by = 363;
		$client->created_time = date("Y-m-d H:i:s");

		//get list from table
		$db    = JFactory::getDBO();
		$query_client_data = $db->getQuery( true );
		$query_client_data
			->select( '*' )
			->from( $db->quoteName( '#__cus_clients_data' ) )
			->where($db->quoteName('email') . ' LIKE '. $db->quote($client->email));
		$db->setQuery( $query_client_data );
		$db->execute();
		$clientDataList = $db->getNumRows();
		
		if ($clientDataList > 0) {
			echo 1;
		}else{
			$result = JFactory::getDbo()->insertObject('#__cus_clients_data', $client);
			echo 0;
		}
		JFactory::getApplication()->close(); 
	}

	public function checkEmailExist(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    	JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$jinput = JFactory::getApplication()->input;
		$email = $jinput->get('email','', 'String');

		//get list from table
		$db    = JFactory::getDBO();
		$query_client_data = $db->getQuery( true );
		$query_client_data
			->select( '*' )
			->from( $db->quoteName( '#__cus_clients_data' ) )
			->where($db->quoteName('email') . ' LIKE '. $db->quote($email));
		$db->setQuery( $query_client_data );
		$db->execute();
		$clientDataList = $db->getNumRows();
		
		if ($clientDataList > 0) {
			echo 1;
		}else{
			echo 0;
		}

		JFactory::getApplication()->close(); 
	}
}
