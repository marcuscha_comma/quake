<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Downlaod_files
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Downlaod_files', JPATH_COMPONENT);
JLoader::register('Downlaod_filesController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Downlaod_files');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
