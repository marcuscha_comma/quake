<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_contact_us
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
$user = JFactory::getUser();

?>

<div>

	<div class="pink-page-title">
   <div class="container">
     <h2>Contact Us</h2>
   </div>
  </div>

	<form action="<?php echo JRoute::_('index.php?option=com_quake_club_contact_us&task=contactus.sendEnquiry'); ?>" method="post">
		<h3 class="f-30 mt-5">Need help with something?</h3>
		<p class="mt-3 font-brand">Just fill up your details below and we'll get back to you as soon as we can. Have a great day!</p>

		<div class="profile-form readonly-form mt-5">
			<div class="mb-3 row">
				<label class="col-sm-3 col-xl-2 col-form-label" for="name">Your name</label>
				<div class="col-sm-9">
					<input readonly="readonly" name="jform[name]" value="<?php echo $user->name;?>" class="form-control form-control-plaintext" autocomplete="off">
				</div>
			</div>
			<div class="mb-3 row">
				<label class="col-sm-3 col-xl-2 col-form-label" for="name">Mobile no.</label>
				<div class="col-sm-9">
					<input id="mobile" class="required form-control form-control-plaintext" name="jform[mobile]"
					value="<?php echo $user->mobileNo;?>"
						type="text" placeholder=" " readonly aria-required="true" autocomplete="off">
				</div>
			</div>
			<div class="mb-3 row">
				<label class="col-sm-3 col-xl-2 col-form-label" for="name">Email address</label>
				<div class="col-sm-9">
					<input id="email" class="required form-control form-control-plaintext" name="jform[email]"
						value="<?php echo $user->email;?>"
						type="text" placeholder=" " readonly aria-required="true" autocomplete="off">
				</div>
			</div>
		</div>

		<div class="profile-form edit-form">

			<div class="mb-3 row">
				<label class="col-sm-3 col-xl-2 col-form-label" for="name">Enquiry title</label>
				<div class="col-sm-9 col-xl-6">
					<input id="subject" class="required form-control" name="jform[subject]" value="" type="text"
						placeholder=" " aria-required="true" autocomplete="off" required>
				</div>
			</div>
			<div class="mb-3 row">
				<label class="col-sm-3 col-xl-2 col-form-label" for="name">Enquiry message</label>
				<div class="col-sm-9 col-xl-6">
					<textarea id="content" required class="required form-control" name="jform[content]" value=""
						aria-required="true"rows="6"></textarea>
				</div>
			</div>

			<div class="mb-3 row justify-content-end">
				<div class="col-sm-9 col-xl-10">
					<button type="submit" class="btn btn-pink btn-wide">Submit</button>
				</div>
			</div>
		</div>
	</form>
</div>
