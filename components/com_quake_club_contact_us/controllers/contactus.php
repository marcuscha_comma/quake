<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_contact_us
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

/**
 * Contactus controller class.
 *
 * @since  1.6
 */
class Quake_club_contact_usControllerContactus extends \Joomla\CMS\MVC\Controller\BaseController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit()
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_quake_club_contact_us.edit.contactus.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_quake_club_contact_us.edit.contactus.id', $editId);

		// Get the model.
		$model = $this->getModel('Contactus', 'Quake_club_contact_usModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_quake_club_contact_us&view=contactusform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.edit', 'com_quake_club_contact_us') || $user->authorise('core.edit.state', 'com_quake_club_contact_us'))
		{
			$model = $this->getModel('Contactus', 'Quake_club_contact_usModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_quake_club_contact_us.edit.contactus.id', null);

			// Flush the data from the session.
			$app->setUserState('com_quake_club_contact_us.edit.contactus.data', null);

			// Redirect to the list screen.
			$this->setMessage(Text::_('COM_QUAKE_CLUB_CONTACT_US_ITEM_SAVED_SUCCESSFULLY'));
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(Route::_('index.php?option=com_quake_club_contact_us&view=contactuses', false));
			}
			else
			{
                $this->setRedirect(Route::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.delete', 'com_quake_club_contact_us'))
		{
			$model = $this->getModel('Contactus', 'Quake_club_contact_usModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_quake_club_contact_us.edit.inventory.id', null);
                $app->setUserState('com_quake_club_contact_us.edit.inventory.data', null);

                $app->enqueueMessage(Text::_('COM_QUAKE_CLUB_CONTACT_US_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(Route::_('index.php?option=com_quake_club_contact_us&view=contactuses', false));
			}

			// Redirect to the list screen.
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(Route::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function sendEnquiry(){

		$app    = JFactory::getApplication();
		$user   = JFactory::getUser();
		$userId = (int) $user->get('id');
		$db = JFactory::getDbo();
		$jdate = new JDate;

		$requestData = $app->input->post->get('jform', array(), 'array');

		$sent = $this->_sendEmail($requestData['name'], $requestData['mobile'], $requestData['email'], $requestData['subject'], $requestData['content']);
		if ($sent) {
			$app->enqueueMessage(JText::_('COM_QUAKE_CLUB_CONTACT_US_EMAIL_SEND_SUCCESSFULLY'));
			$app->redirect(JUri::base().'quake-club/contact-us');
		}else {
			$app->enqueueMessage(JText::_('COM_QUAKE_CLUB_CONTACT_US_EMAIL_SEND_FAIL'),"warning");
			$app->redirect(JUri::base().'quake-club/contact-us');
		}
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   array     $data                  The data to send in the email.
	 * @param   stdClass  $contact               The user information to send the email to
	 * @param   boolean   $copy_email_activated  True to send a copy of the email to the user.
	 *
	 * @return  boolean  True on success sending the email, false on failure.
	 *
	 * @since   1.6.4
	 */
	private function _sendEmail($name, $mobile, $userEmail, $subject, $content)
	{
		$config = JFactory::getConfig();
		$user   = JFactory::getUser();
		$data = $user->getProperties();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$emailSubject = "Contact Us.";
		$emailBody = '<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
		
		<head>
		  <!-- NAME: 1 COLUMN -->
		  <!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
		  <meta charset="UTF-8">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <title>Astro Quake Club - Enquiry</title>
		
		  <style type="text/css">
			p {
			  margin: 5px 0;
			  padding: 0;
			}
		
			table {
			  border-collapse: collapse;
			}
		
			h1,
			h2,
			h3,
			h4,
			h5,
			h6 {
			  display: block;
			  margin: 0;
			  padding: 0;
			}
		
			img,
			a img {
			  border: 0;
			  height: auto;
			  outline: none;
			  text-decoration: none;
			}
		
			body,
			#bodyTable,
			#bodyCell {
			  height: 100%;
			  margin: 0;
			  padding: 0;
			  width: 100%;
			}
		
			.mcnPreviewText {
			  display: none !important;
			}
		
			#outlook a {
			  padding: 0;
			}
		
			img {
			  -ms-interpolation-mode: bicubic;
			}
		
			table {
			  mso-table-lspace: 0pt;
			  mso-table-rspace: 0pt;
			}
		
			.ReadMsgBody {
			  width: 100%;
			}
		
			.ExternalClass {
			  width: 100%;
			}
		
			p,
			a,
			li,
			td,
			blockquote {
			  mso-line-height-rule: exactly;
			}
		
			a[href^=tel],
			a[href^=sms] {
			  color: inherit;
			  cursor: default;
			  text-decoration: none;
			}
		
			p,
			a,
			li,
			td,
			body,
			table,
			blockquote {
			  -ms-text-size-adjust: 100%;
			  -webkit-text-size-adjust: 100%;
			}
		
			.ExternalClass,
			.ExternalClass p,
			.ExternalClass td,
			.ExternalClass div,
			.ExternalClass span,
			.ExternalClass font {
			  line-height: 100%;
			}
		
			a[x-apple-data-detectors] {
			  color: inherit !important;
			  text-decoration: none !important;
			  font-size: inherit !important;
			  font-family: inherit !important;
			  font-weight: inherit !important;
			  line-height: inherit !important;
			}
		
			#bodyCell {
			  padding: 20px;
			}
		
			.templateContainer {
			  max-width: 600px !important;
			}
		
			a.mcnButton {
			  display: block;
			}
		
			.mcnImage,
			.mcnRetinaImage {
			  vertical-align: bottom;
			}
		
			.mcnTextContent {
			  word-break: break-word;
			}
		
			.mcnTextContent img {
			  height: auto !important;
			}
		
			.mcnDividerBlock {
			  table-layout: fixed !important;
			}
		
			body,
			#bodyTable {
		
			  background-color: #fafafa;
			}
		
			#bodyCell {
		
			  border-top: 0;
			}
		
			.templateContainer {
		
			  border: 0;
			}
		
			h1 {
		
			  color: #202020;
		
			  font-family: Helvetica;
		
			  font-size: 26px;
		
			  font-style: normal;
		
			  font-weight: bold;
		
			  line-height: 125%;
		
			  letter-spacing: normal;
		
			  text-align: left;
			}
		
			h2 {
		
			  color: #202020;
		
			  font-family: Helvetica;
		
			  font-size: 22px;
		
			  font-style: normal;
		
			  font-weight: bold;
		
			  line-height: 125%;
		
			  letter-spacing: normal;
		
			  text-align: left;
			}
		
			h3 {
		
			  color: #202020;
		
			  font-family: Helvetica;
		
			  font-size: 20px;
		
			  font-style: normal;
		
			  font-weight: bold;
		
			  line-height: 125%;
		
			  letter-spacing: normal;
		
			  text-align: left;
			}
		
			h4 {
		
			  color: #202020;
		
			  font-family: Helvetica;
		
			  font-size: 18px;
		
			  font-style: normal;
		
			  font-weight: bold;
		
			  line-height: 125%;
		
			  letter-spacing: normal;
		
			  text-align: left;
			}
		
			#templatePreheader {
		
			  background-color: #fafafa;
		
			  background-image: none;
		
			  background-repeat: no-repeat;
		
			  background-position: center;
		
			  background-size: cover;
		
			  border-top: 0;
		
			  border-bottom: 0;
		
			  padding-top: 0px;
		
			  padding-bottom: 0px;
			}
			#templatePreheader .mcnTextContent,
			#templatePreheader .mcnTextContent p {
		
			  color: #000000;
		
			  font-family: Helvetica, Arial, sans-serif;
		
			  font-size: 10px;
		
			  line-height: 150%;
		
			  text-align: center;
			}
		
			#templatePreheader .mcnTextContent a,
			#templatePreheader .mcnTextContent p a {
		
			  color: #000000;
		
			  font-weight: normal;
		
			  text-decoration: underline;
			}
			#templateHeader {
		
			  background-color: #ffffff;
		
			  background-image: none;
		
			  background-repeat: no-repeat;
		
			  background-position: center;
		
			  background-size: cover;
		
			  border-top: 0;
		
			  border-bottom: 0;
		
			  padding-top: 0px;
		
			  padding-bottom: 0;
			}
		
			#templateHeader .mcnTextContent,
			#templateHeader .mcnTextContent p {
		
			  color: #202020;
		
			  font-family: Helvetica, Arial, sans-serif;
		
			  font-size: 16px;
		
			  line-height: 150%;
		
			  text-align: left;
			}
		
			#templateHeader .mcnTextContent a,
			#templateHeader .mcnTextContent p a {
		
			  color: #007C89;
		
			  font-weight: normal;
		
			  text-decoration: underline;
			}
		
			#templateBody {
		
			  background-color: #ffffff;
		
			  background-image: none;
		
			  background-repeat: no-repeat;
		
			  background-position: center;
		
			  background-size: cover;
		
			  border-top: 0;
		
			  border-bottom: 2px solid #EAEAEA;
		
			  padding-top: 20px;
		
			  padding-bottom: 20px;
			}
		
			#templateBody .mcnTextContent,
			#templateBody .mcnTextContent p {
		
			  color: #000000;
		
			  font-family: Helvetica, Arial, sans-serif;
		
			  font-size: 16px;
		
			  line-height: 150%;
		
			  text-align: center;
			}
		
			#templateBody .mcnTextContent a,
			#templateBody .mcnTextContent p a {
		
			  color: #000000;
		
			  font-weight: normal;
		
			  text-decoration: none;
			}
		
			#templateFooter {
		
			  background-color: #000000;
		
			  background-image: none;
		
			  background-repeat: no-repeat;
		
			  background-position: center;
		
			  background-size: cover;
		
			  border-top: 0;
		
			  border-bottom: 0;
		
			  padding-top: 0px;
		
			  padding-bottom: 0px;
			}
			#templateFooter .mcnTextContent,
			#templateFooter .mcnTextContent p {
		
			  color: #ffffff;
		
			  font-family: Helvetica, Arial, sans-serif;
		
			  font-size: 9px;
		
			  line-height: 150%;
		
			  text-align: center;
			}
		
			#templateFooter .mcnTextContent a,
			#templateFooter .mcnTextContent p a {
		
			  color: #ffffff;
		
			  font-weight: normal;
		
			  text-decoration: none;
			}
		
			@media only screen and (min-width:768px) {
			  .templateContainer {
				width: 600px !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  body,
			  table,
			  td,
			  p,
			  a,
			  li,
			  blockquote {
				-webkit-text-size-adjust: none !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  body {
				width: 100% !important;
				min-width: 100% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  #bodyCell {
				padding-top: 10px !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnRetinaImage {
				max-width: 100% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnImage {
				width: 100% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnCartContainer,
			  .mcnCaptionTopContent,
			  .mcnRecContentContainer,
			  .mcnCaptionBottomContent,
			  .mcnTextContentContainer,
			  .mcnBoxedTextContentContainer,
			  .mcnImageGroupContentContainer,
			  .mcnCaptionLeftTextContentContainer,
			  .mcnCaptionRightTextContentContainer,
			  .mcnCaptionLeftImageContentContainer,
			  .mcnCaptionRightImageContentContainer,
			  .mcnImageCardLeftTextContentContainer,
			  .mcnImageCardRightTextContentContainer,
			  .mcnImageCardLeftImageContentContainer,
			  .mcnImageCardRightImageContentContainer {
				max-width: 100% !important;
				width: 100% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnBoxedTextContentContainer {
				min-width: 100% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnImageGroupContent {
				padding: 9px !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnCaptionLeftContentOuter .mcnTextContent,
			  .mcnCaptionRightContentOuter .mcnTextContent {
				padding-top: 9px !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnImageCardTopImageContent,
			  .mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,
			  .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
				padding-top: 18px !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnImageCardBottomImageContent {
				padding-bottom: 9px !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockInner {
				padding-top: 0 !important;
				padding-bottom: 0 !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockOuter {
				padding-top: 9px !important;
				padding-bottom: 9px !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnTextContent,
			  .mcnBoxedTextContentColumn {
				padding-right: 18px !important;
				padding-left: 18px !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnImageCardLeftImageContent,
			  .mcnImageCardRightImageContent {
				padding-right: 18px !important;
				padding-bottom: 0 !important;
				padding-left: 18px !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcpreview-image-uploader {
				display: none !important;
				width: 100% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  h1 {
		
				font-size: 22px !important;
		
				line-height: 125% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  h2 {
		
				font-size: 20px !important;
		
				line-height: 125% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  h3 {
		
				font-size: 18px !important;
		
				line-height: 125% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  h4 {
		
				font-size: 16px !important;
		
				line-height: 150% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  .mcnBoxedTextContentContainer .mcnTextContent,
			  .mcnBoxedTextContentContainer .mcnTextContent p {
		
				font-size: 14px !important;
		
				line-height: 150% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  #templatePreheader {
		
				display: block !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  #templatePreheader .mcnTextContent,
			  #templatePreheader .mcnTextContent p {
		
				font-size: 10px !important;
		
				line-height: 150% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  #templateHeader .mcnTextContent,
			  #templateHeader .mcnTextContent p {
		
				font-size: 16px !important;
		
				line-height: 150% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  #templateBody .mcnTextContent,
			  #templateBody .mcnTextContent p {
		
				font-size: 14px !important;
		
				line-height: 150% !important;
			  }
		
			}
		
			@media only screen and (max-width: 480px) {
			  #templateFooter .mcnTextContent,
			  #templateFooter .mcnTextContent p {
		
				font-size: 9px !important;
		
				line-height: 150% !important;
			  }
		
			}
		  </style>
		</head>
		
		<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fafafa;">
		  <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">You&#39;ve received an enquiry on Astro Quake Club</span>
		  <!--<![endif]-->
		  <center>
			<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #fafafa;">
			  <tr>
				<td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 20px;width: 100%;border-top: 0;">
				  <p style="margin: 20px 0 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: left;">Hi,</p>
		
				  <p style="margin: 5px 0 20px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: left;">You&#39;ve received an enquiry from a member on <b style="color: #EC008C;">Astro Quake Club</b>.</p>
		
		
				  <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border: 1px solid #cccccc;text-align: left;font-size: 12px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; font-family: Helvetica, Arial, sans-serif;" width="100%">
					<tbody>
					  <tr>
						<td width="120" valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <b>Date / Time</b>
						</td>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  '.date("d-m-Y").'
						</td>
					  </tr>
					  <tr>
						<td width="120" valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <b>Name</b>
						</td>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  '.$name.'
						</td>
					  </tr>
					  <tr>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <b>Mobile No.</b>
						</td>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  '.$mobile.'
						</td>
					  </tr>
					  <tr>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <b>Email Address</b>
						</td>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  '.$userEmail.'
						</td>
					  </tr>
					  <tr>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <b>Enquiry Title</b>
						</td>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  '.$subject.'
						</td>
					  </tr>
					  <tr>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <b>Enquiry Message</b>
						</td>
						<td valign="top" style="padding: 10px;border: 1px solid #cccccc;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						 '.$content.'
						</td>
					  </tr>
		
					</tbody>
				  </table>
		
		
				</td>
			  </tr>
			</table>
		  </center>
		</body>
		
		</html>
		';

		// $to = $data['email'];
		$to = array("quakeclub19@gmail.com","quakeclub@astro.com.my");
		$from = array($data['mailfrom'], $data['fromname']);

		// # Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject($emailSubject);
		$mailer->setBody($emailBody);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();
		$mailer->Encoding = 'base64';
		// $mailer->AddEmbeddedImage( './images/Cockoo.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
		return $mailer->send();
	}
}
