<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_event_cart_complete
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Eventcartcompletes list controller class.
 *
 * @since  1.6
 */
class Quake_club_event_cart_completeControllerEventcartcompletes extends Quake_club_event_cart_completeController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Eventcartcompletes', $prefix = 'Quake_club_event_cart_completeModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
