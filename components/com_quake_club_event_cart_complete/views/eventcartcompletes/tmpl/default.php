<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_cart_complete
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>
<div id="app" class="cny-rat">
	<div class="row justify-content-center align-items-end">
		<div class="col-lg-4 order-2 order-lg-first col-7 pt-5 pt-md-0">
			<img class="cny-complete1" src="./images/cny2020/complete-1.png" />
		</div>
		<div class="col-lg-4 col-md-8 cart-complete-content">
			<img width="108" src="./images/club/quake-club-white.png" alt="Quake Club" />
			<h2>Thank you for<br />your redemption.</h2>
			<p>Please check your email for confirmation.</p>

			<div class="row">
				<div class="col-md-8">
					<a href="./prosperatty" class="btn btn-white btn-block mt-4">Back to home page</a>
				</div>
			</div>
		</div>
		<div class="col-lg-2 order-last col-5 text-right">
			<img class="cny-complete2" src="./images/cny2020/complete-2.png" />
		</div>
	</div>
</div>
