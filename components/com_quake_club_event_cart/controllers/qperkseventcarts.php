<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_event_cart
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Qperkseventcarts list controller class.
 *
 * @since  1.6
 */
class Quake_club_event_cartControllerQperkseventcarts extends Quake_club_event_cartController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Qperkseventcarts', $prefix = 'Quake_club_event_cartModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	public function removeCart(){

		$app    = JFactory::getApplication();
		$user   = JFactory::getUser();
		$userId = (int) $user->get('id');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$requestData = $app->input->post->get('jform', array(), 'array');

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT quantity FROM #__cus_qperks_event_cart where id = '. $requestData['cart_id'] );
		$cart_quantity            = $db->loadResult();

		$conditions = array(
			$db->quoteName('id') . ' = '. $requestData['cart_id']
		);

		$query->delete($db->quoteName('#__cus_qperks_event_cart'));
		$query->where($conditions);

		$db->setQuery($query);

		$result = $db->execute();

		if ($result) {

			$db->setQuery('SELECT quantity FROM #__cus_qperks_event_products where id = '. $requestData['product_id']);
			$product_quantity             = $db->loadResult();

			$product = new stdClass();
			$product->quantity = (int)$product_quantity + (int)$cart_quantity;
			$product->id = $requestData['product_id'];

			// Insert the object into the user profile table.
			$resultUpdate = JFactory::getDbo()->updateObject('#__cus_qperks_event_products', $product, 'id');

			$app->enqueueMessage(JText::_('COM_USERS_CART_REMOVE_SUCCESS'));
			$app->redirect(JUri::base().'event-cart');
		}else{
			$app->enqueueMessage(JText::_('COM_USERS_CART_REMOVE_FAILED'));
			$app->redirect(JUri::base().'event-cart');
		}
	}

	public function confirmCart(){

		$app    = JFactory::getApplication();
		$user   = JFactory::getUser();
		$userId = (int) $user->get('id');
		$db = JFactory::getDbo();
		$jdate = new JDate;

		$requestData = $app->input->post->get('jform', array(), 'array');
		$requestItem = $app->input->post->get('item', array(), 'array');

		//Save Image
		if (($_FILES['business_card']['name']!="")){

			// Where the file is going to be stored
			$target_dir = "./images/business-cards";
			$file = $_FILES['business_card']['name'];
			$path = pathinfo($file);
			$filename = '/'.time();
			$ext = 'png';
			$temp_name = $_FILES['business_card']['tmp_name'];
			$path_filename_ext = $target_dir.$filename.".".$ext;

			// Check if file already exists
			if (file_exists($path_filename_ext)) {
				// echo "Sorry, file already exists.";
			}else{
				move_uploaded_file($temp_name,$path_filename_ext);
				// echo "Congratulations! File Uploaded Successfully.";
				$company = new stdClass();
				$company->user_id = $userId;
				$company->business_card = $path_filename_ext;
				$result = JFactory::getDbo()->updateObject('#__cus_qperks_company_user', $company, 'user_id');

			}
		}
		// die();

			if ($requestData['total-point'] >= 0) {

				//insert data into redemption table
				$query = $db->getQuery(true);
				$db->setQuery('SELECT MAX(id) FROM #__cus_quake_club_redemption');
				$redemption_id             = $db->loadResult();

				$redemption_id = sprintf("%07d",($redemption_id+ 1));

				for ($i=0; $i < count($requestItem['id']); $i++) {
					$db->setQuery('SELECT MAX(ordering) FROM #__cus_quake_club_redemption');
					$max             = $db->loadResult();

					$redemption = new stdClass();
					$redemption->user_id = $user->id;
					$redemption->product_id=$requestItem['id'][$i];
					$redemption->quantity=$requestItem['quantity'][$i];
					$redemption->point=$requestItem['point'][$i];
					$redemption->promo=$requestItem['promo'][$i];
					$redemption->promo_qperks=$requestItem['promo_qperks'][$i];
					$redemption->order_number=$redemption_id;
					$redemption->delivery_address=$requestData['address'];
					$redemption->recipient_name=$requestData['recipient_name'];
					$redemption->status=1;
					$redemption->state=1;
					$redemption->created_by=$user->id;
					$redemption->created_on=$jdate->toSql(true);
					$redemption->modified_by=$user->id;
					$redemption->ordering=$max + 1;
					$redemption->browser_type=JBrowser::getInstance()->getBrowser();
					$redemption->browser_ver=JBrowser::getInstance()->getVersion();
					$redemption->platform=JBrowser::getInstance()->getAgentString();
					$redemption->event_type= "2020 New Year Event";
					$resultInsertRedemption = JFactory::getDbo()->insertObject('#__cus_quake_club_redemption', $redemption);

					// if ($resultInsertRedemption) {
						//remove carts
					$query = $db->getQuery(true);
					$conditions = array(
						$db->quoteName('user_id') . ' = '.$user->id,
					);
					$query->delete($db->quoteName('#__cus_qperks_event_cart'));
					$query->where($conditions);
					$db->setQuery($query);
					$result = $db->execute();

					$db->setQuery('SELECT qcoins_after_redemption FROM #__cus_quake_club_game_records where user_id = '. $user->id);
					$record_point            = $db->loadResult();

					$records = new stdClass();
					$records->qcoins_after_redemption = $record_point - ($requestItem['promo'][$i] != 1?($requestItem['point'][$i] * $requestItem['quantity'][$i]):($requestItem['promo_qperks'][$i] * $requestItem['quantity'][$i]));
					$records->user_id = $user->id;

					// Insert the object into the user profile table.
					$resultInsertMonth = JFactory::getDbo()->updateObject('#__cus_quake_club_game_records', $records, 'user_id');
					// }
				}

				if ($resultInsertRedemption) {
					$sent = $this->_sendEmail($requestData, $requestItem, $redemption_id);
					$app->redirect(JUri::base().'event-cart-complete');
				}else{
					$app->enqueueMessage(JText::_('COM_USERS_CART_COMPLETE_FAILED'));
					$app->redirect(JUri::base().'event-cart');
				}

			}

	}

	public function updateQuantity(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$userId     = $user->get('id');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jdate = new JDate;
		$jinput = JFactory::getApplication()->input;
		$quantity = $jinput->get('quantity','', 'Integer');
		$cart_id = $jinput->get('id','', 'String');
		$product_id = $jinput->get('product_id','', 'String');
		$remark = $jinput->get('remark','', 'String');

		$db->setQuery('SELECT quantity FROM #__cus_qperks_event_cart where id='.$cart_id);
		$cart_quantity             = $db->loadResult();

		$cart = new stdClass();
		$cart->quantity = $quantity;
		$cart->remark = $remark;
		$cart->id = $cart_id;

		// Insert the object into the user profile table.
		$result = JFactory::getDbo()->updateObject('#__cus_qperks_event_cart', $cart, 'id');


		$calQuantity = ((int)$cart_quantity - (int)$quantity);

		if ( $calQuantity > 0) {

			$db->setQuery('SELECT quantity FROM #__cus_qperks_event_products where id='.$product_id);
			$product_quantity             = $db->loadResult();
			$product = new stdClass();
			$product->quantity = (int)$product_quantity+(int)$calQuantity;
			$product->id = $product_id;

		}else{

			$db->setQuery('SELECT quantity FROM #__cus_qperks_event_products where id='.$product_id);
			$product_quantity             = $db->loadResult();
			$product = new stdClass();
			$product->quantity = (int)$product_quantity+(int)$calQuantity;
			$product->id = $product_id;

		}
		$resultUpdate = JFactory::getDbo()->updateObject('#__cus_qperks_event_products', $product, 'id');

		if ($result) {
			echo new JResponseJson($result);
		}else{
			echo false;
		}

		JFactory::getApplication()->close();
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   array     $data                  The data to send in the email.
	 * @param   stdClass  $contact               The user information to send the email to
	 * @param   boolean   $copy_email_activated  True to send a copy of the email to the user.
	 *
	 * @return  boolean  True on success sending the email, false on failure.
	 *
	 * @since   1.6.4
	 */
	private function _sendEmail($requestData, $requestItem, $redemption_id)
	{
		$config = JFactory::getConfig();
		$user   = JFactory::getUser();
		$data = $user->getProperties();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$emailSubject = "Congratulations! You have successfully placed an order.";
		$itemStr = "";
		$totalNumber = 0;
		$totalNumberPromo = 0;
		$initialPoint = "";
		$initialPoint = $requestData['point'];

		for ($i=0; $i < count($requestItem['id']); $i++) {
			if ($requestItem['promo'][$i]) {
				$totalNumber = ($requestItem['point'][$i] * $requestItem['quantity'][$i]);
				$totalNumberPromo = ($requestItem['promo_qperks'][$i] * $requestItem['quantity'][$i]);
				$tmp_str = '<td valign="top" style="border-right: 1px solid #000000;padding: 10px;text-align: right;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
				<span style="text-decoration: line-through; color: #999999;margin: 0 5px 0 0;">'.$totalNumber.'</span>'. $totalNumberPromo.'
			  </td>';
			}else{
				$totalNumber = ($requestItem['point'][$i] * $requestItem['quantity'][$i]);
				$tmp_str = '<td valign="top" style="border-right: 1px solid #000000;padding: 10px;text-align: right;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
				'.$totalNumber.'
			  </td>';
			}
			if ($requestItem['remark'][$i] != "") {
				$tmp_remark ='Remarks: '.$requestItem['remark'][$i];
			}else{
				$tmp_remark ="";
			}
			$itemStr .= '<tr>
			<td valign="top" style="padding: 10px;border-right: 1px solid #000000;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
				'.$requestItem['name'][$i].'
				<br /> <em style="font-size: 12px;">'.$tmp_remark.'</em>
			</td>
			<td valign="top" style="padding: 10px;border-right: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
			'.$requestItem['quantity'][$i].'
			</td>
			'.$tmp_str.'
		  </tr>';
		}

		$emailBody = '<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

		<head>
		  <!-- NAME: 1 COLUMN -->
		  <!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
		  <meta charset="UTF-8">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <title>Astro Quake Club - Redemption Summary</title>

		  <style type="text/css">
			p {
			  margin: 5px 0;
			  padding: 0;
			}

			table {
			  border-collapse: collapse;
			}

			h1,
			h2,
			h3,
			h4,
			h5,
			h6 {
			  display: block;
			  margin: 0;
			  padding: 0;
			}

			img,
			a img {
			  border: 0;
			  height: auto;
			  outline: none;
			  text-decoration: none;
			}

			body,
			#bodyTable,
			#bodyCell {
			  height: 100%;
			  margin: 0;
			  padding: 0;
			  width: 100%;
			}

			.mcnPreviewText {
			  display: none !important;
			}

			#outlook a {
			  padding: 0;
			}

			img {
			  -ms-interpolation-mode: bicubic;
			}

			table {
			  mso-table-lspace: 0pt;
			  mso-table-rspace: 0pt;
			}

			.ReadMsgBody {
			  width: 100%;
			}

			.ExternalClass {
			  width: 100%;
			}

			p,
			a,
			li,
			td,
			blockquote {
			  mso-line-height-rule: exactly;
			}

			a[href^=tel],
			a[href^=sms] {
			  color: inherit;
			  cursor: default;
			  text-decoration: none;
			}

			p,
			a,
			li,
			td,
			body,
			table,
			blockquote {
			  -ms-text-size-adjust: 100%;
			  -webkit-text-size-adjust: 100%;
			}

			.ExternalClass,
			.ExternalClass p,
			.ExternalClass td,
			.ExternalClass div,
			.ExternalClass span,
			.ExternalClass font {
			  line-height: 100%;
			}

			a[x-apple-data-detectors] {
			  color: inherit !important;
			  text-decoration: none !important;
			  font-size: inherit !important;
			  font-family: inherit !important;
			  font-weight: inherit !important;
			  line-height: inherit !important;
			}

			#bodyCell {
			  padding: 20px;
			}

			.templateContainer {
			  max-width: 600px !important;
			}

			a.mcnButton {
			  display: block;
			}

			.mcnImage,
			.mcnRetinaImage {
			  vertical-align: bottom;
			}

			.mcnTextContent {
			  word-break: break-word;
			}

			.mcnTextContent img {
			  height: auto !important;
			}

			.mcnDividerBlock {
			  table-layout: fixed !important;
			}



			body,
			#bodyTable {
			  background-color: #fafafa;
			}

			#bodyCell {
			  border-top: 0;
			}

			.templateContainer {
			  border: 0;
			}

			h1 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 26px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h2 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 22px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h3 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 20px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h4 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 18px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			#templatePreheader {

			  background-color: #fafafa;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0px;
			}

			#templatePreheader .mcnTextContent,
			#templatePreheader .mcnTextContent p {

			  color: #000000;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 10px;

			  line-height: 150%;

			  text-align: center;
			}

			#templatePreheader .mcnTextContent a,
			#templatePreheader .mcnTextContent p a {

			  color: #000000;

			  font-weight: normal;

			  text-decoration: underline;
			}

			#templateHeader {

			  background-color: #ffffff;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0;
			}

			#templateHeader .mcnTextContent,
			#templateHeader .mcnTextContent p {

			  color: #202020;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 16px;

			  line-height: 150%;

			  text-align: left;
			}

			#templateHeader .mcnTextContent a,
			#templateHeader .mcnTextContent p a {

			  color: #007C89;

			  font-weight: normal;

			  text-decoration: underline;
			}

			#templateBody {

			  background-color: #ffffff;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 2px solid #EAEAEA;

			  padding-top: 20px;

			  padding-bottom: 20px;
			}

			#templateBody .mcnTextContent,
			#templateBody .mcnTextContent p {

			  color: #000000;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 16px;

			  line-height: 150%;

			  text-align: center;
			}

			#templateBody .mcnTextContent a,
			#templateBody .mcnTextContent p a {

			  color: #000000;

			  font-weight: normal;

			  text-decoration: none;
			}

			#templateFooter {

			  background-color: #000000;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0px;
			}

			#templateFooter .mcnTextContent,
			#templateFooter .mcnTextContent p {

			  color: #ffffff;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 9px;

			  line-height: 150%;

			  text-align: center;
			}

			#templateFooter .mcnTextContent a,
			#templateFooter .mcnTextContent p a {

			  color: #ffffff;

			  font-weight: normal;

			  text-decoration: none;
			}

			@media only screen and (min-width:768px) {
			  .templateContainer {
				width: 600px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  body,
			  table,
			  td,
			  p,
			  a,
			  li,
			  blockquote {
				-webkit-text-size-adjust: none !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  body {
				width: 100% !important;
				min-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #bodyCell {
				padding-top: 10px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnRetinaImage {
				max-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImage {
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnCartContainer,
			  .mcnCaptionTopContent,
			  .mcnRecContentContainer,
			  .mcnCaptionBottomContent,
			  .mcnTextContentContainer,
			  .mcnBoxedTextContentContainer,
			  .mcnImageGroupContentContainer,
			  .mcnCaptionLeftTextContentContainer,
			  .mcnCaptionRightTextContentContainer,
			  .mcnCaptionLeftImageContentContainer,
			  .mcnCaptionRightImageContentContainer,
			  .mcnImageCardLeftTextContentContainer,
			  .mcnImageCardRightTextContentContainer,
			  .mcnImageCardLeftImageContentContainer,
			  .mcnImageCardRightImageContentContainer {
				max-width: 100% !important;
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnBoxedTextContentContainer {
				min-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupContent {
				padding: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnCaptionLeftContentOuter .mcnTextContent,
			  .mcnCaptionRightContentOuter .mcnTextContent {
				padding-top: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardTopImageContent,
			  .mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,
			  .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
				padding-top: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardBottomImageContent {
				padding-bottom: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockInner {
				padding-top: 0 !important;
				padding-bottom: 0 !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockOuter {
				padding-top: 9px !important;
				padding-bottom: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnTextContent,
			  .mcnBoxedTextContentColumn {
				padding-right: 18px !important;
				padding-left: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardLeftImageContent,
			  .mcnImageCardRightImageContent {
				padding-right: 18px !important;
				padding-bottom: 0 !important;
				padding-left: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcpreview-image-uploader {
				display: none !important;
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  h1 {

				font-size: 22px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  h2 {

				font-size: 20px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  h3 {

				font-size: 18px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  h4 {

				font-size: 16px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  .mcnBoxedTextContentContainer .mcnTextContent,
			  .mcnBoxedTextContentContainer .mcnTextContent p {

				font-size: 14px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templatePreheader {

				display: block !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templatePreheader .mcnTextContent,
			  #templatePreheader .mcnTextContent p {

				font-size: 10px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templateHeader .mcnTextContent,
			  #templateHeader .mcnTextContent p {

				font-size: 16px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templateBody .mcnTextContent,
			  #templateBody .mcnTextContent p {

				font-size: 14px !important;

				line-height: 150% !important;
			  }
				#order-table {
	        font-size: 10px !important;
	      }

			}

			@media only screen and (max-width: 480px) {

			  #templateFooter .mcnTextContent,
			  #templateFooter .mcnTextContent p {

				font-size: 9px !important;

				line-height: 150% !important;
			  }

			}
		  </style>
		</head>

		<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fafafa;">
		  <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Thank you for redeeming your exclusive reward.</span>
		  <!--<![endif]-->
		  <center>
			<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #fafafa;">
			  <tr>
				<td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 20px;width: 100%;border-top: 0;">
				  <!-- BEGIN TEMPLATE // -->
				  <!--[if (gte mso 9)|(IE)]>
								<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
								<tr>
								<td align="center" valign="top" width="600" style="width:600px;">
								<![endif]-->
				  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
					<tr>
					  <td valign="top" id="templateHeader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnImageBlockOuter">
							<tr>
							  <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
								<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


										<img align="center" alt="Astro Quake Club" src="http://quake.com.my/images/edm/edm-header.png" width="600" style="max-width: 600px;padding-bottom: 0px;vertical-align: bottom;display: inline !important;border-radius: 0%;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


									  </td>
									</tr>
								  </tbody>
								</table>
							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnImageBlockOuter">
							<tr>
							  <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
								<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


										<img align="center" alt="Thank you" src="http://quake.com.my/images/edm/cny-thankyou.jpg?v=1.1" width="600" style="max-width: 600px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


									  </td>
									</tr>
								  </tbody>
								</table>
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
					<tr>
					  <td valign="top" id="templateBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 2px solid #EAEAEA;padding-top: 20px;padding-bottom: 20px;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">

										<p style="margin: 5px 0 20px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Dear <strong>'.$requestData['name'].'</strong>,</p>

										<p style="margin: 5px 0 20px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Thanks for your redemption!</p>

										<p style="margin: 5px 0 20px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Your request is now in process and we&apos;ll email you once it&apos;s approved. Or you can check for the updates on the Quake Club redemption history.</p>

										<!--<p style="margin: 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">For now, here are your order details:</p>-->

										<p style="margin: 20px 0 20px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">
										  <strong>Order No:</strong> #'.$redemption_id.'<br>
										  <strong>Date of Order:</strong> '.date("d/m/Y").'
										</p>

										<p style="margin: 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Order Summary below.</p>

										<table id="order-table" align="center" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border: 1px solid #000000;text-align: left;font-size: 14px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; font-family: Helvetica, Arial, sans-serif;" width="100%">
										  <tbody>
											<tr>
											  <td valign="top" style="padding: 10px;border: 1px solid #000000;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
												<b>Gift Description</b>
											  </td>
											  <td valign="top" style="padding: 10px;border: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
												<b>Quantity</b>
											  </td>
											  <td valign="top" style="padding: 10px;border: 1px solid #000000;text-align: right;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
												<b>Q-Coins Used</b>
											  </td>
											</tr>
											'.$itemStr.'
											<tr>
											  <td colspan="4" valign="top" style="padding: 10px;border: 1px solid #000000;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;">
												<b>Shipping Address:</b><br>
												'.$requestData['address'].'
											  </td>
											</tr>
										  </tbody>
										</table>

										<p style="margin: 20px 0 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Upon redemption approval, your Q-Coins statement will look like this:</p>

										<div style="margin: 20px 0 20px 0; font-size: 14px;">
										  <div style="display: inline-block; border-right: 1px solid #000000; padding-right: 10px; margin: 0 5px 0 0;font-family: Helvetica, Arial, sans-serif;">
											<b>Initial Q-Coins:</b> '.$initialPoint.'
										  </div>
										  <div style="display: inline-block; border-right: 1px solid #000000;  padding-right: 10px; margin: 0 5px 0 0;font-family: Helvetica, Arial, sans-serif;">
											<b>Total Q-Coins Used:</b> '.$requestData['point-used'].'
										  </div>
										  <div style="display: inline-block;font-family: Helvetica, Arial, sans-serif;">
											<b>Q-Coins Balance:</b> '.$requestData['total-point'].'
										  </div>
										</div>

										<p style="font-size: 12px;margin: 10px 0 0 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;line-height: 150%;text-align: center;">Please use up all your Q-Coins before <b>31 DECEMBER 2019</b>.</p>

										<p style="font-size: 12px;margin: 10px 0 0 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;line-height: 150%;text-align: center;">If you have any questions in relation to this redemption, or the item(s) it relates to,<br> please email us at <a href="mailto:quakeclub@astro.com.my" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-weight: normal;text-decoration: underline;">quakeclub@astro.com.my</a></p>

									  </td>
									</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
					<tr>
					  <td valign="top" id="templateFooter" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #000000;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0px;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="padding-top: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #ffffff;font-family: Helvetica, Arial, sans-serif;font-size: 9px;line-height: 150%;text-align: center;">

										You have received this email because you have confirmed that you would like to receive email communication from Quake Club.<br> We will never share your personal information (such as your email address with any other third
										parties without your consent).<br>
										<br>
										<img height="71" alt="Astro Quake" src="http://quake.com.my/images/edm/footer-quake.png" style="border: 0px;width: 53px;height: 71px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="53">
									  </td>
									</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnFollowBlockOuter">
							<tr>
							  <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowBlockInner">
								<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td align="center" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
										  <tbody>
											<tr>
											  <td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
												<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
												  <tbody>
													<tr>
													  <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														<!--[if mso]>
											<table align="center" border="0" cellspacing="0" cellpadding="0">
											<tr>
											<![endif]-->

														<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->


														<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														  <tbody>
															<tr>
															  <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																  <tbody>
																	<tr>
																	  <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		  <tbody>
																			<tr>

																			  <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																				<a href="https://www.facebook.com/QuakeMY" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-fb.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class="" alt="Follow Astro Quake on Facebook"></a>
																			  </td>


																			</tr>
																		  </tbody>
																		</table>
																	  </td>
																	</tr>
																  </tbody>
																</table>
															  </td>
															</tr>
														  </tbody>
														</table>

														<!--[if mso]>
												</td>
												<![endif]-->

														<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->


														<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														  <tbody>
															<tr>
															  <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																  <tbody>
																	<tr>
																	  <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		  <tbody>
																			<tr>

																			  <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																				<a href="https://api.whatsapp.com/send?phone=60126040968&text=Hi,%20send%20me%20the%20monthly%20issue%20of%20Winning%20Partnership%20Series%20by%20MARKETING" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-whatsapp.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class="" alt="WhatsApp Astro Quake"></a>
																			  </td>


																			</tr>
																		  </tbody>
																		</table>
																	  </td>
																	</tr>
																  </tbody>
																</table>
															  </td>
															</tr>
														  </tbody>
														</table>

														<!--[if mso]>
												</td>
												<![endif]-->

														<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->


														<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														  <tbody>
															<tr>
															  <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																  <tbody>
																	<tr>
																	  <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		  <tbody>
																			<tr>

																			  <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																				<a href="http://quake.com.my/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-web.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class="" alt="Visit Astro Quake Website"></a>
																			  </td>


																			</tr>
																		  </tbody>
																		</table>
																	  </td>
																	</tr>
																  </tbody>
																</table>
															  </td>
															</tr>
														  </tbody>
														</table>

														<!--[if mso]>
												</td>
												<![endif]-->

														<!--[if mso]>
											</tr>
											</table>
											<![endif]-->
													  </td>
													</tr>
												  </tbody>
												</table>
											  </td>
											</tr>
										  </tbody>
										</table>
									  </td>
									</tr>
								  </tbody>
								</table>

							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #ffffff;font-family: Helvetica, Arial, sans-serif;font-size: 9px;line-height: 150%;text-align: center;">

										<!--<span style="font-size:12px"><a href="#" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #ffffff;font-weight: normal;text-decoration: none;">Unsubscribe </a>| <a href="http://quake.com.my/user-profile/profile" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #ffffff;font-weight: normal;text-decoration: none;">Update Profile</a></span><br>-->
										<br> © Astro Quake Club. All Right Reserved
									  </td>
									</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
				  </table>
				  <!--[if (gte mso 9)|(IE)]>
								</td>
								</tr>
								</table>
								<![endif]-->
				  <!-- // END TEMPLATE -->
				</td>
			  </tr>
			</table>
		  </center>
		</body>

		</html>';

		// echo "<pre>";
		// print_r($emailBody);
		// echo "</pre>";
		// die();
		$to = array($data['email'],"quakeclub19@gmail.com","quakeclub@astro.com.my");
		$from = array($data['mailfrom'], $data['fromname']);

		// # Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject($emailSubject);
		$mailer->setBody($emailBody);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();
		$mailer->Encoding = 'base64';
		// $mailer->AddEmbeddedImage( './images/Cockoo.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
		return $mailer->send();
	}
}
