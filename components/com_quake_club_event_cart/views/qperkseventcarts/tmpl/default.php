<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_event_cart
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_quake_club_event_cart') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'qperkscartform.xml');
$canEdit    = $user->authorise('core.edit', 'com_quake_club_event_cart') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'qperkscartform.xml');
$canCheckin = $user->authorise('core.manage', 'com_quake_club_event_cart');
$canChange  = $user->authorise('core.edit.state', 'com_quake_club_event_cart');
$canDelete  = $user->authorise('core.delete', 'com_quake_club_event_cart');

$updateQuantityUrl = JRoute::_('index.php?option=com_quake_club_event_cart&task=qperkseventcarts.updateQuantity');
?>

<div id="app" class="cny-rat">
	<div class="pink-page-title cny-pattern">
		<div class="container">
			<h2>{{title}}</h2>
		</div>
		<img class="cloud-1" src="images/cny2020/cloud1.png" alt="">
		<img class="cloud-2" src="images/cny2020/cloud2.png" alt="">
	</div>

	<div v-show="showStatus" class="row mt-40">
		<div class="col-lg-8" >
			<div id="step-1" v-if="showStep == 1">
				<div class="alert-msg alert-danger" v-if="userQperks-totalPoint < 0">
					Insufficient Q-Coins.
				</div>
				<h3 class="f-20 mb-4">Item(s) added to your basket:</h3>
				<div class="cart-item" v-for="(item, index) in items">
					<div class="row">
						<div class="col-lg-3 col-sm-4 col-3">
							<div class="product-image mb-3 mb-sm-0" :style="{ 'background-image': 'url(./' + item.image1 + ')' }"></div>
						</div>
						<div class="col">
							<form action="<?php echo JRoute::_('index.php?option=com_quake_club_event_cart&task=qperkseventcarts.removeCart');?>" method="post">
								<h3 class="product-name pointer d-inline-block" @click="goToLink(item.id)">{{item.name}}</h3>
								<div class="price">
									<h3 v-if="item.promo == 1" class="product-points"><img src="images/cny2020/qcoin-slant.svg" class="qcoin"/> {{numberConvert(item.promo_qperks)}} Q-Coins</h3>
									<h3 class="product-points" :class="{'ori-price': item.promo == 1 }"><img src="images/cny2020/qcoin-slant.svg" class="qcoin"/> {{numberConvert(item.point)}} Q-Coins</h3>
								</div>

								<div class="mt-2 row">
									<label for="staticEmail" class="col-sm-3 col-lg-2 col-form-label bold">Quantity</label>
									<div class="col-sm-9 col-lg-10">
										<div class="quantity-input d-inline-block">
											<div class="toggle-minus" @click="minusQuantity(item)"><i class="fas fa-minus"></i></div>
											<div class="selected-quantity"><input class="form-control" @change="checkQuantity(item)" name="jform[product_quantity]" v-model="item.cart_quantity" type="text"></div>
											<div class="toggle-add" @click="addQuantity(item)"><i class="fas fa-plus"></i></div>
										</div>
										<!-- <div class="d-inline-block quantity-left">Only {{item.quantity}} item(s) left</div> -->
										<input name="jform[cart_id]" type="hidden" :value="item.cart_id">
										<input name="jform[product_id]" type="hidden" :value="item.id">

									</div>
								</div>
								<div class="mt-2 row edit-form" v-if="item.remark == 1">
									<label for="staticEmail" class="col-form-label bold pt-0 pb-2">Remarks</label>
									<div class="col-lg-10">
										<textarea rows="2" :id="'remarks'+index" name="" :placeholder="item.remark_description" @input="updateQuantity(item.cart_id, item.cart_quantity, item.id, item.cart_remark)" v-model="item.cart_remark" aria-required="true" autocomplete="quake" class="required form-control" style="height: auto; min-height: 100px;"></textarea>
										<div v-if="item.checkEmpty == 1" class="form-text form-error">Please specify your preferred size / colour.</div>
									</div>
								</div>
								<button type="submit" class="mt-3 btn btn-text text-highlight"><i class="fas fa-trash-alt"></i> Remove</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<form action="<?php echo JRoute::_('index.php?option=com_quake_club_event_cart&task=qperkseventcarts.confirmCart'); ?>" method="post" enctype="multipart/form-data">
			<div id="step-2" v-show="showStep == 2">
				<h3 class="f-20 mb-4">Fill in your details</h3>

				<div class="profile-form edit-form">
					<div class="mb-3 row">
						<label class="col-sm-4 col-form-label" for="user_name">Recipient name</label>
						<div class="col-sm-8">
							<input v-model="user.name" name="jform[name]"
							class="required form-control" id="user_name" type="text" placeholder="" required autocomplete="off">

							<div class="form-text form-error" v-if="!checkUserName">Please fill in recipient name</div>

						</div>
					</div>

					<div class="mb-4 row">
						<label class="col-sm-4 col-form-label" for="address">Delivery address
							<p class="form-text m-0">
								P.O. box address is not allowed
							</p>
						</label>
						<div class="col-sm-8">
							<textarea v-model="user.address" name="jform[address]"
							class="required form-control" :class="{'check-validate': !checkAddress}" id="address" type="text" placeholder="" required autocomplete="off" rows="3"></textarea>

							<div class="form-text form-error" v-if="!checkUserName">Please fill in recipient name</div>

						</div>
					</div>

					<hr />

					<div class="pb-3">
						<label class="px-0 py-2 mb-0">Upload business card</label>
						<p class="f-14 font-brand">Please upload your business card for us to verify your identity.</p>

						<div class="row">
							<div class="col-md-6 col-sm-7">

								<div class="hidden_input" :class="{'border-0 overflow-visible': !showImage}">
									<button type="button" v-show="!showImage" class="btn btn-pink">
										Upload file here
									</button>


									<input class="file_upload" name='business_card' type='file' accept="image/*" id="imgInp" @change="readUrl(this)"/>

									<div v-show="showImage"  class="hover">
										<label>Change</label>
									</div>
									<div v-show="showImage" class="bcard-placeholder with-img" id="business-card">
									</div>
								</div>
								<div v-show="showProgress" id="progress-wrp">
                  <div class="progress-bar"></div>
                  <div class="status">0%</div>
                </div>

								<div class="form-text form-error" v-if="!checkBusinessCard">Please upload your business card.</div>
								<div class="form-text form-error" v-if="!checkImgSize">Sorry, we could not upload this file. Maximum upload file size: 2MB</div>
								<div class="form-text form-error" v-if="!checkImgPercent">Please wait while the upload process is completed.</div>

							</div>
						</div>
					</div>

					<hr />

					<div class="row mt-40">
						<div class="col-6">
							<a class="btn btn-pink btn-block" @click="backToPrevious()" href="#my-carts">Back</a>
						</div>
						<div class="col-6">
							<a class="btn btn-pink btn-block" @click="goToNext()" href="#my-carts">Next</a>
						</div>
					</div>

				</div>
			</div>

			<div id="step-3" v-show="showStep == 3">

					<div class="">
						<div class="cart-item"  v-for="item in items">
							<div class="row">
								<div class="col-lg-3 col-4">
									<div class="product-image" :style="{ 'background-image': 'url(./' + item.image1 + ')' }"></div>
								</div>
								<div class="col">
										<h3 class="product-name">{{item.cart_quantity}}x {{item.name}}</h3>
										<div class="price">
											<h3 v-if="item.promo == 1" class="product-points"><img src="images/cny2020/qcoin-slant.svg" class="qcoin"/> {{numberConvert(item.cart_quantity * item.promo_qperks)}} Q-Coins</h3>
											<h3 class="product-points" :class="{'ori-price': item.promo == 1 }"><img src="images/cny2020/qcoin-slant.svg" class="qcoin"/> {{numberConvert(item.cart_quantity * item.point)}} Q-Coins</h3>
										</div>
										<div class="mt-2 small" v-if="item.remark == 1">
											<em><b>Remarks:</b> {{item.cart_remark}}</em>
										</div>

										<input type="hidden" name="item[id][]" :value="item.id">
										<input type="hidden" name="item[quantity][]" :value="item.cart_quantity">
										<input type="hidden" name="item[point][]" :value="item.point">
										<input type="hidden" name="item[promo][]" :value="item.promo">
										<input type="hidden" name="item[promo_qperks][]" :value="item.promo_qperks">
										<input type="hidden" name="item[name][]" :value="item.name">
										<input type="hidden" name="item[remark][]" :value="item.cart_remark">
								</div>
							</div>
						</div>
					</div>

					<hr />

					<div class="profile-form readonly-form">
						<div class="mb-3 row">
							<label class="col-sm-4 col-form-label" for="user_name">Recipient name</label>
							<div class="col-sm-8">
								<div class="form-control">{{user.name}}</div>
								<input name="jform[name]" type="hidden" :value="user.name">
							</div>
						</div>

						<div class="mb-4 row">
							<label class="col-sm-4 col-form-label" for="address">Delivery address</label>
							<div class="col-sm-8">
								<div class="form-control">{{user.address}}</div>
								<input name="jform[recipient_name]" type="hidden" :value="user.name">
								<input name="jform[address]" type="hidden" :value="user.address">
								<input name="jform[point]" type="hidden" :value="userQperks">
								<input name="jform[point-used]" type="hidden" :value="totalPoint">
								<input name="jform[total-point]" type="hidden" :value="userQperks-totalPoint">
							</div>
						</div>

						<div class="mb-3 row">
							<label class="col-sm-4 col-form-label" for="">Business card</label>

							<div class="col-sm-6 col-lg-5 col-md-4">
								<div class="hidden_input">
									<div v-if="imageChanged != 1" class="bcard-placeholder with-img" :style="{ 'background-image': 'url('+businessCard+')'}">
									</div>
									<div v-if="imageChanged == 1" class="bcard-placeholder with-img" :style="{ 'background-image': 'url('+businessCard+')'}">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row mt-40">
						<div class="col-6">
							<a class="btn btn-pink btn-block" @click="backToPrevious()" href="#my-carts">Back</a>
						</div>
						<div class="col-6">
							<button class="btn btn-pink btn-block" type="submit">Confirm</button>
						</div>
					</div>

				</div>
			</form>
		</div>
		<div class="col-lg-4 cart-summary mt-40 mt-lg-0" v-if="showStep == 1">
			<div class="rounded-card bg-grey club-shadow">
				<div class="card-body">
					<h3 class="f-20 mb-4">Summary</h3>

					<dl class="row mb-4">
					  <dt class="col-8 bold"><img src="images/cny2020/qcoin-slant.svg" class="qcoin"/> Your Q-Coins</dt>
					  <dd class="col-4 bold text-highlight">{{numberConvert(userQperks)}}</dd>
					</dl>
					<dl class="row" v-for="item in items" >
						<dt class="col-8"><b>{{item.cart_quantity}}</b> × {{item.name}}</dt>
						<dd class="col-4">
							<span :class="{'ori-price ml-0 mr-2': item.promo == 1 }">{{numberConvert(parseInt(item.cart_quantity) * parseInt(item.point))}}</span>
							<span v-if="item.promo == 1">{{numberConvert(parseInt(item.cart_quantity) * parseInt(item.promo_qperks))}}</span>
						</dd>

					</dl>

					<dl class="row no-gutters summary-balance">
					  <dt class="col-8"><img src="images/cny2020/qcoin-slant.svg" class="qcoin"/> Q-Coins balance</dt>
					  <dd class="col-4 text-highlight">{{numberConvert(userQperks-totalPoint)}}</dd>
					</dl>

				</div>
			</div>
			<a class="btn btn-block btn-pink mt-2" @click="goToNext()" :class="{'disabled': userQperks-totalPoint < 0 }"  href="#my-carts">Proceed to checkout</a>

		</div>
	</div>
	<div v-show="!showStatus" class="no-item">
		<h3 class="f-20 mb-4">Your basket is empty</h3>
		<p class="font-brand">Looks like you have no item in your basket, browse the redemption catalogue and start redeeming now.</p>
		<a href="./prosperatty" class="btn btn-pink btn-wide mt-4">View catalogue</a>
	</div>

</div>

<script>
var itemsFromObject = <?php echo json_encode($this->filtered_items); ?>;
var userQperksFromObject = <?php echo json_encode($this->user_qpoints); ?>;
var businessCardFromObject = <?php echo json_encode($this->business_card); ?>;
var userFromObject = <?php echo json_encode($user); ?>;
var updateQuantityUrl = <?php echo json_encode($updateQuantityUrl); ?>;

    var app = new Vue({
    	el: '#app',
    	data: {
    		items: itemsFromObject,
    		userQperks: userQperksFromObject,
    		businessCard: businessCardFromObject[0],
    		user: userFromObject,
    		userBirthdayPoint: 0,
    		showStep: 1,
    		totalPoint: 0,
    		imageChanged: 0,
    		checkUserName: true,
    		checkAddress: true,
				checkImgSize : true,
    		checkBusinessCard: true,
    		checkImgPercent: true,
    		showImage: false,
				showStatus: false,
				showProgress: false,
				percent: 0,
				title: "My Basket"
    	},
    	mounted: function () {
				if (this.items.length>0) {
					this.showStatus = true;
				}
    		this.qPerksBalance();
    		if (this.businessCard != "") {
    			this.showImage = true;
					this.percent = 100;
					this.checkImgPercent = true;
					var progress_bar_id = "#progress-wrp";

					jQuery(progress_bar_id + " .progress-bar").css("width", +_this.percent + "%");
					jQuery(progress_bar_id + " .status").text(_this.percent + "%");
    			jQuery('#business-card').css('background-image', 'url("' + this.businessCard + '")');
    		}
    	},
    	updated: function () {
				switch (this.showStep) {
					case 1:
						this.title = "My Basket";
						break;
					case 2:
						this.title = "Delivery Information";
						break;
					case 3:
						this.title = "Order Confirmation";
						break;
					default:
						break;
				}
			},
    	methods: {
    		checkQuantity: function (item) {
    			item.cart_quantity = parseInt(item.cart_quantity);
    			if (isNaN(item.cart_quantity)) {
    				item.cart_quantity = 0;
    			}
    			if (item.cart_quantity <= 1) {
    				item.cart_quantity = 1;
    			} else if (item.cart_quantity >= item.quantity) {
    				item.cart_quantity = item.quantity;
    			}
    			this.qPerksBalance();
					this.updateQuantity(item.cart_id, item.cart_quantity, item.id);

    		},
    		minusQuantity: function (item) {
    			item.cart_quantity = parseInt(item.cart_quantity);
    			if (item.cart_quantity <= 1) {
    				item.cart_quantity = 1;
    			} else {
    				item.cart_quantity--;
    			}
    			this.qPerksBalance();
					this.updateQuantity(item.cart_id, item.cart_quantity, item.id);

    		},
    		addQuantity: function (item) {
    			item.quantity = parseInt(item.quantity);
    			if (item.cart_quantity >= item.max_redemption) {
    				item.cart_quantity = item.max_redemption;
    			} else {
    				item.cart_quantity++;
    			}
    			this.qPerksBalance();
					this.updateQuantity(item.cart_id, item.cart_quantity, item.id);

    		},
				updateQuantity: function (id, quantity, product_id,remark){

					jQuery.ajax({
                url: updateQuantityUrl,
                type: 'post',
                data: {'quantity':quantity,'id':id,'product_id':product_id,'remark':remark},
                success: function (result) {
                  console.log("success");

                },
                error: function () {
                  console.log('fail');
                }
              });
				},
				qPerksBalance: function () {
    			var tmp_num = 0;
    			_this = this;
    			_this.totalPoint = 0;
    			for (var index = 0; index < _this.items.length; index++) {
    				if (_this.items[index].promo == 1) {
    					tmp_num = (parseInt(_this.items[index].promo_qperks) * parseInt(_this.items[index].cart_quantity));
    				} else {
    					tmp_num = (parseInt(_this.items[index].point) * parseInt(_this.items[index].cart_quantity));
    				}
    				_this.totalPoint += tmp_num;
    			}
    		},
    		backToPrevious: function () {
    			this.showStep--;
    		},
    		goToNext: function () {
    			var address = document.getElementById('address');
    			var user_name = document.getElementById('user_name');
    			var business_card = document.getElementById('imgInp');
					var counter = 0;

    			switch (this.showStep) {
    				case 1:
						_this=this;
						for (var index = 0; index < _this.items.length; index++) {
								var tmp = document.getElementById('remarks'+index);
								if (tmp) {
									if (tmp.value == "") {
										counter++;
										_this.items[index].checkEmpty = 1;
										_this.items[index].cart_remark = " ";
										_this.items[index].cart_remark = "";
									}else{
										_this.items[index].checkEmpty = 0;
									}
								}
							}
							if (counter>0) {

							}else{
								_this.showStep++;
							}
    					break;
    				case 2:
    					this.checkUserName = user_name.checkValidity();
    					this.checkAddress = address.checkValidity();
							if (this.businessCard == "") {
								this.checkBusinessCard = false;
							}else{
								this.checkBusinessCard = true;
								if (this.checkImgSize) {
									if (this.percent <100) {
								this.checkImgPercent = false;
								}else{
									this.checkImgPercent = true;
								}
								}
							}

    					if (address.checkValidity() && user_name.checkValidity() && this.checkBusinessCard && this.checkImgSize &&this.checkImgPercent) {
    						this.showStep++;
    					}
    					break;

    				default:
    					break;
    			}
    		},
    		readUrl: function(input) {
    			input = jQuery("#imgInp")[0];
    			_this = this;
					_this.percent = 0;
					_this.showProgress = true;
					var progress_bar_id = "#progress-wrp";
					jQuery(progress_bar_id + " .progress-bar").css("width", +_this.percent + "%");
					jQuery(progress_bar_id + " .status").text(_this.percent + "%");

    			if (input.files && input.files[0]) {
						_this.checkBusinessCard = true;

						if (input.files[0].size >= 2376496) {

							_this.checkImgSize = false;

						}else{
							var file = input.files[0];

							var xhr = new XMLHttpRequest();
							_this.checkImgPercent=true;
							xhr.upload.addEventListener('progress', onprogressHandler, false);
							xhr.open('POST', '/upload/uri', true);
							xhr.send(file); // Simple!

							function onprogressHandler(evt) {
									_this.percent = Math.ceil(evt.loaded/evt.total*100);

									console.log('Upload progress: ' + _this.percent + '%');
									jQuery(progress_bar_id + " .progress-bar").css("width", +_this.percent + "%");
    							jQuery(progress_bar_id + " .status").text(_this.percent + "%");
							}
							var reader = new FileReader();

							reader.onload = function (e) {
								jQuery('#business-card').css('background-image', 'url("' + e.target.result + '")');
								_this.businessCard = e.target.result;
								_this.imageChanged = 1;

								// jQuery('#business-card').attr('src', e.target.result);
							}

							reader.readAsDataURL(input.files[0]);
							_this.showImage = true;
							_this.checkImgSize = true;

						}

    			}
    		},
				goToLink :function(link){
						window.location.href = 'rewards-catalogue/qperksproduct/'+link;
				},
				numberConvert :function(number){
					return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				}
    	}
    })
</script>
