<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_programmeupdatespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$about_quake_article = $this->about_quake[0]->content;
?>

<div class="sc">
    <div class="tab-pills scrollable-tab">
      <div class="row">
        <div class="col"><a href="./about-quake/about" class="tab-pill active">About Quake</a></div>
        <div class="col"><a href="./about-quake/our-team" class="tab-pill">Our Team</a></div>
        <div class="col"><a href="./about-quake/get-in-touch" class="tab-pill">Get In Touch</a></div>
      </div>
    </div>

    <div class="editor-content">
      <?php echo $about_quake_article; ?>
    </div>
</div>
