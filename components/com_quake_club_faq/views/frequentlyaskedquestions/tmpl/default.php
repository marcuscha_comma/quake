<div id="app" class="">
  <div class="pink-page-title">
   <div class="container">
     <h2>Frequently Asked Questions (FAQ)</h2>
   </div>
 </div>

 <div class="accordion-holder mt-5">
   <div class="accordion" id="faq-accordion">
     <!-- Collapse start -->
         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-1" aria-expanded="true" aria-controls="faq-1">
               <h5>
                 1.	What is Quake Club?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-1" class="collapse show" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>Quake Club is a participation-based loyalty programme owned and managed by Astro Media Sales. Exclusively designed for professionals in advertising and marketing industry, members can share knowledge with peers and earn points, or better known as Q-Perks. </p>
                     <p>By performing activities listed under the programme, Q-Perks are redeemable by the Members for Rewards from Quake. The Rewards will be sent via postal delivery.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-2" aria-expanded="false" aria-controls="faq-2">
               <h5>
                 2.	How does it work?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-2" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>Users can sign up online on the Quake website, participate in the activities listed under the programme to collect Q-Perks and use the collected Q-Perks to redeem Rewards.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-3" aria-expanded="false" aria-controls="faq-3">
               <h5>
                 3.	How do I become a member?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-3" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>Click <a class="text-highlight" href="<?php echo $this->baseurl ?>/notice">here</a> to sign up.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-4" aria-expanded="false" aria-controls="faq-4">
               <h5>
                 4.	Am I eligible for membership?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-4" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>Only professionals in the advertising and marketing industry are allowed to become a member. Employees, officers, directors, agents and representatives of Quake Club Rewards Programme, Astro employees and its affiliates are eligible for Membership. Do note Astro employees are not allowed to redeem the rewards on Quake Club Rewards Programme. Non-professionals and those who are from other industries are not eligible for Quake Club Rewards Programme membership.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-5" aria-expanded="false" aria-controls="faq-5">
               <h5>
                 5.	Do I have to pay for membership?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-5" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>Membership is free of charge.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-6" aria-expanded="false" aria-controls="faq-6">
               <h5>
                 6.	Is Quake Club membership open to non-Malaysians and what is the age limit?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-6" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>Yes, membership is open to non-Malaysians. The minimum age requirement for membership is 18 years old and above.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-7" aria-expanded="false" aria-controls="faq-7">
               <h5>
                 7.	What is Q-Perks?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-7" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>Q-Perks are points rewarded to Members when they participate in the activities listed under the programme.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-8" aria-expanded="false" aria-controls="faq-8">
               <h5>
                 8.	How do I collect Q-Perks?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-8" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <h5>a. Via activities performed</h5>

                     <div class="qperks-howto mb-4">
                       <div class="row">
                         <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+100</span>
                                   <img alt="First-time sign up" srcset="images/club/reward-01@2x.png 2x, images/club/reward-01.png 1x " src="images/club/reward-01.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>First-time sign up</h4>
                               <p>Enjoy 100 Q-Perks when you sign up for the first time. Q-Perks will be automatically added upon successful sign up.</p>
                             </div>
                           </div>
                         </div>

                         <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+10</span>
                                   <img alt="Profile update" srcset="images/club/reward-03@2x.png 2x, images/club/reward-03.png 1x " src="images/club/reward-03.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>Profile update</h4>
                               <p>Update your profile details at "My Account" page and get 10 Q-Perks. This includes name card upload.</p>
                             </div>
                           </div>
                         </div>

                         <!-- <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+20</span>
                                   <img alt="Article sharing" srcset="images/club/reward-02@2x.png 2x, images/club/reward-02.png 1x " src="images/club/reward-02.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>Article sharing</h4>
                               <p>Earn 20 Q-Perks every time you share an article on Quake website.</p>
                             </div>
                           </div>
                         </div> -->

                         <!-- <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+20</span>
                                   <img alt="File download" srcset="images/club/reward-04@2x.png 2x, images/club/reward-04.png 1x " src="images/club/reward-04.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>File download</h4>
                               <p>Earn 20 Q-Perks every time you download our content on Quake website. </p>
                             </div>
                           </div>
                         </div> -->

                         <!-- <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+30</span>
                                   <img alt="Share winning partnership articles" srcset="images/club/reward-09@2x.png 2x, images/club/reward-09.png 1x " src="images/club/reward-09.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>Winning partnership sharing</h4>
                               <p>Earn 30 Q-Perks whenever you share an article from our Winning Partnership Series on Quake website.</p>
                             </div>
                           </div>
                         </div> -->

                         <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+10</span>
                                   <img alt="Social media linking" srcset="images/club/reward-05@2x.png 2x, images/club/reward-05.png 1x " src="images/club/reward-05.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>Social media linking</h4>
                               <p>Link your social media account at “My Account” page to gain 10 Q-Perks.</p>
                             </div>
                           </div>
                         </div>

                         <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+20</span>
                                   <img alt="Join Quake WhatsApp" srcset="images/club/reward-10@2x.png 2x, images/club/reward-10.png 1x " src="images/club/reward-10.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>Join Quake WhatsApp</h4>
                               <p>Click the Whatsapp link on “My Account” page and send a message to get 20 Q-Perks.</p>
                             </div>
                           </div>
                         </div>

                         <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+50</span>
                                   <img alt="Friend referral" srcset="images/club/reward-06@2x.png 2x, images/club/reward-06.png 1x " src="images/club/reward-06.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>Friend referral</h4>
                               <p>Refer a friend who is eligible for membership to join Quake Club. For each successful sign up from your friend (via your referral link), you will get 50 Q-Perks.</p>
                             </div>
                           </div>
                         </div>

                         <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+80</span>
                                   <img alt="Event attendance" srcset="images/club/reward-07@2x.png 2x, images/club/reward-07.png 1x " src="images/club/reward-07.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>Event attendance</h4>
                               <p>Attend trade events organised by Astro Media Solutions and enjoy 80 Q-Perks per event. </p>
                             </div>
                           </div>
                         </div>

                         <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+200</span>
                                   <img alt="Birthday gift redemption" srcset="images/club/reward-08@2x.png 2x, images/club/reward-08.png 1x " src="images/club/reward-08.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>Birthday gift redemption</h4>
                               <p>A birthday gift of an additional 200 Q-Perks will be sent via email. You can only redeem these points during your birthday month. Enjoy the special gift!</p>
                             </div>
                           </div>
                         </div>

                         <div class="col-lg-6">
                           <div class="row align-items-center mb-4">
                             <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                               <div class="perks-holder">
                                 <div class="bubble-holder">
                                   <span>+20</span>
                                   <img alt="Share" srcset="images/club/reward-watch@2x.png 2x, images/club/reward-watch.png 1x " src="images/club/reward-watch.png" >
                                 </div>
                               </div>
                             </div>
                             <div class="col-xl-9 col-lg-8 col-sm-9 ">
                               <h4>Watch</h4>
                               <p>For every video that you watch on QuakeCast.</p>
                             </div>
                           </div>
                         </div>

                       </div>

                     </div>

                     

                     <h5 class="mb-4">b. Via social media sharing</h5>

                     <div class="qperks-howto">
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="row mb-2 engagement">
                              <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                                <div class="perks-holder">
                                  <div class="bubble-holder">
                                    <span>+3 per<br>engagement</span>
                                    <img alt="Content sharing on social media" src="images/club/engagment@2x.png" >
                                  </div>
                                </div>
                              </div>
                              <div class="col-xl-9 col-lg-8 col-sm-9 ">
                                 <!-- <h4>Content sharing on social media</h4> -->
                                 <div class="small">
                                   <b><u>Step 1</u></b>
                                  <p>Share Quake articles and videos on your social media account.</p>
 
                                  <b><u>Step 2</u></b><br>
                                   Calculate your total Q-perks using the formula below:<br>
                                   <em>1 engagement = 3 Q-Perks</em>
                                   <div class="p-2 bg-light border mb-3">
                                     <p>For example, 52 Likes + 15 Comments + 3 Shares = 70 Total Engagements X 3 Q-Perks = 210 Q-Perks</p>
                                   </div>
                                 
                                   <b><u>Step 3</u></b>
                                 <p>Choose your preferred item from Quake Club gift catalogue, attach your total Q-Perks collected with URL links of shared posts and redeem via email to <a href="mailto:quakeclub@astro.com.my">quakeclub@astro.com.my</a> </p>
                                 </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="row mb-4 justify-content-end">
                              <div class="col-sm-9 col-lg-12">
                                <!-- <h4 class="invisible">Content sharing on social media</h4> -->

                                <div class="small">
                                  <b><u>Terms and conditions apply</u></b>
                                  <ul class="pl-3">
                                    <li>Your social media profile must be set to public.</li>
                                    <li>All articles and videos shared has to be shared within that quarter. <span class="d-inline-block">(Q1: 1 Jan - 31 Mar;</span> <span class="d-inline-block">Q2: 1 Apr - 30 Jun,</span> <span class="d-inline-block">Q3: 1 July - 30 Sep,</span> <span class="d-inline-block">Q4: 1 Oct - 31 Dec).</span> </li>
                                    <li>The URL links to posts that aren't viewable or set to public will be disqualified automatically.</li>
                                    <li>Limited to only 1 redemption per quarter and all unused points will be forfeited in next quarter. </li>
                                  </ul>
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="row align-items-center mb-4">
                              <div class="col-xl-3 col-lg-4 col-sm-3 pr-sm-0">
                                <div class="perks-holder">
                                  <div class="bubble-holder">
                                    <span>+50</span>
                                    <img alt="Share" srcset="images/club/reward-share@2x.png 2x, images/club/reward-share.png 1x " src="images/club/reward-share.png" >
                                  </div>
                                </div>
                              </div>
                              <div class="col-xl-9 col-lg-8 col-sm-9 ">
                                <h4>Share</h4>
                                <p>For sharing QuakeCast videos on Social Media.
                                  <small class="d-block">*Quake Club members to send proof of video sharing to <a href="mailto:quakeclub@astro.com.my">quakeclub@astro.com.my</a></small>
                                 </p>
                              </div>
                            </div>
                          </div>
                        </div>
                     </div>

                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-9" aria-expanded="false" aria-controls="faq-9">
               <h5>
                 9.	Is there a limit to the number of Q-Perks I can accumulate?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-9" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>There is no limit, but each Member can only earn Q-Perks for maximum 3 activities per day. No points will be allocated for the 4th activity and onwards  performed by the Member within the day.  </p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-10" aria-expanded="false" aria-controls="faq-10">
               <h5>
                 10.	Can I have more than one account?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-10" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>Each Member can only have one account.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-11" aria-expanded="false" aria-controls="faq-11">
               <h5>
                 11. How do I redeem my Rewards?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-11" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                  <p>For activities under <b>8a</b>:<br>
                  By selecting the desired item(s) from Reward Catalogue, adding onto the cart and checking out to proceed with the redemption.</p>
                  
                  <p>For activities under <b>8b</b>:<br>
                  By selecting the desired item(s) from Reward Catalogue, , attach your total Q-Perks collected with URL links of shared posts and redeem via email to <a href="mailto:quakeclub@astro.com.my">quakeclub@astro.com.my</a></p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-12" aria-expanded="false" aria-controls="faq-12">
               <h5>
                 12.	How many items can I redeem at one go?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-12" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>You can redeem a lot of items, but can only redeem once a quarter. Exception will be on your birthday month, whereby you can make a redemption regardless if you had already made a redemption in that quarter.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-13" aria-expanded="false" aria-controls="faq-13">
               <h5>
                 13.	Do I have a limit of redemption?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-13" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>You can only redeem once every quarter of the year. Exception will be on your birthday month, whereby you can make a redemption regardless if you already made a redemption in that quarter.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-14" aria-expanded="false" aria-controls="faq-14">
               <h5>
                 14.	How long will I need to wait for the item(s) I have redeemed to arrive?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-14" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>Your item(s) will arrive within 2 weeks of redemption confirmation date. Subject to eligibility and stock availability.</p>
                 </div>
             </div>
         </div>

         <div class="card border-0">
             <div class="card-header" data-toggle="collapse" data-target="#faq-15" aria-expanded="false" aria-controls="faq-15">
               <h5>
                 15.	How can I contact Quake Club?
               </h5>
               <i class="fas fa-plus indicator"></i>
             </div>

             <div id="faq-15" class="collapse" data-parent="#faq-accordion">
                 <div class="card-body">
                     <p>You can email us at <a class="text-highlight" href="mailto:quakeclub@astro.com.my">quakeclub@astro.com.my</a> </p>
                 </div>
             </div>
         </div>
     <!-- Collapse End -->
   </div>
 </div>
</div>
