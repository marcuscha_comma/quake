<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_2021_cny
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Layout\LayoutHelper;

HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('bootstrap.tooltip');
HTMLHelper::_('behavior.multiselect');
HTMLHelper::_('formbehavior.chosen', 'select');

$user       = Factory::getUser();
$userId     = $user->get('id');
$document = Factory::getDocument();
// $document->addStyleSheet(Uri::root() . 'media/com_quake_club_2020_raya/css/list.css');

$document->addStyleSheet(Uri::root() . 'components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/css/sweetalert2.min.css');
$document->addStyleSheet(Uri::root() . 'components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/css/owlcarousel/owl.carousel.min.css');
$document->addStyleSheet(Uri::root() . 'templates/bootstrap4/css/modal.css?v=0620');
$document->addStyleSheet(Uri::root() . 'components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/css/style.css?v=1.02');

// swal2 and owl carousel
$document->addScript(Uri::root() . '/media/jui/js/jquery.min.js?8a4d2b53490940e6cc1327cd9f30a6ec', 'text/javascript');
$document->addScript(Uri::root() . '/media/jui/js/chosen.jquery.min.js?8a4d2b53490940e6cc1327cd9f30a6ec', 'text/javascript');
$document->addScript(Uri::root() . 'components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/js/sweetalert2/sweetalert2.min.js', 'text/javascript');
$document->addScript(Uri::root() . 'components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/js/owlcarousel/owl.carousel.min.js', 'text/javascript');

?>

<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bcd3eb6f160f08f"></script>
<style>

body {
    padding-top: 0px;
}

.body .content > .container {
    max-width: unset;
}

main#content {
	padding: 0px;
}

#flipbook{
    width:400px;
    height:300px;/*
    background: #ece9e6;
    background-image: url('./img/main-bg.jpg'); /*linear-gradient(to top, #fbc2eb 0%, #a6c1ee 100%);*//*
    background-position: center center;
    background-size: cover;*/
    max-width: 100%;
    overflow: hidden;

    display: flex;
    align-items: center;
    justify-content: center;
}

#flipbook .page{
    width:400px;
    height:300px;
    background-color:white;
    line-height:300px;
    font-size:20px;
    text-align:center;
}

#flipbook .page-wrapper{
    -webkit-perspective:2000px;
    -moz-perspective:2000px;
    -ms-perspective:2000px;
    -o-perspective:2000px;
    perspective:2000px;
}

.btn-submit {
  font-size: 12px;
}

.btn-submit i {
  margin-right: 6px;
}

#nav-left, #nav-right {
  cursor: pointer;
  height: 100%;
  display: none;
}

#nav-left i, #nav-right i {
  -webkit-transition: all 0.3s ease-out;
  -moz-transition: all 0.3s ease-out;
  -o-transition: all 0.3s ease-out;
  transition: all 0.3s ease-out;
  margin-top: 100px;
  margin-bottom: 100px;
}

#nav-left:hover i {
  margin-right: 10px;
}
#nav-right:hover i {
  margin-left: 10px;
}

#overlay-div{
		width: 100%;
		position: absolute;
		top: 0;
		bottom: 0;
		z-index: 10;
	}
#middle-content {
	margin: 0px auto;
	max-width: 1280px;
    padding: 0px;
}

button {
	outline: none !important;
	border: none !important;
}

</style>
<div id="cny2021">

<div style="display: none;">
	<audio controls id="bgm" preload="auto" loop>
		<source src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/sound/bgm.mp3" type="audio/mpeg">
		<source src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/sound/android/bgm.ogg" type="audio/ogg">
	  	<source src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/sound/ios/bgm.m4a" type="audio/mp4">
	</audio>
</div>

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-2 col-3 logo-wrapper" >
			<a href="/login">
				<img id="logo-click" src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/astro-media-solutions-white.png" alt="">
			</a>
		</div>
		<div class="col-md-8 col-9 text-right button-wrapper">
		  	<div class="d-inline-block share-button-container">
		    	<span class="share-title">Share: </span>
				<div class="addthis_inline_share_toolbox"></div>
				<div v-if="loginStatus == '0'" id="overlay-div" data-toggle="modal" data-target="#shareCenterModal"></div>
		  	</div>

		  	<div class="d-inline-block">
				<?php if($userId>0){ ?>
					<button class="btn btn-sm btn-submit" @click="downloadFile">
		        		<i class="fas fa-arrow-down" style="cursor:pointer;"></i>Download
					</button>
				<?php }else{ ?>
					<button data-toggle="modal" data-target="#downloadCenterModal" class="btn btn-sm btn-submit">
		        		<i class="fas fa-arrow-down" style="cursor:pointer;"></i>Download
					</button>
				<?php } ?>
				<div class="btn btn-sm btn-submit" id="close-click">
			    	<i class="fas fa-window-close" style="cursor:pointer;"></i> Close
				</div>
		    </div>

		    <div id="expand-click" class="d-inline-block">
				<div class="btn btn-sm btn-submit">
					<i class="fas fa-expand" style="cursor:pointer;"></i> Fullscreen
				</div>
		    </div>

		    <div class="d-inline-block mt-2 mr-2">
				<div class="btn btn-sm btn-submit mute-btn-wrapper">
					<a href="javascript:toggleMute();" class="mute-btn">mute</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
<div class="row align-items-center" id="middle-content-height">

	<div class="col-1">

	</div>

	<div class="col-lg-10" id="middle-content">
		<div id="flipbook" class="flipbook">

			<div class="flipbook-inner-wrapper">
				<div class="flipbook-header">
					<img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/title-banner.png">
				</div>
				<div class="carousel-control">
					<a href="javascript:carouselNavigation(-1);"><i class="arrow leftArrow"></i></a>
					<a href="javascript:carouselNavigation(1);"><i class="arrow rightArrow"></i></a>
				</div>
				<div class="card-carousel">
					<div class="my-card card-09 dum"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-09.png"></div>
					<div class="my-card card-10 dum"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-10.png"></div>
					<div class="my-card card-01"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-01.png"></div>
					<div class="my-card card-02"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-02.png"></div>
					<div class="my-card card-03"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-03.png"></div>
					<div class="my-card card-04"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-04.png"></div>
					<div class="my-card card-05"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-05.png"></div>
					<div class="my-card card-06"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-06.png"></div>
					<div class="my-card card-07"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-07.png"></div>
					<div class="my-card card-08"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-08.png"></div>
					<div class="my-card card-09"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-09.png"></div>
					<div class="my-card card-10"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-10.png"></div>
					<div class="my-card card-01 dum"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-01.png"></div>
					<div class="my-card card-02 dum"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/card-02.png"></div>
				</div>
			</div>

		</div>
	</div>

	<div class="col-1">

	</div>

	<div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" hidden="true"></div>

				<div class="modal-body">
					<div>
						<button type="button" class="close font-brand" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">Close</span>
						</button>

						<div style="clear: both;"></div>
					</div>
					<div class="text-center">
						<h3 class="text-white">Login to Download</h3>
						<p class="text-white mb-4">Please login or signup download this eBook.</p>
	          			<a href="./login" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade custom-modal" ref="vuemodal" id="shareCenterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" hidden="true"></div>

				<div class="modal-body">
					<div>
						<button @click="closeModal" type="button" class="close font-brand" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">Close</span>
						</button>

						<div style="clear: both;"></div>
					</div>
					<div class="text-center">
						<h3 class="text-white">Login in to earn points</h3>
						<p class="text-white mb-4">Hey, we know you're excited to redeem exclusive merchandise! Log in now or sign up to earn Q-Perks when you share this eBook.</p>
						<a href="#" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
						<a href="#" data-dismiss="modal" class="btn btn-white">Skip</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
</div>

</div>
<div class="warning"><div>Please rotate your device</div></div>
<!--
<img class="cloud cloud-left img-fluid" src="images/rsvp/footer-left.png" alt="">
<img class="cloud cloud-right img-fluid" src="images/rsvp/footer-right.png" alt="">
-->
<script type="text/javascript">
var updateUserShareArticlePointUrl = <?php echo json_encode($updateUserShareArticlePointUrl); ?>;
var userId = <?php echo json_encode($userId); ?>;
var bgmPlaying = false;
var source = "share-cny-2021";
var shareOrDownloadStatus = "shareEbook";
var slideReady = true;

jQuery(document).ready(function() {
    // console.log('document ready!');
    var allowCache = true;
    var isMute = false;
    if(allowCache){
    	if(localStorage.hasOwnProperty('cny-mute')){
	        isMute = JSON.parse(localStorage.getItem('cny-mute'));
	    } else {
	        // is not mute by default
	        isMute = false;
	        localStorage.setItem('cny-mute', isMute);
	    };
    };
    // isMute = false;
    // console.log('is mute: ' + isMute);

    // mute button status
    if(isMute){
        jQuery('.mute-btn').addClass('mute');
    } else {
        jQuery('.mute-btn').removeClass('mute');
    };

	window.isMobile = !!navigator.userAgent.match(/iPad|iPhone|iPod|Opera Mini|IEMobile|Android|BlackBerry|Windows Phone|webOS/i);
    window.iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    window.iphone_x = false;
    window.android = (navigator.userAgent.match(/Android/i) ? true : false);

    // audio object properties reference : https://www.w3schools.com/jsref/dom_obj_audio.asp
    if(isMobile){
        jQuery('body').addClass('isMobile');
        if(window.iOS){
            jQuery('body').addClass('is_iOS');
        };
    } else {
        jQuery('body').addClass('isDesktop');
    };

    var thisBGM = jQuery('#bgm')[0];
    // bgm too loud, reduce the volume
    thisBGM.volume = 0.1;
    // looping
    thisBGM.loop = true;
    // thisBGM.autoplay = true;
    // thisBGM.play();
    // console.log(thisBGM.paused);

/*
    console.log("is iOS: " + iOS + ", is android: " + android);
    console.log(flipSFX);
    console.log(thisBGM);
*/
	var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
	if (iOS) {
		$( "#expand-click div" ).hide();
		$('#close-click').hide();
	}
	$('#close-click').hide();

	window.onorientationchange = function() {
        var orientation = window.orientation;
            switch(orientation) {
                case 0:
                case 90:
                case -90: window.location.reload();
                break; }
    };

    window.playBGM = function(play){
    	// console.log('attemp playing bgm');

		if(!isMute && play){
			// play the music
			if(!bgmPlaying){
				bgmPlaying = true;
				thisBGM.currentTime = 0;
				thisBGM.play();
			};
		} else {
			// stop the music
			if(bgmPlaying){
            	bgmPlaying = false;
            	thisBGM.pause();
            };
		};
	};
	// force all buttons to play bgm
	$('.btn.btn-submit').click(function(){
		playBGM(true);
	});
	$('.at-share-btn').click(function(){
		playBGM(true);
	});
	$('#overlay-div').click(function(){
		playBGM(true);
	});

    window.toggleMute = function(){
        // console.log('toggle isMute: ' + isMute);
        isMute = !isMute;
        // console.log('toggled: ' + isMute);

        // button state
        if(isMute){
            jQuery('.mute-btn').addClass('mute');

            playBGM(false);
        } else {
            jQuery('.mute-btn').removeClass('mute');

            // if(flippedOnce){
            	playBGM(true);
            // };
        };

        // save settings
        if(allowCache) localStorage.setItem('cny-mute', isMute);
        // console.log('mute settings saved');
        // console.log(localStorage.getItem('cny-mute'));
    };

	function loadApp() {
		var size = getSize();
		$('.flipbook').width(size.width);
		$('.flipbook').height(size.height);
	}

	function getSize() {
		// console.log(window.innerHeight);
		// var width = document.body.clientWidth;
		// var height = document.body.clientHeight;
		var width = $("#middle-content").width(); //(window.innerWidth < 0) ? $("#middle-content").width() : screen.width;
		var height = (window.innerWidth < 768) ? '400px' : '768px';//(window.innerHeight > 0) ? '768px' : '768px'; // '40vw' : '40vw';
		if(height == '768px' && window.innerHeight <= 768){
			height = '640px';
		};

		console.log(height);

		return {
			width: width,
			height: height
		}
	}

	function resize() {
		// console.log('resize event triggered');
		var size = getSize();
		// console.log(size);

		if (size.width > size.height) { // landscape
			$('.flipbook').turn('display', 'single');
		}
		else {
			$('.flipbook').turn('display', 'single');
		}

		$('.flipbook').turn('size', size.width, size.height);
	}

	// Load App
	loadApp();

	var scrollPosition = [
		self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
		self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
	];

	// var download_button = document.getElementById("download-click");

	// download_button.onclick = function(){
	// 	// location.href = "../../../../../uploads/cny-dl-files/ASTRO CNY Story (8 facts).pdf";
		
	// }

	var elem = document.getElementById("expand-click");

	elem.onclick = function() {
		$('#expand-click div').hide();
		$('#logo-click').hide();
		$('#download-click').hide();
		var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
		if (iOS) {
			$('#close-click').hide();
		}else{
			$('#close-click').show();
		}
		
		if (!document.fullscreenElement &&    // alternative standard method
			!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
			if (document.documentElement.requestFullscreen) {
				document.documentElement.requestFullscreen();
			} else if (document.documentElement.msRequestFullscreen) {
				document.documentElement.msRequestFullscreen();
			} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
			} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		} else {
			if (document.exitFullscreen) {
				document.exitFullscreen();
			} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
			}
		}
		screen.orientation.lock("landscape");
	}

	var elem2 = document.getElementById("close-click");

	elem2.onclick = function() {
		$('#expand-click div').hide();
		$('#logo-click').show();
		$('#download-click').hide();
		var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
		if (iOS) {
			$('#close-click').hide();
		}else{
			$('#close-click').show();
		}
		if (!document.fullscreenElement &&    // alternative standard method
			!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
			if (document.documentElement.requestFullscreen) {
			document.documentElement.requestFullscreen();
			} else if (document.documentElement.msRequestFullscreen) {
			document.documentElement.msRequestFullscreen();
			} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
			} else if (document.documentElement.webkitRequestFullscreen) {
			document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		} else {
			if (document.exitFullscreen) {
			document.exitFullscreen();
			} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
			} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
			}
		}
		screen.orientation.lock("landscape-primary");
	}

	document.addEventListener('fullscreenchange', exitHandler);
	document.addEventListener('webkitfullscreenchange', exitHandler);
	document.addEventListener('mozfullscreenchange', exitHandler);
	document.addEventListener('MSFullscreenChange', exitHandler);
	function exitHandler() {
		if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
			$('#expand-click div').show();
			$('#download-click').show();
			$('#logo-click').show();
			$('#close-click').hide();
			var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
			if (iOS) {
				$('#close-click').hide();
			}else{
				$('#close-click').hide();
			}
		}
	}

	// multiple backdrop fix
	$(".modal").on("shown.bs.modal", function () {
	    if ($(".modal-backdrop").length > 1) {
	        // $(".modal-backdrop").not(':first').remove();
	        $(".modal-backdrop")[0].remove();
	    };
	});
});

var app = new Vue({
    el: '#cny2021',
    data: {
		loginStatus : 0
    },
    mounted: function () {
		$(this.$refs.vuemodal).on("hidden.bs.modal", this.closeModal);
		if (userId>0) {
			this.loginStatus = 1;
		}
    },
    updated: function () {
		
    },
    methods: {
		goToLink :function(){
			window.location.href = './login';
		},
		closeModal :function(){
			this.loginStatus = 1
		},
		downloadFile : function(){
			window.open("<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/pdf/astro-cny-oxpicious-eguide.pdf");

			jQuery.ajax({
				url: updateUserShareArticlePointUrl,
				type: 'post',
				data: {'source':'download-cny-2021','shareOrDownloadStatus':'downloadEbook'},
				success: function (result) {
					console.log("success");

				},
				error: function () {
					console.log('fail');
				}
			});
		}
    }
});

$num = $('.my-card').length;
// $even = $num / 2;
// $odd = ($num + 1) / 2;
// $offset = 0;
$start = 3;
$currentIndex = 1;

console.log($num*0.5);

$cardContent = [
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-01-01.png"></div>',
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-02-01.png"></div>',
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-03-01.png"></div><div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-03-02.png"></div>',
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-04-01.png"></div>',
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-05-01.png"></div>',
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-06-01.png"></div><div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-06-02.png"></div>',
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-07-01.png"></div>',
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-08-01.png"></div>',
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-09-01.png"></div><div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-09-02.png"></div><div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-09-03.png"></div><div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-09-04.png"></div><div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-09-05.png"></div>',
	'<div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-10-01.png"></div><div class="popup-inner-content"><img src="<?php echo Uri::root(); ?>components/com_quake_club_2021_cny/views/quakeclub2021cnies/tmpl/img/content-10-02.png"></div>'
];
$cardClass = ['countdown', 'cooking', 'feng-shui', 'telemovie', 'variety', 'cgm20', 'movie', 'family', 'radio', 'digital'];

/*
if ($num % 2 == 0) {
	$('.my-card:nth-child(' + $even + ')').addClass('active');
	$('.my-card:nth-child(' + $even + ')').prev().addClass('prev');
	$('.my-card:nth-child(' + $even + ')').next().addClass('next');

	$offset = $('.active').width()*0.5;
	$('.card-carousel').css({left: $offset+'px'});
} else {
	$('.my-card:nth-child(' + $odd + ')').addClass('active');
	$('.my-card:nth-child(' + $odd + ')').prev().addClass('prev');
	$('.my-card:nth-child(' + $odd + ')').next().addClass('next');
}
*/

// start from card #1
$('.my-card:nth-child(' + $start + ')').addClass('active');
$('.my-card:nth-child(' + $start + ')').prev().addClass('prev');
$('.my-card:nth-child(' + $start + ')').next().addClass('next');
$thisWidth = $('.active').width();
$offset = $thisWidth*0.5 + $thisWidth*($num*0.5-$start);
$('.card-carousel').css({left: $offset+'px'});

$('.my-card').click(function(e) {
	var thisCard = e.currentTarget.className.substr(e.currentTarget.className.indexOf("card-"), 7);
	var thisIndex = parseInt(thisCard.substr(5, 2));
	// console.log(e);
	// console.log(thisCard);
	// console.log(thisIndex);

	Swal.fire({
        title: '',
        html: '<div class="content-wrapper"><div class="owl-carousel owl-theme">'+$cardContent[thisIndex-1]+'</div></div><div class="footer-icon-wrapper"><div class="footer-icon-left"></div><div class="footer-icon-right"></div></div>',
        allowEscapeKey : false,
        allowOutsideClick: false,
        showConfirmButton: false,
        showCancelButton: false,
        showCloseButton: true,
        customClass: {
            container: 'card-popup custom-popup-theme ' + $cardClass[thisIndex-1]
        },
        buttonsStyling: false,
        onOpen: function(){
        	// console.log('swal is open');
        	// console.log($('.owl-carousel'));
        	$('.owl-carousel').owlCarousel({
        		loop: false,
			    margin: 10,
			    nav: true,
			    dots: false,
			    responsive: {
			        0: {
			            items:1
			        }
			    }
        	});

        }
    }).then((result) => {
        // console.log('swal is closed');
    });
});

var carouselNavigation = function(e){
	// console.log(e);

	if(slideReady){
		slideReady = false;

		var currentCard = $('.active');
		var allowSlide = false;

		currentCard = navigateStart(e, currentCard);
		allowSlide = true;

		if(!bgmPlaying) playBGM(true);

		if(allowSlide){
			currentCard.removeClass('prev next');
			currentCard.siblings().removeClass('prev active next');
			currentCard.addClass('active');
			currentCard.prev().addClass('prev');
			currentCard.next().addClass('next');
		};

		setTimeout(function(e) {
			// console.log(slideReady);
			slideReady = true;
		}.bind(this), 200);
	};
};

var navigateStart = function(direction, currentCard){
	if(direction == -1){
		var thisDirection = '+=';
	} else {
		thisDirection = '-=';
	};

	if(direction == -1){
		var destination = currentCard.prev();
	} else {
		destination = currentCard.next();
	};

	$slide = $('.active').width();

	// check if destination is dummy
	if(destination.hasClass('dum')){
		// console.log('destination is dummy!');
		var destinationClass = destination[0].classList.value;
		var destinationClass = destinationClass.substr(destinationClass.indexOf('card-'), 7);
		// console.log(destinationClass);

		// look for duplicate target
		var thisWidth = $('.active').width();
		if(destinationClass == 'card-01'){
			var targetCard = $('.my-card.card-10.dum');
			var thisOffset = 1540; //-(thisWidth*0.5 - thisWidth*(4));

			// redefine new destination
			destination = targetCard.next();
		} else {
			targetCard = $('.my-card.card-01.dum');
			thisOffset = -1540; //thisWidth*0.5 + thisWidth*(5);

			// redefine new destination
			destination = targetCard.prev();
		};

		// console.log(thisOffset);
		// console.log('current card: ');
		// console.log(currentCard);
		// console.log('swap to target card: ');
		// console.log(targetCard);

		// stop animation
		$('.my-card').addClass('no-animate');
		// teleport
		$('.card-carousel').css({left: thisOffset+'px'});

		// swap active card
		currentCard.removeClass('active');
		targetCard.addClass('active');
		// swap prev card
		currentCard.prev().removeClass('prev');
		targetCard.prev().addClass('prev');
		// swap next card
		currentCard.next().removeClass('next');;
		targetCard.next().addClass('next');

		// flush
		currentCard[0].offsetHeight;
		currentCard.prev()[0].offsetHeight;
		currentCard.next()[0].offsetHeight;
		targetCard[0].offsetHeight;
		targetCard.prev()[0].offsetHeight;
		targetCard.next()[0].offsetHeight;
		
		// re-activate animation
		$('.my-card').removeClass('no-animate');
	};

	// $slide = $('.active').width();
	$('.card-carousel').stop(false, true).animate({left: thisDirection + $slide}, 200, 'swing', ()=> {
		// console.log('complete!');
		// $('.my-card').removeClass('no-animate');
	});

	// to update current card
	return destination;
}


// Keyboard nav
$('html body').keydown(function(e) {
	if (e.keyCode == 37) { // left
		$('.active').prev().trigger('click');
	}
	else if (e.keyCode == 39) { // right
		$('.active').next().trigger('click');
	}
});
</script>