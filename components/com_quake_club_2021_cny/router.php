<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_2021_cny
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class Quake_club_2021_cnyRouter
 *
 */
class Quake_club_2021_cnyRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = Factory::getApplication()->getParams('com_quake_club_2021_cny');
		$this->noIDs = (bool) $params->get('sef_ids');
		
		$quakeclub2021cnies = new RouterViewConfiguration('quakeclub2021cnies');
		$this->registerView($quakeclub2021cnies);
			$quakeclub2021cny = new RouterViewConfiguration('quakeclub2021cny');
			$quakeclub2021cny->setKey('id')->setParent($quakeclub2021cnies);
			$this->registerView($quakeclub2021cny);
			$quakeclub2021cnyform = new RouterViewConfiguration('quakeclub2021cnyform');
			$quakeclub2021cnyform->setKey('id');
			$this->registerView($quakeclub2021cnyform);

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('Quake_club_2021_cnyRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('Quake_club_2021_cnyHelpersQuake_club_2021_cny', __DIR__ . '/helpers/quake_club_2021_cny.php');
			$this->attachRule(new Quake_club_2021_cnyRulesLegacy($this));
		}
	}


	
		/**
		 * Method to get the segment(s) for an quakeclub2021cny
		 *
		 * @param   string  $id     ID of the quakeclub2021cny to retrieve the segments for
		 * @param   array   $query  The request that is built right now
		 *
		 * @return  array|string  The segments of this item
		 */
		public function getQuakeclub2021cnySegment($id, $query)
		{
			return array((int) $id => $id);
		}
			/**
			 * Method to get the segment(s) for an quakeclub2021cnyform
			 *
			 * @param   string  $id     ID of the quakeclub2021cnyform to retrieve the segments for
			 * @param   array   $query  The request that is built right now
			 *
			 * @return  array|string  The segments of this item
			 */
			public function getQuakeclub2021cnyformSegment($id, $query)
			{
				return $this->getQuakeclub2021cnySegment($id, $query);
			}

	
		/**
		 * Method to get the segment(s) for an quakeclub2021cny
		 *
		 * @param   string  $segment  Segment of the quakeclub2021cny to retrieve the ID for
		 * @param   array   $query    The request that is parsed right now
		 *
		 * @return  mixed   The id of this item or false
		 */
		public function getQuakeclub2021cnyId($segment, $query)
		{
			return (int) $segment;
		}
			/**
			 * Method to get the segment(s) for an quakeclub2021cnyform
			 *
			 * @param   string  $segment  Segment of the quakeclub2021cnyform to retrieve the ID for
			 * @param   array   $query    The request that is parsed right now
			 *
			 * @return  mixed   The id of this item or false
			 */
			public function getQuakeclub2021cnyformId($segment, $query)
			{
				return $this->getQuakeclub2021cnyId($segment, $query);
			}
}
