<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_2021_cny
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Quake_club_2021_cny', JPATH_COMPONENT);
JLoader::register('Quake_club_2021_cnyController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Quake_club_2021_cny');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
