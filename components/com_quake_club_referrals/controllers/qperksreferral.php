<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_referrals
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Qperksreferral controller class.
 *
 * @since  1.6
 */
class Quake_club_referralsControllerQperksreferral extends JControllerLegacy
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	public function edit()
	{
		$app = JFactory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_quake_club_referrals.edit.qperksreferral.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_quake_club_referrals.edit.qperksreferral.id', $editId);

		// Get the model.
		$model = $this->getModel('Qperksreferral', 'Quake_club_referralsModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_quake_club_referrals&view=qperksreferralform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Checking if the user can remove object
		$user = JFactory::getUser();

		if ($user->authorise('core.edit', 'com_quake_club_referrals') || $user->authorise('core.edit.state', 'com_quake_club_referrals'))
		{
			$model = $this->getModel('Qperksreferral', 'Quake_club_referralsModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(JText::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_quake_club_referrals.edit.qperksreferral.id', null);

			// Flush the data from the session.
			$app->setUserState('com_quake_club_referrals.edit.qperksreferral.data', null);

			// Redirect to the list screen.
			$this->setMessage(JText::_('COM_QUAKE_CLUB_REFERRALS_ITEM_SAVED_SUCCESSFULLY'));
			$menu = JFactory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(JRoute::_('index.php?option=com_quake_club_referrals&view=qperksreferrals', false));
			}
			else
			{
                $this->setRedirect(JRoute::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Checking if the user can remove object
		$user = JFactory::getUser();

		if ($user->authorise('core.delete', 'com_quake_club_referrals'))
		{
			$model = $this->getModel('Qperksreferral', 'Quake_club_referralsModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(JText::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_quake_club_referrals.edit.inventory.id', null);
                $app->setUserState('com_quake_club_referrals.edit.inventory.data', null);

                $app->enqueueMessage(JText::_('COM_QUAKE_CLUB_REFERRALS_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(JRoute::_('index.php?option=com_quake_club_referrals&view=qperksreferrals', false));
			}

			// Redirect to the list screen.
			$menu = JFactory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(JRoute::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function show(){
		$db = JFactory::getDBO();
		$jinput = JFactory::getApplication()->input;
		$referral_code = $jinput->get('referral_code', '', 'String');
		$db->setQuery('SELECT `user_id` FROM #__cus_qperks_referrals_session WHERE `referral_code` = '.$db->quote($referral_code) );
		$result = $db->loadResult();

		if ($result) {
			$sent = $this->_sendEmail();
			if ($sent) {
				$this->setRedirect(JRoute::_('index.php?option=com_quake_club_referrals&view=qperksreferral&email=sent', false));
			}
		}else{
			$this->setRedirect(JRoute::_('index.php?option=com_quake_club_referrals&view=qperksreferral&email=fail', false));
		}
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   array     $data                  The data to send in the email.
	 * @param   stdClass  $contact               The user information to send the email to
	 * @param   boolean   $copy_email_activated  True to send a copy of the email to the user.
	 *
	 * @return  boolean  True on success sending the email, false on failure.
	 *
	 * @since   1.6.4
	 */
	private function _sendEmail()
	{
		$config = JFactory::getConfig();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$user   = JFactory::getUser();
		$jinput = JFactory::getApplication()->input;
		$emails = $jinput->get('emails', '', 'String');
		$referral_code = $jinput->get('referral_code', '', 'String');
		$referral_link = "https://quake.com.my/registration?ref=" .$referral_code;

		$emailArray =(explode(",",$emails));
		$emailSubject = "You have just been referred. Sign up for instant rewards!";
		$emailBody = '<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

		<head>
		  <!-- NAME: 1 COLUMN -->
		  <!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
		  <meta charset="UTF-8">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <title>Astro Quake Club - Friend Referral</title>

		  <style type="text/css">
			p {
			  margin: 5px 0;
			  padding: 0;
			}

			table {
			  border-collapse: collapse;
			}

			h1,
			h2,
			h3,
			h4,
			h5,
			h6 {
			  display: block;
			  margin: 0;
			  padding: 0;
			}

			img,
			a img {
			  border: 0;
			  height: auto;
			  outline: none;
			  text-decoration: none;
			}

			body,
			#bodyTable,
			#bodyCell {
			  height: 100%;
			  margin: 0;
			  padding: 0;
			  width: 100%;
			}

			.mcnPreviewText {
			  display: none !important;
			}

			#outlook a {
			  padding: 0;
			}

			img {
			  -ms-interpolation-mode: bicubic;
			}

			table {
			  mso-table-lspace: 0pt;
			  mso-table-rspace: 0pt;
			}

			.ReadMsgBody {
			  width: 100%;
			}

			.ExternalClass {
			  width: 100%;
			}

			p,
			a,
			li,
			td,
			blockquote {
			  mso-line-height-rule: exactly;
			}

			a[href^=tel],
			a[href^=sms] {
			  color: inherit;
			  cursor: default;
			  text-decoration: none;
			}

			p,
			a,
			li,
			td,
			body,
			table,
			blockquote {
			  -ms-text-size-adjust: 100%;
			  -webkit-text-size-adjust: 100%;
			}

			.ExternalClass,
			.ExternalClass p,
			.ExternalClass td,
			.ExternalClass div,
			.ExternalClass span,
			.ExternalClass font {
			  line-height: 100%;
			}

			a[x-apple-data-detectors] {
			  color: inherit !important;
			  text-decoration: none !important;
			  font-size: inherit !important;
			  font-family: inherit !important;
			  font-weight: inherit !important;
			  line-height: inherit !important;
			}

			#bodyCell {
			  padding: 20px;
			}

			.templateContainer {
			  max-width: 600px !important;
			}

			a.mcnButton {
			  display: block;
			}

			.mcnImage,
			.mcnRetinaImage {
			  vertical-align: bottom;
			}

			.mcnTextContent {
			  word-break: break-word;
			}

			.mcnTextContent img {
			  height: auto !important;
			}

			.mcnDividerBlock {
			  table-layout: fixed !important;
			}

			body,
			#bodyTable {

			  background-color: #fafafa;
			}
			#bodyCell {

			  border-top: 0;
			}

			.templateContainer {

			  border: 0;
			}

			h1 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 26px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h2 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 22px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}
			h3 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 20px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h4 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 18px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			#templatePreheader {

			  background-color: #fafafa;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0px;
			}
			#templatePreheader .mcnTextContent,
			#templatePreheader .mcnTextContent p {

			  color: #000000;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 10px;

			  line-height: 150%;

			  text-align: center;
			}

			#templatePreheader .mcnTextContent a,
			#templatePreheader .mcnTextContent p a {

			  color: #000000;

			  font-weight: normal;

			  text-decoration: underline;
			}

			#templateHeader {

			  background-color: #ffffff;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0;
			}


			#templateHeader .mcnTextContent,
			#templateHeader .mcnTextContent p {

			  color: #202020;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 16px;

			  line-height: 150%;

			  text-align: left;
			}


			#templateHeader .mcnTextContent a,
			#templateHeader .mcnTextContent p a {

			  color: #007C89;

			  font-weight: normal;

			  text-decoration: underline;
			}

			#templateBody {

			  background-color: #ffffff;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 2px solid #EAEAEA;

			  padding-top: 20px;

			  padding-bottom: 20px;
			}

			#templateBody .mcnTextContent,
			#templateBody .mcnTextContent p {

			  color: #000000;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 16px;

			  line-height: 150%;

			  text-align: center;
			}

			#templateBody .mcnTextContent a,
			#templateBody .mcnTextContent p a {

			  color: #000000;

			  font-weight: normal;

			  text-decoration: none;
			}

			#templateFooter {

			  background-color: #000000;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0px;
			}

			#templateFooter .mcnTextContent,
			#templateFooter .mcnTextContent p {

			  color: #ffffff;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 9px;

			  line-height: 150%;

			  text-align: center;
			}

			#templateFooter .mcnTextContent a,
			#templateFooter .mcnTextContent p a {

			  color: #ffffff;

			  font-weight: normal;

			  text-decoration: none;
			}

			@media only screen and (min-width:768px) {
			  .templateContainer {
				width: 600px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  body,
			  table,
			  td,
			  p,
			  a,
			  li,
			  blockquote {
				-webkit-text-size-adjust: none !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  body {
				width: 100% !important;
				min-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #bodyCell {
				padding-top: 10px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnRetinaImage {
				max-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImage {
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnCartContainer,
			  .mcnCaptionTopContent,
			  .mcnRecContentContainer,
			  .mcnCaptionBottomContent,
			  .mcnTextContentContainer,
			  .mcnBoxedTextContentContainer,
			  .mcnImageGroupContentContainer,
			  .mcnCaptionLeftTextContentContainer,
			  .mcnCaptionRightTextContentContainer,
			  .mcnCaptionLeftImageContentContainer,
			  .mcnCaptionRightImageContentContainer,
			  .mcnImageCardLeftTextContentContainer,
			  .mcnImageCardRightTextContentContainer,
			  .mcnImageCardLeftImageContentContainer,
			  .mcnImageCardRightImageContentContainer {
				max-width: 100% !important;
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnBoxedTextContentContainer {
				min-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupContent {
				padding: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnCaptionLeftContentOuter .mcnTextContent,
			  .mcnCaptionRightContentOuter .mcnTextContent {
				padding-top: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardTopImageContent,
			  .mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,
			  .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
				padding-top: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardBottomImageContent {
				padding-bottom: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockInner {
				padding-top: 0 !important;
				padding-bottom: 0 !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockOuter {
				padding-top: 9px !important;
				padding-bottom: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnTextContent,
			  .mcnBoxedTextContentColumn {
				padding-right: 18px !important;
				padding-left: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardLeftImageContent,
			  .mcnImageCardRightImageContent {
				padding-right: 18px !important;
				padding-bottom: 0 !important;
				padding-left: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcpreview-image-uploader {
				display: none !important;
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  h1 {

				font-size: 22px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  h2 {

				font-size: 20px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  h3 {

				font-size: 18px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  h4 {

				font-size: 16px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  .mcnBoxedTextContentContainer .mcnTextContent,
			  .mcnBoxedTextContentContainer .mcnTextContent p {

				font-size: 14px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templatePreheader {

				display: block !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templatePreheader .mcnTextContent,
			  #templatePreheader .mcnTextContent p {

				font-size: 10px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templateHeader .mcnTextContent,
			  #templateHeader .mcnTextContent p {

				font-size: 16px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templateBody .mcnTextContent,
			  #templateBody .mcnTextContent p {

				font-size: 14px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {

			  #templateFooter .mcnTextContent,
			  #templateFooter .mcnTextContent p {

				font-size: 9px !important;

				line-height: 150% !important;
			  }

			}
		  </style>
		</head>

		<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fafafa;">
		  <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Guess what? Your friend has just sent you a referral to join Quake Club! </span>
		  <!--<![endif]-->
		  <center>
			<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #fafafa;">
			  <tr>
				<td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 20px;width: 100%;border-top: 0;">
				  <!-- BEGIN TEMPLATE // -->
				  <!--[if (gte mso 9)|(IE)]>
								<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
								<tr>
								<td align="center" valign="top" width="600" style="width:600px;">
								<![endif]-->
				  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
					<tr>
					  <td valign="top" id="templateHeader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnImageBlockOuter">
							<tr>
							  <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
								<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


										<img align="center" alt="" src="http://quake.com.my/images/edm/edm-header.png" width="600" style="max-width: 600px;padding-bottom: 0px;vertical-align: bottom;display: inline !important;border-radius: 0%;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


									  </td>
									</tr>
								  </tbody>
								</table>
							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnImageBlockOuter">
							<tr>
							  <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
								<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


										<img align="center" alt="You have been referred!" src="http://quake.com.my/images/edm/main-referral.jpg?v=2.0" width="600" style="max-width: 600px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


									  </td>
									</tr>
								  </tbody>
								</table>
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
					<tr>
					  <td valign="top" id="templateBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 2px solid #EAEAEA;padding-top: 20px;padding-bottom: 20px;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">

										<p style="font-weight: bold;margin: 20px 0 10px 0;font-size: 20px;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;line-height: 150%;text-align: center;">Guess what?</p>
										<p style="margin: 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Your friend <b>'.$user->name.'</b> has just sent you a referral to join Quake Club – a sharing community exclusively for Marketing Professionals!</p>

										<p style="margin: 20px 0 15px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Kick off your membership now and instantly receive 100 Q-Perks. Members can share knowledge with peers and earn points, or better known as Q-Perks.</p>

										<!--<div style="margin: 30px 0 30px 0; ">
										  <p style="font-weight: bold;color: #EC008C;margin: 0 0 5px 0;font-size: 18px;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica, Arial, sans-serif;line-height: 150%;text-align: center;">What is Quake Club?</p>
										  <p style="margin: 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Quake Club is an exclusive rewards programme for advertising and <span style="display:inline-block; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;">marketing professionals</span>.</p>
										</div>-->

										<a href="'.$referral_link.'" style="display: block; margin: 20px 0 10px 0; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img height="54" alt="Click here to sign up now" src="http://quake.com.my/images/edm/btn-referral.png" style="border: 0px;width: 225px;height: 54px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="225"></a>

										<p style="margin: 5px 0 50px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 12px;line-height: 150%;text-align: center;">
											By proceeding to sign up, you are required to fulfill the criteria set by <span style="display:inline-block; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 12px;line-height: 150%;">Quake Club</span>.
										</p>

										<!--<p style="margin: 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Happy collecting!</p>-->

										<!--<p style="font-size: 12px;margin: 50px 0 0 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;line-height: 150%;text-align: center;">If you have any questions in relation to this redemption, or the item(s) it relates to,<br> please email us at <a href="mailto:quakeclub@astro.com.my" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-weight: normal;text-decoration: underline;">quakeclub@astro.com.my</a></p>-->

									  </td>
									</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
					<tr>
					  <td valign="top" id="templateFooter" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #000000;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0px;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="padding-top: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #ffffff;font-family: Helvetica, Arial, sans-serif;font-size: 9px;line-height: 150%;text-align: center;">

										<span style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #ffffff;font-family: Helvetica, Arial, sans-serif;font-size: 9px;line-height: 150%;text-align: center;">You have received this email because you have confirmed that you would like to receive email communication from Quake Club.<br> We will never share your personal information (such as your email address with any other third
										parties without your consent).<br></span>
										<br>
										<img height="71" alt="Astro Quake" src="http://quake.com.my/images/edm/footer-quake.png" style="border: 0px;width: 53px;height: 71px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="53">
									  </td>
									</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnFollowBlockOuter">
							<tr>
							  <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowBlockInner">
								<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td align="center" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
										  <tbody>
											<tr>
											  <td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
												<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
												  <tbody>
													<tr>
													  <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														<!--[if mso]>
											<table align="center" border="0" cellspacing="0" cellpadding="0">
											<tr>
											<![endif]-->

														<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->


														<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														  <tbody>
															<tr>
															  <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																  <tbody>
																	<tr>
																	  <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		  <tbody>
																			<tr>

																			  <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																				<a href="https://www.facebook.com/QuakeMY" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-fb.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
																			  </td>


																			</tr>
																		  </tbody>
																		</table>
																	  </td>
																	</tr>
																  </tbody>
																</table>
															  </td>
															</tr>
														  </tbody>
														</table>

														<!--[if mso]>
												</td>
												<![endif]-->

														<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->


														<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														  <tbody>
															<tr>
															  <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																  <tbody>
																	<tr>
																	  <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		  <tbody>
																			<tr>

																			  <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																				<a href="https://api.whatsapp.com/send?phone=60126040968&text=Hi,%20send%20me%20the%20monthly%20issue%20of%20Winning%20Partnership%20Series%20by%20MARKETING" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-whatsapp.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
																			  </td>


																			</tr>
																		  </tbody>
																		</table>
																	  </td>
																	</tr>
																  </tbody>
																</table>
															  </td>
															</tr>
														  </tbody>
														</table>

														<!--[if mso]>
												</td>
												<![endif]-->

														<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->


														<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														  <tbody>
															<tr>
															  <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																  <tbody>
																	<tr>
																	  <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		  <tbody>
																			<tr>

																			  <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																				<a href="http://quake.com.my/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-web.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
																			  </td>


																			</tr>
																		  </tbody>
																		</table>
																	  </td>
																	</tr>
																  </tbody>
																</table>
															  </td>
															</tr>
														  </tbody>
														</table>

														<!--[if mso]>
												</td>
												<![endif]-->

														<!--[if mso]>
											</tr>
											</table>
											<![endif]-->
													  </td>
													</tr>
												  </tbody>
												</table>
											  </td>
											</tr>
										  </tbody>
										</table>
									  </td>
									</tr>
								  </tbody>
								</table>

							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #ffffff;font-family: Helvetica, Arial, sans-serif;font-size: 9px;line-height: 150%;text-align: center;">

										<!-- <span style="font-size:12px"><a href="#" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #ffffff;font-weight: normal;text-decoration: none;">Unsubscribe </a>| <a href="http://quake.com.my/user-profile/profile" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #ffffff;font-weight: normal;text-decoration: none;">Update Profile</a></span><br> -->
										<br> © Astro Quake Club. All Right Reserved
									  </td>
									</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
				  </table>
				  <!--[if (gte mso 9)|(IE)]>
								</td>
								</tr>
								</table>
								<![endif]-->
				  <!-- // END TEMPLATE -->
				</td>
			  </tr>
			</table>
		  </center>
		</body>

		</html>
		';

		$to = $emailArray;
		$from = array($data['mailfrom'], $data['fromname']);

		# Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject($emailSubject);
		$mailer->setBody($emailBody);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();

		return $mailer->send();
	}
}
