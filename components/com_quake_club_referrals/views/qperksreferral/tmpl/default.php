<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_referrals
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$emailStatus = JRequest::getVar('email')==""?"":JRequest::getVar('email');
$user = JFactory::getUser();
$db = JFactory::getDBO();
$db->setQuery('SELECT `referral_code` FROM #__cus_qperks_referrals_session WHERE `user_id` = '.$user->id  );
$referral_code = $db->loadResult();

if ($referral_code == "") {
	do {
		$ran_referral_code = JUserHelper::genRandomPassword();

		$db->setQuery('SELECT referral_code FROM #__cus_qperks_referrals_session WHERE referral_code = "'.$ran_referral_code.'"' );
		$check_referral_code = $db->loadResult();

		$referral_code = $ran_referral_code;
	} while ($check_referral_code != "");


	$referral = new stdClass();
	$referral->user_id = $user->id;
	$referral->time=time();
	$referral->referral_code=$referral_code;
	$result = JFactory::getDbo()->insertObject('#__cus_qperks_referrals_session',$referral);
}

$db->setQuery('SELECT * FROM #__cus_qperks_referrals_session WHERE user_id = '.$user->id );
$user_referral = $db->loadRow();
$db->setQuery('SELECT count(id) FROM #__cus_qperks_referrals WHERE `referrer_id` = '.$user->id  );
$referrer_num = $db->loadResult();
$referral_link = "http://quake.com.my/registration?ref=".$user_referral[0] ;

// $db->setQuery('SELECT (month01+month02+month03+month04+month05+month06+month07+month07+month08+month09+month10+month11+month12) FROM #__cus_quake_club_qperks_monthly WHERE `user_id` = '.$user->id  );
// $qperks_num = $db->loadResult();
$db->setQuery('SELECT sum(point) FROM #__cus_qperks_user_point WHERE type = 2 and `user_id` = '.$user->id  );
$qperks_num = $db->loadResult();
?>

<div id="app">
	<div class="pink-page-title">
		<div class="container">
			<h2>Friend Referral</h2>
		</div>
	</div>

	<div class="rounded-card bg-yellow referral-overview">
		<div class="row">
			<div class="col-md-4">
				<div class="card-body">
					<span>Total earnings</span>
					<h3><?php echo $qperks_num!=""?$qperks_num:0;?> Q-Perks</h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card-body">
					<span>People referred</span>
					<h3><?php echo $user_referral[3]; ?></h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card-body">
					<span>Successful referees</span>
					<h3><?php echo $referrer_num;?> / 10</h3>
				</div>
			</div>
		</div>
	</div>

	<div>
		<h2 class="f-24">Get 50 Q-Perks when you refer a friend</h2>
		<p>Share your unique link and earn 50 Q-Perks for every friend who successfully joins Quake Club. </p>
	</div>

	<div class="rounded-card bg-grey" id="referral">
		<ul class="nav nav-pills justify-content-center">
		  <li class="nav-item">
		    <div class="nav-link" :class="{active:showStep==1}" @click="showStep=1"><i class="fas fa-link d-md-none"></i><span class="d-none d-md-block">Link</span></div>
		  </li>
		  <li class="nav-item">
		    <div class="nav-link" :class="{active:showStep==2}" @click="showStep=2"><i class="fab fa-facebook-f d-md-none"></i><span class="d-none d-md-block">Facebook</span></div>
		  </li>
		  <li class="nav-item">
		    <div class="nav-link" :class="{active:showStep==3}" @click="showStep=3"><i class="fas fa-envelope d-md-none"></i><span class="d-none d-md-block">Email</span></div>
		  </li>
		</ul>

		<div class="card-body">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<div class="referral-content" v-show="showStep==1">
						<p>Share your unique link and let your friends know that they can get 100 Q-Perks instantly when they sign up. </p>
						<p><b>Share it anywhere:</b></p>
						<div class="position-relative">
							<div class="clipboard"><?php echo $referral_link; ?></div>
							<textarea style="position: absolute; visibility: hidden;" rows="1" id="p1"><?php echo $referral_link; ?></textarea>

						</div>

						<div class="referral-cta"><button @click="copyToClipboard('#p1')" class="btn btn-pink position-relative">
							<div class="copy-alert" v-if="textCopied == 1"><i class="fas fa-check"></i> Link copied</div>
							Copy link
						</button></div>

					</div>
					<div class="referral-content" v-show="showStep==2">
						<p>Share on Facebook and let your friends know that they can get 100 Q-Perks instantly when they sign up. </p>
						<img class="my-4" src="./images/club/quake-club.png" width="240" alt="Quake Club">
						<div class="referral-cta"><a class="btn btn-pink" onclick="window.open(this.href, 'newwindow', 'width=500,height=600,top=200, left=500'); return false;" href="http://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=<?php echo $referral_link; ?>" rel="nofollow">Share</a></div>
					</div>
					<div class="referral-content" v-show="showStep==3">
						<form action="<?php echo JRoute::_('index.php?option=com_quake_club_referrals&task=qperksreferral.show'); ?>" method="post">
							<p>Share your unique link and let your friends know that they can get 100 Q-Perks instantly when they sign up. </p>

							<input class="form-control" name="emails" type="text" placeholder="Enter friend’s email (max. 5, separated by commas)">
							<input name="referral_code" type="hidden" value="<?php echo $user_referral[0]; ?>">
							<div class="referral-cta"><button class=" btn btn-pink">Send email now</button></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<style>
body > textarea {
	position: fixed;
	opacity: 0
}
</style>

<script type="text/javascript">

	// function copyToClipboard(element) {
	// 	var $temp = jQuery("<input>");
	// 	jQuery("body").append($temp);
	// 	$temp.val(jQuery(element).text()).select();
	// 	console.log($temp);

	// 	document.execCommand("copy");
	// 	$temp.remove();
	// }

	var emailStatus = <?php echo json_encode($emailStatus); ?>;

    var app = new Vue({
        el: '#app',
        data: {
					showStep : 1,
					textCopied : 0
        },
        mounted: function () {
			if (emailStatus == 'sent') {
				this.showStep = 3;
			}
        },
        updated: function () {
        },
        methods: {
					copyToClipboard: function(element){
						this.textCopied=1;
						this.setTimer();
						var textArea;
						if (navigator.userAgent.match(/ipad|iphone/i)) {
							textArea = document.createElement('textArea');
							textArea.value = jQuery(element).text();
							document.body.appendChild(textArea);
							range = document.createRange();
							range.selectNodeContents(textArea);
							selection = window.getSelection();
							selection.removeAllRanges();
							selection.addRange(range);
							textArea.setSelectionRange(0, 999999);
							document.execCommand("copy");
							document.body.removeChild(textArea);
							// $temp.remove();
						}else{
							var $temp = jQuery("<input>");
							// console.log($temp.val());

							jQuery("body").append($temp);
							$temp.val(jQuery(element).text()).select();
							document.execCommand("copy");
							// document.body.removeChild(textArea);
							$temp.remove();
						}




					},
					setTimer: function(){
						_this = this;
						setTimeout(function(){ _this.textCopied=0; }, 3000);
					}
        }
    });

</script>
