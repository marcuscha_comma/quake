<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_game_records
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Quakeclubgamerecords list controller class.
 *
 * @since  1.6
 */
class Quake_club_game_recordsControllerQuakeclubgamerecords extends Quake_club_game_recordsController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Quakeclubgamerecords', $prefix = 'Quake_club_game_recordsModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * $email
	 * $password
	 */
	public function eventGameLogin(){
		
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		
		$jinput = JFactory::getApplication()->input;
		$checkEmail = $jinput->get('email','', 'String');
		$email = str_replace(' ', '', $checkEmail);
		$password = $jinput->get('password','', 'String');
		$gxfc = $jinput->get('gxfc','', 'String');
		$guestUniqueKey = $_COOKIE['_guk']?$_COOKIE['_guk']:0;

		// $email = "midoff1@gmail.com";
		// $password = "comma5157";

		if (!$email || !$password) {
			$result = array(
				'user_name' => 0,
				'user_id' => 0,
				'play_times_count' => 0,
				'play_times_earn' => 0,
				'qcoins' => 0
			);
			echo new JResponseJson($result);
			JFactory::getApplication()->close();
		}

		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jdate = new JDate;
		$date = new DateTime();

		$db->setQuery('SELECT id,password,name FROM #__users where email ="'.$email .'"');
		$user             = $db->loadRow();
			
		$passwordMatch = JUserHelper::verifyPassword($password, $user[1], $user[0]);
	
		if ($passwordMatch) {

			$credentials = array();
			$credentials['username']  = $email;
			$credentials['password']  = $password;
			$credentials['secretkey'] = "";
			if (true !== $app->login($credentials, $options))
			{
				// Login failed !
				// Clear user name, password and secret key before sending the login form back to the user.
				$data['remember'] = (int) $options['remember'];
				$data['username'] = '';
				$data['password'] = '';
				$data['secretkey'] = '';
				$app->setUserState('users.login.form.data', $data);
			}

			$db->setQuery('SELECT play_times_count,play_times_earn,qcoins,play_times_used,qcoins_after_redemption FROM #__cus_quake_club_game_records where user_id ="'.$user[0] .'"');
			$record             = $db->loadRow();

			$db->setQuery('SELECT id,qcoins FROM #__cus_quake_club_game_points where guest_unique_key = "'.$guestUniqueKey.'"');
			$checkGamePoints            = $db->loadRow();

			if (!$record) {
				$db->setQuery('SELECT MAX(ordering) FROM #__cus_quake_club_game_records');
				$max             = $db->loadResult();
				$gameRecord = new stdClass();
				$gameRecord->user_id = $user[0];
				$gameRecord->modified_by = $user[0];
				$gameRecord->created_by = $user[0];
				$gameRecord->state=1;
				$gameRecord->ordering=$max + 1;
				$gameRecord->created_on=$jdate->toSql(true);
				$gameRecord->qcoins = $checkGamePoints ? $checkGamePoints[2]:0;
				$gameRecord->qcoins_after_redemption = $checkGamePoints ? $checkGamePoints[2]:0;
				

				JFactory::getDbo()->insertObject('#__cus_quake_club_game_records', $gameRecord);

				$record[0] = 1;
				$record[1] = 2;
				$record[2] = 0;
				$record[3] = 0;
			}

			if ($gxfc == 'true') {
				$result = array(
					'user_name' => $user[2],
					'user_id' => $user[0],
					'play_times_count' => $record[0],
					'play_times_earn' => $record[1],
					'qcoins' => $record[2]
				);
				echo new JResponseJson($result);
				JFactory::getApplication()->close();
			}

			if ($record[2]<388) {
				if ($record[0]>0) {
					
					if ( $checkGamePoints  ) {

						$gamePoints = new stdClass();
						$gamePoints->user_id = $user[0];
						$gamePoints->id = $checkGamePoints[0];
						$gamePoints->week_num=date("W");
		
						JFactory::getDbo()->updateObject('#__cus_quake_club_game_points', $gamePoints, 'id');
						
					}else{
						$gamePoints = new stdClass();
						$gamePoints->user_id = $user[0];
						$gamePoints->qcoins=0;
						$gamePoints->guest_unique_key=$guestUniqueKey == 0?($user[0].'_'.$this->generateRandomString()):$guestUniqueKey;
						$gamePoints->created_on=$jdate->toSql(true);
						$gamePoints->week_num=date("W");
		
						JFactory::getDbo()->insertObject('#__cus_quake_club_game_points', $gamePoints);
					}
				
					$gameRecord = new stdClass();
					$gameRecord->user_id = $user[0];
					$gameRecord->play_times_count = $record[0] -1;
					$gameRecord->play_times_used = $record[3] +1;
					$gameRecord->qcoins = $record[2] + $checkGamePoints[1];
					$gameRecord->qcoins_after_redemption = $record[2] + $checkGamePoints[1];
	
					JFactory::getDbo()->updateObject('#__cus_quake_club_game_records', $gameRecord, 'user_id');
				}else{
					$gameRecord->play_times_count = $record[0];
					$gameRecord->qcoins = $record[2];
				}
			}else{
				$gameRecord->play_times_count = $record[0];
				$gameRecord->qcoins = $record[2];
			}

			$result = array(
				'user_name' => $user[2],
				'user_id' => $user[0],
				'play_times_count' => $gameRecord->play_times_count,
				'play_times_earn' => $record[1],
				'qcoins' => $gameRecord->qcoins
			);
			echo new JResponseJson($result);
	
		}else{
			$result = array(
				'user_name' => 0,
				'user_id' => 0,
				'play_times_count' => 0,
				'play_times_earn' => 0,
				'qcoins' => 0
			);
			echo new JResponseJson($result);
		}
		JFactory::getApplication()->close();
	}

	public function getRandomQuestion(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		JResponse::setHeader('Content-Type','text/html; charset=utf-8');

		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		$db->setQuery('SELECT id,question,answer_1,answer_2 FROM #__cus_qperk_event_questions ORDER BY RAND() LIMIT 1');
		
		$question           = $db->loadRow();

		$result = array(
			'question_id' => $question[0],
			'question' => $question[1],
			'answer_1' => $question[2],
			'answer_2' => $question[3]
			// 'correct_answer' => $question[4]
		);

		echo new JResponseJson($result);
		JFactory::getApplication()->close();
	}

	public function checkRandomQuestion(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		JResponse::setHeader('Content-Type','text/html; charset=utf-8');

		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jinput = JFactory::getApplication()->input;
		$question_id = $jinput->get('question_id','', 'String');		
		$correct_answer = $jinput->get('correct_answer','', 'String');

		$db->setQuery('SELECT correct_answer FROM #__cus_qperk_event_questions where id ='.$question_id);
		
		$question_answer           = $db->loadResult();

		if ($correct_answer == $question_answer) {
			$result = array(
				'result' => "Correct"
			);
		}else{
			$result = array(
				'result' => "Wrong"
			);
		}

		echo new JResponseJson($result);
		JFactory::getApplication()->close();
	}

	/**
	 * $user_id
	 * $point
	 */
	public function updateEventGamePoint(){

		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$jinput = JFactory::getApplication()->input;
		$user_id = $jinput->get('user_id','', 'String');		
		$point = $jinput->get('point','', 'String');
		$game_log = $jinput->get('game_log','', 'String');

		// var_dump($user_id );
		// die();
		// $user_id = $user_id?$user_id:0;
		// $user_id = 1566;
		// $point = 180;

		if (!$point) {
			$result = array(
				'user_name' => 0,
				'user_id' => 0,
				'play_times_count' => 0,
				'play_times_earn' => 0,
				'qcoins' => 0
			);
			echo new JResponseJson($result);
			JFactory::getApplication()->close();
		}

		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jdate = new JDate;
		$date = new DateTime();
		$cookie_value = $user_id.'_'.$this->generateRandomString();
		$cookie_name = "_guk";

		if($_COOKIE['_guk'] || $_COOKIE['_guk'] != 0){
			$guestUniqueKey = $_COOKIE['_guk'];
		}else{
			setcookie($cookie_name, $cookie_value, time() + (3600*2), "/");
			$guestUniqueKey = $cookie_value;
		}

		if ($user_id == 0) {
			
			$this->updateGameRecords($user_id, $point, $guestUniqueKey,$game_log);

			$result = array(
				'user_name' => 0,
				'user_id' => $user_id,
				'play_times_count' => 0,
				'play_times_earn' => 0,
				'qcoins' => $point
			);
			echo new JResponseJson($result);
			
		}else{

			$db->setQuery('SELECT r.play_times_count,r.play_times_earn,r.play_times_used,r.qcoins,u.name FROM #__cus_quake_club_game_records r join #__users u on u.id = r.user_id where user_id = "'.$user_id.'"');
			$checkGameRecord           = $db->loadRow();

			$db->setQuery('SELECT qcoins FROM #__cus_quake_club_game_points where guest_unique_key = "'.$guestUniqueKey.'"');
			$checkGamePoints          = $db->loadRow();
			
			if ($checkGameRecord) {
				if ($checkGamePoints) {
					$gamePoints = new stdClass();
					$gamePoints->user_id = $user_id;
					$gamePoints->qcoins=$point;
					$gamePoints->guest_unique_key=$guestUniqueKey;
					$gamePoints->week_num=date("W");
					$gamePoints->created_on=$jdate->toSql(true);
					$gamePoints->game_log=$game_log;

					JFactory::getDbo()->updateObject('#__cus_quake_club_game_points', $gamePoints, 'guest_unique_key');

					// $this->updateGameRecords($user_id, $point, $guestUniqueKey);

					$gameRecord = new stdClass();
					$gameRecord->user_id = $user_id;
					$gameRecord->play_times_count = $checkGameRecord[0];
					$gameRecord->play_times_used = $checkGameRecord[2];
					$gameRecord->qcoins = $checkGameRecord[3] +$point;
					$gameRecord->qcoins_after_redemption = $checkGameRecord[3] +$point;
					$gamePoints->week_num=date("W");

					JFactory::getDbo()->updateObject('#__cus_quake_club_game_records', $gameRecord, 'user_id');

					$result = array(
						'user_name' => $checkGameRecord[4],
						'user_id' => $user_id,
						'play_times_count' => $gameRecord->play_times_count,
						'play_times_earn' => $checkGameRecord[1],
						'qcoins' => $gameRecord->qcoins
					);
					echo new JResponseJson($result);
				}else{
					$result = array(
						'user_name' => $checkGameRecord[4],
						'user_id' => $user_id,
						'play_times_count' => $checkGameRecord[0],
						'play_times_earn' => $checkGameRecord[1],
						'qcoins' => 0
					);
					echo new JResponseJson($result);
				}
				
			}else{
				$result = array(
					'user_name' => $checkGameRecord[4],
					'user_id' => $user_id,
					'play_times_count' => $checkGameRecord[0],
					'play_times_earn' => $checkGameRecord[1],
					'qcoins' => $checkGameRecord[3]
				);
				echo new JResponseJson($result);
			}
			
		}
		JFactory::getApplication()->close();
	}

	public function updateUserPlayTimes(){

		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jinput = JFactory::getApplication()->input;
		$user_id = $jinput->get('user_id','', 'String');
		$jdate = new JDate;	
		// $user_id =1566;	

		if ($user_id == 0) {
			$result = array(
				'result' => FALSE,
				'play_times_earn' => 0
			);
			echo new JResponseJson($result);
			JFactory::getApplication()->close();

		}

		$cookie_value = $user_id.'_'.$this->generateRandomString();
		$cookie_name = "_guk";
		setcookie($cookie_name, $cookie_value, time() + (3600*2), "/");
		$guestUniqueKey = $cookie_value;

		$db->setQuery('SELECT play_times_count,play_times_earn,play_times_used,qcoins FROM #__cus_quake_club_game_records where user_id = "'.$user_id.'"');
		$checkGameRecord           = $db->loadRow();

		

		$db->setQuery('SELECT name FROM #__users where id = "'.$user_id.'"');
		$user_name           = $db->loadResult();

		if ($checkGameRecord && $checkGameRecord[0]>0) {

			$gameRecord = new stdClass();
			$gameRecord->user_id = $user_id;
			$gameRecord->play_times_count = $checkGameRecord[0] -1;
			$gameRecord->play_times_used = $checkGameRecord[2] +1;

			JFactory::getDbo()->updateObject('#__cus_quake_club_game_records', $gameRecord, 'user_id');

			$gamePoints = new stdClass();
			$gamePoints->user_id = $user_id;
			$gamePoints->guest_unique_key=$guestUniqueKey;
			$gamePoints->week_num=date("W");
			$gamePoints->created_on=$jdate->toSql(true);

			JFactory::getDbo()->insertObject('#__cus_quake_club_game_points', $gamePoints);

			$result = array(
				'result' => TRUE,
				'play_times_earn' => $checkGameRecord[1]

			);
			echo new JResponseJson($result);
			
		}else{
			$result = array(
				'result' => FALSE,
				'play_times_earn' => 0
			);
			echo new JResponseJson($result);
		}
		JFactory::getApplication()->close();
	}

	public function checkUserPlayTimes(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jinput = JFactory::getApplication()->input;
		$user_id = $jinput->get('user_id','', 'String');

		if ($user_id == 0) {
			$result = array(
				'result' => FALSE,
				'play_times_earn' => 0
			);
			echo new JResponseJson($result);
			JFactory::getApplication()->close();

		}

		$db->setQuery('SELECT play_times_count,play_times_earn,play_times_used,qcoins FROM #__cus_quake_club_game_records where user_id = "'.$user_id.'"');
		$checkGameRecord           = $db->loadRow();
		if ($checkGameRecord && $checkGameRecord[0]>0) {
			$result = array(
				'result' => TRUE,
				'play_times_earn' => $checkGameRecord[1]
			);
		}else{
			$result = array(
				'result' => FALSE,
				'play_times_earn' => $checkGameRecord[1]
			);
		}
		echo new JResponseJson($result);
		JFactory::getApplication()->close();
	}

	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	function updateGameRecords($user_id, $point, $guestUniqueKey, $game_log){

		$db    = JFactory::getDBO();
		$jdate = new JDate;

		$db->setQuery('SELECT id FROM #__cus_quake_club_game_points where guest_unique_key = "'.$guestUniqueKey.'" and user_id ="'.$user_id.'"');
		$checkGamePoints            = $db->loadResult();

		if ( $checkGamePoints ) {
			$gamePoints = new stdClass();
			$gamePoints->user_id = $user_id;
			$gamePoints->qcoins=$point;
			$gamePoints->id = $checkGamePoints;
			$gamePoints->week_num=date("W");
			$gamePoints->game_log=$game_log;

			JFactory::getDbo()->updateObject('#__cus_quake_club_game_points', $gamePoints, 'id');
		}else{

			$gamePoints = new stdClass();
			$gamePoints->user_id = $user_id;
			$gamePoints->qcoins=$point;
			$gamePoints->guest_unique_key=$guestUniqueKey;
			$gamePoints->week_num=date("W");
			$gamePoints->created_on=$jdate->toSql(true);
			$gamePoints->game_log=$game_log;

			JFactory::getDbo()->insertObject('#__cus_quake_club_game_points', $gamePoints);
		}

	}

	public function initialGameRecord(){
		echo "yes";
		// $db    = JFactory::getDBO();
		// $query = $db->getQuery(true);
		// $db->setQuery('SELECT id FROM #__cus_quake_club_game_records');
		// $gameRecordIds           = $db->loadAssocList();

		// foreach ($gameRecordIds as $key => $value) {
		// 	$gameRecord = new stdClass();
		// 	$gameRecord->id = $value['id'];
		// 	$gameRecord->play_times_count = 0;
		// 	$gameRecord->play_times_used = 3;
		// 	$gameRecord->play_times_earn = 0;
		// 	JFactory::getDbo()->updateObject('#__cus_quake_club_game_records', $gameRecord, 'id');
		// }
		

	}

}
