<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_game_records
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('bootstrap.tooltip');
HTMLHelper::_('behavior.multiselect');
HTMLHelper::_('formbehavior.chosen', 'select');

$user       = Factory::getUser();
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_quake_club_game_records') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'quakeclubgamerecordform.xml');
$canEdit    = $user->authorise('core.edit', 'com_quake_club_game_records') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'quakeclubgamerecordform.xml');
$canCheckin = $user->authorise('core.manage', 'com_quake_club_game_records');
$canChange  = $user->authorise('core.edit.state', 'com_quake_club_game_records');
$canDelete  = $user->authorise('core.delete', 'com_quake_club_game_records');

$userId     = $user->get('id');
$name = $user->get('name');
$totalQcoins = $this->item->qcoins;
$playTimesCount = $this->item->play_times_count;
$playTimesEarn = $this->item->play_times_earn;
$baseUrl = JURI::base();

// check if user exist
if(!$userId){
    $name = 'Guest';
    $totalQcoins = 0;
    $playTimesCount = 1;
    $playTimesEarn = 0;
};

// check if coin exist
if(!$totalQcoins){
    // $totalQcoins = 0;
};


// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_quake_club_game_records/css/list.css');
// kent custom
$document->addStyleSheet(Uri::root() . 'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/css/sweetalert2.min.css');
$document->addStyleSheet(Uri::root() . 'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/css/style.css');
// $document->addScript(Uri::root() . 'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/bower_components/jquery/dist/jquery.min.js', 'text/javascript');
$document->addScript(Uri::root() . 'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/js/lib/sweetalert2/sweetalert2.min.js', 'text/javascript');
$document->addScript(Uri::root() . 'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/js/lib/phaser/phaser.min.js', 'text/javascript');
$document->addScript(Uri::root() . 'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/js/preloader.js', 'text/javascript');
$document->addScript(Uri::root() . 'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/js/menuscene.js', 'text/javascript');
$document->addScript(Uri::root() . 'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/js/gamescene.js', 'text/javascript');
$document->addScript(Uri::root() . 'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/js/soundmanager.js', 'text/javascript');

?>
<!--
<div id="app">
    <button @click="actionClick()">miao</button>
</div>
-->

<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="game-wrapper">
    <h1 class="boldFont-test">test</h1>
    
    <div class="countdown disable"><h1>3</h1></div>
    
    <div id="gameDiv">
        <div class="mute-btn-wrapper">
            <a href="javascript:toggleMute();" class="mute-btn">mute</a>
        </div>
    </div>
    <a href=""></a>
</div>

<script type="text/javascript">
/*
    var app = new Vue({
        el: '#app',
        data: {
        },
        mounted: function () {
        },
        updated: function () {
        },
        methods: {
            actionClick :function(link){
                
                jQuery.ajax({
                    url: 'http://43.228.245.193/~cheeliem/astro-joomla/mouse-test',
                    type: 'post',
                    data: {
                        'userId': 'midoff1@gmail.com',
                        'password': 'comma5157',
                    },
                    success: function (result) {},
                    error: function () {
                        console.log('fail');
                    }
                });
            }
        }
    })*/
    jQuery(document).ready(function() {
        console.log('document ready!');

        window.hasGame = false;
        window.gameAvailable = false;
        if(localStorage.hasOwnProperty('mute')){
            var isMute = JSON.parse(localStorage.getItem('mute'));
        } else {
            // console.log('isMute unset!!!');
            // is not mute by default
            isMute = false;
            localStorage.setItem('mute', isMute);
        };
        // console.log('isMute: ');
        // console.log(isMute);

        window.setTimeout(
            (function(){
                // check if is previously muted
                // console.log('isMute: ' + isMute);
                // button state
                if(isMute){
                    jQuery('.mute-btn').addClass('mute');
                } else {
                    jQuery('.mute-btn').removeClass('mute');
                };
                
                var screenWidth = 1100; //window.innerWidth; 
                var screenHeight = 700; //window.innerHeight; // jQuery('#gameDiv').height(); // window.innerHeight; //700;
                /*console.log('screenWidth: ' + screenWidth + ', screenHeight: ' + screenHeight);
                console.log('innerWidth: ' + window.innerWidth + ', innerHeight: ' + window.innerHeight);
                console.log(jQuery(document).height() + ', ' + jQuery(window).height());*/

                jQuery('#gameDiv').height(screenHeight);

                window.isMobile = !!navigator.userAgent.match(/iPad|iPhone|iPod|Opera Mini|IEMobile|Android|BlackBerry|Windows Phone|webOS/i);
                window.iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
                window.iphone_x = false;
                window.android = (navigator.userAgent.match(/Android/i) ? true : false);
                var fullscreen = false;
                var iOS_screenWidth;
                var iOS_screenHeight;
                var orientation = '';

                var game_idealWidth = 1100;
                var game_idealHeight = 700;

                if(isMobile){
                    jQuery('body').addClass('isMobile');
                    if(window.iOS){
                        jQuery('body').addClass('is_iOS');
                    };
                } else {
                    jQuery('body').addClass('isDesktop');
                    jQuery('.game-wrapper').height(window.innerHeight);
                };

                console.log("is iOS: " + iOS + ", is android: " + android);
                // window.alert("isMobile: " + isMobile + ", is iOS: " + iOS + ", is android: " + android);

                if(iOS){
                    // window.addEventListener("load", handleWindowLoad, false);
                    window.addEventListener("orientationchange", handleOrientationChange, false);
                }else{
                    if(isMobile){
                        window.addEventListener("resize", handleResize, false);
                    };  
                };

                if(isMobile){
                    handleWindowLoad();
                } else {
                    console.log('is not mobile');
                    if(!window.hasGame){
                        window.hasGame = true;
                        // var game = new Phaser.Game(screenWidth, screenHeight, Phaser.CANVAS, 'gameDiv');
                        
                        var config = {
                            type: Phaser.AUTO,
                            width: screenWidth,
                            height: screenHeight,
                            parent: 'gameDiv',
                            backgroundColor: '#000000',
                            physics: {
                                    default: 'arcade',
                                    arcade: {
                                        gravity: 0
                                        // ,debug: true
                                    }
                                },
                            scale: {
                                mode: Phaser.Scale.FIT,
                                autoCenter: Phaser.Scale.CENTER_BOTH
                            },
                            // scene: [ Preloader, MainMenu, GameScene ]
                            scene: [ Preloader, GameScene ]
                        };

                        var game = new Phaser.Game(config);
                        initListener(game);
                    };
                };

                function handleWindowLoad() { // check orientation
                    if(iOS){
                        iOS_orientationCheck();
                    }else{
                        handleResize();
                    };
                };

                function handleOrientationChange() { // for iOS
                    iOS_orientationCheck();
                }

                function iOS_orientationCheck(){
                    // window.alert('iOS_orientationCheck: ' + window.orientation);
                    if (Math.abs(window.orientation) === 90) {
                        // Landscape
                        iOS_screenWidth = window.screen.height;
                        iOS_screenHeight = window.screen.width;
                        orientation = 'landscape';
                    } else {
                        // Portrait
                        iOS_screenWidth = window.screen.width;
                        iOS_screenHeight = window.screen.height;
                        orientation = 'portrait';
                    };

                    if((window.screen.width == 375 && window.screen.height == 812) || (window.screen.width == 812 && window.screen.height == 375) || (window.screen.width == 414 && window.screen.height == 896) || (window.screen.width == 896 && window.screen.height == 414)){
                        window.iphone_x = true;
                        jQuery('body').addClass('iphone-x');
                        console.log('is iphone x');
                    };

                    if(orientation == 'landscape'){
                        jQuery('#container').removeClass('portrait');
                        jQuery('body').removeClass('portrait');
                    } else {
                        jQuery('#container').removeClass('landscape');
                        jQuery('body').removeClass('landscape');
                    };
                    jQuery('#container').addClass(orientation);
                    jQuery('body').addClass(orientation);

                    window.setTimeout(function(){
                        jQuery('body').scrollTop(0);
                        onscreenchange();
                    }, 500);
                }

                function handleResize(){ // for android
                    if(window.screen.width > window.screen.height){
                        orientation = 'landscape';
                    } else {
                        orientation = 'portrait';
                    };

                    window.setTimeout(onscreenchange(), 200);
                };

                function onscreenchange() {
                    console.log('onscreenchange: '+orientation);
                    if(orientation == 'landscape'){
                        jQuery('#container').removeClass('portrait');
                        jQuery('body').removeClass('portrait');
                    } else {
                        jQuery('#container').removeClass('landscape');
                        jQuery('body').removeClass('landscape');
                    };
                    jQuery('#container').addClass(orientation);
                    jQuery('body').addClass(orientation);

                    // window.alert('onscreenchange, orientation: ' + orientation);

                    if(orientation == 'landscape'){
                        if(window.iOS){
                        //     screenWidth = iOS_screenWidth;
                        //     screenHeight = iOS_screenHeight;
                        // } else {
                            screenWidth = window.innerWidth; // jQuery('#gameDiv').width(); // 0;
                            screenHeight = window.innerHeight;
                        };
                        // if(window.iphone_x){
                        //     var offset = -50;
                        // } else {
                        //     offset = 0;
                        //     jQuery('#gameDiv').height(screenHeight);
                        // };

                        // game_idealHeight = (game_idealWidth*screenHeight/screenWidth);
                        // if(!window.iOS) game_idealWidth = (screenWidth*game_idealHeight/screenHeight);
                        if(!window.hasGame) jQuery('#gameDiv').height(window.innerHeight);

                        console.log('screenWidth: ' + screenWidth + ', screenHeight: ' + screenHeight);
                        console.log('game_idealWidth: ' + game_idealWidth + ', game_idealHeight: ' + game_idealHeight);

                        // window.alert('widthRef: ' + game_idealWidth + ', heightRef: ' + game_idealHeight);
                        // window.alert('window.screen.width: '+window.screen.width+', window.innerHeight: '+window.innerHeight+', window.screen.availHeight: '+window.screen.availHeight);

                        // if(!window.iOS){
                        //     screenWidth = game_idealWidth;
                        //     screenHeight = game_idealHeight;
                        // };

                        if(!window.hasGame){
                            window.hasGame = true;
                            // game start
                            // var game = new Phaser.Game(game_idealWidth, game_idealHeight, Phaser.CANVAS, 'gameDiv');
                            //var powerup = _getPowerup();

                            var config = {
                                type: Phaser.CANVAS,
                                width: game_idealWidth,
                                height: game_idealHeight,
                                parent: 'gameDiv',
                                backgroundColor: '#000000',
                                physics: {
                                        default: 'arcade',
                                        arcade: {
                                            gravity: 0
                                            //,debug: true
                                        }
                                    },
                                scale: {
                                    mode: Phaser.Scale.FIT,
                                    // autoCenter: Phaser.Scale.CENTER_BOTH
                                },
                                audio: {
                                    disableWebAudio: true
                                },
                                // scene: [ Preloader, MainMenu, GameScene ]
                                scene: [ Preloader, GameScene ]
                            };

                            var game = new Phaser.Game(config);
                            initListener(game);
                        };

                        // game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

                        // if(window.iOS){
                            // window.setTimeout(function(){
                                // console.log('sad');
                                // jQuery('body').scrollTop(-10);
                                // window.scrollTo(0, 0);
                            // }, 500);
                        // };
                    };
                };
            })
        , 1000);

        var initListener = function(ref) {
            // get user detail
            var temp = {
                user_id: <?php echo $userId; ?>,
                name: <?php echo json_encode($name); ?>,
                totalCoins: <?php echo json_encode($totalQcoins); ?>,
                playTimesCount: <?php echo json_encode($playTimesCount); ?>,
                playTimesEarn: <?php echo json_encode($playTimesEarn); ?>
            };
            var baseUrl = <?php echo json_encode($baseUrl); ?>;
            var restartPending = false;
            console.log(temp);

            // pump into gameOptions
            gameOptions.baseUrl = baseUrl;

            var successSfx = new Audio('img/assets/sound/success.wav');
            var failSfx = new Audio('img/assets/sound/die.mp3');

            window.addEventListener('level check', function(e){
                _levelCheck(ref, temp);
            }, false);

            window.addEventListener('init user', function(e){
                _initUser(ref, temp.user_id);
            }, false);

            window.addEventListener('update user', function(e){
                console.log('update user event received');
                console.log(e);

                if(e.detail.user_id){
                    temp = {
                        user_id: e.detail.user_id,
                        name: e.detail.user_name,
                        totalCoins: e.detail.qcoins,
                        playTimesCount: e.detail.play_times_count,
                        playTimesEarn: e.detail.play_times_earn
                    };
                };

            }, false);

            window.addEventListener('gxfc', function(e){
                gxfc = true;

                Swal.fire({
                    title: 'Gong Xi Fa Cai!',
                    html: '<div class="form-group"><a href="javascript:login();">Log in and Play</a><small>Instantly collect your Q-Coins</small></div><div class="form-group"><a href="javascript:signup();">Sign up</a></div><div class="form-group"><a href="javascript:menu();">Play Without Logging in</a></div>',
                    allowEscapeKey : false,
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    showCancelButton: false,
                    customClass: {
                        container: 'gxfc custom-popup-theme'
                    },
                    buttonsStyling: false
                }).then(function(result) {
                    console.log(result);
                });
            }, false);

            window.addEventListener('display menu', function (e) {
                // console.log(temp);
                // console.log(temp.name);

                // _callIn(ref, e.detail+" sadddddddddddddddssss");
                var welcome = '<div class="welcome guest">Welcome, '+temp.name+'!</div>';
                var menu = '<a href="javascript:start()">Start</a><a href="javascript:howto()">How to Play</a><a href="javascript:quit()">Quit</a>';
                if(temp.user_id){
                    // show life
                    welcome = '<div class="welcome">Welcome, '+temp.name+'!</div><div class="game-life"><span>Life</span>'+temp.playTimesCount+'</div>';
                    // menu = '<a href="javascript:start()">Start</a><a href="javascript:howto()">How to Play</a><a href="javascript:earnMore()">Earn More Life'+((temp.playTimesEarn > 0) ? '('+temp.playTimesEarn+')' : '')+'</a><a href="javascript:quit()">Quit</a>';
                    menu = '<a href="javascript:start()">Start</a><a href="javascript:howto()">How to Play</a><a href="javascript:earnMore()">Earn More Life ('+temp.playTimesEarn+')</a><a href="javascript:quit()">Quit</a>';
                };

                Swal.fire({
                    title: 'Menu',
                    html: welcome+menu,
                    allowEscapeKey : false,
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    showCancelButton: false,
                    customClass: {
                        container: 'mainmenu-popup'
                    },
                    buttonsStyling: false
                }).then(function(result) {
                    console.log(result);
                });
            }, false);

            var quizID = -1;
            var ans = [];
            var gxfc = true;
            var timerInterval;
            var clickOnce = true;
            var startOnce = true;

            window.login = function(){
                var loginForm = '<form><div class="form-group"><label><span>Email</span><input type="text" class="username"></label></div><div class="form-group"><label><span>Password</span><input type="password" class="password"></label></div><div class="form-group error disable"></div></form>';

                Swal.fire({
                    title: 'Login',
                    html: loginForm+'<a href="javascript:login_now();">Login Now</a>',
                    allowEscapeKey : false,
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    showCancelButton: false,
                    showCloseButton: true,
                    customClass: {
                        container: 'login-popup custom-popup-theme'
                    },
                    buttonsStyling: false
                }).then(function(result) {
                    console.log(result);
                    if(result.dismiss == 'close'){
                        // window.dispatchEvent(new CustomEvent('gxfc'));
                        window.menu();
                    };
                });
            };

            window.login_now = function(){
                // console.log('login now!');
                var username = jQuery('.username').val();
                var password = jQuery('.password').val();

                if(username.length > 0 && password.length > 0){
                    jQuery.post(baseUrl+'event-game-login', {email: username, password: password, gxfc: gxfc}, function(data){
                        var result = JSON.parse(data).data;
                        console.log('login callback received!');
                        console.log(result);

                        if(result.user_id){
                            // pump in user id and show main menu
                            temp = {
                                user_id: parseInt(result.user_id),
                                name: result.user_name,
                                totalCoins: result.qcoins,
                                playTimesCount: result.play_times_count,
                                playTimesEarn: result.play_times_earn
                            };
                            console.log(temp);

                            // pass in user
                            _levelCheck(ref, temp);
                            // _loggedIn(ref, temp);

                            // show main menu
                            window.dispatchEvent(new CustomEvent('display menu'));
                        } else {
                            jQuery('.login-popup .error').removeClass('disable');
                            jQuery('.login-popup .error').html('Invalid username/password');
                        };
                    });
                };
            };

            window.signup = function(){
                // console.log('sign up!');
                window.location.href = baseUrl+'notice';
            };

            window.menu = function(){
                if(restartPending){
                    restartPending = false;
                    // _gameOverCallback(ref, true);

                    // check game level
                    _levelCheck(ref, temp);
                } else {
                    window.dispatchEvent(new CustomEvent('display menu'));
                };
            };

            window.toggleMute = function(){
                console.log('toggle isMute: ' + isMute);
                isMute = !isMute;
                console.log('toggled: ' + isMute);

                // button state
                if(isMute){
                    jQuery('.mute-btn').addClass('mute');
                } else {
                    jQuery('.mute-btn').removeClass('mute');
                };

                // game state
                _toggleMute(ref);
            };

            window.start = function() {
                if(startOnce){
                    if(temp.user_id){
                        console.log(temp);
                        jQuery.post(baseUrl+'update-user-play-times', {user_id: temp.user_id}, function(data){
                            var result = JSON.parse(data).data;
                            console.log('update user play times callback received:');
                            console.log(result);

                            if(result.result){
                                Swal.close();
                                startOnce = false;
                                _gameStart(ref, temp.user_id);
                            } else {
                                // update available play time to claim
                                temp.playTimesEarn = result.play_times_earn;
                                // out of life, show Earn Additional Life popup
                                window.earnAdditionalLife();
                            };
                        });
                    } else {
                        // guest 
                        Swal.close();
                        startOnce = false;
                        _gameStart(ref, temp.user_id);
                    };
                };
            };

            window.howto = function() {
                // console.log('howto');
                var imgRoot = baseUrl+'components/com_quake_club_game_records/views/quakeclubgamerecords/tmpl/img/';
                Swal.fire({
                    title: 'How to Play',
                    html: '<div class="row"><div class="col-2 col-md-2 mouse"><img src="'+imgRoot+'howto-mouse.png"></div><div class="col-10 col-md-10 align-left"><p><strong>You have 1 minute to play the game.</strong> Collect the coins, avoid the traps, get the Mystery Power Up and Ang Pau. The rat will run automatically to the right.</p><p>There are two types of jumps as per below.</p></div></div><div class="row"><div class="col-6 col-md-6 jump-desktop"><h4>Desktop</h4><img src="'+imgRoot+'howto-desktop.png"><p>Press space bar once to jump. Press this twice to jump further.</p></div><div class="col-6 col-md-6 jump-mobile"><h4>Mobile</h4><img src="'+imgRoot+'howto-mobile.png"><p>Tap screen once to jump. Tap screen twice to jump further.</p></div></div><div class="row rewards"><div class="col-12 col-md-12"><h4>Rewards</h4></div><div class="col-md-4"><div><img src="'+imgRoot+'howto-coin.png"></div><strong>Q-Coins</strong><p>Reward Point(s)</p></div><div class="col-md-4"><div><img src="'+imgRoot+'howto-angbao.png"></div><strong>Ang Pau</strong><p>Answer the pop-up question for bonus Q-Coins</p></div><div class="col-md-4"><div><img src="'+imgRoot+'howto-cheese.png"></div><strong>Mystery Power Up</strong><p>Double points<br>OR<br>Bonus time +10 secs<br>OR<br>Be invisible to obstacles</p></div></div><div class="row"><div class="col-12 col-md-12"><h4>Obstacles</h4></div><div class="col-3 col-md-3"><img src="'+imgRoot+'howto-trap-01.png"></div><div class="col-3 col-md-3"><img src="'+imgRoot+'howto-trap-02.png"></div><div class="col-3 col-md-3"><img src="'+imgRoot+'howto-trap-03.png"></div><div class="col-3 col-md-3"><img src="'+imgRoot+'howto-trap-04.png"></div><div class="col-12 col-md-12"><p>Avoid at all cost, each stage comes with different traps.</p></div></div><div class="row"><div class="col-12 col-md-12"><h4>Levels</h4></div><div class="col-12 col-md-12"><p>Accumulate Q-Coins to unlock different backgrounds and levels of difficulty.</p><p><strong>Beginner Level</strong> - Accumulated <strong>0 - 387</strong> Q-Coin(s).</p><p><strong>Intermediate Level</strong> - Accumulated <strong>388 - 687</strong> Q-Coins.</p><p><strong>Advanced Level</strong> - Accumulated <strong>688 and above</strong> Q-Coins.</p></div></div>',
                    allowEscapeKey : false,
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    showCancelButton: false,
                    showCloseButton: true,
                    customClass: {
                        container: 'howto-popup custom-popup-theme'
                    },
                    buttonsStyling: false
                }).then(function(result) {
                    console.log(result);
                    if(result.dismiss == 'close'){
                        // window.menu();
                        window.dispatchEvent(new CustomEvent('display menu'));
                    };
                });
            };

            window.earnMore = function() {
                Swal.fire({
                    title: 'Earn More Life',
                    html: '<div class="text-content"><strong>How?</strong></div><div class="text-content">When you complete the game, you have more chances to earn more lives by performing these actions on Quake website*<br>(max 2 actions for 2 lives per day):</div><div><a href="'+baseUrl+'download-centre?s=2">Download<br>document</a><a href="'+baseUrl+'whats-on?s=2">Share<br>Article</a></div><div class="text-content earn-more-left">Number of Life Left: <span class="life-badge">'+temp.playTimesEarn+'</span></div><div class="text-content"><small>*you will not earn another chance if you download/share the same article that you have downloaded/shared previously.</small></div>',
                    allowEscapeKey : false,
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    showCancelButton: false,
                    showCloseButton: true,
                    customClass: {
                        container: 'earn-more-life-popup custom-popup-theme'
                    },
                    buttonsStyling: false
                }).then(function(result) {
                    console.log(result);
                    if(result.dismiss == 'close'){
                        window.menu();
                    };
                });
            };

            window.quit = function() {
                // console.log('quit');
                window.location.href = baseUrl+'prosperatty';
            };

            window.earnAdditionalLife = function(){
                console.log(temp.playTimesEarn);
                if(temp.playTimesEarn == 2){
                    var chance = 'TWO';
                } else if(temp.playTimesEarn == 1){
                    chance = 'ONE';
                } else if(temp.playTimesEarn == 0){
                    chance = 'NO MORE';
                };

                Swal.fire({
                    title: 'Earn Additional Life',
                    html: '<div class="text-content">Download ONE document/share ONE articles<br>to earn ONE life to play.<br>You have <span>'+chance+'</span> unclaimed life(s) today.</div><a href="'+baseUrl+'download-centre?s=2">Download</a><a href="'+baseUrl+'whats-on?s=2">Share</a>',
                    allowEscapeKey : false,
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    showCancelButton: false,
                    showCloseButton: true,
                    customClass: {
                        container: 'earn-additional-life-popup custom-popup-theme'
                    },
                    buttonsStyling: false
                }).then(function(result) {
                    console.log(result);
                    if(result.dismiss == 'close'){
                        window.menu();
                    };
                });
            };

            window.answer = function(_ans){
                console.log(_ans);
                console.log(ans[_ans]);

                // stop timer 
                clearInterval(timerInterval);

                jQuery.post(baseUrl+'check-event-questions', {question_id: quizID, correct_answer: ans[_ans].toString()}, function(data){
                    var result = JSON.parse(data).data;
                    console.log('check event questions callback received:');
                    // console.log(result);

                    if(result.result == "Correct"){
                        if(!isMute) successSfx.play();
                        Swal.close();
                    } else {
                        if(!isMute) failSfx.play();
                        // show correct answer
                        if(_ans == 0){
                            var target = '.ans-1';
                        } else {
                            target = '.ans-0';
                        };
                        jQuery(target).addClass('correct');
                        // delay 1 sec
                        window.setTimeout(function(e){
                            Swal.close();
                        }, 3000);
                    };

                    if(clickOnce){
                        clickOnce = false;
                        jQuery('.swal2-close').addClass('disable');
                        _ansQuiz(ref, result.result);
                    };
                });
            };

            window.collectQcoins = function(e){
                // console.log('collect Q-Coins! Coins available for collect: ' + e);

                Swal.fire({
                    title: 'Collect Q-Coins',
                    html: '<div class="row"><div class="col-5 login-now"><p>Welcome back, member!</p><div class="form-group"><a href="javascript:login();">Login Now</a></div></div><div class="col-2 text-or">or</div><div class="col-5 not-a-member"><p>Not a member yet?</p><div class="form-group"><a href="javascript:signup();">Sign up</a></div><p>Be a member of the quake club as a professional from the marketing industry.</p></div></div>',
                    allowEscapeKey : false,
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    showCancelButton: false,
                    showCloseButton: true,
                    customClass: {
                        container: 'collect-q-coins-popup custom-popup-theme'
                    },
                    buttonsStyling: false
                }).then(function(result) {
                    console.log(result);
                    if(result.dismiss == 'close'){
                        window.menu();
                    };
                });
            };

            window.exitGame = function(){
                // console.log('exit game!');
                window.location.href = baseUrl+'prosperatty';
            };

            window.playAgain = function(){
                /*
                Swal.close();
                _gameOverCallback(ref, true);
                */
                if(temp.user_id){
                    jQuery.post(baseUrl+'check-user-play-times', {user_id: temp.user_id}, function(data){
                        var result = JSON.parse(data).data;
                        console.log('check user play times callback received:');
                        console.log(result);

                        if(result.result){
                            window.menu();
                        } else {
                            // update available play time to claim
                            temp.playTimesEarn = result.play_times_earn;
                            // out of life, show Earn Additional Life popup
                            window.earnAdditionalLife();
                        };
                    });
                } else {
                    // guest 
                    window.menu();
                };
            };

            window.addEventListener('quiz', function(e){
                clickOnce = true;

                console.log('call api and get quiz question');
                jQuery.post(baseUrl+'get-event-questions', function(data){
                    console.log('callback received!');
                    var result = JSON.parse(data).data;
                    // console.log(result);

                    quizID = result.question_id;
                    ans = [result.answer_1, result.answer_2];
                    var timeLeft = 10;

                    Swal.fire({
                        title: 'Question',
                        html: '<div class="question">'+result.question+'</div><div class="answer"><a href="javascript:answer(0)" class="ans-0">'+result.answer_1+'</a><a href="javascript:answer(1)" class="ans-1">'+result.answer_2+'</a></div><div class="custom-timer">10</div>',
                        allowEscapeKey : false,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        showCancelButton: false,
                        showCloseButton: true,
                        timer: 10000,
                        // confirmButtonColor: '#3085d6',
                        // cancelButtonColor: '#d33',
                        customClass: {
                            container: 'quiz-popup'
                        },
                        buttonsStyling: false,
                        onBeforeOpen: function() {
                            timerInterval = setInterval(function() {
                                timeLeft--;
                                jQuery('.custom-timer').html(timeLeft);
                                if(timeLeft == 0){
                                    clearInterval(timerInterval);
                                };
                            }, 1000);
                        },
                        onClose: function() {
                            clearInterval(timerInterval);
                        }
                    }).then(function(result){
                        // console.log('alert box dismissed, reason:');
                        // console.log(result.dismiss);
                        if (result.dismiss == 'close' || result.dismiss === Swal.DismissReason.timer) {
                            // console.log('I was closed by the timer');
                            if(clickOnce){
                                clickOnce = false;
                                _skipQuiz(ref, false);
                            };
                        };
                    });
                });
            }, false);

            window.addEventListener('gameOver', function(e){
                restartPending = true;
                gxfc = false;
                startOnce = true;

                // console.log(e);
                var thisTitle = 'Game End';
                var thisContent = 'You have won <span>'+e.detail.coin+'</span> Q-Coins<div class="answer"><a href="javascript:exitGame()">EXIT GAME</a><a href="javascript:playAgain()">PLAY AGAIN</a>';
                if(!e.detail.reason){
                    thisTitle = 'Oops!';
                };

                if(!e.detail.user_id){
                    thisContent = 'You have won <span>'+e.detail.coin+'</span> Q-Coins<div class="answer"><div class="form-group"><a href="javascript:collectQcoins('+e.detail.coin+');">Collect Q-Coins</a></div><div class="form-group"><a href="javascript:exitGame();">EXIT GAME</a></div><div class="form-group"><a href="javascript:playAgain();">PLAY AGAIN</a></div>';
                };

                Swal.fire({
                    title: thisTitle,
                    html: thisContent,
                    allowEscapeKey : false,
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    showCancelButton: false,
                    // confirmButtonColor: '#3085d6',
                    // cancelButtonColor: '#d33',
                    customClass: {
                        container: 'gameover-popup custom-popup-theme'
                    },
                    confirmButtonText: 'Retry',
                    cancelButtonText: 'Quit',
                    buttonsStyling: false
                }).then(function(result) {
                    console.log('....');
                    // _gameOverCallback(ref, result.value);
                });
            }, false);
        }
    });
</script>
