class SoundManager extends Phaser.Scene {
    constructor(x, y, scene){
        super({ key: 'gamescene' });

		this.scene = scene;
		this.hasSound = true;
        this.gameState = false; // game is running, player not dead
		this.soundAsset = [];

        this.scene.events.on('toggleMute', function(){
            console.log('toggleMute Event received!');
            this.muteAll(this.hasSound);
        }, this);
    }

    initialize(key, loop, volume){
        if(!isset(loop)){
            loop = false;
        };
        if(!isset(volume)){
            volume = 1;
        };
    	this.soundAsset[key] = this.scene.sound.add(key, { loop: loop, volume: volume});

        // console.log('initialize: ' + key + ', loop: ' + loop + ', volume: ' + volume);
        // console.log(this.soundAsset[key]);
    }

    updateGameState(state){
        this.gameState = state;
    }

    muteSettingsCheck(){
        if(localStorage.hasOwnProperty('mute')){
            console.log('isMute property exist');
            var isMute = JSON.parse(localStorage.getItem('mute'));
        } else {
            console.log('isMute unset!!!');
            // is not mute by default
            isMute = false;
            localStorage.setItem('mute', isMute);
        };
        console.log('isMute: ' + isMute);
        if(isMute){
            // mute all as per previous settings
            this.muteAll(isMute);
        };

        return isMute;
    }

    muteAll(goingToMute) {
    	if(goingToMute){
    		// mute sound
    		this.hasSound = false;

            // immediately mute all playing or looping sound
            Object.entries(this.soundAsset).forEach(function([key, val]) {
                val.stop();
            });
    	} else {
    		this.hasSound = true;
            // immediately play looping sound
            Object.entries(this.soundAsset).forEach(function([key, val]) {
                if(this.gameState && val.loop) val.play({volume: val.config.volume});
            });
    	};

        // save settings
        localStorage.setItem('mute', goingToMute);
        console.log('mute settings saved');
        console.log(localStorage.getItem('mute'));
    }

    play(key, volume){
        if(this.hasSound){
            if(!isset(volume)){
                volume = this.soundAsset[key].config.volume;
            };

            this.soundAsset[key].play({volume: volume});
        };
    }

    isPlaying(key) {
        return this.soundAsset[key].isPlaying;
    }

    fadeOut(key, volume, duration){
        if(!isset(volume)){
            volume = 0;
        };
        if(!isset(duration)){
            duration = 500;
        };

        var originalVolume = this.soundAsset[key].config.volume;
    	this.scene.tweens.add({ targets: this.soundAsset[key], volume: volume, duration: duration, onComplete: function(){
            // stop the sound
            this.soundAsset[key].stop();
            this.soundAsset[key].volume = originalVolume;
        }, onCompleteScope: this});
    }
}

function isset(target){
	return (typeof target !== 'undefined');
}