// Phaser3 example game
// preloader and loading bar

// global game options
var gameOptions = {
	level: 1,
    speed: [6,9,12],
    offset: 15,
    trapRatio: [0.4, 0.6, 0.4],
    trapOffsetY: [0, 180],
    maxC: [88,98,118],
    frequency: 150,
    totalTime: 60,
    expectedCoins: 10,
    expectedTraps: 10,
    //bad: trap under coins; combo: number of traps that come together; rate: chance of combo [0-100]; gap: minimum space between traps
    difficulty: [{bad: 2, combo: 2, rate: 20, gap: 2}, {bad: 4, combo: 3, rate: 30, gap: 2}, {bad: 6, combo: 3, rate: 60, gap:1}],
    // player gravity
    playerGravity: 1500,
    // player jump force
    jumpForce: 700,
    // player starting X position
    playerStartPosition: 100,
    // consecutive jumps allowed
    jumps: 2,
    // % of probability a coin appears on the platform
    coinPercent: 80,
    // % of probability a fire appears on the platform
    trapPercent: 30
}

var dispatcher;

var Preloader = new Phaser.Class({

	Extends: Phaser.Scene,

	initialize:

	function Preloader ()
	{
		// note: the pack:{files[]} acts like a pre-preloader
		// this eliminates the need for an extra "boot" scene just to preload the loadingbar images
		Phaser.Scene.call(this, {
			key: 'preloader',
			pack: {
				files: [
					{ type: 'image', key: 'loadingbar_bg', url: 'img/loadingbar_bg.png' },
					{ type: 'image', key: 'loadingbar_fill', url: 'img/loadingbar_fill.png' }
				]
			}
		});
	},
	
	setPreloadSprite: function (sprite)
	{
		this.preloadSprite = { sprite: sprite, width: sprite.width, height: sprite.height };

		//sprite.crop(this.preloadSprite.rect);
		sprite.visible = true;

		// set callback for loading progress updates
		this.load.on('progress', this.onProgress, this );
		this.load.on('fileprogress', this.onFileProgress, this );
	},
	
	onProgress: function (value) {

		if (this.preloadSprite)
		{
			// calculate width based on value=0.0 .. 1.0
			var w = Math.floor(this.preloadSprite.width * value);
			// console.log('onProgress: value=' + value + " w=" + w);
			
			// sprite.frame.width cannot be zero
			//w = (w <= 0 ? 1 : w);
			
			// set width of sprite			
			this.preloadSprite.sprite.frame.width    = (w <= 0 ? 1 : w);
			this.preloadSprite.sprite.frame.cutWidth = w;

			// update screen
			this.preloadSprite.sprite.frame.updateUVs();
		}
	},
	
	onFileProgress: function (file) {
		// console.log('onFileProgress: file.key=' + file.key);
	},

	preload: function ()
	{
		// console.log(this.cameras.main.centerX);
		var camera = this.cameras.main;
		// setup the loading bar
		// note: images are available during preload because of the pack-property in the constructor
		this.loadingbar_bg   = this.add.sprite(camera.centerX, camera.centerY, "loadingbar_bg");
		this.loadingbar_fill = this.add.sprite(camera.centerX, camera.centerY, "loadingbar_fill");
		this.setPreloadSprite(this.loadingbar_fill);
/*
		// now load images, audio etc.
		// sprites, note: see free sprite atlas creation tool here https://www.leshylabs.com/apps/sstool/
		this.load.atlas('sprites', 'img/spritearray.png', 'img/spritearray.json');

		// font
		this.load.bitmapFont('fontwhite', 'img/fontwhite.png', 'img/fontwhite.xml');
		
		// sound effects
		//this.load.audio('bg', [this.p('audio/bg.mp3'),this.p('audio/bg.ogg')]);
		this.load.audio('coin', ['snd/coin.mp3', 'snd/coin.ogg']);
		this.load.audio('bomb', ['snd/expl.mp3', 'snd/expl.ogg']);
		this.load.audio('btn',  ['snd/btn.mp3', 'snd/btn.ogg']);
*/		

		// collectables
		this.load.spritesheet('coin', 'img/animations/coin.png', { frameWidth: 120, frameHeight: 120 });
		// obstacles
		this.load.spritesheet('bat', 'img/animations/bat.png', { frameWidth: 240, frameHeight: 120 });
		this.load.image('trap', 'img/assets/trap-01.png');
		this.load.image('trap2', 'img/assets/trap-02.png');
		this.load.image('trap3', 'img/assets/trap-03.png');
		// powerup
		this.load.image('powerup', 'img/assets/cheese.png');
		// bonus quiz
		this.load.image('angpau', 'img/assets/angpau.png');
		// glow fx
		this.load.image('glowfx', 'img/assets/glow-fx-white.png');
		// player
		this.load.spritesheet('player', 'img/animations/mouse.png', { frameWidth: 180, frameHeight: 260 });
		this.load.image('playerDead', 'img/assets/player-dead.png');
		// ui
		this.load.image('scorebg', 'img/assets/coin-background.png');
		this.load.image('timebg', 'img/assets/time-background.png');
		// misc
		this.load.image('background-01', 'img/assets/light-stage-01.png');
		this.load.image('background-02', 'img/assets/light-stage-02.png');
		this.load.image('background-03', 'img/assets/light-stage-03.png');

		// sounds
		this.load.audio('bgm', 'img/assets/sound/bgm.mp3');
		this.load.audio('jumpSfx', 'img/assets/sound/jump.wav');
		this.load.audio('coinSfx', 'img/assets/sound/coin.mp3');
		this.load.audio('dieSfx', 'img/assets/sound/die.mp3');
		this.load.audio('bonusSfx', 'img/assets/sound/bonus.wav');
		this.load.audio('successSfx', 'img/assets/sound/success.wav');

		// !! TESTING !! load the same image 500 times just to slow down the load and test the loading bar
		// for (var i = 0; i < 500; i++) {
			// this.load.image('testloading'+i, 'img/loadingbar_bg.png');
		// };
		// !! TESTING !!
	},

	create: function ()
	{
		var coinAnimation = this.anims.create({
            key: 'coinRotate',
            frames: this.anims.generateFrameNumbers('coin'),
            frameRate: 12,
            repeat: -1
        });

        var playerRun = this.anims.create({
            key: 'playerRun',
            frames: this.anims.generateFrameNumbers('player'),
            frameRate: 16,
            repeat: -1
        });
        // var mouseSprite = this.add.sprite(300, 300, 'player').play('playerRun');

        var batFly = this.anims.create({
            key: 'batFly',
            frames: this.anims.generateFrameNumbers('bat'),
            frameRate: 16,
            repeat: -1
        });
        // var batSprite = this.add.sprite(500, 300, 'bat').play('batFly');
			
		console.log('Preloader scene is ready, now start the actual game and never return to this scene');

		// dispose loader bar images
		this.loadingbar_bg.destroy();
		this.loadingbar_fill.destroy();
		this.preloadSprite = null;

		var customEvent = new CustomEvent('level check');
		window.dispatchEvent(customEvent);

		// start actual game
		// this.scene.start('mainmenu');
		// this.scene.start('gamescene');
	}
});

function _levelCheck(ref, data){
	if(data.totalCoins >= 388 && data.totalCoins < 688){
		// level 2
		gameOptions.level = 2;
	} else if(data.totalCoins >= 688) {
		// level 3
		gameOptions.level = 3;
	};

	ref.scene.start('gamescene');
	_loggedIn(ref, data.user_id);
}

function _initUser(ref, data){
	if(data){
		console.log('user id exist, display menu!!');
        // user id exist, show main menu
        var customEvent = new CustomEvent('display menu');
    } else {
    	console.log('user id not exist, show gong xi fa cai');
        // gong xi fa cai screen
        customEvent = new CustomEvent('gxfc');
    };
    window.dispatchEvent(customEvent);
}

function _gameStart(ref, data){
	// initialize and display countdown
	jQuery('.countdown').html('<h1>3<small class="level">You are now at Level '+gameOptions.level+'</small></h1>');
	jQuery('.countdown').removeClass('disable');

	var counter = 3;
	var interval = setInterval(function() {
	    counter--;
	    jQuery('.countdown').html('<h1>'+counter+'<small class="level">You are now at Level '+gameOptions.level+'</small></h1>');

	    // Display 'counter' wherever you want to display it.
	    if (counter == 0) {
	        jQuery('.countdown').addClass('disable');
	        clearInterval(interval);
	        ref.scene.getScene('gamescene').events.emit('call in', data);
	    }
	}, 1000);
}

function _ansQuiz(ref, result){
	// initialize and display countdown
	var counter = 6;
	var displayText = '3';
	if(result == 'Wrong'){
    	result = false;
    } else if(result == 'Correct') {
    	result = true;
    	displayText = '<div class="coin-notifier-text">+10 Coins<div class="coin-rotate"></div></div>';
    	jQuery('.countdown').removeClass('disable');
    	jQuery('.countdown').html('<h1>'+displayText+'</h1>');
    };

	var interval = setInterval(function() {
	    counter--;
	    if(counter <= 3){
	    	jQuery('.countdown').removeClass('disable');
	    	jQuery('.countdown').html('<h1>'+counter+'</h1>');
	    };

	    // Display 'counter' wherever you want to display it.
	    if (counter == 0) {
	        jQuery('.countdown').addClass('disable');
	        clearInterval(interval);
	        ref.scene.getScene('gamescene').events.emit('quiz ans', result);
	    }
	}, 1000);
	/*
	setTimeout(function(e){
		ref.scene.getScene('gamescene').events.emit('quiz ans', result);
	}, 3000);*/
}

function _skipQuiz(ref){
	// initialize and display countdown
	var counter = 3;
	var displayText = '3';
	var result = false;

	jQuery('.countdown').html('<h1>'+displayText+'</h1>');
	jQuery('.countdown').removeClass('disable');

	var interval = setInterval(function() {
	    counter--;
	    if(counter <= 3) jQuery('.countdown').html('<h1>'+counter+'</h1>');

	    // Display 'counter' wherever you want to display it.
	    if (counter == 0) {
	        jQuery('.countdown').addClass('disable');
	        clearInterval(interval);
	        ref.scene.getScene('gamescene').events.emit('quiz ans', result);
	    }
	}, 1000);
}

function _loggedIn(ref, result){
	ref.scene.getScene('gamescene').events.emit('loginCallback', result);
}

function _gameOverCallback(ref, result){
	ref.scene.getScene('gamescene').events.emit('gameOverCallback', result);
}

function _toggleMute(ref){
	ref.scene.getScene('gamescene').events.emit('toggleMute');
}