// Phaser3 example game
// main game scene

var DIR_UP    = 1;
var DIR_DOWN  = 2;
var DIR_LEFT  = 4;
var DIR_RIGHT = 8;

var initOnce = false;

var GameScene = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

    function GameScene ()
    {
        Phaser.Scene.call(this, { key: 'gamescene' });
    },

    preload: function ()
    {

    },

    create: function ()
    {
        this.user_id = -1;
        this.gameTime = undefined;
        this.paused = false;
        this.level = gameOptions.level;
        speed = gameOptions.speed[this.level-1];

        // console.log(this.game.loop.actualFps);
        var camera = this.cameras.main;
        var thisBackground = 'background-0'+this.level;
        this.background = this.add.tileSprite(camera.centerX, camera.centerY, camera.displayWidth, camera.displayHeight, thisBackground);
        // this.background.alpha = 0.75;
        this.ground = this.physics.add.staticGroup();
        this.groundHeight = 80;
        var rect = this.add.rectangle(camera.centerX, camera.displayHeight-this.groundHeight*0.5, camera.displayWidth, this.groundHeight, 0x0000ff);
        rect.alpha = 0.2;
        this.ground.add(rect);

        this.retries = 0;
        this.maxRetries = 20;
        this.createDistribution();

        // group with all active coins.
        this.coinGroup = this.physics.add.group({
            // once a coin is removed, it's added to the pool
            removeCallback: function(coin){
                console.log('coinGroup: removeCallback!');
                // coin.scene.coinPool.add(coin);
            }
        });

        // group with all active traps.
        this.trapGroup = this.physics.add.group({
            maxSize: 2000,
            createCallback: function (trap) {
                console.log('Created', trap);
            },
            // once a trap is removed, it's added to the pool
            removeCallback: function(trap){
                // trap.scene.trapPool.add(trap)
                console.log('Removed', trap);
            }
        });

        this.bonusGroup = this.physics.add.group();

        // ui for timer and score
        this.scoreUI = this.add.sprite(0, 0, 'scorebg');
        this.scoreUI.setScale(0.8);
        this.scoreUI.x = camera.displayWidth-(this.scoreUI.displayWidth*0.5+10);
        this.scoreUI.y = 0 + this.scoreUI.displayHeight*0.5 + 10;
        this.scoreText = this.add.text(this.scoreUI.x, this.scoreUI.y-16, 'X 00 ', { fontSize: '32px', fill: '#FFF', fontFamily: 'regularFont, "Times New Roman", Tahoma, serif' });
        this.timeUI = this.add.sprite(0, 0, 'timebg');
        this.timeUI.setScale(0.8);
        this.timeUI.x = camera.displayWidth-(this.scoreUI.displayWidth+this.timeUI.displayWidth*0.5+10);
        this.timeUI.y = 0 + this.timeUI.displayHeight*0.5 + 10;
        this.timeText = this.add.text(this.timeUI.x, this.timeUI.y-16, '1:00 ', { fontSize: '32px', fill: '#FFF', fontFamily: 'regularFont, "Times New Roman", Tahoma, serif' });
        
        this.travelDistance = 0;
        // this.nextDistance = 0;
        this.playerJumps = 0;
        this.coinsCollected = 0;
        this.startTime = 0;
        this.totalTime = gameOptions.totalTime;
        this.powerupList = ['Double Points', 'Bonus Time', 'Invisible'];
        this.hasPowerup = false;
        this.multiplier = 1;
        this.quizLog = [];
        this.powerupLog = [];
        this.coinLog = [];
        this.jumpLog = [];
        this.gameOverReason = '';
        this.dying = false;
        this.gameClear = false;
        this.onFloor = false;

        // adding the player
        this.player = this.add.container(gameOptions.playerStartPosition, camera.displayHeight * 0.72).setName('playerContainer');
        this.player.setSize(90, 130);
        this.physics.world.enable(this.player);
        this.playerSprite = this.add.sprite(0, 0, 'player').setScale(0.5);
        this.player.add(this.playerSprite);
        this.player.body.setGravityY(gameOptions.playerGravity);
        console.log(this.player);
        this.player.setDepth(3);
        this.groundCollider = this.physics.add.collider(this.player, this.ground, function(){
            // play "run" animation if the player is on a platform
            if(!this.dying && !this.playerSprite.anims.isPlaying){
                this.onFloor = true;
                this.playerJumps = 0;
                this.playerSprite.anims.play('playerRun');
                // console.log('player on floor, start running animation');
            };
        }, null, this);

        // adding coin/trap to the game, the arguments are x position and y position
        // this.addElements(camera.displayWidth*0.5, camera.displayHeight-this.groundHeight);
        this.coinIndex = 0;
        this.trapIndex = 0;
        this.initElements();

        // setting collisions between the player and the coin group
        this.coinCollider = this.physics.add.overlap(this.player, this.coinGroup, function(player, coin){
            if(!coin.collected && !this.dying){
                coin.collected = true;
                this.coinsCollected += 1*this.multiplier;
                var displayScore = (this.coinsCollected.toString().length == 1) ? '0'+this.coinsCollected.toString()+' ' : this.coinsCollected.toString()+' ';
                this.scoreText.setText('X ' + displayScore);

                // safety log
                this.coinLog.push({time: this.getTotalTime(), location: this.travelDistance, jump: this.playerJumps, index: coin.index, multiplier: this.multiplier});

                // soundFx
                // this.coinSfx.play();
                this.soundManager.play('coinSfx');

                this.tweens.add({
                    targets: coin,
                    y: coin.y - 100,
                    alpha: 0,
                    duration: 800,
                    ease: "Cubic.easeOut",
                    callbackScope: this,
                    onComplete: function(){
                        // console.log('coin tween complete!');
                        this.removeCoin(coin);
                    }
                });

                // +1 text
                var thisText = '+'+(1*this.multiplier).toString()+' ';
                var thisTextPosY = (this.hasPowerup) ? ((this.thisPowerup == 'Bonus Time') ? -this.playerSprite.displayHeight*0.5-(64+32) : -this.playerSprite.displayHeight*0.5-(32+32)) : -this.playerSprite.displayHeight*0.5-32;
                var scoreTitle = this.add.text(0, thisTextPosY, thisText, { fontSize: '32px', fill: '#FFF', fontFamily: 'boldFont, "Times New Roman", Tahoma, serif' });
                scoreTitle.x -= scoreTitle.displayWidth*0.5;
                this.player.add(scoreTitle);

                this.tweens.add({
                    targets: scoreTitle,
                    alpha: 0,
                    y: scoreTitle.y - 50,
                    duration: 500,
                    callbackScope: this,
                    onComplete: function(){
                        scoreTitle.destroy();
                    }
                });
            };
        }, null, this);

        // setting collisions between the player and the trap group
        this.trapCollider = this.physics.add.overlap(this.player, this.trapGroup, function(player, trap){
            this.dying = true;
            // this.deadSprite = this.add.sprite(0, 0, 'playerDead').setScale(0.5);
            this.playerSprite.anims.stop();
            this.playerSprite.setFrame(0);
            var currentHeight = this.playerSprite.displayHeight;
            this.playerSprite.setTexture('playerDead');
            this.playerSprite.y = (currentHeight-this.playerSprite.displayHeight)*0.5;
            // this.player.body.setVelocityY(-200);
            // this.physics.world.removeCollider(this.groundCollider);

            this.gameOverReason = 'Stepped on trap, index: ' + trap.index;

            // soundFx
            // this.dieSfx.play();
            this.soundManager.play('dieSfx');
            // fade out bgm
            // this.tweens.add({ targets: this.bgm, volume: 0, duration: 500 });
            this.soundManager.fadeOut('bgm', 0, 500)
            this.soundManager.updateGameState(false);

            // dead, no longer interact with trap
            this.physics.world.removeCollider(this.trapCollider);

            this.gameOver();
        }, null, this);

        // bonus: powerup and quiz
        this.physics.add.overlap(this.player, this.bonusGroup, function(player, bonus){
            console.log(bonus.type);
            this.bonusGroup.killAndHide(bonus);
            this.bonusGroup.remove(bonus);

            // soundFx
            // this.bonusSfx.play();
            this.soundManager.play('bonusSfx');

            if(bonus.type == 'powerup'){
                this.hasPowerup = true;

                // remove the glow
                this.bonusGroup.killAndHide(this.powerupGlow);
                this.bonusGroup.remove(this.powerupGlow);

                // randomize powerup bonus
                var thisStart = 0;
                if(this.coinList.length >= (gameOptions.maxC[this.level-1]-gameOptions.offset-5)){
                    thisStart = 1;
                };
                this.thisPowerup = this.powerupList[Phaser.Math.Between(thisStart, 2)];
                // thisPowerup = 'Invisible';
                if(this.thisPowerup == 'Bonus Time'){
                    this.powerupTitle = this.add.text(0, -this.playerSprite.displayHeight*0.5-64, this.thisPowerup+'\n+10 sec ', { fontSize: '32px', fill: '#FFF', fontFamily: 'boldFont, "Times New Roman", Tahoma, serif' });
                } else {
                    this.powerupTitle = this.add.text(0, -this.playerSprite.displayHeight*0.5-32, this.thisPowerup+' ', { fontSize: '32px', fill: '#FFF', fontFamily: 'boldFont, "Times New Roman", Tahoma, serif' });
                };
                this.powerupTitle.x -= this.powerupTitle.displayWidth*0.5;
                this.player.add(this.powerupTitle);

                // safety log
                this.powerupLog.push({time: this.getTotalTime(), location: this.travelDistance, jump: this.playerJumps, powerup: this.thisPowerup});

                if(this.thisPowerup == 'Double Points'){
                    console.log('double points!!!');
                    this.multiplier = 2;

                    this.tweens.add({
                        targets: this.powerupTitle,
                        alpha: 0,
                        delay: 8000,
                        duration: 500,
                        yoyo: true,
                        repeat: 2,
                        callbackScope: this,
                        onComplete: function(){
                            this.powerupTitle.destroy();
                            this.multiplier = 1;
                            this.hasPowerup = false;
                        }
                    });
                };

                if(this.thisPowerup == 'Bonus Time'){
                    console.log('bonus time!!!');
                    this.totalTime += 10;

                    this.tweens.add({
                        targets: this.powerupTitle,
                        alpha: 0,
                        delay: 3000,
                        duration: 500,
                        callbackScope: this,
                        onComplete: function(){
                            this.powerupTitle.destroy();
                            this.hasPowerup = false;
                        }
                    });
                };

                if(this.thisPowerup == 'Invisible'){
                    console.log('invisible!!');
                    // invisible!
                    this.trapCollider.active = false;
                    // mouse opacity change
                    this.playerSprite.alpha = 0.5;

                    this.tweens.add({
                        targets: this.powerupTitle,
                        alpha: 0,
                        delay: 8000,
                        duration: 500,
                        yoyo: true,
                        repeat: 2,
                        callbackScope: this,
                        onComplete: function(){
                            this.powerupTitle.destroy();
                            this.trapCollider.active = true;
                            this.hasPowerup = false;
                            this.playerSprite.alpha = 1;
                        }
                    });
                };
            };

            if(bonus.type == 'quiz'){
                // remove the glow
                this.bonusGroup.killAndHide(this.quizGlow);
                this.bonusGroup.remove(this.quizGlow);

                // pause game and show modal
                this.showQuiz();
            };
        }, null, this);

        // checking for input
        this.input.on("pointerdown", this.jump, this);
        this.input.keyboard.on('keydown_SPACE', this.jump, this);
        this.input.keyboard.addCapture('SPACE');

        if(!initOnce){
            initOnce = true;

            this.events.on('call in', function(e){
                this.user_id = e;
                this.pauseGame(false);
                this.startTimeRef = this.gameTime.repeatCount;
                this.startTime = this.getTotalTime();
                // console.log(this.gameTime.repeatCount);
            }, this);

            this.events.on('quiz ans', function(e){
                console.log('quiz answer received');
                console.log(e);

                // safety log
                this.quizLog.push({time: this.getTotalTime(), location: this.travelDistance, jump: this.playerJumps, quizResult: e});
                
                if(e){
                    // correct answer
                    // add coins
                    this.coinsCollected += 10;
                    // update coin text
                    var displayScore = (this.coinsCollected.toString().length == 1) ? '0'+this.coinsCollected.toString()+' ' : this.coinsCollected.toString()+' ';
                    this.scoreText.setText('X ' + displayScore);
                };

                // this.time.addEvent({ delay: 3000, callback: this.scene.resume(), callbackScope: this });
                this.resumeGame();
            }, this);

            this.events.on('loginCallback', function(e){
                console.log('login callback received');
                console.log(e);
                if(e){
                    this.user_id = e.user_id;
                };
            }, this);

            this.events.on('gameOverCallback', function(e){
                console.log('gameOverCallback received');
                console.log(e);
                if(e){
                    this.scene.start("gamescene");
                };
            }, this);

            // sound Manager
            this.soundManager = new SoundManager(0, 0, this);
            this.soundManager.muteSettingsCheck();
            console.log(this.soundManager);
            // bgm
            this.soundManager.initialize('bgm', true, 0.25);
            // soundFx
            this.soundManager.initialize('jumpSfx', false, 0.5);
            this.soundManager.initialize('coinSfx', false, 0.5);
            this.soundManager.initialize('dieSfx', false, 0.5);
            this.soundManager.initialize('bonusSfx', false, 0.5);
            this.soundManager.initialize('successSfx', false, 0.5);
        };

        // start game
        // this.gameStart();
        this.pauseGame(true);
        // this.showQuiz();
    },

    update: function (time, delta){
        /*
        // game over
        if(this.player.y > this.cameras.main.displayHeight){
            this.gameOver();
        }
        */

        // player stay still
        this.player.x = gameOptions.playerStartPosition;
/*
        if(speed < gameOptions.maxSpeed){
            speed *= 1.0006;
        } else {
            // reach max speed
            speed = gameOptions.maxSpeed;
        };
*/
        if(!this.dying){
            this.background.tilePositionX ﻿+= speed;

            // group speed
            // var currentSpeed = speed*this.game.loop.actualFps;
            // move elements
            // this.physics.world.step(0);
            // this.coinGroup.setVelocityX(-currentSpeed);
            // this.trapGroup.setVelocityX(-currentSpeed);
            Phaser.Actions.Call(this.coinGroup.getChildren(), function(coin) {
                coin.x -= speed;
            });
            Phaser.Actions.Call(this.trapGroup.getChildren(), function(trap) {
                // trap.setVelocityX(-currentSpeed);
                trap.x -= speed;
            });
            Phaser.Actions.Call(this.bonusGroup.getChildren(), function(bonus){
                bonus.x -= speed;
            });
            // this.coinGroup.body.velocity.normalize().scale(-this.currentSpeed);

            // travel distance
            this.travelDistance += speed;/*
            if(this.travelDistance > this.nextDistance){
                console.log('addElements!');
                // reach the point to spawn coin or traps
                this.addElements(this.cameras.main.displayWidth, this.cameras.main.displayHeight-this.groundHeight);
            };*/

            // remove or recycling coins that out of screen
            this.coinGroup.getChildren().forEach(function(coin){
                if(!coin.collected && coin.x < - coin.displayWidth / 2){
                    this.removeCoin(coin);
                };
            }, this);
     
            // remove or recycling trap that out of screen
            this.trapGroup.getChildren().forEach(function(trap){
                if(trap.x < - trap.displayWidth / 2){
                    this.removeTrap(trap);
                };
            }, this);

            // remove bonus that out of screen
            this.bonusGroup.getChildren().forEach(function(bonus){
                if(bonus.x < bonus.displayWidth/2){
                    this.bonusGroup.killAndHide(bonus);
                    this.bonusGroup.remove(bonus);
                };
            }, this);
        };
    },

    createDistribution: function() {
        this.coinCount = Phaser.Math.Between(gameOptions.maxC[this.level-1]-gameOptions.offset, gameOptions.maxC[this.level-1])-10; //Math.random()*(gameOptions.offset)+(gameOptions.maxC[0]-gameOptions.offset);
        var minDistance = gameOptions.speed[this.level-1]*this.game.loop.actualFps*(gameOptions.totalTime);
        var totalDistance = gameOptions.speed[this.level-1]*this.game.loop.actualFps*(gameOptions.totalTime+10);
        this.standardDistance = gameOptions.speed[0]*this.game.loop.actualFps*(gameOptions.totalTime+10);
        this.distributionDistance = this.standardDistance/gameOptions.frequency;
        var maxFrequency = Math.floor(totalDistance/this.distributionDistance);
        this.minFrequencyEnd = Math.floor(minDistance/this.distributionDistance);
        if(gameOptions.frequency > maxFrequency){
            maxFrequency = gameOptions.frequency;
        };
        this.distributionPos = [];
        this.subCoinList = [];
        this.coinList = [];
        this.trapCount = Math.floor(this.coinCount*gameOptions.trapRatio[this.level-1]);
        if(this.level == 3){
            // increase difficulty
            // this.trapCount *= 2;
        };
        this.trapList = [];

        // initialize
        for(var i=0; i<maxFrequency; i++){
            this.distributionPos.push(i);
        };

        // quiz
        this.quizPos = Phaser.Math.Between(this.distributionPos.length*0.2, this.distributionPos.length*0.6);
        // powerup
        this.powerupPos = Phaser.Math.Between(this.distributionPos.length*0.2, this.distributionPos.length*0.6);
        do{
            // randomize again
            // console.log('oh no!');
            this.powerupPos = Phaser.Math.Between(this.distributionPos.length*0.2, this.distributionPos.length*0.6);
        } while(this.powerupPos == this.quizPos);
        console.log('quizPos: '+this.quizPos+', powerup pos: '+this.powerupPos);

        // coins
        for(i=0; i<this.coinCount; i++){
            var thisHeight = (this.level == 3) ? Phaser.Math.Between(1, 2) : 1;
            this.coinList.push({id: this.distributionPos.splice(Phaser.Math.Between(0, this.distributionPos.length-1), 1)[0], height: thisHeight});
            this.subCoinList.push(this.coinList[this.coinList.length-1].id);
        };
        this.coinList.sort(function(a, b){return a.id - b.id});
        // console.log('this.subCoinList: ' + this.subCoinList.sort(function(a, b){return a - b}));
        // console.log('distributionList: ' + this.distributionPos);

        // remove the slot used by powerup
        var removeQPos = this.distributionPos.splice(this.distributionPos.indexOf(this.quizPos), 1);
        var removePPos = this.distributionPos.splice(this.distributionPos.indexOf(this.powerupPos), 1);
        console.log('remove slots used by powerup/quiz --- quiz: ' + removeQPos + ', powerup: ' + removePPos);

        // traps
        var evilTrapCount = gameOptions.difficulty[this.level-1].bad;
        // this.trapCount -= evilTrapCount;
        for(i=0; i<evilTrapCount; i++){
            this.trapList.push({id: this.subCoinList.splice(Phaser.Math.Between(0, this.subCoinList.length-1), 1)[0], type: 1});
        };
        console.log('evil trap: ');
        console.log(this.trapList);
        for(i=evilTrapCount; i<this.trapCount; i++){
            var currentCount = this.trapList.length;
            var thisIndex = Phaser.Math.Between(0, this.distributionPos.length-1);
            var totalTrap = 1;
            var isCombo = (Phaser.Math.Between(0, 100) <= gameOptions.difficulty[this.level-1].rate) ? true : false;
            if(isCombo){
                // 2 traps in a row
                totalTrap++;
                // i++;

                if(gameOptions.difficulty[this.level-1].combo == 3){
                    // 3 in a row available!!
                    var lastCombo = (Phaser.Math.Between(0, 100) <= 50) ? 1 : 0;
                    totalTrap += lastCombo;
                    // i += lastCombo;
                };
            };
            if(this.distributionPos.length > 0 && this.retries < this.maxRetries){
                this.assignTrap(thisIndex, totalTrap);
            };

            var totalAssigned = this.trapCount.length-currentCount;
            if(totalAssigned > 1){
                i += totalAssigned-1;
            };
        };

        if(this.retries >= this.maxRetries){
            // console.log('!!!!!!!!!!!');
            // console.log('error occurred, redistributing position of everything.');
            this.retries = 0;
            this.createDistribution();
        } else {
            this.trapList.sort(function(a, b){return a - b});
            console.log('trapList: ');
            console.log(this.trapList);
            // this.validateTraps();

            console.log('max coin: ' + this.coinCount);
            console.log('max trap: ' + this.trapCount);
            console.log('coinList: ');
            console.log(this.coinList);
            console.log('evilTrap count: ' + evilTrapCount);
            // console.log('validated trapList: ' + this.trapList);
            console.log('fps: ' + this.game.loop.actualFps);
            console.log('distribute distance: ' + this.distributionDistance);
            console.log('max on screen: ' + this.cameras.main.displayWidth/this.distributionDistance);
        };
    },

    assignTrap:function(index, trapCount){
        var thisLocation = this.distributionPos[index];
        var validPosition = true;
        // var prevLocation = thisLocation-1;
        var nextLocation = thisLocation+trapCount;
        var proposedTrap = [];
        var toSplice = [];

        for(var i=0; i<trapCount; i++){
            proposedTrap.push(thisLocation+i);
        };
        // console.log('trap #'+this.trapList.length+', trap index: ' + index + ', trap count: ' + trapCount);
        // console.log('proposed trap location: ' + proposedTrap);
        // check if position available for trap
        for(i=0; i<proposedTrap.length; i++){
            validPosition = this.positionCheck(proposedTrap[i]).valid;
            if(!validPosition){
                // console.log("!!!!");
                break;
            };
        };

        if(validPosition){
            // check if next item exist
            // console.log('next position check: '+nextLocation);
            var temp = this.positionCheck(nextLocation);
            validPosition = temp.valid;
            if(validPosition && temp.toSplice != -1){
                toSplice.push(temp.toSplice);
            };

            // check if bigger gap is available
            if(validPosition && (gameOptions.difficulty[this.level-1].gap > 1)){
                var additionalLocation = nextLocation+1;
                // console.log('additional position check: '+additionalLocation);
                temp = this.positionCheck(additionalLocation);
                validPosition = temp.valid;
                if(validPosition && temp.toSplice != -1){
                    toSplice.push(temp.toSplice);
                };
            };
        } else {
            // console.log('proposed trap position not available...');
        };

        if(validPosition){
            // reset count check
            this.retries = 0;

            // pump in trap
            var batRate = 20; //gameOptions.difficulty[this.level-1].rate*0.5;
            var trapType = (this.level > 2) ? ((Phaser.Math.Between(0,100) <= batRate) ? 2 : 1 ) : 1;
            this.trapList.push({id: this.distributionPos.splice(index, trapCount)[0], type: trapType});
            // console.log('trap result: ' + proposedTrap);
            // dig up spaces
            // console.log('dig up spaces: ' + toSplice);
            for(i=0; i<toSplice.length; i++){
                this.distributionPos.splice(this.distributionPos.indexOf(toSplice[i]), 1);
            };
        } else {
            // randomize trap again
            // console.log('attempt to randomize trap again');

            this.retries++;

            var thisIndex = Phaser.Math.Between(0, this.distributionPos.length-1);
            var totalTrap = 1;
            var isCombo = (Phaser.Math.Between(0, 100) <= gameOptions.difficulty[this.level-1].rate) ? true : false;
            if(isCombo){
                // reduce to at most 2 traps in a row
                totalTrap++;
            };

            // if(this.retries >= this.maxRetries){
                // console.log('???????');
            // };
            if(this.distributionPos.length > 0 && this.retries < this.maxRetries){
                this.assignTrap(thisIndex, totalTrap);
            };
        };
    },

    positionCheck:function(location) {
        var valid = true;
        var isCoin = false;

        if(this.distributionPos.indexOf(location) >= 0){
            // next item exist, hooray
        } else {
            // hopefully next item is coin
            if(this.subCoinList.indexOf(location) >= 0){
                // is coin, safe
                isCoin = true;
            } else {
                // no luck
                valid = false;
            };
        };

        if(valid){
            // console.log('valid position');
        } else {
            // console.log('position invalid');
        };

        if(isCoin){
            location = -1;
        };

        return {valid: valid, toSplice: location};
    },

    validateTraps:function() {
        var badRow = gameOptions.difficulty[this.level-1].combo;
        var currentIndex = 0;
        var continuous = 0;
        for(var i=0; i<this.trapList.length; i++){
            currentIndex = this.trapList[i].id;
            // check if next item available
            if(i+1 < this.trapList.length){
                // console.log('currentIndex: '+currentIndex+', nextIndex: '+this.trapList[i+1]);
                // check if is continuous index
                if(currentIndex+1 == this.trapList[i+1].id){
                    continuous++;
                    // console.log('detected continuous: '+continuous);
                    if(continuous == badRow){
                        // console.log('4 in a row detected!');
                        this.trapList[i+1].id += 1;
                        continuous = 0;
                        // console.log('move next index backwards: ' + this.trapList[i+1]);
                    };
                } else {
                    // continuous index break
                    continuous = 0;
                };

                // check if next item is same index
                if(currentIndex == this.trapList[i+1].id){
                    // move backwards
                    this.trapList[i+1].id += 1;
                    continuous++;
                    // console.log('move next index backwards: ' + this.trapList[i+1].id);
                    if(continuous == badRow){
                        // console.log('4 in a row detected! What luck! Just restart from beginning.....');
                        i = -1;
                        continuous = 0;
                    };
                };
            };
        };
    },

    pauseGame: function(isPaused){
        if(isPaused){
            this.scene.pause();
            var customEvent = new CustomEvent('init user');
            setTimeout(function(e){
                window.dispatchEvent(customEvent);
                // this.pauseGame(false);
            }, 0);
        } else {
            if(typeof this.gameTime !== 'undefined'){
                // resume game
                this.scene.resume();
            } else {
                // start game for the first time
                this.gameStart();
            };
        };
    },

    resumeGame: function(){
        this.scene.resume();
    },

    getTotalTime: function(){
        var totalCount = this.startTimeRef-this.gameTime.repeatCount-1;
        return totalCount+(1000-this.gameTime.getElapsed())/1000;
    },

    showQuiz: function() {
        console.log('show quiz!');
        this.scene.pause();
        window.dispatchEvent(new CustomEvent('quiz', {detail: 'quiz'}));
    },

    gameStart: function(){
        this.scene.resume();
        this.gameTime = this.time.addEvent({ delay: 1000, callback: this.gameTimeHandler, callbackScope: this, loop: true });

        // this.bgm.play({volume: 0.25});
        this.soundManager.updateGameState(true);
        this.soundManager.play('bgm');
    },

    gameTimeHandler: function(){
        this.totalTime --;
        var displayTime = (this.totalTime.toString().length == 1) ? '0'+this.totalTime.toString()+' ' : this.totalTime.toString()+' ';
        this.timeText.setText('0:' + displayTime);

        if(this.totalTime == 0){
            // stop timer
            this.gameTime.remove(false);

            // no longer interact with any in-game objects
            this.coinCollider.active = false;
            this.trapCollider.active = false;

            // soundFx
            // this.successSfx.play();
            this.soundManager.play('successSfx');

            // game over
            this.gameOverReason = 'Time up';
            this.gameClear = true;
            this.gameOver();
        };
    },

    // the player jumps when on the ground, or once in the air as long as there are jumps left and the first jump was on the ground
    // and obviously if the player is not dying
    jump: function(){
        // console.log(this.player.body);
        if((!this.dying) && (this.onFloor || (this.playerJumps > 0 && this.playerJumps < gameOptions.jumps)) && !this.gameClear){
            // console.log('player jump');
            if(this.onFloor){
                this.onFloor = false;
            };
            this.player.body.setVelocityY(gameOptions.jumpForce * -1);
            this.playerJumps++;

            // safety log
            this.jumpLog.push({time: this.getTotalTime(), location: this.travelDistance, type: this.playerJumps});

            // jump soundFx
            // this.jumpSfx.play();
            this.soundManager.play('jumpSfx');

            var _isPlaying = this.soundManager.isPlaying('bgm');
            console.log('check if bgm is playing: ' + _isPlaying);

            // stops animation
            this.playerSprite.anims.stopOnFrame(this.playerSprite.anims.currentAnim.frames[2]);
        }
    },

    initElements: function() {
        this.safeZone = this.cameras.main.displayWidth*0.5; // good start without misfortune

        // put in the first 10 coins
        for(var i=0; i<gameOptions.expectedCoins; i++){
            // console.log(i);
            var posX = this.safeZone + this.coinList[i].id*this.distributionDistance - this.travelDistance;
            var posY = this.cameras.main.displayHeight-this.groundHeight - 96*this.coinList[i].height;
            this.addElements(posX, posY, 'coin', this.coinGroup);
        };

        // the first 10 traps
        // for(i=0; i<gameOptions.expectedTraps; i++){
        for(i=0; i<this.trapList.length; i++){
            posX = this.safeZone + this.trapList[i].id*this.distributionDistance - this.travelDistance;
            posY = this.cameras.main.displayHeight-this.groundHeight - 20;

            // check type of trap
            if(this.trapList[i].type == 1){
                var trapType = 'trap';
                if(this.level == 2){
                    trapType = 'trap2';
                    posY -= 10;
                } else if(this.level == 3){
                    trapType = 'trap3';
                };
            } else if(this.trapList[i].type == 2){
                trapType = 'bat';
            };
            posY -= gameOptions.trapOffsetY[this.trapList[i].type-1];

            this.addElements(posX, posY, trapType, this.trapGroup);
        };

        this.circleCoin();

        // add powerup
        posX = this.safeZone + this.powerupPos*this.distributionDistance - this.travelDistance;
        posY = this.cameras.main.displayHeight-this.groundHeight - 196;
        this.powerup = this.physics.add.sprite(posX, posY, 'powerup');
        this.powerupGlow = this.add.sprite(posX, posY, 'glowfx');
        this.powerup.setDepth(3);
        this.powerupGlow.setDepth(2);
        // this.powerup.setScale(0.5);
        this.powerup.setSize(90, 90, true);
        this.powerup.type = 'powerup';
        // add quiz
        posX = this.safeZone + this.quizPos*this.distributionDistance - this.travelDistance;
        this.quiz = this.physics.add.sprite(posX, posY, 'angpau');
        this.quizGlow = this.add.sprite(posX, posY, 'glowfx');
        this.quiz.setDepth(3);
        this.quizGlow.setDepth(2);
        // this.quiz.setScale(0.5);
        this.quiz.setSize(90, 90, true);
        this.quiz.type = 'quiz';
        // put into bonus group
        this.bonusGroup.add(this.powerup);
        this.bonusGroup.add(this.quiz);
        this.bonusGroup.add(this.powerupGlow);
        this.bonusGroup.add(this.quizGlow);
        // disable body for glow
        this.powerupGlow.body.setEnable(false);
        this.quizGlow.body.setEnable(false);

        console.log('total coins on stage: ' + this.coinGroup.getLength() + ', current coin index: ' + this.coinIndex);
        console.log('total traps on stage: ' + this.trapGroup.getLength() + ', current trap index: ' + this.trapIndex);
    },

    addElements: function(posX, posY, elementRef, groupRef){
        if(elementRef == 'coin'){
            this.coinIndex++;
            var thisElement = this.add.sprite(posX, posY, elementRef);
            thisElement.anims.play('coinRotate');
            thisElement.collected = false;
            thisElement.index = this.coinIndex-1;
        } else {
            this.trapIndex++;
            thisElement = this.physics.add.sprite(posX, posY, elementRef);
            thisElement.setImmovable(true);
            if(elementRef == 'trap'){
                thisElement.setSize(140, 2, true);
            } else if(elementRef == 'trap2'){
                thisElement.setSize(140, 40, true);
            } else if(elementRef == 'trap3'){
                thisElement.setSize(140, 40, true);
            } else if (elementRef == 'bat'){
                thisElement.setSize(100, 100, true);
                thisElement.anims.play('batFly');
            };
            thisElement.index = this.trapIndex-1;
        };
        thisElement.setDepth(2);
        // add to group
        groupRef.add(thisElement);
        // scale after add to group
        thisElement.setScale(0.5);
        /*
        if(elementRef == 'coin'){
            var test = thisElement.body.setCircle(elementRef.displayWidth*0.8, elementRef.displayWidth*0.2, elementRef.displayHeight*0.2);
            console.log('set coin circle body');
            console.log(test);
        };*/
    },

    circleCoin: function() {
        this.coinGroup.getChildren().forEach(function(coin){
            coin.body.setCircle(coin.displayWidth*0.8, coin.displayWidth*0.2, coin.displayHeight*0.2);
        }, this);
    },

    removeCoin: function(coin) {
        this.coinGroup.killAndHide(coin);
        this.coinGroup.remove(coin);

        // loop the coin!
        var hasExtraTime = (this.powerupLog.length > 0) ? ((this.powerupLog[0].powerup == 'Bonus Time') ? true : false) : false;
        var extraCoin = (this.coinIndex > this.minFrequencyEnd && hasExtraTime) ? true : false; // extra coin exist with extra time available
        if(this.coinIndex < this.coinList.length && (this.coinIndex <= this.minFrequencyEnd || extraCoin)){
            var posX = this.safeZone + this.coinList[this.coinIndex].id*this.distributionDistance - this.travelDistance;
            var posY = this.cameras.main.displayHeight-this.groundHeight - 96*this.coinList[this.coinIndex].height;
            this.addElements(posX, posY, 'coin', this.coinGroup);
            this.circleCoin();
        } else {
            // out of coins!
            console.log('out of coins!!');
        };
        // console.log('current coin index: ' + this.coinIndex);
        // console.log('coinList length: ' + this.coinList.length);
    },

    removeTrap: function(trap) {
        this.trapGroup.killAndHide(trap);
        this.trapGroup.remove(trap);
/*
        // loop the trap!
        if(this.trapIndex < this.trapList.length-1){
            var posX = this.safeZone + this.trapList[this.trapIndex].id*this.distributionDistance - this.travelDistance;
            var posY = this.cameras.main.displayHeight-this.groundHeight - 20;

            // check type of trap
            if(this.trapList[this.trapIndex].type == 1){
                var trapType = 'trap';
            } else if(this.trapList[this.trapIndex].type == 2){
                trapType = 'trap2';
            } else if(this.trapList[this.trapIndex].type == 3){
                trapType = 'bat';
            };
            posY -= gameOptions.trapOffsetY[this.trapList[this.trapIndex].type-1];

            this.addElements(posX, posY, trapType, this.trapGroup);
        } else {
            // out of traps!
            console.log('out of traps!!');
        };
*/        
        console.log('current trap index: ' + this.trapIndex);
        console.log('trapList length: ' + this.trapList.length);
    },
    
    gameOver: function(){
        // this.scene.pause();
        var thisDelay = 1500;
        if(this.gameClear){
            thisDelay = 0;
        };
        this.time.delayedCall(thisDelay, function(){
            this.scene.pause();
        }, null, this);

        // stop timer
        // console.log(this.gameTime.repeatCount);
        this.deadTime = this.getTotalTime();
        this.gameTime.remove(false);

        var tempDetails = this.coinsCollected;
        var customEvent = new CustomEvent('gameOver', { detail: {coin: tempDetails, reason: (this.gameOverReason == 'Time up') ? true : false, user_id: this.user_id}});
        setTimeout(function(e){
            window.dispatchEvent(customEvent);
        }, thisDelay);

        var game_log = {
            coinList: this.coinList,
            trapList: this.trapList,
            coinLog: this.coinLog,
            jumpLog: this.jumpLog,
            powerupLog: this.powerupLog,
            quizLog: this.quizLog,
            reason: this.gameOverReason,
            startTime: this.startTime,
            deadTime: this.deadTime
        };

        // logs and records
        // console.log(this.coinList);
        // console.log(this.trapList);
        console.log(this.coinsCollected);
        // console.log(this.quizLog);
        // console.log(this.powerupLog);
        // console.log(this.coinLog);
        // console.log(this.jumpLog);
        // console.log(this.gameOverReason);
        console.log(game_log);
        console.log('stringify: '+JSON.stringify(game_log));
        console.log(this.deadTime-this.startTime);

        jQuery.post(gameOptions.baseUrl+'update-event-game-point', {user_id: this.user_id, point: this.coinsCollected, game_log: JSON.stringify(game_log)}, function(data){
            var temp = JSON.parse(data).data;
            console.log('update event game point callback received:');
            temp.user_id = parseInt(temp.user_id);
            temp.qcoins = parseInt(temp.qcoins);
            temp.play_times_count = parseInt(temp.play_times_count);
            temp.play_times_earn = parseInt(temp.play_times_earn);
            console.log(temp);

            var _customEvent = new CustomEvent('update user', {detail: temp});
            window.dispatchEvent(_customEvent);
        });

        // this.scene.start("gamescene");
    }

});