<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_game_records
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_quake_club_game_records');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_quake_club_game_records'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_GAME_RECORDS_FORM_LBL_QUAKECLUBGAMERECORD_CREATED_ON'); ?></th>
			<td><?php echo $this->item->created_on; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_GAME_RECORDS_FORM_LBL_QUAKECLUBGAMERECORD_USER_ID'); ?></th>
			<td><?php echo $this->item->user_id_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_GAME_RECORDS_FORM_LBL_QUAKECLUBGAMERECORD_QCOINS'); ?></th>
			<td><?php echo $this->item->qcoins; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_GAME_RECORDS_FORM_LBL_QUAKECLUBGAMERECORD_MODIFIED_ON'); ?></th>
			<td><?php echo $this->item->modified_on; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_GAME_RECORDS_FORM_LBL_QUAKECLUBGAMERECORD_IS_WINNER'); ?></th>
			<td><?php echo $this->item->is_winner; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_GAME_RECORDS_FORM_LBL_QUAKECLUBGAMERECORD_PLAY_TIMES_COUNT'); ?></th>
			<td><?php echo $this->item->play_times_count; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_quake_club_game_records&task=quakeclubgamerecord.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_QUAKE_CLUB_GAME_RECORDS_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_quake_club_game_records.quakeclubgamerecord.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_QUAKE_CLUB_GAME_RECORDS_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_QUAKE_CLUB_GAME_RECORDS_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_QUAKE_CLUB_GAME_RECORDS_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_quake_club_game_records&task=quakeclubgamerecord.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_QUAKE_CLUB_GAME_RECORDS_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>