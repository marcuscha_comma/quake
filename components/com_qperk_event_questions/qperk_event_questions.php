<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Qperk_event_questions
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Qperk_event_questions', JPATH_COMPONENT);
JLoader::register('Qperk_event_questionsController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Qperk_event_questions');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
