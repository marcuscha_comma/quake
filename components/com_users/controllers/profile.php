<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');

/**
 * Profile controller class for Users.
 *
 * @since  1.6
 */
class UsersControllerProfile extends UsersController
{
	/**
	 * Method to check out a user for editing and redirect to the edit form.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	public function edit()
	{
		$app         = JFactory::getApplication();
		$user        = JFactory::getUser();
		$loginUserId = (int) $user->get('id');

		// Get the previous user id (if any) and the current user id.
		$previousId = (int) $app->getUserState('com_users.edit.profile.id');
		$userId     = $this->input->getInt('user_id');

		// Check if the user is trying to edit another users profile.
		if ($userId != $loginUserId)
		{
			$app->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'error');
			$app->setHeader('status', 403, true);

			return false;
		}

		$cookieLogin = $user->get('cookieLogin');

		// Check if the user logged in with a cookie
		if (!empty($cookieLogin))
		{
			// If so, the user must login to edit the password and other data.
			$app->enqueueMessage(JText::_('JGLOBAL_REMEMBER_MUST_LOGIN'), 'message');
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));

			return false;
		}

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_users.edit.profile.id', $userId);

		// Get the model.
		$model = $this->getModel('Profile', 'UsersModel');

		// Check out the user.
		if ($userId)
		{
			$model->checkout($userId);
		}

		// Check in the previous user.
		if ($previousId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit', false));

		return true;
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function save()
	{
		// Check for request forgeries.
		$this->checkToken();

		$app    = JFactory::getApplication();
		$model  = $this->getModel('Profile', 'UsersModel');
		$user   = JFactory::getUser();
		$userId = (int) $user->get('id');
		$userEmail = $user->get('email');

		// Get the user data.
		$requestData = $app->input->post->get('jform', array(), 'array');

		if ($requestData['dob']) {
			if($requestData['dob'] != "-"){
				$tmpDate = strtotime($requestData['dob']);
				$requestData['dob'] = date('Y-m-d H:i:s', $tmpDate);  
			}else{
				$requestData['dob'] = NULL;
			}
		}
		unset($requestData['dob']);
		unset($requestData['email1']);
		unset($requestData['email2']);

		// Force the ID to this user.
		$requestData['id'] = $userId;
		$requestData['modified_by'] = $userId;
		$requestData['mobileNo'] = htmlspecialchars($requestData['country_code'] .'-'.$requestData['contact_number']);
		$requestData['name'] = htmlspecialchars( $requestData['name'] );
		$requestData['address'] = htmlspecialchars( $requestData['address'] );
		$requestData['email1'] = $userEmail;
		$requestData['email2'] = $userEmail;
		// Validate the posted data.
		$form = $model->getForm();

		if (!$form)
		{
			JError::raiseError(500, $model->getError());

			return false;
		}

		// Send an object which can be modified through the plugin event
		$objData = (object) $requestData;
		$app->triggerEvent(
			'onContentNormaliseRequestData',
			array('com_users.user', $objData, $form)
		);
		$requestData = (array) $objData;
		$data = $requestData;

		// Validate the posted data.
		// $data = $model->validate($form, $requestData);

		// Check for errors.
		if ($data === false)
		{
			// Get the validation messages.
			$errors = $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				}
				else
				{
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			// Unset the passwords.
			unset($requestData['password1'], $requestData['password2']);

			// Save the data in the session.
			$app->setUserState('com_users.edit.profile.data', $requestData);

			// Redirect back to the edit screen.
			$userId = (int) $app->getUserState('com_users.edit.profile.id');
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));

			return false;
		}

		// Attempt to save the data.
		$return = $model->save($data);

		// Check for errors.
		if ($return === false)
		{
			// Save the data in the session.
			$app->setUserState('com_users.edit.profile.data', $data);

			// Redirect back to the edit screen.
			$userId = (int) $app->getUserState('com_users.edit.profile.id');
			$this->setMessage(JText::sprintf('COM_USERS_PROFILE_SAVE_FAILED', $model->getError()), 'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));


			return false;
		}

		// Redirect the user and adjust session state based on the chosen task.
		switch ($this->getTask())
		{
			case 'apply':
				// Check out the profile.
				$app->setUserState('com_users.edit.profile.id', $return);
				$model->checkout($return);

				// Redirect back to the edit screen.
				$this->setMessage(JText::_('COM_USERS_PROFILE_SAVE_SUCCESS'));

				$redirect = $app->getUserState('com_users.edit.profile.redirect');

				// Don't redirect to an external URL.
				if (!JUri::isInternal($redirect))
				{
					$redirect = null;
				}

				if (!$redirect)
				{
					$redirect = 'index.php?option=com_users&view=profile&layout=edit&hidemainmenu=1';
				}

				$this->setRedirect(JRoute::_($redirect, false));
				break;

			default:
				// Check in the profile.
				$userId = (int) $app->getUserState('com_users.edit.profile.id');

				if ($userId)
				{
					$model->checkin($userId);
				}

				// Clear the profile id from the session.
				$app->setUserState('com_users.edit.profile.id', null);

				$redirect = $app->getUserState('com_users.edit.profile.redirect');

				// Don't redirect to an external URL.
				if (!JUri::isInternal($redirect))
				{
					$redirect = null;
				}

				if (!$redirect)
				{
					$redirect = 'index.php?option=com_users&view=profile&user_id=' . $return;
				}

				// Redirect to the list screen.
				$this->setMessage(JText::_('COM_USERS_PROFILE_SAVE_SUCCESS'));
				$this->setRedirect(JRoute::_($redirect, false));
				break;
		}

		// Flush the data from the session.
		$app->setUserState('com_users.edit.profile.data', null);
	}

	public function companySave(){
		$this->checkToken();
		$app    = JFactory::getApplication();
		$user   = JFactory::getUser();
		$userId = (int) $user->get('id');
		$config = JFactory::getConfig();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$requestData = $app->input->post->get('jform', array(), 'array');
		$requestData['mobile_no'] = htmlspecialchars($requestData['country_code'] .'-'.$requestData['contact_number']);
		$requestData['name'] = htmlspecialchars( $requestData['name'] );
		$requestData['address'] = htmlspecialchars( $requestData['address'] );
		$requestData['classification'] = htmlspecialchars( $requestData['classification'] );
		$requestData['industry'] = htmlspecialchars( $requestData['industry'] );
		$requestData['designation'] = htmlspecialchars( $requestData['designation'] );

		$company = new stdClass();
		//Save Image
		if (($_FILES['business_card']['name']!="")){
			
			// Where the file is going to be stored
			$target_dir = "./images/business-cards";
			$file = $_FILES['business_card']['name'];
			$path = pathinfo($file);
			$filename = '/'.time();
			$ext = 'png';
			$temp_name = $_FILES['business_card']['tmp_name'];
			$path_filename_ext = $target_dir.$filename.".".$ext;
		
			// Check if file already exists
			if (file_exists($path_filename_ext)) {
				// echo "Sorry, file already exists.";
			}else{
				move_uploaded_file($temp_name,$path_filename_ext);
				// echo "Congratulations! File Uploaded Successfully.";
			}
			$company->business_card = $path_filename_ext;
		}
		$company->user_id = $userId;
		$company->industry = $requestData['industry'];
		$company->classification = $requestData['classification'];
		$company->designation = $requestData['designation'];
		$company->email = $requestData['email'];
		$company->address = $requestData['address'];
		$company->name = $requestData['name'];
		$company->mobile_no = $requestData['country_code'].'-'.$requestData['contact_number'];
		
		// Insert the object into the user profile table.
		$result = JFactory::getDbo()->updateObject('#__cus_qperks_company_user', $company, 'user_id');

		// if ($requestData['emailChangedStatus']) {
		// 	$db->setQuery('SELECT id FROM #__users where email="'.$requestData['email'].'"');
		// 	$checkEmailExist             = $db->loadResult();
		// 	if ($checkEmailExist) {
		// 		$app->enqueueMessage(JText::_('COM_USERS_REGISTER_EMAIL1_MESSAGE'),"warning");
		// 		$app->redirect(JUri::base().'user-profile/profile');
		// 	}

		// 	$originalMail = $user->email;
		// 	$user = new stdClass();
		// 	$user->id = $requestData['id'];
		// 	$user->email = $requestData['email'];
		// 	$userUpdateResult = JFactory::getDbo()->updateObject('#__users', $user, 'id');
		// 	if ($userUpdateResult) {
		// 		$sent = $this->_sendEmail($data['mailfrom'], $data['fromname'], $originalMail, $requestData['email']);
		// 	}
		// }

		if ($result) {
			$this->setMessage(JText::_('COM_USERS_COMPANY_PROFILE_SAVE_SUCCESS'));
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
			$app->setUserState('com_users.edit.profile.data', null);

		}else{
			$this->setMessage(JText::sprintf('COM_USERS_PROFILE_SAVE_FAILED', $model->getError()), 'warning');
			return false;
		}
	}

	public function userFacebookSave(){
		$this->checkToken();

		$app    = JFactory::getApplication();
		$user   = JFactory::getUser();
		$userId = (int) $user->get('id');
		$jdate = new JDate;
		$config = JFactory::getConfig();
		$offset = $config->get('offset');
		$date = new JDate('now', $offset);
		$convertedDate = date('Y-m-d h:i:s');


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$requestData = $app->input->post->get('jform', array(), 'array');

		$userData = new stdClass();
		$userData->facebook = $requestData['facebook'];
		$userData->id = $userId;

		// Insert the object into the user profile table.
		$result = JFactory::getDbo()->updateObject('#__users', $userData, 'id');

		$db->setQuery('SELECT count(id) FROM #__cus_qperks_user_point where user_id="'.$userId.'" and type=8');
		$checkSocialMediaPointAdded             = $db->loadResult();

		// $db->setQuery('SELECT count(id) as total FROM #__cus_qperks_user_point WHERE type in(7,3,1,8) and state > 0 and user_id = '.$userId.' and date(created_on) = "'.date("Y-m-d").'"');
		// $times = $db->loadResult();

		if ($result) {
			if ($checkSocialMediaPointAdded == 0) {
				$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_user_point');
				$max             = $db->loadResult();

				$profile1 = new stdClass();
				$profile1->user_id = $userId;
				$profile1->point=10;
				$profile1->type=8;
				$profile1->state=1;
				$profile1->created_by=$userId;
				$profile1->created_on=$convertedDate;
				$profile1->modified_by=$userId;
				$profile1->ordering=$max + 1;

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__cus_qperks_user_point', $profile1);

				$month = 'month'.date('m');
				$db->setQuery('SELECT id FROM #__cus_quake_club_qperks_monthly where user_id = '. $userId .' AND year ="'.date('Y').'"' );
				$monthly_id             = $db->loadResult();
				$db->setQuery('SELECT '.$month.' FROM #__cus_quake_club_qperks_monthly where user_id = '. $userId .' AND year ="'.date('Y').'"' );
				$monthly_point            = $db->loadResult();
				
				$monthly = new stdClass();
				$monthly->$month = $monthly_point + 10;
				$monthly->id = $monthly_id;

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');
			}

			$app->enqueueMessage(JText::_('COM_USERS_PROFILE_SAVE_SUCCESS'));
			$app->redirect(JUri::base().'user-profile/profile');
			$app->setUserState('com_users.edit.profile.data', null);

		}else{
			$app->enqueueMessage(JText::_('COM_USERS_PROFILE_SAVE_FAILED','Fail'), 'warning');
			$app->redirect(JUri::base().'user-profile/profile');
		}
	}

	public function userWhatsappSave(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
    JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$userId     = $user->get('id');
		$jdate = new JDate;
		$jinput = JFactory::getApplication()->input;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$whatsapp = $jinput->get('whatsapp','', 'String');
		$config = JFactory::getConfig();
		$offset = $config->get('offset');
		$date = new JDate('now', $offset);
		$convertedDate = date('Y-m-d h:i:s');

		$userData = new stdClass();
		$userData->whatsapp = $whatsapp;
		$userData->id = $userId;

		// Insert the object into the user profile table.
		$result = JFactory::getDbo()->updateObject('#__users', $userData, 'id');

		$db->setQuery('SELECT count(id) FROM #__cus_qperks_user_point where user_id="'.$userId.'" and type=12');
		$checkSocialMediaPointAdded             = $db->loadResult();

		if ($result) {
			if ($checkSocialMediaPointAdded == 0) {
				$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_user_point');
				$max             = $db->loadResult();

				$profile1 = new stdClass();
				$profile1->user_id = $userId;
				$profile1->point=20;
				$profile1->type=12;
				$profile1->state=1;
				$profile1->created_by=$userId;
				$profile1->created_on=$convertedDate;
				$profile1->modified_by=$userId;
				$profile1->ordering=$max + 1;

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__cus_qperks_user_point', $profile1);

				$month = 'month'.date('m');
				$db->setQuery('SELECT id FROM #__cus_quake_club_qperks_monthly where user_id = '. $userId .' AND year ="'.date('Y').'"' );
				$monthly_id             = $db->loadResult();
				$db->setQuery('SELECT '.$month.' FROM #__cus_quake_club_qperks_monthly where user_id = '. $userId .' AND year ="'.date('Y').'"' );
				$monthly_point            = $db->loadResult();
				
				$monthly = new stdClass();
				$monthly->$month = $monthly_point + 20;
				$monthly->id = $monthly_id;

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');
			}
			echo 1;
		}else{
			echo 0;
		}
		JFactory::getApplication()->close();

	}
}
