<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Profile view class for Users.
 *
 * @since  1.6
 */
class UsersViewProfile extends JViewLegacy
{
	protected $data;

	protected $form;

	protected $params;

	protected $state;

	/**
	 * An instance of JDatabaseDriver.
	 *
	 * @var    JDatabaseDriver
	 * @since  3.6.3
	 */
	protected $db;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		$user = JFactory::getUser();
		$quarter_month = "";
		$quarter_month_previous = "";
		$quarter_year = "";
		$quarter_year_previous = "";

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$db->setQuery('SELECT * FROM #__cus_qperks_company_user WHERE user_id = "'. $user->id.'"');
		$this->company = $db->loadObject();

		// Get the view data.
		$this->data	        = $this->get('Data');
		$this->form	        = $this->getModel()->getForm(new JObject(array('id' => $user->id)));
		$this->state            = $this->get('State');
		$this->params           = $this->state->get('params');
		$this->twofactorform    = $this->get('Twofactorform');
		$this->twofactormethods = UsersHelper::getTwoFactorMethods();
		$this->otpConfig        = $this->get('OtpConfig');
		$this->db               = JFactory::getDbo();
		$this->calProfileComplete = $this->getModel()->calProfileComplete();
		$this->quarter_date_html = "";
		$this->quarter_expire_text = "";
		$this->previous_quarter_expire_text = "";

		switch (date('m')) {
			case 1:
			case 2:
			case 3:
				$this->quarter_date_html ='<h5 class="f-14">01 Jan - 31 Mar</h5>';
				$this->previous_quarter_date_html ='<h5 class="f-14">Oct Jul - 31 Dec</h5>';
				$quarter_month_previous = 4;
				$quarter_year_previous = date("Y", strtotime('-1 year'));
				$quarter_month = 1;
				$quarter_year = date("Y");
				$this->quarter_expire_text = "31 Mar ".date("Y", strtotime('+1 year'));
				$this->previous_quarter_expire_text = "31 Dec ".date("Y", strtotime('+1 year'));
				break;
			case 4:
			case 5:
			case 6:
				$this->quarter_date_html ='<h5 class="f-14">01 Apr - 30 Jun</h5>';
				$this->previous_quarter_date_html ='<h5 class="f-14">01 Jan - 31 Mar</h5>';
				$quarter_month_previous = 1;
				$quarter_year_previous = date("Y");
				$quarter_month = 2;
				$quarter_year = date("Y");
				$this->quarter_expire_text = "30 Jun ".date("Y", strtotime('+1 year'));
				$this->previous_quarter_expire_text = "31 Mar ".date("Y", strtotime('+1 year'));
				break;
			case 7:
			case 8:
			case 9:
				$this->quarter_date_html ='<h5 class="f-14">01 Jul - 30 Sept</h5>';
				$this->previous_quarter_date_html ='<h5 class="f-14">01 Apr - 30 Jun</h5>';
				$quarter_month_previous = 2;
				$quarter_year_previous = date("Y");
				$quarter_month = 3;
				$quarter_year = date("Y");
				$this->quarter_expire_text = "30 Sept ".date("Y", strtotime('+1 year'));
				$this->previous_quarter_expire_text = "31 Jun ".date("Y", strtotime('+1 year'));
				break;
			case 10:
			case 11:
			case 12:
				$this->quarter_date_html ='<h5 class="f-14">01 Oct - 31 Dec</h5>';
				$this->previous_quarter_date_html ='<h5 class="f-14">01 Jul - 30 Sept</h5>';
				$quarter_month_previous = 3;
				$quarter_year_previous = date("Y");
				$quarter_month = 4;
				$quarter_year = date("Y");
				$this->quarter_expire_text = "31 Dec ".date("Y", strtotime('+1 year'));
				$this->previous_quarter_expire_text = "31 Sept ".date("Y", strtotime('+1 year'));
				break;
			default:
				# code...
				break;
		}
		$db    = JFactory::getDBO();
		// $db->setQuery('SELECT '.$quarter_month.' as point FROM #__cus_quake_club_qperks_monthly WHERE state > 0 and user_id ='.$user->id.' and year = '.$quarter_year);
		$db->setQuery('SELECT expired_point as point FROM #__cus_qperks_user_point_expired WHERE user_id ='.$user->id.' and year = '.$quarter_year. ' and quarter='.$quarter_month);
		$this->quarter_month_point = $db->loadResult();
		
		$db    = JFactory::getDBO();
		// $db->setQuery('SELECT '.$quarter_month_previous.' as point FROM #__cus_quake_club_qperks_monthly WHERE state > 0 and user_id ='.$user->id.' and year = '.$quarter_year_previous);
		$db->setQuery('SELECT expired_point as point FROM #__cus_qperks_user_point_expired WHERE user_id ='.$user->id.' and year = '.$quarter_year_previous. ' and quarter='.$quarter_month_previous);

		$this->previous_quarter_month_point = $db->loadResult();
		
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// View also takes responsibility for checking if the user logged in with remember me.
		$cookieLogin = $user->get('cookieLogin');

		if (!empty($cookieLogin))
		{
			// If so, the user must login to edit the password and other data.
			// What should happen here? Should we force a logout which destroys the cookies?
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('JGLOBAL_REMEMBER_MUST_LOGIN'), 'message');
			$app->redirect(JRoute::_('index.php?option=com_users&view=login', false));

			return false;
		}

		// Check if a user was found.
		if (!$this->data->id)
		{
			JError::raiseError(404, JText::_('JERROR_USERS_PROFILE_NOT_FOUND'));

			return false;
		}

		JPluginHelper::importPlugin('content');
		$this->data->text = '';
		JEventDispatcher::getInstance()->trigger('onContentPrepare', array ('com_users.user', &$this->data, &$this->data->params, 0));
		unset($this->data->text);

		// Check for layout override
		$active = JFactory::getApplication()->getMenu()->getActive();

		if (isset($active->query['layout']))
		{
			$this->setLayout($active->query['layout']);
		}

		// Escape strings for HTML output
		$this->pageclass_sfx = htmlspecialchars($this->params->get('pageclass_sfx'));

		$this->prepareDocument();

		return parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$user  = JFactory::getUser();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $user->name));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_USERS_PROFILE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
