<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;

/**
 * Registration model class for Users.
 *
 * @since  1.6
 */
class UsersModelRegistration extends JModelForm
{
	/**
	 * @var    object  The user registration data.
	 * @since  1.6
	 */
	protected $data;

	/**
	 * Constructor
	 *
	 * @param   array  $config  An array of configuration options (name, state, dbo, table_path, ignore_request).
	 *
	 * @since   3.6
	 *
	 * @throws  Exception
	 */
	public function __construct($config = array())
	{
		$config = array_merge(
			array(
				'events_map' => array('validate' => 'user')
			), $config
		);

		parent::__construct($config);
	}

	/**
	 * Method to get the user ID from the given token
	 *
	 * @param   string  $token  The activation token.
	 *
	 * @return  mixed   False on failure, id of the user on success
	 *
	 * @since   3.8.13
	 */
	public function getUserIdFromToken($token)
	{
		$db = $this->getDbo();

		// Get the user id based on the token.
		$query = $db->getQuery(true);
		$query->select($db->quoteName('id'))
			->from($db->quoteName('#__users'))
			->where($db->quoteName('activation') . ' = ' . $db->quote($token))
			->where($db->quoteName('block') . ' = ' . 1)
			->where($db->quoteName('lastvisitDate') . ' = ' . $db->quote($db->getNullDate()));
		$db->setQuery($query);

		try
		{
			return (int) $db->loadResult();
		}
		catch (RuntimeException $e)
		{
			$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);

			return false;
		}
	}

	/**
	 * Method to activate a user account.
	 *
	 * @param   string  $token  The activation token.
	 *
	 * @return  mixed    False on failure, user object on success.
	 *
	 * @since   1.6
	 */
	public function activate($token)
	{
		$config     = JFactory::getConfig();
		$userParams = JComponentHelper::getParams('com_users');
		$userId     = $this->getUserIdFromToken($token);

		// Check for a valid user id.
		if (!$userId)
		{
			$this->setError(JText::_('COM_USERS_ACTIVATION_TOKEN_NOT_FOUND'));

			return false;
		}

		// Load the users plugin group.
		JPluginHelper::importPlugin('user');

		// Activate the user.
		$user = JFactory::getUser($userId);

		// Admin activation is on and user is verifying their email
		if (($userParams->get('useractivation') == 2) && !$user->getParam('activate', 0))
		{
			$linkMode = $config->get('force_ssl', 0) == 2 ? Route::TLS_FORCE : Route::TLS_IGNORE;

			// Compile the admin notification mail values.
			$data = $user->getProperties();
			$data['activation'] = JApplicationHelper::getHash(JUserHelper::genRandomPassword());
			$user->set('activation', $data['activation']);
			$data['siteurl'] = JUri::base();
			$data['activate'] = JRoute::link(
				'site',
				'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
				false,
				$linkMode,
				true
			);

			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$user->setParam('activate', 1);
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_SUBJECT',
				$data['name'],
				$data['sitename']
			);

			$emailBody = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_BODY',
				$data['sitename'],
				$data['name'],
				$data['email'],
				$data['username'],
				$data['activate']
			);

			// Get all admin users
			$db = $this->getDbo();
			$query = $db->getQuery(true)
				->select($db->quoteName(array('name', 'email', 'sendEmail', 'id')))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('sendEmail') . ' = 1')
				->where($db->quoteName('block') . ' = 0');

			$db->setQuery($query);

			try
			{
				$rows = $db->loadObjectList();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);

				return false;
			}

			// Send mail to all users with users creating permissions and receiving system emails
			foreach ($rows as $row)
			{
				$usercreator = JFactory::getUser($row->id);

				if ($usercreator->authorise('core.create', 'com_users') && $usercreator->authorise('core.manage', 'com_users'))
				{
					$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBody);

					// Check for an error.
					if ($return !== true)
					{
						$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));

						return false;
					}
				}
			}
		}
		// Admin activation is on and admin is activating the account
		elseif (($userParams->get('useractivation') == 2) && $user->getParam('activate', 0))
		{
			$user->set('activation', '');
			$user->set('block', '0');

			// Compile the user activated notification mail values.
			$data = $user->getProperties();
			$user->setParam('activate', 0);
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$data['siteurl'] = JUri::base();
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_SUBJECT',
				$data['name'],
				$data['sitename']
			);

			$emailBody = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_BODY',
				$data['name'],
				$data['siteurl'],
				$data['username']
			);

			$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

			// Check for an error.
			if ($return !== true)
			{
				$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));

				return false;
			}
		}
		else
		{
			$user->set('activation', '');
			$user->set('block', '0');
		}

		// Store the user object.
		if (!$user->save())
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_ACTIVATION_SAVE_FAILED', $user->getError()));

			return false;
		}

		return $user;
	}

	/**
	 * Method to get the registration form data.
	 *
	 * The base form data is loaded and then an event is fired
	 * for users plugins to extend the data.
	 *
	 * @return  mixed  Data object on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getData()
	{
		if ($this->data === null)
		{
			$this->data = new stdClass;
			$app = JFactory::getApplication();
			$params = JComponentHelper::getParams('com_users');

			// Override the base user data with any data in the session.
			$temp = (array) $app->getUserState('com_users.registration.data', array());

			// Don't load the data in this getForm call, or we'll call ourself
			$form = $this->getForm(array(), false);

			foreach ($temp as $k => $v)
			{
				// Here we could have a grouped field, let's check it
				if (is_array($v))
				{
					$this->data->$k = new stdClass;

					foreach ($v as $key => $val)
					{
						if ($form->getField($key, $k) !== false)
						{
							$this->data->$k->$key = $val;
						}
					}
				}
				// Only merge the field if it exists in the form.
				elseif ($form->getField($k) !== false)
				{
					$this->data->$k = $v;
				}
			}

			// Get the groups the user should be added to after registration.
			$this->data->groups = array();

			// Get the default new user group, guest or public group if not specified.
			$system = $params->get('new_usertype', $params->get('guest_usergroup', 1));

			$this->data->groups[] = $system;

			// Unset the passwords.
			unset($this->data->password1, $this->data->password2);

			// Get the dispatcher and load the users plugins.
			$dispatcher = JEventDispatcher::getInstance();
			JPluginHelper::importPlugin('user');

			// Trigger the data preparation event.
			$results = $dispatcher->trigger('onContentPrepareData', array('com_users.registration', $this->data));

			// Check for errors encountered while preparing the data.
			if (count($results) && in_array(false, $results, true))
			{
				$this->setError($dispatcher->getError());
				$this->data = false;
			}
		}

		return $this->data;
	}

	/**
	 * Method to get the registration form.
	 *
	 * The base form is loaded from XML and then an event is fired
	 * for users plugins to extend the form with extra fields.
	 *
	 * @param   array    $data      An optional array of data for the form to interrogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_users.registration', 'registration', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form))
		{
			return false;
		}

		// When multilanguage is set, a user's default site language should also be a Content Language
		if (JLanguageMultilang::isEnabled())
		{
			$form->setFieldAttribute('language', 'type', 'frontend_language', 'params');
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = $this->getData();

		if (JLanguageMultilang::isEnabled() && empty($data->language))
		{
			$data->language = JFactory::getLanguage()->getTag();
		}

		$this->preprocessData('com_users.registration', $data);

		return $data;
	}

	/**
	 * Override preprocessForm to load the user plugin group instead of content.
	 *
	 * @param   JForm   $form   A JForm object.
	 * @param   mixed   $data   The data expected for the form.
	 * @param   string  $group  The name of the plugin group to import (defaults to "content").
	 *
	 * @return  void
	 *
	 * @since   1.6
	 * @throws  Exception if there is an error in the form event.
	 */
	protected function preprocessForm(JForm $form, $data, $group = 'user')
	{
		$userParams = JComponentHelper::getParams('com_users');

		// Add the choice for site language at registration time
		if ($userParams->get('site_language') == 1 && $userParams->get('frontend_userparams') == 1)
		{
			$form->loadFile('sitelang', false);
		}

		parent::preprocessForm($form, $data, $group);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		// Get the application object.
		$app = JFactory::getApplication();
		$params = $app->getParams('com_users');

		// Load the parameters.
		$this->setState('params', $params);
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $temp  The form data.
	 *
	 * @return  mixed  The user id on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function register($temp)
	{
		$params = JComponentHelper::getParams('com_users');
		$jdate = new JDate;
		$isRegister = false;
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$config = JFactory::getConfig();
		$offset = $config->get('offset');
		$date = new JDate('now', $offset);
		$convertedDate = date('Y-m-d h:i:s');


		$data = (array) $this->getData();
		// Merge in the registration data.
		foreach ($temp as $k => $v)
		{
			$data[$k] = $v;
		}

		// Initialise the table with JUser.
		$user = new JUser;

		$db->setQuery('SELECT u.id FROM #__users u join #__cus_qperks_company_user cu on cu.user_id = u.id where u.quakeClubUser = -1 and u.email = "'.$data['email1'].'"');
		$checkUserDetail             = $db->loadResult();

		$db->setQuery('SELECT u.id FROM #__users u join #__cus_qperks_company_user cu on cu.user_id = u.id where u.quakeClubUser = 1 and u.email = "'.$data['email1'].'"');
		$checkUserDetailExist             = $db->loadResult();

		if ($checkUserDetailExist) {
			return false;
		}

		if ($checkUserDetail) {
			$data['id'] = $checkUserDetail;
		}
			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			// die();

		// Prepare the data for the user object.
		$data['email'] = JStringPunycode::emailToPunycode($data['email1']);
		$data['password'] = $data['password1'];
		$useractivation = $params->get('useractivation');
		$sendpassword = $params->get('sendpassword', 1);

		// Check if the user needs to activate their account.
		if (($useractivation == 1) || ($useractivation == 2))
		{
			$data['activation'] = JApplicationHelper::getHash(JUserHelper::genRandomPassword());
			$data['block'] = 1;
		}

		// Bind the data.
		if (!$user->bind($data))
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));

			return false;
		}
		// Load the users plugin group.
		JPluginHelper::importPlugin('user');

		// Store the data.
		if (!$user->save())
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));

			return false;
		}

		$config = JFactory::getConfig();

		$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_user_point');
		$max             = $db->loadResult();

		$profile1 = new stdClass();
		$profile1->user_id = $user->id;
		$profile1->point=100;
		$profile1->type=6;
		$profile1->state=1;
		$profile1->created_by=$user->id;
		$profile1->created_on=$convertedDate;
		$profile1->modified_by=$user->id;
		$profile1->ordering=$max + 1;

		// Insert the object into the user profile table.
		$result = JFactory::getDbo()->insertObject('#__cus_qperks_user_point', $profile1);

		$db->setQuery('SELECT id FROM #__cus_quake_club_qperks_monthly where user_id = '. $user->id .' AND year ="'.date('Y').'"' );
		$monthly_id             = $db->loadResult();
		$month = 'month'.date('m');
		if ( !$monthly_id ) {
			$db->setQuery('SELECT MAX(ordering) FROM #__cus_quake_club_qperks_monthly');
			$max             = $db->loadResult();
			$month = 'month'.date('m');
			$monthly = new stdClass();
			$monthly->user_id = $user->id;
			$monthly->$month = 100;
			$monthly->year = date('Y');
			$monthly->state=1;
			$monthly->created_by=$user->id;
			$monthly->modified_by=$user->id;
			$monthly->ordering=$max + 1;
			$result = JFactory::getDbo()->insertObject('#__cus_quake_club_qperks_monthly', $monthly);
		}else{
			$db->setQuery('SELECT '.$month.' FROM #__cus_quake_club_qperks_monthly where user_id = '. $user->id .' AND year ="'.date('Y').'"' );
				$monthly_point            = $db->loadResult();

				$monthly = new stdClass();
				$monthly->$month = $monthly_point + 100;
				$monthly->id = $monthly_id;

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');
		}

		// Insert the object into the user profile table.
		

		$db->setQuery('SELECT id FROM #__cus_qperks_company_user where user_id='.$user->id);
		$checkUserCompany             = $db->loadResult();
		if ($checkUserCompany) {
			$company = new stdClass();
			$company->industry = $data['industry'];
			$company->name = $data['company'];
			$company->classification = $data['classification'];
			$company->designation = $data['designation'];
			$company->id = $checkUserCompany;
			$result = JFactory::getDbo()->updateObject('#__cus_qperks_company_user', $company, 'id');

		}else{
			$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_company_user');
			$max             = $db->loadResult();

			$company = new stdClass();
			$company->user_id = $user->id;
			$company->industry = $data['industry'];
			$company->name = $data['company'];
			$company->classification = $data['classification'];
			$company->designation = $data['designation'];
			$company->mobile_no = "-";
			$company->email = $user->email;
			$company->state=1;
			$company->created_by=$user->id;
			$company->modified_by=$user->id;
			$company->ordering=$max + 1;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_qperks_company_user', $company);
		}



		$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_referrals');
		$max             = $db->loadResult();
		$db->setQuery('SELECT user_id FROM #__cus_qperks_referrals_session where referral_code = "'.$data['referral_code'].'"');
		$referrer_id             = $db->loadResult();

		$db->setQuery('SELECT count(id) as total FROM #__cus_qperks_referrals where referrer_id = "'.$referrer_id.'"');
		$referrer_id_count       = $db->loadResult();

		if ($referrer_id != "" && $referrer_id_count < 10) {
			$referral = new stdClass();
			$referral->referrer_id = $referrer_id;
			$referral->referred_id = $user->id;
			$referral->state=1;
			$referral->created_by=$user->id;
			$referral->modified_by=$user->id;
			$referral->ordering=$max + 1;
			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_qperks_referrals', $referral);

			$month = 'month'.date('m');
			$db->setQuery('SELECT id FROM #__cus_quake_club_qperks_monthly where user_id = '. $referrer_id .' AND year ="'.date('Y').'"' );
			$monthly_id             = $db->loadResult();
			$db->setQuery('SELECT '.$month.' FROM #__cus_quake_club_qperks_monthly where user_id = '. $referrer_id .' AND year ="'.date('Y').'"' );
			$monthly_point            = $db->loadResult();

			$monthly = new stdClass();
			$monthly->$month = $monthly_point + 50;
			$monthly->id = $monthly_id;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');
			$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_user_point');
			$max             = $db->loadResult();

			$profile1 = new stdClass();
			$profile1->user_id = $referrer_id;
			$profile1->point=50;
			$profile1->type=2;
			$profile1->state=1;
			$profile1->source=$user->id;
			$profile1->created_by=$referrer_id;
			$profile1->created_on=$convertedDate;
			$profile1->modified_by=$referrer_id;
			$profile1->ordering=$max + 1;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_qperks_user_point', $profile1);
		}

		// Compile the notification mail values.
		$data = $user->getProperties();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$data['siteurl'] = JUri::root();

		// Handle account activation/confirmation emails.
		if ($useractivation == 2)
		{
			// Set the link to confirm the user email.
			$uri = JUri::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);

			// Remove administrator/ from activate URL in case this method is called from admin
			if (JFactory::getApplication()->isClient('administrator'))
			{
				$adminPos         = strrpos($data['activate'], 'administrator/');
				$data['activate'] = substr_replace($data['activate'], '', $adminPos, 14);
			}

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username']
				);
			}
		}
		elseif ($useractivation == 1)
		{
			// Set the link to activate the user account.
			$uri = JUri::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);

			// Remove administrator/ from activate URL in case this method is called from admin
			if (JFactory::getApplication()->isClient('administrator'))
			{
				$adminPos         = strrpos($data['activate'], 'administrator/');
				$data['activate'] = substr_replace($data['activate'], '', $adminPos, 14);
			}

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username']
				);
			}
		}
		else
		{
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_BODY',
					$data['name'],
					$data['sitename'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['siteurl']
				);
				$isRegister = true;
			}
		}

		// Send the registration email.
		if ($isRegister) {
			$return = $this->_sendEmail($data['mailfrom'], $data['fromname'], $user->email, $data['name']);
		}else{
			$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);
		}

		// Send Notification mail to administrators
		if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1))
		{
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			$emailBodyAdmin = JText::sprintf(
				'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
				$data['name'],
				$data['username'],
				$data['siteurl']
			);

			// Get all admin users
			$query->clear()
				->select($db->quoteName(array('name', 'email', 'sendEmail')))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('sendEmail') . ' = 1')
				->where($db->quoteName('block') . ' = 0');

			$db->setQuery($query);

			try
			{
				$rows = $db->loadObjectList();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);

				return false;
			}

			// Send mail to all superadministrators id
			foreach ($rows as $row)
			{
				$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);

				// Check for an error.
				if ($return !== true)
				{
					$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));

					return false;
				}
			}
		}

		// Check for an error.
		if ($return !== true)
		{
			$this->setError(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));

			// Send a system message to administrators receiving system mails
			$db = $this->getDbo();
			$query->clear()
				->select($db->quoteName('id'))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('block') . ' = ' . (int) 0)
				->where($db->quoteName('sendEmail') . ' = ' . (int) 1);
			$db->setQuery($query);

			try
			{
				$userids = $db->loadColumn();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);

				return false;
			}

			if (count($userids) > 0)
			{
				$jdate = new JDate;

				// Build the query to add the messages
				foreach ($userids as $userid)
				{
					$values = array(
						$db->quote($userid),
						$db->quote($userid),
						$db->quote($convertedDate),
						$db->quote(JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT')),
						$db->quote(JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username']))
					);
					$query->clear()
						->insert($db->quoteName('#__messages'))
						->columns($db->quoteName(array('user_id_from', 'user_id_to', 'date_time', 'subject', 'message')))
						->values(implode(',', $values));
					$db->setQuery($query);

					try
					{
						$db->execute();
					}
					catch (RuntimeException $e)
					{
						$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);

						return false;
					}
				}
			}

			return false;
		}

		if ($useractivation == 1)
		{
			return 'useractivate';
		}
		elseif ($useractivation == 2)
		{
			return 'adminactivate';
		}
		else
		{
			return $user->id;
		}
	}

	private function _sendEmail($mailfrom, $fromname, $email, $name)
	{
		$user   = JFactory::getUser();
		$jinput = JFactory::getApplication()->input;
		$emailSubject = "Thank you for your registration.";
		$emailBody = '<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

		<head>
		  <!-- NAME: 1 COLUMN -->
		  <!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
		  <meta charset="UTF-8">
		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <title>Astro Quake Club - Registration</title>

		  <style type="text/css">
			p {
			  margin: 5px 0;
			  padding: 0;
			}

			table {
			  border-collapse: collapse;
			}

			h1,
			h2,
			h3,
			h4,
			h5,
			h6 {
			  display: block;
			  margin: 0;
			  padding: 0;
			}

			img,
			a img {
			  border: 0;
			  height: auto;
			  outline: none;
			  text-decoration: none;
			}

			body,
			#bodyTable,
			#bodyCell {
			  height: 100%;
			  margin: 0;
			  padding: 0;
			  width: 100%;
			}

			.mcnPreviewText {
			  display: none !important;
			}

			#outlook a {
			  padding: 0;
			}

			img {
			  -ms-interpolation-mode: bicubic;
			}

			table {
			  mso-table-lspace: 0pt;
			  mso-table-rspace: 0pt;
			}

			.ReadMsgBody {
			  width: 100%;
			}

			.ExternalClass {
			  width: 100%;
			}

			p,
			a,
			li,
			td,
			blockquote {
			  mso-line-height-rule: exactly;
			}

			a[href^=tel],
			a[href^=sms] {
			  color: inherit;
			  cursor: default;
			  text-decoration: none;
			}

			p,
			a,
			li,
			td,
			body,
			table,
			blockquote {
			  -ms-text-size-adjust: 100%;
			  -webkit-text-size-adjust: 100%;
			}

			.ExternalClass,
			.ExternalClass p,
			.ExternalClass td,
			.ExternalClass div,
			.ExternalClass span,
			.ExternalClass font {
			  line-height: 100%;
			}

			a[x-apple-data-detectors] {
			  color: inherit !important;
			  text-decoration: none !important;
			  font-size: inherit !important;
			  font-family: inherit !important;
			  font-weight: inherit !important;
			  line-height: inherit !important;
			}

			#bodyCell {
			  padding: 20px;
			}

			.templateContainer {
			  max-width: 600px !important;
			}

			a.mcnButton {
			  display: block;
			}

			.mcnImage,
			.mcnRetinaImage {
			  vertical-align: bottom;
			}

			.mcnTextContent {
			  word-break: break-word;
			}

			.mcnTextContent img {
			  height: auto !important;
			}

			.mcnDividerBlock {
			  table-layout: fixed !important;
			}

			body,
			#bodyTable {

			  background-color: #fafafa;
			}

			#bodyCell {

			  border-top: 0;
			}

			.templateContainer {

			  border: 0;
			}

			h1 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 26px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h2 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 22px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h3 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 20px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			h4 {

			  color: #202020;

			  font-family: Helvetica;

			  font-size: 18px;

			  font-style: normal;

			  font-weight: bold;

			  line-height: 125%;

			  letter-spacing: normal;

			  text-align: left;
			}

			#templatePreheader {

			  background-color: #fafafa;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0px;
			}
			#templatePreheader .mcnTextContent,
			#templatePreheader .mcnTextContent p {

			  color: #000000;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 10px;

			  line-height: 150%;

			  text-align: center;
			}

			#templatePreheader .mcnTextContent a,
			#templatePreheader .mcnTextContent p a {

			  color: #000000;

			  font-weight: normal;

			  text-decoration: underline;
			}
			#templateHeader {

			  background-color: #ffffff;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0;
			}

			#templateHeader .mcnTextContent,
			#templateHeader .mcnTextContent p {

			  color: #202020;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 16px;

			  line-height: 150%;

			  text-align: left;
			}

			#templateHeader .mcnTextContent a,
			#templateHeader .mcnTextContent p a {

			  color: #007C89;

			  font-weight: normal;

			  text-decoration: underline;
			}

			#templateBody {

			  background-color: #ffffff;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 2px solid #EAEAEA;

			  padding-top: 20px;

			  padding-bottom: 20px;
			}

			#templateBody .mcnTextContent,
			#templateBody .mcnTextContent p {

			  color: #000000;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 16px;

			  line-height: 150%;

			  text-align: center;
			}

			#templateBody .mcnTextContent a,
			#templateBody .mcnTextContent p a {

			  color: #000000;

			  font-weight: normal;

			  text-decoration: none;
			}

			#templateFooter {

			  background-color: #000000;

			  background-image: none;

			  background-repeat: no-repeat;

			  background-position: center;

			  background-size: cover;

			  border-top: 0;

			  border-bottom: 0;

			  padding-top: 0px;

			  padding-bottom: 0px;
			}
			#templateFooter .mcnTextContent,
			#templateFooter .mcnTextContent p {

			  color: #ffffff;

			  font-family: Helvetica, Arial, sans-serif;

			  font-size: 9px;

			  line-height: 150%;

			  text-align: center;
			}

			#templateFooter .mcnTextContent a,
			#templateFooter .mcnTextContent p a {

			  color: #ffffff;

			  font-weight: normal;

			  text-decoration: none;
			}

			@media only screen and (min-width:768px) {
			  .templateContainer {
				width: 600px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  body,
			  table,
			  td,
			  p,
			  a,
			  li,
			  blockquote {
				-webkit-text-size-adjust: none !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  body {
				width: 100% !important;
				min-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #bodyCell {
				padding-top: 10px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnRetinaImage {
				max-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImage {
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnCartContainer,
			  .mcnCaptionTopContent,
			  .mcnRecContentContainer,
			  .mcnCaptionBottomContent,
			  .mcnTextContentContainer,
			  .mcnBoxedTextContentContainer,
			  .mcnImageGroupContentContainer,
			  .mcnCaptionLeftTextContentContainer,
			  .mcnCaptionRightTextContentContainer,
			  .mcnCaptionLeftImageContentContainer,
			  .mcnCaptionRightImageContentContainer,
			  .mcnImageCardLeftTextContentContainer,
			  .mcnImageCardRightTextContentContainer,
			  .mcnImageCardLeftImageContentContainer,
			  .mcnImageCardRightImageContentContainer {
				max-width: 100% !important;
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnBoxedTextContentContainer {
				min-width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupContent {
				padding: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnCaptionLeftContentOuter .mcnTextContent,
			  .mcnCaptionRightContentOuter .mcnTextContent {
				padding-top: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardTopImageContent,
			  .mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,
			  .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
				padding-top: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardBottomImageContent {
				padding-bottom: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockInner {
				padding-top: 0 !important;
				padding-bottom: 0 !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageGroupBlockOuter {
				padding-top: 9px !important;
				padding-bottom: 9px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnTextContent,
			  .mcnBoxedTextContentColumn {
				padding-right: 18px !important;
				padding-left: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnImageCardLeftImageContent,
			  .mcnImageCardRightImageContent {
				padding-right: 18px !important;
				padding-bottom: 0 !important;
				padding-left: 18px !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcpreview-image-uploader {
				display: none !important;
				width: 100% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  h1 {

				font-size: 22px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  h2 {

				font-size: 20px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  h3 {

				font-size: 18px !important;

				line-height: 125% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  h4 {

				font-size: 16px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  .mcnBoxedTextContentContainer .mcnTextContent,
			  .mcnBoxedTextContentContainer .mcnTextContent p {

				font-size: 14px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #templatePreheader {

				display: block !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #templatePreheader .mcnTextContent,
			  #templatePreheader .mcnTextContent p {

				font-size: 10px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #templateHeader .mcnTextContent,
			  #templateHeader .mcnTextContent p {

				font-size: 16px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #templateBody .mcnTextContent,
			  #templateBody .mcnTextContent p {

				font-size: 14px !important;

				line-height: 150% !important;
			  }

			}

			@media only screen and (max-width: 480px) {
			  #templateFooter .mcnTextContent,
			  #templateFooter .mcnTextContent p {

				font-size: 9px !important;

				line-height: 150% !important;
			  }

			}
		  </style>
		</head>

		<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fafafa;">
		  <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Thank you for your registration at Quake Club!</span>
		  <!--<![endif]-->
		  <center>
			<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #fafafa;">
			  <tr>
				<td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 20px;width: 100%;border-top: 0;">
				  <!-- BEGIN TEMPLATE // -->
				  <!--[if (gte mso 9)|(IE)]>
								<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
								<tr>
								<td align="center" valign="top" width="600" style="width:600px;">
								<![endif]-->
				  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
					<tr>
					  <td valign="top" id="templateHeader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnImageBlockOuter">
							<tr>
							  <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
								<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


										<img align="center" alt="" src="http://quake.com.my/images/edm/edm-header.png" width="600" style="max-width: 600px;padding-bottom: 0px;vertical-align: bottom;display: inline !important;border-radius: 0%;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


									  </td>
									</tr>
								  </tbody>
								</table>
							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnImageBlockOuter">
							<tr>
							  <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
								<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">


										<img align="center" alt="Welcome to Quake Club" src="http://quake.com.my/images/edm/main-register.jpg" width="600" style="max-width: 600px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">


									  </td>
									</tr>
								  </tbody>
								</table>
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
					<tr>
					  <td valign="top" id="templateBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 2px solid #EAEAEA;padding-top: 20px;padding-bottom: 20px;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">

										<p style="margin: 5px 0 20px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Hey there <b>'.$name.'</b>,</p>

										<p style="margin: 5px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">Welcome to Quake Club! Thank you for registering with us. Here&#39;s your first 100 Q-Perks to get you started.</p>

										<p style="margin: 20px 0 5px;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: center;">You may log in using the email address and password you registered with us.</p>

										<a href="http://quake.com.my/login" style="display: block; margin: 30px 0 20px 0; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img height="54" alt="Login now" src="http://quake.com.my/images/edm/btn-login.png" style="border: 0px;width: 113px;height: 54px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="113"></a>



									  </td>
									</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
					<tr>
					  <td valign="top" id="templateFooter" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #000000;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0px;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="padding-top: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #ffffff;font-family: Helvetica, Arial, sans-serif;font-size: 9px;line-height: 150%;text-align: center;">

										You have received this email because you have confirmed that you would like to receive email communication from Quake Club.<br> We will never share your personal information (such as your email address with any other third
										parties without your consent).<br>
										<br>
										<img height="71" alt="Astro Quake" src="http://quake.com.my/images/edm/footer-quake.png" style="border: 0px;width: 53px;height: 71px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="53">
									  </td>
									</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnFollowBlockOuter">
							<tr>
							  <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowBlockInner">
								<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								  <tbody>
									<tr>
									  <td align="center" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
										  <tbody>
											<tr>
											  <td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
												<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
												  <tbody>
													<tr>
													  <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														<!--[if mso]>
											<table align="center" border="0" cellspacing="0" cellpadding="0">
											<tr>
											<![endif]-->

														<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->


														<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														  <tbody>
															<tr>
															  <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																  <tbody>
																	<tr>
																	  <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		  <tbody>
																			<tr>

																			  <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																				<a href="https://www.facebook.com/QuakeMY" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-fb.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
																			  </td>


																			</tr>
																		  </tbody>
																		</table>
																	  </td>
																	</tr>
																  </tbody>
																</table>
															  </td>
															</tr>
														  </tbody>
														</table>

														<!--[if mso]>
												</td>
												<![endif]-->

														<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->


														<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														  <tbody>
															<tr>
															  <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																  <tbody>
																	<tr>
																	  <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		  <tbody>
																			<tr>

																			  <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																				<a href="https://api.whatsapp.com/send?phone=60126040968&text=Hi,%20send%20me%20the%20monthly%20issue%20of%20Winning%20Partnership%20Series%20by%20MARKETING" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-whatsapp.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
																			  </td>


																			</tr>
																		  </tbody>
																		</table>
																	  </td>
																	</tr>
																  </tbody>
																</table>
															  </td>
															</tr>
														  </tbody>
														</table>

														<!--[if mso]>
												</td>
												<![endif]-->

														<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->


														<table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
														  <tbody>
															<tr>
															  <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																  <tbody>
																	<tr>
																	  <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		<table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																		  <tbody>
																			<tr>

																			  <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
																				<a href="http://quake.com.my/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="http://quake.com.my/images/edm/footer-web.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="29" width="29" class=""></a>
																			  </td>


																			</tr>
																		  </tbody>
																		</table>
																	  </td>
																	</tr>
																  </tbody>
																</table>
															  </td>
															</tr>
														  </tbody>
														</table>

														<!--[if mso]>
												</td>
												<![endif]-->

														<!--[if mso]>
											</tr>
											</table>
											<![endif]-->
													  </td>
													</tr>
												  </tbody>
												</table>
											  </td>
											</tr>
										  </tbody>
										</table>
									  </td>
									</tr>
								  </tbody>
								</table>

							  </td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
						  <tbody class="mcnTextBlockOuter">
							<tr>
							  <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
								<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->

								<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
								  <tbody>
									<tr>

									  <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #ffffff;font-family: Helvetica, Arial, sans-serif;font-size: 9px;line-height: 150%;text-align: center;">

										<!-- <span style="font-size:12px"><a href="#" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #ffffff;font-weight: normal;text-decoration: none;">Unsubscribe </a>| <a href="http://quake.com.my/user-profile/profile" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #ffffff;font-weight: normal;text-decoration: none;">Update Profile</a></span><br> -->
										<br> © Astro Quake Club. All Right Reserved
									  </td>
									</tr>
								  </tbody>
								</table>
								<!--[if mso]>
						</td>
						<![endif]-->

								<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
							  </td>
							</tr>
						  </tbody>
						</table>
					  </td>
					</tr>
				  </table>
				  <!--[if (gte mso 9)|(IE)]>
								</td>
								</tr>
								</table>
								<![endif]-->
				  <!-- // END TEMPLATE -->
				</td>
			  </tr>
			</table>
		  </center>
		</body>

		</html>
		';

		$to = $email;
		$from = array($mailfrom, $fromname);

		# Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject($emailSubject);
		$mailer->setBody($emailBody);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();

		return $mailer->send();
	}
}
