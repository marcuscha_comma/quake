<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Segment_monthly_share
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Segment_monthly_share', JPATH_COMPONENT);
JLoader::register('Segment_monthly_shareController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Segment_monthly_share');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
