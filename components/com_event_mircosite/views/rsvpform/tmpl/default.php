<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Event_mircosite
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = Factory::getLanguage();
$lang->load('com_event_mircosite', JPATH_SITE);
$doc = Factory::getDocument();
$doc->addScript(Uri::base() . '/media/com_event_mircosite/js/form.js');

$user    = Factory::getUser();
$canEdit = Event_mircositeHelpersEvent_mircosite::canUserEdit($this->item, $user);


?>

<div class="rsvp-edit front-end-edit">
	<?php if (!$canEdit) : ?>
		<h3>
			<?php throw new Exception(Text::_('COM_EVENT_MIRCOSITE_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
		</h3>
	<?php else : ?>
		<?php if (!empty($this->item->id)): ?>
			<h1><?php echo Text::sprintf('COM_EVENT_MIRCOSITE_EDIT_ITEM_TITLE', $this->item->id); ?></h1>
		<?php else: ?>
			<h1><?php echo Text::_('COM_EVENT_MIRCOSITE_ADD_ITEM_TITLE'); ?></h1>
		<?php endif; ?>

		<form id="form-rsvp"
			  action="<?php echo Route::_('index.php?option=com_event_mircosite&task=rsvpform.save'); ?>"
			  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
			
	<input type="hidden" name="jform[id]" value="<?php echo isset($this->item->id) ? $this->item->id : ''; ?>" />

	<input type="hidden" name="jform[ordering]" value="<?php echo isset($this->item->ordering) ? $this->item->ordering : ''; ?>" />

	<input type="hidden" name="jform[state]" value="<?php echo isset($this->item->state) ? $this->item->state : ''; ?>" />

	<input type="hidden" name="jform[checked_out]" value="<?php echo isset($this->item->checked_out) ? $this->item->checked_out : ''; ?>" />

	<input type="hidden" name="jform[checked_out_time]" value="<?php echo isset($this->item->checked_out_time) ? $this->item->checked_out_time : ''; ?>" />

				<?php echo $this->form->getInput('created_by'); ?>
				<?php echo $this->form->getInput('modified_by'); ?>
	<?php echo $this->form->renderField('title'); ?>

	<?php echo $this->form->renderField('alias'); ?>

	<?php echo $this->form->renderField('logo'); ?>

	<?php echo $this->form->renderField('subtitle'); ?>

	<?php echo $this->form->renderField('subtitle_description'); ?>

	<?php echo $this->form->renderField('venue'); ?>

	<?php echo $this->form->renderField('date'); ?>

	<?php echo $this->form->renderField('time'); ?>

				<?php echo $this->form->getInput('created_at'); ?>
				<?php echo $this->form->getInput('updated_at'); ?>
	<?php echo $this->form->renderField('speakers_title'); ?>

	<?php echo $this->form->renderField('speakers_sort'); ?>

	<?php echo $this->form->renderField('speakers'); ?>

	<?php echo $this->form->renderField('event_highlight_title'); ?>

	<?php echo $this->form->renderField('event_highlight_description'); ?>

	<?php echo $this->form->renderField('agenda_title'); ?>

	<?php echo $this->form->renderField('agenda'); ?>

	<?php echo $this->form->renderField('background_image'); ?>

	<?php echo $this->form->renderField('background_image_size'); ?>

	<?php echo $this->form->renderField('background_color'); ?>

	<?php echo $this->form->renderField('theme_color'); ?>

	<?php echo $this->form->renderField('body_text_color'); ?>

	<?php echo $this->form->renderField('rsvp_bar_background_color'); ?>

	<?php echo $this->form->renderField('rsvp_bar_button_color'); ?>

			<div class="control-group">
				<div class="controls">

					<?php if ($this->canSave): ?>
						<button type="submit" class="validate btn btn-primary">
							<?php echo Text::_('JSUBMIT'); ?>
						</button>
					<?php endif; ?>
					<a class="btn"
					   href="<?php echo Route::_('index.php?option=com_event_mircosite&task=rsvpform.cancel'); ?>"
					   title="<?php echo Text::_('JCANCEL'); ?>">
						<?php echo Text::_('JCANCEL'); ?>
					</a>
				</div>
			</div>

			<input type="hidden" name="option" value="com_event_mircosite"/>
			<input type="hidden" name="task"
				   value="rsvpform.save"/>
			<?php echo HTMLHelper::_('form.token'); ?>
		</form>
	<?php endif; ?>
</div>
