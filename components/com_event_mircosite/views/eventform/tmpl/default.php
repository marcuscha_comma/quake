<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
$loginUrl = JRoute::_('index.php?option=com_event_mircosite&task=event.login');

?>

<div class="row justify-content-center" id="app">
	<div class="col-xl-6 col-lg-8 col-md-10" v-if="showLogin == 1">
		<div class="club-logo mx-auto mb-4">Quake Club</div>

		<div class="card rounded-card bg-grey mb-4 club-shadow">
			<div class="card-body">

				<h2 class="f-30 mb-3 mt-0 text-center">Login</h2>
				<form class="club-form" action="" method="post">
					<div class="field-placey form-group text-center">
						<div class="form-text form-error" v-if="!checklogin_username">Please fill in your email address
						</div>
						<i class="fas fa-envelope form-icon"></i>
						<input type="text" v-model="login_username" name="username" id="username"
							placeholder="example@email.com" value="" class="validate-username required" size="25"
							required="" aria-required="true" autofocus="" autocomplete="quake" readonly
							onfocus="this.removeAttribute('readonly');">
						<label id="username-lbl" for="username" class="required">Email</label>
					</div>

					<div class="field-placey form-group text-center">
						<div class="form-text form-error" v-if="!checklogin_password">Please fill in password
						</div>
						<div class="form-text form-error" v-if="!checkLoginAccess">Invalid email or password
						</div>
						<i class="fas fa-key form-icon"></i>
						<!--<i class="far fa-eye-slash toggle-password"></i> -->
						<input type="password" name="password" id="password" placeholder=" " value=""
							v-model="login_password" class="validate-password required form-control" size="25"
							maxlength="99" required="" aria-required="true" autocomplete="quake" readonly
							onfocus="this.removeAttribute('readonly');">
						<label id="password-lbl" for="password" class="required">Password</label>
					</div>

					<button @click="login()" type="button" class="btn btn-pink btn-block mt-2">
						RSVP Now
					</button>

				</form>
			</div>
		</div>
	</div>
	<div v-if="showLogin == 2">
		<div class="club-logo mx-auto mb-4">Quake Club</div>

		<div class="card rounded-card bg-grey mb-4 club-shadow">
			<div class="card-body">

				<h2 class="f-30 mb-3 mt-0 text-center">Welcome</h2>
				<div class="club-form">
					<div class="field-placey form-group text-center">
						<h1>asd</h1>
					</div>

					<a :href="external_link"><button type="button" class="btn btn-pink btn-block mt-2">
							RSVP Now
						</button></a>

				</div>
			</div>
		</div>
	</div>
	<div v-if="showLogin == 3">
		<div class="club-logo mx-auto mb-4">Quake Club</div>

		<div class="card rounded-card bg-grey mb-4 club-shadow">
			<div class="card-body">

				<h2 class="f-30 mb-3 mt-0 text-center">Sorry</h2>
				<div class="club-form">
					<div class="field-placey form-group text-center">
						<h1>Please contact admin.</h1>
					</div>

					<a :href="external_link"><button type="button" class="btn btn-pink btn-block mt-2">
							Back
						</button></a>

				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

jQuery( document ).ready(function() {
		jQuery(".btm-footer").hide();

	});
	
	jQuery(".toggle-password").click(function() {

		jQuery(this).toggleClass("fa-eye fa-eye-slash");
		var input = jQuery(this).next();
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});

	jQuery('#password').on('click focus touchstart', function() {
		jQuery(this).attr("readonly", false);
 	});


	var loginUrl = <?php echo json_encode($loginUrl); ?>;

	var app = new Vue({
		el: '#app',
		data: {
			userEmail: "",
			userName: "",
			external_link: "",
			login_username: "",
			login_password: "",
			checklogin_username: true,
			checklogin_password: true,
			checkLoginAccess: true,
			showLogin: 1
		},
		mounted: function () {
			_this = this;

			jQuery('input').change(function () {
				this.value = jQuery.trim(this.value);
				jQuery(this)[0].dispatchEvent(new Event('input'));
			});

			jQuery(".toggle-password").click(function () {

				jQuery(this).toggleClass("fa-eye fa-eye-slash");
				var input = jQuery(this).next();
				if (input.attr("type") == "password") {
					input.attr("type", "text");
				} else {
					input.attr("type", "password");
				}
			});
		},
		methods: {
			login: function () {

				if (this.login_username == "") {
					this.checklogin_username = false;
				} else {
					this.checklogin_username = true;
				}
				if (this.login_password == "") {
					this.checklogin_password = false;
				} else {
					this.checklogin_password = true;
				}
				if (this.checklogin_username && this.checklogin_password) {
					_this = this;

					jQuery.ajax({
						url: loginUrl,
						type: 'post',
						data: {
							'email': _this.login_username,
							'password': _this.login_password,
							'title': "MooMooDa Ox-picious CNY Get-Together"
						},
						success: function (result) {
							var tmp = result.split("-");

							if (tmp[0] == 2) {
								_this.showStep = 2;
								_this.userName = tmp[1];
								_this.external_link = tmp[2];
								_this.showLogin = 2;
							} else if (tmp[0] == 0) {
								_this.showStep = 3;
								_this.checkLoginAccess = false;
								_this.showLogin = 3;
							} else {
								_this.showStep = 2;
								_this.userName = tmp[1];
								_this.external_link = tmp[2];
								_this.showLogin = 2;
							}

						},
						error: function () {
							console.log('fail');
						}
					});
				}
			},
			register: function(){
				location.href = "http://43.228.245.193/~quakesta/login#quake-sign-up";
			},
		}
	})
</script>
