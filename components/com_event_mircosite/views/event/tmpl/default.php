<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_tour
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
use \Joomla\CMS\Uri\Uri;

$document = JFactory::getDocument();
$userId      = $this->userId;
$fillFormUrl = JRoute::_('index.php?option=com_event_mircosite&task=event.fillFrom');
$loginUrl = JRoute::_('index.php?option=com_event_mircosite&task=event.login');
$updateUserPlayGameUrl = JRoute::_('index.php?option=com_event_mircosite&task=event.updateUserPlayGameStatus');
$document->addStyleSheet(Uri::root() . 'components/com_event_mircosite/views/event/tmpl/css/style.css');

if ($userId) {
	$userToken = $this->userToken;
}

?>

<div id="app">
<div style="display: none;">
	<audio controls id="bgm" preload="auto" loop>
		<source src="<?php echo Uri::root(); ?>components/com_event_mircosite/views/event/tmpl/sound/bgm.mp3" type="audio/mpeg">
		<source src="<?php echo Uri::root(); ?>components/com_event_mircosite/views/event/tmpl/sound/android/bgm.ogg" type="audio/ogg">
	  	<source src="<?php echo Uri::root(); ?>components/com_event_mircosite/views/event/tmpl/sound/ios/bgm.m4a" type="audio/mp4">
	</audio>
</div>
	<div class="event-cover container text-center">
		<div class="astro-logo">
			<img src="./images/big2019/astro.svg" alt="">
		</div>

		<img class="big-logo" src="<?php echo $this->item->logo ?>" alt="">
	</div>
	<div class="d-inline-block mt-2 mr-2">
				<div class="btn btn-sm btn-submit mute-btn-wrapper">
					<a href="javascript:toggleMute();" class="mute-btn">mute</a>
				</div>
			</div>

	<!-- <div class="event-cover">
		<img class="event-logo" src="<?php echo $this->item->logo ?>" alt="">
	</div> -->

	<div class="rsvp-header container text-center">
		<h2 :style="[text_theme_color]" class="text_theme_color"><?php echo $this->item->subtitle; ?></h2>
		<div><?php echo $this->item->subtitle_description; ?></div>
	</div>

	<div class="container">
		<div class="text-center mb-5">
			<div class="row justify-content-center align-item-center">
				<!-- <div class="col-lg-3">
					<div class="event-info-box">
						<h3 class="text-theme f-20">VENUE:</h3>
						<b>Ballroom 2, <br />
							Sheraton, Petaling Jaya</b>
					</div>
				</div> -->
				<div class="col-lg-3">
					<div class="event-info-box">
						<h3 :style="[text_theme_color]" class="text_theme_color f-20">DATE:</h3>
						<?php echo $this->item->date; ?>

					</div>
				</div>
				<div class="col-lg-4 last">
					<div class="event-info-box">
						<h3 :style="[text_theme_color]" class="text_theme_color f-20">TIME:</h3>
						<?php echo $this->item->time; ?>
						
					</div>
				</div>
			</div>
		</div>

		<div class="position-relative" id="event-highlights">
			<h2 :style="[text_theme_color]" class="heading text-uppercase text-center mt-4 mb-2 text_theme_color"><?php echo $this->item->speakers_title; ?></h2>

			<div class="row mt-4 justify-content-center text-center">
				<div :class="'col-md-'+speakers_sort+' col-sm-6 mt-5 mt-sm-3'" v-for="speaker in speakers">
					<!-- col-md-3: 4 in a row / col-md-4: 3 in a row -->
					
					<img :src="'./../../'+speaker.profile_pic" :alt="speaker.name" class="img-fluid">
					
					<div class="">
						<h6 class="text-uppercase mt-4">{{speaker.title}}</h6>
						<h5 :style="[text_theme_color]" class="f-20 text_theme_color text-uppercase mt-3">{{speaker.name}}</h5>
						<p class="text-uppercase mb-0">{{speaker.designation}}</p>
					</div>
				</div> 
			</div>
		</div>

		<div class="highlight">
			<h2 class="text-center mt-5 mb-3">
				<span :style="[text_theme_color]" class="heading text-uppercase text_theme_color"><?php echo $this->item->event_highlight_title; ?></span>
			</h2>

			<div class="event-highlight">
				<?php echo $this->item->event_highlight_description; ?>
			</div>
		</div>

		<div class="agenda">
			<h2 class="text-center mt-5 mb-3">
				<span :style="[text_theme_color]" class="heading text-uppercase text_theme_color"><?php echo $this->item->agenda_title; ?></span>
			</h2>
			<div v-for="(value, name) in agenda">
				<div class="agenda-list">
					<div :style="[text_theme_color]" class="row agenda-header theme-border text_theme_color d-none d-lg-flex">
						<div class="col-lg-2 time-col">
							<h5>TIME</h5>
						</div>
						<div class="col-lg-7 agenda-col">
							<h5>PROGRAMME</h5>
						</div>
						<!-- <div class="col-lg-3 speaker-col">
							<h5>SPEAKER</h5>
						</div> -->
					</div>
					<div class="row theme-border" v-for="zz in value">
						<div :style="[text_theme_color]" class="col-lg-2 time-col text_theme_color">
							{{zz.time}}
						</div>
						<div class="col-lg-7 agenda-col">
							<div class="agenda-title">
								{{zz.programme}}
							</div>
						</div>
						<!-- <div class="col-lg-3 speaker-col">
							<div :style="[text_theme_color]" class="text_theme_color mobile-title">
								SPEAKER
							</div> 
							{{zz.speakers}}
						</div> -->
					</div>
				</div>
				
			</div>

<!-- 
			<div class="agenda-list" v-for="(value, name) in agenda" >
			{{name}}
				<div v-for="zz in value">
				{{zz.time}}
				{{zz.programme}}
				{{zz.speakers}}
				{{zz.remark}}
				</div>
			</div> -->
		</div>

		<!-- <div class="footer">
			<b>© 2019 Astro Quake Rights Reserved.</b>
		</div> -->
	</div>

	<div v-if="!rsvpMember" class="float-register" data-toggle="modal" data-target="#downloadCenterModal">
		<div class="container">
			I would like to attend {{title}}.
			<button :disabled="loginProgressing" type="button" class="btn btn-white">RSVP Now</button>
		</div>
	</div>

	<div v-if="rsvpMember" class="float-register">
		<div class="container">
			Registered Successful.
			<a :href="external_link" target="_blank"><button type="button" class="btn btn-white">Visit Webinar</button></a>
			<a href="index.php?option=com_users&task=user.logout&<?php echo $userToken ?>=1&return=rsvp"><button type="button" class="btn btn-white">Logout</button> </a>
		</div>
	</div>


	<!-- Modal Start -->
	<div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" hidden="true"></div>

				<div class="modal-body">
				<!-- 1.0 Register -->
				<div v-if="showStep == 1" class="">
						<div>
							<button type="button" class="close font-brand mb-3" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">Close</span>
							</button>

							<div style="clear: both;"></div>
						</div>
						<div class="border-bottom border-grey">
							<div class="row align-items-center mb-0 mb-md-3">
								<div class="col-md-7 order-md-last text-right">
									<button @click="showStep = 3" type="button" id="sticky" class="btn btn-pink">Express RSVP for Quake Club Members <i
											class="fas fa-angle-right"></i></button>
								</div>
								<div class="col-md-5">
									<h2 class="h2 mb-0">Almost there!</h2>
								</div>
							</div>

							<p>Complete the registration form below:</p>

						</div>
						<div class="py-3">
							<em class="text-blue f-14 mb-3 d-block">* All fields are mandatory.</em>

							<form class="club-form" action="" method="post">
								<div class="field-placey form-group">
									<div class="form-text form-error" v-if="!checkComEmail">Please fill in your company’s email address
									</div>
									<div class="form-text form-error" v-if="!checkEmailExist">This email address is already registered.
									</div>
									<input autocomplete="quake" id="com_email" v-model="userEmail"
										pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control"
										:class="{'check-validate': !checkComEmail}" name="jform[email1]" type="email" id="email"
										placeholder=" " required>
									<label for="com_email">Company email</label>
								</div>

								<div class="field-placey form-group">
									<div class="form-text form-error" v-if="!checkUserName">Please fill in your full name</div>
									<input autocomplete="quake" id="user_name" v-model="userName" class="form-control"
										 :class="{'check-validate': !checkUserName}" name="jform[name]" type="text" placeholder=" " required>
									<label for="name">Full name</label>
								</div>

								<div class="field-placey form-group">
									<div class="form-text form-error" v-if="!checkCompany">Please fill in your company’s name</div>
									<input v-model="vcompname" autocomplete="quake" class="form-control"
										:class="{'check-validate': !checkCompany}" name="jform[company]" id="company" type="text"
										placeholder=" " required>
									<label for="company">Company’s name</label>
								</div>

								<div class="field-placey form-group">
									<div class="form-text form-error" v-if="!checkDesignation">Please fill in your job designation</div>
									<input autocomplete="quake" id="designation" class="form-control" v-model="vdesignation"
										:class="{'check-validate': !checkDesignation}" name="jform[designation]" type="text"
										placeholder=" " required>
									<label for="designation">Designation</label>
								</div>

								<div class="mb-4">
									<div class="row">
										<div class="col-md-4 col-5">
											<select v-model="userCountryCode" name="jform[country_code]" id="country_code"
												class="selectpicker white-dropdown" :class="{'check-validate': !checkCountryCode}"
												data-width="100%" required>
												<option v-for="item in country_code" :value="item">{{item}}</option>
											</select>
										</div>
										<div class="col-md-8 col-7">
											<div class="field-placey">
												<div class="form-text form-error" v-if="!checkMobile">Please fill in your mobile number</div>
												<div class="form-text form-error" v-if="!checkMobileValid">Please fill in a valid mobile number
												</div>
												<input autocomplete="quake" v-model="userContactNumber" class="form-control"
													:class="{'check-validate': !checkMobile}" id="mobile" name="jform[contact_number]" type="tel"
													placeholder=" " required>
												<label for="mobile">Mobile number</label>
											</div>
										</div>
									</div>
								</div>

								<div class="custom-control text-left custom-checkbox mt-4 remember-me">

									<input type="checkbox" class="custom-control-input" id="join-game" v-model="checkJoinGameStatus">
									<label class="custom-control-label text-white" for="join-game">I will agree to join game</label>
								</div>

								<div class="custom-control text-left custom-checkbox mt-4 remember-me">

									<input type="checkbox" class="custom-control-input" id="agree-terms" v-model="checkPDPAStatus">
									<label class="custom-control-label text-white" for="agree-terms">I will agree to the <a
											href="#" class="text-white" data-toggle="modal" data-target="#pdpa"><u>Personal Data Protection Act</u></a></label>
											<div class="form-text form-error" v-if="!checkPDPAStatusShow">Checkbox Require
												</div>
								</div>

								<div class="field-placey form-group" v-if="checkJoinGameStatus">
									<div class="form-text form-error" v-if="!this.checkDelivery">Please fill in your delivery address</div>
									<input autocomplete="quake" id="delivery" class="form-control" v-model="delivery"
										:class="{'check-validate': !checkDesignation}" name="jform[delivery]" type="text"
										placeholder=" " required>
									<label for="delivery">Delivery</label>
								</div>

								<button type="button" @click="fillFrom()" class="btn btn-pink btn-block mt-4">
									RSVP Now
								</button>

							</form>
						</div>

					</div>
					<!-- END Register -->
					<!-- 1.1 Login Quake -->
					<div v-show="showStep == 3" class="">
						<div>
						<button @click="showStep = 1" type="button" class="close float-left back font-brand mb-3">
								<span><i class="fas fa-angle-left"></i> Back</span>
							</button>
							<button type="button" class="close font-brand mb-3" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">Close</span>
							</button>

							<div style="clear: both;"></div>
						</div>
						<div class="text-center">
							<img src="./images/big2019/rsvp-quake.svg" class="mx-auto mt-5 mb-4" alt="RSVP with Quake Club">
						</div>
						<div class="py-3">
							<form class="club-form" action="" method="post">
								<div class="field-placey form-group text-center">
									<div class="form-text form-error" v-if="!checklogin_username">Please fill in your email address
									</div>
									<i class="fas fa-envelope form-icon"></i>
									<input type="text" v-model="login_username" name="username" id="username" placeholder="example@email.com" value=""
										class="validate-username required" size="25" required="" aria-required="true" autofocus=""
										autocomplete="quake" readonly onfocus="this.removeAttribute('readonly');">
									<label id="username-lbl" for="username" class="required">Email</label>
								</div>

								<div class="field-placey form-group text-center">
									<div class="form-text form-error" v-if="!checklogin_password">Please fill in password
									</div>
									<div class="form-text form-error" v-if="!checkLoginAccess">Invalid email or password
									</div>
									 <i class="fas fa-key form-icon"></i>
									<!--<i class="far fa-eye-slash toggle-password"></i> -->
									<input type="password" name="password" id="password" placeholder=" " value="" v-model="login_password"
										class="validate-password required form-control" size="25" maxlength="99" required=""
										aria-required="true" autocomplete="quake" readonly onfocus="this.removeAttribute('readonly');">
									<label id="password-lbl" for="password" class="required">Password</label>
								</div>

								<div class="custom-control text-left custom-checkbox mt-4 remember-me">
									<input type="checkbox" class="custom-control-input" id="join-game-login" v-model="checkJoinGameStatus">
									<label class="custom-control-label text-white" for="join-game-login">I will agree to join game</label>
								</div>

								<div class="field-placey form-group" v-if="checkJoinGameStatus">
									<div class="form-text form-error" v-if="!this.checkDelivery">Please fill in your delivery address</div>
									<input autocomplete="quake" id="delivery-login" class="form-control" v-model="delivery"
										:class="{'check-validate': !checkDesignation}" name="jform[delivery]" type="text"
										placeholder=" " required>
									<label for="delivery-login">Delivery</label>
								</div>

								<button @click="login()" type="button" class="btn btn-pink btn-block mt-2">
									RSVP Now
								</button>

								<div class="text-center mt-4">
									<a class="bold link-underline border-white text-white" target="_blank"
										href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
										Forgot your password?
									</a>
								</div>


								<button @click="register()" type="button" class="btn btn-link text-white btn-block mt-3">
									Not a member yet? Sign up now
								</button>

							</form>
						</div>

					</div>
					<!-- END Login Quake -->

					<!-- 2. Step Success -->
					<div v-if="showStep == 2">
						<div class="text-center">
							<img src="./images/big2019/success.svg" class="icon-done" alt="">

							<h2 class="h2 text-white">RSVP Successful!</h2>
							<span v-if="!checkLAldyRegisted">
								<h2 class="h2 text-highlight">{{userName}}</h2> <!-- Name -->
								<!-- <a :href="external_link" target="_blank"><button type="button" class="btn btn-pink btn-block">Visit Webinar</button></a> -->
								<!-- <p class="text-white mb-4 font-weight-bold">Thank you and see you on 31st July in Sheraton PJ!</p> -->
							</span>
							<span v-if="checkLAldyRegisted">
								<h2>Hi <span class="h2 text-highlight">{{userName}}</span>,</h2> <!-- Name -->
								<!-- <p class="text-white mb-4 font-weight-bold">you have already registered to this event. See you on 31st July in Sheraton PJ!</p> -->
							</span>

							<button type="button" data-dismiss="modal" class="btn btn-pink btn-block">Close</button>
						</div>

						<!-- For Normal RSVP: Ask to join Quake -->
						<div class="pink-block" v-if="checkLoginStatus">
							<h3 class="f-20">Are you interested to join Quake Club?</h3>
							<p>A sharing community for marketing professionals</p>

							<a href="./notice" target="_blank" class="btn btn-white btn-block">Sign Up Now</a>
						</div>
						<!---------------------------------------->

					</div>
					<!-- END Step Success -->


				</div>
			</div>
		</div>
	</div>
	<!-- Modal End -->

	<!-- Modal Start -->
	<div class="modal fade custom-modal" ref="vuemodal" id="pdpa" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close font-brand mb-3" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">Close</span>
					</button>
					<h2 class="mb-4">PDPA Policy</h2>
        		<div class="scroll-content">
							<p>The Personal Data Protection Act 2010 was introduced by the Government to regulate the processing of personal data in commercial transactions. The Act, which applies to all companies and firms that are established in Malaysia, requires us to inform you of your rights in respect of your personal data that is being processed or that is to be collected and further processed by us and the purposes for the data processing. The Act also requires us to obtain your consent to the processing of your personal data.</p>

	        		<p>Consequently, please be informed that the personal data and other information (collectively, “Personal Data”) provided in your application to register for the use of the Astro Quake Website and, if relevant, for subscription to Astro Quake Services may be used and processed by Astro Malaysia Holdings Sdn Bhd (“AMH”) for the following purposes:-</p>

	        		<ul>
	        			<li>assessing your application to register for the use of the Astro Quake Website</li>
	        			<li>assessing your application for subscription</li>
	        			<li>to communicate with you;</li>
	        			<li>to provide services to you;</li>
	        			<li>to process your payment transactions;</li>
	        			<li>respond to your inquiries;</li>
	        			<li>administer your participation in contests;</li>
	        			<li>conduct internal activities;</li>
	        			<li>market surveys and trend analysis;</li>
	        			<li>to provide you with information on products and services of AMH and our related corporations;</li>
	        			<li>to provide you with information on products and services of our business partners;</li>
	        			<li>other legitimate business activities of AMH;</li>
	        			<li>such other purposes as set out in the Astro Quake Website Terms of Use; and/or</li>
	        			<li>if relevant, such other purposes as set out in the General Terms and Conditions and, if applicable, Campaign Terms and Conditions.</li>
	        		</ul>

	        		<p>(collectively “Purposes”).</p>

	        		<p>Further, please be informed that if required for any of the foregoing Purposes, your Personal Data may be transferred to locations outside Malaysia or disclosed to our related corporations, licensees, business partners and/or service providers, who may be located within or outside Malaysia. Save for the foregoing, your Personal Data will not be knowingly transferred to any place outside Malaysia or be knowingly disclosed to any third party.</p>

	        		<p>In order to process your Personal Data, your consent is required.  If you do not consent to the processing of your Personal Data other than in relation to the advertising or marketing of any product or service of AMH or our business partners, we cannot process your Personal Data for any of the above Purposes and we will not be able to approve your application for registration for use of the Astro Quake website.</p>

	        		<p>In relation to direct marketing, you may request AMH by written notice (in accordance with the following paragraph) not to process your Personal Data for any of the following Purposes: (i) advertising or marketing via phone any product or service of AMH or our business partners, (ii) sending to you via post any advertising or marketing material relating to any product or service of AMH or our business partners; (iii) sending to you via email or SMS any advertising or marketing material relating to any product or service of AMH or our business partners, or (iv) communicating to you by whatever means any advertising or marketing material of AMH or our business partners.</p>

	        		<p>You may at any time hereafter make inquiries, complaints and request for access to, or correction of, your Personal Data or limit the processing of your Personal Data by submitting such request to the Personal Data Protection Officer of AMH via registered post or email as set out below.</p>

	        		<p><strong>Postal address:</strong></p>

	        		<p>Personal Data Protection Officer,
			        			<br>
								Astro Malaysia Holdings Sdn Bhd,
								<br>
								Peti Surat 10335,
								<br>
								50710 Kuala Lumpur
							</p>

							<p><strong>Email address:</strong>&nbsp;<a href="mailto:pdpo@astro.com.my" class="text-white">pdpo@astro.com.my</a></p>
        		</div>

					<button type="button" data-dismiss="modal" class="btn btn-block btn-pink mt-3">Back</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal End -->
</div>

<script>
var fillFormUrl = <?php echo json_encode($fillFormUrl); ?>;
var loginUrl = <?php echo json_encode($loginUrl); ?>;
var updateUserPlayGameUrl = <?php echo json_encode($updateUserPlayGameUrl); ?>;

var title = <?php echo json_encode($this->item->title); ?>;
var userId = <?php echo json_encode($userId); ?>;
var rsvpMember = <?php echo json_encode($this->rsvpMember); ?>;
var speakers = <?php echo json_encode($this->item->speakers); ?>;
var agenda = <?php echo json_encode($this->item->agenda); ?>;
var body_text_color = <?php echo json_encode($this->item->body_text_color); ?>;
var rsvp_bar_background_color = <?php echo json_encode($this->item->rsvp_bar_background_color); ?>;
var rsvp_bar_button_color = <?php echo json_encode($this->item->rsvp_bar_button_color); ?>;
var theme_color = <?php echo json_encode($this->item->theme_color); ?>;
var background_color = <?php echo json_encode($this->item->background_color); ?>;
var background_image = <?php echo json_encode($this->item->background_image); ?>;
var background_image_size = <?php echo json_encode($this->item->background_image_size); ?>;
var speakers_sort = <?php echo json_encode($this->item->speakers_sort); ?>;
var external_link = <?php echo json_encode($this->external_link); ?>;
var bgmPlaying = false;


	jQuery( document ).ready(function() {
		jQuery('.content > .container').addClass('container-fluid');
		jQuery('.content > .container').removeClass('container');
		jQuery(".btm-footer").hide();
		document.title = title;

		jQuery(".toggle-agenda").click(function() {
			jQuery( this ).parent('.agenda-desc').toggleClass( "expand" );
		});

		jQuery('#pdpa').on('shown.bs.modal', function (e) {
			jQuery('#downloadCenterModal').addClass('dimmed');
		});

		jQuery('#pdpa').on('hide.bs.modal', function (e) {
			jQuery('#downloadCenterModal').removeClass('dimmed');
		});

		jQuery('body').css({
			'color': body_text_color,
			'background-color': background_color,
			'background-image': 'url(../../' + background_image + ')',
			'background-size' : background_image_size // In CMS use name "Strech" for 'cover', "Fit" for 'contain'
		});

		jQuery('.theme-border, .agenda-col, .speaker-col').css({
			'border-color': body_text_color,
		});

		var allowCache = true;
    var isMute = false;
    if(allowCache){
    	if(localStorage.hasOwnProperty('cny-mute')){
	        isMute = JSON.parse(localStorage.getItem('cny-mute'));
	    } else {
	        // is not mute by default
	        isMute = false;
	        localStorage.setItem('cny-mute', isMute);
	    };
    };
    // isMute = false;
    // console.log('is mute: ' + isMute);

    // mute button status
    if(isMute){
        jQuery('.mute-btn').addClass('mute');
    } else {
        jQuery('.mute-btn').removeClass('mute');
    };

	var thisBGM = jQuery('#bgm')[0];
    // bgm too loud, reduce the volume
    thisBGM.volume = 0.1;
    // looping
    thisBGM.loop = true;

	window.playBGM = function(play){
    	// console.log('attemp playing bgm');

		if(!isMute && play){
			// play the music
			if(!bgmPlaying){
				bgmPlaying = true;
				thisBGM.currentTime = 0;
				thisBGM.play();
			};
		} else {
			// stop the music
			if(bgmPlaying){
            	bgmPlaying = false;
            	thisBGM.pause();
            };
		};
	};
	// force all buttons to play bgm
	jQuery('.btn.btn-submit').click(function(){
		playBGM(true);
	});
	jQuery('.at-share-btn').click(function(){
		playBGM(true);
	});
	jQuery('#overlay-div').click(function(){
		playBGM(true);
	});

    window.toggleMute = function(){
        // console.log('toggle isMute: ' + isMute);
        isMute = !isMute;
        // console.log('toggled: ' + isMute);

        // button state
        if(isMute){
            jQuery('.mute-btn').addClass('mute');

            playBGM(false);
        } else {
            jQuery('.mute-btn').removeClass('mute');

            // if(flippedOnce){
            	playBGM(true);
            // };
        };

        // save settings
        if(allowCache) localStorage.setItem('cny-mute', isMute);
        // console.log('mute settings saved');
        // console.log(localStorage.getItem('cny-mute'));
    };

	});


	var app = new Vue({
		el: '#app',
		data: {
			showStep: 1,
			checkComEmail: true,
			checkEmailExist: true,
			checkUserName: true,
			checkCompany: true,
			checkDesignation: true,
			checkDelivery: true,
			checkMobile: true,
			checkMobileValid: true,
			checkCountryCode: true,
			checklogin_username: true,
			checklogin_password: true,
			checkPDPAStatusShow: true,
			checkLoginAccess: true,
			checkLoginStatus: false,
			checkPDPAStatus: false,
			checkJoinGameStatus: false,
			userEmail: "",
			userName: "",
			vcompname: "",
			userCountryCode: "+60",
			userContactNumber: "",
			vdesignation: "",
			delivery: "",
			login_username: "",
			login_password: "",
			title: title,
			country_code: ["+60", "+65", "+86", "+852", "+91"],
			userId: 0,
			loginProgressing: false,
			rsvpMember: false,
			playGameStatus: false,
			speakers: [],
			agenda: [],
			text_theme_color: {
				color: "#fff"
			},
			speakers_sort: 3,
			external_link: ""
		},
		mounted: function () {
			_this = this;
			_this.userId = userId;
			_this.rsvpMember = rsvpMember;
			_this.speakers = JSON.parse(speakers);
			_this.agenda = JSON.parse(agenda);
			_this.text_theme_color.color = theme_color;
			_this.speakers_sort = speakers_sort;
			_this.external_link = external_link;

			jQuery('#downloadCenterModal').on('hidden.bs.modal', function () {
				location.reload();
				_this.loginProgressing = true;
			})

			jQuery('input').change(function () {
				this.value = jQuery.trim(this.value);
				jQuery(this)[0].dispatchEvent(new Event('input'));
			});

			jQuery(".toggle-password").click(function () {

				jQuery(this).toggleClass("fa-eye fa-eye-slash");
				var input = jQuery(this).next();
				if (input.attr("type") == "password") {
					input.attr("type", "text");
				} else {
					input.attr("type", "password");
				}
			});
			// this.checkPlayGameStatus();
		},
		updated: function () {
			jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
		},
		methods: {
			fillFrom: function () {
				if (this.userEmail.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/) == null) {
					this.checkComEmail = false;
				} else {
					this.checkComEmail = true;
				}
				if (this.userName == "") {
					this.checkUserName = false;
				} else {
					this.checkUserName = true;
				}
				if (this.vcompname == "") {
					this.checkCompany = false;
				} else {
					this.checkCompany = true;
				}
				if (this.vdesignation == "") {
					this.checkDesignation = false;
				} else {
					this.checkDesignation = true;
				}
				
				if (!this.userContactNumber.match(/.{9,16}/)) {
					this.checkMobileValid = false;
				} else {
					this.checkMobileValid = true;
				}
				if (!this.checkPDPAStatus) {
					this.checkPDPAStatusShow = false;
				} else {
					this.checkPDPAStatusShow = true;
				}
				if (this.checkJoinGameStatus) {
					if (this.delivery == "") {
						this.checkDelivery = false;
					}else{
						this.checkDelivery = true;
					}
				}else{
					this.checkDelivery = true;
				}

				if (this.checkMobileValid && this.checkMobile && this.checkDesignation && this.checkCompany && this.checkUserName && this.checkComEmail && this.checkPDPAStatus && this.checkDelivery) {
					_this = this;
					jQuery.ajax({
						url: fillFormUrl,
						type: 'post',
						data: {
							'email': _this.userEmail,
							'name': _this.userName,
							'comp_name': _this.vcompname,
							'mobileNo': _this.userCountryCode + '-' + _this.userContactNumber,
							'designation': _this.vdesignation,
							'title': _this.title,
							'delivery': _this.delivery,
							'checkJoinGameStatus': _this.checkJoinGameStatus
						},
						success: function (result) {
							var tmp = result.split("-");

							if (tmp[0] == 2) {
								_this.showStep = 2;
								_this.checkLoginStatus = true;
								_this.userName = tmp[1];
								_this.checkLAldyRegisted = true;
								_this.external_link = tmp[2];

							} else if (tmp[0] == 0) {
								_this.showStep = 1;

							} else {
								_this.showStep = 2;
								_this.checkLoginStatus = true;
								_this.userName = tmp[1];
								_this.checkLAldyRegisted = false;
								_this.external_link = tmp[2];
							}

						},
						error: function () {
							console.log('fail');
						}
					});
				}
			},
			register: function () {
				location.href = "https://quake.com.my/login#quake-sign-up";
			},
			login: function () {

				if (this.login_username == "") {
					this.checklogin_username = false;
				} else {
					this.checklogin_username = true;
				}
				if (this.login_password == "") {
					this.checklogin_password = false;
				} else {
					this.checklogin_password = true;
				}
				if (this.checkJoinGameStatus) {
					if (this.delivery == "") {
						this.checkDelivery = false;
					}else{
						this.checkDelivery = true;
					}
				}else{
					this.checkDelivery = true;
				}
				if (this.checklogin_username && this.checklogin_password && this.checkDelivery) {
					_this = this;

					jQuery.ajax({
						url: loginUrl,
						type: 'post',
						data: {
							'email': _this.login_username,
							'password': _this.login_password,
							'title': _this.title,
							'delivery': _this.delivery,
							'checkJoinGameStatus': _this.checkJoinGameStatus
						},
						success: function (result) {
							var tmp = result.split("-");

							if (tmp[0] == 2) {
								_this.showStep = 2;
								_this.userName = tmp[1];
								_this.checkLAldyRegisted = true;
								_this.external_link = tmp[2];

							} else if (tmp[0] == 0) {
								_this.showStep = 3;
								_this.checkLoginAccess = false;
							} else {
								_this.showStep = 2;
								_this.userName = tmp[1];
								_this.checkLAldyRegisted = false;
								_this.external_link = tmp[2];

							}

						},
						error: function () {
							console.log('fail');
						}
					});
				}
			},
			backToPrevious: function () {
				this.showStep--;
			},
			goToNext: function () {
				var address = document.getElementById('address');
				var user_name = document.getElementById('user_name');
				var business_card = document.getElementById('imgInp');

				switch (this.showStep) {
					case 1:
						this.showStep++;
						break;
					case 2:
						this.checkUserName = user_name.checkValidity();
						this.checkAddress = address.checkValidity();
						if (this.businessCard == "") {
							this.checkBusinessCard = false;
						} else {
							this.checkBusinessCard = true;
						}

						if (address.checkValidity() && user_name.checkValidity() && this.checkBusinessCard && this.checkImgSize) {
							this.showStep++;
						}
						break;

					default:
						break;
				}
			}
		}
	})
</script>
