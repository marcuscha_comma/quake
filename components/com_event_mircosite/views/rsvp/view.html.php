<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Event_mircosite
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;
use Joomla\Utilities\ArrayHelper;

/**
 * View to edit
 *
 * @since  1.6
 */
class Event_mircositeViewRsvp extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = Factory::getApplication();
		$user = Factory::getUser();

		$this->state  = $this->get('State');
		$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_event_mircosite');
		$this->user        = JFactory::getUser();
		$this->userId      = $user->get('id');
		$this->userToken = JSession::getFormToken();
        $this->rsvpMember = "";
		$this->replacements = array();
        $this->searches    = $this->getSearches();
        $agenda = json_decode($this->item->agenda);
        $tmp = array();
        
        foreach ($agenda as $key => $value) {
            $tmp[$value->date][] = array(
                "time"=>$value->time,
                "programme"=>$value->programme,
                "speakers"=>$value->speakers,
                "remark"=>$value->remark
            );
        }
        $this->item->agenda = json_encode($tmp);


        // Hide any youtube links already embedded with <iframe>
        if (preg_match_all('#<iframe.*src=["\']\S*youtube\.com.*</iframe>#', $this->item->event_highlight_description, $iframes)) {
            foreach ($iframes[0] as $source) {
                $replaceKey = sprintf('{{%s}}', md5($source));
                if (!isset($this->replacements[$replaceKey])) {
                    $this->replacements[$replaceKey] = $source;
                    $this->item->event_highlight_description = str_replace($source, $replaceKey, $this->item->event_highlight_description);
                }
            }
		}

		 // Do links first to hide them from plain url searches
		 foreach ($this->searches as $regex) {
            $linkRegex = '#(?:<a.*href=[\'"])' . addcslashes($regex, '#') . '(?:[\'"].*>.*</a>)#';
            $this->createPlaceholders($linkRegex, $this->item->event_highlight_description, true);
        }

        // Now we can safely look for non-link instances
        foreach ($this->searches as $regex) {
            $plainRegex = '#' . addcslashes($regex, '#') . '#';
            $this->createPlaceholders($plainRegex, $this->item->event_highlight_description);
        }

        if ($this->replacements) {
            $this->item->event_highlight_description = str_replace(array_keys($this->replacements), $this->replacements, $this->item->event_highlight_description);
		}

		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		if ($this->userId) {
			$db->setQuery('SELECT external_link FROM #__cus_rsvp_tour_data where user_id = "'.$this->userId.'" and source ="'.$this->item->title .'"');
			$this->external_link             = $db->loadResult();
			if ($this->external_link) {
				$rsvpMember = true;
			}else{
				$rsvpMember = false;
			}
			$this->rsvpMember = $rsvpMember;
		}

		if (!empty($this->item))
		{
			$this->form = $this->get('Form');
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_event_mircosite');

			if ($authorised !== true)
			{
				throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
			}
		}

        $this->_prepareDocument();
        
        if ($this->item->title == "") {
            header('Location: /');
            exit;
        }
        

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = Factory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', Text::_('COM_EVENT_MIRCOSITE_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = Text::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = Text::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	protected function createPlaceholders($regex, &$text, $links = false)
    {
        if (preg_match_all($regex, $text, $matches)) {
            foreach ($matches[0] as $k => $source) {
                $sourceUrl = $matches[1][$k];
                $videoCode = $matches[2][$k];
                $query     = html_entity_decode($matches[3][$k]);
                $hash      = $matches[4][$k];

                $replaceKey = sprintf('{{%s}}', md5($source));

                if (!isset($this->replacements[$replaceKey])) {
                    if ($this->ignoreLinks && $links) {
                        // Hide the link temporarily to avoid crashes
                        $this->replacements[$replaceKey] = $source;

                    } else {
                        // Convert to embedded iframe
                        if ($query && $query[0] == '?') {
                            $query = substr($query, 1);
                        }
                        parse_str($query, $query);

                        $url       = $this->getUrl($sourceUrl, $videoCode, $query, $hash);
                        $embedCode = $this->youtubeCodeEmbed($videoCode, $url);

                        $this->replacements[$replaceKey] = $embedCode;
                    }

                    $text = str_replace($source, $replaceKey, $text);
                }
            }
        }
	}
	
	/**
     * Load the regular expressions to search for in the text
     *
     * @return array
     */
    protected function getSearches()
    {
        $searches = array(
            '(https?://(?:www\.)?youtube.com/embed/([a-zA-Z0-9-_&;=]+))(\?[a-zA-Z0-9-_&;=]*)?(#[a-zA-Z0-9-_&;=]*)?',
            '(https?://(?:www\.)?youtube.com/watch\?v=([a-zA-Z0-9-_;]+))(&[a-zA-Z0-9-_&;=]*)?(#[a-zA-Z0-9-_&;=]*)?'
        );

        return $searches;
	}
	
	/**
     * @param string $sourceUrl
     * @param string $videoCode
     * @param array  $query
     * @param string $hash
     *
     * @return string
     */
    protected function getUrl($sourceUrl, $videoCode, array $query = array(), $hash = null)
    {
        $query = array_merge(array('wmode' => 'transparent'), $query);

        $url = sprintf(
            'https://www.youtube.com/embed/%s?%s%s',
            $videoCode,
            http_build_query($query),
            $hash
        );

        return $url;
	}
	
	/**
     * @param string $videoCode
     * @param string $iframeSrc
     *
     * @return string
     */
    protected function youtubeCodeEmbed($videoCode, $iframeSrc)
    {
        $output = '';
        $params = $this->params;

        $width      = $params->get('width', 425);
        $height     = $params->get('height', 344);
        $responsive = $params->get('responsive', 1);

        if ($responsive) {
            JHtml::_('stylesheet', 'plugins/content/osyoutube/style.css');
            $output .= '<div class="video-responsive">';
        }

        // The "Load after page load" feature is only available in Pro
        // but iframe is loaded in Free, so this is needed here
        $afterLoad = $this->params->get('load_after_page_load', 0);

        if ($afterLoad) {
            // This is used as a placeholder for the "Load after page load" feature in Pro
            $iframeDataSrc = $iframeSrc;
            $iframeSrc     = '';
        }

        $id = 'youtube_' . $videoCode;
        if (!empty($this->videoIds[$videoCode])) {
            $id .= '_' . ($this->videoIds[$videoCode]++);
        } else {
            $this->videoIds[$videoCode] = 1;
        }

        $attribs = array(
            'id'          => $id,
            'width'       => $width,
            'height'      => $height,
            'frameborder' => '0',
            'src'         => $iframeSrc
        );

        if (!empty($iframeDataSrc)) {
            $attribs['data-src'] = $iframeDataSrc;
        }

        $output .= '<iframe ' . ArrayHelper::toString($attribs) . ' allowfullscreen></iframe>';

        if ($responsive) {
            $output .= '</div>';
        }

        return $output;
    }
}
