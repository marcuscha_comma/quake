(function(exports) {
    var MS_IN_MINUTES = 60 * 1000;
  
    var formatTime = function(date) {
      return date.toISOString().replace(/-|:|\.\d+/g, '');
    };
  
    var calculateEndTime = function(event) {
      return event.end ?
        formatTime(event.end) :
        formatTime(new Date(event.start.getTime() + (event.duration * MS_IN_MINUTES)));
    };
  
    var calendarGenerators = {
      google: function(event) {
        var startTime = formatTime(event.start);
        var endTime = calculateEndTime(event);
        var href = encodeURI([
          'https://www.google.com/calendar/render',
          '?action=TEMPLATE',
          '&text=' + (event.title || ''),
          '&dates=' + (startTime || ''),
          '/' + (endTime || ''),
          '&details=' + (event.description || ''),
          '&location=' + (event.address || ''),
          '&sprop=&sprop=name:'
        ].join(''));
        return '<a class="" target="_blank" href="' +
          href + '">Google Calendar</a>' + "\xa0\xa0\xa0\xa0" ;
      },
      ics: function(event, eClass) {
        var startTime = formatTime(event.start);
        var endTime = calculateEndTime(event);
  
        var href = encodeURI(
          'data:text/calendar;charset=utf8,' + [
            'BEGIN:VCALENDAR',
            'VERSION:2.0',
            'BEGIN:VEVENT',
            'URL:' + document.URL,
            'DTSTART:' + (startTime || ''),
            'DTEND:' + (endTime || ''),
            'SUMMARY:' + (event.title || ''),
            'DESCRIPTION:' + (event.description || ''),
            'LOCATION:' + (event.address || ''),
            'END:VEVENT',
            'END:VCALENDAR'].join('\n'));
  
        return '<a class="' + eClass + '" target="_blank" href="' +
          href + '">' + ' Apple Calendar</a>';
      },
  
      ical: function(event) {
        return this.ics(event, 'iCal');
      },
    };
  
    var generateCalendars = function(event) {
      return {
        google: calendarGenerators.google(event),
        ical: calendarGenerators.ical(event),
      };
    };
  
    // Make sure we have the necessary event data, such as start time and event duration
    var validParams = function(params) {
      return params.data !== undefined && params.data.start !== undefined &&
        (params.data.end !== undefined || params.data.duration !== undefined);
    };
  
    var generateMarkup = function(calendars, clazz, calendarId) {
      var result = document.createElement('button');
      Object.keys(calendars).forEach(function(services) {
        result.innerHTML += calendars[services];
      });
      
      result.className = 'add2cal btn btn-pink';
      if (clazz !== undefined) {
        result.className += (' ' + clazz);
      }
      result.id = calendarId;
      return result;
    };
  
    var getClass = function(params) {
      if (params.options && params.options.class) {
        return params.options.class;
      }
    };
  
    var getOrGenerateCalendarId = function(params) {
      return params.options && params.options.id ?
        params.options.id :
        Math.floor(Math.random() * 1000000); // Generate a 6-digit random ID
    };
  
    exports.createCalendar = function(params) {
      if (!validParams(params)) {
        console.log('Event details missing.');
        return;
      }
  
     return generateMarkup(generateCalendars(params.data),
                            getClass(params),
                            getOrGenerateCalendarId(params));
    };
  })(this);