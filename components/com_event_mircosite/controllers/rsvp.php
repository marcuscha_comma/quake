<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Event_mircosite
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

/**
 * Rsvp controller class.
 *
 * @since  1.6
 */
class Event_mircositeControllerRsvp extends \Joomla\CMS\MVC\Controller\BaseController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit()
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_event_mircosite.edit.rsvp.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_event_mircosite.edit.rsvp.id', $editId);

		// Get the model.
		$model = $this->getModel('Rsvp', 'Event_mircositeModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_event_mircosite&view=rsvpform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.edit', 'com_event_mircosite') || $user->authorise('core.edit.state', 'com_event_mircosite'))
		{
			$model = $this->getModel('Rsvp', 'Event_mircositeModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_event_mircosite.edit.rsvp.id', null);

			// Flush the data from the session.
			$app->setUserState('com_event_mircosite.edit.rsvp.data', null);

			// Redirect to the list screen.
			$this->setMessage(Text::_('COM_EVENT_MIRCOSITE_ITEM_SAVED_SUCCESSFULLY'));
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(Route::_('index.php?option=com_event_mircosite&view=events', false));
			}
			else
			{
                $this->setRedirect(Route::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.delete', 'com_event_mircosite'))
		{
			$model = $this->getModel('Rsvp', 'Event_mircositeModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_event_mircosite.edit.rsvp.id', null);
                $app->setUserState('com_event_mircosite.edit.rsvp.data', null);

                $app->enqueueMessage(Text::_('COM_EVENT_MIRCOSITE_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(Route::_('index.php?option=com_event_mircosite&view=events', false));
			}

			// Redirect to the list screen.
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(Route::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	private function checkUserIsInvited($email = NULL){
		if (!$email) {
			return false;
		}

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT id FROM #__cus_rsvp_invited_list where email = '.$db->quote($email));
		$result             = $db->loadRow();
		if ($result) {
			return true;
		}else{
			return false;
		}
	}

	private function checkUserInMasterList($email = NULL){
		if (!$email) {
			return false;
		}

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT id FROM #__users where email = '.$db->quote($email) .'and (quakeClubUser = -1 or quakeClubUser = 1)');
		$result             = $db->loadResult();

		if ($result) {
			return $result;
		}else{
			return false;
		}
	}

	private function checkUserInRSVP($user_id = 0, $title = ""){
		if (!$user_id) {
			return false;
		}

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT id FROM #__cus_rsvp_tour_data where user_id = '.$db->quote($user_id).' and source ='.$db->quote($title));
		$result             = $db->loadResult();
		
		if ($result) {
			return true;
		}else{
			return false;
		}
	}

	public function fillFrom(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		
		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$jinput = $app->input;

		$email = $jinput->get('email','', 'String');
		$name = $jinput->get('name','', 'String');
		$comp_name = $jinput->get('comp_name','', 'String');
		$designation = $jinput->get('designation','', 'String');
		$mobileNo = $jinput->get('mobileNo','', 'String');
		$delivery = $jinput->get('delivery','', 'String');
		$checkJoinGameStatus = $jinput->get('checkJoinGameStatus','', 'String');
		$event_id = $jinput->get('event_id','', 'Integer');
		$external_link = "";
		$canRegister = true;
		$isInMasterList = false;
		$isInRSVPList = false;

		//get event detail
		$db->setQuery('SELECT title,check_invited_guess,register_status,is_webinar,webinar_id,schedule,alias FROM #__cus_event_rsvp where id = '.$db->quote($event_id));
		$event             = $db->loadObject();

		if ($event->check_invited_guess) {
			$canRegister = $this->checkUserIsInvited($email);
		}

		if ($canRegister) {
		
			$isInMasterList = $this->checkUserInMasterList($email);
			
			if ($isInMasterList) {

				$isInRSVPList = $this->checkUserInRSVP($isInMasterList, $event->alias);

				if ($isInRSVPList) {
					echo '2-';
					echo $name;
					JFactory::getApplication()->close();

				}else{
					$external_link = $this->getWebinar($name, $email, $event->webinar_id, $event->schedule);
	
					$db->setQuery('SELECT MAX(ordering) FROM #__cus_rsvp_tour_data');
					$max             = $db->loadResult();
	
					$redemption = new stdClass();
					$redemption->user_id = $isInMasterList;
					$redemption->designation = $designation;
					$redemption->mobileNo = $mobileNo;
					$redemption->email=$email;
					$redemption->name= $name;
					$redemption->delivery= $delivery;
					$redemption->joinGameStatus= $checkJoinGameStatus=="true"?1:0;
					$redemption->comp_name = $comp_name;
					$redemption->created_by=$isInMasterList;
					$redemption->modified_by=$isInMasterList;
					$redemption->source=$event->alias;
					$redemption->external_link=$external_link;
					$redemption->ordering=$max + 1;
					$redemption->updated_at = date("Y-m-d H:i:s");
	
					// Insert the object into the user profile table.
					$result = JFactory::getDbo()->insertObject('#__cus_rsvp_tour_data', $redemption);

					if($event->is_webinar==0){
						$return = $this->_sendEmail($email);
					}

					echo '3-';
					echo $name;
					JFactory::getApplication()->close();

				}
			}else{
					$external_link = $this->getWebinar($name, $email, $event->webinar_id, $event->schedule);
				
					$user = new stdClass();
					$user->name= $name;
					$user->username = "user_".time();
					$user->email=$email;
					$user->block=0;
					$user->activation="";
					$user->params="{}";
					$user->otep="";
					$user->mobileNo = $mobileNo;
					$user->source = 8;
					$user->quakeClubUser = -1;
	
					// Insert the object into the user profile table.
					$result = JFactory::getDbo()->insertObject('#__users', $user);
					$user_id = JFactory::getDbo()->insertID();
	
					$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_company_user');
					$max             = $db->loadResult();
	
					$company = new stdClass();
					$company->user_id = $user_id;
					$company->name = $comp_name;
					$company->designation = $designation;
					$company->mobile_no = $mobileNo;
					$company->email = $email;
					$company->state=1;
					$company->created_by=$user_id;
					$company->modified_by=$user_id;
					$company->ordering=$max + 1;
	
					// Insert the object into the user profile table.
					$result = JFactory::getDbo()->insertObject('#__cus_qperks_company_user', $company);
	
					$db->setQuery('SELECT MAX(ordering) FROM #__cus_rsvp_tour_data');
					$max             = $db->loadResult();
	
					$redemption = new stdClass();
					$redemption->user_id = $user_id;
					$redemption->created_by=$user_id;
					$redemption->modified_by=$user_id;
					$redemption->designation = $designation;
					$redemption->mobileNo = $mobileNo;
					$redemption->email=$email;
					$redemption->name= $name;
					$redemption->delivery= $delivery;
					$redemption->joinGameStatus= $checkJoinGameStatus=="true"?1:0;
					$redemption->comp_name = $comp_name;
					$redemption->source=$event->alias;
					$redemption->external_link=$external_link;
					$redemption->ordering=$max + 1;
					$redemption->updated_at = date("Y-m-d H:i:s");
	
					// Insert the object into the user profile table.
					$result = JFactory::getDbo()->insertObject('#__cus_rsvp_tour_data', $redemption);

					if($event->is_webinar==0){
						$return = $this->_sendEmail($email);
					}

					echo '3-';
					echo $name;
					JFactory::getApplication()->close();
			}
		}else{
			echo '5-';
			JFactory::getApplication()->close();
		}

		JFactory::getApplication()->close();
	}

	public function login(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		
		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();

		$jinput = $app->input;
		$email = $jinput->get('email','', 'String');
		$password = $jinput->get('password','', 'String');
		$delivery = $jinput->get('delivery','', 'String');
		$checkJoinGameStatus = $jinput->get('checkJoinGameStatus','', 'String');
		$event_id = $jinput->get('event_id','', 'Integer');
		
		// $email = "midoff1@gmail.com";
		// $password = "comma5157";

		//get event detail
		$db->setQuery('SELECT title,check_invited_guess,register_status,is_webinar,webinar_id,schedule,alias FROM #__cus_event_rsvp where id = '.$db->quote($event_id));
		$event             = $db->loadObject();

		if ($event->check_invited_guess) {
			$canRegister = $this->checkUserIsInvited($email);
		}
	
		if ( $canRegister ) {

			$db->setQuery('SELECT id,password,name,email FROM #__users where email ='.$db->quote($email));
			$user             = $db->loadObject();
			
			// $passwordMatch = JUserHelper::verifyPassword($password, $user[1], $user[0]);
			$credentials = array();
			$credentials['username']  = $email;
			$credentials['password']  = $password;
			$credentials['secretkey'] = "";
			$external_link = "";

			$options = array();
			$options['remember'] = false;
			$options['return']   = "index.php?option=com_users&view=profile";

			if (true === $app->login($credentials, $options)) {
				$isInRSVPList = $this->checkUserInRSVP($user->id, $event->alias);
				

				$external_link = $this->getWebinar($user->name, $user->email, $event->webinar_id, $event->schedule);

				$db->setQuery('SELECT MAX(ordering) FROM #__cus_rsvp_tour_data');
				$max             = $db->loadResult();

				$redemption = new stdClass();
				$redemption->user_id = $user->id;
				$redemption->created_by= $user->id;
				$redemption->modified_by= $user->id;
				$redemption->ordering=$max + 1;
				$redemption->source=$event->alias;
				$redemption->email=$email;
				$redemption->delivery=$delivery;
				$redemption->joinGameStatus=$checkJoinGameStatus=="true"?1:0;
				$redemption->external_link=$external_link;
				$redemption->updated_at = date("Y-m-d H:i:s");

				if($event->is_webinar==0){
					$return = $this->_sendEmail($email);
				}

				// Insert the object into the user profile table.
				
				if (!$isInRSVPList) {
					$result = JFactory::getDbo()->insertObject('#__cus_rsvp_tour_data', $redemption);
				}
				echo '3-';
				echo $user->name;
				JFactory::getApplication()->close();
				$app->setUserState('users.login.form.data', array());

			}else{
				echo '0-';
				JFactory::getApplication()->close();

			}
		}else{
			echo '5-';
			JFactory::getApplication()->close();
		}
		
		JFactory::getApplication()->close();
	}

	public function getWebinar($userName = "", $userEmail = "", $webinar_id = 0, $schedule = 0){
		$app = JFactory::getApplication();

		if ($userEmail) {
			$post = [
				"api_key" => "924579a7-73a3-4241-8efc-95b7a0847088",
				"webinar_id" => $webinar_id,
				"first_name" => $userName,
				"email" => $userEmail,
				"schedule" => $schedule
			];
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, 'https://api.webinarjam.com/webinarjam/register');
			$result = curl_exec($ch);
			curl_close($ch);

			$result = json_decode($result);

			if ($result->status !== "success") {
				return "";
			}else{
				return $result->user->live_room_url;
			}
		}else{
			return "";
		}
		JFactory::getApplication()->close();

	}

	public function checkPlayGameStatus(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$app = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;
		$title = $jinput->get('title','', 'String');

		$count = $this->checkPlayGameCount($title);

		echo $count;

		JFactory::getApplication()->close();
	}

	public function checkRSVPEmailExist(){
		// echo "asd";
		// die();
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		
		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$jinput = JFactory::getApplication()->input;
		$event_id = $jinput->get('event_id','', 'String');
		$email = $jinput->get('email','', 'String');

		//get event detail
		$db->setQuery('SELECT title,check_invited_guess,register_status,is_webinar,webinar_id,schedule,alias FROM #__cus_event_rsvp where id = '.$db->quote($event_id));
		$event             = $db->loadObject();

		$db->setQuery('SELECT user_id FROM #__cus_rsvp_tour_data where email ="'.$email .'" and source="'.$event->alias.'"');
		$user             = $db->loadRow();

		if ( $user ) {
			echo 1;
		JFactory::getApplication()->close();

		}else{
			echo 0;
		JFactory::getApplication()->close();

		}

		JFactory::getApplication()->close();
	}

	public function checkEmailIsMember(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$jinput = JFactory::getApplication()->input;
		$email = $jinput->get('email','', 'String');

		$db->setQuery('SELECT id FROM #__users where quakeClubUser = 1 and email ="'.$email.'"');
		$user             = $db->loadRow();

		if ( $user ) {
			echo 1;
		JFactory::getApplication()->close();

		}else{
			echo 0;
		JFactory::getApplication()->close();

		}

		JFactory::getApplication()->close();
	}

	public function checkInvitedEmailExist(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$jinput = JFactory::getApplication()->input;
		$title = $jinput->get('title','', 'String');
		$email = $jinput->get('email','', 'String');

		$db->setQuery('SELECT id FROM #__cus_rsvp_invited_list where email ="'.$email .'"');
		$user             = $db->loadRow();

		if ( $user ) {
			echo 1;
		JFactory::getApplication()->close();

		}else{
			echo 0;
		JFactory::getApplication()->close();

		}

		JFactory::getApplication()->close();
	}

	public function checkPlayGameCount($title = ""){
		
		$db    = JFactory::getDBO();
		$db->setQuery('SELECT count(id) FROM #__cus_rsvp_tour_data where is_play_game = 1 and source ="'.$title.'" ');
		$count             = $db->loadRow();
		return $count[0];
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   array     $data                  The data to send in the email.
	 * @param   stdClass  $contact               The user information to send the email to
	 * @param   boolean   $copy_email_activated  True to send a copy of the email to the user.
	 *
	 * @return  boolean  True on success sending the email, false on failure.
	 *
	 * @since   1.6.4
	 */
	public function _sendEmail($email)
	{

		$config = JFactory::getConfig();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');

		$body = '<p style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="http://quake.com.my/images/edm/image002.jpg" width="312" height="190" /></p>
		<p style="text-align: center;"><span style="font-size: 12pt;"><i><sup><span lang="EN-GB">Powered by</span></sup></i> </span><i><span lang="EN-GB"> <img src="http://quake.com.my/images/edm/image003.png" width="123" height="25" /></span></i></p>
		<p style="text-align: center;"> </p>
		<p style="text-align: center;"><span style="font-size: 16pt;"><strong><b>We have received your registration. Thank You!</b></strong></span></p>
		<p style="text-align: center;"> </p>
		<p style="text-align: center;"><span style="font-size: 13pt;">On the day, please enter the webinar at</span></p>
		<p style="text-align: center;"><span style="font-size: 13pt;"><a href="https://event.webinarjam.com/channel/LightsFinanceAction">https://event.webinarjam.com/channel/LightsFinanceAction</a></span></p>
		<p style="text-align: center;"> </p>
		<p style="text-align: center;"><span style="font-size: 16pt;"><strong>NAMMA SME Series</strong></span></p>
		<p style="text-align: center;"><span style="font-size: 11pt;"><strong>Lights! Finance! Action! </strong></span></p>
		<p style="text-align: center;"><span style="font-size: 11pt;"><strong>SME Business Funding Made Easy</strong></span></p>
		<p style="text-align: center;"> <strong> </strong></p>
		<p style="text-align: center;"><span style="font-size: 13pt;"><strong>Date: 8 Apr 2021, Thursday</strong></span></p>
		<p style="text-align: center;"><span style="font-size: 13pt;"><strong>Time: 8.00pm – 9.00pm</strong></span></p>
		<p style="text-align: center;"> </p>
		<p style="text-align: center;"><span style="font-size: 13pt;">Stay tuned and looking forward to seeing you!</span></p>
		<p style="text-align: center;"><span style="font-size: 13pt;">In the meantime, check out our previous events at: <a href="quakecast">https://quake.com.my/quakecast</a></span></p>
		<p style="text-align: center;"><span style="font-size: 10pt;"> </span></p>
		<p style="text-align: center;"><span style="font-size: 13pt;">Cheers,</span></p>
		<p style="text-align: center;"><span style="font-size: 13pt;">Astro Media Solutions Team</span></p>
		<p style="text-align: center;"><span style="font-size: 10pt;"> </span></p>
		<p style="text-align: center;"> </p>
		<p style="text-align: center;"><span style="font-size: 10pt;">Copyright © 2021 Astro Malaysia Holdings Berhad, All Rights Reserved.</span></p>
		<p style="text-align: center;"><span style="font-size: 10pt;"> You are receiving this email because you have previously collaborated with Astro, or have expressed interest in collaborating with Astro.</span></p>';

		// $to = array($email);
		$to = "midoff1@gmail.com";
		$from = array("mediasolutions@astro.com.my", "Astro Media Solutions");

		# Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject("Registration Confirmed  |  NAMMA SME Webinar @ 8 Apr 2021");
		$mailer->setBody($body);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();

		return $mailer->send();
	}
}
