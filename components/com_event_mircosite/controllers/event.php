<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Event_mircosite
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

/**
 * Event controller class.
 *
 * @since  1.6
 */
class Event_mircositeControllerEvent extends \Joomla\CMS\MVC\Controller\BaseController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit()
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_event_mircosite.edit.event.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_event_mircosite.edit.event.id', $editId);

		// Get the model.
		$model = $this->getModel('Event', 'Event_mircositeModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_event_mircosite&view=eventform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.edit', 'com_event_mircosite') || $user->authorise('core.edit.state', 'com_event_mircosite'))
		{
			$model = $this->getModel('Event', 'Event_mircositeModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_event_mircosite.edit.event.id', null);

			// Flush the data from the session.
			$app->setUserState('com_event_mircosite.edit.event.data', null);

			// Redirect to the list screen.
			$this->setMessage(Text::_('COM_EVENT_MIRCOSITE_ITEM_SAVED_SUCCESSFULLY'));
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(Route::_('index.php?option=com_event_mircosite&view=events', false));
			}
			else
			{
                $this->setRedirect(Route::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.delete', 'com_event_mircosite'))
		{
			$model = $this->getModel('Event', 'Event_mircositeModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_event_mircosite.edit.event.id', null);
                $app->setUserState('com_event_mircosite.edit.event.data', null);

                $app->enqueueMessage(Text::_('COM_EVENT_MIRCOSITE_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(Route::_('index.php?option=com_event_mircosite&view=events', false));
			}

			// Redirect to the list screen.
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(Route::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function fillFrom(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		
		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$jdate = new JDate;

		$jinput = JFactory::getApplication()->input;

		$email = $jinput->get('email','', 'String');
		$name = $jinput->get('name','', 'String');
		$comp_name = $jinput->get('comp_name','', 'String');
		$designation = $jinput->get('designation','', 'String');
		$mobileNo = $jinput->get('mobileNo','', 'String');
		$title = $jinput->get('title','', 'String');
		$delivery = $jinput->get('delivery','', 'String');
		$checkJoinGameStatus = $jinput->get('checkJoinGameStatus','', 'String');
		$checkInvitedGuessStatus = $jinput->get('checkInvitedGuessStatus','', 'String');
		$external_link = "";
		// echo "<pre>";
		// print_r($jinput);
		// echo "</pre>";
		// die();

		if ($checkInvitedGuessStatus == 1) {
			$db->setQuery('SELECT id FROM #__cus_rsvp_invited_list where email = "'.$email.'"');
			$checkInvitedGuessList             = $db->loadRow();
		}else{
			$checkInvitedGuessList = true;
		}

		if ($checkInvitedGuessList) {
		
			$db->setQuery('SELECT id FROM #__users where email = "'.$email.'" and (quakeClubUser = -1 or quakeClubUser = 1)');
			$checkEmailExistNotMember             = $db->loadResult();

			$db->setQuery('SELECT id FROM #__cus_rsvp_tour_data where user_id = "'.$checkEmailExistNotMember.'" and source ="'.$title .'"');
			$checkBigBigTourList             = $db->loadResult();

			if ($checkEmailExistNotMember) {

				if ($checkBigBigTourList) {
					echo '2-';
					echo $name;
					JFactory::getApplication()->close();

				}else{
					// $external_link = $this->getWebinar($name, $email);
					// if ($external_link->status !== "success") {
					// 	echo '0-';
					// }
	
					$db->setQuery('SELECT MAX(ordering) FROM #__cus_rsvp_tour_data');
					$max             = $db->loadResult();
	
					$redemption = new stdClass();
					$redemption->user_id = $checkEmailExistNotMember;
					$redemption->redemption_status = 0;
					$redemption->is_attended = 0;
					$redemption->is_deleted = 0;
					$redemption->is_valid = 1;
					$redemption->state=1;
					$redemption->designation = $designation;
					$redemption->mobileNo = $mobileNo;
					$redemption->email=$email;
					$redemption->name= $name;
					$redemption->delivery= $delivery;
					$redemption->joinGameStatus= $checkJoinGameStatus=="true"?1:0;
					$redemption->comp_name = $comp_name;
					$redemption->created_by=$checkEmailExistNotMember;
					$redemption->modified_by=$checkEmailExistNotMember;
					$redemption->source=$title;
					// $redemption->external_link=$external_link->user->live_room_url;
					$redemption->ordering=$max + 1;
					$redemption->updated_at = $jdate->toSql(true);
	
					// Insert the object into the user profile table.
					$result = JFactory::getDbo()->insertObject('#__cus_rsvp_tour_data', $redemption);
					echo '3-';
					echo $name;
					JFactory::getApplication()->close();

				}
			}else{
				// $external_link = $this->getWebinar($name, $email);
				// if ($external_link->status !== "success") {
				// 	echo '0-';
				// 	JFactory::getApplication()->close();

				// }else{
					$user = new stdClass();
					$user->name= $name;
					$user->username = "user_".time();
					$user->email=$email;
					$user->block=0;
					$user->activation="";
					$user->params="{}";
					$user->otep="";
					$user->mobileNo = $mobileNo;
					$user->source = 8;
					$user->quakeClubUser = -1;
	
					// Insert the object into the user profile table.
					$result = JFactory::getDbo()->insertObject('#__users', $user);
					$user_id = JFactory::getDbo()->insertID();
	
					$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_company_user');
					$max             = $db->loadResult();
	
					$company = new stdClass();
					$company->user_id = $user_id;
					$company->name = $comp_name;
					$company->designation = $designation;
					$company->mobile_no = $mobileNo;
					$company->email = $email;
					$company->state=1;
					$company->created_by=$user_id;
					$company->modified_by=$user_id;
					$company->ordering=$max + 1;
	
					// Insert the object into the user profile table.
					$result = JFactory::getDbo()->insertObject('#__cus_qperks_company_user', $company);
	
					$db->setQuery('SELECT MAX(ordering) FROM #__cus_rsvp_tour_data');
					$max             = $db->loadResult();
	
					$redemption = new stdClass();
					$redemption->user_id = $user_id;
					$redemption->redemption_status = 0;
					$redemption->is_attended = 0;
					$redemption->is_deleted = 0;
					$redemption->is_valid = 1;
					$redemption->state=1;
					$redemption->created_by=$user_id;
					$redemption->modified_by=$user_id;
					$redemption->delivery= $delivery;
					$redemption->joinGameStatus= $checkJoinGameStatus=="true"?1:0;
					$redemption->source=$title;
					// $redemption->external_link=$external_link->user->live_room_url;
					$redemption->ordering=$max + 1;
					$redemption->updated_at = $jdate->toSql(true);
	
					// Insert the object into the user profile table.
					$result = JFactory::getDbo()->insertObject('#__cus_rsvp_tour_data', $redemption);
					echo '3-';
					echo $name;
					JFactory::getApplication()->close();
				// }

			}
		}else{
			echo '5-';
			JFactory::getApplication()->close();
		}

		JFactory::getApplication()->close();
	}

	public function login(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		
		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$jdate = new JDate;

		$jinput = JFactory::getApplication()->input;
		$email = $jinput->get('email','', 'String');
		$password = $jinput->get('password','', 'String');
		$title = $jinput->get('title','', 'String');
		$delivery = $jinput->get('delivery','', 'String');
		$checkJoinGameStatus = $jinput->get('checkJoinGameStatus','', 'String');
		$checkInvitedGuessStatus = $jinput->get('checkInvitedGuessStatus','', 'String');
		
		// $email = "midoff1@gmail.com";
		// $password = "comma5157";

		
		if ($checkInvitedGuessStatus == 1) {
			$db->setQuery('SELECT id FROM #__cus_rsvp_invited_list where email = "'.$email.'"');
			$checkInvitedGuessList             = $db->loadRow();
		}else{
			$checkInvitedGuessList = true;
		}
	
		if ( $checkInvitedGuessList ) {

			$db->setQuery('SELECT id,password,name,email FROM #__users where email ="'.$email .'"');
			$user             = $db->loadRow();
			
			$passwordMatch = JUserHelper::verifyPassword($password, $user[1], $user[0]);
			$credentials = array();
			$credentials['username']  = $email;
			$credentials['password']  = $password;
			$credentials['secretkey'] = "";
			$external_link = "";

			$options = array();
			$options['remember'] = false;
			$options['return']   = "index.php?option=com_users&view=profile";

			if (true === $app->login($credentials, $options)) {

				// $db->setQuery('SELECT external_link FROM #__cus_rsvp_tour_data where user_id = "'.$user[0].'" and source ="'.$title .'"');
				// $getExternalLink             = $db->loadRow();

				// if ( $getExternalLink ) {
					
				// 	echo '2-';
				// 	echo $user[2];
				// 	echo '-'.$getExternalLink[0];
				// 	JFactory::getApplication()->close();

				// }
				// else{
					// $external_link = $this->getWebinar($user[2], $user[3]);
					// if ($external_link->status !== "success") {
					// 	echo '0-';
					// 	JFactory::getApplication()->close();

					// }else{
						$db->setQuery('SELECT MAX(ordering) FROM #__cus_rsvp_tour_data');
						$max             = $db->loadResult();
		
						$redemption = new stdClass();
						$redemption->user_id = $user[0];
						$redemption->redemption_status = 0;
						$redemption->is_attended = 0;
						$redemption->is_deleted = 0;
						$redemption->is_valid = 1;
						$redemption->state=1;
						$redemption->created_by=$user[0];
						$redemption->modified_by=$user[0];
						$redemption->ordering=$max + 1;
						$redemption->source=$title;
						$redemption->email=$email;
						$redemption->delivery=$delivery;
						$redemption->joinGameStatus=$checkJoinGameStatus=="true"?1:0;
						// $redemption->external_link=$external_link->user->live_room_url;
						$redemption->updated_at = date("Y-m-d H:i:s");
		
						// Insert the object into the user profile table.
						$result = JFactory::getDbo()->insertObject('#__cus_rsvp_tour_data', $redemption);
						// echo $user[2];
						echo '3-';
						echo $user[2];
						// echo '-'.$external_link->user->live_room_url;
						JFactory::getApplication()->close();

					// }
				// }
				$app->setUserState('users.login.form.data', array());

			}else{
				echo '0-';
				JFactory::getApplication()->close();

			}
		}else{
			echo '5-';
			JFactory::getApplication()->close();
		}
		
		JFactory::getApplication()->close();
	}

	public function getWebinar($userName = "", $userEmail = ""){
		$app = JFactory::getApplication();

		if ($userEmail) {
			$post = [
				"api_key" => "924579a7-73a3-4241-8efc-95b7a0847088",
				"webinar_id" => 52,
				"first_name" => $userName,
				"email" => $userEmail,
				"schedule" => 62
			];
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, 'https://api.webinarjam.com/webinarjam/register');
			$result = curl_exec($ch);
			curl_close($ch);

			return json_decode($result);
		}else{
			return false;
		}
		JFactory::getApplication()->close();

	}

	public function checkPlayGameStatus(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$app = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;
		$title = $jinput->get('title','', 'String');

		$count = $this->checkPlayGameCount($title);

		echo $count;

		JFactory::getApplication()->close();
	}

	public function checkRSVPEmailExist(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$jinput = JFactory::getApplication()->input;
		$title = $jinput->get('title','', 'String');
		$email = $jinput->get('email','', 'String');

		$db->setQuery('SELECT user_id FROM #__cus_rsvp_tour_data where email ="'.$email .'" and source="'.$title.'"');
		$user             = $db->loadRow();

		if ( $user ) {
			echo 1;
		JFactory::getApplication()->close();

		}else{
			echo 0;
		JFactory::getApplication()->close();

		}

		JFactory::getApplication()->close();
	}

	public function checkEmailIsMember(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$jinput = JFactory::getApplication()->input;
		$email = $jinput->get('email','', 'String');

		$db->setQuery('SELECT id FROM #__users where quakeClubUser = 1 and email ="'.$email.'"');
		$user             = $db->loadRow();

		if ( $user ) {
			echo 1;
		JFactory::getApplication()->close();

		}else{
			echo 0;
		JFactory::getApplication()->close();

		}

		JFactory::getApplication()->close();
	}

	public function checkInvitedEmailExist(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');

		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$jinput = JFactory::getApplication()->input;
		$title = $jinput->get('title','', 'String');
		$email = $jinput->get('email','', 'String');

		$db->setQuery('SELECT id FROM #__cus_rsvp_invited_list where email ="'.$email .'"');
		$user             = $db->loadRow();

		if ( $user ) {
			echo 1;
		JFactory::getApplication()->close();

		}else{
			echo 0;
		JFactory::getApplication()->close();

		}

		JFactory::getApplication()->close();
	}

	public function checkPlayGameCount($title = ""){
		
		$db    = JFactory::getDBO();
		$db->setQuery('SELECT count(id) FROM #__cus_rsvp_tour_data where is_play_game = 1 and source ="'.$title.'" ');
		$count             = $db->loadRow();
		return $count[0];
	}

}
