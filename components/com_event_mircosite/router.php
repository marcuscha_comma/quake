<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Event_mircosite
 * @author     tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright  2020 tan chee liem
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class Event_mircositeRouter
 *
 */
class Event_mircositeRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = JComponentHelper::getComponent('com_event_mircosite')->params;
		$this->noIDs = (bool) $params->get('sef_ids');
		
		$events = new RouterViewConfiguration('events');
		$this->registerView($events);
			$rsvp = new RouterViewConfiguration('rsvp');
			$rsvp->setKey('id')->setParent($events);
			$this->registerView($rsvp);
			$rsvpform = new RouterViewConfiguration('rsvpform');
			$rsvpform->setKey('id');
			$this->registerView($rsvpform);

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('Event_mircositeRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('Event_mircositeHelpersEvent_mircosite', __DIR__ . '/helpers/event_mircosite.php');
			$this->attachRule(new Event_mircositeRulesLegacy($this));
		}
	}


	
		/**
		 * Method to get the segment(s) for an rsvp
		 *
		 * @param   string  $id     ID of the rsvp to retrieve the segments for
		 * @param   array   $query  The request that is built right now
		 *
		 * @return  array|string  The segments of this item
		 */
		public function getRsvpSegment($id, $query)
		{
			if (!strpos($id, ':'))
			{
				$db = Factory::getDbo();
				$dbquery = $db->getQuery(true);
				$dbquery->select($dbquery->qn('alias'))
					->from($dbquery->qn('#__cus_event_rsvp'))
					->where('id = ' . $dbquery->q($id));
				$db->setQuery($dbquery);

				$id .= ':' . $db->loadResult();
			}

			if ($this->noIDs)
			{
				list($void, $segment) = explode(':', $id, 2);

				return array($void => $segment);
			}
			return array((int) $id => $id);
		}
			/**
			 * Method to get the segment(s) for an rsvpform
			 *
			 * @param   string  $id     ID of the rsvpform to retrieve the segments for
			 * @param   array   $query  The request that is built right now
			 *
			 * @return  array|string  The segments of this item
			 */
			public function getRsvpformSegment($id, $query)
			{
				return $this->getRsvpSegment($id, $query);
			}

	
		/**
		 * Method to get the segment(s) for an rsvp
		 *
		 * @param   string  $segment  Segment of the rsvp to retrieve the ID for
		 * @param   array   $query    The request that is parsed right now
		 *
		 * @return  mixed   The id of this item or false
		 */
		public function getRsvpId($segment, $query)
		{
			if ($this->noIDs)
			{
				$db = JFactory::getDbo();
				$dbquery = $db->getQuery(true);
				$dbquery->select($dbquery->qn('id'))
					->from($dbquery->qn('#__cus_event_rsvp'))
					->where('alias = ' . $dbquery->q($segment));
				$db->setQuery($dbquery);

				return (int) $db->loadResult();
			}
			return (int) $segment;
		}
			/**
			 * Method to get the segment(s) for an rsvpform
			 *
			 * @param   string  $segment  Segment of the rsvpform to retrieve the ID for
			 * @param   array   $query    The request that is parsed right now
			 *
			 * @return  mixed   The id of this item or false
			 */
			public function getRsvpformId($segment, $query)
			{
				return $this->getRsvpId($segment, $query);
			}
}
