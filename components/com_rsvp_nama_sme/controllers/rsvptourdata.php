<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_nama_sme
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Rsvptourdata list controller class.
 *
 * @since  1.6
 */
class Rsvp_nama_smeControllerRsvptourdata extends Rsvp_nama_smeController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Rsvptourdata', $prefix = 'Rsvp_nama_smeModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
