<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_nama_sme
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = Factory::getLanguage();
$lang->load('com_rsvp_nama_sme', JPATH_SITE);
$doc = Factory::getDocument();
$doc->addScript(Uri::base() . '/media/com_rsvp_nama_sme/js/form.js');

$user    = Factory::getUser();
$canEdit = Rsvp_nama_smeHelpersRsvp_nama_sme::canUserEdit($this->item, $user);


?>

<div class="rsvptourdatum-edit front-end-edit">
	<?php if (!$canEdit) : ?>
		<h3>
			<?php throw new Exception(Text::_('COM_RSVP_NAMA_SME_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
		</h3>
	<?php else : ?>
		<?php if (!empty($this->item->id)): ?>
			<h1><?php echo Text::sprintf('COM_RSVP_NAMA_SME_EDIT_ITEM_TITLE', $this->item->id); ?></h1>
		<?php else: ?>
			<h1><?php echo Text::_('COM_RSVP_NAMA_SME_ADD_ITEM_TITLE'); ?></h1>
		<?php endif; ?>

		<form id="form-rsvptourdatum"
			  action="<?php echo Route::_('index.php?option=com_rsvp_nama_sme&task=rsvptourdatumform.save'); ?>"
			  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
			
	<input type="hidden" name="jform[id]" value="<?php echo isset($this->item->id) ? $this->item->id : ''; ?>" />

	<input type="hidden" name="jform[ordering]" value="<?php echo isset($this->item->ordering) ? $this->item->ordering : ''; ?>" />

	<input type="hidden" name="jform[state]" value="<?php echo isset($this->item->state) ? $this->item->state : ''; ?>" />

	<input type="hidden" name="jform[checked_out]" value="<?php echo isset($this->item->checked_out) ? $this->item->checked_out : ''; ?>" />

	<input type="hidden" name="jform[checked_out_time]" value="<?php echo isset($this->item->checked_out_time) ? $this->item->checked_out_time : ''; ?>" />

				<?php echo $this->form->getInput('created_by'); ?>
				<?php echo $this->form->getInput('modified_by'); ?>
	<?php echo $this->form->renderField('user_id'); ?>

	<?php echo $this->form->renderField('redemption_status'); ?>

	<?php echo $this->form->renderField('is_attended'); ?>

	<?php echo $this->form->renderField('is_valid'); ?>

	<?php echo $this->form->renderField('is_deleted'); ?>

	<?php echo $this->form->renderField('updated_at'); ?>

	<?php echo $this->form->renderField('name'); ?>

	<?php echo $this->form->renderField('comp_name'); ?>

	<?php echo $this->form->renderField('designation'); ?>

	<?php echo $this->form->renderField('mobileno'); ?>

	<?php echo $this->form->renderField('email'); ?>

	<?php echo $this->form->renderField('source'); ?>

	<?php echo $this->form->renderField('external_link'); ?>

	<?php echo $this->form->renderField('delivery'); ?>

	<?php echo $this->form->renderField('joingamestatus'); ?>

			<div class="control-group">
				<div class="controls">

					<?php if ($this->canSave): ?>
						<button type="submit" class="validate btn btn-primary">
							<?php echo Text::_('JSUBMIT'); ?>
						</button>
					<?php endif; ?>
					<a class="btn"
					   href="<?php echo Route::_('index.php?option=com_rsvp_nama_sme&task=rsvptourdatumform.cancel'); ?>"
					   title="<?php echo Text::_('JCANCEL'); ?>">
						<?php echo Text::_('JCANCEL'); ?>
					</a>
				</div>
			</div>

			<input type="hidden" name="option" value="com_rsvp_nama_sme"/>
			<input type="hidden" name="task"
				   value="rsvptourdatumform.save"/>
			<?php echo HTMLHelper::_('form.token'); ?>
		</form>
	<?php endif; ?>
</div>
