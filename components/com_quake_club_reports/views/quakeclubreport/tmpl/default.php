<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_quake_club_reports');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_quake_club_reports'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_ARTICLE_TOTAL_SHARED'); ?></th>
			<td><?php echo $this->item->article_total_shared; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_ARTICLE_TOP_TEN_SHARED'); ?></th>
			<td><?php echo $this->item->article_top_ten_shared; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_DOWNLOAD_TOTAL_EACH_SEGMENTS'); ?></th>
			<td><?php echo $this->item->download_total_each_segments; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_DOWNLOAD_TOP_TEN_ALL_SEGMENTS'); ?></th>
			<td><?php echo $this->item->download_top_ten_all_segments; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_USER_ACTIVITES'); ?></th>
			<td><?php echo $this->item->user_activites; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_GROWTH_OF_ACTIVITIES'); ?></th>
			<td><?php echo $this->item->growth_of_activities; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_ARTICLE_MOST_SHARED'); ?></th>
			<td><?php echo $this->item->article_most_shared; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOTAL_MEMBERS'); ?></th>
			<td><?php echo $this->item->total_members; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOTAL_NEW_MEMBERS'); ?></th>
			<td><?php echo $this->item->total_new_members; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOP_TEN_USERS_EARNED_QPERKS'); ?></th>
			<td><?php echo $this->item->top_ten_users_earned_qperks; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_USER_PROFILE_COMPLETENESS'); ?></th>
			<td><?php echo $this->item->user_profile_completeness; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOTAL_PRODUCT_REDEEMED'); ?></th>
			<td><?php echo $this->item->total_product_redeemed; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOP_TEN_PRODUCT_REDEEMED'); ?></th>
			<td><?php echo $this->item->top_ten_product_redeemed; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOTAL_STATUS_REDEMPTION_COUNT'); ?></th>
			<td><?php echo $this->item->total_status_redemption_count; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOTAL_REDEEMED_QPERKS'); ?></th>
			<td><?php echo $this->item->total_redeemed_qperks; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOTAL_QPERKS_EARNED'); ?></th>
			<td><?php echo $this->item->total_qperks_earned; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOTAL_REIMBURSEMENT_QPERKS'); ?></th>
			<td><?php echo $this->item->total_reimbursement_qperks; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOP_TEN_FRIEND_REFERRAL'); ?></th>
			<td><?php echo $this->item->top_ten_friend_referral; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_TOTAL_SUCCESS_FRIEND_REFERRAL'); ?></th>
			<td><?php echo $this->item->total_success_friend_referral; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_MOST_SHARED_DOWNLOAD_BY_DAY'); ?></th>
			<td><?php echo $this->item->most_shared_download_by_day; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_YEAR'); ?></th>
			<td><?php echo $this->item->year; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_MONTH'); ?></th>
			<td><?php echo $this->item->month; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_FORM_LBL_QUAKECLUBREPORT_WEEK'); ?></th>
			<td><?php echo $this->item->week; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_quake_club_reports&task=quakeclubreport.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_QUAKE_CLUB_REPORTS_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_quake_club_reports.quakeclubreport.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_QUAKE_CLUB_REPORTS_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_QUAKE_CLUB_REPORTS_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_QUAKE_CLUB_REPORTS_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_quake_club_reports&task=quakeclubreport.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_QUAKE_CLUB_REPORTS_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>