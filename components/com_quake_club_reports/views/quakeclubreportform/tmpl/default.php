<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = Factory::getLanguage();
$lang->load('com_quake_club_reports', JPATH_SITE);
$doc = Factory::getDocument();
$doc->addScript(Uri::base() . '/media/com_quake_club_reports/js/form.js');

$user    = Factory::getUser();
$canEdit = Quake_club_reportsHelpersQuake_club_reports::canUserEdit($this->item, $user);


?>

<div class="quakeclubreport-edit front-end-edit">
	<?php if (!$canEdit) : ?>
		<h3>
			<?php throw new Exception(Text::_('COM_QUAKE_CLUB_REPORTS_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
		</h3>
	<?php else : ?>
		<?php if (!empty($this->item->id)): ?>
			<h1><?php echo Text::sprintf('COM_QUAKE_CLUB_REPORTS_EDIT_ITEM_TITLE', $this->item->id); ?></h1>
		<?php else: ?>
			<h1><?php echo Text::_('COM_QUAKE_CLUB_REPORTS_ADD_ITEM_TITLE'); ?></h1>
		<?php endif; ?>

		<form id="form-quakeclubreport"
			  action="<?php echo Route::_('index.php?option=com_quake_club_reports&task=quakeclubreport.save'); ?>"
			  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
			
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />

	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->getInput('created_by'); ?>
				<?php echo $this->form->getInput('modified_by'); ?>
	<?php echo $this->form->renderField('article_total_shared'); ?>

	<?php echo $this->form->renderField('article_top_ten_shared'); ?>

	<?php echo $this->form->renderField('download_total_each_segments'); ?>

	<?php echo $this->form->renderField('download_top_ten_all_segments'); ?>

	<?php echo $this->form->renderField('user_activites'); ?>

	<?php echo $this->form->renderField('growth_of_activities'); ?>

	<?php echo $this->form->renderField('article_most_shared'); ?>

	<?php echo $this->form->renderField('total_members'); ?>

	<?php echo $this->form->renderField('total_new_members'); ?>

	<?php echo $this->form->renderField('top_ten_users_earned_qperks'); ?>

	<?php echo $this->form->renderField('user_profile_completeness'); ?>

	<?php echo $this->form->renderField('total_product_redeemed'); ?>

	<?php echo $this->form->renderField('top_ten_product_redeemed'); ?>

	<?php echo $this->form->renderField('total_status_redemption_count'); ?>

	<?php echo $this->form->renderField('total_redeemed_qperks'); ?>

	<?php echo $this->form->renderField('total_qperks_earned'); ?>

	<?php echo $this->form->renderField('total_reimbursement_qperks'); ?>

	<?php echo $this->form->renderField('top_ten_friend_referral'); ?>

	<?php echo $this->form->renderField('total_success_friend_referral'); ?>

	<?php echo $this->form->renderField('most_shared_download_by_day'); ?>

	<?php echo $this->form->renderField('year'); ?>

	<?php echo $this->form->renderField('month'); ?>

	<?php echo $this->form->renderField('week'); ?>

			<div class="control-group">
				<div class="controls">

					<?php if ($this->canSave): ?>
						<button type="submit" class="validate btn btn-primary">
							<?php echo Text::_('JSUBMIT'); ?>
						</button>
					<?php endif; ?>
					<a class="btn"
					   href="<?php echo Route::_('index.php?option=com_quake_club_reports&task=quakeclubreportform.cancel'); ?>"
					   title="<?php echo Text::_('JCANCEL'); ?>">
						<?php echo Text::_('JCANCEL'); ?>
					</a>
				</div>
			</div>

			<input type="hidden" name="option" value="com_quake_club_reports"/>
			<input type="hidden" name="task"
				   value="quakeclubreportform.save"/>
			<?php echo HTMLHelper::_('form.token'); ?>
		</form>
	<?php endif; ?>
</div>
