<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
jimport('phpspreadsheet.phpspreadsheet');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Joomla\CMS\Date\Date;
use Joomla\CMS\Factory;

/**
 * Quakeclubreports list controller class.
 *
 * @since  1.6
 */
class Quake_club_reportsControllerQuakeclubreports extends Quake_club_reportsController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Quakeclubreports', $prefix = 'Quake_club_reportsModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	public function record_reports(){
		$total_shared_articles = $this->total_shared_articles();
		$total_shared_articles_by_week = $this->total_shared_articles_by_week();
		$top_ten_shared_articles = $this->top_ten_shared_articles();
		$top_ten_shared_articles_by_week = $this->top_ten_shared_articles_by_week();
		$total_download_articles_each_segments = $this->total_download_articles_each_segments();
		$top_ten_downloaded_articles = $this->top_ten_downloaded_articles();
		$top_ten_downloaded_articles_by_week = $this->top_ten_downloaded_articles_by_week();
		$top_users_activities = $this->top_users_activities();
		$top_users_activities_by_week = $this->top_users_activities_by_week();
		$growth_of_activities = $this->growth_of_activities();
		$most_shared_articles_each_segments = $this->most_shared_articles_each_segments();
		$total_members = $this->total_members();
		$total_new_member = $this->total_new_member();
		$top_ten_users_earned = $this->top_ten_users_earned();
		$user_profile_completeness = $this->user_profile_completeness();
		$total_number_product_redeemed = $this->total_number_product_redeemed();
		$total_number_product_redeemed_by_week = $this->total_number_product_redeemed_by_week();
		$top_ten_product_redeemed = $this->top_ten_product_redeemed();
		$top_ten_product_redeemed_by_week = $this->top_ten_product_redeemed_by_week();
		$total_number_of_redeemed_status = $this->total_number_of_redeemed_status();
		$total_number_of_redeemed_status_by_week = $this->total_number_of_redeemed_status_by_week();
		$total_number_of_redeemed_qperks = $this->total_number_of_redeemed_qperks();
		$total_number_of_redeemed_qperks_by_week = $this->total_number_of_redeemed_qperks_by_week();
		$total_qperks_earned = $this->total_qperks_earned();
		$total_qperks_earned_by_week = $this->total_qperks_earned_by_week();
		$total_qperks_reimbursement = $this->total_qperks_reimbursement();
		$total_qperks_reimbursement_by_week = $this->total_qperks_reimbursement_by_week();
		$top_ten_friend_referral = $this->top_ten_friend_referral();
		$total_success_friend_referral = $this->total_success_friend_referral();
		$most_shared_and_download_by_day = $this->most_shared_and_download_by_day();

		$reports = new stdClass();
		$reports->year=date('Y');
		$reports->month=date('m');
		$reports->week= date( 'W', strtotime( 'last week' ) );
		$reports->article_total_shared=$total_shared_articles;
		$reports->article_total_shared_by_week=$total_shared_articles_by_week;
		$reports->article_top_ten_shared=$top_ten_shared_articles;
		$reports->article_top_ten_shared_by_week=$top_ten_shared_articles_by_week;
		$reports->download_total_each_segments=$total_download_articles_each_segments;
		$reports->download_top_ten_all_segments=$top_ten_downloaded_articles;
		$reports->download_top_ten_all_segments_by_week=$top_ten_downloaded_articles_by_week;
		$reports->user_activites=$top_users_activities;
		$reports->user_activites_by_week=$top_users_activities_by_week;
		$reports->growth_of_activities=$growth_of_activities;
		$reports->article_most_shared = $most_shared_articles_each_segments;
		$reports->total_members=$total_members;
		$reports->total_new_members=$total_new_member;
		$reports->top_ten_users_earned_qperks=$top_ten_users_earned;
		$reports->user_profile_completeness=$user_profile_completeness;
		$reports->total_product_redeemed=$total_number_product_redeemed;
		$reports->total_product_redeemed_by_week=$total_number_product_redeemed_by_week;
		$reports->top_ten_product_redeemed=$top_ten_product_redeemed;
		$reports->top_ten_product_redeemed_by_week=$top_ten_product_redeemed_by_week;
		$reports->total_status_redemption_count=$total_number_of_redeemed_status;
		$reports->total_status_redemption_count_by_week=$total_number_of_redeemed_status_by_week;
		$reports->total_redeemed_qperks=$total_number_of_redeemed_qperks;
		$reports->total_redeemed_qperks_by_week=$total_number_of_redeemed_qperks_by_week;
		$reports->total_qperks_earned=$total_qperks_earned;
		$reports->total_qperks_earned_by_week=$total_qperks_earned_by_week;
		$reports->total_reimbursement_qperks=$total_qperks_reimbursement;
		$reports->total_reimbursement_qperks_by_week=$total_qperks_reimbursement_by_week;
		$reports->top_ten_friend_referral=$top_ten_friend_referral;
		$reports->total_success_friend_referral=$total_success_friend_referral;
		$reports->most_shared_download_by_day=$most_shared_and_download_by_day;
		$reports->state=1;

		// Insert the object into the user profile table.
		$result = JFactory::getDbo()->insertObject('#__cus_quake_club_reports', $reports);
	}

	public function download_centre_record_reports(){
		$getCredentialsByMonth = $this->getCredentialsByMonth();
		$getCredentialsByWeek = $this->getCredentialsByWeek();
		$getBrandProfilesByMonth = $this->getBrandProfilesByMonth();
		$getBrandProfilesByWeek = $this->getBrandProfilesByWeek();
		$getRateCardsByMonth = $this->getRateCardsByMonth();
		$getRateCardsByWeek = $this->getRateCardsByWeek();
		$getProgrammeSchedulesByMonth = $this->getProgrammeSchedulesByMonth();
		$getProgrammeSchedulesByweek = $this->getProgrammeSchedulesByweek();

		$reports = new stdClass();
		$reports->year=date('Y');
		$reports->month=date('m');
		$reports->week= date( 'W', strtotime( 'last week' ) );
		$reports->credentials_by_month=$getCredentialsByMonth;
		$reports->credentials_by_week=$getCredentialsByWeek;
		$reports->brand_profiles_by_month=$getBrandProfilesByMonth;
		$reports->brand_profiles_by_week=$getBrandProfilesByWeek;
		$reports->rate_cards_by_month=$getRateCardsByMonth;
		$reports->rate_cards_by_week=$getRateCardsByWeek;
		$reports->programme_schedules_by_month=$getProgrammeSchedulesByMonth;
		$reports->programme_schedules_by_week=$getProgrammeSchedulesByweek;
		$reports->state=1;

		// Insert the object into the user profile table.
		$result = JFactory::getDbo()->insertObject('#__cus_quake_club_download_centre_reports', $reports);
	}
	
	public function total_shared_articles(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) FROM #__cus_qperks_user_point WHERE type = 3 and created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE)');
		$segments = $db->loadResult();

		return $segments;
	}

	public function total_shared_articles_by_week(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) FROM #__cus_qperks_user_point WHERE type = 3 and created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE)');
		$segments = $db->loadResult();
		
		return $segments;
	}

	public function total_download_articles_each_segments(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(f.id) as total, f.attach_file as source, f.segment, count(up.user_id) as user_count FROM #__cus_programme_scheduler_files f join qkpe1_cus_qperks_user_point up on up.source = f.attach_file where up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by f.segment order by total desc limit 10');
		$result = $db->loadObjectList();

		foreach ($result as $key => $item) {
			switch ($item->segment) {
				case '1':
				$item->value = "Malay";
				break;
				case '2':
				$item->value = "Chinese";
				break;
				case '3':
				$item->value = "Indian";			
				break;
				case '4':
				$item->value = "English";				
				break;
				case '5':
				$item->value = "GenNext";				
				break;
				case '6':
				$item->value = "Sports";			
				break;
				case '7':
				$item->value = "News";				
				break;
				case '8':
				$item->value = "Korean";			
				break;
				default:
				break;
			}
		}
		return json_encode($result);
	}

	public function most_shared_articles_each_segments(){
		$ret = $this->getMonthDate();
		
		$tmp_array=[];
		for ($i=1; $i <= 9; $i++) {
				switch ($i) {
					case '1':
					$db = JFactory::getDBO();
					$db->setQuery('SELECT count(up.id) total, up.source, c.id, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and v.value = '.$i.' and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 1');
					$result = $db->loadObjectList();
					$result[0]->value = "Malay";
					$tmp_array[] = $result;
					break;
					case '2':
					$db = JFactory::getDBO();
					$db->setQuery('SELECT count(up.id) total, up.source, c.id, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and v.value = '.$i.' and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 1');
					$result = $db->loadObjectList();
					$result[0]->value = "Chinese";
					$tmp_array[] = $result;
					break;
					case '3':
					$db = JFactory::getDBO();
					$db->setQuery('SELECT count(up.id) total, up.source, c.id, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and v.value = '.$i.' and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 1');
					$result = $db->loadObjectList();
					$result[0]->value = "Indian";			
					$tmp_array[] = $result;
					break;
					case '4':
					$db = JFactory::getDBO();
					$db->setQuery('SELECT count(up.id) total, up.source, c.id, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and v.value = '.$i.' and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 1');
					$result = $db->loadObjectList();
					$result[0]->value = "English";				
					$tmp_array[] = $result;
					break;
					case '5':
					$db = JFactory::getDBO();
					$db->setQuery('SELECT count(up.id) total, up.source, c.id, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and v.value = '.$i.' and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 1');
					$result = $db->loadObjectList();
					$result[0]->value = "GenNext";				
					$tmp_array[] = $result;
					break;
					case '6':
					$db = JFactory::getDBO();
					$db->setQuery('SELECT count(up.id) total, up.source, c.id, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and v.value = '.$i.' and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 1');
					$result = $db->loadObjectList();
					$result[0]->value = "Sports";			
					$tmp_array[] = $result;
					break;
					case '7':
					$db = JFactory::getDBO();
					$db->setQuery('SELECT count(up.id) total, up.source, c.id, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and v.value = '.$i.' and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 1');
					$result = $db->loadObjectList();
					$result[0]->value = "News";				
					$tmp_array[] = $result;
					break;
					case '8':
					$db = JFactory::getDBO();
					$db->setQuery('SELECT count(up.id) total, up.source, c.id, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and v.value = '.$i.' and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 1');
					$result = $db->loadObjectList();
					$result[0]->value = "Korean";			
					$tmp_array[] = $result;
					break;
					default:
					$db = JFactory::getDBO();
					$db->setQuery('SELECT count(up.id) total, up.source, c.id, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source left join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and v.value is NULL and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 1');
					$result = $db->loadObjectList();
					$result[0]->value = "Others";
					$tmp_array[] = $result;
					break;
				}
			}

		return json_encode($tmp_array);
	}

	public function top_ten_shared_articles(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) as total , source, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source left join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by up.source order by total desc limit 10');
		$segments = $db->loadObjectList();

		foreach ($segments as $key => $item) {
			switch ($item->value) {
				case '1':
				$item->value = "Malay";
				break;
				case '2':
				$item->value = "Chinese";
				break;
				case '3':
				$item->value = "Indian";			
				break;
				case '4':
				$item->value = "English";				
				break;
				case '5':
				$item->value = "GenNext";				
				break;
				case '6':
				$item->value = "Sports";			
				break;
				case '7':
				$item->value = "News";				
				break;
				case '8':
				$item->value = "Korean";			
				break;
				default:
				$item->value = "Others";			
				break;
			}
		}
		
		return json_encode($segments);
	}

	public function top_ten_shared_articles_by_week(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) as total , up.source, v.value FROM #__cus_qperks_user_point up join qkpe1_content c on c.alias = up.source left join qkpe1_fields_values v on v.field_id = 1 and v.item_id = c.id WHERE up.type = 3 and up.created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by up.source order by total desc limit 10');
		$segments = $db->loadObjectList();

		foreach ($segments as $key => $item) {
			switch ($item->value) {
				case '1':
				$item->value = "Malay";
				break;
				case '2':
				$item->value = "Chinese";
				break;
				case '3':
				$item->value = "Indian";			
				break;
				case '4':
				$item->value = "English";				
				break;
				case '5':
				$item->value = "GenNext";				
				break;
				case '6':
				$item->value = "Sports";			
				break;
				case '7':
				$item->value = "News";				
				break;
				case '8':
				$item->value = "Korean";			
				break;
				default:
				$item->value = "Others";			
				break;
			}
		}
		
		return json_encode($segments);
	}

	public function top_ten_downloaded_articles(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(f.id) as total, f.attach_file as source, f.segment FROM #__cus_programme_scheduler_files f join qkpe1_cus_qperks_user_point up on up.source = f.attach_file where up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) and f.attach_file <> "" group by f.attach_file order by total desc limit 10');
		$result = $db->loadObjectList();

		foreach ($result as $key => $item) {
			switch ($item->segment) {
				case '1':
				$item->value = "Malay";
				break;
				case '2':
				$item->value = "Chinese";
				break;
				case '3':
				$item->value = "Indian";			
				break;
				case '4':
				$item->value = "English";				
				break;
				case '5':
				$item->value = "GenNext";				
				break;
				case '6':
				$item->value = "Sports";			
				break;
				case '7':
				$item->value = "News";				
				break;
				case '8':
				$item->value = "Korean";			
				break;
				default:
				$item->value = "Others";			
				break;
			}
		}

		return json_encode($result);
	}

	public function top_ten_downloaded_articles_by_week(){
		$ret = $this->getWeekDate();
		
		$db = JFactory::getDBO();
		
		$db->setQuery('SELECT count(f.id) as total, f.attach_file as source, f.segment FROM #__cus_programme_scheduler_files f join qkpe1_cus_qperks_user_point up on up.source = f.attach_file where up.created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) and f.attach_file <> "" group by f.attach_file order by total desc limit 10');
		$result = $db->loadObjectList();

		foreach ($result as $key => $item) {
			switch ($item->segment) {
				case '1':
				$item->value = "Malay";
				break;
				case '2':
				$item->value = "Chinese";
				break;
				case '3':
				$item->value = "Indian";			
				break;
				case '4':
				$item->value = "English";				
				break;
				case '5':
				$item->value = "GenNext";				
				break;
				case '6':
				$item->value = "Sports";			
				break;
				case '7':
				$item->value = "News";				
				break;
				case '8':
				$item->value = "Korean";			
				break;
				default:
				$item->value = "Others";			
				break;
			}
		}

		return json_encode($result);
	}

	public function top_users_activities(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total, type FROM #__cus_qperks_user_point where created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by type order by type desc');
		$segments = $db->loadAssocList();

		return json_encode($segments);
	}

	public function top_users_activities_by_week(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total, type FROM #__cus_qperks_user_point where created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by type order by type desc');
		$segments = $db->loadAssocList();

		return json_encode($segments);
	}

	public function growth_of_activities(){
		$currentWeekNumber = date('W');
		$year = date('Y');
		if (date('W') == 1) {
			$year = date( 'Y', strtotime( 'last year' ) );
		}
		$dto = new DateTime();
		$dto->setISODate($year, $currentWeekNumber-2);
		$ret2['week_start'] = $dto->format('Y-m-d 23:59:59');
		$dto->modify('+6 days');
		$ret2['week_end'] = $dto->format('Y-m-d 23:59:59');

		$dto = new DateTime();
		$dto->setISODate($year, $currentWeekNumber-1);
		$ret['week_start'] = $dto->format('Y-m-d 23:59:59');
		$dto->modify('+6 days');
		$ret['week_end'] = $dto->format('Y-m-d 23:59:59');

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total FROM #__cus_qperks_user_point where type in (3,7,13,14) and created_on between "'.$ret2['week_start'] .'" and "' . $ret2['week_end'].'"');
		$result = $db->loadResult();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total FROM #__cus_qperks_user_point where type in (3,7,13,14) and created_on between "'.$ret['week_start'] .'" and "' . $ret['week_end'].'"');
		$result2 = $db->loadResult();
		return ($result2-$result) / 100;
	}

	public function total_members(){
		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total FROM #__users where quakeClubUser = 1');
		$result = $db->loadResult();

		return $result;
	}

	public function total_new_member(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total FROM #__users where registerDate between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE)');
		$result = $db->loadResult();

		return $result;
	}

	public function top_ten_users_earned(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT sum(up.point) as total, u.name, u.email FROM #__cus_qperks_user_point as up join qkpe1_users u on u.id = up.user_id where up.created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by up.user_id order by total desc limit 10');
		$result = $db->loadAssocList();

		return json_encode($result);
	}

	public function user_profile_completeness(){
		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total FROM #__cus_qperks_user_point where type = 1');
		$result = $db->loadResult();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total FROM #__users where quakeClubUser = 1');
		$result2 = $db->loadResult();

		return (float)$result/(float)$result2*100;

	}

	public function total_number_product_redeemed(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total FROM #__cus_quake_club_redemption where state > 0 and created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE)');
		$result = $db->loadResult();

		return $result;
	}

	public function total_number_product_redeemed_by_week(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total FROM #__cus_quake_club_redemption where state > 0 and created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE)');
		$result = $db->loadResult();

		return $result;
	}

	public function top_ten_product_redeemed(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(r.id) as total, p.name FROM #__cus_quake_club_redemption r join qkpe1_cus_qperks_products p on p.id = r.product_id where r.state > 0 and r.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by r.product_id order by total desc limit 10');
		$result = $db->loadAssocList();

		return json_encode($result);
	}

	public function top_ten_product_redeemed_by_week(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(r.id) as total, p.name FROM #__cus_quake_club_redemption r join qkpe1_cus_qperks_products p on p.id = r.product_id where r.state > 0 and r.created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by r.product_id order by total desc limit 10');
		$result = $db->loadAssocList();

		return json_encode($result);
	}
	
	public function total_number_of_redeemed_status(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total, status FROM #__cus_quake_club_redemption where state > 0 and created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by status');
		$result = $db->loadAssocList();

		
		return json_encode($result);
	}

	public function total_number_of_redeemed_status_by_week(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as total, status FROM #__cus_quake_club_redemption where state > 0 and created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by status');
		$result = $db->loadAssocList();

		return json_encode($result);
	}

	public function total_number_of_redeemed_qperks(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as count, status, sum(point) as total FROM #__cus_quake_club_redemption where state > 0 and created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by status');
		$result = $db->loadAssocList();

		return json_encode($result);
	}

	public function total_number_of_redeemed_qperks_by_week(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) as count, status, sum(point)  as total FROM #__cus_quake_club_redemption where state > 0 and created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by status');
		$result = $db->loadObjectList();

		return json_encode($result);
	}

	public function total_qperks_earned(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT sum(point) FROM #__cus_qperks_user_point where state > 0 and created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE)');
		$result = $db->loadResult();

		return $result;
	}

	public function total_qperks_earned_by_week(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT sum(point) FROM #__cus_qperks_user_point where state > 0 and created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE)');
		$result = $db->loadResult();

		return $result;
	}

	public function total_qperks_reimbursement(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT sum(point) FROM #__cus_qperks_user_point where state > 0 and type=10 and created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE)');
		$result = $db->loadResult();

		return $result;
	}

	public function total_qperks_reimbursement_by_week(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT sum(point) FROM #__cus_qperks_user_point where state > 0 and type=10 and created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE)');
		$result = $db->loadResult();

		return $result;
	}

	public function top_ten_friend_referral(){
		$db = JFactory::getDBO();
		$db->setQuery('SELECT sum(s.hits) as total, u.name, u.email FROM #__cus_qperks_referrals_session s join qkpe1_users u on u.id = s.user_id group by user_id order by total desc limit 10');
		$result = $db->loadAssocList();

		return json_encode($result);
	}

	public function total_success_friend_referral(){
		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(id) FROM #__cus_qperks_referrals');
		$result = $db->loadResult();

		return $result;
	}

	public function most_shared_and_download_by_day(){
		$currentWeekNumber = date( 'W', strtotime( 'last week' ) );
		$dto = new DateTime();
		$year = date('Y');
		if (date('W') == 1) {
			$year = date( 'Y', strtotime( 'last year' ) );
		}
		$dto->setISODate($year, $currentWeekNumber);
		$ret['week_start'] = $dto->format('Y-m-d');

		$tmp_date = $ret['week_start'];
		for ($i=1; $i <= 7; $i++) {
			$db = JFactory::getDBO();
			$db->setQuery('SELECT count(id) as total,source FROM #__cus_qperks_user_point where state > 0 and created_on between "'.$tmp_date .' 00:00:00" and "' . $tmp_date.' 23:59:59" and type = 3 group by source order by total desc limit 1');
			$result = $db->loadAssocList();

			$db = JFactory::getDBO();
			$db->setQuery('SELECT count(id) as total,source FROM #__cus_qperks_user_point where state > 0 and created_on between "'.$tmp_date .' 00:00:00" and "' . $tmp_date.' 23:59:59" and type = 7 group by source order by total desc limit 1');
			$result2 = $db->loadAssocList();
			

			$tmp_array[]= array(
				"date" =>$tmp_date,
				"share" => $result,
				"download" => $result2
			);

			$dto->modify('+1 days');
			$tmp_date = $dto->format('Y-m-d');
			
		}
		return json_encode($tmp_array);

	}

	public function getWeekDate(){
		$lastWeekNumber = date( 'W', strtotime( 'last week' ) );
		$dto = new DateTime();
		$year = date('Y');
		if (date('W') == 1) {
			$year = date( 'Y', strtotime( 'last year' ) );
		}
		$dto->setISODate($year, $lastWeekNumber);
		$ret['week_start'] = $dto->format('Y-m-d');
		$dto->modify('+6 days');
		$ret['week_end'] = $dto->format('Y-m-d');

		return $ret;
	}

	public function getMonthDate(){
		$ret['first_day_this_month'] = date('Y-m-01',strtotime('last month'));
		$ret['last_day_this_month']  = date('Y-m-t',strtotime('last month'));
		// $date = "2020-04-01";
		// $ret['first_day_this_month'] = date('Y-m-01',strtotime($date));
		// $ret['last_day_this_month']  = date('Y-m-t',strtotime($date));
		return $ret;
	}

	public function getCredentialsByMonth(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) total, cf.title, cf.attach_file FROM #__cus_credential_files cf join qkpe1_cus_qperks_user_point up on up.source = cf.attach_file where up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by cf.attach_file order by total desc');
		$result = $db->loadObjectList();

		return json_encode($result);
	}

	public function getCredentialsByWeek(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) total, cf.title, cf.attach_file FROM #__cus_credential_files cf join qkpe1_cus_qperks_user_point up on up.source = cf.attach_file where up.created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by cf.attach_file order by total desc');
		$result = $db->loadObjectList();

		return json_encode($result);
	}

	public function getBrandProfilesByMonth(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) total, cf.title, cf.attach_file FROM #__cus_channel_profile_files cf join qkpe1_cus_qperks_user_point up on up.source = cf.attach_file where up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by cf.attach_file order by total desc');
		$result = $db->loadObjectList();
		return json_encode($result);
	}

	public function getBrandProfilesByWeek(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) total, cf.title, cf.attach_file FROM #__cus_channel_profile_files cf join qkpe1_cus_qperks_user_point up on up.source = cf.attach_file where up.created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by cf.attach_file order by total desc');
		$result = $db->loadObjectList();

		return json_encode($result);
	}

	public function getRateCardsByMonth(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) total, cf.title, cf.attach_file FROM #__cus_rate_card_files cf join qkpe1_cus_qperks_user_point up on up.source = cf.attach_file where up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by cf.attach_file order by total desc');
		$result = $db->loadObjectList();

		return json_encode($result);
	}

	public function testtest(){
		$timezone = Factory::getUser()->getTimezone();
		$db = JFactory::getDBO();
		$jdate = new JDate;

		$db->setQuery('SELECT count(id) as total FROM #__cus_qperks_user_point WHERE type in(7,3,11,13,14) and state > 0 and user_id = 1566 and date(created_on) = "'.date("Y-m-d").'"');
		$times = $db->loadObjectList();
		echo "<pre>";
		echo date("Y-m-d h:m:s");
		echo "</pre>";
		
		$config = JFactory::getConfig();
		$offset = $config->get('offset');
		$date = new JDate('now', $offset);
		$convertedDate = $date->format('Y-m-d');
		echo $offset;
		echo "<br>";
		$date = new JDate('now', $offset);
		$date2 = $date->format('Y-m-d');
		echo "real".$date . "and" . $date2;
		echo "<br>";
		echo "asd" . $jdate;
		echo "<br>";

		$tmp = JFactory::getDate()->format('Y-m-d h:m:s');
		echo "edit". JFactory::getDate('-4 hour')->format('Y-m-d h:m:s');
		echo "<br>";
		echo "sql".JFactory::getDate()->toSql(true);
		echo "<br>";
		echo "jdate".$jdate->toSql(true);;
		echo "<br>";
		echo "factory".\JFactory::getDate()->toSql(true);
		
		

		// echo new Date();

	}

	public function checkUserPointTele(){
		$user_id = 1386;
		$db = JFactory::getDBO();
		$db->setQuery('select sum(month01+month02+month03+month04+month05+month06+month07+month08+month09+month10+month11+month12) as total
		from qkpe1_cus_quake_club_qperks_monthly where user_id ='.$user_id);
		$result1 = $db->loadObjectList();

		

		$db = JFactory::getDBO();
		$db->setQuery('select 
		case 
		 when r.promo = 1 then r.quantity * r.promo_qperks
		 when r.promo = 0 then r.quantity * r.point
		end as point
		from qkpe1_cus_quake_club_redemption r
		where r.user_id ='.$user_id.' and r.status not in (0)');
		$result2 = $db->loadObjectList();

		$tmp = 0;
		foreach ($result2 as $key => $value) {
			$tmp = $tmp + $value->point;
		}

		$db = JFactory::getDBO();
		$db->setQuery('select sum(point)
		from qkpe1_cus_qperks_user_point
		where user_id ='.$user_id.' and type not in(10)');
		$result3 = $db->loadObjectList();

		echo "<pre>";
		echo "result1";
		print_r($result1);
		echo "<br>";
		echo "result2 = ";
		print_r($tmp);
		echo "<br>";
		echo "result2";
		print_r($result3);
		echo "</pre>";
	}

	public function getRateCardsByWeek(){
		
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) total, cf.title, cf.attach_file FROM #__cus_rate_card_files cf join qkpe1_cus_qperks_user_point up on up.source = cf.attach_file where up.created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by cf.attach_file order by total desc');
		$result = $db->loadObjectList();

		return json_encode($result);
	}

	public function getProgrammeSchedulesByMonth(){
		$ret = $this->getMonthDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) total, cf.title as file_title, s.title as segment, c.title as channel, cf.attach_file, cf.month, cf.year FROM #__cus_programme_scheduler_files cf join qkpe1_cus_qperks_user_point up on up.source = cf.attach_file join qkpe1_cus_segments s on s.id = cf.segment join qkpe1_cus_channels c on c.id = cf.channel where cf.attach_file <> "" and up.created_on between CAST("'.$ret['first_day_this_month'] .'" AS DATE) and CAST("' . $ret['last_day_this_month'].'" AS DATE) group by cf.attach_file order by s.id asc,c.title asc,cf.month asc');
		$result = $db->loadObjectList();

		return json_encode($result);
	}

	public function getProgrammeSchedulesByWeek(){
		$ret = $this->getWeekDate();

		$db = JFactory::getDBO();
		$db->setQuery('SELECT count(up.id) total, cf.title as file_title, s.title as segment, c.title as channel, cf.attach_file, cf.month, cf.year FROM #__cus_programme_scheduler_files cf join qkpe1_cus_qperks_user_point up on up.source = cf.attach_file join qkpe1_cus_segments s on s.id = cf.segment join qkpe1_cus_channels c on c.id = cf.channel where cf.attach_file <> "" and up.created_on between CAST("'.$ret['week_start'] .'" AS DATE) and CAST("' . $ret['week_end'].'" AS DATE) group by cf.attach_file order by s.id asc,c.title asc,cf.month asc');
		$result = $db->loadObjectList();

		return json_encode($result);
	}

	public function getTopUserActivitiesById(){
		$ret = $this->getMonthDate();
		$variable = [
			1664,
			7814,
			7162,
			7327,
			7699,
			6615,
			1641,
			563,
			6746,
			7497,
			677,
			919,
			7686,
			7620,
			7274,
			7768,
			3105,
			7557,
			7591,
			6614,
			1629,
			7520,
			7492,
			1363,
			6743,
			7562,
			7549,
			6802,
			7276,
			7728,
			7735,
			7275,
			1386,
			7348,
			7783,
			7777,
			6671,
			673,
			7487,
			1495,
			7815,
			7347,
			1272,
			7434,
			5431,
			7564,
			6664,
			1647,
			1585,
			528,
			7569,
			6742,
			6683,
			6604,
			1448,
			7664,
			6657,
			514,
			844,
			7805,
			569,
			7400,
			1306,
			7803,
			6733,
			6637,
			1631,
			7658,
			1695,
			7353,
			566,
			1510,
			1391,
			1737,
			7209,
			816,
			7514,
			767,
			6672,
			7556,
			1354,
			1590,
			7786,
			1502,
			7784,
			874,
			6665,
			1716,
			1642,
			1422,
			7792,
			752,
			1532,
			861,
			675,
			1059,
			1374,
			7729,
			7704,
			7119
		];

		$tmp = [];
		foreach ($variable as $key => $value) {
			$db = JFactory::getDBO();
				$db->setQuery('SELECT u.email, u.name, u.id, up.point,
				case 
					when up.type = 1 THEN "Profile update"
					when up.type = 2 THEN "Friend referral"
					when up.type = 3 THEN "Article sharing"
					when up.type = 4 THEN "Birthday gift redemption"
					when up.type = 5 THEN "Event attendance"
					when up.type = 6 THEN "First-time sign up"
					when up.type = 7 THEN "Article download"
					when up.type = 8 THEN "Social media linking"
					when up.type = 9 THEN "Event attendance"
					when up.type = 10 THEN "Q-Perks reimbursement"
					when up.type = 11 THEN "Winning partnership sharing"
					when up.type = 12 THEN "Join Quake WhatsApp"
					when up.type = 13 THEN "Ebook sharing"
					when up.type = 14 THEN "Ebook download"
				end as actype
				from qkpe1_cus_qperks_user_point up
				join qkpe1_users u
				on u.id = up.user_id
				where up.created_on between "2020-05-01 00:00:00" and "2020-05-31 23:59:59" and u.id = '.$value);
				$result = $db->loadObjectList();
				$tmp[] = $result;
		}

		$spreadsheet = new Spreadsheet();
		$count=1;
			$sheet = $spreadsheet->getActiveSheet();
			$objWorkSheet = $spreadsheet->createSheet(0);

			foreach ($tmp as $key => $value) {
				foreach ($value as $key => $value1) {
					$objWorkSheet->setCellValue('A'.($count), $value1->name);
					$objWorkSheet->setCellValue('B'.($count), $value1->email);
					$objWorkSheet->setCellValue('C'.($count), $value1->actype);
					$objWorkSheet->setCellValue('D'.($count), $value1->point);
					$count++;
				}
				$count=$count+2;

			}
			$writer = new Xlsx($spreadsheet);
				
			// $writer->save('../uploads/'.$value.'.xlsx');
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="2020 May report.xlsx"');
			$writer->save("php://output");
			die();
	}
	
}
