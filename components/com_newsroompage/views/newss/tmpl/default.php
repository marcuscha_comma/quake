<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_programmeupdatespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<div id="app">
    <div class="tab-pills scrollable-tab">
      <div class="row">
        <div class="col"><a href="./whats-on" class="tab-pill">Programme Highlights</a></div>
        <div class="col"><a href="./whats-on/latest-updates" class="tab-pill <?php echo $this->catid==10?'active':'';?>">Latest Updates</a></div>
        <div class="col"><a href="./whats-on/press-room" class="tab-pill <?php echo $this->catid==13?'active':'';?>">Press Room</a></div>

      </div>
    </div>

    <div>
        <div class="d-none d-md-block">
          <div class="filter-options">
          <label class="form-label">Filter By</label>
            <select data-width="fit" class="selectpicker" v-model="selectedYear" @change="selectOnChanged">
                <option value="0">All Year</option>
                <option v-for="n in currentYear" v-if="n>=2017" :value="n">{{n}}</option>
            </select>
            <select data-width="fit" class="selectpicker" v-model="selectedMonth" @change="selectOnChanged">
                <option value="0">All Months</option>
                <option value="1">January</option>
                <option value="2">Febuary</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
          </div>
        </div>

        <div class="mobile-filter d-block d-sm-block d-md-none">
          <div id="filter-trigger" class="btn btn-grey btn-block">
            Filter By
          </div>

          <div class="mobile-filter-container">
            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Year</label>
              </div>
              <div class="col-7">
                <select class="selectpicker" v-model="selectedYear" @change="selectOnChanged" data-mobile="true">
                    <option value="0">All Year</option>
                    <option v-for="n in currentYear" v-if="n>=2016" :value="n">{{n}}</option>
                </select>
              </div>
            </div>

            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Months</label>
              </div>
              <div class="col-7">
                <select  class="selectpicker" v-model="selectedMonth" @change="selectOnChanged" data-mobile="true" >
                    <option value="0">Months</option>
                    <option value="1">January</option>
                    <option value="2">Febuary</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
              </div>
            </div>

            <div id="filter-close" class="btn btn-pink">
              Done
            </div>
          </div>
        </div>

        <div>
            <div class="row" v-show="showStatus">
                <div class="col-md-4 col-sm-6 article article-listing" v-for="(article, index) in articlesArray"  v-if="(index+1) <= loadMoreNumber">
                  <div class="hover-area pointer" @click="goToLink(article.url);">
                    <a>
                    <div class="pointer img-holder"
                      :style="{ 'background-image': 'url(../' + article.image_link + ')' }">
                      <img class="placeholder" src="images/card-guide.png" alt="">
                      <div class="overlayer">

                      </div>
                    </div>
                    <div class="article-date">{{article.convDate}}</div>
                    <div class="article-title pointer">{{article.title}}</div>
                    <div class="article-link" >Read More</div>
                    </a>
                  </div>
                </div>
            </div>
            <div v-show="!showStatus" style="text-align:center;">
                <h5 class="section-desc text-muted">No results have been found.</h5>
            </div>
            <div class="loading-bar" v-if="loadMoreNumber < articlesArray.length">
              <div @click="loadMore(loadMoreNumber)">Load More</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var articlesObjFromPhp = <?php echo json_encode($this->articles); ?>;

    var app = new Vue({
        el: '#app',
        data: {
            articlesArray: articlesObjFromPhp,
            // selectedYear: new Date().getFullYear(),
            selectedYear: 0,
            selectedMonth: 0,
            currentYear: new Date().getFullYear(),
            loadMoreNumber: 6,
            showStatus : true,
        },
        mounted: function () {
            this.selectOnChanged();
        },
        updated: function () {
            jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
            selectOnChanged :function(){
                // this.selectedChannel = "";
                _this = this;
                _this.articlesArray = articlesObjFromPhp;

                if (_this.selectedYear != 0) {
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.year == _this.selectedYear;
                    });
                }
                if (_this.selectedMonth != 0) {
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.month == _this.selectedMonth;
                    });
                }
                if (_this.articlesArray.length > 0) {
                    _this.showStatus = true;
                }else{
                    // if (_this.selectedYear == new Date().getFullYear()) {
                    //     _this.selectedYear = _this.selectedYear-1;
                    //     _this.selectOnChanged();
                    // }else{
                        _this.showStatus = false;
                    // }
                }
            },
            loadMore :function(loadMoreNumber){
                this.loadMoreNumber =loadMoreNumber + 6;
            },
            goToLink :function(link){
                if (link == "/latest-update/230-cny-8-huat-facts") {
                    window.location.href = '/cny-8-huat-facts';
                }else if(link == "/latest-update/465-a-raya-aidilfitri-guide-for-marketers"){
                  window.open(
                    '/raya-guide-for-marketers',
                    '_blank'
                  );
                }else if(link == "/latest-update/551-an-oxpicious-guide-for-marketers"){
                  window.open(
                    '/moomooda2021',
                    '_blank'
                  );
                }else if(link == "/latest-update/579-mmdcny2021-together-registration"){
                  window.open(
                    '/events/rsvp/mmd2021',
                    '_blank'
                  );
                }else{
                    window.location.href = link;
                }                 
            }
        }
    })
</script>
