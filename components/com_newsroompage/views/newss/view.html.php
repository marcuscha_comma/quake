<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Newsroompage
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
if(!class_exists('ContentHelperRoute')) require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

jimport('joomla.application.component.view');

/**
 * View class for a list of Newsroompage.
 *
 * @since  1.6
 */
class NewsroompageViewNewss extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();
		$menu = $app->getMenu();

		switch ($menu->getActive()->alias) {
			case 'press-room':
				$this->catid = 13;
				break;
			
			default:
				$this->catid = 10;
				break;
		}
		
		// get list of article
		$db_article    = JFactory::getDBO();
		$query_article = $db_article->getQuery( true );
		$query_article
			->select( 'c.*, DATE_FORMAT(c.publish_up,"%M %d,%Y") as convDate, fv3.value as image_link, MONTH(c.publish_up) as month, YEAR(c.publish_up) as year' )
			->from( $db_article->quoteName( '#__content', 'c') )
			->join('INNER', $db_article->quoteName('#__fields_values', 'fv3') . ' ON (' . $db_article->quoteName('fv3.item_id') . ' = ' . $db_article->quoteName('c.id') . ') and fv3.field_id = 5')
			->where($db_article->quoteName('c.catid')." = ".$this->catid)
			->where($db_article->quoteName('c.state')." > 0")
			->where($db_article->quoteName('c.publish_up')." < '" .JHtml::date('now', 'Y-m-d H:i:s')."'")
			->order( 'publish_up desc' );
		$db_article->setQuery( $query_article );
		$articles = $db_article->loadObjectList();

		foreach ($articles as $key => $articlevalue) {
			
			$articlevalue->url = JRoute::_(ContentHelperRoute::getArticleRoute(  $articlevalue->id,  $articlevalue->catid ));
		
		}

		$this->state = $this->get('State');
		$this->params = $app->getParams('com_newsroompage');
		

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		$this->_prepareDocument();

		$this->articles = $articles;
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_NEWSROOMPAGE_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
