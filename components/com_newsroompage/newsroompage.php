<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Newsroompage
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Newsroompage', JPATH_COMPONENT);
JLoader::register('NewsroompageController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Newsroompage');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
