<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_buka_lembaran_baharu
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Rsvp_buka_lembaran_baharu', JPATH_COMPONENT);
JLoader::register('Rsvp_buka_lembaran_baharuController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Rsvp_buka_lembaran_baharu');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
