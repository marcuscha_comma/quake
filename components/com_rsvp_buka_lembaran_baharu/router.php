<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_buka_lembaran_baharu
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class Rsvp_buka_lembaran_baharuRouter
 *
 */
class Rsvp_buka_lembaran_baharuRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = JComponentHelper::getComponent('com_rsvp_buka_lembaran_baharu')->params;
		$this->noIDs = (bool) $params->get('sef_ids');
		
		

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('Rsvp_buka_lembaran_baharuRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('Rsvp_buka_lembaran_baharuHelpersRsvp_buka_lembaran_baharu', __DIR__ . '/helpers/rsvp_buka_lembaran_baharu.php');
			$this->attachRule(new Rsvp_buka_lembaran_baharuRulesLegacy($this));
		}
	}


	

	
}
