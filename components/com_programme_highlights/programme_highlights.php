<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Programme_highlights
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Programme_highlights', JPATH_COMPONENT);
JLoader::register('Programme_highlightsController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Programme_highlights');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
