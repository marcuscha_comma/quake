<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Programme_highlights
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Programmehighlights list controller class.
 *
 * @since  1.6
 */
class Programme_highlightsControllerProgrammehighlights extends Programme_highlightsController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Programmehighlights', $prefix = 'Programme_highlightsModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
