<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_programmeupdatespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$app = JFactory::getApplication();   // equivalent of $app = JFactory::getApplication();
$input = $app->input;

if ($input->exists('s'))
{
  $segment_params = $input->get('s', 0, "INT");
}else{
  $segment_params = "";
}
?>
<div id="app">
    <div class="tab-pills scrollable-tab">
      <div class="row">
        <div class="col"><a href="./whats-on" class="tab-pill active">Programme Highlights</a></div>
        <div class="col"><a href="./whats-on/latest-updates" class="tab-pill">Latest Updates</a></div>
        <div class="col"><a href="./whats-on/press-room" class="tab-pill">Press Room</a></div>
      </div>
    </div>

    <div>
      <div>
          <h5 class="section-desc">Take a look at our new programmes that are open for brand partnerships</h5>
      </div>
        <div class="d-none d-md-block">

          <div class="filter-options">
            <label class="form-label">Filter By</label>
            <select class="selectpicker" v-model="selectedSegment" @change="selectOnChanged();selectedChannel=''" data-width="fit">
                <option value="">All Segments</option>
                <option v-for="segment in segmentsArray" :value="segment.id">
                    {{ segment.title }}
                </option>
            </select>
            <select  class="selectpicker" v-model="selectedChannel" @change="selectOnChanged" data-width="fit">
                <option value="">All Channels</option>
                <option v-for="channel in channelsArray" v-show="channel.segment == selectedSegment || selectedSegment ==''" :value="channel.id">
                    {{ channel.title }}
                </option>
            </select>
            <select class="selectpicker" v-model="selectedYear" @change="selectOnChanged" data-width="fit">
                <option value="0">All Year</option>
                <option v-for="n in currentYear" v-if="n>=2016" :value="n">{{n}}</option>
            </select>
            <select  class="selectpicker" v-model="selectedMonth" @change="selectOnChanged" data-width="fit">
                <option value="0">All Months</option>
                <option value="1">January</option>
                <option value="2">Febuary</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
          </div>
        </div>

        <div class="mobile-filter d-block d-sm-block d-md-none">
          <div id="filter-trigger" class="btn btn-grey btn-block">
            Filter By
          </div>

          <div class="mobile-filter-container">
            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Segments</label>
              </div>
              <div class="col-7">
                <select class="selectpicker" v-model="selectedSegment" @change="selectOnChanged();selectedChannel=''" data-mobile="true">
                    <option value="">All Segments</option>
                    <option v-for="segment in segmentsArray" :value="segment.id">
                        {{ segment.title }}
                    </option>
                </select>
              </div>
            </div>

            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Channels</label>
              </div>
              <div class="col-7">
                <select  class="selectpicker" v-model="selectedChannel" @change="selectOnChanged" data-mobile="true">
                    <option value="">All Channels</option>
                    <option v-for="channel in channelsArray" v-show="channel.segment == selectedSegment || selectedSegment ==''" :value="channel.id">
                        {{ channel.title }}
                    </option>
                </select>
              </div>
            </div>

            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Year</label>
              </div>
              <div class="col-7">
                <select class="selectpicker" v-model="selectedYear" @change="selectOnChanged" data-mobile="true">
                    <option value="0">All Year</option>
                    <option v-for="n in currentYear" v-if="n>=2016" :value="n">{{n}}</option>
                </select>
              </div>
            </div>

            <div class="row border-bottom">
              <div class="col-5">
                <label class="form-label">Months</label>
              </div>
              <div class="col-7">
                <select  class="selectpicker" v-model="selectedMonth" @change="selectOnChanged" data-mobile="true">
                    <option value="0">All Months</option>
                    <option value="1">January</option>
                    <option value="2">Febuary</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
              </div>
            </div>

            <div id="filter-close" class="btn btn-pink">
              Done
            </div>
          </div>
        </div>

        <div>
            <div class="row" v-show="showStatus">
                <div class="col-md-4 col-sm-6 article article-listing"  v-for="(article, index) in articlesArray"  v-if="(index+1) <= loadMoreNumber">
                    <div class="hover-area pointer" @click="goToLink(article.url);">
                      <a :href="article.url">
                        <div class="pointer img-holder" :style="{ 'background-image': 'url(./' + article.image_link + ')' }">
                        <img class="placeholder" src="images/2020/card-guide.png" alt="">
                        <div class="overlayer">

                        </div>
                        </div>
                        <div class="article-date">{{article.convDate}}</div>
                        <div @click="goToLink(article.url);" class="article-title pointer">{{article.title}}</div>
                        <div class="article-link" @click="goToLink(article.url);">Read More</div>
                        </a>
                    </div>
                </div>
            </div>
            <div v-show="!showStatus" style="text-align:center;">
                <h5 class="section-desc text-muted">No results have been found.</h5>
            </div>
            <div class="loading-bar" v-if="loadMoreNumber < articlesArray.length">
              <div @click="loadMore(loadMoreNumber)">Load More</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var segmentsObjFromPhp = <?php echo json_encode($this->segments); ?>;
    var channelsObjFromPhp = <?php echo json_encode($this->channels); ?>;
    var articlesObjFromPhp = <?php echo json_encode($this->articles); ?>;
    var segmentParamsObjFromPhp = <?php echo json_encode($segment_params); ?>;
    

    var app = new Vue({
        el: '#app',
        data: {
            segmentsArray: segmentsObjFromPhp,
            channelsArray: channelsObjFromPhp,
            articlesArray: articlesObjFromPhp,
            selectedSegment: segmentParamsObjFromPhp,
            selectedChannel: "",
            selectedYear: 0,
            selectedMonth: 0,
            currentYear: new Date().getFullYear(),
            loadMoreNumber: 6,
            showStatus : true,
        },
        mounted: function () {
            this.loadByDefault();
            this.selectOnChanged();
        },
        updated: function() {
            var JS =jQuery.noConflict();
            JS(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
            loadByDefault: function() {
                _this = this;
                _this.articlesArray = articlesObjFromPhp;
            },
            selectOnChanged :function(){
                // this.selectedChannel = "";
                _this = this;
                _this.articlesArray = articlesObjFromPhp;

                if (this.selectedSegment != "") {
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.segment.filter(function(segment){

                            return segment == _this.selectedSegment;
                        }).length > 0;
                        
                    });
                }
                if (this.selectedChannel != "") {
                    _this.articlesArray = _this.articlesArray.filter(function(article){

                        return article.channel.filter(function(channel){

                            return channel == _this.selectedChannel;
                        }).length > 0;
                        // console.log(asd);
                    });
                }
                if(this.selectedYear != 0) {
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.year == _this.selectedYear;
                    });
                }
                if(this.selectedMonth != 0) {
                    _this.articlesArray = _this.articlesArray.filter(function(article){
                        return article.month == _this.selectedMonth;
                    });
                }

                if (_this.articlesArray.length > 0) {
                    _this.showStatus = true;
                }else{
                    _this.showStatus = false;
                }

                // console.log(this.selectedSegment,this.selectedChannel,this.selectedYear,this.selectedMonth);
            },
            loadMore :function(loadMoreNumber){
                this.loadMoreNumber = loadMoreNumber + 6;
            },
            goToLink :function(link){
                window.location.href = link;
            }
        }
    })
</script>
