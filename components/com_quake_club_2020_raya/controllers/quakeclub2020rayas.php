<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_2020_raya
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Quakeclub2020rayas list controller class.
 *
 * @since  1.6
 */
class Quake_club_2020_rayaControllerQuakeclub2020rayas extends Quake_club_2020_rayaController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Quakeclub2020rayas', $prefix = 'Quake_club_2020_rayaModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
