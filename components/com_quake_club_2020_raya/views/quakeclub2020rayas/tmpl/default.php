<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_2020_raya
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;
$user       = JFactory::getUser();
$userId     = $user->get('id');
$updateUserShareArticlePointUrl = JRoute::_('index.php?option=com_quake_club_qperks&task=qperksuserpoint.addPoint');



// HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
// HTMLHelper::_('bootstrap.tooltip');
// HTMLHelper::_('behavior.multiselect');
// HTMLHelper::_('formbehavior.chosen', 'select');

// $user       = Factory::getUser();
// $userId     = $user->get('id');
// $listOrder  = $this->state->get('list.ordering');
// $listDirn   = $this->state->get('list.direction');
// $canCreate  = $user->authorise('core.create', 'com_quake_club_2020_raya') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'quakeclub2020rayaform.xml');
// $canEdit    = $user->authorise('core.edit', 'com_quake_club_2020_raya') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'quakeclub2020rayaform.xml');
// $canCheckin = $user->authorise('core.manage', 'com_quake_club_2020_raya');
// $canChange  = $user->authorise('core.edit.state', 'com_quake_club_2020_raya');
// $canDelete  = $user->authorise('core.delete', 'com_quake_club_2020_raya');

// Import CSS
$document = Factory::getDocument();
// $document->addStyleSheet(Uri::root() . 'media/com_quake_club_2020_raya/css/list.css');
$document->addStyleSheet(Uri::root() . 'components/com_quake_club_2020_raya/views/quakeclub2020rayas/tmpl/css/style.css?v=1.01');
$document->addStyleSheet(Uri::root() . 'templates/bootstrap4/css/modal.css?v=0620');

?>

<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bcd3eb6f160f08f"></script>

<style>
    /* style the elements with CSS */
    #pleaserotate-graphic{
        fill: #fff;
    }

    #pleaserotate-backdrop {
        color: #fff;
        background-color: #000;
	}
	/* body{
		overflow:hidden;
	} */

#flipbook{
    width:400px;
    height:300px;
}

#flipbook .page{
    width:400px;
    height:300px;
    background-color:white;
    line-height:300px;
    font-size:20px;
    text-align:center;
}

#flipbook .page-wrapper{
    -webkit-perspective:2000px;
    -moz-perspective:2000px;
    -ms-perspective:2000px;
    -o-perspective:2000px;
    perspective:2000px;
}

#flipbook .hard{
    background:#ccc !important;
    color:#333;
    -webkit-box-shadow:inset 0 0 5px #666;
    -moz-box-shadow:inset 0 0 5px #666;
    -o-box-shadow:inset 0 0 5px #666;
    -ms-box-shadow:inset 0 0 5px #666;
    box-shadow:inset 0 0 5px #666;
    font-weight:bold;
}

#flipbook .odd{
    background:-webkit-gradient(linear, right top, left top, color-stop(0.95, #FFF), color-stop(1, #DADADA));
    background-image:-webkit-linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    background-image:-moz-linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    background-image:-ms-linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    background-image:-o-linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    background-image:linear-gradient(right, #FFF 95%, #C4C4C4 100%);
    -webkit-box-shadow:inset 0 0 5px #666;
    -moz-box-shadow:inset 0 0 5px #666;
    -o-box-shadow:inset 0 0 5px #666;
    -ms-box-shadow:inset 0 0 5px #666;
    box-shadow:inset 0 0 5px #666;

}

#flipbook .even{
    background:-webkit-gradient(linear, left top, right top, color-stop(0.95, #fff), color-stop(1, #dadada));
    background-image:-webkit-linear-gradient(left, #fff 95%, #dadada 100%);
    background-image:-moz-linear-gradient(left, #fff 95%, #dadada 100%);
    background-image:-ms-linear-gradient(left, #fff 95%, #dadada 100%);
    background-image:-o-linear-gradient(left, #fff 95%, #dadada 100%);
    background-image:linear-gradient(left, #fff 95%, #dadada 100%);
    -webkit-box-shadow:inset 0 0 5px #666;
    -moz-box-shadow:inset 0 0 5px #666;
    -o-box-shadow:inset 0 0 5px #666;
    -ms-box-shadow:inset 0 0 5px #666;
    box-shadow:inset 0 0 5px #666;
}

.btn-submit {
  font-size: 12px;
}

.btn-submit i {
  margin-right: 6px;
}

#nav-left, #nav-right {
  cursor: pointer;
  height: 100%;
}

#nav-left i, #nav-right i {
  -webkit-transition: all 0.3s ease-out;
  -moz-transition: all 0.3s ease-out;
  -o-transition: all 0.3s ease-out;
  transition: all 0.3s ease-out;
  margin-top: 100px;
  margin-bottom: 100px;
}

#nav-left:hover i {
  margin-right: 10px;
}
#nav-right:hover i {
  margin-left: 10px;
}

#overlay-div{
		width: 100%;
		position: absolute;
		top: 0;
		bottom: 0;
		z-index: 10;
	}

</style>
<div id="app">


<div style="display: none;">
	<audio controls id="pageFlip" preload="auto">
		<source src="includes/sound/page-flip.mp3" type="audio/mpeg">
		<source src="includes/sound/page-flip.wav" type="audio/wav">
		<source src="includes/sound/android/page-flip.ogg" type="audio/ogg">
	  	<source src="includes/sound/ios/page-flip.m4a" type="audio/mp4">
	</audio>
	<audio controls id="bgm" preload="auto" loop>
		<source src="includes/sound/bgm.mp3" type="audio/mpeg">
		<source src="includes/sound/android/bgm.ogg" type="audio/ogg">
	  	<source src="includes/sound/ios/bgm.m4a" type="audio/mp4">
	</audio>
</div>

<div class="container-fluid mt-5">
<div class="row justify-content-center">
	<div class="col-md-2 col-2 logo-wrapper" >
	<a href="/login">
		<img id="logo-click" src="./images/astro-media-solutions.png" alt="" style="margin-bottom: 15px;">
	</a>
	</div>
  <div class="col-md-8 col-8 text-right button-wrapper">
  	<div class="d-inline-block mt-2 mr-2 share-button-container">
    	<span class="share-title">Share: </span>
		<div class="addthis_inline_share_toolbox"></div>
		<div v-if="loginStatus == '0'" id="overlay-div" data-toggle="modal" data-target="#shareCenterModal"></div>
  	</div>

  	<div class="d-inline-block mt-2 mr-2">
			<?php if($userId>0){ ?>
				<button class="btn btn-sm btn-submit" @click="downloadFile">
        	<i class="fas fa-arrow-down" style="cursor:pointer;"></i>Download
				</button>
			<?php }else{ ?>
				<button data-toggle="modal" data-target="#downloadCenterModal" class="btn btn-sm btn-submit">
        <i class="fas fa-arrow-down" style="cursor:pointer;"></i>Download
			</button>
			<?php } ?>
	  <div class="btn btn-sm btn-submit" id="close-click">
        <i class="fas fa-window-close" style="cursor:pointer;"></i> Close
      </div>
    </div>

    <div id="expand-click" class="d-inline-block mt-2 mr-2">
      <div class="btn btn-sm btn-submit">
        <i class="fas fa-expand" style="cursor:pointer;"></i> Fullscreen
      </div>
    </div>

    <div class="d-inline-block mt-2 mr-2">
  	  <div class="btn btn-sm btn-submit mute-btn-wrapper">
		<a href="javascript:toggleMute();" class="mute-btn">mute</a>
	  </div>
	</div>
  </div>
</div>
<div class="row align-items-center" id="middle-content-height">

	<div class="col-1 text-right " id="nav-left" onclick='flipAction("previous");'>
		<i class="fas fa-chevron-left"></i>
	</div>

	<div class="col-10" id="middle-content">
		<div id="flipbook" class="flipbook">
			<!-- <div class="hard"> Turn.js </div> -->
			<!-- <div class="hard"></div> -->
			<div class="cny-box-img-1 cny-box"></div>
			<div class="cny-box-img-2 cny-box"></div>
			<div class="cny-box-img-3 cny-box"></div>
			<div class="cny-box-img-4 cny-box"></div>
			<div class="cny-box-img-5 cny-box"></div>
			<div class="cny-box-img-6 cny-box"></div>
			<div class="cny-box-img-7 cny-box"></div>
			<div class="cny-box-img-8 cny-box"></div>
			<div class="cny-box-img-9 cny-box"></div>
			<div class="cny-box-img-10 cny-box"></div>
			<div class="cny-box-img-11 cny-box"></div>
			<div class="cny-box-img-12 cny-box"></div>
			<div class="cny-box-img-13 cny-box"></div>
			<div class="cny-box-img-14 cny-box"></div>
			<div class="cny-box-img-15 cny-box"></div>

			<!-- <div class="hard"></div>
			<div class="hard"></div> -->
		</div>
	</div>

	<div class="col-1" id="nav-right" onclick='flipAction("next");'>
		<i class="fas fa-chevron-right"></i>
	</div>

	<div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" hidden="true"></div>

			<div class="modal-body">
				<div>
					<button type="button" class="close font-brand" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">Close</span>
					</button>

					<div style="clear: both;"></div>
				</div>
				<div class="text-center">
					<h3 class="text-white">Login to Download</h3>
					<p class="text-white mb-4">Please login or signup download this eBook.</p>
          			<a href="./login" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
					<!-- <a href="#" data-dismiss="modal" class="btn btn-white">Skip</a> -->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade custom-modal" ref="vuemodal" id="shareCenterModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" hidden="true"></div>

			<div class="modal-body">
				<div>
					<button @click="closeModal" type="button" class="close font-brand" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">Close</span>
					</button>

					<div style="clear: both;"></div>
				</div>
				<div class="text-center">
					<h3 class="text-white">Login in to earn points</h3>
					<p class="text-white mb-4">Hey, we know you're excited to redeem exclusive merchandise! Log in now or sign up to earn Q-Perks when you share this eBook.</p>
					<a href="#" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
					<a href="#" data-dismiss="modal" class="btn btn-white">Skip</a>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
</div>
<!--
<img class="cloud cloud-left img-fluid" src="images/rsvp/footer-left.png" alt="">
<img class="cloud cloud-right img-fluid" src="images/rsvp/footer-right.png" alt="">
-->
<script type="text/javascript">
var updateUserShareArticlePointUrl = <?php echo json_encode($updateUserShareArticlePointUrl); ?>;
var userId = <?php echo json_encode($userId); ?>;

jQuery(document).ready(function() {
    // console.log('document ready!');
    var bgmPlaying = false;
    var flippedOnce = false;
    var allowCache = true;
    if(allowCache){
    	if(localStorage.hasOwnProperty('cny-mute')){
	        var isMute = JSON.parse(localStorage.getItem('cny-mute'));
	    } else {
	        // is not mute by default
	        isMute = false;
	        localStorage.setItem('cny-mute', isMute);
	    };
    };
    isMute = false;
    // console.log('is mute: ' + isMute);

    // mute button status
    if(isMute){
        jQuery('.mute-btn').addClass('mute');
    } else {
        jQuery('.mute-btn').removeClass('mute');
    };

	window.isMobile = !!navigator.userAgent.match(/iPad|iPhone|iPod|Opera Mini|IEMobile|Android|BlackBerry|Windows Phone|webOS/i);
    window.iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    window.iphone_x = false;
    window.android = (navigator.userAgent.match(/Android/i) ? true : false);

    // audio object properties reference : https://www.w3schools.com/jsref/dom_obj_audio.asp
    if(isMobile){
        jQuery('body').addClass('isMobile');
        if(window.iOS){
            jQuery('body').addClass('is_iOS');
            // var flipSFX = new Audio('includes/sound/android/page-flip.m4a');
            // var thisBGM = new Audio('includes/sound/android/bgm.m4a');
        } else {
        	// flipSFX = new Audio('includes/sound/android/page-flip.ogg');
        	// thisBGM = new Audio('includes/sound/android/bgm.ogg');
        };
    } else {
        jQuery('body').addClass('isDesktop');
        // flipSFX = new Audio('includes/sound/android/page-flip.ogg');
        // thisBGM = new Audio('includes/sound/android/bgm.ogg');
    };
    var flipSFX = jQuery('#pageFlip')[0];
    var thisBGM = jQuery('#bgm')[0];
    // bgm too loud, reduce the volume
    thisBGM.volume = 0.1;
    // looping
    thisBGM.loop = true;
    // thisBGM.autoplay = true;
    // thisBGM.play();
    // console.log(thisBGM.paused);
/*
    console.log("is iOS: " + iOS + ", is android: " + android);
    console.log(flipSFX);
    console.log(thisBGM);
*/
	var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
	if (iOS) {
		$( "#expand-click div" ).hide();
		$('#close-click').hide();
	}
	$('#close-click').hide();

	window.onorientationchange = function() {
        var orientation = window.orientation;
            switch(orientation) {
                case 0:
                case 90:
                case -90: window.location.reload();
                break; }
    };
/*
	document.body.addEventListener('touchmove', function(e) {
		e.preventDefault();
		// e.stopPropagation();
	});
*/

	window.playBGM = function(play){
		if(play){
			// play the music
			if(!bgmPlaying){
				bgmPlaying = true;
				thisBGM.currentTime = 0;
				thisBGM.play();
			};
		} else {
			// stop the music
			if(bgmPlaying){
            	bgmPlaying = false;
            	thisBGM.pause();
            };
		};
	};
	// force all buttons to play bgm
	$('.btn.btn-submit').click(function(){
		playBGM(true);
	});
	$('.at-share-btn').click(function(){
		playBGM(true);
	});
	$('#overlay-div').click(function(){
		playBGM(true);
	});

	window.flipAction = function(direction){
		// console.log('flip to direction: ' + direction);
		var currentPage = $("#flipbook").turn("page");
		$("#flipbook").turn(direction);
		var nextPage = $("#flipbook").turn("page");
		// console.log('current page: ' + currentPage + ', next page: ' + nextPage);

		if(iOS){
			if(currentPage != nextPage){
				if(!isMute){
					flipSFX.currentTime = 0;
					flipSFX.play();

					playBGM(true);
				};

				if(!flippedOnce){
					// user already flipped the book once, during unmute the bgm will auto start playing
					flippedOnce = true;
				};
			};
		};
	};

	window.toggleMute = function(){
        // console.log('toggle isMute: ' + isMute);
        isMute = !isMute;
        // console.log('toggled: ' + isMute);

        // button state
        if(isMute){
            jQuery('.mute-btn').addClass('mute');

            playBGM(false);
        } else {
            jQuery('.mute-btn').removeClass('mute');

            // if(flippedOnce){
            	playBGM(true);
            // };
        };

        // save settings
        if(allowCache) localStorage.setItem('cny-mute', isMute);
        // console.log('mute settings saved');
        // console.log(localStorage.getItem('cny-mute'));
    };

	function loadApp() {
		var size = getSize();

		// Create the flipbook
		$('.flipbook').turn({
			// Width
			width: size.width,
			// Height
			height: size.height,
			// Elevation
			elevation: 50,
			// Enable gradients
			gradients: true,
			// Auto center this flipbook
			autoCenter: true,
			duration : 2000,
		});
		$('.flipbook').turn('display', 'single');

		$("#flipbook").bind("turning", function(event, page, view) {
			// console.log("Turning the page to: "+page);
			if(!isMute){
				flipSFX.currentTime = 0;
				flipSFX.play();

				playBGM(true);
			};

			if(!flippedOnce){
				// user already flipped the book once, during unmute the bgm will auto start playing
				flippedOnce = true;
			};
		});
	}

	function getSize() {
		// var width = document.body.clientWidth;
		// var height = document.body.clientHeight;
		var width = (window.innerWidth > 0) ? $("#middle-content").width() : screen.width;
		var height = (window.innerHeight > 0) ? '40vw' : '40vw';
		return {
			width: width,
			height: height
		}
	}

	function resize() {
		// console.log('resize event triggered');
		var size = getSize();
		// console.log(size);

		if (size.width > size.height) { // landscape
			$('.flipbook').turn('display', 'single');
		}
		else {
			$('.flipbook').turn('display', 'single');
		}

		$('.flipbook').turn('size', size.width, size.height);
	}

	// Load App
	loadApp();

	var scrollPosition = [
		self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
		self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
	];

	// var download_button = document.getElementById("download-click");

	// download_button.onclick = function(){
	// 	// location.href = "../../../../../uploads/cny-dl-files/ASTRO CNY Story (8 facts).pdf";
		
	// }

	var elem = document.getElementById("expand-click");

	elem.onclick = function() {
		$('#expand-click div').hide();
		$('#logo-click').hide();
		$('#download-click').hide();
		var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
		if (iOS) {
			$('#close-click').hide();
		}else{
			$('#close-click').show();
		}
		
		if (!document.fullscreenElement &&    // alternative standard method
			!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
			if (document.documentElement.requestFullscreen) {
				document.documentElement.requestFullscreen();
			} else if (document.documentElement.msRequestFullscreen) {
				document.documentElement.msRequestFullscreen();
			} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
			} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		} else {
			if (document.exitFullscreen) {
				document.exitFullscreen();
			} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
			}
		}
		screen.orientation.lock("landscape");
	}

	var elem2 = document.getElementById("close-click");

	elem2.onclick = function() {
		$('#expand-click div').hide();
		$('#logo-click').show();
		$('#download-click').hide();
		var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
		if (iOS) {
			$('#close-click').hide();
		}else{
			$('#close-click').show();
		}
		if (!document.fullscreenElement &&    // alternative standard method
			!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
			if (document.documentElement.requestFullscreen) {
			document.documentElement.requestFullscreen();
			} else if (document.documentElement.msRequestFullscreen) {
			document.documentElement.msRequestFullscreen();
			} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
			} else if (document.documentElement.webkitRequestFullscreen) {
			document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		} else {
			if (document.exitFullscreen) {
			document.exitFullscreen();
			} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
			} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
			}
		}
		screen.orientation.lock("landscape-primary");
	}

	document.addEventListener('fullscreenchange', exitHandler);
	document.addEventListener('webkitfullscreenchange', exitHandler);
	document.addEventListener('mozfullscreenchange', exitHandler);
	document.addEventListener('MSFullscreenChange', exitHandler);
	function exitHandler() {
		if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
			$('#expand-click div').show();
			$('#download-click').show();
			$('#logo-click').show();
			$('#close-click').hide();
			var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
			if (iOS) {
				$('#close-click').hide();
			}else{
				$('#close-click').hide();
			}
		}
	}
});

var app = new Vue({
        el: '#app',
        data: {
					loginStatus : 0
        },
        mounted: function () {
					$(this.$refs.vuemodal).on("hidden.bs.modal", this.closeModal);
					if (userId>0) {
						this.loginStatus = 1;
					}
        },
        updated: function () {
			
        },
        methods: {
					goToLink :function(){
										window.location.href = './login';
								},
					closeModal :function(){
						this.loginStatus = 1
					},
					downloadFile : function(){
						window.open("./uploads/raya-dl-files/a-raya-guide-for-marketers.pdf");

						jQuery.ajax({
							url: updateUserShareArticlePointUrl,
							type: 'post',
							data: {'source':'download-senyuman-raya-2020','shareOrDownloadStatus':'downloadEbook'},
							success: function (result) {
								console.log("success");

							},
							error: function () {
								console.log('fail');
							}
						});
					}
        }
    })
</script>
