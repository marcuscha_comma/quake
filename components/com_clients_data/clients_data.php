<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Clients_data
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Clients_data', JPATH_COMPONENT);
JLoader::register('Clients_dataController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Clients_data');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
