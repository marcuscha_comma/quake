<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_redemption
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Quakeclubredemptions list controller class.
 *
 * @since  1.6
 */
class Quake_club_redemptionControllerQuakeclubredemptions extends Quake_club_redemptionController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Quakeclubredemptions', $prefix = 'Quake_club_redemptionModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
