<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_redemption
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

/**
 * View class for a list of Quake_club_redemption.
 *
 * @since  1.6
 */
class Quake_club_redemptionViewQuakeclubredemptions extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app = Factory::getApplication();
		$user       = JFactory::getUser();
		$userId     = $user->get('id');
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->params = $app->getParams('com_quake_club_redemption');
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT r.*, date(r.created_on) as created_on, p.name as product_name, p.image1
		FROM #__cus_quake_club_redemption r 
		join #__cus_qperks_products p on (p.id = r.product_id and r.event_type is null)
		WHERE r.state > 0 and r.user_id ='.$userId.' ORDER BY id desc');
		$this->filtered_items = $db->loadObjectList();

		foreach ($this->filtered_items as $key => $value) {
			$value->status_label = JText::_('COM_QUAKE_CLUB_REDEMPTION_QUAKECLUBREDEMPTIONS_STATUS_OPTION_'.$value->status);
		}

		$db    = JFactory::getDBO();
		$db->setQuery('SELECT r.*, date(r.created_on) as created_on, p.name as product_name, p.image1
		FROM #__cus_quake_club_redemption r 
		join #__cus_qperks_event_products p on (p.id = r.product_id and r.event_type is not null)
		WHERE r.state > 0 and r.user_id ='.$userId.' ORDER BY id desc');
		$this->filtered_items_event = $db->loadObjectList();

		foreach ($this->filtered_items_event as $key => $value) {
			$value->status_label = JText::_('COM_QUAKE_CLUB_REDEMPTION_QUAKECLUBREDEMPTIONS_STATUS_OPTION_'.$value->status);
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		$this->_prepareDocument();
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = Factory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', Text::_('COM_QUAKE_CLUB_REDEMPTION_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = Text::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = Text::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
