<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_redemption
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
?>
<div id="app">
  <div class="pink-page-title">
     <div class="container">
       <h2>Redemption List</h2>
     </div>
  </div>

  <div  v-show="showStatus">
    <h2 class="f-20 mb-3 mt-4">All activities</h2>
    <div class="row" >
        <div class="col-lg-6 cart-item"  v-for="(item, index) in items"
					v-if="(index+1) <= loadMoreNumber">
        <!-- <div class="col-lg-6 cart-item" v-for="item in itemsArray"> -->
          <div class="row">
            <div class="col-4">
							<div class="product-image" :style="{ 'background-image': 'url(../' + item.image1 + ')' }"></div>
						</div>
						<div class="col">
              <div class="text-muted f-14 mb-2">{{item.created_on}}</div>

							<h3 class="product-name">{{item.quantity}}x {{item.product_name}}</h3>
							<div class="price mt-3">
                <h3 v-if="item.promo == 1" class="product-points">-{{item.quantity * item.promo_qperks}} Q-Perks</h3>
                <h3 class="product-points" :class="{'ori-price': item.promo == 1 }">-{{parseInt(item.quantity) * parseInt(item.point)}} Q-Perks</h3>
              </div>

              <label class="redeem-status" :class="{'rejected': item.status == 0 , 'pending': item.status == 1, 'confirmed': item.status == 2 }">{{item.status_label}}</label>
              <!-- 0 = rejected 1 = pending 2 = confirmed -->

						</div>
          </div>
        </div>
    </div>
  </div>
  <div class="loading-bar" v-if="loadMoreNumber < itemsArray.length">
    <div @click="loadMore(loadMoreNumber)">Load more</div>
  </div>

  <div v-show="!showStatus" class="text-center no-item">
    <h3 class="f-20 mb-4 mt-2">You have not made any redemption yet</h3>
    <p class="font-brand">Browse the rewards catalogue and start redeeming now.</p>
    <a href="./quake-club/rewards-catalogue" class="btn btn-pink btn-wide mt-4">View catalogue</a>
  </div>

</div>

<script type="text/javascript">
    var itemsObjFromPhp = <?php echo json_encode($this->filtered_items); ?>;
    var itemsEventObjFromPhp = <?php echo json_encode($this->filtered_items_event); ?>;

    var app = new Vue({
        el: '#app',
        data: {
            itemsArray: itemsObjFromPhp,
            itemsArrayEvent: itemsEventObjFromPhp,
            items : [],
            loadMoreNumber: 10,
            showStatus : true,
        },
        mounted: function () {
          this.selectOnChanged () ;
        },
        updated: function () {
            jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
          selectOnChanged :function(){
              _this = this;
              _this.itemsArray = itemsObjFromPhp;
              _this.itemsArrayEvent = itemsEventObjFromPhp;

              _this.items = _this.itemsArray.concat(_this.itemsArrayEvent);

              _this.items = _this.items.sort(function(a,b){ 
                return b.ordering - a.ordering;
              });

              if (_this.itemsArray.length > 0) {
                          _this.showStatus = true;
                      }else{
                          _this.showStatus = false;
                      }
                      // return this.itemsArray.sort((a, b) => new Date(b.created_on) - new Date(a.created_on))
            },

            loadMore :function(loadMoreNumber){
                this.loadMoreNumber =loadMoreNumber + 10;
            }
        }
    })
</script>
