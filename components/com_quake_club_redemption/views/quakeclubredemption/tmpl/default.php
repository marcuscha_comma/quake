<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_redemption
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REDEMPTION_FORM_LBL_QUAKECLUBREDEMPTION_ORDER_NUMBER'); ?></th>
			<td><?php echo $this->item->order_number; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REDEMPTION_FORM_LBL_QUAKECLUBREDEMPTION_USER_ID'); ?></th>
			<td><?php echo $this->item->user_id_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REDEMPTION_FORM_LBL_QUAKECLUBREDEMPTION_PRODUCT_ID'); ?></th>
			<td><?php echo $this->item->product_id; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REDEMPTION_FORM_LBL_QUAKECLUBREDEMPTION_QUANTITY'); ?></th>
			<td><?php echo $this->item->quantity; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REDEMPTION_FORM_LBL_QUAKECLUBREDEMPTION_POINT'); ?></th>
			<td><?php echo $this->item->point; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REDEMPTION_FORM_LBL_QUAKECLUBREDEMPTION_PROMO'); ?></th>
			<td><?php echo $this->item->promo; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REDEMPTION_FORM_LBL_QUAKECLUBREDEMPTION_PROMO_QPERKS'); ?></th>
			<td><?php if( $this->item->promo == 1 ) echo $this->item->promo_qperks; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REDEMPTION_FORM_LBL_QUAKECLUBREDEMPTION_STATUS'); ?></th>
			<td><?php echo $this->item->status; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_REDEMPTION_FORM_LBL_QUAKECLUBREDEMPTION_CREATED_ON'); ?></th>
			<td><?php echo $this->item->created_on; ?></td>
		</tr>

	</table>

</div>

