<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_mmd
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_rsvp_mmd');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_rsvp_mmd'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_USER_ID'); ?></th>
			<td><?php echo $this->item->user_id_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_REDEMPTION_STATUS'); ?></th>
			<td><?php echo $this->item->redemption_status; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_IS_ATTENDED'); ?></th>
			<td><?php echo $this->item->is_attended; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_IS_VALID'); ?></th>
			<td><?php echo $this->item->is_valid; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_IS_DELETED'); ?></th>
			<td><?php echo $this->item->is_deleted; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_UPDATED_AT'); ?></th>
			<td><?php echo $this->item->updated_at; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_NAME'); ?></th>
			<td><?php echo $this->item->name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_COMP_NAME'); ?></th>
			<td><?php echo $this->item->comp_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_DESIGNATION'); ?></th>
			<td><?php echo $this->item->designation; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_MOBILENO'); ?></th>
			<td><?php echo $this->item->mobileno; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_EMAIL'); ?></th>
			<td><?php echo $this->item->email; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_SOURCE'); ?></th>
			<td><?php echo $this->item->source; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_EXTERNAL_LINK'); ?></th>
			<td><?php echo $this->item->external_link; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_DELIVERY'); ?></th>
			<td><?php echo nl2br($this->item->delivery); ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_MMD_FORM_LBL_RSVPTOURDATUM_JOINGAMESTATUS'); ?></th>
			<td><?php echo $this->item->joingamestatus; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_rsvp_mmd&task=rsvptourdatum.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_RSVP_MMD_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_rsvp_mmd.rsvptourdatum.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_RSVP_MMD_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_RSVP_MMD_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_RSVP_MMD_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_rsvp_mmd&task=rsvptourdatum.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_RSVP_MMD_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>