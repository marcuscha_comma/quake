<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_mmd
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class Rsvp_mmdRouter
 *
 */
class Rsvp_mmdRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = JComponentHelper::getComponent('com_rsvp_mmd')->params;
		$this->noIDs = (bool) $params->get('sef_ids');
		
		$rsvptourdata = new RouterViewConfiguration('rsvptourdata');
		$this->registerView($rsvptourdata);
			$rsvptourdatum = new RouterViewConfiguration('rsvptourdatum');
			$rsvptourdatum->setKey('id')->setParent($rsvptourdata);
			$this->registerView($rsvptourdatum);
			$rsvptourdatumform = new RouterViewConfiguration('rsvptourdatumform');
			$rsvptourdatumform->setKey('id');
			$this->registerView($rsvptourdatumform);

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('Rsvp_mmdRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('Rsvp_mmdHelpersRsvp_mmd', __DIR__ . '/helpers/rsvp_mmd.php');
			$this->attachRule(new Rsvp_mmdRulesLegacy($this));
		}
	}


	
		/**
		 * Method to get the segment(s) for an rsvptourdatum
		 *
		 * @param   string  $id     ID of the rsvptourdatum to retrieve the segments for
		 * @param   array   $query  The request that is built right now
		 *
		 * @return  array|string  The segments of this item
		 */
		public function getRsvptourdatumSegment($id, $query)
		{
			return array((int) $id => $id);
		}
			/**
			 * Method to get the segment(s) for an rsvptourdatumform
			 *
			 * @param   string  $id     ID of the rsvptourdatumform to retrieve the segments for
			 * @param   array   $query  The request that is built right now
			 *
			 * @return  array|string  The segments of this item
			 */
			public function getRsvptourdatumformSegment($id, $query)
			{
				return $this->getRsvptourdatumSegment($id, $query);
			}

	
		/**
		 * Method to get the segment(s) for an rsvptourdatum
		 *
		 * @param   string  $segment  Segment of the rsvptourdatum to retrieve the ID for
		 * @param   array   $query    The request that is parsed right now
		 *
		 * @return  mixed   The id of this item or false
		 */
		public function getRsvptourdatumId($segment, $query)
		{
			return (int) $segment;
		}
			/**
			 * Method to get the segment(s) for an rsvptourdatumform
			 *
			 * @param   string  $segment  Segment of the rsvptourdatumform to retrieve the ID for
			 * @param   array   $query    The request that is parsed right now
			 *
			 * @return  mixed   The id of this item or false
			 */
			public function getRsvptourdatumformId($segment, $query)
			{
				return $this->getRsvptourdatumId($segment, $query);
			}
}
