<?php
/**
 * @version    CVS: 1.0.2
 * @package    Com_Segments_digital
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Segments_digital', JPATH_COMPONENT);
JLoader::register('Segments_digitalController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Segments_digital');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
