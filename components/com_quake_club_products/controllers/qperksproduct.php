<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_products
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

/**
 * Qperksproduct controller class.
 *
 * @since  1.6
 */
class Quake_club_productsControllerQperksproduct extends \Joomla\CMS\MVC\Controller\BaseController
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
     *
     * @throws Exception
	 */
	public function edit()
	{
		$app = Factory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_quake_club_products.edit.qperksproduct.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_quake_club_products.edit.qperksproduct.id', $editId);

		// Get the model.
		$model = $this->getModel('Qperksproduct', 'Quake_club_productsModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId && $previousId !== $editId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(Route::_('index.php?option=com_quake_club_products&view=qperksproductform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return    void
	 *
	 * @throws Exception
	 * @since    1.6
	 */
	public function publish()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.edit', 'com_quake_club_products') || $user->authorise('core.edit.state', 'com_quake_club_products'))
		{
			$model = $this->getModel('Qperksproduct', 'Quake_club_productsModel');

			// Get the user data.
			$id    = $app->input->getInt('id');
			$state = $app->input->getInt('state');

			// Attempt to save the data.
			$return = $model->publish($id, $state);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Save failed: %s', $model->getError()), 'warning');
			}

			// Clear the profile id from the session.
			$app->setUserState('com_quake_club_products.edit.qperksproduct.id', null);

			// Flush the data from the session.
			$app->setUserState('com_quake_club_products.edit.qperksproduct.data', null);

			// Redirect to the list screen.
			$this->setMessage(Text::_('COM_QUAKE_CLUB_PRODUCTS_ITEM_SAVED_SUCCESSFULLY'));
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();

			if (!$item)
			{
				// If there isn't any menu item active, redirect to list view
				$this->setRedirect(Route::_('index.php?option=com_quake_club_products&view=qperksproducts', false));
			}
			else
			{
                $this->setRedirect(Route::_('index.php?Itemid='. $item->id, false));
			}
		}
		else
		{
			throw new Exception(500);
		}
	}

	/**
	 * Remove data
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function remove()
	{
		// Initialise variables.
		$app = Factory::getApplication();

		// Checking if the user can remove object
		$user = Factory::getUser();

		if ($user->authorise('core.delete', 'com_quake_club_products'))
		{
			$model = $this->getModel('Qperksproduct', 'Quake_club_productsModel');

			// Get the user data.
			$id = $app->input->getInt('id', 0);

			// Attempt to save the data.
			$return = $model->delete($id);

			// Check for errors.
			if ($return === false)
			{
				$this->setMessage(Text::sprintf('Delete failed', $model->getError()), 'warning');
			}
			else
			{
				// Check in the profile.
				if ($return)
				{
					$model->checkin($return);
				}

                $app->setUserState('com_quake_club_products.edit.inventory.id', null);
                $app->setUserState('com_quake_club_products.edit.inventory.data', null);

                $app->enqueueMessage(Text::_('COM_QUAKE_CLUB_PRODUCTS_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(Route::_('index.php?option=com_quake_club_products&view=qperksproducts', false));
			}

			// Redirect to the list screen.
			$menu = Factory::getApplication()->getMenu();
			$item = $menu->getActive();
			$this->setRedirect(Route::_($item->link, false));
		}
		else
		{
			throw new Exception(500);
		}
	}

	public function addToCart(){
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$app    = JFactory::getApplication();
		$user   = JFactory::getUser();
		$userId = (int) $user->get('id');
		$requestData = $app->input->post->get('jform', array(), 'array');
		$convertedDate = date("Y-m-d H:i:s");

		$db->setQuery('SELECT id,quantity FROM #__cus_qperks_cart where user_id ='.$userId.' and product_id='. $db->Quote($db->escape($requestData['product_id'], true)) .' and remark='.$db->Quote($db->escape($requestData['remark_description_value'], true)));
		$checkCarts             = $db->loadAssoc();

		$db->setQuery('SELECT id,max_redemption FROM #__cus_qperks_products where quantity > 0 and id='. $db->Quote($db->escape($requestData['product_id'], true)));
		$checkProducts = $db->loadAssoc();
 		// echo "<pre>";
        // print_r($checkProducts);
        // echo "</pre>";
        // die();
		if ($requestData['product_quantity'] <= 0 || !$checkProducts) {
			$app->redirect(JUri::base().'quake-club/my-carts');
			die();
		}
		
		if ($checkCarts) {
			$cart = new stdClass();
			$cart->quantity = ($checkCarts['quantity'] + $requestData['product_quantity']>$checkProducts['max_redemption']?$checkProducts['max_redemption']:$checkCarts['quantity'] + $requestData['product_quantity']);
			$cart->remark = $requestData['remark_description_value'];
			$cart->variation_index = $requestData['index'];
			$cart->id = $checkCarts['id'];
			$result = JFactory::getDbo()->updateObject('#__cus_qperks_cart', $cart, 'id');

		}else{
			$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_cart');
			$max             = $db->loadResult();

			$cart = new stdClass();
			$cart->quantity = $requestData['product_quantity']>$checkProducts['max_redemption']?$checkProducts['max_redemption']:$requestData['product_quantity'];
			$cart->user_id = $userId;
			$cart->product_id = $requestData['product_id'];
			$cart->remark = $requestData['remark_description_value'];
			$cart->variation_index = $requestData['index'];
			$cart->state=1;
			$cart->created_by=$userId;
			$cart->modified_by=$userId;
			$cart->created_on=$convertedDate;
			$cart->ordering=$max + 1;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_qperks_cart', $cart);
		}
		$app->redirect(JUri::base().'quake-club/my-carts');

	}
}
