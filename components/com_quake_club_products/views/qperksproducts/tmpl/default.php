<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_products
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


// echo "<pre>";
// print_r($this->pagination);
// echo "</pre>";
//http://43.228.245.193/~cheeliem/astro-joomla/rewards-catalogue/qperksproduct/1
?>

<div id="app">
	<div class="pink-page-title">
		<div class="container">
			<h2>Rewards Catalogue</h2>
		</div>
	</div>

	<div>
		<div class="mt-4 d-none d-md-block">
			<div class="filter-options">
				<label v-if="hideCategory" class="form-label">Filter by</label>
				<select v-if="hideCategory" data-width="fit" class="selectpicker" v-model="selectedCategory" @change="selectOnChanged">
					<option value="All categories">All categories</option>
					<option v-for="category in categoryArray" :value="category">{{category}}</option>
				</select>
				<label class="form-label">Sort by</label>
				<select data-width="fit" class="selectpicker" v-model="selectedSort" @change="selectOnChanged">
					<option value="0">Q-Perks low to high</option>
					<option value="1">Q-Perks high to low</option>
				</select>
			</div>
		</div>

		<div class="mobile-filter d-block d-sm-block d-md-none">
			<div id="filter-trigger" class="btn btn-grey btn-block">
				Filter by
			</div>

			<div class="mobile-filter-container">
				<div class="row border-bottom">
					<div v-if="hideCategory" class="col-5">
						<label class="form-label">Filter by</label>
					</div>
					<div v-if="hideCategory" class="col-7">
						<select class="selectpicker" v-model="selectedCategory" @change="selectOnChanged"
							data-mobile="true">
							<option value="All categories">All categories</option>
							<option v-for="category in categoryArray" :value="category">{{category}}</option>
						</select>
					</div>
				</div>

				<div class="row border-bottom">
					<div class="col-5">
						<label class="form-label">Sort by</label>
					</div>
					<div class="col-7">
						<select class="selectpicker" v-model="selectedSort" @change="selectOnChanged"
							data-mobile="true">
							<option value="0">Q-Perks low to high</option>
							<option value="1">Q-Perks high to low</option>
						</select>
					</div>
				</div>

				<div id="filter-close" class="btn btn-pink">
					Done
				</div>
			</div>
		</div>

		<div>
			<h2 class="f-20 mb-3 mt-4">{{selectedCategory}}</h2>
			<div class="row" v-show="showStatus">
				<div class="col-md-4 col-sm-6" v-for="(item, index) in itemsArray"
					v-if="(index+1) <= loadMoreNumber">


					<div @click="goToLink(item.id)" class="product-item">
			       		<div class="image-holder" :style="{ 'background-image': 'url(../' + item.image1 + ')' }">
								<div v-if="item.promo == 1 && item.promo_label != ''" class="product-promo" >
 							 		{{item.promo_label}}
								  </div>
								  
								  	<div v-if="item.quantity == 0 || item.stock_status == 0" class="product-promo out-stock" >
										OUT OF STOCK
									</div>
							</div>
							<div class="price">
								<h3 v-if="item.promo == 1" class="product-points">{{numberConvert(item.promo_qperks)}} Q-Perks</h3>
								<h3 class="product-points" :class="{'ori-price': item.promo == 1 }">{{numberConvert(item.point)}} Q-Perks</h3>
							</div>
							<div class="promo-note">
		            {{item.promo_description}}
		          </div>
						<h3 class="product-name">{{item.name}}</h3>
					</div>

				</div>
			</div>
			<div v-show="!showStatus" style="text-align:center;">
				<h5 class="section-desc text-muted">No results have been found.</h5>
			</div>
			<div class="loading-bar" v-if="loadMoreNumber < itemsArray.length">
				<div @click="loadMore(loadMoreNumber)">Load more</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    var itemsObjFromPhp = <?php echo json_encode($this->items); ?>;
    var catgoryObjFromPhp = <?php echo json_encode($this->category_list); ?>;

    var app = new Vue({
        el: '#app',
        data: {
            itemsArray: itemsObjFromPhp,
			categoryArray: catgoryObjFromPhp,
            selectedCategory: 'All categories',
            selectedSort: 0,
            currentYear: new Date().getFullYear(),
            loadMoreNumber: 9,
            showStatus : true,
            hideCategory : false,

        },
        mounted: function () {
            this.selectOnChanged();
			var tmp = "";
			_this = this;

			if (document.cookie.split(';').filter(function(item) {
				return item.trim().indexOf('loadmore=') == 0
			}).length) {
				document.cookie.split(";").forEach(function(item){
					if (item.trim().indexOf('loadmore=')==0) {
						tmp = item.split("=");
						_this.loadMoreNumber = tmp[1];
					}

				});
				// console.log(item.trim().indexOf('loadmore='));

				// console.log('The cookie "reader" exists (ES5)',document.cookie.split(";"))
			}
        },
        updated: function () {
            jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
        },
        methods: {
            selectOnChanged :function(){
                _this = this;
                _this.itemsArray = itemsObjFromPhp;

                if (_this.selectedCategory != 'All categories') {
                    _this.itemsArray = _this.itemsArray.filter(function(item){
                        return item.category == _this.selectedCategory;
                    });
                }
				if (_this.itemsArray.length > 0) {
                    _this.showStatus = true;
                }else{
                    _this.showStatus = false;
                }
                if (_this.selectedSort != "0") {
                    return _this.itemsArray.sort(function(a, b){ return b.sort_value - a.sort_value });
                }else{
                    return _this.itemsArray.sort(function(a, b){ return a.sort_value - b.sort_value });
				}

            },
            loadMore :function(loadMoreNumber){
				var now = new Date();
				var time = now.getTime();
				var expireTime = time + 1000*900;
				now.setTime(expireTime);
                this.loadMoreNumber =loadMoreNumber + 6;
				document.cookie = "loadmore="+this.loadMoreNumber+"; expires=" + now.toUTCString()+';path=/';
            },
            goToLink :function(link){
                window.location.href = 'rewards-catalogue/qperksproduct/'+link;
            },
			numberConvert :function(number){
				return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
        }
    })
</script>
