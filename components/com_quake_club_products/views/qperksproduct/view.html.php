<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_products
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

/**
 * View to edit
 *
 * @since  1.6
 */
class Quake_club_productsViewQperksproduct extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = Factory::getApplication();
		$user = Factory::getUser();
		$db    = JFactory::getDBO();

		$this->state  = $this->get('State');
		$this->item   = $this->get('Item');
		$this->params = $app->getParams('com_quake_club_products');

		
		if (!$this->item->id)
		{
			$app->redirect(JUri::base());		
		}

		$db->setQuery('SELECT quantity,remark FROM #__cus_qperks_cart WHERE product_id ='.$this->item->id.' and user_id ='.$user->id);
		$value = $db->loadRow();
		
		$this->cart_quantity = $value[0];
		$this->cart_remark = $value[1];

		if ($this->item->variation) {
			$this->item->list_templates = json_decode($this->item->list_templates);
			$this->item->ori_list_templates = $this->item->list_templates;
			// $this->item->color_list = array_unique($this->item->list_templates->color);
			$this->item->size_list = array_unique($this->item->list_templates->size);
			foreach ($this->item->list_templates->size as $key => $value) {
				$this->item->list[$key]["size"] = $this->item->list_templates->size[$key];
				$this->item->list[$key]["color"] = $this->item->list_templates->color[$key];
				$this->item->list[$key]["quantity"] = $this->item->list_templates->quantity[$key];
			}
			foreach ($this->item->list as $key => $value) {
				
			}
		}

		//Check User redemption status
		if ($user->redemption_num > 0 || $user->dob_redemption_num == 1) {
			//Check item stock status
			if ($this->item->stock_status) {
				//Check product quantity
				if ($this->item->quantity>0) {
						$user->redemption_status = "allow_add_to_cart";
				}else{
					$user->redemption_status = "out_of_stock";
				}
			}else{
				$user->redemption_status = "out_of_stock";
			}
		}else{
			$user->redemption_status = "lack_of_redemption_num";
		}

		//Check special redemption
		if ( $user->special_redemption_num == 1 ) {
			//Check the product in promo
			if ($this->item->special_redemption_status == 1) {
				//Check item stock status
				if ($this->item->stock_status) {
					//Check product quantity
					if ($this->item->quantity>0) {
							$user->redemption_status = "allow_add_to_cart";
					}else{
						$user->redemption_status = "out_of_stock";
					}
				}else{
					$user->redemption_status = "out_of_stock";
				}
			}
		}

		$this->redemption_status = $user->redemption_status;
		unset($this->item->category);
		unset($this->item->checked_out);
		unset($this->item->checked_out_time);
		unset($this->item->created_by);
		unset($this->item->created_by_name);
		unset($this->item->highlight);
		unset($this->item->lock_quantity);
		unset($this->item->modified_by);
		unset($this->item->modified_by_name);
		unset($this->item->ordering);
		unset($this->item->quantity);
		unset($this->item->special_redemption_num);
		unset($this->item->special_redemption_status);
		unset($this->item->state);

		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_quake_club_products');

			if ($authorised !== true)
			{
				throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
			}
		}

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = Factory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', Text::_('COM_QUAKE_CLUB_PRODUCTS_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = Text::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = Text::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
