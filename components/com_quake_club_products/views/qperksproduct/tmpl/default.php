<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_products
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
// echo $this->item->list_templates;
?>

<div id="product-inner">
	<div class="product-pink-bg">
		<div class="container">
			<button onclick="window.history.back()" class="btn btn-white btn-wide">Back</button>
		</div>
	</div>

	<form id="app" class=""
		action="<?php echo JRoute::_('index.php?option=com_quake_club_products&task=qperksproduct.addToCart'); ?>" method="post">
		<div class="product-pink-bg">
			<div class="container">
				<div class="row">
					<div class="col-xl-4 col-md-5">
						<div class="product-slider">
							<div class="owl-carousel owl-theme">
							<?php for ($i=1; $i <= 5; $i++) {
									$tmpImgStr = 'image'.$i;
									if ($this->item->$tmpImgStr) { ?>
											<div class="item" style="background-image: url('<?php echo $this->item->$tmpImgStr; ?>')">
											</div>
									<?php }
							} ?>
							</div>

							<div v-if="item.promo == 1 && item.promo_label != ''" class="product-promo" >
								{{item.promo_label}}
							</div>
							<div v-if="item.stock_status == 0" class="product-promo out-stock" >
							OUT OF STOCK
							</div>
						</div>
					</div>
					<div class="col-xl-8 col-md-7 ">
						<h2 class="product-name">{{item.name}}</h2>
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-end product-info">
			<div class="col-xl-8 col-md-7">
				<div class="form-group row">
			    	<label for="staticEmail" class="col-3 col-lg-2 col-form-label bold">Required</label>
			    	<div class="col-9 col-lg-10">
						<div class="price mt-2">
							<h3 v-if="item.promo == 1" class="text-highlight product-points f-20">{{numberConvert(item.promo_qperks)}} Q-Perks</h3>
							<h3 class="product-points" :class="{'ori-price': item.promo == 1 }">{{numberConvert(item.point)}} Q-Perks</h3>
							<div class="promo-note">
								{{item.promo_description}}
							</div>
						</div>
					</div>
			  	</div>
				
				<div v-if="item.variation == 1">
					<div class="form-group row" v-if="selectedSize != '-'">
						<label for="staticEmail" class="col-3 col-lg-2 col-form-label bold">Size</label>
						<div class="col-9 col-lg-10 py-2">
							<div class="custom-control custom-radio custom-control-inline" v-for="size in item.size_list" @change="checkStockQuantity('')">
								<input type="radio" :id="size" name="size" class="custom-control-input" :value="size" v-model="selectedSize" required>
								<label class="custom-control-label" :for="size">{{size}}</label>
							</div>
						</div>						
					</div>
					<div v-if="selectedSize!=''">
						<div class="form-group row">
						<label for="staticEmail" class="col-3 col-lg-2 col-form-label bold">Color</label>
						<div class="col-9 col-lg-10 py-2">
								<div class="custom-control custom-radio custom-control-inline" v-for="(list, index) in item.list" v-if="selectedSize == list.size"  @change="checkStockQuantity(index)">
									<input type="radio" :id="list.color" name="color" class="custom-control-input" :value="list.color" v-model="selectedColor" required>
									<label class="custom-control-label" :for="list.color">{{list.color}}</label>
								</div>
							</div>
						</div>						
					</div>
				</div>

				<div class="form-group row" v-if=" redemption_status == 'allow_add_to_cart' ">
					<label for="staticEmail" class="col-3 col-lg-2 col-form-label bold">Quantity</label>
					<div class="col-9 col-lg-10">
							<div class="quantity-input d-inline-block">
								<div class="toggle-minus" @click="minusQuantity()"><i class="fas fa-minus"></i></div>
								<div class="selected-quantity"><input readonly @change="checkQuantity()" name="jform[product_quantity]" v-model="itemQuantity" type="text"></div>
								<div class="toggle-add" @click="addQuantity()"><i class="fas fa-plus"></i></div>
							</div>
					</div>
				</div>
				
				<!-- <div class="form-group row edit-form" v-if="item.remark == 1 &&( redemption_status)"> -->
					<!-- <label for="remarks" class="col-3 col-lg-2 col-form-label bold">Remarks</label> -->
					<!-- <div class="col-9 col-lg-6"> -->
							<input type="hidden" id="remarks" name="jform[remark_description_value]" :placeholder="item.remark_description" :value="cartRemark">
							<!-- <div class="form-text form-error">Please specify your preferred size / colour.</div> -->
					<!-- </div> -->
			  	<!-- </div> -->

				<div class="row">
					<div class="col-sm-5 col-md-8 col-lg-5" v-if=" redemption_status == 'allow_add_to_cart' ">
						<button :disabled="(itemQuantity == 0 || parseInt(cartQuantity)>=parseInt(item.max_redemption))" type="submit" class="btn btn-pink btn-icon btn-block"><i class="fa fa-shopping-cart"></i> Add to cart</button>
						<!-- <button disabled type="submit" class="btn btn-pink btn-icon btn-block"><i class="fa fa-shopping-cart"></i> Add to cart</button> -->
						<div class="font-brand small mt-3">* Free shipping nationwide</div>
					</div>
					<div class="col-sm-12 col-lg-8" v-if=" redemption_status == 'lack_of_redemption_num' ">
						<div class="alert-msg alert-danger f-14">Sorry, you have already reached the redemption limit in this quarter.</div>
					</div>
					<div class="col-sm-12 col-lg-8" v-if=" redemption_status == 'out_of_stock' ">
						<div class="alert-msg alert-secondary f-14">Sorry, this item is out of stock.</div>
					</div>
				</div>
			</div>
		</div>

		<input name="jform[product_id]" type="hidden" :value="item.id">
		<input name="jform[size]" type="hidden" :value="selectedSize">
		<input name="jform[color]" type="hidden" :value="selectedColor">
		<input name="jform[variation]" type="hidden" :value="item.variation">
		<input name="jform[index]" type="hidden" :value="selectedIndex">
		<input name="jform[ori_list_templates]" type="hidden" :value="JSON.stringify(item.ori_list_templates)">
		

	</form>

	<div class="product-description">
		<h5 class="mb-4">Information</h5>
		<div class="editor-content line-height"><?php echo $this->item->description; ?></div>
	</div>
</div>

<script>

var itemFromObject = <?php echo json_encode($this->item); ?>;
var redemption_status = <?php echo json_encode($this->redemption_status); ?>;
var quantityFromObject = <?php echo json_encode($this->cart_quantity); ?>;
var remarkFromObject = <?php echo json_encode($this->cart_remark); ?>;

    var app = new Vue({
        el: '#app',
        data: {
			item : itemFromObject,
			redemption_status : redemption_status,
			cartQuantity : quantityFromObject,
			cartRemark : remarkFromObject,
			itemQuantity : 1,
			selectedSize : "-",
			selectedColor : "",
			selectedQuantity : 0,
			selectedIndex: ""
        },
        mounted: function () {
        },
        updated: function () {
        },
        methods: {
			checkQuantity:function (){
				this.itemQuantity = parseInt(this.itemQuantity);
				if (isNaN(this.itemQuantity)) {
					this.itemQuantity = 0;
				}
				if(this.itemQuantity <= 0 ){
					this.itemQuantity = 0;
				}else if(this.itemQuantity >= this.item.max_redemption){
					this.itemQuantity = this.item.max_redemption;
				}
			},
			minusQuantity:function (){
				this.itemQuantity = parseInt(this.itemQuantity);
				if(this.itemQuantity <= 0 ){
					this.itemQuantity = 0;
				}else{
					this.itemQuantity --;
				}
			},
			addQuantity:function (){
				this.itemQuantity = parseInt(this.itemQuantity);
				if(this.itemQuantity >= this.item.max_redemption ){
					this.itemQuantity = this.item.max_redemption;
				}else{
					this.itemQuantity ++;
				}
			},
			numberConvert :function(number){
				return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			},
			checkStockQuantity:function (index){
				console.log(index);
				
				_this = this;
				var tmp = "";
				tmp = _this.item.list.filter(function(list){
                    return (list.size == _this.selectedSize && list.color == _this.selectedColor);
                });
				if (tmp.length) {
					_this.selectedQuantity = tmp[0].quantity;
				}else{
					_this.selectedQuantity = 0;
				}
				_this.item.max_redemption = _this.selectedQuantity;

				if (_this.selectedSize=="-") {
					_this.cartRemark = "Color : " + _this.selectedColor;
					
				}else{
					_this.cartRemark = "Size : " +_this.selectedSize +", Color : " + _this.selectedColor;
				}

				if (index === '') {
					_this.selectedColor ="";
				}else{
					_this.selectedIndex = index;
				}
			}
        }
    })
</script>
