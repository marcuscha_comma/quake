<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_products
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
$user = JFactory::getUser();
?>

<div id="product-inner">
	<div class="product-pink-bg">
		<div class="container">
			<button onclick="window.history.back()" class="btn btn-white btn-wide">Back</button>
		</div>
	</div>

	<form id="app" class=""
		action="<?php echo JRoute::_('index.php?option=com_quake_club_products&task=qperksproduct.addToCart'); ?>" method="post">
		<div class="product-pink-bg">
			<div class="container">
				<div class="row">
					<div class="col-xl-4 col-md-5">
						<div class="product-slider">
							<div class="owl-carousel owl-theme">
							<?php for ($i=1; $i <= 5; $i++) {
									$tmpImgStr = 'image'.$i;
									if ($this->item->$tmpImgStr) { ?>
											<div class="item" style="background-image: url('<?php echo $this->item->$tmpImgStr; ?>')">
											</div>
									<?php }
							} ?>
						 </div>

						 <div v-if="item.promo == 1" class="product-promo" >
							 {{item.promo_label}}
						</div>
						</div>
					</div>
					<div class="col-xl-8 col-md-7 ">
						<h2 class="product-name">{{item.name}}</h2>
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-end product-info">
			<div class="col-xl-8 col-md-7">
				<div class="form-group row">
			    <label for="staticEmail" class="col-3 col-lg-2 col-form-label bold">Required</label>
			    <div class="col-9 col-lg-10">
						<div class="price mt-2">
							<h3 v-if="item.promo == 1" class="text-highlight product-points f-20">{{item.promo_qperks}} Q-Perks</h3>
							<h3 class="product-points" :class="{'ori-price': item.promo == 1 }">{{item.point}} Q-Perks</h3>
							<div class="promo-note">
								{{item.promo_description}}
							</div>
						</div>
			    </div>
			  </div>

				<div class="form-group row" v-if="user.redemption_num == '1' || user.dob_redemption_num == '1'">
			    <label for="staticEmail" class="col-3 col-lg-2 col-form-label bold">Quantity</label>
			    <div class="col-9 col-lg-10">
						<div class="quantity-input d-inline-block">
							<div class="toggle-minus" @click="minusQuantity()"><i class="fas fa-minus"></i></div>
							<div class="selected-quantity"><input @change="checkQuantity()" name="jform[product_quantity]" v-model="itemQuantity" type="text"></div>
							<div class="toggle-add" @click="addQuantity()"><i class="fas fa-plus"></i></div>
						</div>
						<!-- <div class="d-inline-block quantity-left">Only {{item.quantity}} items left</div> -->
						<input name="jform[quantity]" type="hidden" :value="item.quantity">
			    </div>
			  </div>

				<div class="row">
					<div class="col-sm-5 col-md-8 col-lg-5" v-if="user.redemption_num == '1' || user.dob_redemption_num == '1'">
						<button :disabled="itemQuantity == 0" type="submit" class="btn btn-pink btn-icon btn-block"><i class="fa fa-shopping-cart"></i> Add to cart</button>
						<div class="font-brand small mt-3">* Free shipping nationwide</div>
					</div>
					<div class="col-sm-12 col-lg-8" v-if="user.redemption_num != '1' && user.dob_redemption_num != '1'">
						<div class="alert-msg alert-danger f-14">Sorry, you have already reached the redemption limit in this quater.</div>
					</div>
				</div>
			</div>
		</div>

		<input name="jform[product_id]" type="hidden" :value="item.id">

	</form>

	<div class="product-description">
		<h5 class="mb-4">Information</h5>
		<div class="editor-content line-height"><?php echo $this->item->description; ?></div>
	</div>
</div>

<script>

var itemFromObject = <?php echo json_encode($this->item); ?>;
var userFromObject = <?php echo json_encode($user); ?>;

    var app = new Vue({
        el: '#app',
        data: {
			item : itemFromObject,
			user : userFromObject,
			itemQuantity : 1
        },
        mounted: function () {
        },
        updated: function () {
        },
        methods: {
			checkQuantity:function (){
				this.itemQuantity = parseInt(this.itemQuantity);
				if (isNaN(this.itemQuantity)) {
					this.itemQuantity = 0;
				}
				if(this.itemQuantity <= 0 ){
					this.itemQuantity = 0;
				}else if(this.itemQuantity >= this.item.quantity){
					this.itemQuantity = this.item.quantity;
				}
			},
			minusQuantity:function (){
				this.itemQuantity = parseInt(this.itemQuantity);
				if(this.itemQuantity <= 0 ){
					this.itemQuantity = 0;
				}else{
					this.itemQuantity --;
				}
			},
			addQuantity:function (){
				this.itemQuantity = parseInt(this.itemQuantity);
				if(this.itemQuantity >= this.item.quantity ){
					this.itemQuantity = this.item.quantity;
				}else{
					this.itemQuantity ++;
				}
			}
        }
    })
</script>
