<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_download_centre_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_quake_club_download_centre_reports');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_quake_club_download_centre_reports'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_CREDENTIALS_BY_MONTH'); ?></th>
			<td><?php echo $this->item->credentials_by_month; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_CREDENTIALS_BY_WEEK'); ?></th>
			<td><?php echo $this->item->credentials_by_week; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_BRAND_PROFILES_BY_MONTH'); ?></th>
			<td><?php echo $this->item->brand_profiles_by_month; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_BRAND_PROFILES_BY_WEEK'); ?></th>
			<td><?php echo $this->item->brand_profiles_by_week; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_RATE_CARDS_BY_MONTH'); ?></th>
			<td><?php echo $this->item->rate_cards_by_month; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_RATE_CARDS_BY_WEEK'); ?></th>
			<td><?php echo $this->item->rate_cards_by_week; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_PROGRAMME_SCHEDULES_BY_MONTH'); ?></th>
			<td><?php echo $this->item->programme_schedules_by_month; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_PROGRAMME_SCHEDULES_BY_WEEK'); ?></th>
			<td><?php echo $this->item->programme_schedules_by_week; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_PACKAGES_BY_MONTH'); ?></th>
			<td><?php echo $this->item->packages_by_month; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_PACKAGES_BY_WEEK'); ?></th>
			<td><?php echo $this->item->packages_by_week; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_YEAR'); ?></th>
			<td><?php echo $this->item->year; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_MONTH'); ?></th>
			<td><?php echo $this->item->month; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_FORM_LBL_QUAKECLUBDOWNLOADCENTREREPORT_WEEK'); ?></th>
			<td><?php echo $this->item->week; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_quake_club_download_centre_reports&task=quakeclubdownloadcentrereport.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_quake_club_download_centre_reports.quakeclubdownloadcentrereport.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_quake_club_download_centre_reports&task=quakeclubdownloadcentrereport.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>