<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_download_centre_reports
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2020 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = Factory::getLanguage();
$lang->load('com_quake_club_download_centre_reports', JPATH_SITE);
$doc = Factory::getDocument();
$doc->addScript(Uri::base() . '/media/com_quake_club_download_centre_reports/js/form.js');

$user    = Factory::getUser();
$canEdit = Quake_club_download_centre_reportsHelpersQuake_club_download_centre_reports::canUserEdit($this->item, $user);


?>

<div class="quakeclubdownloadcentrereport-edit front-end-edit">
	<?php if (!$canEdit) : ?>
		<h3>
			<?php throw new Exception(Text::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
		</h3>
	<?php else : ?>
		<?php if (!empty($this->item->id)): ?>
			<h1><?php echo Text::sprintf('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_EDIT_ITEM_TITLE', $this->item->id); ?></h1>
		<?php else: ?>
			<h1><?php echo Text::_('COM_QUAKE_CLUB_DOWNLOAD_CENTRE_REPORTS_ADD_ITEM_TITLE'); ?></h1>
		<?php endif; ?>

		<form id="form-quakeclubdownloadcentrereport"
			  action="<?php echo Route::_('index.php?option=com_quake_club_download_centre_reports&task=quakeclubdownloadcentrereport.save'); ?>"
			  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
			
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />

	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->getInput('created_by'); ?>
				<?php echo $this->form->getInput('modified_by'); ?>
	<?php echo $this->form->renderField('credentials_by_month'); ?>

	<?php echo $this->form->renderField('credentials_by_week'); ?>

	<?php echo $this->form->renderField('brand_profiles_by_month'); ?>

	<?php echo $this->form->renderField('brand_profiles_by_week'); ?>

	<?php echo $this->form->renderField('rate_cards_by_month'); ?>

	<?php echo $this->form->renderField('rate_cards_by_week'); ?>

	<?php echo $this->form->renderField('programme_schedules_by_month'); ?>

	<?php echo $this->form->renderField('programme_schedules_by_week'); ?>

	<?php echo $this->form->renderField('packages_by_month'); ?>

	<?php echo $this->form->renderField('packages_by_week'); ?>

	<?php echo $this->form->renderField('year'); ?>

	<?php echo $this->form->renderField('month'); ?>

	<?php echo $this->form->renderField('week'); ?>

			<div class="control-group">
				<div class="controls">

					<?php if ($this->canSave): ?>
						<button type="submit" class="validate btn btn-primary">
							<?php echo Text::_('JSUBMIT'); ?>
						</button>
					<?php endif; ?>
					<a class="btn"
					   href="<?php echo Route::_('index.php?option=com_quake_club_download_centre_reports&task=quakeclubdownloadcentrereportform.cancel'); ?>"
					   title="<?php echo Text::_('JCANCEL'); ?>">
						<?php echo Text::_('JCANCEL'); ?>
					</a>
				</div>
			</div>

			<input type="hidden" name="option" value="com_quake_club_download_centre_reports"/>
			<input type="hidden" name="task"
				   value="quakeclubdownloadcentrereportform.save"/>
			<?php echo HTMLHelper::_('form.token'); ?>
		</form>
	<?php endif; ?>
</div>
