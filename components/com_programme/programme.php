<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Programme
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Programme', JPATH_COMPONENT);
JLoader::register('ProgrammeController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Programme');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
