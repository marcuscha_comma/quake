<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_cart_complete
 * @author     midoff <midoff89@gmail.com>
 * @copyright  2019 midoff
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>
<div id="app">
	<div class="row">
		<div class="col-md-4 order-last order-md-first  pt-5 pt-md-0">
			<img class="window" src="./images/club/girl-box-03.png" />
		</div>
		<div class="col-md-6">
			<img width="108" src="./images/club/quake-club-white.png" alt="Quake Club" />
			<h2>Thank you for<br />redeeming your Q-Perks.</h2>
			<p>Check your email for confirmation.</p>

			<div class="row">
				<div class="col-lg-8">
					<a href="./quake-club-dashboard" class="btn btn-white btn-block mt-4">Back to home page</a>
				</div>
			</div>
		</div>
		<div class="col-md-2 order-first order-md-last text-right">
			<img class="balloon" src="./images/club/balloon-02.png" />
		</div>
	</div>
</div>
