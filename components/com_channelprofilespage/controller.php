<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_channelprofilespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * Brand Profiles
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
/**
 * Channel Profiles Page Component Controller
 *
 * @since  0.0.1
 */
class channelprofilespageController extends JControllerLegacy
{
}