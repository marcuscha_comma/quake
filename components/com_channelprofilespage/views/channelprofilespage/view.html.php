<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_channelprofilespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the Channel Profiles Page Component
 *
 * @since  0.0.1
 */
class channelprofilespageViewchannelprofilespage extends JViewLegacy
{
	/**
	 * Display the Channel Profiles Page view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Assign data to the view
		$this->msg = 'Channel Profiles Page';

		//get list of segments
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_segments WHERE state > 0 order by id asc');
		$segments = $db->loadObjectList();

		//get list of segments digital
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_segments_digital WHERE state > 0 order by id asc');
		$segments_digital = $db->loadObjectList();

		//get list of segments radio
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_segments_radio WHERE state > 0 order by id asc');
		$segments_radio = $db->loadObjectList();
		
		//get list of channels
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_channels WHERE state > 0 order by ordering asc');
		$channels = $db->loadObjectList();

		//get list of radio
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_radio_stations WHERE state > 0 order by ordering asc');
		$radio_stations = $db->loadObjectList();

		//get list of digitals
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__cus_digitals WHERE state > 0 order by ordering asc');
		$digitals = $db->loadObjectList();

		foreach ($segments as $key => $segmentvalue) {
			$segmentvalue->channels = array();
			foreach ($channels as $key => $channelvalue) {
				$channelvalue->showTooltip = False;
				if ($segmentvalue->id == $channelvalue->segment) {
					array_push($segmentvalue->channels,$channelvalue);
				}
			}
		}

		foreach ($segments_digital as $key => $segmentvalue) {
			$segmentvalue->digitals = array();
			foreach ($digitals as $key => $digitalvalue) {
				$digitalvalue->showTooltip = False;
				if ($segmentvalue->id == $digitalvalue->segment) {
					array_push($segmentvalue->digitals,$digitalvalue);
				}
			}
		}

		foreach ($segments_radio as $key => $segmentvalue) {
			$segmentvalue->radio_stations = array();
			foreach ($radio_stations as $key => $radiovalue) {
				$radiovalue->showTooltip = False;
				if ($segmentvalue->id == $radiovalue->segment) {
					array_push($segmentvalue->radio_stations,$radiovalue);
				}
			}
		}

		$this->segments = array_slice($segments,0,7);
		$this->channels = $channels;
		$this->segments_radio = $segments_radio;
		$this->radio_stations = $radio_stations;
		$this->segments_digital = $segments_digital;
		$this->digitals = $digitals;

		// Display the view
		parent::display($tpl);
	}
}
