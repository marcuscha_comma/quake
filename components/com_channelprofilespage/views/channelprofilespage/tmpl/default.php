<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_channelprofilespage
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<div id="app">
  <div class="tab-pills scrollable-tab">
    <div class="row">
      <div class="col"><a href="./our-brands" class="tab-pill active">Brand Profiles</a></div>
      <div class="col"><a href="./our-brands/reach" class="tab-pill">Audience Reach</a></div>
    </div>
  </div>

    <div>
        <div class="row align-items-center">
            <div class="col-lg-9 col-md-8">
                <b>Brand Profiles</b>
                <p>Discover the best in entertainment with our massive selection of channels and brands. Download the profiles today.</p>
            </div>
            <div class="col-lg-3 col-md-4">
              <a href="./channel-profile" class="btn btn-pink btn-block">Download Centre</a>
            </div>
        </div>

        
        <nav class="nav medium-tab mb-0">
          <a class="nav-link" @click="tabOnCLick('tv')" :class="{ active : tvTabIsShow }">
            <div class="medium-icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-420 -641)"><g transform="translate(424 601.06)"><g transform="translate(0 42.667)"><path d="M26.376,42.667H1.758A1.761,1.761,0,0,0,0,44.425V60.837A1.761,1.761,0,0,0,1.758,62.6h9.964v1.263l-6.544,1.09a.586.586,0,0,0,.1,1.164.544.544,0,0,0,.1-.008l6.985-1.164h3.422L22.762,66.1a.552.552,0,0,0,.1.008.586.586,0,0,0,.095-1.164l-6.542-1.09V62.6h9.964a1.761,1.761,0,0,0,1.758-1.758V44.425A1.761,1.761,0,0,0,26.376,42.667ZM15.24,63.768H12.9V62.6H15.24Zm11.723-2.931a.586.586,0,0,1-.586.586H1.758a.586.586,0,0,1-.586-.586V44.425a.586.586,0,0,1,.586-.586H26.376a.586.586,0,0,1,.586.586Z" transform="translate(0 -42.667)" fill="#989898"/><path d="M65.526,85.333H43.252a.586.586,0,0,0-.586.586V99.987a.586.586,0,0,0,.586.586H65.526a.586.586,0,0,0,.586-.586V85.919A.586.586,0,0,0,65.526,85.333ZM64.939,99.4h-21.1v-12.9h21.1Z" transform="translate(-40.321 -82.988)" fill="#989898"/></g></g><rect width="36" height="28" transform="translate(420 641)" fill="none"/></g></svg>
            </div>
            TV
          </a>
          <a class="nav-link" @click="tabOnCLick('radio')" :class="{ active : radioTabIsShow }">
            <div class="medium-icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-518 -641)"><g transform="translate(522.861 610.73)"><g transform="translate(0 34.005)"><path d="M23.642,38.546H6.888L17.7,35.212a.637.637,0,0,0,.394-.758l0-.015a.606.606,0,0,0-.753-.409L2.788,38.515a.647.647,0,0,0-.068.03h-.3A2.425,2.425,0,0,0,0,40.971V53.7a2.425,2.425,0,0,0,2.425,2.425H23.642A2.425,2.425,0,0,0,26.066,53.7V40.971A2.425,2.425,0,0,0,23.642,38.546ZM24.854,53.7a1.212,1.212,0,0,1-1.212,1.212H2.425A1.212,1.212,0,0,1,1.212,53.7V40.971q0-.031,0-.061a1.182,1.182,0,0,1,1.212-1.151H23.7a1.182,1.182,0,0,1,1.151,1.212Z" transform="translate(0 -34.005)" fill="#989898"/><path d="M56.4,245.091c-.061-.061-.091-.091-.152-.091a3.82,3.82,0,0,0-2-.546,3.729,3.729,0,0,0-2,.546.273.273,0,0,0-.182.091,4,4,0,0,0,0,6.7.273.273,0,0,0,.182.091,3.728,3.728,0,0,0,2,.546,3.819,3.819,0,0,0,2-.546c.061,0,.091-.03.152-.091a3.993,3.993,0,0,0,0-6.7Zm-4.516,4.789a2.365,2.365,0,0,1-.394-1.425A2.394,2.394,0,0,1,51.885,247Zm1.819,1.273c-.3-.03-.606-.121-.606-.182V245.91c0-.061.3-.152.606-.182Zm1.819-.182a2.031,2.031,0,0,0-.606.182v-5.425a2.033,2.033,0,0,0,.606.182Zm1.212-1.091V247c0,.424.394.909.394,1.455S56.734,249.456,56.734,249.88Z" transform="translate(-47.338 -232.245)" fill="#989898"/><path d="M317.957,271.11a2.425,2.425,0,1,0,2.455,2.455c0-.02,0-.04,0-.06A2.425,2.425,0,0,0,317.957,271.11Zm.06,3.637a1.212,1.212,0,1,1,1.006-1.388A1.212,1.212,0,0,1,318.018,274.747Z" transform="translate(-297.255 -257.354)" fill="#989898"/><path d="M223.915,271.11a2.425,2.425,0,1,0,2.455,2.455c0-.02,0-.04,0-.06A2.425,2.425,0,0,0,223.915,271.11Zm.06,3.637a1.212,1.212,0,1,1,1.006-1.388A1.212,1.212,0,0,1,223.976,274.747Z" transform="translate(-208.669 -257.354)" fill="#989898"/><path d="M57.788,151.466H37.177a.606.606,0,0,0-.606.606v3.637a.606.606,0,0,0,.606.606H57.788a.606.606,0,0,0,.606-.606v-3.637A.606.606,0,0,0,57.788,151.466Zm-.606,3.637H51.423v-1.212a.606.606,0,1,0-1.212,0V155.1H37.783v-2.425h19.4V155.1Z" transform="translate(-34.449 -144.651)" fill="#989898"/></g></g><rect width="36" height="28" transform="translate(518 641)" fill="none"/></g></svg>
            </div>
            Radio
          </a>
          <a class="nav-link" @click="tabOnCLick('digital')" :class="{ active : digitalTabIsShow }">
            <div class="medium-icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="36" height="28" viewBox="0 0 36 28"><g transform="translate(-630 -641)"><g transform="translate(40.41 527.503)"><path d="M607.021,118.4a.676.676,0,1,0,.674.676A.677.677,0,0,0,607.021,118.4Z" fill="#989898"/><path d="M615.579,116.5H603.217a1.631,1.631,0,0,0-1.627,1.626v21.493a1.631,1.631,0,0,0,1.627,1.626h12.362a1.633,1.633,0,0,0,1.627-1.626V118.123A1.632,1.632,0,0,0,615.579,116.5Zm.276,20.941v2.177a.276.276,0,0,1-.229.272H603.217a.276.276,0,0,1-.276-.275v-2.174ZM602.941,120.3v-2.173a.277.277,0,0,1,.276-.276h12.362a.276.276,0,0,1,.275.275V120.3Zm12.914,1.355v14.434H602.941V121.652Z" fill="#989898"/><path d="M607.972,139.34h2.852a.675.675,0,1,0,0-1.351h-2.852a.675.675,0,1,0,0,1.351Z" fill="#989898"/><path d="M611.775,118.4h-2.852a.676.676,0,0,0,0,1.352h2.852a.676.676,0,0,0,0-1.352Z" fill="#989898"/></g><rect width="36" height="28" transform="translate(630 641)" fill="none"/></g></svg>
            </div>
            Digital
          </a>
        </nav>

        <!-- tv -->
        <div class="accordion-holder mt-0" v-show="tvTabIsShow">
          <div class="accordion" id="accordion-channel">
            <!-- Collapse start -->
                <div class="card border-0" v-for="segment in segmentsArray">
                    <div class="card-header" :id="'heading'+segment.title" data-toggle="collapse" :data-target="'#'+segment.title" aria-expanded="false" :aria-controls="segment.title">
                      <h5>
                        {{segment.title | capitalize}}
                      </h5>
                      <i class="fas fa-plus indicator"></i>
                    </div>

                    <div :id="segment.title" class="collapse" :aria-labelledby="'heading'+segment.title" data-parent="#accordion-channel">
                        <div class="card-body">
                            <div v-for="channel in segment.channels" class="channel-item">
                                <img :src="'./'+channel.image" alt="" @mouseover="mouseOver(channel)" @mouseleave="mouseLeave(channel)">
                                <div class="channel-tooltip">
                                  <div class="close">
                                    <i class="fas fa-times"></i>
                                  </div>
                                  <div class="tooltip-header">
                                    <img :src="'./'+channel.image" />
                                  </div>
                                    <div class="tooltip-desc">
                                      <div class="channel-title">{{channel.title}}</div>
                                      {{channel.description}}
                                    </div>
                                    <!-- <div class="tooltip-footer">
                                        <div class="row">
                                          <div class="col-sm-7">{{channel.channel_popup_label}}</div>
                                          <div class="col-sm-5 reach-figure">{{channel.channel_popup_text}}</div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- Collapse End -->
          </div>
        </div>

        <!-- Radio -->
        <div class="accordion-holder mt-0" v-show="radioTabIsShow">
          <div class="accordion" id="accordion-radio">
            <!-- Collapse start -->
                <div class="card border-0" v-for="segment in segmentsRadioArray">
                    <div class="card-header" :id="'radio-heading'+segment.title" data-toggle="collapse" :data-target="'#'+'radio'+segment.title" aria-expanded="false" :aria-controls="segment.title">
                      <h5>
                        {{segment.title | capitalize}}
                      </h5>
                      <i class="fas fa-plus indicator"></i>
                    </div>

                    <div :id="'radio'+segment.title" class="collapse" :aria-labelledby="'radio-heading'+segment.title" data-parent="#accordion-radio">
                        <div class="card-body">
                            <div v-for="radio in segment.radio_stations" class="channel-item">
                                <img :src="'./'+radio.image" alt="" @mouseover="mouseOver(radio)" @mouseleave="mouseLeave(radio)">
                                <div class="channel-tooltip">
                                  <div class="close">
                                    <i class="fas fa-times"></i>
                                  </div>
                                  <div class="tooltip-header">
                                    <img :src="'./'+radio.image" />
                                  </div>
                                    <div class="tooltip-desc">
                                      <div class="channel-title">{{radio.title}}</div>
                                      {{radio.description}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- Collapse End -->
          </div>
        </div>

        <!-- Digital -->
        <div class="accordion-holder mt-0" v-show="digitalTabIsShow">
          <div class="accordion" id="accordion-digital">
            <!-- Collapse start -->
                <div class="card border-0" v-for="segment in segmentsDigitalArray">
                    <div class="card-header" :id="'digital-heading'+segment.title" data-toggle="collapse" :data-target="'#'+'digital'+segment.title" aria-expanded="false" :aria-controls="segment.title">
                      <h5>
                        {{segment.title | capitalize}}
                      </h5>
                      <i class="fas fa-plus indicator"></i>
                    </div>

                    <div :id="'digital'+segment.title" class="collapse" :aria-labelledby="'digital-heading'+segment.title" data-parent="#accordion-digital">
                        <div class="card-body">
                            <div v-for="digital in segment.digitals" v-if="digital.title != 'Astro Arena'" class="channel-item">
                                <img :src="'./'+digital.image" alt="" @mouseover="mouseOver(digital)" @mouseleave="mouseLeave(digital)">
                                <div class="channel-tooltip">
                                  <div class="close">
                                    <i class="fas fa-times"></i>
                                  </div>
                                  <div class="tooltip-header">
                                    <img :src="'./'+digital.image" />
                                  </div>
                                    <div class="tooltip-desc">
                                    <div class="channel-title">{{digital.title}}</div>
                                      {{digital.description}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- Collapse End -->
          </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    var segmentsObjFromPhp = <?php echo json_encode($this->segments); ?>;
    var channelsObjFromPhp = <?php echo json_encode($this->channels); ?>;
    var radioStationsObjFromPhp = <?php echo json_encode($this->radio_stations); ?>;
    var segmentsDigitalObjFromPhp = <?php echo json_encode($this->segments_digital); ?>;
    var segmentsRadioObjFromPhp = <?php echo json_encode($this->segments_radio); ?>;
    var digitalsObjFromPhp = <?php echo json_encode($this->digitals); ?>;

    var app = new Vue({
      el: '#app',
      data: {
          message: 'Hello Vue!',
          segmentsArray: segmentsObjFromPhp,
          channelsArray: channelsObjFromPhp,
          radioStationsArray: radioStationsObjFromPhp,
          segmentsDigitalArray: segmentsDigitalObjFromPhp,
          segmentsRadioArray: segmentsRadioObjFromPhp,
          digitalsArray: digitalsObjFromPhp,
          channelProfilesShow: true,
          segmentsHighlightsShow: false,
          selectedSegment: "",
          tvTabIsShow: true,
          radioTabIsShow: false,
          digitalTabIsShow: false
      },
      mounted: function () {
        jQuery('#tooltip-desc').tooltip('enable');
      },
      methods: {
        mouseOver: function(item){
          console.log(item.showTooltip);
          item.showTooltip = true;
          console.log(item.showTooltip);
        },
        mouseLeave: function(item){
          item.showTooltip = false;
        },
        convertNumberIntoSuffix: function(number){
          return Math.abs(Number(number)) >= 1.0e+9

            ? Math.abs(Number(number)) / 1.0e+9 + "B"
            // Six Zeroes for Millions 
            : Math.abs(Number(number)) >= 1.0e+6

            ? Math.abs(Number(number)) / 1.0e+6 + " mil"
            // Three Zeroes for Thousands
            : Math.abs(Number(number)) >= 1.0e+3

            ? Math.abs(Number(number)) / 1.0e+3 + "K"

            : Math.abs(Number(number)); 
        },
        tabOnCLick: function(tab){
          switch (tab) {
            case "tv":
              this.tvTabIsShow= true;
              this.radioTabIsShow= false;
              this.digitalTabIsShow= false;
              break;
            case "radio":
              this.tvTabIsShow= false;
              this.radioTabIsShow= true;
              this.digitalTabIsShow= false;
              break;
            case "digital":
              this.tvTabIsShow= false;
              this.radioTabIsShow= false;
              this.digitalTabIsShow= true;
              break;
          
            default:
              break;
          }
        }
      },
      filters: {
        capitalize: function (value) {
          if (!value) return ''
          value = value.toString()
          if (value == "All-Language") {
            value="All Languages";
          }
          return value.charAt(0).toUpperCase() + value.slice(1)
        }
      }
    })
</script>
