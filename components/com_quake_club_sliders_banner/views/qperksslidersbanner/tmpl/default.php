<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_sliders_banner
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_SLIDERS_BANNER_FORM_LBL_QPERKSSLIDERSBANNER_TITLE'); ?></th>
			<td><?php echo $this->item->title; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_SLIDERS_BANNER_FORM_LBL_QPERKSSLIDERSBANNER_IMAGE'); ?></th>
			<td><?php echo $this->item->image; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_SLIDERS_BANNER_FORM_LBL_QPERKSSLIDERSBANNER_LINK_TYPE'); ?></th>
			<td><?php echo $this->item->link_type; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_SLIDERS_BANNER_FORM_LBL_QPERKSSLIDERSBANNER_URL_ADDRESS'); ?></th>
			<td><?php if( $this->item->link_type == 1 ) echo $this->item->url_address; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_SLIDERS_BANNER_FORM_LBL_QPERKSSLIDERSBANNER_YOUTUBE_LINK'); ?></th>
			<td><?php if( $this->item->link_type == 2 ) echo $this->item->youtube_link; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_SLIDERS_BANNER_FORM_LBL_QPERKSSLIDERSBANNER_TARGET_WINDOW'); ?></th>
			<td><?php echo $this->item->target_window; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_QUAKE_CLUB_SLIDERS_BANNER_FORM_LBL_QPERKSSLIDERSBANNER_PUBLISH_DATE'); ?></th>
			<td><?php echo $this->item->publish_date; ?></td>
		</tr>

	</table>

</div>

