<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Quake_club_sliders_banner
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Quake_club_sliders_banner', JPATH_COMPONENT);
JLoader::register('Quake_club_sliders_bannerController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Quake_club_sliders_banner');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
