<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Programme_scheduler
 * @author     ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Programmeschedulerfiles list controller class.
 *
 * @since  1.6
 */
class Programme_schedulerControllerProgrammeschedulerfiles extends Programme_schedulerController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Programmeschedulerfiles', $prefix = 'Programme_schedulerModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
