<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Programme_scheduler
 * @author     ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_PROGRAMME_SCHEDULER_FORM_LBL_PROGRAMMESCHEDULERFILE_TITLE'); ?></th>
			<td><?php echo $this->item->title; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_PROGRAMME_SCHEDULER_FORM_LBL_PROGRAMMESCHEDULERFILE_SEGMENT'); ?></th>
			<td><?php echo $this->item->segment; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_PROGRAMME_SCHEDULER_FORM_LBL_PROGRAMMESCHEDULERFILE_CHANNEL'); ?></th>
			<td><?php echo $this->item->channel; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_PROGRAMME_SCHEDULER_FORM_LBL_PROGRAMMESCHEDULERFILE_ATTACH_FILE'); ?></th>
			<td>
			<?php
			foreach ((array) $this->item->attach_file as $singleFile) : 
				if (!is_array($singleFile)) : 
					$uploadPath = 'uploads/programme-scheduler' . DIRECTORY_SEPARATOR . $singleFile;
					 echo '<a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank">' . $singleFile . '</a> ';
				endif;
			endforeach;
		?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_PROGRAMME_SCHEDULER_FORM_LBL_PROGRAMMESCHEDULERFILE_MONTH'); ?></th>
			<td><?php echo $this->item->month; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_PROGRAMME_SCHEDULER_FORM_LBL_PROGRAMMESCHEDULERFILE_YEAR'); ?></th>
			<td><?php echo $this->item->year; ?></td>
		</tr>

	</table>

</div>

