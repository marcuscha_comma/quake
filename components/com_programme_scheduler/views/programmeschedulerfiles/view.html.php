<?php

/**
 * @version    CVS: 1.0.3
 * @package    Com_Programme_scheduler
 * @author     ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Programme_scheduler.
 *
 * @since  1.6
 */
class Programme_schedulerViewProgrammeschedulerfiles extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();

		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$user       = JFactory::getUser();
		$userId     = $user->get('id');

		//get list of segments
		$db_segments    = JFactory::getDBO();
		$query_segments = $db_segments->getQuery( true );
		$query_segments
			->select( 'id, title' )
			->from( $db_segments->quoteName( '#__cus_segments' ) )
			->where($db_segments->quoteName('state')." > 0")			
			->order( 'id asc' );
		$db_segments->setQuery( $query_segments );
		$segments = $db_segments->loadObjectList();
		
		//get list of channels
		$db_channel    = JFactory::getDBO();
		$query_channel = $db_channel->getQuery( true );
		$query_channel
			->select( 'id, title, segment' )
			->from( $db_channel->quoteName( '#__cus_channels' ) )
			->where($db_channel->quoteName('state')." > 0")			
			->order( 'title asc' );
		$db_channel->setQuery( $query_channel );
		$channels = $db_channel->loadObjectList();

		//get list of programme scheduler files
		$db_ps_files    = JFactory::getDBO();
		$query_ps_files = $db_ps_files->getQuery( true );
		$query_ps_files
			->select( '*, case when up.id is null then "0" ELSE up.type end as source_id' )
			->from( $db_ps_files->quoteName( '#__cus_programme_scheduler_files', 'psf' ) )
			->join('INNER', $db_ps_files->quoteName('#__cus_month', 'm') . ' ON (' . $db_ps_files->quoteName('m.key') . ' = ' . $db_ps_files->quoteName('psf.month') . ')')
			->join('LEFT', $db_ps_files->quoteName('#__cus_qperks_user_point', 'up') . ' ON (up.source = psf.attach_file and up.user_id ='.$userId.')')
			->where($db_ps_files->quoteName('psf.state')." > 0")
			->order( 'psf.year asc' )
			->order( 'psf.month+ 0 asc' );
		$db_ps_files->setQuery( $query_ps_files );
		$ps_files = $db_ps_files->loadObjectList();

		
		foreach ($segments as $key => $segmentvalue) {
			$segmentvalue->channels = array();
			// $segmentvalue->title = strtolower($segmentvalue->title);
			foreach ($channels as $key => $channelvalue) {
				$channelvalue->showTooltip = False;
				if ($segmentvalue->id == $channelvalue->segment) {
					array_push($segmentvalue->channels,$channelvalue);
				}
			}
		}

		foreach ($channels as $key => $channelvalue) {
			$channelvalue->download_file = array();
			$channelvalue->channelShowDownload = True;
			foreach ($ps_files as $key => $ps_filevalue) {
				if ($channelvalue->id == $ps_filevalue->channel) {
					$channelvalue->download_file[] = $ps_filevalue;
				}
			}
			$channelvalue->download_file = array_slice($channelvalue->download_file, -3, 3, true);

			// echo "<pre>";
			// print_r($channelvalue->download_file);
			// echo "</pre>";
		} 
		

		$this->segments = array_slice($segments,0,7);
		$this->channels = $channels;
		$this->ps_files = $ps_files;
		
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_PROGRAMME_SCHEDULER_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
