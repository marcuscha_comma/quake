<?php
/**
 * @version    CVS: 1.0.3
 * @package    Com_Programme_scheduler
 * @author     ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
$app = JFactory::getApplication();   // equivalent of $app = JFactory::getApplication();
$input = $app->input;

$user        = JFactory::getUser();
$userId      = $user->get('id');
$checkEmailUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.checkEmailExist');
$createClientUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.createClient');
// echo $downloadUrl;
if ($input->exists('s'))
{
  $segment_params = $input->get('s', 0, "INT");
}else{
  $segment_params = "3";
}
?>


<div id="app">
<div class="tab-pills scrollable-tab">
    <div class="row">
      <div class="col"><a href="./credentials" class="tab-pill">Credential</a></div>
      <div class="col"><a href="./channel-profile" class="tab-pill">Brand Profile</a></div>
      <div class="col"><a href="./rate-card" class="tab-pill">Rate Cards</a></div>
      <div class="col"><a href="./programme-schedules" class="tab-pill active">TV Programme Schedules</a></div>
      <div class="col"><a href="./packages" class="tab-pill">Advertising Packages</a></div>
    </div>
  </div>

	<div>
		<h2 class="section-title">TV Programme Schedules</h2>
		<p>Download the monthly programme schedules here.</p>
	</div>
	<div class="accordion-holder download-center">
          <div class="accordion" id="accordion-channel">
            <!-- Collapse start -->
                <div class="card border-0" v-for="(segment, index) in segmentsArray">
                    <div @click="selectedChannel=[];selectedChannelArray=[]" class="card-header" :id="'heading'+segment.title" data-toggle="collapse" :data-target="'#'+segment.title" :aria-expanded="index==0?true:false" :aria-controls="segment.title">
                      <h5>
                        {{segment.title | capitalize}}
                      </h5>
                      <i class="fas fa-plus indicator"></i>
                    </div>

                    <div :id="segment.title" :class="{'collapse':true,'show': index==0}" :aria-labelledby="'heading'+segment.title" data-parent="#accordion-channel">
                        <div class="card-body px-3 px-sm-0 py-4">
                          <div class="row">
                            <div class="col-xl-6 mb-3 mb-xl-0">
                              <div class="card-body bg-grey h-100 py-4">
                                <div class="row align-items-center">
                                  <div class="col-sm-4">
                                    <label class="form-label section-title m-md-0">Choose Channel</label>
                                  </div>
                                  <div class="col-sm-8">
                                    <select class="selectpicker white-dropdown" v-model="selectedChannel" data-width="100%">
                      								<option value="">Choose Channel</option>
                      								<option :value="channel" v-if="channel.download_file.length != 0" v-for="channel in segment.channels">{{channel.title}}</option>
                      							</select>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                            <div class="col-xl-6">
                              <div class="row xcard-deck xh-100">
                                <div class="col-sm-4" v-for="(file, index) in selectedChannelArray.download_file" v-if="selectedChannelStatus">
                                  <div class="card astro-card h-100 mb-3 mb-sm-0">
                                    <h3 class="card-header">
                                      {{file.title}}
                                    </h3>
                                    <div class="card-body">
                                      <h5 class="card-title section-title">{{file.value}}</h5>
                                    </div>
                                    <div class="card-footer">
                                      <div class="download-center-button" v-if="loginStatus != 1" data-toggle="modal" data-target="#downloadCenterModal">
                                        Download
                                      </div>
                                      <div class="download-center-button" v-if="loginStatus == 1" @click="downloadUrl(file.attach_file)">
                                        <span v-if="file.source_id != 0"><i class="far fa-check-circle"></i> Downloaded</span>
                                        <span v-if="file.source_id == 0">Download</span>    
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                    </div>
                </div>
            <!-- Collapse End -->
          </div>
        </div>

		<!--
			Overlay CSS design
			@version 1.2
			@author Toures Tiu <toures.tiu@comma.com.my>
			@created at 21/8/2018
			@updated at 26/9/2018 by SyuQian
		-->
		<!-- Modal Start -->
		<div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" hidden="true"></div>

        <div class="modal-body">
          <div>
            <button type="button" class="close font-brand" data-dismiss="modal"
              aria-label="Close">
              <span aria-hidden="true">Close</span>
            </button>

            <div style="clear: both;"></div>
          </div>
          <div class="text-center">
          <h3 class="text-white">Login to Download</h3>
					<p class="text-white mb-4">Please login or sign up to download this file.</p>
            <a href="./login" class="btn btn-pink" @click="goToLink()">Login or Sign up</a>
            <!-- <a href="#" data-dismiss="modal" class="btn btn-white">Skip</a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
		<!-- Modal End -->

</div>



<script type="text/javascript">
    var segmentsObjFromPhp = <?php echo json_encode($this->segments); ?>;
    var channelsObjFromPhp = <?php echo json_encode($this->channels); ?>;
    var ps_filesObjFromPhp = <?php echo json_encode($this->ps_files); ?>;
    var checkEmailUrl = <?php echo json_encode($checkEmailUrl); ?>;
    var createClientUrl = <?php echo json_encode($createClientUrl); ?>;
	  var userIdFromObject = <?php echo json_encode($userId); ?>;
    var updateUserShareArticlePointUrl = <?php echo json_encode($updateUserShareArticlePointUrl); ?>;
    var segmentParamsObjFromPhp = <?php echo json_encode($segment_params); ?>;


    var app = new Vue({
        el: '#app',
        data: {
          segmentsArray: segmentsObjFromPhp,
          channelsArray: channelsObjFromPhp,
			    ps_filesArray: ps_filesObjFromPhp,
          selectedChannel :[],
          selectedChannelArray : [],
          selectedChannelStatus : true,
          urlLink : "",
          firstRegisterUser: false,
          userEmail: "",
          userName: "",
          userPhone: "",
          userCompany: "",
          userDesignation: "",
          clientDataCreateStatus: false,
          item : userIdFromObject,
          loginStatus : 0,
          selectedSegment : segmentParamsObjFromPhp
        },
        mounted: function () {

          if (parseInt(this.item) > 0) {
            this.loginStatus = 1;
          }
          _this = this;
          // if (_this.selectedSegment == 2) {
          //   jQuery('#Chinese').collapse('toggle');
          //   _this.selectedChannel = _this.channelsArray[5];
          // }else{
          //   _this.selectedChannel = _this.channelsArray[9];
          // }
          // if (_this.selectedChannel != '') {
          //   _this.selectedChannelArray = _this.selectedChannel;
          //   _this.selectedChannelStatus = true;
          // }else{
          //   _this.selectedChannelStatus = true;
          // }


        },
        updated: function () {
          _this = this;
          jQuery(this.$el).find('.selectpicker').selectpicker('refresh');

          if (_this.selectedChannel != '') {
            _this.selectedChannelArray = _this.selectedChannel;
            _this.selectedChannelStatus = true;
          }else{
            _this.selectedChannelStatus = true;
          }
        },
        methods: {
            goToLink :function(link){
              window.location.href = link;
            },
            checkEmail : function(){
              _this = this;

              jQuery.ajax({
                url: checkEmailUrl,
                type: 'post',
                data: { email: _this.userEmail },
                success: function (result) {

                  if (result == 1) {
                    window.location.href = _this.urlLink;
                  }else{
                    if (_this.firstRegisterUser) {
                      jQuery.ajax({
                      url: createClientUrl,
                      type: 'post',
                      data: {
                        email: _this.userEmail,
                        name: _this.userName,
                        phone: _this.userPhone,
                        company: _this.userCompany,
                        designation: _this.userDesignation
                      },
                      success: function (result) {
                          window.location.href = _this.urlLink;
                          _this.clientDataCreateStatus = true;
                      },
                      error: function () {
                        console.log('fail');
                      }
                      });
                    }
                    _this.firstRegisterUser = true;
                  }

                },
                error: function () {
                  console.log('fail');
                }
              });
            },
            downloadUrl :function(urlLink){
              source = urlLink;
              this.urlLink = './uploads/programme-scheduler/'+ urlLink;
              window.location.href = this.urlLink;

              jQuery.ajax({
                url: updateUserShareArticlePointUrl,
                type: 'post',
                data: {'source':urlLink},
                success: function (result) {
                  console.log("success");

                },
                error: function () {
                  console.log('fail');
                }
              });


              // this.userEmail = "";
              // this.userName = "";
              // this.userPhone = "";
              // this.userCompany = "";
              // this.userDesignation = "";
            }
        },
        filters: {
            capitalize: function (value) {
                if (!value) return ''
                value = value.toString()
                return value.charAt(0).toUpperCase() + value.slice(1)
            }
        }
    })
</script>
