<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_cny
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Rsvpcnydatum controller class.
 *
 * @since  1.6
 */
class Rsvp_cnyControllerRsvpcnydatumForm extends JControllerForm
{
	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	public function edit($key = NULL, $urlVar = NULL)
	{
		$app = JFactory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_rsvp_cny.edit.rsvpcnydatum.id');
		$editId     = $app->input->getInt('id', 0);

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_rsvp_cny.edit.rsvpcnydatum.id', $editId);

		// Get the model.
		$model = $this->getModel('RsvpcnydatumForm', 'Rsvp_cnyModel');

		// Check out the item
		if ($editId)
		{
			$model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId)
		{
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_rsvp_cny&view=rsvpcnydatumform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return void
	 *
	 * @throws Exception
	 * @since  1.6
	 */
	public function save($key = NULL, $urlVar = NULL)
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$session = JFactory::getSession();

		// Initialise variables.
		$app   = JFactory::getApplication();
		$model = $this->getModel('RsvpcnydatumForm', 'Rsvp_cnyModel');

		// Get the user data.
		$data = JFactory::getApplication()->input->get('jform', array(), 'array');

		// Validate the posted data.
		$form = $model->getForm();

		if (!$form)
		{
			throw new Exception($model->getError(), 500);
		}

		// Validate the posted data.
		$data = $model->validate($form, $data);

		// check duplicate of rsvp cny data
		$db    = JFactory::getDBO();
		$query = $db->getQuery( true );
		$query
			->select( '*' )
			->from( $db->quoteName( '#__cus_rsvp_cny_data' ) )
			->where($db->quoteName('email')."=". $db->quote($data['email']))
			->where($db->quoteName('state').">0");
		$db->setQuery( $query );
		$db->execute();
		$rsvp_duplicate_number = $db->getNumRows();

		// Check for errors.
		if ($data === false)
		{
			// Get the validation messages.
			$errors = $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				}
				else
				{
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			$input = $app->input;
			$jform = $input->get('jform', array(), 'ARRAY');

			// Save the data in the session.
			$app->setUserState('com_rsvp_cny.edit.rsvpcnydatum.data', $jform);

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_rsvp_cny.edit.rsvpcnydatum.id');
			$this->setRedirect(JRoute::_('index.php?option=com_rsvp_cny&view=rsvpcnydatumform&layout=edit&id=' . $id, false));

			$this->redirect();
		}

		if ($rsvp_duplicate_number > 0) {

			$session->set('registerStatus', "duplicated");
			$this->setRedirect(JUri::base().'bangdudu2019');

		}else{
			// Attempt to save the data.
			$return = $model->save($data);

			// Check for errors.
			if ($return === false)
			{
				// Save the data in the session.
				$app->setUserState('com_rsvp_cny.edit.rsvpcnydatum.data', $data);

				// Redirect back to the edit screen.
				$id = (int) $app->getUserState('com_rsvp_cny.edit.rsvpcnydatum.id');
				// $this->setMessage(JText::sprintf('Save failed', $model->getError()), 'warning');
				$session->set('registerStatus','fail');
				$this->setRedirect(JRoute::_('index.php?option=com_rsvp_cny&view=rsvpcnydatumform&layout=edit&id=' . $id, false));
			}

			// Check in the profile.
			if ($return)
			{
				$model->checkin($return);
				$session->set('registerStatus', "success");
				$sent = $this->_sendEmail($data['email']);
			}

			// Clear the profile id from the session.
			$app->setUserState('com_rsvp_cny.edit.rsvpcnydatum.id', null);

			// Redirect to the list screen.
			// $this->setMessage(JText::_('COM_RSVP_CNY_ITEM_SAVED_SUCCESSFULLY'));
			$menu = JFactory::getApplication()->getMenu();
			$item = $menu->getActive();
			$url  = (empty($item->link) ? 'index.php?option=com_rsvp_cny&view=rsvpcnydata' : $item->link);
			// $this->setRedirect(JRoute::_($url, false));
			$this->setRedirect(JUri::base().'bangdudu2019');

			// Flush the data from the session.
			$app->setUserState('com_rsvp_cny.edit.rsvpcnydatum.data', null);
		}

		
	}

	/**
	 * Method to abort current operation
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function cancel($key = NULL)
	{
		$app = JFactory::getApplication();

		// Get the current edit id.
		$editId = (int) $app->getUserState('com_rsvp_cny.edit.rsvpcnydatum.id');

		// Get the model.
		$model = $this->getModel('RsvpcnydatumForm', 'Rsvp_cnyModel');

		// Check in the item
		if ($editId)
		{
			$model->checkin($editId);
		}

		$menu = JFactory::getApplication()->getMenu();
		$item = $menu->getActive();
		$url  = (empty($item->link) ? 'index.php?option=com_rsvp_cny&view=rsvpcnydata' : $item->link);
		$this->setRedirect(JRoute::_($url, false));
	}

	/**
	 * Method to remove data
	 *
	 * @return void
	 *
	 * @throws Exception
     *
     * @since 1.6
	 */
	public function remove()
    {
        $app   = JFactory::getApplication();
        $model = $this->getModel('RsvpcnydatumForm', 'Rsvp_cnyModel');
        $pk    = $app->input->getInt('id');

        // Attempt to save the data
        try
        {
            $return = $model->delete($pk);

            // Check in the profile
            $model->checkin($return);

            // Clear the profile id from the session.
            $app->setUserState('com_rsvp_cny.edit.rsvpcnydatum.id', null);

            $menu = $app->getMenu();
            $item = $menu->getActive();
            $url = (empty($item->link) ? 'index.php?option=com_rsvp_cny&view=rsvpcnydata' : $item->link);

            // Redirect to the list screen
            $this->setMessage(JText::_('COM_EXAMPLE_ITEM_DELETED_SUCCESSFULLY'));
            $this->setRedirect(JRoute::_($url, false));

            // Flush the data from the session.
            $app->setUserState('com_rsvp_cny.edit.rsvpcnydatum.data', null);
        }
        catch (Exception $e)
        {
            $errorType = ($e->getCode() == '404') ? 'error' : 'warning';
            $this->setMessage($e->getMessage(), $errorType);
            $this->setRedirect('index.php?option=com_rsvp_cny&view=rsvpcnydata');
        }
	}
	
	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   array     $data                  The data to send in the email.
	 * @param   stdClass  $contact               The user information to send the email to
	 * @param   boolean   $copy_email_activated  True to send a copy of the email to the user.
	 *
	 * @return  boolean  True on success sending the email, false on failure.
	 *
	 * @since   1.6.4
	 */
	private function _sendEmail($email="")
	{
		$jinput = JFactory::getApplication()->input;
		$tableHtmlStart = "<html>";
		$mContent = '<p>Hi, </p><p>You’re now in the running to get a GG Bond plush toy! <br />We will notify you via email if you’re among the first 500 who registered. </p><p>Here’s wishing you a happy and prosperous Chinese New Year! </p><p>From, <br />Team Astro Media Sales</p><p>(This is an auto-response email, please do not reply.)</p>';
		$tableHtmlEnd = "</html>";
		$body = $tableHtmlStart . $mContent .$tableHtmlEnd;

		$to = $email;
		$from = array("astroquake@quake.my", "Astro Media Sales");

		# Invoke JMail Class
		$mailer = JFactory::getMailer();

		# Set sender array so that my name will show up neatly in your inbox
		$mailer->setSender($from);

		# Add a recipient -- this can be a single address (string) or an array of addresses
		$mailer->addRecipient($to);

		$mailer->setSubject("Thank you, we’ve received your registration #BangDuDu2019");
		$mailer->setBody($body);
		$mailer->AltBody =JMailHelper::cleanText( strip_tags( $body));

		# If you would like to send as HTML, include this line; otherwise, leave it out
		$mailer->isHTML();

		return $mailer->send();
	}
}
