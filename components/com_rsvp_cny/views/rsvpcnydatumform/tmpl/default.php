<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_cny
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_rsvp_cny', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/media/com_rsvp_cny/js/form.js');
$styles = $headData['styles'];
$session = JFactory::getSession();

unset($styles['/templates/bootstrap4css/style.css']);

$user    = JFactory::getUser();
$canEdit = Rsvp_cnyHelpersRsvp_cny::canUserEdit($this->item, $user);


?>
<div class="flower-wrapper">
	<img class="img-1" src="../../images/rsvp/flower-1.png" alt="">
	<img class="img-2" src="../../images/rsvp/flower-2.png" alt="">
	<img class="img-3" src="../../images/rsvp/flower-3.png" alt="">
	<img class="img-4" src="../../images/rsvp/flower-4.png" alt="">
</div>
<div class="astro-mobile">
	<img src="../../../../../images/rsvp/astro.png" alt="">
</div>

<div class="logo-light"></div>

<div class="rsvpcnydatum-edit front-end-edit">
	<?php if (!$canEdit) : ?>
		<h3>
			<?php throw new Exception(JText::_('COM_RSVP_CNY_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
		</h3>
	<?php else : ?>
	<div class="container-fluid">
		
		<div class="row align-center">
			<div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
								<!-- <img class="img-fluid" src="../../../../../images/rsvp/logo_light.png" alt=""> -->
				<img class="img-fluid" src="../../../../../images/rsvp/logo.png" alt="">
			</div>
			<div class="col-sm-12 col-md-12 col-lg-5 col-xl-5">
				<div class="astro text-right">
					<img src="../../../../../images/rsvp/astro.png" alt="">
				</div>
				<div class="section-title">
					<h2 class="mb-15">Almost there!</h2>
					<h3 class="mb-35">Be the first 500 to register and bring home an exclusive GG Bond plush toy.</h3>
				</div>
				<div class="divider mb-35"></div>
				<div class="section-title">
					<h3 class="mb-15">Complete the registration form below <span>by 11.59pm, 16 Jan 2019</span></h3>
					<p class="mandatory mb-35">* All fields are mandatory</p>
				</div>
				
				<div>
					<form id="form-rsvpcnydatum"
						  action="<?php echo JRoute::_('index.php?option=com_rsvp_cny&task=rsvpcnydatum.save'); ?>"
						  method="post" class=" form-horizontal" enctype="multipart/form-data">
						
						<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

						<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

						<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

						<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />

						<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

							<?php echo $this->form->getInput('created_by'); ?>
							<?php echo $this->form->getInput('modified_by'); ?>
						<div class="row">
							<div class="form-title col-4 col-sm-4 col-md-3 col-lg-4 col-xl-5">
								<label>Title</label>
							</div>
							<div class="col-8 col-sm-8 col-md-9 col-lg-8 col-xl-7">
								<fieldset id="jform_title" class="radio">
									<div class="row">
										<div class="col-12">
											<div class="row">
												<div class="col-sm-12 col-md-6">
													<div class="custom-control custom-radio">
													  	<input type="radio" id="jform_title2" name="jform[title]" class="custom-control-input" value="2">
													  	<label class="custom-control-label" for="jform_title2">Datuk / Dato</label>
													</div>
												</div>
												<div class="col-sm-12 col-md-6">
													<div class="custom-control custom-radio">
													  	<input type="radio" id="jform_title3" name="jform[title]" class="custom-control-input" value="3">
													  	<label class="custom-control-label" for="jform_title3">Datin</label>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12 col-md-6">
													<div class="custom-control custom-radio">
													  	<input type="radio" id="jform_title0" name="jform[title]" class="custom-control-input" value="0" checked="checked">
													  	<label class="custom-control-label" for="jform_title0">Mr</label>
													</div>
												</div>
												<div class="col-sm-12 col-md-6">
													<div class="custom-control custom-radio">
													  	<input type="radio" id="jform_title1" name="jform[title]" class="custom-control-input" value="1">
													  	<label class="custom-control-label" for="jform_title1">Ms</label>
													</div>
												</div>
											</div>
										</div>
									</div>

								</fieldset>
							</div>
						</div>
						<div class="row">
							<div class="form-title col-4 col-sm-4 col-md-3 col-lg-4 col-xl-5">
								<p class="form-title-name">Name</p>
							</div>
							<div class="col-8 col-sm-8 col-md-9 col-lg-8 col-xl-7">
								<input type="text" name="jform[name]" id="jform_name" value="" class="required" required="" aria-required="true" autocomplete="off">
							</div>
						</div>
						<div class="row">
							<div class="form-title col-4 col-sm-4 col-md-3 col-lg-4 col-xl-5">
								<p class="form-title-company">Company Name</p>
							</div>
							<div class="col-8 col-sm-8 col-md-9 col-lg-8 col-xl-7">
								<input type="text" name="jform[company_name]" id="jform_company_name" value="" class="required" required="" aria-required="true" autocomplete="off">
							</div>
						</div>
						<div class="row">
							<div class="form-title col-4 col-sm-4 col-md-3 col-lg-4 col-xl-5">
								<p class="form-title-designation">Designation</p>
							</div>
							<div class="col-8 col-sm-8 col-md-9 col-lg-8 col-xl-7">
								<input type="text" name="jform[designation]" id="jform_designation" value="" class="required" required="" aria-required="true" autocomplete="off">
							</div>
						</div>
						<div class="row">
							<div class="form-title col-4 col-sm-4 col-md-3 col-lg-4 col-xl-5">
								<p class="form-title-email">Email Address</p>
							</div>
							<div class="col-8 col-sm-8 col-md-9 col-lg-8 col-xl-7">
								<div class="controls"><input type="email" name="jform[email]" class="validate-email required" id="jform_email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="" required="" aria-required="true" autocomplete="off"></div>
							</div>
						</div>
						<div class="row">
							<div class="form-title col-4 col-sm-4 col-md-3 col-lg-4 col-xl-5">
								<p class="form-title-mobile">Mobile Number</p>
							</div>
							<div class="col-8 col-sm-8 col-md-9 col-lg-8 col-xl-7">
								<input type="tel" name="jform[phone_number]" class="required" id="jform_phone_number" pattern="^[0-9-+s()]*$" value="" required="" aria-required="true" autocomplete="off">
							</div>
						</div>

						<div class="row">
							<div class="col-12">
								<div class="custom-control custom-checkbox">
								  	<input type="checkbox" class="custom-control-input" id="customCheck1" required>
								  	<label class="custom-control-label" for="customCheck1">I agree to all <a href="" data-toggle="modal" data-target="#tandc">Terms and Conditions</a> and the <a href="" data-toggle="modal" data-target="#exampleModal">Personal Data Protection Act</a>.</label>
								</div>

							</div>
						</div>
						<div class="row">
							<div class="col-sm-4 col-md-3 col-lg-4 col-xl-5">
								
							</div>
							<div class="mobile-center col-12 col-sm-8 col-md-9 col-lg-8 col-xl-7">
								<?php if ($this->canSave): ?>
									<button type="submit" class="validate btn btn-submit">
										Submit Your Entry
									</button>
								<?php endif; ?>
							</div>
						</div>

						<input type="hidden" name="option" value="com_rsvp_cny"/>
						<input type="hidden" name="task"
							   value="rsvpcnydatumform.save"/>

						<?php echo JHtml::_('form.token'); ?>

					</form>
				</div>

			</div>
		</div>
	</div>
	<?php endif; ?>
</div>

<div class="modal fade" id="tandc" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h6 class="modal-title" id="tandc">Privacy Statement</h6>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">
	        	<h6>1. Organiser</h6>
				<p>MEASAT BROADCAST NETWORK SYSTEMS SDN BHD</p>        
	
				<h6>2. Name of Contest</h6>
				<p>The CNY 2019 GG Bond Plush Toy Giveaway</p>        

				<h6>3. Brief Description of Contest/ Programme</h6>
				<p>The CNY 2019 GG Bond Plush Toy Giveaway is a contest to push Quake.my database registration by asking the participants to click on the link provided within the email. The toy giveaway will be based on the first 500 participants.</p>        
				
				<h6>4. Brief Description of Contest/ Programme</h6>
				<p>4.1 This Campaign begins on 3rd January 2019 and ends at 11.59pm on 16th January 2019 (“Campaign Period”).</p> 
				<p>4.2 This Campaign applies for one entry per person who receives the call-to-action email for registration.</p>
				<p>4.3 Entries on behalf of another person will not be accepted and joint submissions are not allowed.</p>
				<p>4.4 How to participate:</p>
				<p>&emsp;&ensp; Participants are required to click into the link provided within the email.</p>
				<p>&emsp;&ensp; Participants are required to register their details at <a href="http://www.quake.com.my/bangdudu2019">http://www.quake.com.my/bangdudu2019</a></p>

				<p>4.5 First 500 eligible registrants will be awarded 1 GG Bond Plush Toy each.</p>
				<p>4.6 The Organiser reserves the right to award the prize, of their choosing, to the first 500 eligible registrants. The Organiser reserves the right to substitute any of the prizes with items of equivalent value without prior notice.</p>

				<h6>5. Eligibility Criteria</h6>
				<p>The Campaign is open to Malaysian citizens aged 18 and above as of 3rd January 2019 who are in the Media, Marketing and Advertising industry only. Employees and immediate families of The Organiser, its related companies and agencies and appointed distributors are not eligible to participate in this Campaign. Participants under the age of 18 years old as of 3rd January 2019 must obtain consent of parents or legal guardians and by participating in this Campaign it is deemed that consent has been obtained.</p>

				<h6>6. Ineligibility</h6>
				<p>Employees of the Measat Broadcast Network Systems Sdn Bhd, Astro Entertainment Sdn Bhd, Astro Production Sdn Bhd, Digital Five Sdn Bhd and Astro All Asia Network plc and their immediate family members.</p>

				<h6>7. Age of Eligibility </h6>
				<p>a) The Contest is open to all Malaysians who are between 18 and above as of 3rd January 2019</p>
				<p>b) Where the Age of Eligibility permits participation of Contestants under the age of 18 years old, the Contestant must obtain the consent of his/her parent or legal guardian in order to be eligible to participate in the Contest and to receive the Prize. The Organiser considers it the responsibility of parents and/or guardian to monitor their children’s participation in this Contest.</p>

				<h6>8. Charges</h6>
				<p>The standard data charges charged by the Contestant’s relevant telecommunications service provider applies including charges for failed online submission.</p>

				<h6>9. Contest Period</h6>
				<p>The Contest shall be held from 3rd January 2019 to 16th January 2019 and will close at 11.59pm or such other time that the Organiser shall decide from time to time.</p>
				<p>The Organiser reserves the right to vary, postpone or re-schedule the Contest Period or any dates thereof at its sole discretion.</p>

				<h6>10. Language of Contest</h6>
				<p>The Contest will be organized by the Organiser in English. </p>

				<h6>11. Entry Procedure</h6>
				<p>11.1 Participants will receive one entry per person call-to-action email for registration.</p>
				<p>11.2 Entries on behalf of another person will not be accepted and joint submissions are not allowed.</p>
				<p>11.3 Participants are required to click into the link provided within the email.</p>
				<p>11.4 Participants are required to register their details at <a href="http://www.quake.com.my/bangdudu2019">http://www.quake.com.my/bangdudu2019</a></p>
				<p>11.5 The Organiser reserves the right to reject any participant failing to meet the requirement above.</p>

				<h6>12. Entry Deadline</h6>
				<p>Entries must be received by the Organiser on or before 11.59pm on 16th January 2019.</p>
				<p>Entries received before the commencement of the Contest Period and after the stipulated Entry Deadline will be disqualified and ineligible for consideration for prizes.</p>

				<h6>13. Mode</h6>
				<p>13.1 Participants are required to click into the link provided within the email.</p>
				<p>13.2 Participants are required to register their details at <a href="http://www.quake.com.my/bangdudu2019">http://www.quake.com.my/bangdudu2019</a></p>

				<h6>14. Address If to be sent through website</h6>
				<p><a href="http://www.quake.com.my/bangdudu2019">http://www.quake.com.my/bangdudu2019</a></p>

				<h6>15. Selection of Winners</h6>
				<p>Contest winner will be chosen based on first 500 eligible registrants who registered at <a href="http://www.quake.com.my/bangdudu2019">http://www.quake.com.my/bangdudu2019</a></p>

				<h6>16. Prize</h6>
				<p>GG Bond Plush Toy</p>

				<h6>17. Notification of winners</h6>
				<p>Winner(s) will be announced via email within the next three (3) working days from the end of the Campaign Period.</p>

				<h6>18. Collection Period</h6>
				<p>22nd – 31st January 2019</p>

				<h6>19. Collection Venue</h6>
				<p>Media sales team will deliver to the winners.</p>

				<h6>20. Additional Terms, if any</h6>
				<p>Winner(s) must present their proof of identification as provided on the details shared on <a href="http://www.quake.com.my/bangdudu2019">http://www.quake.com.my/bangdudu2019</a></p>

				<hr>

				<p>The Basic Terms and the Contest Standard Terms and Conditions (collectively “Terms and Conditions”) shall be binding on all contestants who participate in this Contest (“Contestants”). The definitions in the Contest Standard Terms and Conditions shall apply unless otherwise expressly stated in the Basic Terms. In the event of any inconsistency between the Basic Contest Terms and the Contest Standard Terms and Conditions, the Basic Terms shall prevail to the extent of such inconsistency.</p>

				<p>Entry and participation in the Contest shall be deemed an unconditional acceptance by the Contestants of the Terms and Conditions.</p>
				<p>find out more <a href="http://quake.com.my/bangdudu2019/terms-and-conditions">here</a></p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-submit" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h6 class="modal-title" id="exampleModalLabel">PDPA Policy</h6>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">
        		<p>The Personal Data Protection Act 2010 was introduced by the Government to regulate the processing of personal data in commercial transactions. The Act, which applies to all companies and firms that are established in Malaysia, requires us to inform you of your rights in respect of your personal data that is being processed or that is to be collected and further processed by us and the purposes for the data processing. The Act also requires us to obtain your consent to the processing of your personal data.</p>

        		<p>Consequently, please be informed that the personal data and other information (collectively, “Personal Data”) provided in your application to register for the use of the Astro Radar Website and, if relevant, for subscription to Astro Radar Services may be used and processed by Astro Malaysia Holdings Sdn Bhd (“AMH”) for the following purposes:-</p>

        		<ul>
        			<li>assessing your application to register for the use of the Astro Radar Website</li>
        			<li>assessing your application for subscription</li>
        			<li>to communicate with you;</li>
        			<li>to provide services to you;</li>
        			<li>to process your payment transactions;</li>
        			<li>respond to your inquiries;</li>
        			<li>administer your participation in contests;</li>
        			<li>conduct internal activities;</li>
        			<li>market surveys and trend analysis;</li>
        			<li>to provide you with information on products and services of AMH and our related corporations;</li>
        			<li>to provide you with information on products and services of our business partners;</li>
        			<li>other legitimate business activities of AMH;</li>
        			<li>such other purposes as set out in the Astro Radar Website Terms of Use; and/or</li>
        			<li>if relevant, such other purposes as set out in the General Terms and Conditions and, if applicable, Campaign Terms and Conditions.</li>
        		</ul>

        		<p>(collectively “Purposes”).</p>

        		<p>Further, please be informed that if required for any of the foregoing Purposes, your Personal Data may be transferred to locations outside Malaysia or disclosed to our related corporations, licensees, business partners and/or service providers, who may be located within or outside Malaysia. Save for the foregoing, your Personal Data will not be knowingly transferred to any place outside Malaysia or be knowingly disclosed to any third party.</p>

        		<p>In order to process your Personal Data, your consent is required.  If you do not consent to the processing of your Personal Data other than in relation to the advertising or marketing of any product or service of AMH or our business partners, we cannot process your Personal Data for any of the above Purposes and we will not be able to approve your application for registration for use of the Astro Radar website.</p>

        		<p>In relation to direct marketing, you may request AMH by written notice (in accordance with the following paragraph) not to process your Personal Data for any of the following Purposes: (i) advertising or marketing via phone any product or service of AMH or our business partners, (ii) sending to you via post any advertising or marketing material relating to any product or service of AMH or our business partners; (iii) sending to you via email or SMS any advertising or marketing material relating to any product or service of AMH or our business partners, or (iv) communicating to you by whatever means any advertising or marketing material of AMH or our business partners.</p>

        		<p>You may at any time hereafter make inquiries, complaints and request for access to, or correction of, your Personal Data or limit the processing of your Personal Data by submitting such request to the Personal Data Protection Officer of AMH via registered post or email as set out below.</p>

        		<p><strong>Postal address:</strong></p>

        		<p>Personal Data Protection Officer,
        			<br>
					Astro Malaysia Holdings Sdn Bhd,
					<br>
					Peti Surat 10335,
					<br>
					50710 Kuala Lumpur
				</p>

				<p><strong>Email address:</strong>&nbsp;<a href="mailto:pdpo@astro.com.my">pdpo@astro.com.my</a></p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-submit" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<img class="cloud cloud-left img-fluid" src="../../../../../images/rsvp/footer-left.png" alt="">
<img class="cloud cloud-right img-fluid" src="../../../../../images/rsvp/footer-right.png" alt="">
<?php if($session->get('registerStatus') == 'error'): ?>
<script>
	swal({
        title: "Opps!",
        text: "Something's wrong, please try again later",
        icon: "error",
        button: "Go Back",
    });
	
</script>
<?php $session->set('registerStatus', "");  ?>
<?php elseif($session->get('registerStatus') == 'success'): ?>

<script>
	var span = document.createElement("span");
	span.innerHTML = "You’ve successfully registered! <br /> We will be sending you an email notification if you’re among the first 500 who registered.";
	swal({
      title: "Thank you!",
      content: span,
      icon: "success",
      button: "Close",
    }).then(function(value){
        location.reload();
    });
</script>
<?php $session->set('registerStatus', ""); ?>
<?php elseif($session->get('registerStatus') == 'duplicated'): ?>

<script>
	var span = document.createElement("span");
	span.innerHTML = "Your email is already registered. <br> Please use a different email address";
	swal({
      	title: "Sorry.",
      	content: span,
      	icon: "info",
      	button: "Go Back",
    }).then(function(value){
        location.reload();
    });
</script>
<?php $session->set('registerStatus', ""); ?>
<?php endif;?>
<script>
	jQuery( document ).ready(function() {
		jQuery('.content .container').addClass('container-fluid');
		jQuery('.content .container').removeClass('container');
		jQuery('body').addClass('cny-body');
		jQuery(".btm-footer").hide();
		document.title = 'bangdudu2019';
	});
</script>