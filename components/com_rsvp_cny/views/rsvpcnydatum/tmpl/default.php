<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_cny
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_rsvp_cny');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_rsvp_cny'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_RSVP_CNY_FORM_LBL_RSVPCNYDATUM_COMPANY_NAME'); ?></th>
			<td><?php echo $this->item->company_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_CNY_FORM_LBL_RSVPCNYDATUM_TITLE'); ?></th>
			<td><?php echo $this->item->title; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_CNY_FORM_LBL_RSVPCNYDATUM_NAME'); ?></th>
			<td><?php echo $this->item->name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_CNY_FORM_LBL_RSVPCNYDATUM_DESIGNATION'); ?></th>
			<td><?php echo $this->item->designation; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_CNY_FORM_LBL_RSVPCNYDATUM_EMAIL'); ?></th>
			<td><?php echo $this->item->email; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_CNY_FORM_LBL_RSVPCNYDATUM_PHONE_NUMBER'); ?></th>
			<td><?php echo $this->item->phone_number; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_RSVP_CNY_FORM_LBL_RSVPCNYDATUM_SELECTED'); ?></th>
			<td><?php echo $this->item->selected; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_rsvp_cny&task=rsvpcnydatum.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_RSVP_CNY_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_rsvp_cny.rsvpcnydatum.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_RSVP_CNY_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_RSVP_CNY_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_RSVP_CNY_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_rsvp_cny&task=rsvpcnydatum.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_RSVP_CNY_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>