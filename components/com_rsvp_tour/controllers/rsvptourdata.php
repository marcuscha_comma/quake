<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_tour
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
/**
 * Rsvptourdata list controller class.
 *
 * @since  1.6
 */
class Rsvp_tourControllerRsvptourdata extends Rsvp_tourController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Rsvptourdata', $prefix = 'Rsvp_tourModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	public function fillFrom(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		
		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jdate = new JDate;

		$jinput = JFactory::getApplication()->input;

		$email = $jinput->get('email','', 'String');
		$name = $jinput->get('name','', 'String');
		$comp_name = $jinput->get('comp_name','', 'String');
		$designation = $jinput->get('designation','', 'String');
		$mobileNo = $jinput->get('mobileNo','', 'String');

		$db->setQuery('SELECT id FROM #__users where email = "'.$email.'" and quakeClubUser = 0');
		$checkEmailExist             = $db->loadResult();


		if ($checkEmailExist) {
			echo '0-';
		}else{
			$db->setQuery('SELECT id FROM #__users where email = "'.$email.'" and (quakeClubUser = -1 or quakeClubUser = 1)');
			$checkEmailExistNotMember             = $db->loadResult();

			$db->setQuery('SELECT id FROM #__cus_rsvp_tour_data where user_id = "'.$checkEmailExistNotMember.'"');
			$checkBigBigTourList             = $db->loadResult();
			

			if ($checkEmailExistNotMember) {

				if ($checkBigBigTourList) {
					echo '2-';
					echo $name;
				}else{
	
					$db->setQuery('SELECT MAX(ordering) FROM #__cus_rsvp_tour_data');
					$max             = $db->loadResult();
	
					$redemption = new stdClass();
					$redemption->user_id = $checkEmailExistNotMember;
					$redemption->redemption_status = 0;
					$redemption->is_attended = 0;
					$redemption->is_deleted = 0;
					$redemption->is_valid = 1;
					$redemption->state=1;
					$redemption->designation = $designation;
					$redemption->mobileNo = $mobileNo;
					$redemption->email=$email;
					$redemption->name= $name;
					$redemption->comp_name = $comp_name;
					$redemption->created_by=$checkEmailExistNotMember;
					$redemption->modified_by=$checkEmailExistNotMember;
					$redemption->ordering=$max + 1;
					$redemption->updated_at = $jdate->toSql(true);
	
					// Insert the object into the user profile table.
					$result = JFactory::getDbo()->insertObject('#__cus_rsvp_tour_data', $redemption);
					echo '3-';
					echo $name;
				}
			}else{

				$user = new stdClass();
				$user->name= $name;
				$user->username = "user_".time();
				$user->email=$email;
				$user->block=0;
				$user->activation="";
				$user->params="{}";
				$user->otep="";
				$user->mobileNo = $mobileNo;
				$user->source = 7;
				$user->quakeClubUser = -1;

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__users', $user);
				$user_id = JFactory::getDbo()->insertID();

				$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_company_user');
				$max             = $db->loadResult();

				$company = new stdClass();
				$company->user_id = $user_id;
				$company->name = $comp_name;
				$company->designation = $designation;
				$company->mobile_no = $mobileNo;
				$company->email = $email;
				$company->state=1;
				$company->created_by=$user_id;
				$company->modified_by=$user_id;
				$company->ordering=$max + 1;

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__cus_qperks_company_user', $company);

				$db->setQuery('SELECT MAX(ordering) FROM #__cus_rsvp_tour_data');
				$max             = $db->loadResult();

				$redemption = new stdClass();
				$redemption->user_id = $user_id;
				$redemption->redemption_status = 0;
				$redemption->is_attended = 0;
				$redemption->is_deleted = 0;
				$redemption->is_valid = 1;
				$redemption->state=1;
				$redemption->created_by=$user_id;
				$redemption->modified_by=$user_id;
				$redemption->ordering=$max + 1;
				$redemption->updated_at = $jdate->toSql(true);

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__cus_rsvp_tour_data', $redemption);
				echo '3-';
				echo $name;
			}
		}

		JFactory::getApplication()->close();
	}

	public function login(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		
		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jdate = new JDate;

		$jinput = JFactory::getApplication()->input;
		$email = $jinput->get('email','', 'String');
		$password = $jinput->get('password','', 'String');
		// $email = "midoff1@gmail.com";
		// $password = "comma5157";
		$db->setQuery('SELECT id,password,name FROM #__users where email ="'.$email .'"');
		$user             = $db->loadRow();
		
		$passwordMatch = JUserHelper::verifyPassword($password, $user[1], $user[0]);

		if ($passwordMatch) {

			$db->setQuery('SELECT id FROM #__cus_rsvp_tour_data where user_id = "'.$user[0].'"');
			$checkBigBigTourList             = $db->loadResult();
			if ($checkBigBigTourList  ) {
				echo '2-';
				echo $user[2];
			}else{
				$db->setQuery('SELECT MAX(ordering) FROM #__cus_rsvp_tour_data');
				$max             = $db->loadResult();

				$redemption = new stdClass();
				$redemption->user_id = $user[0];
				$redemption->redemption_status = 0;
				$redemption->is_attended = 0;
				$redemption->is_deleted = 0;
				$redemption->is_valid = 1;
				$redemption->state=1;
				$redemption->created_by=$user[0];
				$redemption->modified_by=$user[0];
				$redemption->ordering=$max + 1;
				$redemption->updated_at = $jdate->toSql(true);

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__cus_rsvp_tour_data', $redemption);
				// echo $user[2];
				echo '3-';
				echo $user[2];
			}

		}else{
			echo '0-';
		}
		JFactory::getApplication()->close();
	}

	public function addPoint(){
		JFactory::getDocument()->setMimeEncoding( 'application/json' );
		JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		
		$app = JFactory::getApplication();
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$jdate = new JDate;

		$jinput = JFactory::getApplication()->input;
		$id = $jinput->get('id','', 'String');
		$status = $jinput->get('status','', 'String');
		
		if ($status == 'true') {
			
			$db->setQuery('SELECT MAX(ordering) FROM #__cus_qperks_user_point');
			$max             = $db->loadResult();

			$profile1 = new stdClass();
			$profile1->user_id = $id;
			$profile1->point=80;
			$profile1->type=9;
			$profile1->state=1;
			$profile1->source="Bigbigshow2019";
			$profile1->created_by=$id;
			$profile1->created_on=$jdate->toSql(true);
			$profile1->modified_by=$id;
			$profile1->ordering=$max + 1;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__cus_qperks_user_point', $profile1);

			$month = 'month'.date('m');
			$db->setQuery('SELECT id FROM #__cus_quake_club_qperks_monthly where user_id = '. $id .' AND year ="'.date('Y').'"' );
			$monthly_id             = $db->loadResult();
			if (!$monthly_id) {

				$db->setQuery('SELECT MAX(ordering) FROM #__cus_quake_club_qperks_monthly');
				$max             = $db->loadResult();
				$month = 'month'.date('m');
				$monthly = new stdClass();
				$monthly->user_id = $id;
				$monthly->$month = 80;
				$monthly->year = date('Y');
				$monthly->state=1;
				$monthly->created_by=$id;
				$monthly->modified_by=$id;
				$monthly->ordering=$max + 1;

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->insertObject('#__cus_quake_club_qperks_monthly', $monthly);
			}else{
				$db->setQuery('SELECT '.$month.' FROM #__cus_quake_club_qperks_monthly where user_id = '. $id .' AND year ="'.date('Y').'"' );
				$monthly_point            = $db->loadResult();

				$monthly = new stdClass();
				$monthly->$month = $monthly_point + 80;
				$monthly->id = $monthly_id;

				// Insert the object into the user profile table.
				$result = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');
			}
			
		}else{
			
			$db->setQuery('SELECT id FROM #__cus_qperks_user_point where user_id ='.$id.' and type=9');
			$point_id             = $db->loadResult();

			$conditions = array(
				$db->quoteName('id') . ' = '. $point_id
			);

			$query->delete($db->quoteName('#__cus_qperks_user_point'));
			$query->where($conditions);

			$db->setQuery($query);

			$result = $db->execute();

			$month = 'month'.date('m');
			$db->setQuery('SELECT id FROM #__cus_quake_club_qperks_monthly where user_id = '. $id .' AND year ="'.date('Y').'"' );
			$monthly_id             = $db->loadResult();
			$db->setQuery('SELECT '.$month.' FROM #__cus_quake_club_qperks_monthly where user_id = '. $id .' AND year ="'.date('Y').'"' );
			$monthly_point            = $db->loadResult();

			$monthly = new stdClass();
			$monthly->$month = $monthly_point - 80;
			$monthly->id = $monthly_id;

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->updateObject('#__cus_quake_club_qperks_monthly', $monthly, 'id');
		}

		JFactory::getApplication()->close();

	}

	public function getWebinar(){
		// JFactory::getDocument()->setMimeEncoding( 'application/json' );
		// JResponse::setHeader('Content-Disposition','attachment;filename="progress-report-results.json"');
		
		$app = JFactory::getApplication();
		$user       = Factory::getUser();
		$userId     = $user->get('id');
		$userName     = $user->get('name');
		$userEmail     = $user->get('email');
		
		if ($userId) {
			$post = [
				"api_key" => "924579a7-73a3-4241-8efc-95b7a0847088",
				"webinar_id" => 18,
				"first_name" => $userName,
				"email" => $userEmail,
				"schedule" => 18
			];
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, 'https://api.webinarjam.com/webinarjam/register');
			$result = curl_exec($ch);
			curl_close($ch);

			echo $result;
		}else{
			echo '0-';
		}
		JFactory::getApplication()->close();
	}
}
