<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_tour
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$fillFormUrl = JRoute::_('index.php?option=com_rsvp_tour&task=rsvptourdata.fillFrom');
$loginUrl = JRoute::_('index.php?option=com_rsvp_tour&task=rsvptourdata.login');
$getWebinar = JRoute::_('index.php?option=com_rsvp_tour&task=rsvptourdata.getWebinar');

// $ch = curl_init();
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// curl_setopt($ch, CURLOPT_URL, 'https://api.webinarjam.com/webinarjam/webinars?api_key=924579a7-73a3-4241-8efc-95b7a0847088');
// $result = curl_exec($ch);
// curl_close($ch);

// $obj = $result;

?>
<style>
	body {
		padding-top: 0;
		padding-bottom: 100px;
		background: black url(./images/big2019/bg.jpg) no-repeat top center / 1920px;
		color: white;
		font-family: "FS Albert Pro", sans-serif;
	}
	.rsvp-header {
		position: relative;
		padding-top: 100px;
		margin-bottom: 50px;
	}
	.big-logo {
		margin: 0 auto;
		display: block;
		max-width: 100%;
	}
	.astro-logo {
		position: absolute;
		right: 0;
		top: 40px;
	}
	h1 {
		font-size: 40px;
	}
	h2 {
		font-size: 22px;
	}
	.black-box {
		background-color: rgba(0, 0, 0, 0.6);
		padding: 25px;
	}
	.text-blue {color:#00ADEE;}
	.event-info-box {
		padding: 0 30px;
		height: 100%;
	}
	div:not(.last) > .event-info-box {
		border-right: 1px solid #00ADEE;
	}
	.pink-neon {
		color: #ED01A0;
		font-size: 35px;
		text-shadow: 0 0 7px rgba(224,46,146, 0.8), 0 0 20px rgba(224,46,146, 0.8);
	}
	.blue-neon {
		color: #00ADEE;
		font-size: 35px;
		text-shadow: 0 0 7px #00ADEE, 0 0 20px rgba(0,173,238,0.5);
	}
	.agenda-list .row {
		padding: 15px 0;
	}
	.agenda-list .row:not(:last-child):not(.agenda-header) {
		border-bottom: 1px solid #454545;
	}
	.agenda-header.row {
		margin-top: 20px;
		border-top: 3px solid #454545;
		border-bottom: 3px solid #454545;
		padding: 20px 0;
	}
	.agenda-header h5 { margin: 0;}
	.time-col {
		color: #00ADEE;
		font-weight: bold;
		text-transform: uppercase;
	}
	.agenda-title {
		font-weight: bold;
		margin-top: 5px;
	}
	.mobile-title {
		display: none;
	}
	.agenda-desc {
		margin-top: 10px;
		color: #B5B5B5;
	}
	.agenda-desc .short-content {
		transition: all 0.6s;
		max-height: 25px;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
	.agenda-desc.expand .short-content {
		max-height: 1000px;
		text-overflow: inherit;
		white-space: normal;
		overflow: visible;
	}
	.toggle-agenda {
		color: #ED01A0;
		font-size: 14px;
		display: inline-block;
		cursor: pointer;
		padding: 10px 10px 10px 0;
	}
	.toggle-agenda i {
		margin-right: 5px;
		font-size: 10px;
	}
	.agenda-desc .hide {
		display: none;
	}
	.agenda-desc.expand .hide {
		display: block;
	}
	.agenda-desc.expand .collapsed {
		display: none;
	}
	.float-register {
		background-color: #ED01A0;
		color: white;
		position: fixed;
		bottom: 0;
		left: 0;
		right: 0;
		padding: 10px;
		display: flex;
		align-items: center;
		font-size: 20px;
		cursor: pointer;
		text-align: center;
		transition: all .3s;
	}
	.float-register .btn {
		margin-left: 10px;
		box-shadow: none;
		font-size: 20px;
		padding: 10px 20px;
	}
	#downloadCenterModal .modal-body,
	.custom-modal .modal-body,
	.custom-modal .modal-header {
		background: #171717;
		border-color: transparent;
	}
	.modal {
		overflow-y: auto;
	}
	.icon-done {
		width: 167px;
		margin: 20px auto 40px;
	}
	.pink-block {
		background-color: #ED01A0;
		padding: 30px;
		margin: 30px -30px -30px;
		text-align: center;
	}
	.club-form {
		color: black;
	}
	.form-control[readonly] {
    background-color: white;
	}
	.sticker {
		background-color: #ED01A0;
		border-radius: 50%;
		text-align: center;
		color: #ffffff;
		font-size: 14px;
		font-weight: bold;
		padding: 10px;
		display: flex;
		align-items: center;
	}
	.modal.dimmed {
		opacity: 0.3;
	}
	#pdpa {
		font-size: 14px;
	}
	@media (min-width: 992px) {
		.float-register:hover {
			padding: 20px 10px;
		}
		.pink-border {
			border-right:1px solid #ED01A0;
		}
	}
	@media (min-width: 768px) {
		#sticky {
			margin-right: -30px;
		}
		.sticker {
			position: absolute;
			right: -30px;
			top: -30px;
			width: 120px;
			height: 120px;
		}
	}
	@media (max-width: 991px) {
		.pink-border {
			border-bottom:1px solid #ED01A0;
			margin-bottom: 20px;
			padding-bottom: 20px;
		}
		.mobile-title {
			display: block;
			color: #00ADEE;
			/* font-weight: bold; */
			margin-bottom: 5px;
			font-style: italic;
		}
		.agenda-col {margin-top: 10px;}
		.speaker-col {
			padding-top: 15px;
		}
		.agenda-col, .speaker-col {
			padding-left: 35px;
		}
		.agenda-col:before, .speaker-col:before {
			content: "";
			position: absolute;
			left: 15px;
			top: 0;
			bottom: 0;
			width: 1px;
			background-color: #454545;
		}
		.agenda-list .row {
			padding: 30px 0;
		}
		.float-register .btn {
			padding: 3px 15px;
		}
		div:not(.last) > .event-info-box {
			border-right: 0;
			border-bottom: 1px solid #00ADEE;
		}
		.event-info-box {
			padding: 30px 0;
		}
	}
	@media (max-width: 767px) {
		.sticker {
			width: 160px;
			height: 160px;
			margin: 0 auto 50px;
		}
		.float-register {
			font-size: 16px;
		}
		.float-register .btn {
			margin: 10px auto 0;
			display: block;
			padding: 0px 15px;
			font-size: 16px;
		}
		#sticky {
			padding: 5px 10px;
			margin: 0 -30px 20px auto;
			word-break: normal;
			white-space: normal;
			text-align: right;
		}
	}
	@media (max-width: 575px) {
		.astro-logo {max-width: 70px}
		.black-box {
			padding: 25px 15px;
		}
		h1 {
			font-size: 34px;
		}
		.rsvp-header h2 {
			font-size: 15px;
		}
		.pink-neon, .blue-neon {
			font-size: 28px;
		}
		.black-box h2:not(.pink-neon):not(.blue-neon) {
			font-size: 18px;
		}
		.modal .h2 {
			font-size: 26px;
		}
		#sticky {
			font-size: 14px;
		}
		#downloadCenterModal .modal-body, .custom-modal .modal-body {
			padding: 10px;
		}
		#sticky {
			margin-right: -10px;
		}
		#pdpa {
			font-size: 12px;
		}
		#event-highlights .f-20 {
			font-size: 17px;
		}
		#event-highlights {
			font-size: 14px;
		}
	}
</style>
<div id="app">
	<div class="rsvp-header container text-center">
		<img class="astro-logo" src="./images/big2019/astro.svg" alt="">

		<img class="big-logo" src="./images/big2019/logo.png" alt="Big Big">

		<h1 class="mt-4 text-uppercase">Win Or Lose With Content</h1>
		<h2>The Magnetic Pulse to Hearts & Minds</h2>
	</div>

	<div class="container">
		<div class="black-box text-center mb-5">
			<h2 class="font-weight-normal mb-4">Embark with us on a BIG BIG <span class="d-inline-block">content marketing journey</span></h2>

			<div class="row justify-content-center align-item-center">
				<div class="col-lg-3">
					<div class="event-info-box">
						<h3 class="text-blue f-20">VENUE:</h3>
						<b>Ballroom 2, <br />
							Sheraton, Petaling Jaya</b>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="event-info-box">
						<h3 class="text-blue f-20">DATE:</h3>
						<b>31 July 2019</b><br />
						(Wednesday)
					</div>
				</div>
				<div class="col-lg-4 last">
					<div class="event-info-box">
						<h3 class="text-blue f-20">TIME:</h3>
						<b>12:00pm – 2:00pm</b> (Lunch)<br />
						<b>2:00pm – 4:30pm</b> (Sharing Session)
					</div>
				</div>
			</div>
		</div>

		<div class="black-box position-relative" id="event-highlights">
			<h2 class="pink-neon text-uppercase text-center mt-4 mb-5">Event Highlight</h2>

			<!-- <div class="sticker">
				Special appearance by International celebrities
			</div> -->

			<!-- <div class="row text-center">
				<div class="col-lg-4 pink-border h-100">
					<h6 class="text-uppercase mb-4">Opening Keynote</h6>
					<img src="./images/big2019/DatoKhairul.png" alt="Dato’ Khairul Anwar Salleh" class="img-fluid">
					<h5 class="f-20 text-highlight text-uppercase mt-4">Dato’ Khairul Anwar Salleh</h5>
					<p class="text-uppercase mb-0">VP, Chief of Malay & Nusantara Business</p>
				</div>

				<div class="col-lg-8 ">
					<h6 class="text-uppercase mb-4">Special Sharing Session by Top
							Malaysian Blockbuster Directors:</h6>

					<div class="row">
						<div class="col-6">
							<div class="row align-items-center no-gutters">
								<div class="col-md-6 text-lg-right">
									<img src="./images/big2019/Adrian.png" alt="ADRIAN TEH" class="img-fluid">
								</div>
								<div class="col-md-6 mt-4 mt-lg-0 pr-xl-4">
									<h5 class="f-20 text-highlight text-uppercase">ADRIAN TEH</h5>
									<p class="text-uppercase mb-0">Director of <br />Paskal</p>
								</div>
							</div>
						</div>

						<div class="col-6">
							<div class="row align-items-center no-gutters">
								<div class="col-md-6 text-lg-right">
									<img src="./images/big2019/Joel.png" alt="JOEL SOH" class="img-fluid">
								</div>
								<div class="col-md-6 mt-4 mt-lg-0 pr-xl-4">
									<h5 class="f-20 text-highlight text-uppercase">JOEL SOH</h5>
									<p class="text-uppercase mb-0">Director of <br />Polis Evo 2</p>
								</div>
							</div>
						</div>
					</div>

					<h6 class="text-uppercase mb-4 mt-5">Special Appearance by Secret Guests <span class="d-inline-block">from TVB</span>:</h6>
					<div class="row">
						<div class="col-6 text-center text-md-right">
							<img src="./images/big2019/secret1.png" alt="Secret Guests from TVB" class="img-fluid">
						</div>
						<div class="col-6 text-center text-md-left">
							<img src="./images/big2019/secret2.png" alt="Secret Guests from TVB" class="img-fluid">
						</div>
					</div>
				</div>
			</div> -->

			<div class="row text-center">
				<div class="col-lg-4 pink-border">
					<h6 class="text-uppercase">Opening Keynote</h6>
				</div>
				<div class="col-lg-8 d-lg-block d-none">
					<h6><span class="text-uppercase">Special Sharing Session by Top
							Malaysian Blockbuster Directors:</span></h6>
				</div>
			</div>
			<div class="row text-center">
				<div class="col-lg-4 pink-border mobile">
					<div class="row mt-4 align-items-center">
						<div class="col-xl-12">
							<img src="./images/big2019/DatoKhairul2.png" alt="Dato’ Khairul Anwar Salleh" class="img-fluid">
						</div>
						<div class="col-xl-12 mt-4 mt-xl-0">
							<h5 class="f-20 text-highlight text-uppercase mt-4">Dato’ Khairul Anwar Salleh</h5>
							<p class="text-uppercase mb-0">VP, Chief of Malay & <br />Nusantara Business</p>
						</div>
					</div>
				</div>
				<div class="col-12 d-block d-lg-none">
					<h6><span class="text-uppercase">Special Sharing Session by</span> <em
							class="font-weight-normal d-block d-md-inline-block">Top Malaysian Blockbuster Directors:</em></h6>
				</div>
				<div class="col-lg-4 col-md-6 mb-3 mb-lg-0 ">

					<div class="row mt-4 align-items-center">
						<div class="col-xl-12">
							<img src="./images/big2019/Adrian2.png" alt="ADRIAN TEH" class="img-fluid">
						</div>
						<div class="col-xl-12 mt-4 mt-xl-0">
							<h5 class="f-20 text-highlight text-uppercase mt-4">ADRIAN TEH</h5>
							<p class="text-uppercase mb-0">Director of Paskal</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="row mt-4 align-items-center">
						<div class="col-xl-12">
							<img src="./images/big2019/Joel2.png" alt="JOEL SOH" class="img-fluid">
						</div>
						<div class="col-xl-12 mt-4 mt-xl-0">
							<h5 class="f-20 text-highlight text-uppercase mt-4">JOEL SOH</h5>
							<p class="text-uppercase mb-0">Director of Polis Evo 2</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="agenda">
			<h2 class="text-center mt-5 mb-3">
				<span class="blue-neon text-uppercase">Event Agenda</span>
			</h2>

			<div class="agenda-list">
				<div class="row agenda-header text-blue d-none d-lg-flex">
					<div class="col-lg-2 time-col">
						<h5>TIME</h5>
					</div>
					<div class="col-lg-7 agenda-col">
						<h5>PROGRAMME</h5>
					</div>
					<div class="col-lg-3 speaker-col">
						<h5>SPEAKERS</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						12:00PM – 2:00PM
					</div>
					<div class="col-lg-7 agenda-col">
						Registration & Networking Lunch
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						2:00PM – 2:20PM
					</div>
					<div class="col-lg-7 agenda-col">
						Welcome Keynote Speech
						<div class="agenda-title">
							THE POWER OF CONTENT
						</div>
					</div>
					<div class="col-lg-3 speaker-col">
						<div class="mobile-title">
							SPEAKER
						</div>
						<b>Dato’ Khairul Anwar Salleh</b> <em>– VP, Chief of Malay & Nusantara Business</em>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						2:20pm – 2:30pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Sharing Session #1</em>
						<div class="agenda-title">
							GROW YOUR BRAND ON THE BIG STAGE<br />
							The credibility breakthrough to social media stardom
						</div>
						<!-- <div class="agenda-desc">
							<div class=" short-content">
								Recorded box office numbers prove there’s a phenomenal interest in Malaysian flicks, but where are the
								advertisers for this highly sought-after space? Listen to the directors to discover golden advertising
								opportunities, from scripting, character building, content customisation, on-ground activation to
								adaptive marketing. Turn your brand into a blockbuster sensation!
							</div>

							<div class="toggle-agenda">
								<div class="collapsed">
									<i class="fas fa-plus"></i> Show More
								</div>
								<div class="hide">
									<i class="fas fa-minus"></i> Hide
								</div>
							</div>
						</div> -->
					</div>
					<div class="col-lg-3 speaker-col">
						<div class="mobile-title">
							SPEAKER
						</div>
						<b>Dato’ Khairul Anwar Salleh</b> <em>– VP, Chief of Malay & Nusantara Business</em>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						2:30pm – 2:40pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Sharing Session #2</em>
						<div class="agenda-title">
							REAL STORIES SELL<br />
							Generate brand love through real people with unscripted stories
						</div>
						<!-- <div class="agenda-desc">
							<div class=" short-content">
								Forget models and perfect human imagery, brands break convention by getting real people from Thinker
								Studios to talk about their products in the most authentic way… unscripted with no editorial control.
								And that’s how Herbal Essences won the hearts and minds of millennials and became the fastest growing
								brand in Malaysia.
							</div>

							<div class="toggle-agenda">
								<div class="collapsed">
									<i class="fas fa-plus"></i> Show More
								</div>
								<div class="hide">
									<i class="fas fa-minus"></i> Hide
								</div>
							</div>
						</div> -->
					</div>
					<div class="col-lg-3 speaker-col">
						<div class="mobile-title">
							SPEAKER
						</div>
						<b>Adly Ramly</b> <em>– Head of Thinker Studios</em>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						2:40pm – 2:55pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Sharing Session #3</em>
						<div class="agenda-title">
							BE ON THE NO.1 ENTERTAINMENT SHOW – MELETOP<br />
							Entertainment content to unleash branding power
						</div>
						<!-- <div class="agenda-desc">
							<div class=" short-content">
								Big star power, entertaining, quirky & unexpected brand integration, plus extensive touchpoints beyond
								TV have turned Meletop into the most sought-after content by brands. Entertainment Marketing is getting
								hotter than ever, simply because it… entertains!
							</div>

							<div class="toggle-agenda">
								<div class="collapsed">
									<i class="fas fa-plus"></i> Show More
								</div>
								<div class="hide">
									<i class="fas fa-minus"></i> Hide
								</div>
							</div>
						</div> -->
					</div>
					<div class="col-lg-3 speaker-col">
						<div class="mobile-title">
							SPEAKER
						</div>
						<b>Nabil Ahmad</b> <em>– The Best Entertainment Host (Asian Academy Creative Awards 2018 @ Cannes)</em>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						2:55pm – 3:05pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Interactive Game Session #1</em>
						<div class="agenda-title">Punch with BIGBIG Impact</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						3:05pm – 3:15pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Sharing Session #4</em>
						<div class="agenda-title">
							MAKING WAVES WITH BRANDED CONTENT<br />
							Reimagining radio to ignite connection with audience
						</div>
						<!-- <div class="agenda-desc">
							<div class=" short-content">
								For over 20 years, Malaysians rise and shine with Astro Radio. And it is still relevant today, evolving
								from just a platform for interaction into focusing on brand journey, creating emotional connections and
								driving discoveries. Don’t miss talks on “How We Create Content Today” to learn more about IJM Land Web
								Drama Case Study & “What Radio Looks Like Today” to learn more about SYOK, Malaysia’s free lifestyle and
								entertainment application.
							</div>

							<div class="toggle-agenda">
								<div class="collapsed">
									<i class="fas fa-plus"></i> Show More
								</div>
								<div class="hide">
									<i class="fas fa-minus"></i> Hide
								</div>
							</div>
						</div> -->
					</div>
					<div class="col-lg-3 speaker-col">
						<div class="mobile-title">
							SPEAKERS
						</div>
						<b>Aaron Pinto</b> <em>– GM of Content, Astro Radio</em><br />
						<b>Daphne Lourdes</b> <em>– GM of Digital & Marketing, Astro Radio</em>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						3:15pm – 3:25pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Sharing Session #5</em>
						<div class="agenda-title">
							DIGITAL INFLUENCERS: DOES POWER COME WITH RESPONSIBILITY?  <br />
							Entrusting the Hot-X-Squad to create virality in a brand-safe environment
						</div>
						<!-- <div class="agenda-desc">
							<div class=" short-content">
								Youtubers are popular and powerful. But with the risks they often take for their contents, can brands
								safely advertise with them? Introducing Hot-X-Squad, the 6 digital ‘influen-racters’ who are powerful,
								yet responsible. Their characters are professionally generated in order to inform and engage. They
								understand the importance of brand safety and allows content to be scripted, controlled and customised
								according to the brand message.
							</div>

							<div class="toggle-agenda">
								<div class="collapsed">
									<i class="fas fa-plus"></i> Show More
								</div>
								<div class="hide">
									<i class="fas fa-minus"></i> Hide
								</div>
							</div>
						</div> -->
					</div>
					<div class="col-lg-3 speaker-col">
						<div class="mobile-title">
							SPEAKER
						</div>
						<b>Wong Siah Ping</b> <em>– VP, Chinese Customer Business</em>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						3:25pm – 3:35pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Sharing Session #6</em>
						<div class="agenda-title">
							SENSATIONAL YET UNDERRATED – SEPAK TAKRAW<br />
							Win the hearts, spark the passion of the grassroots
						</div>
						<!-- <div class="agenda-desc">
							<div class=" short-content">
								This skill-based game with acrobatic kicks has all the elements of an entertaining spectator sport. It
								conjures a passionate level of fanaticism amongst the Malay grassroots and has even led to a
								collaboration with an international brand. Now discover how your brand can get close to the action and
								be at the centre of its craziness.
							</div>

							<div class="toggle-agenda">
								<div class="collapsed">
									<i class="fas fa-plus"></i> Show More
								</div>
								<div class="hide">
									<i class="fas fa-minus"></i> Hide
								</div>
							</div>
						</div> -->
					</div>
					<div class="col-lg-3 speaker-col">
						<div class="mobile-title">
							SPEAKERS
						</div>
						<b>Azlan Mubin</b> <em>– Sports Host & Ex-National Sepak Takraw Player </em><br />
						<b>Priscilla Yee</b><em> – Sponsorship Marketing Manager, Astro Arena</em>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						3:35pm – 3:45pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Interactive Game Session #2</em>
						<div class="agenda-title">
							Kick with BIGBIG Style
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						3:45pm – 3:55pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Sharing Session #7</em>
						<div class="agenda-title">
							DIDI & FRIENDS + OMAR & HANA: <br />
							360° way to spread joy & engage the hearts of kids and parents
						</div>
						<!-- <div class="agenda-desc">
							<div class=" short-content">
								Their content is about making people happy and creating value for viewers - which then translates to
								emotional connection, brand following and loyalty. Go 360° with their branding via great content,
								merchandising, digital marketing, mascots, on-ground events and also apps. Because when you win the
								hearts of children, you’ve won the hearts of their parents, too.
							</div>

							<div class="toggle-agenda">
								<div class="collapsed">
									<i class="fas fa-plus"></i> Show More
								</div>
								<div class="hide">
									<i class="fas fa-minus"></i> Hide
								</div>
							</div>
						</div> -->
					</div>
					<div class="col-lg-3 speaker-col">
						<div class="mobile-title">
							SPEAKER
						</div>
						<b>Sinan Ismail</b> <em>– CEO of Digital Durian </em>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						3:55pm – 4:10pm
					</div>
					<div class="col-lg-7 agenda-col">
						<em>Sharing Session #8</em>
						<div class="agenda-title">
							BLOCKBUSTER DIRECTORS REVEAL ALL<br />
							How blockbusters boost brands’ growth
						</div>

						<!-- <div class="agenda-desc">
							<div class=" short-content">
								The current sensations, in the likes of Ande Bernadee, Arif Barhan and Sarah Suhairi, were once only
								known within their own niche realms in Youtube. But on the BIG STAGE, they have a legitimacy to become a
								mainstream star, to finally be known and idolised by everyone. Find out how you can grow your brand with
								these rising stars.
							</div>

							<div class="toggle-agenda">
								<div class="collapsed">
									<i class="fas fa-plus"></i> Show More
								</div>
								<div class="hide">
									<i class="fas fa-minus"></i> Hide
								</div>
							</div>
						</div> -->
					</div>
					<div class="col-lg-3 speaker-col">
						<div class="mobile-title">
							SPEAKERS
						</div>
						<b>Adrian Teh</b> <em>– Director of Paskal</em><br />
						<b>Joel Soh</b> <em>– Director of Polis Evo 2 </em><br />
						<b>Jastina Arshad</b> <em>– VP, Head of Astro Shaw and Nusantara</em>
					</div>

				</div>
				<div class="row">
					<div class="col-lg-2 time-col">
						4:10pm – 5:30pm
					</div>
					<div class="col-lg-7 agenda-col">
						Networking Session
					</div>
				</div>
			</div>
		</div>

		<!-- <div class="footer">
			<b>© 2019 Astro Quake Rights Reserved.</b>
		</div> -->
	</div>

	<div class="float-register" data-toggle="modal" data-target="#downloadCenterModal">
		<div class="container">
			I would like to attend Big Big Show.
			<button type="button" class="btn btn-white">RSVP Now</button>
		</div>
	</div>
	<button @click="registerWebinar()" type="button" class="btn btn-white">Test Now</button>
	<button @click="getWebinar()" type="button" class="btn btn-white">Test Now 2</button>
	


	<!-- Modal Start -->
	<div class="modal fade" ref="vuemodal" id="downloadCenterModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" hidden="true"></div>

				<div class="modal-body">

					<!-- 1.0 Register -->
					<div v-if="showStep == 1" class="">
						<div>
							<button type="button" class="close font-brand mb-3" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">Close</span>
							</button>

							<div style="clear: both;"></div>
						</div>
						<div class="border-bottom border-grey">
							<div class="row align-items-center mb-0 mb-md-3">
								<div class="col-md-7 order-md-last text-right">
									<button @click="showStep = 3" type="button" id="sticky" class="btn btn-pink">Express RSVP for Quake Club Members <i
											class="fas fa-angle-right"></i></button>
								</div>
								<div class="col-md-5">
									<h2 class="h2 mb-0">Almost there!</h2>
								</div>
							</div>

							<p>Complete the registration form below:</p>

						</div>
						<div class="py-3">
							<em class="text-blue f-14 mb-3 d-block">* All fields are mandatory.</em>

							<form class="club-form" action="" method="post">
								<div class="field-placey form-group">
									<div class="form-text form-error" v-if="!checkComEmail">Please fill in your company’s email address
									</div>
									<div class="form-text form-error" v-if="!checkEmailExist">This email address is already registered.
									</div>
									<input autocomplete="quake" id="com_email" v-model="userEmail"
										pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control"
										:class="{'check-validate': !checkComEmail}" name="jform[email1]" type="email" id="email"
										placeholder=" " required>
									<label for="com_email">Company email</label>
								</div>

								<div class="field-placey form-group">
									<div class="form-text form-error" v-if="!checkUserName">Please fill in your full name</div>
									<input autocomplete="quake" id="user_name" v-model="userName" class="form-control"
										 :class="{'check-validate': !checkUserName}" name="jform[name]" type="text" placeholder=" " required>
									<label for="name">Full name</label>
								</div>

								<div class="field-placey form-group">
									<div class="form-text form-error" v-if="!checkCompany">Please fill in your company’s name</div>
									<input v-model="vcompname" autocomplete="quake" class="form-control"
										:class="{'check-validate': !checkCompany}" name="jform[company]" id="company" type="text"
										placeholder=" " required>
									<label for="company">Company’s name</label>
								</div>

								<div class="field-placey form-group">
									<div class="form-text form-error" v-if="!checkDesignation">Please fill in your job designation</div>
									<input autocomplete="quake" id="designation" class="form-control" v-model="vdesignation"
										:class="{'check-validate': !checkDesignation}" name="jform[designation]" type="text"
										placeholder=" " required>
									<label for="designation">Designation</label>
								</div>

								<div class="mb-4">
									<div class="row">
										<div class="col-md-4 col-5">
											<select v-model="userCountryCode" name="jform[country_code]" id="country_code"
												class="selectpicker white-dropdown" :class="{'check-validate': !checkCountryCode}"
												data-width="100%" required>
												<option v-for="item in country_code" :value="item">{{item}}</option>
											</select>
										</div>
										<div class="col-md-8 col-7">
											<div class="field-placey">
												<div class="form-text form-error" v-if="!checkMobile">Please fill in your mobile number</div>
												<div class="form-text form-error" v-if="!checkMobileValid">Please fill in a valid mobile number
												</div>
												<input autocomplete="quake" v-model="userContactNumber" class="form-control"
													:class="{'check-validate': !checkMobile}" id="mobile" name="jform[contact_number]" type="tel"
													placeholder=" " required>
												<label for="mobile">Mobile number</label>
											</div>
										</div>
									</div>
								</div>

								<div class="custom-control text-left custom-checkbox mt-4 remember-me">

									<input type="checkbox" class="custom-control-input" id="agree-terms" v-model="checkPDPAStatus">
									<label class="custom-control-label text-white" for="agree-terms">I will agree to the <a
											href="#" class="text-white" data-toggle="modal" data-target="#pdpa"><u>Personal Data Protection Act</u></a></label>
											<div class="form-text form-error" v-if="!checkPDPAStatusShow">Checkbox Require
												</div>
								</div>

								<button type="button" @click="fillFrom()" class="btn btn-pink btn-block mt-4">
									RSVP Now
								</button>

							</form>
						</div>

					</div>
					<!-- END Register -->

					<!-- 1.1 Login Quake -->
					<div v-show="showStep == 3" class="">
						<div>
							<button @click="showStep = 1" type="button" class="close float-left back font-brand mb-3">
								<span><i class="fas fa-angle-left"></i> Back</span>
							</button>

							<button type="button" class="close font-brand mb-3" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">Close</span>
							</button>

							<div style="clear: both;"></div>
						</div>
						<div class="text-center">
							<img src="./images/big2019/rsvp-quake.svg" class="mx-auto mt-5 mb-4" alt="RSVP with Quake Club">
						</div>
						<div class="py-3">
							<form class="club-form" action="" method="post">
								<div class="field-placey form-group text-center">
									<div class="form-text form-error" v-if="!checklogin_username">Please fill in your email address
									</div>
									<i class="fas fa-envelope form-icon"></i>
									<input type="text" v-model="login_username" name="username" id="username" placeholder="example@email.com" value=""
										class="validate-username required" size="25" required="" aria-required="true" autofocus=""
										autocomplete="quake" readonly onfocus="this.removeAttribute('readonly');">
									<label id="username-lbl" for="username" class="required">Email</label>
								</div>

								<div class="field-placey form-group text-center">
									<div class="form-text form-error" v-if="!checklogin_password">Please fill in password
									</div>
									<div class="form-text form-error" v-if="!checkLoginAccess">Invalid email or password
									</div>
									 <i class="fas fa-key form-icon"></i>
									<!--<i class="far fa-eye-slash toggle-password"></i> -->
									<input type="password" name="password" id="password" placeholder=" " value="" v-model="login_password"
										class="validate-password required form-control" size="25" maxlength="99" required=""
										aria-required="true" autocomplete="quake" readonly onfocus="this.removeAttribute('readonly');">
									<label id="password-lbl" for="password" class="required">Password</label>
								</div>

								<button @click="login()" type="button" class="btn btn-pink btn-block mt-2">
									RSVP Now
								</button>

								<div class="text-center mt-4">
									<a class="bold link-underline border-white text-white" target="_blank"
										href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
										Forgot your password?
									</a>
								</div>

							</form>
						</div>

					</div>
					<!-- END Login Quake -->

					<!-- 2. Step Success -->
					<div v-if="showStep == 2">
						<div class="text-center">
							<img src="./images/big2019/success.svg" class="icon-done" alt="">

							<h2 class="h2 text-white">RSVP Successful!</h2>
							<span v-if="!checkLAldyRegisted">
								<h2 class="h2 text-highlight">{{userName}}</h2> <!-- Name -->
								<p class="text-white mb-4 font-weight-bold">Thank you and see you on 31st July in Sheraton PJ!</p>
							</span>
							<span v-if="checkLAldyRegisted">
								<h2>Hi <span class="h2 text-highlight">{{userName}}</span>,</h2> <!-- Name -->
								<p class="text-white mb-4 font-weight-bold">you have already registered to this event. See you on 31st July in Sheraton PJ!</p>
							</span>

							<button type="button" data-dismiss="modal" class="btn btn-pink btn-block">Close</button>
						</div>

						<!-- For Normal RSVP: Ask to join Quake -->
						<div class="pink-block" v-if="checkLoginStatus">
							<h3 class="f-20">Are you interested to join Quake Club?</h3>
							<p>A sharing community for marketing professionals</p>

							<a href="./notice" target="_blank" class="btn btn-white btn-block">Sign Up Now</a>
						</div>
						<!---------------------------------------->

					</div>
					<!-- END Step Success -->


				</div>
			</div>
		</div>
	</div>
	<!-- Modal End -->

	<!-- Modal Start -->
	<div class="modal fade custom-modal" ref="vuemodal" id="pdpa" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close font-brand mb-3" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">Close</span>
					</button>
					<h2 class="mb-4">PDPA Policy</h2>
        		<div class="scroll-content">
							<p>The Personal Data Protection Act 2010 was introduced by the Government to regulate the processing of personal data in commercial transactions. The Act, which applies to all companies and firms that are established in Malaysia, requires us to inform you of your rights in respect of your personal data that is being processed or that is to be collected and further processed by us and the purposes for the data processing. The Act also requires us to obtain your consent to the processing of your personal data.</p>

	        		<p>Consequently, please be informed that the personal data and other information (collectively, “Personal Data”) provided in your application to register for the use of the Astro Quake Website and, if relevant, for subscription to Astro Quake Services may be used and processed by Astro Malaysia Holdings Sdn Bhd (“AMH”) for the following purposes:-</p>

	        		<ul>
	        			<li>assessing your application to register for the use of the Astro Quake Website</li>
	        			<li>assessing your application for subscription</li>
	        			<li>to communicate with you;</li>
	        			<li>to provide services to you;</li>
	        			<li>to process your payment transactions;</li>
	        			<li>respond to your inquiries;</li>
	        			<li>administer your participation in contests;</li>
	        			<li>conduct internal activities;</li>
	        			<li>market surveys and trend analysis;</li>
	        			<li>to provide you with information on products and services of AMH and our related corporations;</li>
	        			<li>to provide you with information on products and services of our business partners;</li>
	        			<li>other legitimate business activities of AMH;</li>
	        			<li>such other purposes as set out in the Astro Quake Website Terms of Use; and/or</li>
	        			<li>if relevant, such other purposes as set out in the General Terms and Conditions and, if applicable, Campaign Terms and Conditions.</li>
	        		</ul>

	        		<p>(collectively “Purposes”).</p>

	        		<p>Further, please be informed that if required for any of the foregoing Purposes, your Personal Data may be transferred to locations outside Malaysia or disclosed to our related corporations, licensees, business partners and/or service providers, who may be located within or outside Malaysia. Save for the foregoing, your Personal Data will not be knowingly transferred to any place outside Malaysia or be knowingly disclosed to any third party.</p>

	        		<p>In order to process your Personal Data, your consent is required.  If you do not consent to the processing of your Personal Data other than in relation to the advertising or marketing of any product or service of AMH or our business partners, we cannot process your Personal Data for any of the above Purposes and we will not be able to approve your application for registration for use of the Astro Quake website.</p>

	        		<p>In relation to direct marketing, you may request AMH by written notice (in accordance with the following paragraph) not to process your Personal Data for any of the following Purposes: (i) advertising or marketing via phone any product or service of AMH or our business partners, (ii) sending to you via post any advertising or marketing material relating to any product or service of AMH or our business partners; (iii) sending to you via email or SMS any advertising or marketing material relating to any product or service of AMH or our business partners, or (iv) communicating to you by whatever means any advertising or marketing material of AMH or our business partners.</p>

	        		<p>You may at any time hereafter make inquiries, complaints and request for access to, or correction of, your Personal Data or limit the processing of your Personal Data by submitting such request to the Personal Data Protection Officer of AMH via registered post or email as set out below.</p>

	        		<p><strong>Postal address:</strong></p>

	        		<p>Personal Data Protection Officer,
			        			<br>
								Astro Malaysia Holdings Sdn Bhd,
								<br>
								Peti Surat 10335,
								<br>
								50710 Kuala Lumpur
							</p>

							<p><strong>Email address:</strong>&nbsp;<a href="mailto:pdpo@astro.com.my" class="text-white">pdpo@astro.com.my</a></p>
        		</div>

					<button type="button" data-dismiss="modal" class="btn btn-block btn-pink mt-3">Back</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal End -->
</div>

<script>
  var fillFormUrl = <?php echo json_encode($fillFormUrl); ?>;
	var loginUrl = <?php echo json_encode($loginUrl); ?>;
	var getWebinar = <?php echo json_encode($getWebinar); ?>;
	var obj = <?php echo json_encode($obj); ?>;

	

	jQuery( document ).ready(function() {
		// jQuery('.content .container').addClass('container-fluid');
		// jQuery('.content .container').removeClass('container');
		// jQuery('body').addClass('bigbigshow2019');
		jQuery(".btm-footer").hide();
		document.title = 'Bigbigshow2019';

		jQuery(".toggle-agenda").click(function() {
			jQuery( this ).parent('.agenda-desc').toggleClass( "expand" );
		});

		jQuery('#pdpa').on('shown.bs.modal', function (e) {
			jQuery('#downloadCenterModal').addClass('dimmed');
		});

		jQuery('#pdpa').on('hide.bs.modal', function (e) {
			jQuery('#downloadCenterModal').removeClass('dimmed');
		});

	});



	var app = new Vue({
    	el: '#app',
    	data: {
				showStep:1,
				checkComEmail:true,
				checkEmailExist:true,
				checkUserName:true,
				checkCompany:true,
				checkDesignation:true,
				checkMobile:true,
				checkMobileValid:true,
				checkCountryCode:true,
				checklogin_username:true,
				checklogin_password:true,
				checkPDPAStatusShow:true,
				checkLoginAccess:true,
				checkLoginStatus: false,
				checkPDPAStatus:false,
				userEmail:"",
				userName:"",
				vcompname:"",
				userCountryCode:"+60",
				userContactNumber:"",
				vdesignation:"",
				login_username:"",
				login_password:"",
				country_code : ["+60","+65","+86","+852","+91"],
				result: []

    	},
    	mounted: function () {
				_this = this;
				_this.result = obj;
				jQuery('#downloadCenterModal').on('hidden.bs.modal', function () {
					_this.showStep = 1;
					_this.checkComEmail=true;
					_this.checkEmailExist=true;
					_this.checkUserName=true;
					_this.checkCompany=true;
					_this.checkDesignation=true;
					_this.checkMobile=true;
					_this.checkMobileValid=true;
					_this.checkCountryCode=true;
					_this.checkPDPAStatusShow = true;
					_this.checklogin_username=true;
					_this.checklogin_password=true;
					_this.checkLoginStatus= false;
					_this.checkPDPAStatus= false;
					_this.checkLoginAccess= true;
					_this.checkLAldyRegisted= false;
					_this.userEmail="";
					_this.userName="";
					_this.vcompname="";
					_this.userCountryCode="+60";
					_this.userContactNumber="";
					_this.vdesignation="";
					_this.login_username="";
					_this.login_password="";
				})

				jQuery('input').change(function(){
					this.value = jQuery.trim(this.value);
					jQuery(this)[0].dispatchEvent(new Event('input'));
				});

				jQuery(".toggle-password").click(function() {

					jQuery(this).toggleClass("fa-eye fa-eye-slash");
					var input = jQuery(this).next();
					if (input.attr("type") == "password") {
						input.attr("type", "text");
					} else {
						input.attr("type", "password");
					}
				});
    	},
    	updated: function () {
				jQuery(this.$el).find('.selectpicker').selectpicker('refresh');
			},
    	methods: {
				fillFrom: function (){

					if (this.userEmail.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/) == null) {
						this.checkComEmail = false;
					}else{
						this.checkComEmail = true;
					}
					if (this.userName == "") {
						this.checkUserName = false;
					}else{
						this.checkUserName = true;
					}
					if (this.vcompname == "") {
						this.checkCompany = false;
					}else{
						this.checkCompany = true;
					}
					if (this.vdesignation == "") {
						this.checkDesignation = false;
					}else{
						this.checkDesignation = true;
					}
					// if (this.userContactNumber == "") {
					// 	this.checkMobile = false;
					// }else{
					// 	this.checkMobile = true;
					// }
					if (!this.userContactNumber.match(/.{9,16}/)) {
						this.checkMobileValid = false;
					}else{
						this.checkMobileValid = true;
					}
					if (!this.checkPDPAStatus) {
						this.checkPDPAStatusShow = false;
					}else{
						this.checkPDPAStatusShow =true;
					}

					if (this.checkMobileValid && this.checkMobile && this.checkDesignation && this.checkCompany && this.checkUserName && this.checkComEmail && this.checkPDPAStatus) {
						_this = this;
						jQuery.ajax({
									url: fillFormUrl,
									type: 'post',
									data: {
										'email':_this.userEmail,
										'name':_this.userName,
										'comp_name':_this.vcompname,
										'mobileNo':_this.userCountryCode+'-'+_this.userContactNumber,
										'designation':_this.vdesignation
										},
									success: function (result) {
										var tmp = result.split("-");

										if (tmp[0] == 2) {
											_this.showStep = 2;
											_this.checkLoginStatus = true;
											_this.userName = tmp[1];
											_this.checkLAldyRegisted = true;

										}else if(tmp[0] == 0) {
											_this.showStep = 1;

										} else {
											_this.showStep = 2;
											_this.checkLoginStatus = true;
											_this.userName = tmp[1];
											_this.checkLAldyRegisted = false;
										}

									},
									error: function () {
										console.log('fail');
									}
								});
					}
				},
				registerWebinar: function(){
					_this=this;
					jQuery.ajax({
                url: "https://api.webinarjam.com/webinarjam/register",
								type: 'post',
								data: {
									'api_key' : '924579a7-73a3-4241-8efc-95b7a0847088',
									'webinar_id' : 18,
									'first_name' : "asd",
									'email' : 'cheeliem.tan@comma.com.my',
									'schedule' : 1
									},
                success: function (result) {
									console.log(result);
                },
                error: function () {
                  console.log('fail');
                }
              });
				},
				getWebinar: function(){
					_this=this;
					jQuery.ajax({
								url: getWebinar,
								type: 'post', 
                success: function (result) {
									var zzz = JSON.parse(result);
									console.log(zzz);
                },
                error: function () {
                  console.log('fail');
                }
              });
				},
				login: function (){

					if (this.login_username == "") {
						this.checklogin_username = false;
					}else{
						this.checklogin_username = true;
					}
					if (this.login_password == "") {
						this.checklogin_password = false;
					}else{
						this.checklogin_password = true;
					}
					if (this.checklogin_username && this.checklogin_password) {
						_this = this;

						jQuery.ajax({
                url: loginUrl,
                type: 'post',
                data: {
									'email':_this.login_username,
									'password':_this.login_password,
									},
                success: function (result) {
									var tmp = result.split("-");

									if (tmp[0] == 2) {
										_this.showStep = 2;
										_this.userName = tmp[1];
										_this.checkLAldyRegisted = true;
									}else if(tmp[0] == 0) {
										_this.showStep = 3;
										_this.checkLoginAccess = false;
									} else {
										_this.showStep = 2;
										_this.userName = tmp[1];
										_this.checkLAldyRegisted= false;
									}

                },
                error: function () {
                  console.log('fail');
                }
              });
					}
				},
    		backToPrevious: function () {
    			this.showStep--;
    		},
    		goToNext: function () {
    			var address = document.getElementById('address');
    			var user_name = document.getElementById('user_name');
    			var business_card = document.getElementById('imgInp');

    			switch (this.showStep) {
    				case 1:
    					this.showStep++;
    					break;
    				case 2:
    					this.checkUserName = user_name.checkValidity();
    					this.checkAddress = address.checkValidity();
							if (this.businessCard == "") {
								this.checkBusinessCard = false;
							}else{
								this.checkBusinessCard = true;
							}

    					if (address.checkValidity() && user_name.checkValidity() && this.checkBusinessCard && this.checkImgSize) {
    						this.showStep++;
    					}
    					break;

    				default:
    					break;
    			}
    		}
    	}
    })

</script>
