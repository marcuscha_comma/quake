<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Rsvp_tour
 * @author     Ice <Ice@example.com>
 * @copyright  Ice
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Rsvp_tour', JPATH_COMPONENT);
JLoader::register('Rsvp_tourController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Rsvp_tour');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
