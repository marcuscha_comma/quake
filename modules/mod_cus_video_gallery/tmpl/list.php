<?php
/**
 * @version     CVS: 1.0.0
 * @package     com_cus_video_gallery
 * @subpackage  mod_cus_video_gallery
 * @author      tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright   2020 tan chee liem
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$elements = ModCus_video_galleryHelper::getList($params);

$tableField = explode(':', $params->get('field'));
$table_name = !empty($tableField[0]) ? $tableField[0] : '';
$field_name = !empty($tableField[1]) ? $tableField[1] : '';
?>

<?php if (!empty($elements)) : ?>
	<table class="table">
		<?php foreach ($elements as $element) : ?>
			<tr>
				<th><?php echo ModCus_video_galleryHelper::renderTranslatableHeader($table_name, $field_name); ?></th>
				<td><?php echo ModCus_video_galleryHelper::renderElement(
						$table_name, $params->get('field'), $element->{$field_name}
					); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
<?php endif;
