<?php

/**
 * @version     CVS: 1.0.0
 * @package     com_cus_video_gallery
 * @subpackage  mod_cus_video_gallery
 * @author      tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright   2020 tan chee liem
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Helper\ModuleHelper;

// Include the syndicate functions only once
JLoader::register('ModCus_video_galleryHelper', dirname(__FILE__) . '/helper.php');

$doc = Factory::getDocument();

/* */
$doc->addStyleSheet(URI::base() . 'media/mod_cus_video_gallery/css/style.css');

/* */
$doc->addScript(URI::base() . 'media/mod_cus_video_gallery/js/script.js');

require ModuleHelper::getLayoutPath('mod_cus_video_gallery', $params->get('content_type', 'blank'));
