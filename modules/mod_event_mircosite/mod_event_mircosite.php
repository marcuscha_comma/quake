<?php

/**
 * @version     CVS: 1.0.0
 * @package     com_event_mircosite
 * @subpackage  mod_event_mircosite
 * @author      tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright   2020 tan chee liem
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Helper\ModuleHelper;

// Include the syndicate functions only once
JLoader::register('ModEvent_mircositeHelper', dirname(__FILE__) . '/helper.php');

$doc = Factory::getDocument();

/* */
$doc->addStyleSheet(URI::base() . 'media/mod_event_mircosite/css/style.css');

/* */
$doc->addScript(URI::base() . 'media/mod_event_mircosite/js/script.js');

require ModuleHelper::getLayoutPath('mod_event_mircosite', $params->get('content_type', 'blank'));
