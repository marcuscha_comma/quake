<?php

/**
 * @version     CVS: 1.0.0
 * @package     com_replicate_brand
 * @subpackage  mod_replicate_brand
 * @author      tan chee liem <cheeliem.tan@comma.com.my>
 * @copyright   2020 tan chee liem
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Helper\ModuleHelper;

// Include the syndicate functions only once
JLoader::register('ModReplicate_brandHelper', dirname(__FILE__) . '/helper.php');

$doc = Factory::getDocument();

/* */
$doc->addStyleSheet(URI::base() . 'media/mod_replicate_brand/css/style.css');

/* */
$doc->addScript(URI::base() . 'media/mod_replicate_brand/js/script.js');

require ModuleHelper::getLayoutPath('mod_replicate_brand', $params->get('content_type', 'blank'));
