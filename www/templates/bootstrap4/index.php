<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$menu = $app->getMenu();
$menu_items = $menu->getItems('menutype', 'top');
$this->language = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
$isMobile = "";
$checkEmailUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.checkEmailExist');
$createClientUrl = JRoute::_('index.php?option=com_downlaod_files&task=dowloadfile.createClient');

if ($task == "edit" || $layout == "form") {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}

// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap.min.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/fontawesome-all.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap-select.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/owl.carousel.min.css"');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/owl.theme.default.min.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/lity/dist/lity.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/style.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/modal.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/js/light-slider/dist/css/lightslider.css');

// Add scripts
JHtml::_('jquery.framework');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/popper.min.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/bootstrap.min.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/vue.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/jquery.matchHeight.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/bootstrap-select.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/lity/dist/lity.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/chart-js/dist/Chart.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/script.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/light-slider/dist/js/lightslider.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/chartjs-plugin-labels.js');


// Adjusting content width
if ($this->countModules('sidebar-left') && $this->countModules('sidebar-right')) {
    $span = "col-md-6";
} elseif ($this->countModules('sidebar-left') && !$this->countModules('sidebar-right')) {
    $span = "col-md-9";
} elseif (!$this->countModules('sidebar-left') && $this->countModules('sidebar-right')) {
    $span = "col-md-9";
} else {
    $span = "col-md-12";
}

//get all slider
$db_cus_sliders_banner    = JFactory::getDBO();
$query_cus_sliders_banner = $db_cus_sliders_banner->getQuery( true );
$query_cus_sliders_banner
  ->select( '*' )
  ->from( $db_cus_sliders_banner->quoteName( '#__cus_sliders_banner' ) )
  ->where($db_cus_sliders_banner->quoteName('state')." > 0")
  ->order( 'id asc' );
$db_cus_sliders_banner->setQuery( $query_cus_sliders_banner );
$cus_sliders_banner = $db_cus_sliders_banner->loadObjectList();

//get all slider highlighs
$db_cus_sliders_highlight    = JFactory::getDBO();
$query_cus_sliders_highlight = $db_cus_sliders_highlight->getQuery( true );
$query_cus_sliders_highlight
  ->select( '*' )
  ->from( $db_cus_sliders_highlight->quoteName( '#__cus_sliders_highlight' ) )
  ->where($db_cus_sliders_highlight->quoteName('state')." > 0")
  ->order( 'id asc' );
$db_cus_sliders_highlight->setQuery( $query_cus_sliders_highlight );
$cus_sliders_highlight = $db_cus_sliders_highlight->loadObjectList();

//get all slider highlighs
$db_cus_banner_v1ewership    = JFactory::getDBO();
$query_cus_banner_v1ewership = $db_cus_banner_v1ewership->getQuery( true );
$query_cus_banner_v1ewership
  ->select( '*' )
  ->from( $db_cus_banner_v1ewership->quoteName( '#__cus_banner_v1ewership' ) )
  ->order( 'id asc' );
$db_cus_banner_v1ewership->setQuery( $query_cus_banner_v1ewership );
$cus_banner_v1ewership = $db_cus_banner_v1ewership->loadObjectList();

//detect mobile or desktop
if (stristr($_SERVER['HTTP_USER_AGENT'],'mobi')!==FALSE) {
  $isMobile = True;
}else{
  $isMobile = False;
}
// echo $this->params->get('favicon');

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <jdoc:include type="head" />
        <link rel="shortcut icon" href="<?php echo JUri::root(true) . htmlspecialchars($this->params->get('favicon'), ENT_COMPAT, 'UTF-8'); ?>" />
        <!--[if lt IE 9]>
                <script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
        <![endif]-->
    </head>
    <body>
      <header class="navbar navbar-expand-lg navbar-light">
        <?php if ($this->countModules('position-1')) : ?>
            <!-- Logo start -->
            <a class="brand pull-left" href="<?php echo $this->baseurl; ?>/">
              <?php echo '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename .'" />'; ?>
            </a>
            <!-- Logo End -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <div id="nav-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              </div>
            </button>
            <!-- Menu List Start -->
            <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="menu navbar-nav ml-auto">
            <?php $i=0; foreach ($menu_items as $key => $menu_item_value) {

              if ($menu_item_value->level == 1) {
                if ($menu_item_value === end($menu_items)) {
                  echo "<li>";
                  echo "<a class='nav-link last' href='". $this->baseurl.'/'.$menu_item_value->alias ."'>" . $menu_item_value->title . "</a>";
                  echo "</li>";
                }else{
                  echo "<li>";
                  echo "<a class='nav-link' href='". $this->baseurl.'/'.$menu_item_value->alias ."'>" . $menu_item_value->title . "</a>";
                  echo "</li>";
                }
              }

              ?>
            <?php }; ?>
            </ul>
              <!-- <jdoc:include type="modules" name="position-1" style="none"/> -->
            </div>
            <!-- Menu List End -->
        <?php endif; ?>
      </header>

        <div class="body">
            <div class="content">
                <!-- Head Banner Start -->
                <div class="page-banner-wrapper">
                  <jdoc:include type="modules" name="top-banner" style="none" />
                </div>
                <?php if ($menu->getActive() == $menu->getDefault()) :
                ?>
                  <div class="owl-carousel owl-theme" id="index-banner">
                      <?php foreach ($cus_sliders_banner as $key => $cus_sliders_banner_value) {
                         /**
                         * link_type value meaning
                         * 0 = Don't link
                         * 1 = Url Address
                         * 2 = Youtube
                         */
                        ?>
                        <div class="item-video">
                          <?php if ($cus_sliders_banner_value->link_type == 2) {?>

                            <a href="<?php echo $cus_sliders_banner_value->youtube_link; ?>" data-lity>
                            <div class="banner-image" style="background-image: url('<? echo $cus_sliders_banner_value->desktop_image;?>')">
                              <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                            </a>

                          <?php }elseif ($cus_sliders_banner_value->link_type == 1) { ?>

                            <a target="<?php echo $cus_sliders_banner_value->target_window; ?>" href="<?php echo $cus_sliders_banner_value->url_address; ?>">
                            <div class="banner-image" style="background-image: url('<? echo $cus_sliders_banner_value->desktop_image;?>')">
                              <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                            </a>

                          <?php }elseif ($cus_sliders_banner_value->link_type == 0) { ?>
                            <div class="banner-image" style="background-image: url('<? echo $cus_sliders_banner_value->desktop_image;?>')">
                              <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                          <?php } ?>
                        </div>
                        <?php } ?>
                  </div>
                <?php endif; ?>

                <div id="inline" style="background:#fff" class="lity-hide">
                Inline content
                </div>
                <!-- Head Banner End -->
                <!-- Slider Highlights Start -->
                <?php if ($menu->getActive() == $menu->getDefault()) : ?>
                <div class="highlight-banner-class">
                  <div class="container">
                    <h2>Quake Happenings</h2>
                    <p>Stay-up-to date</p>
                    <div class="owl-carousel owl-theme" id="highlight-banner">
                      <?php foreach ($cus_sliders_highlight as $key => $cus_sliders_highlight_value) { ?>

                          <div class="item highlight-banner-wrapper">
                          <a href="<?php echo $cus_sliders_highlight_value->url_address; ?>">
                            <div class="highlight-image" style="background-image: url('<? echo $cus_sliders_highlight_value->image;?>')">
                              <img src="images/2020/banner-mobile-guide.png" class="guide-xs" />
                            </div>
                            <div class="highlight-banner-div">
                              <div class="highlight-banner-tag"><?php echo $cus_sliders_highlight_value->tag; ?></div>
                              <h3><?php echo $cus_sliders_highlight_value->title; ?></h3>
                              <p><?php echo $cus_sliders_highlight_value->description; ?></p>
                            </div>
                          </a>
                          </div>

                      <?php } ?>
                    </div>
                  </div>
                </div>

                <?php endif; ?>
                <!-- Slider Highlights End -->
                <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                    <jdoc:include type="modules" name="banner" style="xhtml" />
                    <?php if ($this->countModules('breadcrumbs')) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <jdoc:include type="modules" name="breadcrumbs" style="xhtml" />
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <?php if ($this->countModules('sidebar-left')) : ?>
                            <div id="sidebar" class="col-md-3">
                                <div class="sidebar-nav">
                                    <jdoc:include type="modules" name="sidebar-left" style="xhtml" />
                                </div>
                            </div>
                        <?php endif; ?>
                        <main id="content" role="main" class="<?php echo $span; ?>">
                            <jdoc:include type="modules" name="position-3" style="xhtml" />
                            <jdoc:include type="message" />
                            <jdoc:include type="component" />
                             <!-- <jdoc:include type="modules" name="position-2" style="none" />  -->
                        </main>
                        <?php if ($this->countModules('sidebar-right')) : ?>
                            <div id="aside" class="col-md-3">
                                <jdoc:include type="modules" name="sidebar-right" style="xhtml" />
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <footer class="" role="contentinfo">
            <jdoc:include type="modules" name="viewership" style="none" />
            <jdoc:include type="modules" name="footer" style="none" />

            <div class="footer">
              <div class="container">
                <div class="row mb-4 mb-lg-5 align-items-center">
                  <div class="col-6">
                    <img src="images/2020/astro-media.png" alt="Astro Media Sales" width="95" class="astro-media">
                  </div>
                  <div class="col-6 text-right">
                    <h4 class="upper d-inline-block">Follow us on</h4>
                    <a href="#" target="_blank"><i class="ml-2 fab fa-facebook-square text-highlight"></i></a>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4 mb-3 mb-md-0">
                    <ul class="upper list-unstyled mb-0">
                      <li>
                        <a href="./privacy-policy"><h4>Privacy Policy</h4></a>
                      </li>
                      <li>
                        <a href="./privacy-notice"><h4>Privacy Notice</h4></a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-4 mb-4 mb-md-0">
                    <h4 class="upper">Astro Media Sales</h4>
                    <div>
                      <address class="mb-2">
                        Lower Penthouse B-21-1, Northpoint Offices,<br />
                        No. 1 Medan Syed Putra Utara,<br />
                        59200 Kuala Lumpur
                      </address>
                      <a href="./about-quake/get-in-touch" class="bold text-highlight">Be in Touch</a>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <h4 class="upper">Subscribe to Our Newsletter</h4>
                    <p>Be in the know of all things Quake</p>

                    <form class="input-group" action="" id="subscribe-form">
                    <input id="subscribe-email" type="email" class="form-control" placeholder="What's Your Email Address?" aria-label="Email Address" aria-describedby="button-addon2" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                      <div class="input-group-append">
                          <img id="loading-img" style="height: 51px;width: 50px; background-color:white;" src="" alt="">
                          <button class="btn btn-link" type="submit" id="button-addon2">Subscribe</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <!-- <div class="container<?php// echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>"> -->
            <div class="container">
              <div class="copyright">
                &copy; <?php echo date('Y'); ?> Astro Quake Right Reserved.
              </div>
            </div>

            <a id="backTop">
                <i class="fas fa-chevron-up"></i>
            </a>
        </footer>
        <jdoc:include type="modules" name="debug" style="none" />
    </body>
</html>
<script>
  var checkEmailUrl = <?php echo json_encode($checkEmailUrl); ?>;
  var createClientUrl = <?php echo json_encode($createClientUrl); ?>;
  var banner_viewership = <?php echo json_encode($cus_banner_v1ewership); ?>;
  //bind viewership data from db
  jQuery('#viewership-figure-1').html(banner_viewership[0]['block_1_title']);
  jQuery('#viewership-content-1').html(banner_viewership[0]['block_1_description']);

  jQuery('#viewership-figure-2').html(banner_viewership[0]['block_2_title']);
  jQuery('#viewership-content-2').html(banner_viewership[0]['block_2_description']);

  jQuery('#viewership-figure-3').html(banner_viewership[0]['block_3_title']);
  jQuery('#viewership-content-3').html(banner_viewership[0]['block_3_description']);

</script>
<?php
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/owl.carousel.min.js');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/custom.js');
?>
